// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ONLINEBLUEPRINTSUPPORT_K2Node_InAppPurchaseQuery2_generated_h
#error "K2Node_InAppPurchaseQuery2.generated.h already included, missing '#pragma once' in K2Node_InAppPurchaseQuery2.h"
#endif
#define ONLINEBLUEPRINTSUPPORT_K2Node_InAppPurchaseQuery2_generated_h

#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_SPARSE_DATA
#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_InAppPurchaseQuery2(); \
	friend struct Z_Construct_UClass_UK2Node_InAppPurchaseQuery2_Statics; \
public: \
	DECLARE_CLASS(UK2Node_InAppPurchaseQuery2, UK2Node_BaseAsyncTask, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OnlineBlueprintSupport"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_InAppPurchaseQuery2)


#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_InAppPurchaseQuery2(); \
	friend struct Z_Construct_UClass_UK2Node_InAppPurchaseQuery2_Statics; \
public: \
	DECLARE_CLASS(UK2Node_InAppPurchaseQuery2, UK2Node_BaseAsyncTask, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OnlineBlueprintSupport"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_InAppPurchaseQuery2)


#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_InAppPurchaseQuery2(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_InAppPurchaseQuery2) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_InAppPurchaseQuery2); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_InAppPurchaseQuery2); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_InAppPurchaseQuery2(UK2Node_InAppPurchaseQuery2&&); \
	NO_API UK2Node_InAppPurchaseQuery2(const UK2Node_InAppPurchaseQuery2&); \
public:


#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_InAppPurchaseQuery2(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_InAppPurchaseQuery2(UK2Node_InAppPurchaseQuery2&&); \
	NO_API UK2Node_InAppPurchaseQuery2(const UK2Node_InAppPurchaseQuery2&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_InAppPurchaseQuery2); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_InAppPurchaseQuery2); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_InAppPurchaseQuery2)


#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_10_PROLOG
#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_INCLASS \
	Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class K2Node_InAppPurchaseQuery2."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONLINEBLUEPRINTSUPPORT_API UClass* StaticClass<class UK2Node_InAppPurchaseQuery2>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineSubsystemUtils_Source_OnlineBlueprintSupport_Classes_K2Node_InAppPurchaseQuery2_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
