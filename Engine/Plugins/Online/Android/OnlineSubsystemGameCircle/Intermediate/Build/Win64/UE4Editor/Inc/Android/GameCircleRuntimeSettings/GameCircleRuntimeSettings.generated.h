// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMECIRCLERUNTIMESETTINGS_GameCircleRuntimeSettings_generated_h
#error "GameCircleRuntimeSettings.generated.h already included, missing '#pragma once' in GameCircleRuntimeSettings.h"
#endif
#define GAMECIRCLERUNTIMESETTINGS_GameCircleRuntimeSettings_generated_h

#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_SPARSE_DATA
#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_RPC_WRAPPERS
#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameCircleRuntimeSettings(); \
	friend struct Z_Construct_UClass_UGameCircleRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(UGameCircleRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/GameCircleRuntimeSettings"), NO_API) \
	DECLARE_SERIALIZER(UGameCircleRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUGameCircleRuntimeSettings(); \
	friend struct Z_Construct_UClass_UGameCircleRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(UGameCircleRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/GameCircleRuntimeSettings"), NO_API) \
	DECLARE_SERIALIZER(UGameCircleRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameCircleRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameCircleRuntimeSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameCircleRuntimeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameCircleRuntimeSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameCircleRuntimeSettings(UGameCircleRuntimeSettings&&); \
	NO_API UGameCircleRuntimeSettings(const UGameCircleRuntimeSettings&); \
public:


#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameCircleRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameCircleRuntimeSettings(UGameCircleRuntimeSettings&&); \
	NO_API UGameCircleRuntimeSettings(const UGameCircleRuntimeSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameCircleRuntimeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameCircleRuntimeSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameCircleRuntimeSettings)


#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_13_PROLOG
#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_RPC_WRAPPERS \
	Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_INCLASS \
	Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GameCircleRuntimeSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMECIRCLERUNTIMESETTINGS_API UClass* StaticClass<class UGameCircleRuntimeSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_Android_OnlineSubsystemGameCircle_Source_GameCircleRuntimeSettings_Classes_GameCircleRuntimeSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
