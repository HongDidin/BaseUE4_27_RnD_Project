// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameCircleRuntimeSettings/Classes/GameCircleRuntimeSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameCircleRuntimeSettings() {}
// Cross Module References
	GAMECIRCLERUNTIMESETTINGS_API UClass* Z_Construct_UClass_UGameCircleRuntimeSettings_NoRegister();
	GAMECIRCLERUNTIMESETTINGS_API UClass* Z_Construct_UClass_UGameCircleRuntimeSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_GameCircleRuntimeSettings();
// End Cross Module References
	void UGameCircleRuntimeSettings::StaticRegisterNativesUGameCircleRuntimeSettings()
	{
	}
	UClass* Z_Construct_UClass_UGameCircleRuntimeSettings_NoRegister()
	{
		return UGameCircleRuntimeSettings::StaticClass();
	}
	struct Z_Construct_UClass_UGameCircleRuntimeSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableAmazonGameCircleSupport_MetaData[];
#endif
		static void NewProp_bEnableAmazonGameCircleSupport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableAmazonGameCircleSupport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSupportsInAppPurchasing_MetaData[];
#endif
		static void NewProp_bSupportsInAppPurchasing_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSupportsInAppPurchasing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableFireTVSupport_MetaData[];
#endif
		static void NewProp_bEnableFireTVSupport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableFireTVSupport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugAPIKeyFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DebugAPIKeyFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistributionAPIKeyFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DistributionAPIKeyFile;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GameCircleRuntimeSettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Implements the settings for the OnlineSubsystemGameCircle plugin.\n*/" },
		{ "IncludePath", "GameCircleRuntimeSettings.h" },
		{ "ModuleRelativePath", "Classes/GameCircleRuntimeSettings.h" },
		{ "ToolTip", "Implements the settings for the OnlineSubsystemGameCircle plugin." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableAmazonGameCircleSupport_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Enables the Amazon GameCircle OnlineSubsystem\n" },
		{ "ModuleRelativePath", "Classes/GameCircleRuntimeSettings.h" },
		{ "ToolTip", "Enables the Amazon GameCircle OnlineSubsystem" },
	};
#endif
	void Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableAmazonGameCircleSupport_SetBit(void* Obj)
	{
		((UGameCircleRuntimeSettings*)Obj)->bEnableAmazonGameCircleSupport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableAmazonGameCircleSupport = { "bEnableAmazonGameCircleSupport", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGameCircleRuntimeSettings), &Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableAmazonGameCircleSupport_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableAmazonGameCircleSupport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableAmazonGameCircleSupport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bSupportsInAppPurchasing_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Enables Amazon In App Purchasing\n" },
		{ "ModuleRelativePath", "Classes/GameCircleRuntimeSettings.h" },
		{ "ToolTip", "Enables Amazon In App Purchasing" },
	};
#endif
	void Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bSupportsInAppPurchasing_SetBit(void* Obj)
	{
		((UGameCircleRuntimeSettings*)Obj)->bSupportsInAppPurchasing = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bSupportsInAppPurchasing = { "bSupportsInAppPurchasing", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGameCircleRuntimeSettings), &Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bSupportsInAppPurchasing_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bSupportsInAppPurchasing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bSupportsInAppPurchasing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableFireTVSupport_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Removes touchscreen hardware requirement when enabled\n" },
		{ "DisplayName", "Enable Fire TV Support" },
		{ "ModuleRelativePath", "Classes/GameCircleRuntimeSettings.h" },
		{ "ToolTip", "Removes touchscreen hardware requirement when enabled" },
	};
#endif
	void Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableFireTVSupport_SetBit(void* Obj)
	{
		((UGameCircleRuntimeSettings*)Obj)->bEnableFireTVSupport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableFireTVSupport = { "bEnableFireTVSupport", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGameCircleRuntimeSettings), &Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableFireTVSupport_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableFireTVSupport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableFireTVSupport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_DebugAPIKeyFile_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// The path of the api_key text file generated by Amazon from your debug keystore from the Game Project directory\n" },
		{ "DisplayName", "Debug API Key File" },
		{ "ModuleRelativePath", "Classes/GameCircleRuntimeSettings.h" },
		{ "ToolTip", "The path of the api_key text file generated by Amazon from your debug keystore from the Game Project directory" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_DebugAPIKeyFile = { "DebugAPIKeyFile", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameCircleRuntimeSettings, DebugAPIKeyFile), METADATA_PARAMS(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_DebugAPIKeyFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_DebugAPIKeyFile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_DistributionAPIKeyFile_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// The path of the api_key text file generated by Amazon from your distribution keystore from the Game Project directory\n" },
		{ "DisplayName", "Distribution API Key File" },
		{ "ModuleRelativePath", "Classes/GameCircleRuntimeSettings.h" },
		{ "ToolTip", "The path of the api_key text file generated by Amazon from your distribution keystore from the Game Project directory" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_DistributionAPIKeyFile = { "DistributionAPIKeyFile", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameCircleRuntimeSettings, DistributionAPIKeyFile), METADATA_PARAMS(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_DistributionAPIKeyFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_DistributionAPIKeyFile_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableAmazonGameCircleSupport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bSupportsInAppPurchasing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_bEnableFireTVSupport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_DebugAPIKeyFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::NewProp_DistributionAPIKeyFile,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameCircleRuntimeSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::ClassParams = {
		&UGameCircleRuntimeSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameCircleRuntimeSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameCircleRuntimeSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameCircleRuntimeSettings, 1777141210);
	template<> GAMECIRCLERUNTIMESETTINGS_API UClass* StaticClass<UGameCircleRuntimeSettings>()
	{
		return UGameCircleRuntimeSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameCircleRuntimeSettings(Z_Construct_UClass_UGameCircleRuntimeSettings, &UGameCircleRuntimeSettings::StaticClass, TEXT("/Script/GameCircleRuntimeSettings"), TEXT("UGameCircleRuntimeSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameCircleRuntimeSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
