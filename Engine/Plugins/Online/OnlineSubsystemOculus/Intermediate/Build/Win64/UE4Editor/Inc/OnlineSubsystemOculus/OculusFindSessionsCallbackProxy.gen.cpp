// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Classes/OculusFindSessionsCallbackProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusFindSessionsCallbackProxy() {}
// Cross Module References
	ONLINESUBSYSTEMOCULUS_API UClass* Z_Construct_UClass_UOculusFindSessionsCallbackProxy_NoRegister();
	ONLINESUBSYSTEMOCULUS_API UClass* Z_Construct_UClass_UOculusFindSessionsCallbackProxy();
	ENGINE_API UClass* Z_Construct_UClass_UOnlineBlueprintCallProxyBase();
	UPackage* Z_Construct_UPackage__Script_OnlineSubsystemOculus();
	ONLINESUBSYSTEMUTILS_API UFunction* Z_Construct_UDelegateFunction_OnlineSubsystemUtils_BlueprintFindSessionsResultDelegate__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UOculusFindSessionsCallbackProxy::execFindModeratedSessions)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_MaxResults);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UOculusFindSessionsCallbackProxy**)Z_Param__Result=UOculusFindSessionsCallbackProxy::FindModeratedSessions(Z_Param_MaxResults);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusFindSessionsCallbackProxy::execFindMatchmakingSessions)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_MaxResults);
		P_GET_PROPERTY(FStrProperty,Z_Param_OculusMatchmakingPool);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UOculusFindSessionsCallbackProxy**)Z_Param__Result=UOculusFindSessionsCallbackProxy::FindMatchmakingSessions(Z_Param_MaxResults,Z_Param_OculusMatchmakingPool);
		P_NATIVE_END;
	}
	void UOculusFindSessionsCallbackProxy::StaticRegisterNativesUOculusFindSessionsCallbackProxy()
	{
		UClass* Class = UOculusFindSessionsCallbackProxy::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "FindMatchmakingSessions", &UOculusFindSessionsCallbackProxy::execFindMatchmakingSessions },
			{ "FindModeratedSessions", &UOculusFindSessionsCallbackProxy::execFindModeratedSessions },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics
	{
		struct OculusFindSessionsCallbackProxy_eventFindMatchmakingSessions_Parms
		{
			int32 MaxResults;
			FString OculusMatchmakingPool;
			UOculusFindSessionsCallbackProxy* ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxResults;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusMatchmakingPool;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::NewProp_MaxResults = { "MaxResults", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OculusFindSessionsCallbackProxy_eventFindMatchmakingSessions_Parms, MaxResults), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::NewProp_OculusMatchmakingPool = { "OculusMatchmakingPool", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OculusFindSessionsCallbackProxy_eventFindMatchmakingSessions_Parms, OculusMatchmakingPool), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OculusFindSessionsCallbackProxy_eventFindMatchmakingSessions_Parms, ReturnValue), Z_Construct_UClass_UOculusFindSessionsCallbackProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::NewProp_MaxResults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::NewProp_OculusMatchmakingPool,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "Oculus|Session" },
		{ "Comment", "// Searches for matchmaking room sessions with the oculus online subsystem\n" },
		{ "ModuleRelativePath", "Classes/OculusFindSessionsCallbackProxy.h" },
		{ "ToolTip", "Searches for matchmaking room sessions with the oculus online subsystem" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusFindSessionsCallbackProxy, nullptr, "FindMatchmakingSessions", nullptr, nullptr, sizeof(OculusFindSessionsCallbackProxy_eventFindMatchmakingSessions_Parms), Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics
	{
		struct OculusFindSessionsCallbackProxy_eventFindModeratedSessions_Parms
		{
			int32 MaxResults;
			UOculusFindSessionsCallbackProxy* ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxResults;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::NewProp_MaxResults = { "MaxResults", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OculusFindSessionsCallbackProxy_eventFindModeratedSessions_Parms, MaxResults), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OculusFindSessionsCallbackProxy_eventFindModeratedSessions_Parms, ReturnValue), Z_Construct_UClass_UOculusFindSessionsCallbackProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::NewProp_MaxResults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "Oculus|Session" },
		{ "Comment", "// Searches for moderated room sessions with the oculus online subsystem\n" },
		{ "ModuleRelativePath", "Classes/OculusFindSessionsCallbackProxy.h" },
		{ "ToolTip", "Searches for moderated room sessions with the oculus online subsystem" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusFindSessionsCallbackProxy, nullptr, "FindModeratedSessions", nullptr, nullptr, sizeof(OculusFindSessionsCallbackProxy_eventFindModeratedSessions_Parms), Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UOculusFindSessionsCallbackProxy_NoRegister()
	{
		return UOculusFindSessionsCallbackProxy::StaticClass();
	}
	struct Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSuccess_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSuccess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnFailure_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnFailure;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UOnlineBlueprintCallProxyBase,
		(UObject* (*)())Z_Construct_UPackage__Script_OnlineSubsystemOculus,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindMatchmakingSessions, "FindMatchmakingSessions" }, // 1332380205
		{ &Z_Construct_UFunction_UOculusFindSessionsCallbackProxy_FindModeratedSessions, "FindModeratedSessions" }, // 3365703428
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Exposes FindSession of the Platform SDK for blueprint use.\n */" },
		{ "IncludePath", "OculusFindSessionsCallbackProxy.h" },
		{ "ModuleRelativePath", "Classes/OculusFindSessionsCallbackProxy.h" },
		{ "ToolTip", "Exposes FindSession of the Platform SDK for blueprint use." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::NewProp_OnSuccess_MetaData[] = {
		{ "Comment", "// Called when there is a successful query\n" },
		{ "ModuleRelativePath", "Classes/OculusFindSessionsCallbackProxy.h" },
		{ "ToolTip", "Called when there is a successful query" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::NewProp_OnSuccess = { "OnSuccess", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusFindSessionsCallbackProxy, OnSuccess), Z_Construct_UDelegateFunction_OnlineSubsystemUtils_BlueprintFindSessionsResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::NewProp_OnSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::NewProp_OnSuccess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::NewProp_OnFailure_MetaData[] = {
		{ "Comment", "// Called when there is an unsuccessful query\n" },
		{ "ModuleRelativePath", "Classes/OculusFindSessionsCallbackProxy.h" },
		{ "ToolTip", "Called when there is an unsuccessful query" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::NewProp_OnFailure = { "OnFailure", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusFindSessionsCallbackProxy, OnFailure), Z_Construct_UDelegateFunction_OnlineSubsystemUtils_BlueprintFindSessionsResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::NewProp_OnFailure_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::NewProp_OnFailure_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::NewProp_OnSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::NewProp_OnFailure,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusFindSessionsCallbackProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::ClassParams = {
		&UOculusFindSessionsCallbackProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::PropPointers),
		0,
		0x008800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusFindSessionsCallbackProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusFindSessionsCallbackProxy, 59553492);
	template<> ONLINESUBSYSTEMOCULUS_API UClass* StaticClass<UOculusFindSessionsCallbackProxy>()
	{
		return UOculusFindSessionsCallbackProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusFindSessionsCallbackProxy(Z_Construct_UClass_UOculusFindSessionsCallbackProxy, &UOculusFindSessionsCallbackProxy::StaticClass, TEXT("/Script/OnlineSubsystemOculus"), TEXT("UOculusFindSessionsCallbackProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusFindSessionsCallbackProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
