// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ONLINESUBSYSTEMOCULUS_OculusNetConnection_generated_h
#error "OculusNetConnection.generated.h already included, missing '#pragma once' in OculusNetConnection.h"
#endif
#define ONLINESUBSYSTEMOCULUS_OculusNetConnection_generated_h

#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_SPARSE_DATA
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusNetConnection(); \
	friend struct Z_Construct_UClass_UOculusNetConnection_Statics; \
public: \
	DECLARE_CLASS(UOculusNetConnection, UIpConnection, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OnlineSubsystemOculus"), NO_API) \
	DECLARE_SERIALIZER(UOculusNetConnection)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUOculusNetConnection(); \
	friend struct Z_Construct_UClass_UOculusNetConnection_Statics; \
public: \
	DECLARE_CLASS(UOculusNetConnection, UIpConnection, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OnlineSubsystemOculus"), NO_API) \
	DECLARE_SERIALIZER(UOculusNetConnection)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusNetConnection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusNetConnection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusNetConnection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusNetConnection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusNetConnection(UOculusNetConnection&&); \
	NO_API UOculusNetConnection(const UOculusNetConnection&); \
public:


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusNetConnection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusNetConnection(UOculusNetConnection&&); \
	NO_API UOculusNetConnection(const UOculusNetConnection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusNetConnection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusNetConnection); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusNetConnection)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_12_PROLOG
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_INCLASS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONLINESUBSYSTEMOCULUS_API UClass* StaticClass<class UOculusNetConnection>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetConnection_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
