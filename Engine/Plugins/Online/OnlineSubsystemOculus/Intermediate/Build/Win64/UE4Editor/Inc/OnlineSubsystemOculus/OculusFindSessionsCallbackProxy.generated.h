// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UOculusFindSessionsCallbackProxy;
#ifdef ONLINESUBSYSTEMOCULUS_OculusFindSessionsCallbackProxy_generated_h
#error "OculusFindSessionsCallbackProxy.generated.h already included, missing '#pragma once' in OculusFindSessionsCallbackProxy.h"
#endif
#define ONLINESUBSYSTEMOCULUS_OculusFindSessionsCallbackProxy_generated_h

#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_SPARSE_DATA
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execFindModeratedSessions); \
	DECLARE_FUNCTION(execFindMatchmakingSessions);


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execFindModeratedSessions); \
	DECLARE_FUNCTION(execFindMatchmakingSessions);


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusFindSessionsCallbackProxy(); \
	friend struct Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics; \
public: \
	DECLARE_CLASS(UOculusFindSessionsCallbackProxy, UOnlineBlueprintCallProxyBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OnlineSubsystemOculus"), ONLINESUBSYSTEMOCULUS_API) \
	DECLARE_SERIALIZER(UOculusFindSessionsCallbackProxy)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUOculusFindSessionsCallbackProxy(); \
	friend struct Z_Construct_UClass_UOculusFindSessionsCallbackProxy_Statics; \
public: \
	DECLARE_CLASS(UOculusFindSessionsCallbackProxy, UOnlineBlueprintCallProxyBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OnlineSubsystemOculus"), ONLINESUBSYSTEMOCULUS_API) \
	DECLARE_SERIALIZER(UOculusFindSessionsCallbackProxy)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ONLINESUBSYSTEMOCULUS_API UOculusFindSessionsCallbackProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusFindSessionsCallbackProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ONLINESUBSYSTEMOCULUS_API, UOculusFindSessionsCallbackProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusFindSessionsCallbackProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ONLINESUBSYSTEMOCULUS_API UOculusFindSessionsCallbackProxy(UOculusFindSessionsCallbackProxy&&); \
	ONLINESUBSYSTEMOCULUS_API UOculusFindSessionsCallbackProxy(const UOculusFindSessionsCallbackProxy&); \
public:


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ONLINESUBSYSTEMOCULUS_API UOculusFindSessionsCallbackProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ONLINESUBSYSTEMOCULUS_API UOculusFindSessionsCallbackProxy(UOculusFindSessionsCallbackProxy&&); \
	ONLINESUBSYSTEMOCULUS_API UOculusFindSessionsCallbackProxy(const UOculusFindSessionsCallbackProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ONLINESUBSYSTEMOCULUS_API, UOculusFindSessionsCallbackProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusFindSessionsCallbackProxy); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusFindSessionsCallbackProxy)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_14_PROLOG
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_INCLASS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OculusFindSessionsCallbackProxy."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONLINESUBSYSTEMOCULUS_API UClass* StaticClass<class UOculusFindSessionsCallbackProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusFindSessionsCallbackProxy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
