// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Classes/OculusIdentityCallbackProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusIdentityCallbackProxy() {}
// Cross Module References
	ONLINESUBSYSTEMOCULUS_API UFunction* Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentityFailureResult__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_OnlineSubsystemOculus();
	ONLINESUBSYSTEMOCULUS_API UFunction* Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature();
	ONLINESUBSYSTEMOCULUS_API UClass* Z_Construct_UClass_UOculusIdentityCallbackProxy_NoRegister();
	ONLINESUBSYSTEMOCULUS_API UClass* Z_Construct_UClass_UOculusIdentityCallbackProxy();
	ENGINE_API UClass* Z_Construct_UClass_UOnlineBlueprintCallProxyBase();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentityFailureResult__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentityFailureResult__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/OculusIdentityCallbackProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentityFailureResult__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_OnlineSubsystemOculus, nullptr, "OculusIdentityFailureResult__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentityFailureResult__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentityFailureResult__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentityFailureResult__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentityFailureResult__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics
	{
		struct _Script_OnlineSubsystemOculus_eventOculusIdentitySuccessResult_Parms
		{
			FString OculusId;
			FString OculusName;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusId;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::NewProp_OculusId = { "OculusId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_OnlineSubsystemOculus_eventOculusIdentitySuccessResult_Parms, OculusId), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::NewProp_OculusName = { "OculusName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_OnlineSubsystemOculus_eventOculusIdentitySuccessResult_Parms, OculusName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::NewProp_OculusId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::NewProp_OculusName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/OculusIdentityCallbackProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_OnlineSubsystemOculus, nullptr, "OculusIdentitySuccessResult__DelegateSignature", nullptr, nullptr, sizeof(_Script_OnlineSubsystemOculus_eventOculusIdentitySuccessResult_Parms), Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UOculusIdentityCallbackProxy::execGetOculusIdentity)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_LocalUserNum);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UOculusIdentityCallbackProxy**)Z_Param__Result=UOculusIdentityCallbackProxy::GetOculusIdentity(Z_Param_LocalUserNum);
		P_NATIVE_END;
	}
	void UOculusIdentityCallbackProxy::StaticRegisterNativesUOculusIdentityCallbackProxy()
	{
		UClass* Class = UOculusIdentityCallbackProxy::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetOculusIdentity", &UOculusIdentityCallbackProxy::execGetOculusIdentity },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics
	{
		struct OculusIdentityCallbackProxy_eventGetOculusIdentity_Parms
		{
			int32 LocalUserNum;
			UOculusIdentityCallbackProxy* ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LocalUserNum;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::NewProp_LocalUserNum = { "LocalUserNum", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OculusIdentityCallbackProxy_eventGetOculusIdentity_Parms, LocalUserNum), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OculusIdentityCallbackProxy_eventGetOculusIdentity_Parms, ReturnValue), Z_Construct_UClass_UOculusIdentityCallbackProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::NewProp_LocalUserNum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "Oculus|Identity" },
		{ "Comment", "// Kick off GetOculusIdentity. Asynchronous-- see OnLoginCompleteDelegate for results.\n" },
		{ "ModuleRelativePath", "Classes/OculusIdentityCallbackProxy.h" },
		{ "ToolTip", "Kick off GetOculusIdentity. Asynchronous-- see OnLoginCompleteDelegate for results." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusIdentityCallbackProxy, nullptr, "GetOculusIdentity", nullptr, nullptr, sizeof(OculusIdentityCallbackProxy_eventGetOculusIdentity_Parms), Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UOculusIdentityCallbackProxy_NoRegister()
	{
		return UOculusIdentityCallbackProxy::StaticClass();
	}
	struct Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSuccess_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSuccess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnFailure_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnFailure;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UOnlineBlueprintCallProxyBase,
		(UObject* (*)())Z_Construct_UPackage__Script_OnlineSubsystemOculus,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOculusIdentityCallbackProxy_GetOculusIdentity, "GetOculusIdentity" }, // 3391720910
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Exposes the oculus id of the Platform SDK for blueprint use.\n */" },
		{ "IncludePath", "OculusIdentityCallbackProxy.h" },
		{ "ModuleRelativePath", "Classes/OculusIdentityCallbackProxy.h" },
		{ "ToolTip", "Exposes the oculus id of the Platform SDK for blueprint use." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::NewProp_OnSuccess_MetaData[] = {
		{ "Comment", "// Called when it successfully gets back the oculus id\n" },
		{ "ModuleRelativePath", "Classes/OculusIdentityCallbackProxy.h" },
		{ "ToolTip", "Called when it successfully gets back the oculus id" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::NewProp_OnSuccess = { "OnSuccess", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusIdentityCallbackProxy, OnSuccess), Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentitySuccessResult__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::NewProp_OnSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::NewProp_OnSuccess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::NewProp_OnFailure_MetaData[] = {
		{ "Comment", "// Called when it fails to get the oculus id\n" },
		{ "ModuleRelativePath", "Classes/OculusIdentityCallbackProxy.h" },
		{ "ToolTip", "Called when it fails to get the oculus id" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::NewProp_OnFailure = { "OnFailure", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusIdentityCallbackProxy, OnFailure), Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusIdentityFailureResult__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::NewProp_OnFailure_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::NewProp_OnFailure_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::NewProp_OnSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::NewProp_OnFailure,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusIdentityCallbackProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::ClassParams = {
		&UOculusIdentityCallbackProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::PropPointers),
		0,
		0x008800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusIdentityCallbackProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusIdentityCallbackProxy, 3716520686);
	template<> ONLINESUBSYSTEMOCULUS_API UClass* StaticClass<UOculusIdentityCallbackProxy>()
	{
		return UOculusIdentityCallbackProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusIdentityCallbackProxy(Z_Construct_UClass_UOculusIdentityCallbackProxy, &UOculusIdentityCallbackProxy::StaticClass, TEXT("/Script/OnlineSubsystemOculus"), TEXT("UOculusIdentityCallbackProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusIdentityCallbackProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
