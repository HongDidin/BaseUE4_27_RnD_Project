// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Classes/OculusUpdateSessionCallbackProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusUpdateSessionCallbackProxy() {}
// Cross Module References
	ONLINESUBSYSTEMOCULUS_API UClass* Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_NoRegister();
	ONLINESUBSYSTEMOCULUS_API UClass* Z_Construct_UClass_UOculusUpdateSessionCallbackProxy();
	ENGINE_API UClass* Z_Construct_UClass_UOnlineBlueprintCallProxyBase();
	UPackage* Z_Construct_UPackage__Script_OnlineSubsystemOculus();
	ENGINE_API UFunction* Z_Construct_UDelegateFunction_Engine_EmptyOnlineDelegate__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UOculusUpdateSessionCallbackProxy::execSetSessionEnqueue)
	{
		P_GET_UBOOL(Z_Param_bShouldEnqueueInMatchmakingPool);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UOculusUpdateSessionCallbackProxy**)Z_Param__Result=UOculusUpdateSessionCallbackProxy::SetSessionEnqueue(Z_Param_bShouldEnqueueInMatchmakingPool);
		P_NATIVE_END;
	}
	void UOculusUpdateSessionCallbackProxy::StaticRegisterNativesUOculusUpdateSessionCallbackProxy()
	{
		UClass* Class = UOculusUpdateSessionCallbackProxy::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetSessionEnqueue", &UOculusUpdateSessionCallbackProxy::execSetSessionEnqueue },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics
	{
		struct OculusUpdateSessionCallbackProxy_eventSetSessionEnqueue_Parms
		{
			bool bShouldEnqueueInMatchmakingPool;
			UOculusUpdateSessionCallbackProxy* ReturnValue;
		};
		static void NewProp_bShouldEnqueueInMatchmakingPool_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldEnqueueInMatchmakingPool;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::NewProp_bShouldEnqueueInMatchmakingPool_SetBit(void* Obj)
	{
		((OculusUpdateSessionCallbackProxy_eventSetSessionEnqueue_Parms*)Obj)->bShouldEnqueueInMatchmakingPool = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::NewProp_bShouldEnqueueInMatchmakingPool = { "bShouldEnqueueInMatchmakingPool", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(OculusUpdateSessionCallbackProxy_eventSetSessionEnqueue_Parms), &Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::NewProp_bShouldEnqueueInMatchmakingPool_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OculusUpdateSessionCallbackProxy_eventSetSessionEnqueue_Parms, ReturnValue), Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::NewProp_bShouldEnqueueInMatchmakingPool,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "Oculus|Session" },
		{ "Comment", "// Kick off UpdateSession check. Asynchronous-- see OnUpdateCompleteDelegate for results.\n" },
		{ "ModuleRelativePath", "Classes/OculusUpdateSessionCallbackProxy.h" },
		{ "ToolTip", "Kick off UpdateSession check. Asynchronous-- see OnUpdateCompleteDelegate for results." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusUpdateSessionCallbackProxy, nullptr, "SetSessionEnqueue", nullptr, nullptr, sizeof(OculusUpdateSessionCallbackProxy_eventSetSessionEnqueue_Parms), Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_NoRegister()
	{
		return UOculusUpdateSessionCallbackProxy::StaticClass();
	}
	struct Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSuccess_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSuccess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnFailure_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnFailure;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UOnlineBlueprintCallProxyBase,
		(UObject* (*)())Z_Construct_UPackage__Script_OnlineSubsystemOculus,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOculusUpdateSessionCallbackProxy_SetSessionEnqueue, "SetSessionEnqueue" }, // 3340115545
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Exposes UpdateSession of the Platform SDK for blueprint use.\n */" },
		{ "IncludePath", "OculusUpdateSessionCallbackProxy.h" },
		{ "ModuleRelativePath", "Classes/OculusUpdateSessionCallbackProxy.h" },
		{ "ToolTip", "Exposes UpdateSession of the Platform SDK for blueprint use." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::NewProp_OnSuccess_MetaData[] = {
		{ "Comment", "// Called when the session was updated successfully\n" },
		{ "ModuleRelativePath", "Classes/OculusUpdateSessionCallbackProxy.h" },
		{ "ToolTip", "Called when the session was updated successfully" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::NewProp_OnSuccess = { "OnSuccess", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusUpdateSessionCallbackProxy, OnSuccess), Z_Construct_UDelegateFunction_Engine_EmptyOnlineDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::NewProp_OnSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::NewProp_OnSuccess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::NewProp_OnFailure_MetaData[] = {
		{ "Comment", "// Called when there was an error updating the session\n" },
		{ "ModuleRelativePath", "Classes/OculusUpdateSessionCallbackProxy.h" },
		{ "ToolTip", "Called when there was an error updating the session" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::NewProp_OnFailure = { "OnFailure", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusUpdateSessionCallbackProxy, OnFailure), Z_Construct_UDelegateFunction_Engine_EmptyOnlineDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::NewProp_OnFailure_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::NewProp_OnFailure_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::NewProp_OnSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::NewProp_OnFailure,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusUpdateSessionCallbackProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::ClassParams = {
		&UOculusUpdateSessionCallbackProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::PropPointers),
		0,
		0x008800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusUpdateSessionCallbackProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusUpdateSessionCallbackProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusUpdateSessionCallbackProxy, 757935922);
	template<> ONLINESUBSYSTEMOCULUS_API UClass* StaticClass<UOculusUpdateSessionCallbackProxy>()
	{
		return UOculusUpdateSessionCallbackProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusUpdateSessionCallbackProxy(Z_Construct_UClass_UOculusUpdateSessionCallbackProxy, &UOculusUpdateSessionCallbackProxy::StaticClass, TEXT("/Script/OnlineSubsystemOculus"), TEXT("UOculusUpdateSessionCallbackProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusUpdateSessionCallbackProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
