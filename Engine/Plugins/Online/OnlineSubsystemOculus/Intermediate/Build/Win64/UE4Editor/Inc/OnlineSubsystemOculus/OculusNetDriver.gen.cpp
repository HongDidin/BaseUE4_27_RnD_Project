// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Classes/OculusNetDriver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusNetDriver() {}
// Cross Module References
	ONLINESUBSYSTEMOCULUS_API UClass* Z_Construct_UClass_UOculusNetDriver_NoRegister();
	ONLINESUBSYSTEMOCULUS_API UClass* Z_Construct_UClass_UOculusNetDriver();
	ONLINESUBSYSTEMUTILS_API UClass* Z_Construct_UClass_UIpNetDriver();
	UPackage* Z_Construct_UPackage__Script_OnlineSubsystemOculus();
// End Cross Module References
	void UOculusNetDriver::StaticRegisterNativesUOculusNetDriver()
	{
	}
	UClass* Z_Construct_UClass_UOculusNetDriver_NoRegister()
	{
		return UOculusNetDriver::StaticClass();
	}
	struct Z_Construct_UClass_UOculusNetDriver_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusNetDriver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UIpNetDriver,
		(UObject* (*)())Z_Construct_UPackage__Script_OnlineSubsystemOculus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusNetDriver_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "OculusNetDriver.h" },
		{ "ModuleRelativePath", "Classes/OculusNetDriver.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusNetDriver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusNetDriver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusNetDriver_Statics::ClassParams = {
		&UOculusNetDriver::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UOculusNetDriver_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusNetDriver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusNetDriver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusNetDriver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusNetDriver, 1863044273);
	template<> ONLINESUBSYSTEMOCULUS_API UClass* StaticClass<UOculusNetDriver>()
	{
		return UOculusNetDriver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusNetDriver(Z_Construct_UClass_UOculusNetDriver, &UOculusNetDriver::StaticClass, TEXT("/Script/OnlineSubsystemOculus"), TEXT("UOculusNetDriver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusNetDriver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
