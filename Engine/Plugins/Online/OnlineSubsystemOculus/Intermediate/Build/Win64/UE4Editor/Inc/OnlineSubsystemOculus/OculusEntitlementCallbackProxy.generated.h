// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UOculusEntitlementCallbackProxy;
#ifdef ONLINESUBSYSTEMOCULUS_OculusEntitlementCallbackProxy_generated_h
#error "OculusEntitlementCallbackProxy.generated.h already included, missing '#pragma once' in OculusEntitlementCallbackProxy.h"
#endif
#define ONLINESUBSYSTEMOCULUS_OculusEntitlementCallbackProxy_generated_h

#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_10_DELEGATE \
static inline void FOculusEntitlementCheckResult_DelegateWrapper(const FMulticastScriptDelegate& OculusEntitlementCheckResult) \
{ \
	OculusEntitlementCheckResult.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_SPARSE_DATA
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execVerifyEntitlement);


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execVerifyEntitlement);


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusEntitlementCallbackProxy(); \
	friend struct Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics; \
public: \
	DECLARE_CLASS(UOculusEntitlementCallbackProxy, UOnlineBlueprintCallProxyBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OnlineSubsystemOculus"), ONLINESUBSYSTEMOCULUS_API) \
	DECLARE_SERIALIZER(UOculusEntitlementCallbackProxy)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUOculusEntitlementCallbackProxy(); \
	friend struct Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics; \
public: \
	DECLARE_CLASS(UOculusEntitlementCallbackProxy, UOnlineBlueprintCallProxyBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OnlineSubsystemOculus"), ONLINESUBSYSTEMOCULUS_API) \
	DECLARE_SERIALIZER(UOculusEntitlementCallbackProxy)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ONLINESUBSYSTEMOCULUS_API UOculusEntitlementCallbackProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusEntitlementCallbackProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ONLINESUBSYSTEMOCULUS_API, UOculusEntitlementCallbackProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusEntitlementCallbackProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ONLINESUBSYSTEMOCULUS_API UOculusEntitlementCallbackProxy(UOculusEntitlementCallbackProxy&&); \
	ONLINESUBSYSTEMOCULUS_API UOculusEntitlementCallbackProxy(const UOculusEntitlementCallbackProxy&); \
public:


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ONLINESUBSYSTEMOCULUS_API UOculusEntitlementCallbackProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ONLINESUBSYSTEMOCULUS_API UOculusEntitlementCallbackProxy(UOculusEntitlementCallbackProxy&&); \
	ONLINESUBSYSTEMOCULUS_API UOculusEntitlementCallbackProxy(const UOculusEntitlementCallbackProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ONLINESUBSYSTEMOCULUS_API, UOculusEntitlementCallbackProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusEntitlementCallbackProxy); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusEntitlementCallbackProxy)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_15_PROLOG
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_INCLASS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OculusEntitlementCallbackProxy."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONLINESUBSYSTEMOCULUS_API UClass* StaticClass<class UOculusEntitlementCallbackProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusEntitlementCallbackProxy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
