// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ONLINESUBSYSTEMOCULUS_OculusNetDriver_generated_h
#error "OculusNetDriver.generated.h already included, missing '#pragma once' in OculusNetDriver.h"
#endif
#define ONLINESUBSYSTEMOCULUS_OculusNetDriver_generated_h

#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_SPARSE_DATA
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusNetDriver(); \
	friend struct Z_Construct_UClass_UOculusNetDriver_Statics; \
public: \
	DECLARE_CLASS(UOculusNetDriver, UIpNetDriver, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OnlineSubsystemOculus"), NO_API) \
	DECLARE_SERIALIZER(UOculusNetDriver)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUOculusNetDriver(); \
	friend struct Z_Construct_UClass_UOculusNetDriver_Statics; \
public: \
	DECLARE_CLASS(UOculusNetDriver, UIpNetDriver, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OnlineSubsystemOculus"), NO_API) \
	DECLARE_SERIALIZER(UOculusNetDriver)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusNetDriver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusNetDriver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusNetDriver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusNetDriver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusNetDriver(UOculusNetDriver&&); \
	NO_API UOculusNetDriver(const UOculusNetDriver&); \
public:


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusNetDriver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusNetDriver(UOculusNetDriver&&); \
	NO_API UOculusNetDriver(const UOculusNetDriver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusNetDriver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusNetDriver); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusNetDriver)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_14_PROLOG
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_INCLASS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONLINESUBSYSTEMOCULUS_API UClass* StaticClass<class UOculusNetDriver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusNetDriver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
