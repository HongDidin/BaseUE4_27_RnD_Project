// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UOculusIdentityCallbackProxy;
#ifdef ONLINESUBSYSTEMOCULUS_OculusIdentityCallbackProxy_generated_h
#error "OculusIdentityCallbackProxy.generated.h already included, missing '#pragma once' in OculusIdentityCallbackProxy.h"
#endif
#define ONLINESUBSYSTEMOCULUS_OculusIdentityCallbackProxy_generated_h

#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_11_DELEGATE \
static inline void FOculusIdentityFailureResult_DelegateWrapper(const FMulticastScriptDelegate& OculusIdentityFailureResult) \
{ \
	OculusIdentityFailureResult.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_10_DELEGATE \
struct _Script_OnlineSubsystemOculus_eventOculusIdentitySuccessResult_Parms \
{ \
	FString OculusId; \
	FString OculusName; \
}; \
static inline void FOculusIdentitySuccessResult_DelegateWrapper(const FMulticastScriptDelegate& OculusIdentitySuccessResult, const FString& OculusId, const FString& OculusName) \
{ \
	_Script_OnlineSubsystemOculus_eventOculusIdentitySuccessResult_Parms Parms; \
	Parms.OculusId=OculusId; \
	Parms.OculusName=OculusName; \
	OculusIdentitySuccessResult.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_SPARSE_DATA
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetOculusIdentity);


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetOculusIdentity);


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusIdentityCallbackProxy(); \
	friend struct Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics; \
public: \
	DECLARE_CLASS(UOculusIdentityCallbackProxy, UOnlineBlueprintCallProxyBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OnlineSubsystemOculus"), ONLINESUBSYSTEMOCULUS_API) \
	DECLARE_SERIALIZER(UOculusIdentityCallbackProxy)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUOculusIdentityCallbackProxy(); \
	friend struct Z_Construct_UClass_UOculusIdentityCallbackProxy_Statics; \
public: \
	DECLARE_CLASS(UOculusIdentityCallbackProxy, UOnlineBlueprintCallProxyBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OnlineSubsystemOculus"), ONLINESUBSYSTEMOCULUS_API) \
	DECLARE_SERIALIZER(UOculusIdentityCallbackProxy)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ONLINESUBSYSTEMOCULUS_API UOculusIdentityCallbackProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusIdentityCallbackProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ONLINESUBSYSTEMOCULUS_API, UOculusIdentityCallbackProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusIdentityCallbackProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ONLINESUBSYSTEMOCULUS_API UOculusIdentityCallbackProxy(UOculusIdentityCallbackProxy&&); \
	ONLINESUBSYSTEMOCULUS_API UOculusIdentityCallbackProxy(const UOculusIdentityCallbackProxy&); \
public:


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ONLINESUBSYSTEMOCULUS_API UOculusIdentityCallbackProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ONLINESUBSYSTEMOCULUS_API UOculusIdentityCallbackProxy(UOculusIdentityCallbackProxy&&); \
	ONLINESUBSYSTEMOCULUS_API UOculusIdentityCallbackProxy(const UOculusIdentityCallbackProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ONLINESUBSYSTEMOCULUS_API, UOculusIdentityCallbackProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusIdentityCallbackProxy); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusIdentityCallbackProxy)


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_16_PROLOG
#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_INCLASS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h_19_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OculusIdentityCallbackProxy."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONLINESUBSYSTEMOCULUS_API UClass* StaticClass<class UOculusIdentityCallbackProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineSubsystemOculus_Source_Classes_OculusIdentityCallbackProxy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
