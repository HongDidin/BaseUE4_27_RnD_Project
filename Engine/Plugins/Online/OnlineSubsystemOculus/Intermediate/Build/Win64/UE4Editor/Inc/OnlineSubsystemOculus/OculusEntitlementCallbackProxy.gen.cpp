// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Classes/OculusEntitlementCallbackProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusEntitlementCallbackProxy() {}
// Cross Module References
	ONLINESUBSYSTEMOCULUS_API UFunction* Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusEntitlementCheckResult__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_OnlineSubsystemOculus();
	ONLINESUBSYSTEMOCULUS_API UClass* Z_Construct_UClass_UOculusEntitlementCallbackProxy_NoRegister();
	ONLINESUBSYSTEMOCULUS_API UClass* Z_Construct_UClass_UOculusEntitlementCallbackProxy();
	ENGINE_API UClass* Z_Construct_UClass_UOnlineBlueprintCallProxyBase();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusEntitlementCheckResult__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusEntitlementCheckResult__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/OculusEntitlementCallbackProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusEntitlementCheckResult__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_OnlineSubsystemOculus, nullptr, "OculusEntitlementCheckResult__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusEntitlementCheckResult__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusEntitlementCheckResult__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusEntitlementCheckResult__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusEntitlementCheckResult__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UOculusEntitlementCallbackProxy::execVerifyEntitlement)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UOculusEntitlementCallbackProxy**)Z_Param__Result=UOculusEntitlementCallbackProxy::VerifyEntitlement();
		P_NATIVE_END;
	}
	void UOculusEntitlementCallbackProxy::StaticRegisterNativesUOculusEntitlementCallbackProxy()
	{
		UClass* Class = UOculusEntitlementCallbackProxy::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "VerifyEntitlement", &UOculusEntitlementCallbackProxy::execVerifyEntitlement },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics
	{
		struct OculusEntitlementCallbackProxy_eventVerifyEntitlement_Parms
		{
			UOculusEntitlementCallbackProxy* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OculusEntitlementCallbackProxy_eventVerifyEntitlement_Parms, ReturnValue), Z_Construct_UClass_UOculusEntitlementCallbackProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "Oculus|Entitlement" },
		{ "Comment", "// Kick off entitlement check. Asynchronous-- see OnUserPrivilegeCompleteDelegate for results.\n" },
		{ "ModuleRelativePath", "Classes/OculusEntitlementCallbackProxy.h" },
		{ "ToolTip", "Kick off entitlement check. Asynchronous-- see OnUserPrivilegeCompleteDelegate for results." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusEntitlementCallbackProxy, nullptr, "VerifyEntitlement", nullptr, nullptr, sizeof(OculusEntitlementCallbackProxy_eventVerifyEntitlement_Parms), Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UOculusEntitlementCallbackProxy_NoRegister()
	{
		return UOculusEntitlementCallbackProxy::StaticClass();
	}
	struct Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSuccess_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSuccess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnFailure_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnFailure;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UOnlineBlueprintCallProxyBase,
		(UObject* (*)())Z_Construct_UPackage__Script_OnlineSubsystemOculus,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOculusEntitlementCallbackProxy_VerifyEntitlement, "VerifyEntitlement" }, // 3049622707
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Exposes some of the Platform SDK for blueprint use.\n */" },
		{ "IncludePath", "OculusEntitlementCallbackProxy.h" },
		{ "ModuleRelativePath", "Classes/OculusEntitlementCallbackProxy.h" },
		{ "ToolTip", "Exposes some of the Platform SDK for blueprint use." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::NewProp_OnSuccess_MetaData[] = {
		{ "Comment", "// Called when there is a successful entitlement check\n" },
		{ "ModuleRelativePath", "Classes/OculusEntitlementCallbackProxy.h" },
		{ "ToolTip", "Called when there is a successful entitlement check" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::NewProp_OnSuccess = { "OnSuccess", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusEntitlementCallbackProxy, OnSuccess), Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusEntitlementCheckResult__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::NewProp_OnSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::NewProp_OnSuccess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::NewProp_OnFailure_MetaData[] = {
		{ "Comment", "// Called when there is an unsuccessful entitlement check\n" },
		{ "ModuleRelativePath", "Classes/OculusEntitlementCallbackProxy.h" },
		{ "ToolTip", "Called when there is an unsuccessful entitlement check" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::NewProp_OnFailure = { "OnFailure", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusEntitlementCallbackProxy, OnFailure), Z_Construct_UDelegateFunction_OnlineSubsystemOculus_OculusEntitlementCheckResult__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::NewProp_OnFailure_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::NewProp_OnFailure_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::NewProp_OnSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::NewProp_OnFailure,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusEntitlementCallbackProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::ClassParams = {
		&UOculusEntitlementCallbackProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::PropPointers),
		0,
		0x008800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusEntitlementCallbackProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusEntitlementCallbackProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusEntitlementCallbackProxy, 4032953321);
	template<> ONLINESUBSYSTEMOCULUS_API UClass* StaticClass<UOculusEntitlementCallbackProxy>()
	{
		return UOculusEntitlementCallbackProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusEntitlementCallbackProxy(Z_Construct_UClass_UOculusEntitlementCallbackProxy, &UOculusEntitlementCallbackProxy::StaticClass, TEXT("/Script/OnlineSubsystemOculus"), TEXT("UOculusEntitlementCallbackProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusEntitlementCallbackProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
