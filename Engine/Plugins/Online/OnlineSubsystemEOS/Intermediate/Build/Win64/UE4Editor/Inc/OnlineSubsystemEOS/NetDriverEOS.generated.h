// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ONLINESUBSYSTEMEOS_NetDriverEOS_generated_h
#error "NetDriverEOS.generated.h already included, missing '#pragma once' in NetDriverEOS.h"
#endif
#define ONLINESUBSYSTEMEOS_NetDriverEOS_generated_h

#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_SPARSE_DATA
#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNetDriverEOS(); \
	friend struct Z_Construct_UClass_UNetDriverEOS_Statics; \
public: \
	DECLARE_CLASS(UNetDriverEOS, UIpNetDriver, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OnlineSubsystemEOS"), NO_API) \
	DECLARE_SERIALIZER(UNetDriverEOS)


#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUNetDriverEOS(); \
	friend struct Z_Construct_UClass_UNetDriverEOS_Statics; \
public: \
	DECLARE_CLASS(UNetDriverEOS, UIpNetDriver, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OnlineSubsystemEOS"), NO_API) \
	DECLARE_SERIALIZER(UNetDriverEOS)


#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetDriverEOS(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetDriverEOS) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetDriverEOS); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetDriverEOS); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetDriverEOS(UNetDriverEOS&&); \
	NO_API UNetDriverEOS(const UNetDriverEOS&); \
public:


#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetDriverEOS(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetDriverEOS(UNetDriverEOS&&); \
	NO_API UNetDriverEOS(const UNetDriverEOS&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetDriverEOS); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetDriverEOS); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetDriverEOS)


#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_11_PROLOG
#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_INCLASS \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONLINESUBSYSTEMEOS_API UClass* StaticClass<class UNetDriverEOS>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetDriverEOS_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
