// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OnlineSubsystemEOS/Private/NetDriverEOS.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetDriverEOS() {}
// Cross Module References
	ONLINESUBSYSTEMEOS_API UClass* Z_Construct_UClass_UNetDriverEOS_NoRegister();
	ONLINESUBSYSTEMEOS_API UClass* Z_Construct_UClass_UNetDriverEOS();
	ONLINESUBSYSTEMUTILS_API UClass* Z_Construct_UClass_UIpNetDriver();
	UPackage* Z_Construct_UPackage__Script_OnlineSubsystemEOS();
// End Cross Module References
	void UNetDriverEOS::StaticRegisterNativesUNetDriverEOS()
	{
	}
	UClass* Z_Construct_UClass_UNetDriverEOS_NoRegister()
	{
		return UNetDriverEOS::StaticClass();
	}
	struct Z_Construct_UClass_UNetDriverEOS_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsPassthrough_MetaData[];
#endif
		static void NewProp_bIsPassthrough_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsPassthrough;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsUsingP2PSockets_MetaData[];
#endif
		static void NewProp_bIsUsingP2PSockets_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsUsingP2PSockets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNetDriverEOS_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UIpNetDriver,
		(UObject* (*)())Z_Construct_UPackage__Script_OnlineSubsystemEOS,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetDriverEOS_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NetDriverEOS.h" },
		{ "ModuleRelativePath", "Private/NetDriverEOS.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsPassthrough_MetaData[] = {
		{ "ModuleRelativePath", "Private/NetDriverEOS.h" },
	};
#endif
	void Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsPassthrough_SetBit(void* Obj)
	{
		((UNetDriverEOS*)Obj)->bIsPassthrough = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsPassthrough = { "bIsPassthrough", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNetDriverEOS), &Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsPassthrough_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsPassthrough_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsPassthrough_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsUsingP2PSockets_MetaData[] = {
		{ "ModuleRelativePath", "Private/NetDriverEOS.h" },
	};
#endif
	void Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsUsingP2PSockets_SetBit(void* Obj)
	{
		((UNetDriverEOS*)Obj)->bIsUsingP2PSockets = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsUsingP2PSockets = { "bIsUsingP2PSockets", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNetDriverEOS), &Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsUsingP2PSockets_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsUsingP2PSockets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsUsingP2PSockets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNetDriverEOS_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsPassthrough,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetDriverEOS_Statics::NewProp_bIsUsingP2PSockets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNetDriverEOS_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNetDriverEOS>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNetDriverEOS_Statics::ClassParams = {
		&UNetDriverEOS::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNetDriverEOS_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNetDriverEOS_Statics::PropPointers),
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UNetDriverEOS_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNetDriverEOS_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNetDriverEOS()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNetDriverEOS_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNetDriverEOS, 3951918874);
	template<> ONLINESUBSYSTEMEOS_API UClass* StaticClass<UNetDriverEOS>()
	{
		return UNetDriverEOS::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNetDriverEOS(Z_Construct_UClass_UNetDriverEOS, &UNetDriverEOS::StaticClass, TEXT("/Script/OnlineSubsystemEOS"), TEXT("UNetDriverEOS"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNetDriverEOS);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
