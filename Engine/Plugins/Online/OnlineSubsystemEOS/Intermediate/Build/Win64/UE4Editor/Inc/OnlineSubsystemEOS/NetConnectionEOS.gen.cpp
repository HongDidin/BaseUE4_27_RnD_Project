// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OnlineSubsystemEOS/Private/NetConnectionEOS.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetConnectionEOS() {}
// Cross Module References
	ONLINESUBSYSTEMEOS_API UClass* Z_Construct_UClass_UNetConnectionEOS_NoRegister();
	ONLINESUBSYSTEMEOS_API UClass* Z_Construct_UClass_UNetConnectionEOS();
	ONLINESUBSYSTEMUTILS_API UClass* Z_Construct_UClass_UIpConnection();
	UPackage* Z_Construct_UPackage__Script_OnlineSubsystemEOS();
// End Cross Module References
	void UNetConnectionEOS::StaticRegisterNativesUNetConnectionEOS()
	{
	}
	UClass* Z_Construct_UClass_UNetConnectionEOS_NoRegister()
	{
		return UNetConnectionEOS::StaticClass();
	}
	struct Z_Construct_UClass_UNetConnectionEOS_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNetConnectionEOS_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UIpConnection,
		(UObject* (*)())Z_Construct_UPackage__Script_OnlineSubsystemEOS,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetConnectionEOS_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NetConnectionEOS.h" },
		{ "ModuleRelativePath", "Private/NetConnectionEOS.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNetConnectionEOS_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNetConnectionEOS>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNetConnectionEOS_Statics::ClassParams = {
		&UNetConnectionEOS::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UNetConnectionEOS_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNetConnectionEOS_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNetConnectionEOS()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNetConnectionEOS_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNetConnectionEOS, 536359401);
	template<> ONLINESUBSYSTEMEOS_API UClass* StaticClass<UNetConnectionEOS>()
	{
		return UNetConnectionEOS::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNetConnectionEOS(Z_Construct_UClass_UNetConnectionEOS, &UNetConnectionEOS::StaticClass, TEXT("/Script/OnlineSubsystemEOS"), TEXT("UNetConnectionEOS"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNetConnectionEOS);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
