// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ONLINESUBSYSTEMEOS_NetConnectionEOS_generated_h
#error "NetConnectionEOS.generated.h already included, missing '#pragma once' in NetConnectionEOS.h"
#endif
#define ONLINESUBSYSTEMEOS_NetConnectionEOS_generated_h

#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_SPARSE_DATA
#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNetConnectionEOS(); \
	friend struct Z_Construct_UClass_UNetConnectionEOS_Statics; \
public: \
	DECLARE_CLASS(UNetConnectionEOS, UIpConnection, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OnlineSubsystemEOS"), NO_API) \
	DECLARE_SERIALIZER(UNetConnectionEOS)


#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUNetConnectionEOS(); \
	friend struct Z_Construct_UClass_UNetConnectionEOS_Statics; \
public: \
	DECLARE_CLASS(UNetConnectionEOS, UIpConnection, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OnlineSubsystemEOS"), NO_API) \
	DECLARE_SERIALIZER(UNetConnectionEOS)


#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetConnectionEOS(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetConnectionEOS) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetConnectionEOS); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetConnectionEOS); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetConnectionEOS(UNetConnectionEOS&&); \
	NO_API UNetConnectionEOS(const UNetConnectionEOS&); \
public:


#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetConnectionEOS(UNetConnectionEOS&&); \
	NO_API UNetConnectionEOS(const UNetConnectionEOS&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetConnectionEOS); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetConnectionEOS); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetConnectionEOS)


#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_9_PROLOG
#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_INCLASS \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_SPARSE_DATA \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONLINESUBSYSTEMEOS_API UClass* StaticClass<class UNetConnectionEOS>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineSubsystemEOS_Source_OnlineSubsystemEOS_Private_NetConnectionEOS_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
