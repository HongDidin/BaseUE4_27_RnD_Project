// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Qos/Public/QosBeaconHost.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeQosBeaconHost() {}
// Cross Module References
	QOS_API UClass* Z_Construct_UClass_AQosBeaconHost_NoRegister();
	QOS_API UClass* Z_Construct_UClass_AQosBeaconHost();
	ONLINESUBSYSTEMUTILS_API UClass* Z_Construct_UClass_AOnlineBeaconHostObject();
	UPackage* Z_Construct_UPackage__Script_Qos();
// End Cross Module References
	void AQosBeaconHost::StaticRegisterNativesAQosBeaconHost()
	{
	}
	UClass* Z_Construct_UClass_AQosBeaconHost_NoRegister()
	{
		return AQosBeaconHost::StaticClass();
	}
	struct Z_Construct_UClass_AQosBeaconHost_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AQosBeaconHost_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AOnlineBeaconHostObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Qos,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AQosBeaconHost_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A beacon host listening for Qos requests from a potential client\n */" },
		{ "IncludePath", "QosBeaconHost.h" },
		{ "ModuleRelativePath", "Public/QosBeaconHost.h" },
		{ "ToolTip", "A beacon host listening for Qos requests from a potential client" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AQosBeaconHost_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AQosBeaconHost>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AQosBeaconHost_Statics::ClassParams = {
		&AQosBeaconHost::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AQosBeaconHost_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AQosBeaconHost_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AQosBeaconHost()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AQosBeaconHost_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AQosBeaconHost, 903144121);
	template<> QOS_API UClass* StaticClass<AQosBeaconHost>()
	{
		return AQosBeaconHost::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AQosBeaconHost(Z_Construct_UClass_AQosBeaconHost, &AQosBeaconHost::StaticClass, TEXT("/Script/Qos"), TEXT("AQosBeaconHost"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AQosBeaconHost);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
