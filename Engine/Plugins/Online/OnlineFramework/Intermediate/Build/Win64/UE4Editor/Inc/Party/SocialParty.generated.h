// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_SocialParty_generated_h
#error "SocialParty.generated.h already included, missing '#pragma once' in SocialParty.h"
#endif
#define PARTY_SocialParty_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPartyRepData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__PrivacySettings() { return STRUCT_OFFSET(FPartyRepData, PrivacySettings); } \
	FORCEINLINE static uint32 __PPO__PlatformSessions() { return STRUCT_OFFSET(FPartyRepData, PlatformSessions); } \
	typedef FOnlinePartyRepDataBase Super;


template<> PARTY_API UScriptStruct* StaticStruct<struct FPartyRepData>();

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSocialParty(); \
	friend struct Z_Construct_UClass_USocialParty_Statics; \
public: \
	DECLARE_CLASS(USocialParty, UObject, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialParty) \
	DECLARE_WITHIN(USocialManager) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_INCLASS \
private: \
	static void StaticRegisterNativesUSocialParty(); \
	friend struct Z_Construct_UClass_USocialParty_Statics; \
public: \
	DECLARE_CLASS(USocialParty, UObject, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialParty) \
	DECLARE_WITHIN(USocialManager) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialParty(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialParty) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialParty); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialParty); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialParty(USocialParty&&); \
	NO_API USocialParty(const USocialParty&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialParty(USocialParty&&); \
	NO_API USocialParty(const USocialParty&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialParty); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialParty); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(USocialParty)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ReservationBeaconClientClass() { return STRUCT_OFFSET(USocialParty, ReservationBeaconClientClass); } \
	FORCEINLINE static uint32 __PPO__SpectatorBeaconClientClass() { return STRUCT_OFFSET(USocialParty, SpectatorBeaconClientClass); } \
	FORCEINLINE static uint32 __PPO__OwningLocalUserId() { return STRUCT_OFFSET(USocialParty, OwningLocalUserId); } \
	FORCEINLINE static uint32 __PPO__CurrentLeaderId() { return STRUCT_OFFSET(USocialParty, CurrentLeaderId); } \
	FORCEINLINE static uint32 __PPO__PartyMembersById() { return STRUCT_OFFSET(USocialParty, PartyMembersById); } \
	FORCEINLINE static uint32 __PPO__bEnableAutomaticPartyRejoin() { return STRUCT_OFFSET(USocialParty, bEnableAutomaticPartyRejoin); } \
	FORCEINLINE static uint32 __PPO__ReservationBeaconClient() { return STRUCT_OFFSET(USocialParty, ReservationBeaconClient); } \
	FORCEINLINE static uint32 __PPO__SpectatorBeaconClient() { return STRUCT_OFFSET(USocialParty, SpectatorBeaconClient); }


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_73_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h_76_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class USocialParty>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_SocialParty_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
