// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EQosResponseType : uint8;
#ifdef QOS_QosBeaconClient_generated_h
#error "QosBeaconClient.generated.h already included, missing '#pragma once' in QosBeaconClient.h"
#endif
#define QOS_QosBeaconClient_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_RPC_WRAPPERS \
	virtual void ClientQosResponse_Implementation(EQosResponseType Response); \
	virtual bool ServerQosRequest_Validate(const FString& ); \
	virtual void ServerQosRequest_Implementation(const FString& InSessionId); \
 \
	DECLARE_FUNCTION(execClientQosResponse); \
	DECLARE_FUNCTION(execServerQosRequest);


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void ClientQosResponse_Implementation(EQosResponseType Response); \
	virtual bool ServerQosRequest_Validate(const FString& ); \
	virtual void ServerQosRequest_Implementation(const FString& InSessionId); \
 \
	DECLARE_FUNCTION(execClientQosResponse); \
	DECLARE_FUNCTION(execServerQosRequest);


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_EVENT_PARMS \
	struct QosBeaconClient_eventClientQosResponse_Parms \
	{ \
		EQosResponseType Response; \
	}; \
	struct QosBeaconClient_eventServerQosRequest_Parms \
	{ \
		FString InSessionId; \
	};


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_CALLBACK_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAQosBeaconClient(); \
	friend struct Z_Construct_UClass_AQosBeaconClient_Statics; \
public: \
	DECLARE_CLASS(AQosBeaconClient, AOnlineBeaconClient, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Qos"), NO_API) \
	DECLARE_SERIALIZER(AQosBeaconClient)


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_INCLASS \
private: \
	static void StaticRegisterNativesAQosBeaconClient(); \
	friend struct Z_Construct_UClass_AQosBeaconClient_Statics; \
public: \
	DECLARE_CLASS(AQosBeaconClient, AOnlineBeaconClient, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Qos"), NO_API) \
	DECLARE_SERIALIZER(AQosBeaconClient)


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AQosBeaconClient(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AQosBeaconClient) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AQosBeaconClient); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AQosBeaconClient); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AQosBeaconClient(AQosBeaconClient&&); \
	NO_API AQosBeaconClient(const AQosBeaconClient&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AQosBeaconClient(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AQosBeaconClient(AQosBeaconClient&&); \
	NO_API AQosBeaconClient(const AQosBeaconClient&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AQosBeaconClient); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AQosBeaconClient); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AQosBeaconClient)


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_37_PROLOG \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_EVENT_PARMS


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_CALLBACK_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_CALLBACK_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h_40_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class QosBeaconClient."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> QOS_API UClass* StaticClass<class AQosBeaconClient>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosBeaconClient_h


#define FOREACH_ENUM_EQOSRESPONSETYPE(op) \
	op(EQosResponseType::NoResponse) \
	op(EQosResponseType::Success) \
	op(EQosResponseType::Failure) 

enum class EQosResponseType : uint8;
template<> QOS_API UEnum* StaticEnum<EQosResponseType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
