// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Lobby/Public/LobbyBeaconHost.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLobbyBeaconHost() {}
// Cross Module References
	LOBBY_API UClass* Z_Construct_UClass_ALobbyBeaconHost_NoRegister();
	LOBBY_API UClass* Z_Construct_UClass_ALobbyBeaconHost();
	ONLINESUBSYSTEMUTILS_API UClass* Z_Construct_UClass_AOnlineBeaconHostObject();
	UPackage* Z_Construct_UPackage__Script_Lobby();
	LOBBY_API UClass* Z_Construct_UClass_ALobbyBeaconState_NoRegister();
// End Cross Module References
	void ALobbyBeaconHost::StaticRegisterNativesALobbyBeaconHost()
	{
	}
	UClass* Z_Construct_UClass_ALobbyBeaconHost_NoRegister()
	{
		return ALobbyBeaconHost::StaticClass();
	}
	struct Z_Construct_UClass_ALobbyBeaconHost_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LobbyStateClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftClassPropertyParams NewProp_LobbyStateClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LobbyState_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LobbyState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALobbyBeaconHost_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AOnlineBeaconHostObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Lobby,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALobbyBeaconHost_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Host object for maintaining a lobby before players actually join a server ready to receive them\n */" },
		{ "IncludePath", "LobbyBeaconHost.h" },
		{ "ModuleRelativePath", "Public/LobbyBeaconHost.h" },
		{ "ToolTip", "Host object for maintaining a lobby before players actually join a server ready to receive them" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALobbyBeaconHost_Statics::NewProp_LobbyStateClass_MetaData[] = {
		{ "Comment", "/** Class to use for the lobby beacon state */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconHost.h" },
		{ "ToolTip", "Class to use for the lobby beacon state" },
	};
#endif
	const UE4CodeGen_Private::FSoftClassPropertyParams Z_Construct_UClass_ALobbyBeaconHost_Statics::NewProp_LobbyStateClass = { "LobbyStateClass", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftClass, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALobbyBeaconHost, LobbyStateClass), Z_Construct_UClass_ALobbyBeaconState_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ALobbyBeaconHost_Statics::NewProp_LobbyStateClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconHost_Statics::NewProp_LobbyStateClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALobbyBeaconHost_Statics::NewProp_LobbyState_MetaData[] = {
		{ "Comment", "/** Actor representing the state of the lobby (similar to AGameState) */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconHost.h" },
		{ "ToolTip", "Actor representing the state of the lobby (similar to AGameState)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ALobbyBeaconHost_Statics::NewProp_LobbyState = { "LobbyState", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALobbyBeaconHost, LobbyState), Z_Construct_UClass_ALobbyBeaconState_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ALobbyBeaconHost_Statics::NewProp_LobbyState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconHost_Statics::NewProp_LobbyState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ALobbyBeaconHost_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALobbyBeaconHost_Statics::NewProp_LobbyStateClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALobbyBeaconHost_Statics::NewProp_LobbyState,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALobbyBeaconHost_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALobbyBeaconHost>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALobbyBeaconHost_Statics::ClassParams = {
		&ALobbyBeaconHost::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ALobbyBeaconHost_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconHost_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ALobbyBeaconHost_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconHost_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALobbyBeaconHost()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALobbyBeaconHost_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALobbyBeaconHost, 3907271247);
	template<> LOBBY_API UClass* StaticClass<ALobbyBeaconHost>()
	{
		return ALobbyBeaconHost::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALobbyBeaconHost(Z_Construct_UClass_ALobbyBeaconHost, &ALobbyBeaconHost::StaticClass, TEXT("/Script/Lobby"), TEXT("ALobbyBeaconHost"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALobbyBeaconHost);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
