// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Hotfix/Public/UpdateManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUpdateManager() {}
// Cross Module References
	HOTFIX_API UEnum* Z_Construct_UEnum_Hotfix_EUpdateCompletionStatus();
	UPackage* Z_Construct_UPackage__Script_Hotfix();
	HOTFIX_API UEnum* Z_Construct_UEnum_Hotfix_EUpdateState();
	HOTFIX_API UClass* Z_Construct_UClass_UUpdateManager_NoRegister();
	HOTFIX_API UClass* Z_Construct_UClass_UUpdateManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	HOTFIX_API UEnum* Z_Construct_UEnum_Hotfix_EHotfixResult();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	COREUOBJECT_API UClass* Z_Construct_UClass_UEnum();
// End Cross Module References
	static UEnum* EUpdateCompletionStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Hotfix_EUpdateCompletionStatus, Z_Construct_UPackage__Script_Hotfix(), TEXT("EUpdateCompletionStatus"));
		}
		return Singleton;
	}
	template<> HOTFIX_API UEnum* StaticEnum<EUpdateCompletionStatus>()
	{
		return EUpdateCompletionStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUpdateCompletionStatus(EUpdateCompletionStatus_StaticEnum, TEXT("/Script/Hotfix"), TEXT("EUpdateCompletionStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Hotfix_EUpdateCompletionStatus_Hash() { return 3376268165U; }
	UEnum* Z_Construct_UEnum_Hotfix_EUpdateCompletionStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Hotfix();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUpdateCompletionStatus"), 0, Get_Z_Construct_UEnum_Hotfix_EUpdateCompletionStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUpdateCompletionStatus::UpdateUnknown", (int64)EUpdateCompletionStatus::UpdateUnknown },
				{ "EUpdateCompletionStatus::UpdateSuccess", (int64)EUpdateCompletionStatus::UpdateSuccess },
				{ "EUpdateCompletionStatus::UpdateSuccess_NoChange", (int64)EUpdateCompletionStatus::UpdateSuccess_NoChange },
				{ "EUpdateCompletionStatus::UpdateSuccess_NeedsReload", (int64)EUpdateCompletionStatus::UpdateSuccess_NeedsReload },
				{ "EUpdateCompletionStatus::UpdateSuccess_NeedsRelaunch", (int64)EUpdateCompletionStatus::UpdateSuccess_NeedsRelaunch },
				{ "EUpdateCompletionStatus::UpdateSuccess_NeedsPatch", (int64)EUpdateCompletionStatus::UpdateSuccess_NeedsPatch },
				{ "EUpdateCompletionStatus::UpdateFailure_PatchCheck", (int64)EUpdateCompletionStatus::UpdateFailure_PatchCheck },
				{ "EUpdateCompletionStatus::UpdateFailure_HotfixCheck", (int64)EUpdateCompletionStatus::UpdateFailure_HotfixCheck },
				{ "EUpdateCompletionStatus::UpdateFailure_NotLoggedIn", (int64)EUpdateCompletionStatus::UpdateFailure_NotLoggedIn },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n * Possible outcomes at the end of an update check\n */" },
				{ "ModuleRelativePath", "Public/UpdateManager.h" },
				{ "ToolTip", "Possible outcomes at the end of an update check" },
				{ "UpdateFailure_HotfixCheck.Comment", "/** Update failed in the hotfix check */" },
				{ "UpdateFailure_HotfixCheck.Name", "EUpdateCompletionStatus::UpdateFailure_HotfixCheck" },
				{ "UpdateFailure_HotfixCheck.ToolTip", "Update failed in the hotfix check" },
				{ "UpdateFailure_NotLoggedIn.Comment", "/** Update failed due to not being logged in */" },
				{ "UpdateFailure_NotLoggedIn.Name", "EUpdateCompletionStatus::UpdateFailure_NotLoggedIn" },
				{ "UpdateFailure_NotLoggedIn.ToolTip", "Update failed due to not being logged in" },
				{ "UpdateFailure_PatchCheck.Comment", "/** Update failed in the patch check */" },
				{ "UpdateFailure_PatchCheck.Name", "EUpdateCompletionStatus::UpdateFailure_PatchCheck" },
				{ "UpdateFailure_PatchCheck.ToolTip", "Update failed in the patch check" },
				{ "UpdateSuccess.Comment", "/** Update completed successfully, some changes applied */" },
				{ "UpdateSuccess.Name", "EUpdateCompletionStatus::UpdateSuccess" },
				{ "UpdateSuccess.ToolTip", "Update completed successfully, some changes applied" },
				{ "UpdateSuccess_NeedsPatch.Comment", "/** Update completed successfully, a patch must be download to continue */" },
				{ "UpdateSuccess_NeedsPatch.Name", "EUpdateCompletionStatus::UpdateSuccess_NeedsPatch" },
				{ "UpdateSuccess_NeedsPatch.ToolTip", "Update completed successfully, a patch must be download to continue" },
				{ "UpdateSuccess_NeedsRelaunch.Comment", "/** Update completed successfully, need to relaunch the game */" },
				{ "UpdateSuccess_NeedsRelaunch.Name", "EUpdateCompletionStatus::UpdateSuccess_NeedsRelaunch" },
				{ "UpdateSuccess_NeedsRelaunch.ToolTip", "Update completed successfully, need to relaunch the game" },
				{ "UpdateSuccess_NeedsReload.Comment", "/** Update completed successfully, need to reload the map */" },
				{ "UpdateSuccess_NeedsReload.Name", "EUpdateCompletionStatus::UpdateSuccess_NeedsReload" },
				{ "UpdateSuccess_NeedsReload.ToolTip", "Update completed successfully, need to reload the map" },
				{ "UpdateSuccess_NoChange.Comment", "/** Update completed successfully, no changed needed */" },
				{ "UpdateSuccess_NoChange.Name", "EUpdateCompletionStatus::UpdateSuccess_NoChange" },
				{ "UpdateSuccess_NoChange.ToolTip", "Update completed successfully, no changed needed" },
				{ "UpdateUnknown.Comment", "/** Unknown update completion */" },
				{ "UpdateUnknown.Name", "EUpdateCompletionStatus::UpdateUnknown" },
				{ "UpdateUnknown.ToolTip", "Unknown update completion" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Hotfix,
				nullptr,
				"EUpdateCompletionStatus",
				"EUpdateCompletionStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EUpdateState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Hotfix_EUpdateState, Z_Construct_UPackage__Script_Hotfix(), TEXT("EUpdateState"));
		}
		return Singleton;
	}
	template<> HOTFIX_API UEnum* StaticEnum<EUpdateState>()
	{
		return EUpdateState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUpdateState(EUpdateState_StaticEnum, TEXT("/Script/Hotfix"), TEXT("EUpdateState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Hotfix_EUpdateState_Hash() { return 455862515U; }
	UEnum* Z_Construct_UEnum_Hotfix_EUpdateState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Hotfix();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUpdateState"), 0, Get_Z_Construct_UEnum_Hotfix_EUpdateState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUpdateState::UpdateIdle", (int64)EUpdateState::UpdateIdle },
				{ "EUpdateState::UpdatePending", (int64)EUpdateState::UpdatePending },
				{ "EUpdateState::CheckingForPatch", (int64)EUpdateState::CheckingForPatch },
				{ "EUpdateState::DetectingPlatformEnvironment", (int64)EUpdateState::DetectingPlatformEnvironment },
				{ "EUpdateState::CheckingForHotfix", (int64)EUpdateState::CheckingForHotfix },
				{ "EUpdateState::WaitingOnInitialLoad", (int64)EUpdateState::WaitingOnInitialLoad },
				{ "EUpdateState::InitialLoadComplete", (int64)EUpdateState::InitialLoadComplete },
				{ "EUpdateState::UpdateComplete", (int64)EUpdateState::UpdateComplete },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "CheckingForHotfix.Comment", "/** Checking with hotfix for available updates */" },
				{ "CheckingForHotfix.Name", "EUpdateState::CheckingForHotfix" },
				{ "CheckingForHotfix.ToolTip", "Checking with hotfix for available updates" },
				{ "CheckingForPatch.Comment", "/** Checking for an available patch */" },
				{ "CheckingForPatch.Name", "EUpdateState::CheckingForPatch" },
				{ "CheckingForPatch.ToolTip", "Checking for an available patch" },
				{ "Comment", "/**\n * Various states the update manager flows through as it checks for patches/hotfixes\n */" },
				{ "DetectingPlatformEnvironment.Comment", "/** Detect the console environment via an auth request */" },
				{ "DetectingPlatformEnvironment.Name", "EUpdateState::DetectingPlatformEnvironment" },
				{ "DetectingPlatformEnvironment.ToolTip", "Detect the console environment via an auth request" },
				{ "InitialLoadComplete.Comment", "/** Preloading complete */" },
				{ "InitialLoadComplete.Name", "EUpdateState::InitialLoadComplete" },
				{ "InitialLoadComplete.ToolTip", "Preloading complete" },
				{ "ModuleRelativePath", "Public/UpdateManager.h" },
				{ "ToolTip", "Various states the update manager flows through as it checks for patches/hotfixes" },
				{ "UpdateComplete.Comment", "/** Last update check completed successfully */" },
				{ "UpdateComplete.Name", "EUpdateState::UpdateComplete" },
				{ "UpdateComplete.ToolTip", "Last update check completed successfully" },
				{ "UpdateIdle.Comment", "/** No updates in progress */" },
				{ "UpdateIdle.Name", "EUpdateState::UpdateIdle" },
				{ "UpdateIdle.ToolTip", "No updates in progress" },
				{ "UpdatePending.Comment", "/** An update is waiting to be triggered at the right time */" },
				{ "UpdatePending.Name", "EUpdateState::UpdatePending" },
				{ "UpdatePending.ToolTip", "An update is waiting to be triggered at the right time" },
				{ "WaitingOnInitialLoad.Comment", "/** Waiting for the async loading / preloading to complete */" },
				{ "WaitingOnInitialLoad.Name", "EUpdateState::WaitingOnInitialLoad" },
				{ "WaitingOnInitialLoad.ToolTip", "Waiting for the async loading / preloading to complete" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Hotfix,
				nullptr,
				"EUpdateState",
				"EUpdateState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UUpdateManager::StaticRegisterNativesUUpdateManager()
	{
	}
	UClass* Z_Construct_UClass_UUpdateManager_NoRegister()
	{
		return UUpdateManager::StaticClass();
	}
	struct Z_Construct_UClass_UUpdateManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HotfixCheckCompleteDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HotfixCheckCompleteDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpdateCheckCompleteDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UpdateCheckCompleteDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HotfixAvailabilityCheckCompleteDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HotfixAvailabilityCheckCompleteDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpdateCheckAvailabilityCompleteDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UpdateCheckAvailabilityCompleteDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppSuspendedUpdateCheckTimeSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AppSuspendedUpdateCheckTimeSeconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPlatformEnvironmentDetected_MetaData[];
#endif
		static void NewProp_bPlatformEnvironmentDetected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPlatformEnvironmentDetected;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInitialUpdateFinished_MetaData[];
#endif
		static void NewProp_bInitialUpdateFinished_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInitialUpdateFinished;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCheckHotfixAvailabilityOnly_MetaData[];
#endif
		static void NewProp_bCheckHotfixAvailabilityOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCheckHotfixAvailabilityOnly;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CurrentUpdateState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentUpdateState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CurrentUpdateState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorstNumFilesPendingLoadViewed_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_WorstNumFilesPendingLoadViewed;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LastHotfixResult_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastHotfixResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LastHotfixResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastUpdateCheck_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastUpdateCheck;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LastCompletionResult_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastCompletionResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LastCompletionResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpdateStateEnum_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UpdateStateEnum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpdateCompletionEnum_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UpdateCompletionEnum;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUpdateManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Hotfix,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Update manager\n *\n * Checks the system and/or backend for the possibility of a patch and hotfix\n * Will not apply a hotfix if a pending patch is available\n * Notifies the game of the result of the check\n * - possibly requires UI to prevent user from playing if a patch is available\n * - possibly requires UI to prevent user from player if a hotfix requires a reload of existing data\n */" },
		{ "IncludePath", "UpdateManager.h" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Update manager\n\nChecks the system and/or backend for the possibility of a patch and hotfix\nWill not apply a hotfix if a pending patch is available\nNotifies the game of the result of the check\n- possibly requires UI to prevent user from playing if a patch is available\n- possibly requires UI to prevent user from player if a hotfix requires a reload of existing data" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_HotfixCheckCompleteDelay_MetaData[] = {
		{ "Comment", "/** Amount of time to wait between the internal hotfix check completing and advancing to the next stage */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Amount of time to wait between the internal hotfix check completing and advancing to the next stage" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_HotfixCheckCompleteDelay = { "HotfixCheckCompleteDelay", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpdateManager, HotfixCheckCompleteDelay), METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_HotfixCheckCompleteDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_HotfixCheckCompleteDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCheckCompleteDelay_MetaData[] = {
		{ "Comment", "/** Amount of time to wait at the end of the entire check before notifying listening entities */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Amount of time to wait at the end of the entire check before notifying listening entities" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCheckCompleteDelay = { "UpdateCheckCompleteDelay", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpdateManager, UpdateCheckCompleteDelay), METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCheckCompleteDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCheckCompleteDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_HotfixAvailabilityCheckCompleteDelay_MetaData[] = {
		{ "Comment", "/** Amount of time to wait between the internal hotfix availability check completing and advancing to the next stage */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Amount of time to wait between the internal hotfix availability check completing and advancing to the next stage" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_HotfixAvailabilityCheckCompleteDelay = { "HotfixAvailabilityCheckCompleteDelay", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpdateManager, HotfixAvailabilityCheckCompleteDelay), METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_HotfixAvailabilityCheckCompleteDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_HotfixAvailabilityCheckCompleteDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCheckAvailabilityCompleteDelay_MetaData[] = {
		{ "Comment", "/** Amount of time to wait at the end of the entire check before notifying listening entities (availability check only) */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Amount of time to wait at the end of the entire check before notifying listening entities (availability check only)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCheckAvailabilityCompleteDelay = { "UpdateCheckAvailabilityCompleteDelay", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpdateManager, UpdateCheckAvailabilityCompleteDelay), METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCheckAvailabilityCompleteDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCheckAvailabilityCompleteDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_AppSuspendedUpdateCheckTimeSeconds_MetaData[] = {
		{ "Comment", "/** If application is suspended longer than this, trigger an update check when resuming */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "If application is suspended longer than this, trigger an update check when resuming" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_AppSuspendedUpdateCheckTimeSeconds = { "AppSuspendedUpdateCheckTimeSeconds", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpdateManager, AppSuspendedUpdateCheckTimeSeconds), METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_AppSuspendedUpdateCheckTimeSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_AppSuspendedUpdateCheckTimeSeconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_bPlatformEnvironmentDetected_MetaData[] = {
		{ "Comment", "/** true if we've already detected the backend environment */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "true if we've already detected the backend environment" },
	};
#endif
	void Z_Construct_UClass_UUpdateManager_Statics::NewProp_bPlatformEnvironmentDetected_SetBit(void* Obj)
	{
		((UUpdateManager*)Obj)->bPlatformEnvironmentDetected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_bPlatformEnvironmentDetected = { "bPlatformEnvironmentDetected", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUpdateManager), &Z_Construct_UClass_UUpdateManager_Statics::NewProp_bPlatformEnvironmentDetected_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_bPlatformEnvironmentDetected_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_bPlatformEnvironmentDetected_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_bInitialUpdateFinished_MetaData[] = {
		{ "Comment", "/** Has the first update completed */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Has the first update completed" },
	};
#endif
	void Z_Construct_UClass_UUpdateManager_Statics::NewProp_bInitialUpdateFinished_SetBit(void* Obj)
	{
		((UUpdateManager*)Obj)->bInitialUpdateFinished = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_bInitialUpdateFinished = { "bInitialUpdateFinished", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUpdateManager), &Z_Construct_UClass_UUpdateManager_Statics::NewProp_bInitialUpdateFinished_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_bInitialUpdateFinished_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_bInitialUpdateFinished_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_bCheckHotfixAvailabilityOnly_MetaData[] = {
		{ "Comment", "/** Is this run only checking and not applying */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Is this run only checking and not applying" },
	};
#endif
	void Z_Construct_UClass_UUpdateManager_Statics::NewProp_bCheckHotfixAvailabilityOnly_SetBit(void* Obj)
	{
		((UUpdateManager*)Obj)->bCheckHotfixAvailabilityOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_bCheckHotfixAvailabilityOnly = { "bCheckHotfixAvailabilityOnly", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUpdateManager), &Z_Construct_UClass_UUpdateManager_Statics::NewProp_bCheckHotfixAvailabilityOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_bCheckHotfixAvailabilityOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_bCheckHotfixAvailabilityOnly_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_CurrentUpdateState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_CurrentUpdateState_MetaData[] = {
		{ "Comment", "/** Current state of the update */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Current state of the update" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_CurrentUpdateState = { "CurrentUpdateState", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpdateManager, CurrentUpdateState), Z_Construct_UEnum_Hotfix_EUpdateState, METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_CurrentUpdateState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_CurrentUpdateState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_WorstNumFilesPendingLoadViewed_MetaData[] = {
		{ "Comment", "/** What was the maximum number of pending async loads we've seen so far */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "What was the maximum number of pending async loads we've seen so far" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_WorstNumFilesPendingLoadViewed = { "WorstNumFilesPendingLoadViewed", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpdateManager, WorstNumFilesPendingLoadViewed), METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_WorstNumFilesPendingLoadViewed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_WorstNumFilesPendingLoadViewed_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastHotfixResult_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastHotfixResult_MetaData[] = {
		{ "Comment", "/** Result of the last hotfix */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Result of the last hotfix" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastHotfixResult = { "LastHotfixResult", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpdateManager, LastHotfixResult), Z_Construct_UEnum_Hotfix_EHotfixResult, METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastHotfixResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastHotfixResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastUpdateCheck_MetaData[] = {
		{ "Comment", "/** Timestamp of last update check (0:normal, 1:availability only) */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Timestamp of last update check (0:normal, 1:availability only)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastUpdateCheck = { "LastUpdateCheck", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(LastUpdateCheck, UUpdateManager), STRUCT_OFFSET(UUpdateManager, LastUpdateCheck), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastUpdateCheck_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastUpdateCheck_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastCompletionResult_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastCompletionResult_MetaData[] = {
		{ "Comment", "/** Last update check result (0:normal, 1:availability only) */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "Last update check result (0:normal, 1:availability only)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastCompletionResult = { "LastCompletionResult", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(LastCompletionResult, UUpdateManager), STRUCT_OFFSET(UUpdateManager, LastCompletionResult), Z_Construct_UEnum_Hotfix_EUpdateCompletionStatus, METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastCompletionResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastCompletionResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateStateEnum_MetaData[] = {
		{ "Comment", "/** String output */" },
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
		{ "ToolTip", "String output" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateStateEnum = { "UpdateStateEnum", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpdateManager, UpdateStateEnum), Z_Construct_UClass_UEnum, METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateStateEnum_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateStateEnum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCompletionEnum_MetaData[] = {
		{ "ModuleRelativePath", "Public/UpdateManager.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCompletionEnum = { "UpdateCompletionEnum", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpdateManager, UpdateCompletionEnum), Z_Construct_UClass_UEnum, METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCompletionEnum_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCompletionEnum_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUpdateManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_HotfixCheckCompleteDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCheckCompleteDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_HotfixAvailabilityCheckCompleteDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCheckAvailabilityCompleteDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_AppSuspendedUpdateCheckTimeSeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_bPlatformEnvironmentDetected,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_bInitialUpdateFinished,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_bCheckHotfixAvailabilityOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_CurrentUpdateState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_CurrentUpdateState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_WorstNumFilesPendingLoadViewed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastHotfixResult_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastHotfixResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastUpdateCheck,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastCompletionResult_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_LastCompletionResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateStateEnum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpdateManager_Statics::NewProp_UpdateCompletionEnum,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUpdateManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUpdateManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUpdateManager_Statics::ClassParams = {
		&UUpdateManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUpdateManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UUpdateManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUpdateManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUpdateManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUpdateManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUpdateManager, 3433750159);
	template<> HOTFIX_API UClass* StaticClass<UUpdateManager>()
	{
		return UUpdateManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUpdateManager(Z_Construct_UClass_UUpdateManager, &UUpdateManager::StaticClass, TEXT("/Script/Hotfix"), TEXT("UUpdateManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUpdateManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
