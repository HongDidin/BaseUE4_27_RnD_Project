// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/Chat/SocialChatRoom.h"
#include "Party/Public/Chat/SocialChatManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialChatRoom() {}
// Cross Module References
	PARTY_API UClass* Z_Construct_UClass_USocialChatRoom_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialChatRoom();
	PARTY_API UClass* Z_Construct_UClass_USocialChatChannel();
	UPackage* Z_Construct_UPackage__Script_Party();
// End Cross Module References
	void USocialChatRoom::StaticRegisterNativesUSocialChatRoom()
	{
	}
	UClass* Z_Construct_UClass_USocialChatRoom_NoRegister()
	{
		return USocialChatRoom::StaticClass();
	}
	struct Z_Construct_UClass_USocialChatRoom_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialChatRoom_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USocialChatChannel,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialChatRoom_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A multi-user chat room channel. Used for all chat situations outside of private user-to-user direct messages. */" },
		{ "IncludePath", "Chat/SocialChatRoom.h" },
		{ "ModuleRelativePath", "Public/Chat/SocialChatRoom.h" },
		{ "ToolTip", "A multi-user chat room channel. Used for all chat situations outside of private user-to-user direct messages." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialChatRoom_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialChatRoom>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialChatRoom_Statics::ClassParams = {
		&USocialChatRoom::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USocialChatRoom_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialChatRoom_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialChatRoom()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialChatRoom_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialChatRoom, 1825358847);
	template<> PARTY_API UClass* StaticClass<USocialChatRoom>()
	{
		return USocialChatRoom::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialChatRoom(Z_Construct_UClass_USocialChatRoom, &USocialChatRoom::StaticClass, TEXT("/Script/Party"), TEXT("USocialChatRoom"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialChatRoom);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
