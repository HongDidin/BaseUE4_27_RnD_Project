// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/SocialToolkit.h"
#include "Party/Public/SocialManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialToolkit() {}
// Cross Module References
	PARTY_API UClass* Z_Construct_UClass_USocialToolkit_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialToolkit();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Party();
	PARTY_API UClass* Z_Construct_UClass_USocialUser_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_ULocalPlayer_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialChatManager_NoRegister();
// End Cross Module References
	void USocialToolkit::StaticRegisterNativesUSocialToolkit()
	{
	}
	UClass* Z_Construct_UClass_USocialToolkit_NoRegister()
	{
		return USocialToolkit::StaticClass();
	}
	struct Z_Construct_UClass_USocialToolkit_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalUser_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LocalUser;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AllUsers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllUsers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllUsers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalPlayerOwner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LocalPlayerOwner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SocialChatManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SocialChatManager;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialToolkit_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialToolkit_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Represents the full suite of social functionality available to a given LocalPlayer */" },
		{ "IncludePath", "SocialToolkit.h" },
		{ "ModuleRelativePath", "Public/SocialToolkit.h" },
		{ "ToolTip", "Represents the full suite of social functionality available to a given LocalPlayer" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialToolkit_Statics::NewProp_LocalUser_MetaData[] = {
		{ "ModuleRelativePath", "Public/SocialToolkit.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialToolkit_Statics::NewProp_LocalUser = { "LocalUser", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialToolkit, LocalUser), Z_Construct_UClass_USocialUser_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USocialToolkit_Statics::NewProp_LocalUser_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialToolkit_Statics::NewProp_LocalUser_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialToolkit_Statics::NewProp_AllUsers_Inner = { "AllUsers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_USocialUser_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialToolkit_Statics::NewProp_AllUsers_MetaData[] = {
		{ "ModuleRelativePath", "Public/SocialToolkit.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USocialToolkit_Statics::NewProp_AllUsers = { "AllUsers", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialToolkit, AllUsers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USocialToolkit_Statics::NewProp_AllUsers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialToolkit_Statics::NewProp_AllUsers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialToolkit_Statics::NewProp_LocalPlayerOwner_MetaData[] = {
		{ "ModuleRelativePath", "Public/SocialToolkit.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialToolkit_Statics::NewProp_LocalPlayerOwner = { "LocalPlayerOwner", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialToolkit, LocalPlayerOwner), Z_Construct_UClass_ULocalPlayer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USocialToolkit_Statics::NewProp_LocalPlayerOwner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialToolkit_Statics::NewProp_LocalPlayerOwner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialToolkit_Statics::NewProp_SocialChatManager_MetaData[] = {
		{ "ModuleRelativePath", "Public/SocialToolkit.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialToolkit_Statics::NewProp_SocialChatManager = { "SocialChatManager", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialToolkit, SocialChatManager), Z_Construct_UClass_USocialChatManager_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USocialToolkit_Statics::NewProp_SocialChatManager_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialToolkit_Statics::NewProp_SocialChatManager_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USocialToolkit_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialToolkit_Statics::NewProp_LocalUser,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialToolkit_Statics::NewProp_AllUsers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialToolkit_Statics::NewProp_AllUsers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialToolkit_Statics::NewProp_LocalPlayerOwner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialToolkit_Statics::NewProp_SocialChatManager,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialToolkit_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialToolkit>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialToolkit_Statics::ClassParams = {
		&USocialToolkit::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USocialToolkit_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USocialToolkit_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USocialToolkit_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialToolkit_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialToolkit()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialToolkit_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialToolkit, 3612767578);
	template<> PARTY_API UClass* StaticClass<USocialToolkit>()
	{
		return USocialToolkit::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialToolkit(Z_Construct_UClass_USocialToolkit, &USocialToolkit::StaticClass, TEXT("/Script/Party"), TEXT("USocialToolkit"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialToolkit);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
