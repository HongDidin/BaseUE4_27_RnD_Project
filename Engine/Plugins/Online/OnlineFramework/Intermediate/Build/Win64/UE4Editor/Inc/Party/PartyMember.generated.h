// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_PartyMember_generated_h
#error "PartyMember.generated.h already included, missing '#pragma once' in PartyMember.h"
#endif
#define PARTY_PartyMember_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPartyMemberRepData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Platform() { return STRUCT_OFFSET(FPartyMemberRepData, Platform); } \
	FORCEINLINE static uint32 __PPO__PlatformUniqueId() { return STRUCT_OFFSET(FPartyMemberRepData, PlatformUniqueId); } \
	FORCEINLINE static uint32 __PPO__PlatformSessionId() { return STRUCT_OFFSET(FPartyMemberRepData, PlatformSessionId); } \
	FORCEINLINE static uint32 __PPO__CrossplayPreference() { return STRUCT_OFFSET(FPartyMemberRepData, CrossplayPreference); } \
	typedef FOnlinePartyRepDataBase Super;


template<> PARTY_API UScriptStruct* StaticStruct<struct FPartyMemberRepData>();

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPartyMember(); \
	friend struct Z_Construct_UClass_UPartyMember_Statics; \
public: \
	DECLARE_CLASS(UPartyMember, UObject, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(UPartyMember) \
	DECLARE_WITHIN(USocialParty) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_INCLASS \
private: \
	static void StaticRegisterNativesUPartyMember(); \
	friend struct Z_Construct_UClass_UPartyMember_Statics; \
public: \
	DECLARE_CLASS(UPartyMember, UObject, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(UPartyMember) \
	DECLARE_WITHIN(USocialParty) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPartyMember(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPartyMember) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPartyMember); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPartyMember); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPartyMember(UPartyMember&&); \
	NO_API UPartyMember(const UPartyMember&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPartyMember(UPartyMember&&); \
	NO_API UPartyMember(const UPartyMember&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPartyMember); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPartyMember); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UPartyMember)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SocialUser() { return STRUCT_OFFSET(UPartyMember, SocialUser); }


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_62_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h_65_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class UPartyMember>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Party_PartyMember_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
