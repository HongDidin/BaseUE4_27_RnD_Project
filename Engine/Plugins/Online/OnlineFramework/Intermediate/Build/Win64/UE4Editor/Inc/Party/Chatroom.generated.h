// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_Chatroom_generated_h
#error "Chatroom.generated.h already included, missing '#pragma once' in Chatroom.h"
#endif
#define PARTY_Chatroom_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUChatroom(); \
	friend struct Z_Construct_UClass_UChatroom_Statics; \
public: \
	DECLARE_CLASS(UChatroom, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(UChatroom) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUChatroom(); \
	friend struct Z_Construct_UClass_UChatroom_Statics; \
public: \
	DECLARE_CLASS(UChatroom, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(UChatroom) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UChatroom(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UChatroom) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UChatroom); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UChatroom); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UChatroom(UChatroom&&); \
	NO_API UChatroom(const UChatroom&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UChatroom(UChatroom&&); \
	NO_API UChatroom(const UChatroom&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UChatroom); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UChatroom); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UChatroom)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CurrentChatRoomId() { return STRUCT_OFFSET(UChatroom, CurrentChatRoomId); } \
	FORCEINLINE static uint32 __PPO__MaxChatRoomRetries() { return STRUCT_OFFSET(UChatroom, MaxChatRoomRetries); } \
	FORCEINLINE static uint32 __PPO__NumChatRoomRetries() { return STRUCT_OFFSET(UChatroom, NumChatRoomRetries); }


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_33_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h_36_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class UChatroom>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chatroom_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
