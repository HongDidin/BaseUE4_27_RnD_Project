// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/Chat/SocialReadOnlyChatChannel.h"
#include "Party/Public/Chat/SocialChatManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialReadOnlyChatChannel() {}
// Cross Module References
	PARTY_API UClass* Z_Construct_UClass_USocialReadOnlyChatChannel_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialReadOnlyChatChannel();
	PARTY_API UClass* Z_Construct_UClass_USocialChatChannel();
	UPackage* Z_Construct_UPackage__Script_Party();
// End Cross Module References
	void USocialReadOnlyChatChannel::StaticRegisterNativesUSocialReadOnlyChatChannel()
	{
	}
	UClass* Z_Construct_UClass_USocialReadOnlyChatChannel_NoRegister()
	{
		return USocialReadOnlyChatChannel::StaticClass();
	}
	struct Z_Construct_UClass_USocialReadOnlyChatChannel_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialReadOnlyChatChannel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USocialChatChannel,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialReadOnlyChatChannel_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A strawman chat channel that relies exclusively on other channels messages for content, does not support sending messages\n */" },
		{ "IncludePath", "Chat/SocialReadOnlyChatChannel.h" },
		{ "ModuleRelativePath", "Public/Chat/SocialReadOnlyChatChannel.h" },
		{ "ToolTip", "A strawman chat channel that relies exclusively on other channels messages for content, does not support sending messages" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialReadOnlyChatChannel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialReadOnlyChatChannel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialReadOnlyChatChannel_Statics::ClassParams = {
		&USocialReadOnlyChatChannel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USocialReadOnlyChatChannel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialReadOnlyChatChannel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialReadOnlyChatChannel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialReadOnlyChatChannel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialReadOnlyChatChannel, 137690592);
	template<> PARTY_API UClass* StaticClass<USocialReadOnlyChatChannel>()
	{
		return USocialReadOnlyChatChannel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialReadOnlyChatChannel(Z_Construct_UClass_USocialReadOnlyChatChannel, &USocialReadOnlyChatChannel::StaticClass, TEXT("/Script/Party"), TEXT("USocialReadOnlyChatChannel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialReadOnlyChatChannel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
