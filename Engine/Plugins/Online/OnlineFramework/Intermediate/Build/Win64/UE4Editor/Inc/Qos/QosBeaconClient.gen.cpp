// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Qos/Public/QosBeaconClient.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeQosBeaconClient() {}
// Cross Module References
	QOS_API UEnum* Z_Construct_UEnum_Qos_EQosResponseType();
	UPackage* Z_Construct_UPackage__Script_Qos();
	QOS_API UClass* Z_Construct_UClass_AQosBeaconClient_NoRegister();
	QOS_API UClass* Z_Construct_UClass_AQosBeaconClient();
	ONLINESUBSYSTEMUTILS_API UClass* Z_Construct_UClass_AOnlineBeaconClient();
// End Cross Module References
	static UEnum* EQosResponseType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Qos_EQosResponseType, Z_Construct_UPackage__Script_Qos(), TEXT("EQosResponseType"));
		}
		return Singleton;
	}
	template<> QOS_API UEnum* StaticEnum<EQosResponseType>()
	{
		return EQosResponseType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EQosResponseType(EQosResponseType_StaticEnum, TEXT("/Script/Qos"), TEXT("EQosResponseType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Qos_EQosResponseType_Hash() { return 2395887506U; }
	UEnum* Z_Construct_UEnum_Qos_EQosResponseType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Qos();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EQosResponseType"), 0, Get_Z_Construct_UEnum_Qos_EQosResponseType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EQosResponseType::NoResponse", (int64)EQosResponseType::NoResponse },
				{ "EQosResponseType::Success", (int64)EQosResponseType::Success },
				{ "EQosResponseType::Failure", (int64)EQosResponseType::Failure },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Types of responses that can come back from the beacon\n */" },
				{ "Failure.Comment", "/** Some kind of failure */" },
				{ "Failure.Name", "EQosResponseType::Failure" },
				{ "Failure.ToolTip", "Some kind of failure" },
				{ "ModuleRelativePath", "Public/QosBeaconClient.h" },
				{ "NoResponse.Comment", "/** Failed to connect to Qos endpoint */" },
				{ "NoResponse.Name", "EQosResponseType::NoResponse" },
				{ "NoResponse.ToolTip", "Failed to connect to Qos endpoint" },
				{ "Success.Comment", "/** Response received from the Qos host  */" },
				{ "Success.Name", "EQosResponseType::Success" },
				{ "Success.ToolTip", "Response received from the Qos host" },
				{ "ToolTip", "Types of responses that can come back from the beacon" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Qos,
				nullptr,
				"EQosResponseType",
				"EQosResponseType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(AQosBeaconClient::execClientQosResponse)
	{
		P_GET_ENUM(EQosResponseType,Z_Param_Response);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClientQosResponse_Implementation(EQosResponseType(Z_Param_Response));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AQosBeaconClient::execServerQosRequest)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InSessionId);
		P_FINISH;
		P_NATIVE_BEGIN;
		if (!P_THIS->ServerQosRequest_Validate(Z_Param_InSessionId))
		{
			RPC_ValidateFailed(TEXT("ServerQosRequest_Validate"));
			return;
		}
		P_THIS->ServerQosRequest_Implementation(Z_Param_InSessionId);
		P_NATIVE_END;
	}
	static FName NAME_AQosBeaconClient_ClientQosResponse = FName(TEXT("ClientQosResponse"));
	void AQosBeaconClient::ClientQosResponse(EQosResponseType Response)
	{
		QosBeaconClient_eventClientQosResponse_Parms Parms;
		Parms.Response=Response;
		ProcessEvent(FindFunctionChecked(NAME_AQosBeaconClient_ClientQosResponse),&Parms);
	}
	static FName NAME_AQosBeaconClient_ServerQosRequest = FName(TEXT("ServerQosRequest"));
	void AQosBeaconClient::ServerQosRequest(const FString& InSessionId)
	{
		QosBeaconClient_eventServerQosRequest_Parms Parms;
		Parms.InSessionId=InSessionId;
		ProcessEvent(FindFunctionChecked(NAME_AQosBeaconClient_ServerQosRequest),&Parms);
	}
	void AQosBeaconClient::StaticRegisterNativesAQosBeaconClient()
	{
		UClass* Class = AQosBeaconClient::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ClientQosResponse", &AQosBeaconClient::execClientQosResponse },
			{ "ServerQosRequest", &AQosBeaconClient::execServerQosRequest },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics
	{
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Response_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Response;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::NewProp_Response_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::NewProp_Response = { "Response", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(QosBeaconClient_eventClientQosResponse_Parms, Response), Z_Construct_UEnum_Qos_EQosResponseType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::NewProp_Response_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::NewProp_Response,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Response from the host session after making a Qos request\n\x09 */" },
		{ "ModuleRelativePath", "Public/QosBeaconClient.h" },
		{ "ToolTip", "Response from the host session after making a Qos request" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AQosBeaconClient, nullptr, "ClientQosResponse", nullptr, nullptr, sizeof(QosBeaconClient_eventClientQosResponse_Parms), Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x01080CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InSessionId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::NewProp_InSessionId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::NewProp_InSessionId = { "InSessionId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(QosBeaconClient_eventServerQosRequest_Parms, InSessionId), METADATA_PARAMS(Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::NewProp_InSessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::NewProp_InSessionId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::NewProp_InSessionId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Contact the server with a Qos request and begin timing\n\x09 *\n\x09 * @param InSessionId reference session id to make sure the session is the correct one\n\x09 */" },
		{ "ModuleRelativePath", "Public/QosBeaconClient.h" },
		{ "ToolTip", "Contact the server with a Qos request and begin timing\n\n@param InSessionId reference session id to make sure the session is the correct one" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AQosBeaconClient, nullptr, "ServerQosRequest", nullptr, nullptr, sizeof(QosBeaconClient_eventServerQosRequest_Parms), Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x80280CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AQosBeaconClient_NoRegister()
	{
		return AQosBeaconClient::StaticClass();
	}
	struct Z_Construct_UClass_AQosBeaconClient_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AQosBeaconClient_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AOnlineBeaconClient,
		(UObject* (*)())Z_Construct_UPackage__Script_Qos,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AQosBeaconClient_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AQosBeaconClient_ClientQosResponse, "ClientQosResponse" }, // 3680997862
		{ &Z_Construct_UFunction_AQosBeaconClient_ServerQosRequest, "ServerQosRequest" }, // 2196064503
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AQosBeaconClient_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A beacon client used for quality timings to a specified session\n */" },
		{ "IncludePath", "QosBeaconClient.h" },
		{ "ModuleRelativePath", "Public/QosBeaconClient.h" },
		{ "ToolTip", "A beacon client used for quality timings to a specified session" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AQosBeaconClient_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AQosBeaconClient>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AQosBeaconClient_Statics::ClassParams = {
		&AQosBeaconClient::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AQosBeaconClient_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AQosBeaconClient_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AQosBeaconClient()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AQosBeaconClient_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AQosBeaconClient, 3705662559);
	template<> QOS_API UClass* StaticClass<AQosBeaconClient>()
	{
		return AQosBeaconClient::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AQosBeaconClient(Z_Construct_UClass_AQosBeaconClient, &AQosBeaconClient::StaticClass, TEXT("/Script/Qos"), TEXT("AQosBeaconClient"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AQosBeaconClient);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
