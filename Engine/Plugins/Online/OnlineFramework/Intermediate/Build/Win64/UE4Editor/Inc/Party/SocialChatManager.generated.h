// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_SocialChatManager_generated_h
#error "SocialChatManager.generated.h already included, missing '#pragma once' in SocialChatManager.h"
#endif
#define PARTY_SocialChatManager_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_27_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics; \
	PARTY_API static class UScriptStruct* StaticStruct();


template<> PARTY_API UScriptStruct* StaticStruct<struct FSocialChatChannelConfig>();

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSocialChatManager(); \
	friend struct Z_Construct_UClass_USocialChatManager_Statics; \
public: \
	DECLARE_CLASS(USocialChatManager, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialChatManager) \
	DECLARE_WITHIN(USocialToolkit) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_INCLASS \
private: \
	static void StaticRegisterNativesUSocialChatManager(); \
	friend struct Z_Construct_UClass_USocialChatManager_Statics; \
public: \
	DECLARE_CLASS(USocialChatManager, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialChatManager) \
	DECLARE_WITHIN(USocialToolkit) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialChatManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialChatManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialChatManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialChatManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialChatManager(USocialChatManager&&); \
	NO_API USocialChatManager(const USocialChatManager&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialChatManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialChatManager(USocialChatManager&&); \
	NO_API USocialChatManager(const USocialChatManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialChatManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialChatManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialChatManager)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DirectChannelsByTargetUser() { return STRUCT_OFFSET(USocialChatManager, DirectChannelsByTargetUser); } \
	FORCEINLINE static uint32 __PPO__ChatRoomsById() { return STRUCT_OFFSET(USocialChatManager, ChatRoomsById); } \
	FORCEINLINE static uint32 __PPO__ReadOnlyChannelsByDisplayName() { return STRUCT_OFFSET(USocialChatManager, ReadOnlyChannelsByDisplayName); } \
	FORCEINLINE static uint32 __PPO__bEnableChatSlashCommands() { return STRUCT_OFFSET(USocialChatManager, bEnableChatSlashCommands); } \
	FORCEINLINE static uint32 __PPO__GroupChannels() { return STRUCT_OFFSET(USocialChatManager, GroupChannels); }


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_50_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h_53_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class USocialChatManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
