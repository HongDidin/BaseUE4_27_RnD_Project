// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/Chatroom.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeChatroom() {}
// Cross Module References
	PARTY_API UClass* Z_Construct_UClass_UChatroom_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_UChatroom();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Party();
// End Cross Module References
	void UChatroom::StaticRegisterNativesUChatroom()
	{
	}
	UClass* Z_Construct_UClass_UChatroom_NoRegister()
	{
		return UChatroom::StaticClass();
	}
	struct Z_Construct_UClass_UChatroom_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentChatRoomId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CurrentChatRoomId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxChatRoomRetries_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxChatRoomRetries;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumChatRoomRetries_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumChatRoomRetries;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UChatroom_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChatroom_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Helper class for maintaining a single chat room at the game level\n */" },
		{ "IncludePath", "Chatroom.h" },
		{ "ModuleRelativePath", "Public/Chatroom.h" },
		{ "ToolTip", "Helper class for maintaining a single chat room at the game level" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChatroom_Statics::NewProp_CurrentChatRoomId_MetaData[] = {
		{ "Comment", "/** Current chat room associated with this object (FString so UPROPERTY works) */" },
		{ "ModuleRelativePath", "Public/Chatroom.h" },
		{ "ToolTip", "Current chat room associated with this object (FString so UPROPERTY works)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UChatroom_Statics::NewProp_CurrentChatRoomId = { "CurrentChatRoomId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChatroom, CurrentChatRoomId), METADATA_PARAMS(Z_Construct_UClass_UChatroom_Statics::NewProp_CurrentChatRoomId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChatroom_Statics::NewProp_CurrentChatRoomId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChatroom_Statics::NewProp_MaxChatRoomRetries_MetaData[] = {
		{ "Comment", "/** Max number of retries before giving up on chat */" },
		{ "ModuleRelativePath", "Public/Chatroom.h" },
		{ "ToolTip", "Max number of retries before giving up on chat" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UChatroom_Statics::NewProp_MaxChatRoomRetries = { "MaxChatRoomRetries", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChatroom, MaxChatRoomRetries), METADATA_PARAMS(Z_Construct_UClass_UChatroom_Statics::NewProp_MaxChatRoomRetries_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChatroom_Statics::NewProp_MaxChatRoomRetries_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UChatroom_Statics::NewProp_NumChatRoomRetries_MetaData[] = {
		{ "Comment", "/** Current number of retries on a chat room */" },
		{ "ModuleRelativePath", "Public/Chatroom.h" },
		{ "ToolTip", "Current number of retries on a chat room" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UChatroom_Statics::NewProp_NumChatRoomRetries = { "NumChatRoomRetries", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UChatroom, NumChatRoomRetries), METADATA_PARAMS(Z_Construct_UClass_UChatroom_Statics::NewProp_NumChatRoomRetries_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UChatroom_Statics::NewProp_NumChatRoomRetries_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UChatroom_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChatroom_Statics::NewProp_CurrentChatRoomId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChatroom_Statics::NewProp_MaxChatRoomRetries,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UChatroom_Statics::NewProp_NumChatRoomRetries,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UChatroom_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UChatroom>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UChatroom_Statics::ClassParams = {
		&UChatroom::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UChatroom_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UChatroom_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UChatroom_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UChatroom_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UChatroom()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UChatroom_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UChatroom, 468894548);
	template<> PARTY_API UClass* StaticClass<UChatroom>()
	{
		return UChatroom::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UChatroom(Z_Construct_UClass_UChatroom, &UChatroom::StaticClass, TEXT("/Script/Party"), TEXT("UChatroom"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UChatroom);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
