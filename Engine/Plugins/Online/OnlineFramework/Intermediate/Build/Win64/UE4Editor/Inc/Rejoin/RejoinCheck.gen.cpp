// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Rejoin/Public/RejoinCheck.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRejoinCheck() {}
// Cross Module References
	REJOIN_API UEnum* Z_Construct_UEnum_Rejoin_ERejoinStatus();
	UPackage* Z_Construct_UPackage__Script_Rejoin();
	REJOIN_API UClass* Z_Construct_UClass_URejoinCheck_NoRegister();
	REJOIN_API UClass* Z_Construct_UClass_URejoinCheck();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	static UEnum* ERejoinStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Rejoin_ERejoinStatus, Z_Construct_UPackage__Script_Rejoin(), TEXT("ERejoinStatus"));
		}
		return Singleton;
	}
	template<> REJOIN_API UEnum* StaticEnum<ERejoinStatus>()
	{
		return ERejoinStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERejoinStatus(ERejoinStatus_StaticEnum, TEXT("/Script/Rejoin"), TEXT("ERejoinStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Rejoin_ERejoinStatus_Hash() { return 143865175U; }
	UEnum* Z_Construct_UEnum_Rejoin_ERejoinStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Rejoin();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERejoinStatus"), 0, Get_Z_Construct_UEnum_Rejoin_ERejoinStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERejoinStatus::NoMatchToRejoin", (int64)ERejoinStatus::NoMatchToRejoin },
				{ "ERejoinStatus::RejoinAvailable", (int64)ERejoinStatus::RejoinAvailable },
				{ "ERejoinStatus::UpdatingStatus", (int64)ERejoinStatus::UpdatingStatus },
				{ "ERejoinStatus::NeedsRecheck", (int64)ERejoinStatus::NeedsRecheck },
				{ "ERejoinStatus::NoMatchToRejoin_MatchEnded", (int64)ERejoinStatus::NoMatchToRejoin_MatchEnded },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n * Possible states that a rejoin check can be in at any given time\n */" },
				{ "ModuleRelativePath", "Public/RejoinCheck.h" },
				{ "NeedsRecheck.Comment", "// We need to recheck the state before allowing any further progress through the UI (e.g right after login or right after leaving a match without it ending normally).\n" },
				{ "NeedsRecheck.Name", "ERejoinStatus::NeedsRecheck" },
				{ "NeedsRecheck.ToolTip", "We need to recheck the state before allowing any further progress through the UI (e.g right after login or right after leaving a match without it ending normally)." },
				{ "NoMatchToRejoin.Comment", "// There is no match to rejoin.  The user is already in a match or there is no match in progress for the user. \n" },
				{ "NoMatchToRejoin.Name", "ERejoinStatus::NoMatchToRejoin" },
				{ "NoMatchToRejoin.ToolTip", "There is no match to rejoin.  The user is already in a match or there is no match in progress for the user." },
				{ "NoMatchToRejoin_MatchEnded.Comment", "// Match ended normally, no check required (only set when returning from a match)\n" },
				{ "NoMatchToRejoin_MatchEnded.Name", "ERejoinStatus::NoMatchToRejoin_MatchEnded" },
				{ "NoMatchToRejoin_MatchEnded.ToolTip", "Match ended normally, no check required (only set when returning from a match)" },
				{ "RejoinAvailable.Comment", "// There is a rejoin available for the user\n" },
				{ "RejoinAvailable.Name", "ERejoinStatus::RejoinAvailable" },
				{ "RejoinAvailable.ToolTip", "There is a rejoin available for the user" },
				{ "ToolTip", "Possible states that a rejoin check can be in at any given time" },
				{ "UpdatingStatus.Comment", "// We are currently updating the status of rejoin\n" },
				{ "UpdatingStatus.Name", "ERejoinStatus::UpdatingStatus" },
				{ "UpdatingStatus.ToolTip", "We are currently updating the status of rejoin" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Rejoin,
				nullptr,
				"ERejoinStatus",
				"ERejoinStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void URejoinCheck::StaticRegisterNativesURejoinCheck()
	{
	}
	UClass* Z_Construct_UClass_URejoinCheck_NoRegister()
	{
		return URejoinCheck::StaticClass();
	}
	struct Z_Construct_UClass_URejoinCheck_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LastKnownStatus_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastKnownStatus_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LastKnownStatus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRejoinAfterCheck_MetaData[];
#endif
		static void NewProp_bRejoinAfterCheck_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRejoinAfterCheck;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAttemptingRejoin_MetaData[];
#endif
		static void NewProp_bAttemptingRejoin_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAttemptingRejoin;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URejoinCheck_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Rejoin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URejoinCheck_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Class responsible for maintaining the status/availability of a session already in progress for a client to join\n */" },
		{ "IncludePath", "RejoinCheck.h" },
		{ "ModuleRelativePath", "Public/RejoinCheck.h" },
		{ "ToolTip", "Class responsible for maintaining the status/availability of a session already in progress for a client to join" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URejoinCheck_Statics::NewProp_LastKnownStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URejoinCheck_Statics::NewProp_LastKnownStatus_MetaData[] = {
		{ "Comment", "/** Rejoin status */" },
		{ "ModuleRelativePath", "Public/RejoinCheck.h" },
		{ "ToolTip", "Rejoin status" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URejoinCheck_Statics::NewProp_LastKnownStatus = { "LastKnownStatus", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URejoinCheck, LastKnownStatus), Z_Construct_UEnum_Rejoin_ERejoinStatus, METADATA_PARAMS(Z_Construct_UClass_URejoinCheck_Statics::NewProp_LastKnownStatus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URejoinCheck_Statics::NewProp_LastKnownStatus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URejoinCheck_Statics::NewProp_bRejoinAfterCheck_MetaData[] = {
		{ "Comment", "/** Flag set during a possible brief period where the user hit rejoin but the check was already in flight */" },
		{ "ModuleRelativePath", "Public/RejoinCheck.h" },
		{ "ToolTip", "Flag set during a possible brief period where the user hit rejoin but the check was already in flight" },
	};
#endif
	void Z_Construct_UClass_URejoinCheck_Statics::NewProp_bRejoinAfterCheck_SetBit(void* Obj)
	{
		((URejoinCheck*)Obj)->bRejoinAfterCheck = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URejoinCheck_Statics::NewProp_bRejoinAfterCheck = { "bRejoinAfterCheck", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URejoinCheck), &Z_Construct_UClass_URejoinCheck_Statics::NewProp_bRejoinAfterCheck_SetBit, METADATA_PARAMS(Z_Construct_UClass_URejoinCheck_Statics::NewProp_bRejoinAfterCheck_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URejoinCheck_Statics::NewProp_bRejoinAfterCheck_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URejoinCheck_Statics::NewProp_bAttemptingRejoin_MetaData[] = {
		{ "Comment", "/** Is a rejoin attempt in progress, prevents reentry */" },
		{ "ModuleRelativePath", "Public/RejoinCheck.h" },
		{ "ToolTip", "Is a rejoin attempt in progress, prevents reentry" },
	};
#endif
	void Z_Construct_UClass_URejoinCheck_Statics::NewProp_bAttemptingRejoin_SetBit(void* Obj)
	{
		((URejoinCheck*)Obj)->bAttemptingRejoin = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URejoinCheck_Statics::NewProp_bAttemptingRejoin = { "bAttemptingRejoin", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URejoinCheck), &Z_Construct_UClass_URejoinCheck_Statics::NewProp_bAttemptingRejoin_SetBit, METADATA_PARAMS(Z_Construct_UClass_URejoinCheck_Statics::NewProp_bAttemptingRejoin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URejoinCheck_Statics::NewProp_bAttemptingRejoin_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URejoinCheck_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URejoinCheck_Statics::NewProp_LastKnownStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URejoinCheck_Statics::NewProp_LastKnownStatus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URejoinCheck_Statics::NewProp_bRejoinAfterCheck,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URejoinCheck_Statics::NewProp_bAttemptingRejoin,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URejoinCheck_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URejoinCheck>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URejoinCheck_Statics::ClassParams = {
		&URejoinCheck::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URejoinCheck_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URejoinCheck_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_URejoinCheck_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URejoinCheck_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URejoinCheck()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URejoinCheck_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URejoinCheck, 1679056741);
	template<> REJOIN_API UClass* StaticClass<URejoinCheck>()
	{
		return URejoinCheck::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URejoinCheck(Z_Construct_UClass_URejoinCheck, &URejoinCheck::StaticClass, TEXT("/Script/Rejoin"), TEXT("URejoinCheck"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URejoinCheck);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
