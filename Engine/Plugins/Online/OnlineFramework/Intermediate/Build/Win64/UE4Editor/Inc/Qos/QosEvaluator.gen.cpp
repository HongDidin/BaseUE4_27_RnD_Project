// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Qos/Private/QosEvaluator.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeQosEvaluator() {}
// Cross Module References
	QOS_API UClass* Z_Construct_UClass_UQosEvaluator_NoRegister();
	QOS_API UClass* Z_Construct_UClass_UQosEvaluator();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Qos();
	QOS_API UScriptStruct* Z_Construct_UScriptStruct_FDatacenterQosInstance();
// End Cross Module References
	void UQosEvaluator::StaticRegisterNativesUQosEvaluator()
	{
	}
	UClass* Z_Construct_UClass_UQosEvaluator_NoRegister()
	{
		return UQosEvaluator::StaticClass();
	}
	struct Z_Construct_UClass_UQosEvaluator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInProgress_MetaData[];
#endif
		static void NewProp_bInProgress_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInProgress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCancelOperation_MetaData[];
#endif
		static void NewProp_bCancelOperation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCancelOperation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Datacenters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Datacenters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Datacenters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UQosEvaluator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Qos,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosEvaluator_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Evaluates QoS metrics to determine the best datacenter under current conditions\n * Additionally capable of generically pinging an array of servers that have a QosBeaconHost active\n */" },
		{ "IncludePath", "QosEvaluator.h" },
		{ "ModuleRelativePath", "Private/QosEvaluator.h" },
		{ "ToolTip", "Evaluates QoS metrics to determine the best datacenter under current conditions\nAdditionally capable of generically pinging an array of servers that have a QosBeaconHost active" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bInProgress_MetaData[] = {
		{ "Comment", "/** A QoS operation is in progress */" },
		{ "ModuleRelativePath", "Private/QosEvaluator.h" },
		{ "ToolTip", "A QoS operation is in progress" },
	};
#endif
	void Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bInProgress_SetBit(void* Obj)
	{
		((UQosEvaluator*)Obj)->bInProgress = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bInProgress = { "bInProgress", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UQosEvaluator), &Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bInProgress_SetBit, METADATA_PARAMS(Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bInProgress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bInProgress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bCancelOperation_MetaData[] = {
		{ "Comment", "/** Should cancel occur at the next available opportunity */" },
		{ "ModuleRelativePath", "Private/QosEvaluator.h" },
		{ "ToolTip", "Should cancel occur at the next available opportunity" },
	};
#endif
	void Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bCancelOperation_SetBit(void* Obj)
	{
		((UQosEvaluator*)Obj)->bCancelOperation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bCancelOperation = { "bCancelOperation", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UQosEvaluator), &Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bCancelOperation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bCancelOperation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bCancelOperation_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UQosEvaluator_Statics::NewProp_Datacenters_Inner = { "Datacenters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDatacenterQosInstance, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosEvaluator_Statics::NewProp_Datacenters_MetaData[] = {
		{ "Comment", "/** Array of datacenters currently being evaluated */" },
		{ "ModuleRelativePath", "Private/QosEvaluator.h" },
		{ "ToolTip", "Array of datacenters currently being evaluated" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UQosEvaluator_Statics::NewProp_Datacenters = { "Datacenters", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosEvaluator, Datacenters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UQosEvaluator_Statics::NewProp_Datacenters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosEvaluator_Statics::NewProp_Datacenters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UQosEvaluator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bInProgress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosEvaluator_Statics::NewProp_bCancelOperation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosEvaluator_Statics::NewProp_Datacenters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosEvaluator_Statics::NewProp_Datacenters,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UQosEvaluator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UQosEvaluator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UQosEvaluator_Statics::ClassParams = {
		&UQosEvaluator::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UQosEvaluator_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UQosEvaluator_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UQosEvaluator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UQosEvaluator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UQosEvaluator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UQosEvaluator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UQosEvaluator, 2996943337);
	template<> QOS_API UClass* StaticClass<UQosEvaluator>()
	{
		return UQosEvaluator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UQosEvaluator(Z_Construct_UClass_UQosEvaluator, &UQosEvaluator::StaticClass, TEXT("/Script/Qos"), TEXT("UQosEvaluator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UQosEvaluator);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
