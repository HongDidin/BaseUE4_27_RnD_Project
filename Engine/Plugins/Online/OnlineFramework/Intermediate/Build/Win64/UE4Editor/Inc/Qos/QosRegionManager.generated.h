// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef QOS_QosRegionManager_generated_h
#error "QosRegionManager.generated.h already included, missing '#pragma once' in QosRegionManager.h"
#endif
#define QOS_QosRegionManager_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_217_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRegionQosInstance_Statics; \
	static class UScriptStruct* StaticStruct();


template<> QOS_API UScriptStruct* StaticStruct<struct FRegionQosInstance>();

#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_159_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics; \
	static class UScriptStruct* StaticStruct();


template<> QOS_API UScriptStruct* StaticStruct<struct FDatacenterQosInstance>();

#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_106_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FQosRegionInfo_Statics; \
	QOS_API static class UScriptStruct* StaticStruct();


template<> QOS_API UScriptStruct* StaticStruct<struct FQosRegionInfo>();

#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_64_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics; \
	QOS_API static class UScriptStruct* StaticStruct();


template<> QOS_API UScriptStruct* StaticStruct<struct FQosDatacenterInfo>();

#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_48_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FQosPingServerInfo_Statics; \
	QOS_API static class UScriptStruct* StaticStruct();


template<> QOS_API UScriptStruct* StaticStruct<struct FQosPingServerInfo>();

#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUQosRegionManager(); \
	friend struct Z_Construct_UClass_UQosRegionManager_Statics; \
public: \
	DECLARE_CLASS(UQosRegionManager, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Qos"), NO_API) \
	DECLARE_SERIALIZER(UQosRegionManager) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_INCLASS \
private: \
	static void StaticRegisterNativesUQosRegionManager(); \
	friend struct Z_Construct_UClass_UQosRegionManager_Statics; \
public: \
	DECLARE_CLASS(UQosRegionManager, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Qos"), NO_API) \
	DECLARE_SERIALIZER(UQosRegionManager) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UQosRegionManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UQosRegionManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UQosRegionManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UQosRegionManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UQosRegionManager(UQosRegionManager&&); \
	NO_API UQosRegionManager(const UQosRegionManager&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UQosRegionManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UQosRegionManager(UQosRegionManager&&); \
	NO_API UQosRegionManager(const UQosRegionManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UQosRegionManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UQosRegionManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UQosRegionManager)


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__NumTestsPerRegion() { return STRUCT_OFFSET(UQosRegionManager, NumTestsPerRegion); } \
	FORCEINLINE static uint32 __PPO__PingTimeout() { return STRUCT_OFFSET(UQosRegionManager, PingTimeout); } \
	FORCEINLINE static uint32 __PPO__RegionDefinitions() { return STRUCT_OFFSET(UQosRegionManager, RegionDefinitions); } \
	FORCEINLINE static uint32 __PPO__DatacenterDefinitions() { return STRUCT_OFFSET(UQosRegionManager, DatacenterDefinitions); } \
	FORCEINLINE static uint32 __PPO__LastCheckTimestamp() { return STRUCT_OFFSET(UQosRegionManager, LastCheckTimestamp); } \
	FORCEINLINE static uint32 __PPO__Evaluator() { return STRUCT_OFFSET(UQosRegionManager, Evaluator); } \
	FORCEINLINE static uint32 __PPO__QosEvalResult() { return STRUCT_OFFSET(UQosRegionManager, QosEvalResult); } \
	FORCEINLINE static uint32 __PPO__RegionOptions() { return STRUCT_OFFSET(UQosRegionManager, RegionOptions); } \
	FORCEINLINE static uint32 __PPO__ForceRegionId() { return STRUCT_OFFSET(UQosRegionManager, ForceRegionId); } \
	FORCEINLINE static uint32 __PPO__bRegionForcedViaCommandline() { return STRUCT_OFFSET(UQosRegionManager, bRegionForcedViaCommandline); } \
	FORCEINLINE static uint32 __PPO__SelectedRegionId() { return STRUCT_OFFSET(UQosRegionManager, SelectedRegionId); }


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_268_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h_272_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class QosRegionManager."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> QOS_API UClass* StaticClass<class UQosRegionManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Qos_Public_QosRegionManager_h


#define FOREACH_ENUM_EQOSCOMPLETIONRESULT(op) \
	op(EQosCompletionResult::Invalid) \
	op(EQosCompletionResult::Success) \
	op(EQosCompletionResult::Failure) \
	op(EQosCompletionResult::Canceled) 

enum class EQosCompletionResult : uint8;
template<> QOS_API UEnum* StaticEnum<EQosCompletionResult>();

#define FOREACH_ENUM_EQOSDATACENTERRESULT(op) \
	op(EQosDatacenterResult::Invalid) \
	op(EQosDatacenterResult::Success) \
	op(EQosDatacenterResult::Incomplete) 

enum class EQosDatacenterResult : uint8;
template<> QOS_API UEnum* StaticEnum<EQosDatacenterResult>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
