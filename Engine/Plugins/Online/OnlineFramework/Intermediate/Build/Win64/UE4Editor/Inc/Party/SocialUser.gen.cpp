// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/User/SocialUser.h"
#include "Party/Public/SocialToolkit.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialUser() {}
// Cross Module References
	PARTY_API UClass* Z_Construct_UClass_USocialUser_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialUser();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Party();
// End Cross Module References
	void USocialUser::StaticRegisterNativesUSocialUser()
	{
	}
	UClass* Z_Construct_UClass_USocialUser_NoRegister()
	{
		return USocialUser::StaticClass();
	}
	struct Z_Construct_UClass_USocialUser_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialUser_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialUser_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "User/SocialUser.h" },
		{ "ModuleRelativePath", "Public/User/SocialUser.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialUser_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialUser>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialUser_Statics::ClassParams = {
		&USocialUser::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USocialUser_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialUser_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialUser()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialUser_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialUser, 1403136993);
	template<> PARTY_API UClass* StaticClass<USocialUser>()
	{
		return USocialUser::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialUser(Z_Construct_UClass_USocialUser, &USocialUser::StaticClass, TEXT("/Script/Party"), TEXT("USocialUser"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialUser);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
