// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOBBY_LobbyBeaconState_generated_h
#error "LobbyBeaconState.generated.h already included, missing '#pragma once' in LobbyBeaconState.h"
#endif
#define LOBBY_LobbyBeaconState_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_84_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics; \
	LOBBY_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Players() { return STRUCT_OFFSET(FLobbyPlayerStateInfoArray, Players); } \
	FORCEINLINE static uint32 __PPO__ParentState() { return STRUCT_OFFSET(FLobbyPlayerStateInfoArray, ParentState); } \
	typedef FFastArraySerializer Super;


template<> LOBBY_API UScriptStruct* StaticStruct<struct FLobbyPlayerStateInfoArray>();

#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_44_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics; \
	LOBBY_API static class UScriptStruct* StaticStruct(); \
	typedef FFastArraySerializerItem Super;


template<> LOBBY_API UScriptStruct* StaticStruct<struct FLobbyPlayerStateActorInfo>();

#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnRep_WaitForPlayersTimeRemaining); \
	DECLARE_FUNCTION(execOnRep_LobbyStarted);


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnRep_WaitForPlayersTimeRemaining); \
	DECLARE_FUNCTION(execOnRep_LobbyStarted);


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALobbyBeaconState(); \
	friend struct Z_Construct_UClass_ALobbyBeaconState_Statics; \
public: \
	DECLARE_CLASS(ALobbyBeaconState, AInfo, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Lobby"), NO_API) \
	DECLARE_SERIALIZER(ALobbyBeaconState) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \
 \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		bLobbyStarted=NETFIELD_REP_START, \
		WaitForPlayersTimeRemaining, \
		Players, \
		NETFIELD_REP_END=Players	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_INCLASS \
private: \
	static void StaticRegisterNativesALobbyBeaconState(); \
	friend struct Z_Construct_UClass_ALobbyBeaconState_Statics; \
public: \
	DECLARE_CLASS(ALobbyBeaconState, AInfo, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Lobby"), NO_API) \
	DECLARE_SERIALIZER(ALobbyBeaconState) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \
 \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		bLobbyStarted=NETFIELD_REP_START, \
		WaitForPlayersTimeRemaining, \
		Players, \
		NETFIELD_REP_END=Players	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALobbyBeaconState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALobbyBeaconState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALobbyBeaconState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALobbyBeaconState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALobbyBeaconState(ALobbyBeaconState&&); \
	NO_API ALobbyBeaconState(const ALobbyBeaconState&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALobbyBeaconState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALobbyBeaconState(ALobbyBeaconState&&); \
	NO_API ALobbyBeaconState(const ALobbyBeaconState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALobbyBeaconState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALobbyBeaconState); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALobbyBeaconState)


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaxPlayers() { return STRUCT_OFFSET(ALobbyBeaconState, MaxPlayers); } \
	FORCEINLINE static uint32 __PPO__LobbyBeaconPlayerStateClass() { return STRUCT_OFFSET(ALobbyBeaconState, LobbyBeaconPlayerStateClass); } \
	FORCEINLINE static uint32 __PPO__bLobbyStarted() { return STRUCT_OFFSET(ALobbyBeaconState, bLobbyStarted); } \
	FORCEINLINE static uint32 __PPO__WaitForPlayersTimeRemaining() { return STRUCT_OFFSET(ALobbyBeaconState, WaitForPlayersTimeRemaining); } \
	FORCEINLINE static uint32 __PPO__Players() { return STRUCT_OFFSET(ALobbyBeaconState, Players); }


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_172_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h_175_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LobbyBeaconState."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LOBBY_API UClass* StaticClass<class ALobbyBeaconState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconState_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
