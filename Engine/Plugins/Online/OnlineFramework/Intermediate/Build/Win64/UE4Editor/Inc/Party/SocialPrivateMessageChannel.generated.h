// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_SocialPrivateMessageChannel_generated_h
#error "SocialPrivateMessageChannel.generated.h already included, missing '#pragma once' in SocialPrivateMessageChannel.h"
#endif
#define PARTY_SocialPrivateMessageChannel_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSocialPrivateMessageChannel(); \
	friend struct Z_Construct_UClass_USocialPrivateMessageChannel_Statics; \
public: \
	DECLARE_CLASS(USocialPrivateMessageChannel, USocialChatChannel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialPrivateMessageChannel)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSocialPrivateMessageChannel(); \
	friend struct Z_Construct_UClass_USocialPrivateMessageChannel_Statics; \
public: \
	DECLARE_CLASS(USocialPrivateMessageChannel, USocialChatChannel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialPrivateMessageChannel)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialPrivateMessageChannel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialPrivateMessageChannel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialPrivateMessageChannel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialPrivateMessageChannel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialPrivateMessageChannel(USocialPrivateMessageChannel&&); \
	NO_API USocialPrivateMessageChannel(const USocialPrivateMessageChannel&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialPrivateMessageChannel() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialPrivateMessageChannel(USocialPrivateMessageChannel&&); \
	NO_API USocialPrivateMessageChannel(const USocialPrivateMessageChannel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialPrivateMessageChannel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialPrivateMessageChannel); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USocialPrivateMessageChannel)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TargetUser() { return STRUCT_OFFSET(USocialPrivateMessageChannel, TargetUser); }


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_12_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class USocialPrivateMessageChannel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPrivateMessageChannel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
