// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/Chat/SocialPartyChatRoom.h"
#include "Party/Public/Chat/SocialChatManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialPartyChatRoom() {}
// Cross Module References
	PARTY_API UClass* Z_Construct_UClass_USocialPartyChatRoom_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialPartyChatRoom();
	PARTY_API UClass* Z_Construct_UClass_USocialChatRoom();
	UPackage* Z_Construct_UPackage__Script_Party();
// End Cross Module References
	void USocialPartyChatRoom::StaticRegisterNativesUSocialPartyChatRoom()
	{
	}
	UClass* Z_Construct_UClass_USocialPartyChatRoom_NoRegister()
	{
		return USocialPartyChatRoom::StaticClass();
	}
	struct Z_Construct_UClass_USocialPartyChatRoom_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialPartyChatRoom_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USocialChatRoom,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialPartyChatRoom_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A multi-user chat room channel. Used for all chat situations outside of private user-to-user direct messages. */" },
		{ "IncludePath", "Chat/SocialPartyChatRoom.h" },
		{ "ModuleRelativePath", "Public/Chat/SocialPartyChatRoom.h" },
		{ "ToolTip", "A multi-user chat room channel. Used for all chat situations outside of private user-to-user direct messages." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialPartyChatRoom_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialPartyChatRoom>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialPartyChatRoom_Statics::ClassParams = {
		&USocialPartyChatRoom::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USocialPartyChatRoom_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialPartyChatRoom_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialPartyChatRoom()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialPartyChatRoom_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialPartyChatRoom, 1488903370);
	template<> PARTY_API UClass* StaticClass<USocialPartyChatRoom>()
	{
		return USocialPartyChatRoom::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialPartyChatRoom(Z_Construct_UClass_USocialPartyChatRoom, &USocialPartyChatRoom::StaticClass, TEXT("/Script/Party"), TEXT("USocialPartyChatRoom"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialPartyChatRoom);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
