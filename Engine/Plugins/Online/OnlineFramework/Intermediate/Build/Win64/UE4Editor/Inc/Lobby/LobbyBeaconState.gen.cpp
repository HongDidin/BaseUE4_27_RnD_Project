// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Lobby/Public/LobbyBeaconState.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLobbyBeaconState() {}
// Cross Module References
	LOBBY_API UScriptStruct* Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray();
	UPackage* Z_Construct_UPackage__Script_Lobby();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FFastArraySerializer();
	LOBBY_API UScriptStruct* Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo();
	LOBBY_API UClass* Z_Construct_UClass_ALobbyBeaconState_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FFastArraySerializerItem();
	LOBBY_API UClass* Z_Construct_UClass_ALobbyBeaconPlayerState_NoRegister();
	LOBBY_API UClass* Z_Construct_UClass_ALobbyBeaconState();
	ENGINE_API UClass* Z_Construct_UClass_AInfo();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References

static_assert(std::is_polymorphic<FLobbyPlayerStateInfoArray>() == std::is_polymorphic<FFastArraySerializer>(), "USTRUCT FLobbyPlayerStateInfoArray cannot be polymorphic unless super FFastArraySerializer is polymorphic");

class UScriptStruct* FLobbyPlayerStateInfoArray::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LOBBY_API uint32 Get_Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray, Z_Construct_UPackage__Script_Lobby(), TEXT("LobbyPlayerStateInfoArray"), sizeof(FLobbyPlayerStateInfoArray), Get_Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Hash());
	}
	return Singleton;
}
template<> LOBBY_API UScriptStruct* StaticStruct<FLobbyPlayerStateInfoArray>()
{
	return FLobbyPlayerStateInfoArray::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLobbyPlayerStateInfoArray(FLobbyPlayerStateInfoArray::StaticStruct, TEXT("/Script/Lobby"), TEXT("LobbyPlayerStateInfoArray"), false, nullptr, nullptr);
static struct FScriptStruct_Lobby_StaticRegisterNativesFLobbyPlayerStateInfoArray
{
	FScriptStruct_Lobby_StaticRegisterNativesFLobbyPlayerStateInfoArray()
	{
		UScriptStruct::DeferCppStructOps<FLobbyPlayerStateInfoArray>(FName(TEXT("LobbyPlayerStateInfoArray")));
	}
} ScriptStruct_Lobby_StaticRegisterNativesFLobbyPlayerStateInfoArray;
	struct Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Players_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Players_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Players;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentState_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParentState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Struct for fast TArray replication of lobby player state */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Struct for fast TArray replication of lobby player state" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLobbyPlayerStateInfoArray>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_Players_Inner = { "Players", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_Players_MetaData[] = {
		{ "Comment", "/** All of the players in the lobby */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "All of the players in the lobby" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_Players = { "Players", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLobbyPlayerStateInfoArray, Players), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_Players_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_Players_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_ParentState_MetaData[] = {
		{ "Comment", "/** Owning lobby beacon for this array of players */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Owning lobby beacon for this array of players" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_ParentState = { "ParentState", nullptr, (EPropertyFlags)0x0040000080000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLobbyPlayerStateInfoArray, ParentState), Z_Construct_UClass_ALobbyBeaconState_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_ParentState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_ParentState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_Players_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_Players,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::NewProp_ParentState,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Lobby,
		Z_Construct_UScriptStruct_FFastArraySerializer,
		&NewStructOps,
		"LobbyPlayerStateInfoArray",
		sizeof(FLobbyPlayerStateInfoArray),
		alignof(FLobbyPlayerStateInfoArray),
		Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Lobby();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LobbyPlayerStateInfoArray"), sizeof(FLobbyPlayerStateInfoArray), Get_Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray_Hash() { return 608690950U; }

static_assert(std::is_polymorphic<FLobbyPlayerStateActorInfo>() == std::is_polymorphic<FFastArraySerializerItem>(), "USTRUCT FLobbyPlayerStateActorInfo cannot be polymorphic unless super FFastArraySerializerItem is polymorphic");

class UScriptStruct* FLobbyPlayerStateActorInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LOBBY_API uint32 Get_Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo, Z_Construct_UPackage__Script_Lobby(), TEXT("LobbyPlayerStateActorInfo"), sizeof(FLobbyPlayerStateActorInfo), Get_Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Hash());
	}
	return Singleton;
}
template<> LOBBY_API UScriptStruct* StaticStruct<FLobbyPlayerStateActorInfo>()
{
	return FLobbyPlayerStateActorInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLobbyPlayerStateActorInfo(FLobbyPlayerStateActorInfo::StaticStruct, TEXT("/Script/Lobby"), TEXT("LobbyPlayerStateActorInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Lobby_StaticRegisterNativesFLobbyPlayerStateActorInfo
{
	FScriptStruct_Lobby_StaticRegisterNativesFLobbyPlayerStateActorInfo()
	{
		UScriptStruct::DeferCppStructOps<FLobbyPlayerStateActorInfo>(FName(TEXT("LobbyPlayerStateActorInfo")));
	}
} ScriptStruct_Lobby_StaticRegisterNativesFLobbyPlayerStateActorInfo;
	struct Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LobbyPlayerState_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LobbyPlayerState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Replication structure for a single beacon player state\n */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Replication structure for a single beacon player state" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLobbyPlayerStateActorInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::NewProp_LobbyPlayerState_MetaData[] = {
		{ "Comment", "/** Actual player state actor */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Actual player state actor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::NewProp_LobbyPlayerState = { "LobbyPlayerState", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLobbyPlayerStateActorInfo, LobbyPlayerState), Z_Construct_UClass_ALobbyBeaconPlayerState_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::NewProp_LobbyPlayerState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::NewProp_LobbyPlayerState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::NewProp_LobbyPlayerState,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Lobby,
		Z_Construct_UScriptStruct_FFastArraySerializerItem,
		&NewStructOps,
		"LobbyPlayerStateActorInfo",
		sizeof(FLobbyPlayerStateActorInfo),
		alignof(FLobbyPlayerStateActorInfo),
		Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Lobby();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LobbyPlayerStateActorInfo"), sizeof(FLobbyPlayerStateActorInfo), Get_Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLobbyPlayerStateActorInfo_Hash() { return 1282729912U; }
	DEFINE_FUNCTION(ALobbyBeaconState::execOnRep_WaitForPlayersTimeRemaining)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRep_WaitForPlayersTimeRemaining();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALobbyBeaconState::execOnRep_LobbyStarted)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRep_LobbyStarted();
		P_NATIVE_END;
	}
	void ALobbyBeaconState::StaticRegisterNativesALobbyBeaconState()
	{
		UClass* Class = ALobbyBeaconState::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnRep_LobbyStarted", &ALobbyBeaconState::execOnRep_LobbyStarted },
			{ "OnRep_WaitForPlayersTimeRemaining", &ALobbyBeaconState::execOnRep_WaitForPlayersTimeRemaining },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ALobbyBeaconState_OnRep_LobbyStarted_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALobbyBeaconState_OnRep_LobbyStarted_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Handle the lobby starting */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Handle the lobby starting" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALobbyBeaconState_OnRep_LobbyStarted_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALobbyBeaconState, nullptr, "OnRep_LobbyStarted", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALobbyBeaconState_OnRep_LobbyStarted_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALobbyBeaconState_OnRep_LobbyStarted_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALobbyBeaconState_OnRep_LobbyStarted()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALobbyBeaconState_OnRep_LobbyStarted_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALobbyBeaconState_OnRep_WaitForPlayersTimeRemaining_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALobbyBeaconState_OnRep_WaitForPlayersTimeRemaining_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Handle notification of time left to wait for lobby to start */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Handle notification of time left to wait for lobby to start" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALobbyBeaconState_OnRep_WaitForPlayersTimeRemaining_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALobbyBeaconState, nullptr, "OnRep_WaitForPlayersTimeRemaining", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALobbyBeaconState_OnRep_WaitForPlayersTimeRemaining_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALobbyBeaconState_OnRep_WaitForPlayersTimeRemaining_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALobbyBeaconState_OnRep_WaitForPlayersTimeRemaining()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALobbyBeaconState_OnRep_WaitForPlayersTimeRemaining_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ALobbyBeaconState_NoRegister()
	{
		return ALobbyBeaconState::StaticClass();
	}
	struct Z_Construct_UClass_ALobbyBeaconState_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxPlayers_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxPlayers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LobbyBeaconPlayerStateClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_LobbyBeaconPlayerStateClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLobbyStarted_MetaData[];
#endif
		static void NewProp_bLobbyStarted_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLobbyStarted;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaitForPlayersTimeRemaining_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaitForPlayersTimeRemaining;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Players_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Players;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALobbyBeaconState_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AInfo,
		(UObject* (*)())Z_Construct_UPackage__Script_Lobby,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ALobbyBeaconState_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ALobbyBeaconState_OnRep_LobbyStarted, "OnRep_LobbyStarted" }, // 2175068572
		{ &Z_Construct_UFunction_ALobbyBeaconState_OnRep_WaitForPlayersTimeRemaining, "OnRep_WaitForPlayersTimeRemaining" }, // 215545620
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALobbyBeaconState_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Shared state of the game from the lobby perspective\n * Duplicates much of the data in the traditional AGameState object for sharing with players\n * connected via beacon only\n */" },
		{ "HideCategories", "Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "LobbyBeaconState.h" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
		{ "ToolTip", "Shared state of the game from the lobby perspective\nDuplicates much of the data in the traditional AGameState object for sharing with players\nconnected via beacon only" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_MaxPlayers_MetaData[] = {
		{ "Comment", "/** Total number of players allowed in the lobby */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Total number of players allowed in the lobby" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_MaxPlayers = { "MaxPlayers", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALobbyBeaconState, MaxPlayers), METADATA_PARAMS(Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_MaxPlayers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_MaxPlayers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_LobbyBeaconPlayerStateClass_MetaData[] = {
		{ "Comment", "/** Class to use for lobby beacon player states */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Class to use for lobby beacon player states" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_LobbyBeaconPlayerStateClass = { "LobbyBeaconPlayerStateClass", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALobbyBeaconState, LobbyBeaconPlayerStateClass), Z_Construct_UClass_ALobbyBeaconPlayerState_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_LobbyBeaconPlayerStateClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_LobbyBeaconPlayerStateClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_bLobbyStarted_MetaData[] = {
		{ "Comment", "/** Has the lobby already been started */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Has the lobby already been started" },
	};
#endif
	void Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_bLobbyStarted_SetBit(void* Obj)
	{
		((ALobbyBeaconState*)Obj)->bLobbyStarted = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_bLobbyStarted = { "bLobbyStarted", "OnRep_LobbyStarted", (EPropertyFlags)0x0020080100000020, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ALobbyBeaconState), &Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_bLobbyStarted_SetBit, METADATA_PARAMS(Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_bLobbyStarted_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_bLobbyStarted_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_WaitForPlayersTimeRemaining_MetaData[] = {
		{ "Comment", "/** Amount of time waiting for other players before starting the lobby */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Amount of time waiting for other players before starting the lobby" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_WaitForPlayersTimeRemaining = { "WaitForPlayersTimeRemaining", "OnRep_WaitForPlayersTimeRemaining", (EPropertyFlags)0x0020080100004020, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALobbyBeaconState, WaitForPlayersTimeRemaining), METADATA_PARAMS(Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_WaitForPlayersTimeRemaining_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_WaitForPlayersTimeRemaining_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_Players_MetaData[] = {
		{ "Comment", "/** Array of players currently in the game, lobby or otherwise */" },
		{ "ModuleRelativePath", "Public/LobbyBeaconState.h" },
		{ "ToolTip", "Array of players currently in the game, lobby or otherwise" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_Players = { "Players", nullptr, (EPropertyFlags)0x0020080000000020, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALobbyBeaconState, Players), Z_Construct_UScriptStruct_FLobbyPlayerStateInfoArray, METADATA_PARAMS(Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_Players_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_Players_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ALobbyBeaconState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_MaxPlayers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_LobbyBeaconPlayerStateClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_bLobbyStarted,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_WaitForPlayersTimeRemaining,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALobbyBeaconState_Statics::NewProp_Players,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALobbyBeaconState_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALobbyBeaconState>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALobbyBeaconState_Statics::ClassParams = {
		&ALobbyBeaconState::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ALobbyBeaconState_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconState_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ALobbyBeaconState_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALobbyBeaconState_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALobbyBeaconState()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALobbyBeaconState_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALobbyBeaconState, 3111054987);
	template<> LOBBY_API UClass* StaticClass<ALobbyBeaconState>()
	{
		return ALobbyBeaconState::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALobbyBeaconState(Z_Construct_UClass_ALobbyBeaconState, &ALobbyBeaconState::StaticClass, TEXT("/Script/Lobby"), TEXT("ALobbyBeaconState"), false, nullptr, nullptr, nullptr);

	void ALobbyBeaconState::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_bLobbyStarted(TEXT("bLobbyStarted"));
		static const FName Name_WaitForPlayersTimeRemaining(TEXT("WaitForPlayersTimeRemaining"));
		static const FName Name_Players(TEXT("Players"));

		const bool bIsValid = true
			&& Name_bLobbyStarted == ClassReps[(int32)ENetFields_Private::bLobbyStarted].Property->GetFName()
			&& Name_WaitForPlayersTimeRemaining == ClassReps[(int32)ENetFields_Private::WaitForPlayersTimeRemaining].Property->GetFName()
			&& Name_Players == ClassReps[(int32)ENetFields_Private::Players].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in ALobbyBeaconState"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALobbyBeaconState);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
