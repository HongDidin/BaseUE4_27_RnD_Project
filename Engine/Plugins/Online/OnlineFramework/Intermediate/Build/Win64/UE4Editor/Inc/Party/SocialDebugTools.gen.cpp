// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/SocialDebugTools.h"
#include "Party/Public/SocialManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialDebugTools() {}
// Cross Module References
	PARTY_API UClass* Z_Construct_UClass_USocialDebugTools_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialDebugTools();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Party();
// End Cross Module References
	void USocialDebugTools::StaticRegisterNativesUSocialDebugTools()
	{
	}
	UClass* Z_Construct_UClass_USocialDebugTools_NoRegister()
	{
		return USocialDebugTools::StaticClass();
	}
	struct Z_Construct_UClass_USocialDebugTools_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialDebugTools_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialDebugTools_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SocialDebugTools.h" },
		{ "ModuleRelativePath", "Public/SocialDebugTools.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialDebugTools_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialDebugTools>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialDebugTools_Statics::ClassParams = {
		&USocialDebugTools::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USocialDebugTools_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialDebugTools_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialDebugTools()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialDebugTools_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialDebugTools, 1143758294);
	template<> PARTY_API UClass* StaticClass<USocialDebugTools>()
	{
		return USocialDebugTools::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialDebugTools(Z_Construct_UClass_USocialDebugTools, &USocialDebugTools::StaticClass, TEXT("/Script/Party"), TEXT("USocialDebugTools"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialDebugTools);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
