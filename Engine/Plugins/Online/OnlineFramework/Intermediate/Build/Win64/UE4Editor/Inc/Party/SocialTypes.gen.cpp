// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/SocialTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialTypes() {}
// Cross Module References
	PARTY_API UEnum* Z_Construct_UEnum_Party_EPlatformIconDisplayRule();
	UPackage* Z_Construct_UPackage__Script_Party();
	PARTY_API UEnum* Z_Construct_UEnum_Party_ECrossplayPreference();
	PARTY_API UEnum* Z_Construct_UEnum_Party_ESocialRelationship();
	PARTY_API UEnum* Z_Construct_UEnum_Party_ESocialSubsystem();
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FUserPlatform();
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FSocialPlatformDescription();
// End Cross Module References
	static UEnum* EPlatformIconDisplayRule_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Party_EPlatformIconDisplayRule, Z_Construct_UPackage__Script_Party(), TEXT("EPlatformIconDisplayRule"));
		}
		return Singleton;
	}
	template<> PARTY_API UEnum* StaticEnum<EPlatformIconDisplayRule>()
	{
		return EPlatformIconDisplayRule_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPlatformIconDisplayRule(EPlatformIconDisplayRule_StaticEnum, TEXT("/Script/Party"), TEXT("EPlatformIconDisplayRule"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Party_EPlatformIconDisplayRule_Hash() { return 3854239033U; }
	UEnum* Z_Construct_UEnum_Party_EPlatformIconDisplayRule()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPlatformIconDisplayRule"), 0, Get_Z_Construct_UEnum_Party_EPlatformIconDisplayRule_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPlatformIconDisplayRule::Always", (int64)EPlatformIconDisplayRule::Always },
				{ "EPlatformIconDisplayRule::AlwaysIfDifferent", (int64)EPlatformIconDisplayRule::AlwaysIfDifferent },
				{ "EPlatformIconDisplayRule::AlwaysWhenInCrossplayParty", (int64)EPlatformIconDisplayRule::AlwaysWhenInCrossplayParty },
				{ "EPlatformIconDisplayRule::AlwaysIfDifferentWhenInCrossplayParty", (int64)EPlatformIconDisplayRule::AlwaysIfDifferentWhenInCrossplayParty },
				{ "EPlatformIconDisplayRule::Never", (int64)EPlatformIconDisplayRule::Never },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Always.Name", "EPlatformIconDisplayRule::Always" },
				{ "AlwaysIfDifferent.Comment", "// always show the platform icon\n" },
				{ "AlwaysIfDifferent.Name", "EPlatformIconDisplayRule::AlwaysIfDifferent" },
				{ "AlwaysIfDifferent.ToolTip", "always show the platform icon" },
				{ "AlwaysIfDifferentWhenInCrossplayParty.Comment", "// always show the icon if I'm in a crossplay party\n" },
				{ "AlwaysIfDifferentWhenInCrossplayParty.Name", "EPlatformIconDisplayRule::AlwaysIfDifferentWhenInCrossplayParty" },
				{ "AlwaysIfDifferentWhenInCrossplayParty.ToolTip", "always show the icon if I'm in a crossplay party" },
				{ "AlwaysWhenInCrossplayParty.Comment", "// always show the icon if it's a different platform from my own\n" },
				{ "AlwaysWhenInCrossplayParty.Name", "EPlatformIconDisplayRule::AlwaysWhenInCrossplayParty" },
				{ "AlwaysWhenInCrossplayParty.ToolTip", "always show the icon if it's a different platform from my own" },
				{ "ModuleRelativePath", "Public/SocialTypes.h" },
				{ "Never.Comment", "// only show the icon if it's different from my own and I'm in a crossplay party\n" },
				{ "Never.Name", "EPlatformIconDisplayRule::Never" },
				{ "Never.ToolTip", "only show the icon if it's different from my own and I'm in a crossplay party" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Party,
				nullptr,
				"EPlatformIconDisplayRule",
				"EPlatformIconDisplayRule",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ECrossplayPreference_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Party_ECrossplayPreference, Z_Construct_UPackage__Script_Party(), TEXT("ECrossplayPreference"));
		}
		return Singleton;
	}
	template<> PARTY_API UEnum* StaticEnum<ECrossplayPreference>()
	{
		return ECrossplayPreference_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECrossplayPreference(ECrossplayPreference_StaticEnum, TEXT("/Script/Party"), TEXT("ECrossplayPreference"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Party_ECrossplayPreference_Hash() { return 4237401445U; }
	UEnum* Z_Construct_UEnum_Party_ECrossplayPreference()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECrossplayPreference"), 0, Get_Z_Construct_UEnum_Party_ECrossplayPreference_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECrossplayPreference::NoSelection", (int64)ECrossplayPreference::NoSelection },
				{ "ECrossplayPreference::OptedIn", (int64)ECrossplayPreference::OptedIn },
				{ "ECrossplayPreference::OptedOut", (int64)ECrossplayPreference::OptedOut },
				{ "ECrossplayPreference::OptedOutRestricted", (int64)ECrossplayPreference::OptedOutRestricted },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/SocialTypes.h" },
				{ "NoSelection.Name", "ECrossplayPreference::NoSelection" },
				{ "OptedIn.Name", "ECrossplayPreference::OptedIn" },
				{ "OptedOut.Name", "ECrossplayPreference::OptedOut" },
				{ "OptedOutRestricted.Name", "ECrossplayPreference::OptedOutRestricted" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Party,
				nullptr,
				"ECrossplayPreference",
				"ECrossplayPreference",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ESocialRelationship_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Party_ESocialRelationship, Z_Construct_UPackage__Script_Party(), TEXT("ESocialRelationship"));
		}
		return Singleton;
	}
	template<> PARTY_API UEnum* StaticEnum<ESocialRelationship>()
	{
		return ESocialRelationship_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESocialRelationship(ESocialRelationship_StaticEnum, TEXT("/Script/Party"), TEXT("ESocialRelationship"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Party_ESocialRelationship_Hash() { return 3369038831U; }
	UEnum* Z_Construct_UEnum_Party_ESocialRelationship()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESocialRelationship"), 0, Get_Z_Construct_UEnum_Party_ESocialRelationship_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESocialRelationship::Any", (int64)ESocialRelationship::Any },
				{ "ESocialRelationship::FriendInviteReceived", (int64)ESocialRelationship::FriendInviteReceived },
				{ "ESocialRelationship::FriendInviteSent", (int64)ESocialRelationship::FriendInviteSent },
				{ "ESocialRelationship::PartyInvite", (int64)ESocialRelationship::PartyInvite },
				{ "ESocialRelationship::Friend", (int64)ESocialRelationship::Friend },
				{ "ESocialRelationship::BlockedPlayer", (int64)ESocialRelationship::BlockedPlayer },
				{ "ESocialRelationship::SuggestedFriend", (int64)ESocialRelationship::SuggestedFriend },
				{ "ESocialRelationship::RecentPlayer", (int64)ESocialRelationship::RecentPlayer },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Any.Name", "ESocialRelationship::Any" },
				{ "BlockedPlayer.Name", "ESocialRelationship::BlockedPlayer" },
				{ "Comment", "/** Per-OSS relationship types */" },
				{ "Friend.Name", "ESocialRelationship::Friend" },
				{ "FriendInviteReceived.Name", "ESocialRelationship::FriendInviteReceived" },
				{ "FriendInviteSent.Name", "ESocialRelationship::FriendInviteSent" },
				{ "ModuleRelativePath", "Public/SocialTypes.h" },
				{ "PartyInvite.Name", "ESocialRelationship::PartyInvite" },
				{ "RecentPlayer.Name", "ESocialRelationship::RecentPlayer" },
				{ "SuggestedFriend.Name", "ESocialRelationship::SuggestedFriend" },
				{ "ToolTip", "Per-OSS relationship types" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Party,
				nullptr,
				"ESocialRelationship",
				"ESocialRelationship",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ESocialSubsystem_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Party_ESocialSubsystem, Z_Construct_UPackage__Script_Party(), TEXT("ESocialSubsystem"));
		}
		return Singleton;
	}
	template<> PARTY_API UEnum* StaticEnum<ESocialSubsystem>()
	{
		return ESocialSubsystem_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESocialSubsystem(ESocialSubsystem_StaticEnum, TEXT("/Script/Party"), TEXT("ESocialSubsystem"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Party_ESocialSubsystem_Hash() { return 3104037263U; }
	UEnum* Z_Construct_UEnum_Party_ESocialSubsystem()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESocialSubsystem"), 0, Get_Z_Construct_UEnum_Party_ESocialSubsystem_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESocialSubsystem::Primary", (int64)ESocialSubsystem::Primary },
				{ "ESocialSubsystem::Platform", (int64)ESocialSubsystem::Platform },
				{ "ESocialSubsystem::MAX", (int64)ESocialSubsystem::MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** All supported subsystems  */" },
				{ "MAX.Comment", "/*Facebook,\n\x09Google,\n\x09Twitch,*/" },
				{ "MAX.Name", "ESocialSubsystem::MAX" },
				{ "MAX.ToolTip", "Facebook,\n       Google,\n       Twitch," },
				{ "ModuleRelativePath", "Public/SocialTypes.h" },
				{ "Platform.Comment", "// OSS specific to the platform on which we're running (PSN, XBL, GameCenter, etc.)\n" },
				{ "Platform.Name", "ESocialSubsystem::Platform" },
				{ "Platform.ToolTip", "OSS specific to the platform on which we're running (PSN, XBL, GameCenter, etc.)" },
				{ "Primary.Comment", "// Publisher-level cross-platform OSS\n" },
				{ "Primary.Name", "ESocialSubsystem::Primary" },
				{ "Primary.ToolTip", "Publisher-level cross-platform OSS" },
				{ "ToolTip", "All supported subsystems" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Party,
				nullptr,
				"ESocialSubsystem",
				"ESocialSubsystem",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FUserPlatform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PARTY_API uint32 Get_Z_Construct_UScriptStruct_FUserPlatform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUserPlatform, Z_Construct_UPackage__Script_Party(), TEXT("UserPlatform"), sizeof(FUserPlatform), Get_Z_Construct_UScriptStruct_FUserPlatform_Hash());
	}
	return Singleton;
}
template<> PARTY_API UScriptStruct* StaticStruct<FUserPlatform>()
{
	return FUserPlatform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUserPlatform(FUserPlatform::StaticStruct, TEXT("/Script/Party"), TEXT("UserPlatform"), false, nullptr, nullptr);
static struct FScriptStruct_Party_StaticRegisterNativesFUserPlatform
{
	FScriptStruct_Party_StaticRegisterNativesFUserPlatform()
	{
		UScriptStruct::DeferCppStructOps<FUserPlatform>(FName(TEXT("UserPlatform")));
	}
} ScriptStruct_Party_StaticRegisterNativesFUserPlatform;
	struct Z_Construct_UScriptStruct_FUserPlatform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlatformDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PlatformDescription;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUserPlatform_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Thin wrapper to infuse a raw platform string with some meaning */" },
		{ "ModuleRelativePath", "Public/SocialTypes.h" },
		{ "ToolTip", "Thin wrapper to infuse a raw platform string with some meaning" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUserPlatform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUserPlatform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUserPlatform_Statics::NewProp_PlatformDescription_MetaData[] = {
		{ "ModuleRelativePath", "Public/SocialTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUserPlatform_Statics::NewProp_PlatformDescription = { "PlatformDescription", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUserPlatform, PlatformDescription), Z_Construct_UScriptStruct_FSocialPlatformDescription, METADATA_PARAMS(Z_Construct_UScriptStruct_FUserPlatform_Statics::NewProp_PlatformDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUserPlatform_Statics::NewProp_PlatformDescription_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUserPlatform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUserPlatform_Statics::NewProp_PlatformDescription,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUserPlatform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
		nullptr,
		&NewStructOps,
		"UserPlatform",
		sizeof(FUserPlatform),
		alignof(FUserPlatform),
		Z_Construct_UScriptStruct_FUserPlatform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUserPlatform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUserPlatform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUserPlatform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUserPlatform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUserPlatform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UserPlatform"), sizeof(FUserPlatform), Get_Z_Construct_UScriptStruct_FUserPlatform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUserPlatform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUserPlatform_Hash() { return 1569853091U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
