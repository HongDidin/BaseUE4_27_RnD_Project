// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/Party/SocialParty.h"
#include "Party/Public/SocialManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialParty() {}
// Cross Module References
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FPartyRepData();
	UPackage* Z_Construct_UPackage__Script_Party();
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FOnlinePartyRepDataBase();
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FPartyPrivacySettings();
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FPartyPlatformSessionInfo();
	PARTY_API UClass* Z_Construct_UClass_USocialParty_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialParty();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ONLINESUBSYSTEMUTILS_API UClass* Z_Construct_UClass_APartyBeaconClient_NoRegister();
	ONLINESUBSYSTEMUTILS_API UClass* Z_Construct_UClass_ASpectatorBeaconClient_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FUniqueNetIdRepl();
	PARTY_API UClass* Z_Construct_UClass_UPartyMember_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FPartyRepData>() == std::is_polymorphic<FOnlinePartyRepDataBase>(), "USTRUCT FPartyRepData cannot be polymorphic unless super FOnlinePartyRepDataBase is polymorphic");

class UScriptStruct* FPartyRepData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PARTY_API uint32 Get_Z_Construct_UScriptStruct_FPartyRepData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPartyRepData, Z_Construct_UPackage__Script_Party(), TEXT("PartyRepData"), sizeof(FPartyRepData), Get_Z_Construct_UScriptStruct_FPartyRepData_Hash());
	}
	return Singleton;
}
template<> PARTY_API UScriptStruct* StaticStruct<FPartyRepData>()
{
	return FPartyRepData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPartyRepData(FPartyRepData::StaticStruct, TEXT("/Script/Party"), TEXT("PartyRepData"), false, nullptr, nullptr);
static struct FScriptStruct_Party_StaticRegisterNativesFPartyRepData
{
	FScriptStruct_Party_StaticRegisterNativesFPartyRepData()
	{
		UScriptStruct::DeferCppStructOps<FPartyRepData>(FName(TEXT("PartyRepData")));
	}
} ScriptStruct_Party_StaticRegisterNativesFPartyRepData;
	struct Z_Construct_UScriptStruct_FPartyRepData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrivacySettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrivacySettings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PlatformSessions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlatformSessions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PlatformSessions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyRepData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Base struct used to replicate data about the state of the party to all members. */" },
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
		{ "ToolTip", "Base struct used to replicate data about the state of the party to all members." },
	};
#endif
	void* Z_Construct_UScriptStruct_FPartyRepData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPartyRepData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PrivacySettings_MetaData[] = {
		{ "Comment", "/** The privacy settings for the party */" },
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
		{ "ToolTip", "The privacy settings for the party" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PrivacySettings = { "PrivacySettings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyRepData, PrivacySettings), Z_Construct_UScriptStruct_FPartyPrivacySettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PrivacySettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PrivacySettings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PlatformSessions_Inner = { "PlatformSessions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPartyPlatformSessionInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PlatformSessions_MetaData[] = {
		{ "Comment", "/** List of platform sessions for the party. Includes one entry per platform that needs a session and has a member of that session. */" },
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
		{ "ToolTip", "List of platform sessions for the party. Includes one entry per platform that needs a session and has a member of that session." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PlatformSessions = { "PlatformSessions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyRepData, PlatformSessions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PlatformSessions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PlatformSessions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPartyRepData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PrivacySettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PlatformSessions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyRepData_Statics::NewProp_PlatformSessions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPartyRepData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
		Z_Construct_UScriptStruct_FOnlinePartyRepDataBase,
		&NewStructOps,
		"PartyRepData",
		sizeof(FPartyRepData),
		alignof(FPartyRepData),
		Z_Construct_UScriptStruct_FPartyRepData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyRepData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyRepData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyRepData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPartyRepData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPartyRepData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PartyRepData"), sizeof(FPartyRepData), Get_Z_Construct_UScriptStruct_FPartyRepData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPartyRepData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPartyRepData_Hash() { return 3284212589U; }
	void USocialParty::StaticRegisterNativesUSocialParty()
	{
	}
	UClass* Z_Construct_UClass_USocialParty_NoRegister()
	{
		return USocialParty::StaticClass();
	}
	struct Z_Construct_UClass_USocialParty_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReservationBeaconClientClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ReservationBeaconClientClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpectatorBeaconClientClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpectatorBeaconClientClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwningLocalUserId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OwningLocalUserId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentLeaderId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentLeaderId;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PartyMembersById_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PartyMembersById_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PartyMembersById_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PartyMembersById;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableAutomaticPartyRejoin_MetaData[];
#endif
		static void NewProp_bEnableAutomaticPartyRejoin_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableAutomaticPartyRejoin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReservationBeaconClient_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReservationBeaconClient;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpectatorBeaconClient_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpectatorBeaconClient;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialParty_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialParty_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Party game state that contains all information relevant to the communication within a party\n * Keeps all players in sync with the state of the party and its individual members\n */" },
		{ "IncludePath", "Party/SocialParty.h" },
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
		{ "ToolTip", "Party game state that contains all information relevant to the communication within a party\nKeeps all players in sync with the state of the party and its individual members" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialParty_Statics::NewProp_ReservationBeaconClientClass_MetaData[] = {
		{ "Comment", "/** Reservation beacon class for getting server approval for new party members while in a game */" },
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
		{ "ToolTip", "Reservation beacon class for getting server approval for new party members while in a game" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_USocialParty_Statics::NewProp_ReservationBeaconClientClass = { "ReservationBeaconClientClass", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialParty, ReservationBeaconClientClass), Z_Construct_UClass_APartyBeaconClient_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_USocialParty_Statics::NewProp_ReservationBeaconClientClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialParty_Statics::NewProp_ReservationBeaconClientClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialParty_Statics::NewProp_SpectatorBeaconClientClass_MetaData[] = {
		{ "Comment", "/** Spectator beacon class for getting server approval for new spectators while in a game */" },
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
		{ "ToolTip", "Spectator beacon class for getting server approval for new spectators while in a game" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_USocialParty_Statics::NewProp_SpectatorBeaconClientClass = { "SpectatorBeaconClientClass", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialParty, SpectatorBeaconClientClass), Z_Construct_UClass_ASpectatorBeaconClient_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_USocialParty_Statics::NewProp_SpectatorBeaconClientClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialParty_Statics::NewProp_SpectatorBeaconClientClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialParty_Statics::NewProp_OwningLocalUserId_MetaData[] = {
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USocialParty_Statics::NewProp_OwningLocalUserId = { "OwningLocalUserId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialParty, OwningLocalUserId), Z_Construct_UScriptStruct_FUniqueNetIdRepl, METADATA_PARAMS(Z_Construct_UClass_USocialParty_Statics::NewProp_OwningLocalUserId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialParty_Statics::NewProp_OwningLocalUserId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialParty_Statics::NewProp_CurrentLeaderId_MetaData[] = {
		{ "Comment", "/** Tracked explicitly so we know which player was demoted whenever the leader changes */" },
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
		{ "ToolTip", "Tracked explicitly so we know which player was demoted whenever the leader changes" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USocialParty_Statics::NewProp_CurrentLeaderId = { "CurrentLeaderId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialParty, CurrentLeaderId), Z_Construct_UScriptStruct_FUniqueNetIdRepl, METADATA_PARAMS(Z_Construct_UClass_USocialParty_Statics::NewProp_CurrentLeaderId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialParty_Statics::NewProp_CurrentLeaderId_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialParty_Statics::NewProp_PartyMembersById_ValueProp = { "PartyMembersById", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UPartyMember_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USocialParty_Statics::NewProp_PartyMembersById_Key_KeyProp = { "PartyMembersById_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FUniqueNetIdRepl, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialParty_Statics::NewProp_PartyMembersById_MetaData[] = {
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_USocialParty_Statics::NewProp_PartyMembersById = { "PartyMembersById", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialParty, PartyMembersById), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USocialParty_Statics::NewProp_PartyMembersById_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialParty_Statics::NewProp_PartyMembersById_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialParty_Statics::NewProp_bEnableAutomaticPartyRejoin_MetaData[] = {
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
	};
#endif
	void Z_Construct_UClass_USocialParty_Statics::NewProp_bEnableAutomaticPartyRejoin_SetBit(void* Obj)
	{
		((USocialParty*)Obj)->bEnableAutomaticPartyRejoin = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USocialParty_Statics::NewProp_bEnableAutomaticPartyRejoin = { "bEnableAutomaticPartyRejoin", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USocialParty), &Z_Construct_UClass_USocialParty_Statics::NewProp_bEnableAutomaticPartyRejoin_SetBit, METADATA_PARAMS(Z_Construct_UClass_USocialParty_Statics::NewProp_bEnableAutomaticPartyRejoin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialParty_Statics::NewProp_bEnableAutomaticPartyRejoin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialParty_Statics::NewProp_ReservationBeaconClient_MetaData[] = {
		{ "Comment", "/** Reservation beacon client instance while getting approval for new party members*/" },
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
		{ "ToolTip", "Reservation beacon client instance while getting approval for new party members" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialParty_Statics::NewProp_ReservationBeaconClient = { "ReservationBeaconClient", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialParty, ReservationBeaconClient), Z_Construct_UClass_APartyBeaconClient_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USocialParty_Statics::NewProp_ReservationBeaconClient_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialParty_Statics::NewProp_ReservationBeaconClient_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialParty_Statics::NewProp_SpectatorBeaconClient_MetaData[] = {
		{ "Comment", "/** Spectator beacon client instance while getting approval for spectator*/" },
		{ "ModuleRelativePath", "Public/Party/SocialParty.h" },
		{ "ToolTip", "Spectator beacon client instance while getting approval for spectator" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialParty_Statics::NewProp_SpectatorBeaconClient = { "SpectatorBeaconClient", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialParty, SpectatorBeaconClient), Z_Construct_UClass_ASpectatorBeaconClient_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USocialParty_Statics::NewProp_SpectatorBeaconClient_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialParty_Statics::NewProp_SpectatorBeaconClient_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USocialParty_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialParty_Statics::NewProp_ReservationBeaconClientClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialParty_Statics::NewProp_SpectatorBeaconClientClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialParty_Statics::NewProp_OwningLocalUserId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialParty_Statics::NewProp_CurrentLeaderId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialParty_Statics::NewProp_PartyMembersById_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialParty_Statics::NewProp_PartyMembersById_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialParty_Statics::NewProp_PartyMembersById,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialParty_Statics::NewProp_bEnableAutomaticPartyRejoin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialParty_Statics::NewProp_ReservationBeaconClient,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialParty_Statics::NewProp_SpectatorBeaconClient,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialParty_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialParty>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialParty_Statics::ClassParams = {
		&USocialParty::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USocialParty_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USocialParty_Statics::PropPointers),
		0,
		0x001000ADu,
		METADATA_PARAMS(Z_Construct_UClass_USocialParty_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialParty_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialParty()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialParty_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialParty, 794519265);
	template<> PARTY_API UClass* StaticClass<USocialParty>()
	{
		return USocialParty::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialParty(Z_Construct_UClass_USocialParty, &USocialParty::StaticClass, TEXT("/Script/Party"), TEXT("USocialParty"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialParty);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
