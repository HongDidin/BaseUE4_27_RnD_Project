// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_SocialUser_generated_h
#error "SocialUser.generated.h already included, missing '#pragma once' in SocialUser.h"
#endif
#define PARTY_SocialUser_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSocialUser(); \
	friend struct Z_Construct_UClass_USocialUser_Statics; \
public: \
	DECLARE_CLASS(USocialUser, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialUser) \
	DECLARE_WITHIN(USocialToolkit)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUSocialUser(); \
	friend struct Z_Construct_UClass_USocialUser_Statics; \
public: \
	DECLARE_CLASS(USocialUser, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialUser) \
	DECLARE_WITHIN(USocialToolkit)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialUser(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialUser) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialUser); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialUser); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialUser(USocialUser&&); \
	NO_API USocialUser(const USocialUser&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialUser(USocialUser&&); \
	NO_API USocialUser(const USocialUser&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialUser); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialUser); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USocialUser)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_27_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h_30_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class USocialUser>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_User_SocialUser_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
