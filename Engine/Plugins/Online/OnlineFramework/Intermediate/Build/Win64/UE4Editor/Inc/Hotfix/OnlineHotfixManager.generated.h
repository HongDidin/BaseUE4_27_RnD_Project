// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOTFIX_OnlineHotfixManager_generated_h
#error "OnlineHotfixManager.generated.h already included, missing '#pragma once' in OnlineHotfixManager.h"
#endif
#define HOTFIX_OnlineHotfixManager_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execStartHotfixProcess);


#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execStartHotfixProcess);


#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOnlineHotfixManager(); \
	friend struct Z_Construct_UClass_UOnlineHotfixManager_Statics; \
public: \
	DECLARE_CLASS(UOnlineHotfixManager, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Hotfix"), NO_API) \
	DECLARE_SERIALIZER(UOnlineHotfixManager) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_INCLASS \
private: \
	static void StaticRegisterNativesUOnlineHotfixManager(); \
	friend struct Z_Construct_UClass_UOnlineHotfixManager_Statics; \
public: \
	DECLARE_CLASS(UOnlineHotfixManager, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Hotfix"), NO_API) \
	DECLARE_SERIALIZER(UOnlineHotfixManager) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOnlineHotfixManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOnlineHotfixManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOnlineHotfixManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOnlineHotfixManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOnlineHotfixManager(UOnlineHotfixManager&&); \
	NO_API UOnlineHotfixManager(const UOnlineHotfixManager&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOnlineHotfixManager(UOnlineHotfixManager&&); \
	NO_API UOnlineHotfixManager(const UOnlineHotfixManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOnlineHotfixManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOnlineHotfixManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOnlineHotfixManager)


#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_74_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h_78_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOTFIX_API UClass* StaticClass<class UOnlineHotfixManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_OnlineHotfixManager_h


#define FOREACH_ENUM_EHOTFIXRESULT(op) \
	op(EHotfixResult::Failed) \
	op(EHotfixResult::Success) \
	op(EHotfixResult::SuccessNoChange) \
	op(EHotfixResult::SuccessNeedsReload) \
	op(EHotfixResult::SuccessNeedsRelaunch) 

enum class EHotfixResult : uint8;
template<> HOTFIX_API UEnum* StaticEnum<EHotfixResult>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
