// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOBBY_LobbyBeaconPlayerState_generated_h
#error "LobbyBeaconPlayerState.generated.h already included, missing '#pragma once' in LobbyBeaconPlayerState.h"
#endif
#define LOBBY_LobbyBeaconPlayerState_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnRep_InLobby); \
	DECLARE_FUNCTION(execOnRep_PartyOwner); \
	DECLARE_FUNCTION(execOnRep_UniqueId);


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnRep_InLobby); \
	DECLARE_FUNCTION(execOnRep_PartyOwner); \
	DECLARE_FUNCTION(execOnRep_UniqueId);


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALobbyBeaconPlayerState(); \
	friend struct Z_Construct_UClass_ALobbyBeaconPlayerState_Statics; \
public: \
	DECLARE_CLASS(ALobbyBeaconPlayerState, AInfo, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Lobby"), NO_API) \
	DECLARE_SERIALIZER(ALobbyBeaconPlayerState) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \
 \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		DisplayName=NETFIELD_REP_START, \
		UniqueId, \
		PartyOwnerUniqueId, \
		bInLobby, \
		ClientActor, \
		NETFIELD_REP_END=ClientActor	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_INCLASS \
private: \
	static void StaticRegisterNativesALobbyBeaconPlayerState(); \
	friend struct Z_Construct_UClass_ALobbyBeaconPlayerState_Statics; \
public: \
	DECLARE_CLASS(ALobbyBeaconPlayerState, AInfo, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Lobby"), NO_API) \
	DECLARE_SERIALIZER(ALobbyBeaconPlayerState) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \
 \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		DisplayName=NETFIELD_REP_START, \
		UniqueId, \
		PartyOwnerUniqueId, \
		bInLobby, \
		ClientActor, \
		NETFIELD_REP_END=ClientActor	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALobbyBeaconPlayerState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALobbyBeaconPlayerState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALobbyBeaconPlayerState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALobbyBeaconPlayerState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALobbyBeaconPlayerState(ALobbyBeaconPlayerState&&); \
	NO_API ALobbyBeaconPlayerState(const ALobbyBeaconPlayerState&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALobbyBeaconPlayerState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALobbyBeaconPlayerState(ALobbyBeaconPlayerState&&); \
	NO_API ALobbyBeaconPlayerState(const ALobbyBeaconPlayerState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALobbyBeaconPlayerState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALobbyBeaconPlayerState); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALobbyBeaconPlayerState)


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_18_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h_21_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LobbyBeaconPlayerState."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LOBBY_API UClass* StaticClass<class ALobbyBeaconPlayerState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconPlayerState_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
