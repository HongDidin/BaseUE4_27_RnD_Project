// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/Chat/SocialChatManager.h"
#include "Party/Public/SocialToolkit.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialChatManager() {}
// Cross Module References
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FSocialChatChannelConfig();
	UPackage* Z_Construct_UPackage__Script_Party();
	PARTY_API UClass* Z_Construct_UClass_USocialUser_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialChatChannel_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialChatManager_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialChatManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	PARTY_API UClass* Z_Construct_UClass_USocialPrivateMessageChannel_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialChatRoom_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialReadOnlyChatChannel_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialGroupChannel_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FUniqueNetIdRepl();
// End Cross Module References
class UScriptStruct* FSocialChatChannelConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PARTY_API uint32 Get_Z_Construct_UScriptStruct_FSocialChatChannelConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSocialChatChannelConfig, Z_Construct_UPackage__Script_Party(), TEXT("SocialChatChannelConfig"), sizeof(FSocialChatChannelConfig), Get_Z_Construct_UScriptStruct_FSocialChatChannelConfig_Hash());
	}
	return Singleton;
}
template<> PARTY_API UScriptStruct* StaticStruct<FSocialChatChannelConfig>()
{
	return FSocialChatChannelConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSocialChatChannelConfig(FSocialChatChannelConfig::StaticStruct, TEXT("/Script/Party"), TEXT("SocialChatChannelConfig"), false, nullptr, nullptr);
static struct FScriptStruct_Party_StaticRegisterNativesFSocialChatChannelConfig
{
	FScriptStruct_Party_StaticRegisterNativesFSocialChatChannelConfig()
	{
		UScriptStruct::DeferCppStructOps<FSocialChatChannelConfig>(FName(TEXT("SocialChatChannelConfig")));
	}
} ScriptStruct_Party_StaticRegisterNativesFSocialChatChannelConfig;
	struct Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SocialUser_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SocialUser;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ListenChannels_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ListenChannels_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ListenChannels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Chat/SocialChatManager.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSocialChatChannelConfig>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_SocialUser_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chat/SocialChatManager.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_SocialUser = { "SocialUser", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSocialChatChannelConfig, SocialUser), Z_Construct_UClass_USocialUser_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_SocialUser_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_SocialUser_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_ListenChannels_Inner = { "ListenChannels", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_USocialChatChannel_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_ListenChannels_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chat/SocialChatManager.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_ListenChannels = { "ListenChannels", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSocialChatChannelConfig, ListenChannels), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_ListenChannels_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_ListenChannels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_SocialUser,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_ListenChannels_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::NewProp_ListenChannels,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
		nullptr,
		&NewStructOps,
		"SocialChatChannelConfig",
		sizeof(FSocialChatChannelConfig),
		alignof(FSocialChatChannelConfig),
		Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSocialChatChannelConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSocialChatChannelConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SocialChatChannelConfig"), sizeof(FSocialChatChannelConfig), Get_Z_Construct_UScriptStruct_FSocialChatChannelConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSocialChatChannelConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSocialChatChannelConfig_Hash() { return 409192997U; }
	void USocialChatManager::StaticRegisterNativesUSocialChatManager()
	{
	}
	UClass* Z_Construct_UClass_USocialChatManager_NoRegister()
	{
		return USocialChatManager::StaticClass();
	}
	struct Z_Construct_UClass_USocialChatManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DirectChannelsByTargetUser_ValueProp;
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_DirectChannelsByTargetUser_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirectChannelsByTargetUser_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_DirectChannelsByTargetUser;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ChatRoomsById_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ChatRoomsById_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChatRoomsById_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ChatRoomsById;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReadOnlyChannelsByDisplayName_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReadOnlyChannelsByDisplayName_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReadOnlyChannelsByDisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReadOnlyChannelsByDisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableChatSlashCommands_MetaData[];
#endif
		static void NewProp_bEnableChatSlashCommands_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableChatSlashCommands;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GroupChannels_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GroupChannels_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupChannels_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_GroupChannels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialChatManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialChatManager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** The chat manager is a fully passive construct that watches for creation of chat rooms and message activity therein */" },
		{ "IncludePath", "Chat/SocialChatManager.h" },
		{ "ModuleRelativePath", "Public/Chat/SocialChatManager.h" },
		{ "ToolTip", "The chat manager is a fully passive construct that watches for creation of chat rooms and message activity therein" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_DirectChannelsByTargetUser_ValueProp = { "DirectChannelsByTargetUser", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_USocialPrivateMessageChannel_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_DirectChannelsByTargetUser_Key_KeyProp = { "DirectChannelsByTargetUser_Key", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_USocialUser_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialChatManager_Statics::NewProp_DirectChannelsByTargetUser_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chat/SocialChatManager.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_DirectChannelsByTargetUser = { "DirectChannelsByTargetUser", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialChatManager, DirectChannelsByTargetUser), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USocialChatManager_Statics::NewProp_DirectChannelsByTargetUser_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialChatManager_Statics::NewProp_DirectChannelsByTargetUser_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_ChatRoomsById_ValueProp = { "ChatRoomsById", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_USocialChatRoom_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_ChatRoomsById_Key_KeyProp = { "ChatRoomsById_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialChatManager_Statics::NewProp_ChatRoomsById_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chat/SocialChatManager.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_ChatRoomsById = { "ChatRoomsById", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialChatManager, ChatRoomsById), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USocialChatManager_Statics::NewProp_ChatRoomsById_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialChatManager_Statics::NewProp_ChatRoomsById_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_ReadOnlyChannelsByDisplayName_ValueProp = { "ReadOnlyChannelsByDisplayName", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_USocialReadOnlyChatChannel_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_ReadOnlyChannelsByDisplayName_Key_KeyProp = { "ReadOnlyChannelsByDisplayName_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialChatManager_Statics::NewProp_ReadOnlyChannelsByDisplayName_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chat/SocialChatManager.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_ReadOnlyChannelsByDisplayName = { "ReadOnlyChannelsByDisplayName", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialChatManager, ReadOnlyChannelsByDisplayName), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USocialChatManager_Statics::NewProp_ReadOnlyChannelsByDisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialChatManager_Statics::NewProp_ReadOnlyChannelsByDisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialChatManager_Statics::NewProp_bEnableChatSlashCommands_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chat/SocialChatManager.h" },
	};
#endif
	void Z_Construct_UClass_USocialChatManager_Statics::NewProp_bEnableChatSlashCommands_SetBit(void* Obj)
	{
		((USocialChatManager*)Obj)->bEnableChatSlashCommands = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_bEnableChatSlashCommands = { "bEnableChatSlashCommands", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USocialChatManager), &Z_Construct_UClass_USocialChatManager_Statics::NewProp_bEnableChatSlashCommands_SetBit, METADATA_PARAMS(Z_Construct_UClass_USocialChatManager_Statics::NewProp_bEnableChatSlashCommands_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialChatManager_Statics::NewProp_bEnableChatSlashCommands_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_GroupChannels_ValueProp = { "GroupChannels", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_USocialGroupChannel_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_GroupChannels_Key_KeyProp = { "GroupChannels_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FUniqueNetIdRepl, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialChatManager_Statics::NewProp_GroupChannels_MetaData[] = {
		{ "ModuleRelativePath", "Public/Chat/SocialChatManager.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_USocialChatManager_Statics::NewProp_GroupChannels = { "GroupChannels", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialChatManager, GroupChannels), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USocialChatManager_Statics::NewProp_GroupChannels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialChatManager_Statics::NewProp_GroupChannels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USocialChatManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_DirectChannelsByTargetUser_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_DirectChannelsByTargetUser_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_DirectChannelsByTargetUser,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_ChatRoomsById_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_ChatRoomsById_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_ChatRoomsById,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_ReadOnlyChannelsByDisplayName_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_ReadOnlyChannelsByDisplayName_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_ReadOnlyChannelsByDisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_bEnableChatSlashCommands,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_GroupChannels_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_GroupChannels_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialChatManager_Statics::NewProp_GroupChannels,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialChatManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialChatManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialChatManager_Statics::ClassParams = {
		&USocialChatManager::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USocialChatManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USocialChatManager_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_USocialChatManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialChatManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialChatManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialChatManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialChatManager, 2825906808);
	template<> PARTY_API UClass* StaticClass<USocialChatManager>()
	{
		return USocialChatManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialChatManager(Z_Construct_UClass_USocialChatManager, &USocialChatManager::StaticClass, TEXT("/Script/Party"), TEXT("USocialChatManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialChatManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
