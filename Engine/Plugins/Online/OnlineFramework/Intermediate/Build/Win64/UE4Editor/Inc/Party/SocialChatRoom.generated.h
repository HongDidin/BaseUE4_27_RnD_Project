// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_SocialChatRoom_generated_h
#error "SocialChatRoom.generated.h already included, missing '#pragma once' in SocialChatRoom.h"
#endif
#define PARTY_SocialChatRoom_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSocialChatRoom(); \
	friend struct Z_Construct_UClass_USocialChatRoom_Statics; \
public: \
	DECLARE_CLASS(USocialChatRoom, USocialChatChannel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialChatRoom)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSocialChatRoom(); \
	friend struct Z_Construct_UClass_USocialChatRoom_Statics; \
public: \
	DECLARE_CLASS(USocialChatRoom, USocialChatChannel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialChatRoom)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialChatRoom(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialChatRoom) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialChatRoom); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialChatRoom); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialChatRoom(USocialChatRoom&&); \
	NO_API USocialChatRoom(const USocialChatRoom&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialChatRoom() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialChatRoom(USocialChatRoom&&); \
	NO_API USocialChatRoom(const USocialChatRoom&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialChatRoom); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialChatRoom); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USocialChatRoom)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_12_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class USocialChatRoom>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialChatRoom_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
