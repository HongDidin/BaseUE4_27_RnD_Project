// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef QOS_QosEvaluator_generated_h
#error "QosEvaluator.generated.h already included, missing '#pragma once' in QosEvaluator.h"
#endif
#define QOS_QosEvaluator_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUQosEvaluator(); \
	friend struct Z_Construct_UClass_UQosEvaluator_Statics; \
public: \
	DECLARE_CLASS(UQosEvaluator, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Qos"), NO_API) \
	DECLARE_SERIALIZER(UQosEvaluator) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_INCLASS \
private: \
	static void StaticRegisterNativesUQosEvaluator(); \
	friend struct Z_Construct_UClass_UQosEvaluator_Statics; \
public: \
	DECLARE_CLASS(UQosEvaluator, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Qos"), NO_API) \
	DECLARE_SERIALIZER(UQosEvaluator) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UQosEvaluator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UQosEvaluator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UQosEvaluator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UQosEvaluator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UQosEvaluator(UQosEvaluator&&); \
	NO_API UQosEvaluator(const UQosEvaluator&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UQosEvaluator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UQosEvaluator(UQosEvaluator&&); \
	NO_API UQosEvaluator(const UQosEvaluator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UQosEvaluator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UQosEvaluator); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UQosEvaluator)


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bInProgress() { return STRUCT_OFFSET(UQosEvaluator, bInProgress); } \
	FORCEINLINE static uint32 __PPO__bCancelOperation() { return STRUCT_OFFSET(UQosEvaluator, bCancelOperation); } \
	FORCEINLINE static uint32 __PPO__Datacenters() { return STRUCT_OFFSET(UQosEvaluator, Datacenters); }


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_51_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h_54_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class QosEvaluator."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> QOS_API UClass* StaticClass<class UQosEvaluator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Qos_Private_QosEvaluator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
