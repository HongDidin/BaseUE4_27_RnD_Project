// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_SocialManager_generated_h
#error "SocialManager.generated.h already included, missing '#pragma once' in SocialManager.h"
#endif
#define PARTY_SocialManager_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSocialManager(); \
	friend struct Z_Construct_UClass_USocialManager_Statics; \
public: \
	DECLARE_CLASS(USocialManager, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialManager) \
	DECLARE_WITHIN(UGameInstance) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_INCLASS \
private: \
	static void StaticRegisterNativesUSocialManager(); \
	friend struct Z_Construct_UClass_USocialManager_Statics; \
public: \
	DECLARE_CLASS(USocialManager, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialManager) \
	DECLARE_WITHIN(UGameInstance) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialManager(USocialManager&&); \
	NO_API USocialManager(const USocialManager&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialManager(USocialManager&&); \
	NO_API USocialManager(const USocialManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USocialManager)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SocialToolkits() { return STRUCT_OFFSET(USocialManager, SocialToolkits); } \
	FORCEINLINE static uint32 __PPO__SocialDebugTools() { return STRUCT_OFFSET(USocialManager, SocialDebugTools); }


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_32_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class USocialManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
