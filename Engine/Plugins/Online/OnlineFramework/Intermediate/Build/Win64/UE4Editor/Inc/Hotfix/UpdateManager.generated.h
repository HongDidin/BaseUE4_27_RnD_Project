// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOTFIX_UpdateManager_generated_h
#error "UpdateManager.generated.h already included, missing '#pragma once' in UpdateManager.h"
#endif
#define HOTFIX_UpdateManager_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUpdateManager(); \
	friend struct Z_Construct_UClass_UUpdateManager_Statics; \
public: \
	DECLARE_CLASS(UUpdateManager, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Hotfix"), NO_API) \
	DECLARE_SERIALIZER(UUpdateManager) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_INCLASS \
private: \
	static void StaticRegisterNativesUUpdateManager(); \
	friend struct Z_Construct_UClass_UUpdateManager_Statics; \
public: \
	DECLARE_CLASS(UUpdateManager, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Hotfix"), NO_API) \
	DECLARE_SERIALIZER(UUpdateManager) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUpdateManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUpdateManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUpdateManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUpdateManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUpdateManager(UUpdateManager&&); \
	NO_API UUpdateManager(const UUpdateManager&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUpdateManager(UUpdateManager&&); \
	NO_API UUpdateManager(const UUpdateManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUpdateManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUpdateManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UUpdateManager)


#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HotfixCheckCompleteDelay() { return STRUCT_OFFSET(UUpdateManager, HotfixCheckCompleteDelay); } \
	FORCEINLINE static uint32 __PPO__UpdateCheckCompleteDelay() { return STRUCT_OFFSET(UUpdateManager, UpdateCheckCompleteDelay); } \
	FORCEINLINE static uint32 __PPO__HotfixAvailabilityCheckCompleteDelay() { return STRUCT_OFFSET(UUpdateManager, HotfixAvailabilityCheckCompleteDelay); } \
	FORCEINLINE static uint32 __PPO__UpdateCheckAvailabilityCompleteDelay() { return STRUCT_OFFSET(UUpdateManager, UpdateCheckAvailabilityCompleteDelay); } \
	FORCEINLINE static uint32 __PPO__AppSuspendedUpdateCheckTimeSeconds() { return STRUCT_OFFSET(UUpdateManager, AppSuspendedUpdateCheckTimeSeconds); } \
	FORCEINLINE static uint32 __PPO__bPlatformEnvironmentDetected() { return STRUCT_OFFSET(UUpdateManager, bPlatformEnvironmentDetected); } \
	FORCEINLINE static uint32 __PPO__bInitialUpdateFinished() { return STRUCT_OFFSET(UUpdateManager, bInitialUpdateFinished); } \
	FORCEINLINE static uint32 __PPO__bCheckHotfixAvailabilityOnly() { return STRUCT_OFFSET(UUpdateManager, bCheckHotfixAvailabilityOnly); } \
	FORCEINLINE static uint32 __PPO__CurrentUpdateState() { return STRUCT_OFFSET(UUpdateManager, CurrentUpdateState); } \
	FORCEINLINE static uint32 __PPO__WorstNumFilesPendingLoadViewed() { return STRUCT_OFFSET(UUpdateManager, WorstNumFilesPendingLoadViewed); } \
	FORCEINLINE static uint32 __PPO__LastHotfixResult() { return STRUCT_OFFSET(UUpdateManager, LastHotfixResult); } \
	FORCEINLINE static uint32 __PPO__LastUpdateCheck() { return STRUCT_OFFSET(UUpdateManager, LastUpdateCheck); } \
	FORCEINLINE static uint32 __PPO__LastCompletionResult() { return STRUCT_OFFSET(UUpdateManager, LastCompletionResult); } \
	FORCEINLINE static uint32 __PPO__UpdateStateEnum() { return STRUCT_OFFSET(UUpdateManager, UpdateStateEnum); } \
	FORCEINLINE static uint32 __PPO__UpdateCompletionEnum() { return STRUCT_OFFSET(UUpdateManager, UpdateCompletionEnum); }


#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_116_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h_119_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOTFIX_API UClass* StaticClass<class UUpdateManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Hotfix_Public_UpdateManager_h


#define FOREACH_ENUM_EUPDATECOMPLETIONSTATUS(op) \
	op(EUpdateCompletionStatus::UpdateUnknown) \
	op(EUpdateCompletionStatus::UpdateSuccess) \
	op(EUpdateCompletionStatus::UpdateSuccess_NoChange) \
	op(EUpdateCompletionStatus::UpdateSuccess_NeedsReload) \
	op(EUpdateCompletionStatus::UpdateSuccess_NeedsRelaunch) \
	op(EUpdateCompletionStatus::UpdateSuccess_NeedsPatch) \
	op(EUpdateCompletionStatus::UpdateFailure_PatchCheck) \
	op(EUpdateCompletionStatus::UpdateFailure_HotfixCheck) \
	op(EUpdateCompletionStatus::UpdateFailure_NotLoggedIn) 

enum class EUpdateCompletionStatus : uint8;
template<> HOTFIX_API UEnum* StaticEnum<EUpdateCompletionStatus>();

#define FOREACH_ENUM_EUPDATESTATE(op) \
	op(EUpdateState::UpdateIdle) \
	op(EUpdateState::UpdatePending) \
	op(EUpdateState::CheckingForPatch) \
	op(EUpdateState::DetectingPlatformEnvironment) \
	op(EUpdateState::CheckingForHotfix) \
	op(EUpdateState::WaitingOnInitialLoad) \
	op(EUpdateState::InitialLoadComplete) \
	op(EUpdateState::UpdateComplete) 

enum class EUpdateState : uint8;
template<> HOTFIX_API UEnum* StaticEnum<EUpdateState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
