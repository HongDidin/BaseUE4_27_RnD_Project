// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_SocialGroupChannel_generated_h
#error "SocialGroupChannel.generated.h already included, missing '#pragma once' in SocialGroupChannel.h"
#endif
#define PARTY_SocialGroupChannel_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSocialGroupChannel(); \
	friend struct Z_Construct_UClass_USocialGroupChannel_Statics; \
public: \
	DECLARE_CLASS(USocialGroupChannel, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialGroupChannel)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUSocialGroupChannel(); \
	friend struct Z_Construct_UClass_USocialGroupChannel_Statics; \
public: \
	DECLARE_CLASS(USocialGroupChannel, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialGroupChannel)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialGroupChannel(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialGroupChannel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialGroupChannel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialGroupChannel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialGroupChannel(USocialGroupChannel&&); \
	NO_API USocialGroupChannel(const USocialGroupChannel&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialGroupChannel(USocialGroupChannel&&); \
	NO_API USocialGroupChannel(const USocialGroupChannel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialGroupChannel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialGroupChannel); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USocialGroupChannel)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SocialUser() { return STRUCT_OFFSET(USocialGroupChannel, SocialUser); } \
	FORCEINLINE static uint32 __PPO__GroupId() { return STRUCT_OFFSET(USocialGroupChannel, GroupId); } \
	FORCEINLINE static uint32 __PPO__DisplayName() { return STRUCT_OFFSET(USocialGroupChannel, DisplayName); } \
	FORCEINLINE static uint32 __PPO__Members() { return STRUCT_OFFSET(USocialGroupChannel, Members); }


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_15_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class USocialGroupChannel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialGroupChannel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
