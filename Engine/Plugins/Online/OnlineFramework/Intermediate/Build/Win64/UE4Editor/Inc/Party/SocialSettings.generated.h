// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_SocialSettings_generated_h
#error "SocialSettings.generated.h already included, missing '#pragma once' in SocialSettings.h"
#endif
#define PARTY_SocialSettings_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSocialPlatformDescription_Statics; \
	PARTY_API static class UScriptStruct* StaticStruct();


template<> PARTY_API UScriptStruct* StaticStruct<struct FSocialPlatformDescription>();

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSocialSettings(); \
	friend struct Z_Construct_UClass_USocialSettings_Statics; \
public: \
	DECLARE_CLASS(USocialSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_INCLASS \
private: \
	static void StaticRegisterNativesUSocialSettings(); \
	friend struct Z_Construct_UClass_USocialSettings_Statics; \
public: \
	DECLARE_CLASS(USocialSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialSettings(USocialSettings&&); \
	NO_API USocialSettings(const USocialSettings&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialSettings(USocialSettings&&); \
	NO_API USocialSettings(const USocialSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USocialSettings)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OssNamesWithEnvironmentIdPrefix() { return STRUCT_OFFSET(USocialSettings, OssNamesWithEnvironmentIdPrefix); } \
	FORCEINLINE static uint32 __PPO__DefaultMaxPartySize() { return STRUCT_OFFSET(USocialSettings, DefaultMaxPartySize); } \
	FORCEINLINE static uint32 __PPO__bPreferPlatformInvites() { return STRUCT_OFFSET(USocialSettings, bPreferPlatformInvites); } \
	FORCEINLINE static uint32 __PPO__bMustSendPrimaryInvites() { return STRUCT_OFFSET(USocialSettings, bMustSendPrimaryInvites); } \
	FORCEINLINE static uint32 __PPO__bLeavePartyOnDisconnect() { return STRUCT_OFFSET(USocialSettings, bLeavePartyOnDisconnect); } \
	FORCEINLINE static uint32 __PPO__bSetDesiredPrivacyOnLocalPlayerBecomesLeader() { return STRUCT_OFFSET(USocialSettings, bSetDesiredPrivacyOnLocalPlayerBecomesLeader); } \
	FORCEINLINE static uint32 __PPO__UserListAutoUpdateRate() { return STRUCT_OFFSET(USocialSettings, UserListAutoUpdateRate); } \
	FORCEINLINE static uint32 __PPO__MinNicknameLength() { return STRUCT_OFFSET(USocialSettings, MinNicknameLength); } \
	FORCEINLINE static uint32 __PPO__MaxNicknameLength() { return STRUCT_OFFSET(USocialSettings, MaxNicknameLength); } \
	FORCEINLINE static uint32 __PPO__SocialPlatformDescriptions() { return STRUCT_OFFSET(USocialSettings, SocialPlatformDescriptions); }


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_50_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h_53_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class USocialSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_SocialSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
