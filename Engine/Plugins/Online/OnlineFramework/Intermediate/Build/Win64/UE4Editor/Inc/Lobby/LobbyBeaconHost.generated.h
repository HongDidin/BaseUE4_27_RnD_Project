// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOBBY_LobbyBeaconHost_generated_h
#error "LobbyBeaconHost.generated.h already included, missing '#pragma once' in LobbyBeaconHost.h"
#endif
#define LOBBY_LobbyBeaconHost_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALobbyBeaconHost(); \
	friend struct Z_Construct_UClass_ALobbyBeaconHost_Statics; \
public: \
	DECLARE_CLASS(ALobbyBeaconHost, AOnlineBeaconHostObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Lobby"), NO_API) \
	DECLARE_SERIALIZER(ALobbyBeaconHost)


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_INCLASS \
private: \
	static void StaticRegisterNativesALobbyBeaconHost(); \
	friend struct Z_Construct_UClass_ALobbyBeaconHost_Statics; \
public: \
	DECLARE_CLASS(ALobbyBeaconHost, AOnlineBeaconHostObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Lobby"), NO_API) \
	DECLARE_SERIALIZER(ALobbyBeaconHost)


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALobbyBeaconHost(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALobbyBeaconHost) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALobbyBeaconHost); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALobbyBeaconHost); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALobbyBeaconHost(ALobbyBeaconHost&&); \
	NO_API ALobbyBeaconHost(const ALobbyBeaconHost&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALobbyBeaconHost(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALobbyBeaconHost(ALobbyBeaconHost&&); \
	NO_API ALobbyBeaconHost(const ALobbyBeaconHost&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALobbyBeaconHost); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALobbyBeaconHost); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALobbyBeaconHost)


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LobbyStateClass() { return STRUCT_OFFSET(ALobbyBeaconHost, LobbyStateClass); } \
	FORCEINLINE static uint32 __PPO__LobbyState() { return STRUCT_OFFSET(ALobbyBeaconHost, LobbyState); }


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_23_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h_26_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LobbyBeaconHost."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LOBBY_API UClass* StaticClass<class ALobbyBeaconHost>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Lobby_Public_LobbyBeaconHost_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
