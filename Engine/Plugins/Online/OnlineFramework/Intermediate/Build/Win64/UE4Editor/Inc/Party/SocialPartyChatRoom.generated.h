// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_SocialPartyChatRoom_generated_h
#error "SocialPartyChatRoom.generated.h already included, missing '#pragma once' in SocialPartyChatRoom.h"
#endif
#define PARTY_SocialPartyChatRoom_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSocialPartyChatRoom(); \
	friend struct Z_Construct_UClass_USocialPartyChatRoom_Statics; \
public: \
	DECLARE_CLASS(USocialPartyChatRoom, USocialChatRoom, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialPartyChatRoom)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSocialPartyChatRoom(); \
	friend struct Z_Construct_UClass_USocialPartyChatRoom_Statics; \
public: \
	DECLARE_CLASS(USocialPartyChatRoom, USocialChatRoom, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialPartyChatRoom)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialPartyChatRoom(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialPartyChatRoom) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialPartyChatRoom); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialPartyChatRoom); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialPartyChatRoom(USocialPartyChatRoom&&); \
	NO_API USocialPartyChatRoom(const USocialPartyChatRoom&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialPartyChatRoom() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialPartyChatRoom(USocialPartyChatRoom&&); \
	NO_API USocialPartyChatRoom(const USocialPartyChatRoom&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialPartyChatRoom); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialPartyChatRoom); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USocialPartyChatRoom)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_12_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class USocialPartyChatRoom>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialPartyChatRoom_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
