// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Qos/Public/QosRegionManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeQosRegionManager() {}
// Cross Module References
	QOS_API UEnum* Z_Construct_UEnum_Qos_EQosCompletionResult();
	UPackage* Z_Construct_UPackage__Script_Qos();
	QOS_API UEnum* Z_Construct_UEnum_Qos_EQosDatacenterResult();
	QOS_API UScriptStruct* Z_Construct_UScriptStruct_FRegionQosInstance();
	QOS_API UScriptStruct* Z_Construct_UScriptStruct_FQosRegionInfo();
	QOS_API UScriptStruct* Z_Construct_UScriptStruct_FDatacenterQosInstance();
	QOS_API UScriptStruct* Z_Construct_UScriptStruct_FQosDatacenterInfo();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	QOS_API UScriptStruct* Z_Construct_UScriptStruct_FQosPingServerInfo();
	QOS_API UClass* Z_Construct_UClass_UQosRegionManager_NoRegister();
	QOS_API UClass* Z_Construct_UClass_UQosRegionManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	QOS_API UClass* Z_Construct_UClass_UQosEvaluator_NoRegister();
// End Cross Module References
	static UEnum* EQosCompletionResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Qos_EQosCompletionResult, Z_Construct_UPackage__Script_Qos(), TEXT("EQosCompletionResult"));
		}
		return Singleton;
	}
	template<> QOS_API UEnum* StaticEnum<EQosCompletionResult>()
	{
		return EQosCompletionResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EQosCompletionResult(EQosCompletionResult_StaticEnum, TEXT("/Script/Qos"), TEXT("EQosCompletionResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Qos_EQosCompletionResult_Hash() { return 1306298086U; }
	UEnum* Z_Construct_UEnum_Qos_EQosCompletionResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Qos();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EQosCompletionResult"), 0, Get_Z_Construct_UEnum_Qos_EQosCompletionResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EQosCompletionResult::Invalid", (int64)EQosCompletionResult::Invalid },
				{ "EQosCompletionResult::Success", (int64)EQosCompletionResult::Success },
				{ "EQosCompletionResult::Failure", (int64)EQosCompletionResult::Failure },
				{ "EQosCompletionResult::Canceled", (int64)EQosCompletionResult::Canceled },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Canceled.Comment", "/** QoS operation was canceled */" },
				{ "Canceled.Name", "EQosCompletionResult::Canceled" },
				{ "Canceled.ToolTip", "QoS operation was canceled" },
				{ "Comment", "/** Enum for possible QoS return codes */" },
				{ "Failure.Comment", "/** QoS operation ended in failure */" },
				{ "Failure.Name", "EQosCompletionResult::Failure" },
				{ "Failure.ToolTip", "QoS operation ended in failure" },
				{ "Invalid.Comment", "/** Incomplete, invalid result */" },
				{ "Invalid.Name", "EQosCompletionResult::Invalid" },
				{ "Invalid.ToolTip", "Incomplete, invalid result" },
				{ "ModuleRelativePath", "Public/QosRegionManager.h" },
				{ "Success.Comment", "/** QoS operation was successful */" },
				{ "Success.Name", "EQosCompletionResult::Success" },
				{ "Success.ToolTip", "QoS operation was successful" },
				{ "ToolTip", "Enum for possible QoS return codes" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Qos,
				nullptr,
				"EQosCompletionResult",
				"EQosCompletionResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EQosDatacenterResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Qos_EQosDatacenterResult, Z_Construct_UPackage__Script_Qos(), TEXT("EQosDatacenterResult"));
		}
		return Singleton;
	}
	template<> QOS_API UEnum* StaticEnum<EQosDatacenterResult>()
	{
		return EQosDatacenterResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EQosDatacenterResult(EQosDatacenterResult_StaticEnum, TEXT("/Script/Qos"), TEXT("EQosDatacenterResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Qos_EQosDatacenterResult_Hash() { return 627627858U; }
	UEnum* Z_Construct_UEnum_Qos_EQosDatacenterResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Qos();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EQosDatacenterResult"), 0, Get_Z_Construct_UEnum_Qos_EQosDatacenterResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EQosDatacenterResult::Invalid", (int64)EQosDatacenterResult::Invalid },
				{ "EQosDatacenterResult::Success", (int64)EQosDatacenterResult::Success },
				{ "EQosDatacenterResult::Incomplete", (int64)EQosDatacenterResult::Incomplete },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Enum for single region QoS return codes */" },
				{ "Incomplete.Comment", "/** QoS operation with one or more ping failures */" },
				{ "Incomplete.Name", "EQosDatacenterResult::Incomplete" },
				{ "Incomplete.ToolTip", "QoS operation with one or more ping failures" },
				{ "Invalid.Comment", "/** Incomplete, invalid result */" },
				{ "Invalid.Name", "EQosDatacenterResult::Invalid" },
				{ "Invalid.ToolTip", "Incomplete, invalid result" },
				{ "ModuleRelativePath", "Public/QosRegionManager.h" },
				{ "Success.Comment", "/** QoS operation was successful */" },
				{ "Success.Name", "EQosDatacenterResult::Success" },
				{ "Success.ToolTip", "QoS operation was successful" },
				{ "ToolTip", "Enum for single region QoS return codes" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Qos,
				nullptr,
				"EQosDatacenterResult",
				"EQosDatacenterResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FRegionQosInstance::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern QOS_API uint32 Get_Z_Construct_UScriptStruct_FRegionQosInstance_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRegionQosInstance, Z_Construct_UPackage__Script_Qos(), TEXT("RegionQosInstance"), sizeof(FRegionQosInstance), Get_Z_Construct_UScriptStruct_FRegionQosInstance_Hash());
	}
	return Singleton;
}
template<> QOS_API UScriptStruct* StaticStruct<FRegionQosInstance>()
{
	return FRegionQosInstance::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRegionQosInstance(FRegionQosInstance::StaticStruct, TEXT("/Script/Qos"), TEXT("RegionQosInstance"), false, nullptr, nullptr);
static struct FScriptStruct_Qos_StaticRegisterNativesFRegionQosInstance
{
	FScriptStruct_Qos_StaticRegisterNativesFRegionQosInstance()
	{
		UScriptStruct::DeferCppStructOps<FRegionQosInstance>(FName(TEXT("RegionQosInstance")));
	}
} ScriptStruct_Qos_StaticRegisterNativesFRegionQosInstance;
	struct Z_Construct_UScriptStruct_FRegionQosInstance_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Definition_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Definition;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DatacenterOptions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DatacenterOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DatacenterOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRegionQosInstance_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRegionQosInstance>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_Definition_MetaData[] = {
		{ "Comment", "/** Information about the region */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Information about the region" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_Definition = { "Definition", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRegionQosInstance, Definition), Z_Construct_UScriptStruct_FQosRegionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_Definition_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_Definition_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_DatacenterOptions_Inner = { "DatacenterOptions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDatacenterQosInstance, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_DatacenterOptions_MetaData[] = {
		{ "Comment", "/** Array of all known datacenters and their status */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Array of all known datacenters and their status" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_DatacenterOptions = { "DatacenterOptions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRegionQosInstance, DatacenterOptions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_DatacenterOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_DatacenterOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRegionQosInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_Definition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_DatacenterOptions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRegionQosInstance_Statics::NewProp_DatacenterOptions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRegionQosInstance_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Qos,
		nullptr,
		&NewStructOps,
		"RegionQosInstance",
		sizeof(FRegionQosInstance),
		alignof(FRegionQosInstance),
		Z_Construct_UScriptStruct_FRegionQosInstance_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRegionQosInstance_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRegionQosInstance_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRegionQosInstance_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRegionQosInstance()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRegionQosInstance_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Qos();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RegionQosInstance"), sizeof(FRegionQosInstance), Get_Z_Construct_UScriptStruct_FRegionQosInstance_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRegionQosInstance_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRegionQosInstance_Hash() { return 2195183765U; }
class UScriptStruct* FDatacenterQosInstance::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern QOS_API uint32 Get_Z_Construct_UScriptStruct_FDatacenterQosInstance_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDatacenterQosInstance, Z_Construct_UPackage__Script_Qos(), TEXT("DatacenterQosInstance"), sizeof(FDatacenterQosInstance), Get_Z_Construct_UScriptStruct_FDatacenterQosInstance_Hash());
	}
	return Singleton;
}
template<> QOS_API UScriptStruct* StaticStruct<FDatacenterQosInstance>()
{
	return FDatacenterQosInstance::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDatacenterQosInstance(FDatacenterQosInstance::StaticStruct, TEXT("/Script/Qos"), TEXT("DatacenterQosInstance"), false, nullptr, nullptr);
static struct FScriptStruct_Qos_StaticRegisterNativesFDatacenterQosInstance
{
	FScriptStruct_Qos_StaticRegisterNativesFDatacenterQosInstance()
	{
		UScriptStruct::DeferCppStructOps<FDatacenterQosInstance>(FName(TEXT("DatacenterQosInstance")));
	}
} ScriptStruct_Qos_StaticRegisterNativesFDatacenterQosInstance;
	struct Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Definition_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Definition;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Result_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Result_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Result;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AvgPingMs_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AvgPingMs;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PingResults_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PingResults_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PingResults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastCheckTimestamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastCheckTimestamp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUsable_MetaData[];
#endif
		static void NewProp_bUsable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Runtime information about a given region */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Runtime information about a given region" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDatacenterQosInstance>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Definition_MetaData[] = {
		{ "Comment", "/** Information about the datacenter */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Information about the datacenter" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Definition = { "Definition", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDatacenterQosInstance, Definition), Z_Construct_UScriptStruct_FQosDatacenterInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Definition_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Definition_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Result_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Result_MetaData[] = {
		{ "Comment", "/** Success of the qos evaluation */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Success of the qos evaluation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDatacenterQosInstance, Result), Z_Construct_UEnum_Qos_EQosDatacenterResult, METADATA_PARAMS(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Result_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Result_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_AvgPingMs_MetaData[] = {
		{ "Comment", "/** Avg ping times across all search results */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Avg ping times across all search results" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_AvgPingMs = { "AvgPingMs", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDatacenterQosInstance, AvgPingMs), METADATA_PARAMS(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_AvgPingMs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_AvgPingMs_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_PingResults_Inner = { "PingResults", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_PingResults_MetaData[] = {
		{ "Comment", "/** Transient list of ping times obtained for this datacenter */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Transient list of ping times obtained for this datacenter" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_PingResults = { "PingResults", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDatacenterQosInstance, PingResults), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_PingResults_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_PingResults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_LastCheckTimestamp_MetaData[] = {
		{ "Comment", "/** Last time this datacenter was checked */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Last time this datacenter was checked" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_LastCheckTimestamp = { "LastCheckTimestamp", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDatacenterQosInstance, LastCheckTimestamp), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_LastCheckTimestamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_LastCheckTimestamp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_bUsable_MetaData[] = {
		{ "Comment", "/** Is the parent region usable */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Is the parent region usable" },
	};
#endif
	void Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_bUsable_SetBit(void* Obj)
	{
		((FDatacenterQosInstance*)Obj)->bUsable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_bUsable = { "bUsable", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDatacenterQosInstance), &Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_bUsable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_bUsable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_bUsable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Definition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Result_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_Result,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_AvgPingMs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_PingResults_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_PingResults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_LastCheckTimestamp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::NewProp_bUsable,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Qos,
		nullptr,
		&NewStructOps,
		"DatacenterQosInstance",
		sizeof(FDatacenterQosInstance),
		alignof(FDatacenterQosInstance),
		Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDatacenterQosInstance()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDatacenterQosInstance_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Qos();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DatacenterQosInstance"), sizeof(FDatacenterQosInstance), Get_Z_Construct_UScriptStruct_FDatacenterQosInstance_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDatacenterQosInstance_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDatacenterQosInstance_Hash() { return 3171230048U; }
class UScriptStruct* FQosRegionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern QOS_API uint32 Get_Z_Construct_UScriptStruct_FQosRegionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FQosRegionInfo, Z_Construct_UPackage__Script_Qos(), TEXT("QosRegionInfo"), sizeof(FQosRegionInfo), Get_Z_Construct_UScriptStruct_FQosRegionInfo_Hash());
	}
	return Singleton;
}
template<> QOS_API UScriptStruct* StaticStruct<FQosRegionInfo>()
{
	return FQosRegionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FQosRegionInfo(FQosRegionInfo::StaticStruct, TEXT("/Script/Qos"), TEXT("QosRegionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Qos_StaticRegisterNativesFQosRegionInfo
{
	FScriptStruct_Qos_StaticRegisterNativesFQosRegionInfo()
	{
		UScriptStruct::DeferCppStructOps<FQosRegionInfo>(FName(TEXT("QosRegionInfo")));
	}
} ScriptStruct_Qos_StaticRegisterNativesFQosRegionInfo;
	struct Z_Construct_UScriptStruct_FQosRegionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RegionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bVisible_MetaData[];
#endif
		static void NewProp_bVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bVisible;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoAssignable_MetaData[];
#endif
		static void NewProp_bAutoAssignable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoAssignable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosRegionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Metadata about regions made up of datacenters\n */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Metadata about regions made up of datacenters" },
	};
#endif
	void* Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FQosRegionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Comment", "/** Localized name of the region */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Localized name of the region" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FQosRegionInfo, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_RegionId_MetaData[] = {
		{ "Comment", "/** Id for the region, all datacenters must reference one of these */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Id for the region, all datacenters must reference one of these" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_RegionId = { "RegionId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FQosRegionInfo, RegionId), METADATA_PARAMS(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_RegionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_RegionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Comment", "/** Is this region tested at all (if false, overrides individual datacenters) */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Is this region tested at all (if false, overrides individual datacenters)" },
	};
#endif
	void Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FQosRegionInfo*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FQosRegionInfo), &Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bVisible_MetaData[] = {
		{ "Comment", "/** Is this region visible in the UI (can be saved by user, replaced with auto if region disappears */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Is this region visible in the UI (can be saved by user, replaced with auto if region disappears" },
	};
#endif
	void Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bVisible_SetBit(void* Obj)
	{
		((FQosRegionInfo*)Obj)->bVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bVisible = { "bVisible", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FQosRegionInfo), &Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bVisible_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bVisible_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bAutoAssignable_MetaData[] = {
		{ "Comment", "/** Can this region be considered for auto detection */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Can this region be considered for auto detection" },
	};
#endif
	void Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bAutoAssignable_SetBit(void* Obj)
	{
		((FQosRegionInfo*)Obj)->bAutoAssignable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bAutoAssignable = { "bAutoAssignable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FQosRegionInfo), &Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bAutoAssignable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bAutoAssignable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bAutoAssignable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FQosRegionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_RegionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bVisible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosRegionInfo_Statics::NewProp_bAutoAssignable,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FQosRegionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Qos,
		nullptr,
		&NewStructOps,
		"QosRegionInfo",
		sizeof(FQosRegionInfo),
		alignof(FQosRegionInfo),
		Z_Construct_UScriptStruct_FQosRegionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosRegionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FQosRegionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FQosRegionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Qos();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("QosRegionInfo"), sizeof(FQosRegionInfo), Get_Z_Construct_UScriptStruct_FQosRegionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FQosRegionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FQosRegionInfo_Hash() { return 2086419272U; }
class UScriptStruct* FQosDatacenterInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern QOS_API uint32 Get_Z_Construct_UScriptStruct_FQosDatacenterInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FQosDatacenterInfo, Z_Construct_UPackage__Script_Qos(), TEXT("QosDatacenterInfo"), sizeof(FQosDatacenterInfo), Get_Z_Construct_UScriptStruct_FQosDatacenterInfo_Hash());
	}
	return Singleton;
}
template<> QOS_API UScriptStruct* StaticStruct<FQosDatacenterInfo>()
{
	return FQosDatacenterInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FQosDatacenterInfo(FQosDatacenterInfo::StaticStruct, TEXT("/Script/Qos"), TEXT("QosDatacenterInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Qos_StaticRegisterNativesFQosDatacenterInfo
{
	FScriptStruct_Qos_StaticRegisterNativesFQosDatacenterInfo()
	{
		UScriptStruct::DeferCppStructOps<FQosDatacenterInfo>(FName(TEXT("QosDatacenterInfo")));
	}
} ScriptStruct_Qos_StaticRegisterNativesFQosDatacenterInfo;
	struct Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RegionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Servers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Servers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Servers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Metadata about datacenters that can be queried\n */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Metadata about datacenters that can be queried" },
	};
#endif
	void* Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FQosDatacenterInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Id_MetaData[] = {
		{ "Comment", "/** Id for this datacenter */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Id for this datacenter" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FQosDatacenterInfo, Id), METADATA_PARAMS(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_RegionId_MetaData[] = {
		{ "Comment", "/** Parent Region */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Parent Region" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_RegionId = { "RegionId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FQosDatacenterInfo, RegionId), METADATA_PARAMS(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_RegionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_RegionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Comment", "/** Is this region tested (only valid if region is enabled) */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Is this region tested (only valid if region is enabled)" },
	};
#endif
	void Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FQosDatacenterInfo*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FQosDatacenterInfo), &Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_bEnabled_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Servers_Inner = { "Servers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FQosPingServerInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Servers_MetaData[] = {
		{ "Comment", "/** Addresses of ping servers */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Addresses of ping servers" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Servers = { "Servers", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FQosDatacenterInfo, Servers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Servers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Servers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_RegionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Servers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::NewProp_Servers,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Qos,
		nullptr,
		&NewStructOps,
		"QosDatacenterInfo",
		sizeof(FQosDatacenterInfo),
		alignof(FQosDatacenterInfo),
		Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FQosDatacenterInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FQosDatacenterInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Qos();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("QosDatacenterInfo"), sizeof(FQosDatacenterInfo), Get_Z_Construct_UScriptStruct_FQosDatacenterInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FQosDatacenterInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FQosDatacenterInfo_Hash() { return 3653396012U; }
class UScriptStruct* FQosPingServerInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern QOS_API uint32 Get_Z_Construct_UScriptStruct_FQosPingServerInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FQosPingServerInfo, Z_Construct_UPackage__Script_Qos(), TEXT("QosPingServerInfo"), sizeof(FQosPingServerInfo), Get_Z_Construct_UScriptStruct_FQosPingServerInfo_Hash());
	}
	return Singleton;
}
template<> QOS_API UScriptStruct* StaticStruct<FQosPingServerInfo>()
{
	return FQosPingServerInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FQosPingServerInfo(FQosPingServerInfo::StaticStruct, TEXT("/Script/Qos"), TEXT("QosPingServerInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Qos_StaticRegisterNativesFQosPingServerInfo
{
	FScriptStruct_Qos_StaticRegisterNativesFQosPingServerInfo()
	{
		UScriptStruct::DeferCppStructOps<FQosPingServerInfo>(FName(TEXT("QosPingServerInfo")));
	}
} ScriptStruct_Qos_StaticRegisterNativesFQosPingServerInfo;
	struct Z_Construct_UScriptStruct_FQosPingServerInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Address_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Address;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Port_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Port;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Individual ping server details\n */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Individual ping server details" },
	};
#endif
	void* Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FQosPingServerInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewProp_Address_MetaData[] = {
		{ "Comment", "/** Address of server */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Address of server" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewProp_Address = { "Address", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FQosPingServerInfo, Address), METADATA_PARAMS(Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewProp_Address_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewProp_Address_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewProp_Port_MetaData[] = {
		{ "Comment", "/** Port of server */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Port of server" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewProp_Port = { "Port", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FQosPingServerInfo, Port), METADATA_PARAMS(Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewProp_Port_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewProp_Port_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewProp_Address,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::NewProp_Port,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Qos,
		nullptr,
		&NewStructOps,
		"QosPingServerInfo",
		sizeof(FQosPingServerInfo),
		alignof(FQosPingServerInfo),
		Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FQosPingServerInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FQosPingServerInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Qos();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("QosPingServerInfo"), sizeof(FQosPingServerInfo), Get_Z_Construct_UScriptStruct_FQosPingServerInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FQosPingServerInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FQosPingServerInfo_Hash() { return 2014296872U; }
	void UQosRegionManager::StaticRegisterNativesUQosRegionManager()
	{
	}
	UClass* Z_Construct_UClass_UQosRegionManager_NoRegister()
	{
		return UQosRegionManager::StaticClass();
	}
	struct Z_Construct_UClass_UQosRegionManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumTestsPerRegion_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumTestsPerRegion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PingTimeout_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PingTimeout;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RegionDefinitions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegionDefinitions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RegionDefinitions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DatacenterDefinitions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DatacenterDefinitions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DatacenterDefinitions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastCheckTimestamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastCheckTimestamp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Evaluator_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Evaluator;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_QosEvalResult_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_QosEvalResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_QosEvalResult;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RegionOptions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegionOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RegionOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForceRegionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ForceRegionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRegionForcedViaCommandline_MetaData[];
#endif
		static void NewProp_bRegionForcedViaCommandline_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRegionForcedViaCommandline;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedRegionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SelectedRegionId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UQosRegionManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Qos,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Main Qos interface for actions related to server quality of service\n */" },
		{ "IncludePath", "QosRegionManager.h" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Main Qos interface for actions related to server quality of service" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_NumTestsPerRegion_MetaData[] = {
		{ "Comment", "/** Number of times to ping a given region using random sampling of available servers */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Number of times to ping a given region using random sampling of available servers" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_NumTestsPerRegion = { "NumTestsPerRegion", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosRegionManager, NumTestsPerRegion), METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_NumTestsPerRegion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_NumTestsPerRegion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_PingTimeout_MetaData[] = {
		{ "Comment", "/** Timeout value for each ping request */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Timeout value for each ping request" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_PingTimeout = { "PingTimeout", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosRegionManager, PingTimeout), METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_PingTimeout_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_PingTimeout_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionDefinitions_Inner = { "RegionDefinitions", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FQosRegionInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionDefinitions_MetaData[] = {
		{ "Comment", "/** Metadata about existing regions */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Metadata about existing regions" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionDefinitions = { "RegionDefinitions", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosRegionManager, RegionDefinitions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionDefinitions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionDefinitions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_DatacenterDefinitions_Inner = { "DatacenterDefinitions", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FQosDatacenterInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_DatacenterDefinitions_MetaData[] = {
		{ "Comment", "/** Metadata about datacenters within existing regions */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Metadata about datacenters within existing regions" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_DatacenterDefinitions = { "DatacenterDefinitions", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosRegionManager, DatacenterDefinitions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_DatacenterDefinitions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_DatacenterDefinitions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_LastCheckTimestamp_MetaData[] = {
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_LastCheckTimestamp = { "LastCheckTimestamp", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosRegionManager, LastCheckTimestamp), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_LastCheckTimestamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_LastCheckTimestamp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_Evaluator_MetaData[] = {
		{ "Comment", "/** Reference to the evaluator for making datacenter determinations (null when not active) */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Reference to the evaluator for making datacenter determinations (null when not active)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_Evaluator = { "Evaluator", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosRegionManager, Evaluator), Z_Construct_UClass_UQosEvaluator_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_Evaluator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_Evaluator_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_QosEvalResult_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_QosEvalResult_MetaData[] = {
		{ "Comment", "/** Result of the last datacenter test */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Result of the last datacenter test" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_QosEvalResult = { "QosEvalResult", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosRegionManager, QosEvalResult), Z_Construct_UEnum_Qos_EQosCompletionResult, METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_QosEvalResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_QosEvalResult_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionOptions_Inner = { "RegionOptions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRegionQosInstance, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionOptions_MetaData[] = {
		{ "Comment", "/** Array of all known regions and the datacenters in them */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Array of all known regions and the datacenters in them" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionOptions = { "RegionOptions", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosRegionManager, RegionOptions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_ForceRegionId_MetaData[] = {
		{ "Comment", "/** Value forced to be the region (development) */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Value forced to be the region (development)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_ForceRegionId = { "ForceRegionId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosRegionManager, ForceRegionId), METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_ForceRegionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_ForceRegionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_bRegionForcedViaCommandline_MetaData[] = {
		{ "Comment", "/** Was the region forced via commandline */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Was the region forced via commandline" },
	};
#endif
	void Z_Construct_UClass_UQosRegionManager_Statics::NewProp_bRegionForcedViaCommandline_SetBit(void* Obj)
	{
		((UQosRegionManager*)Obj)->bRegionForcedViaCommandline = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_bRegionForcedViaCommandline = { "bRegionForcedViaCommandline", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UQosRegionManager), &Z_Construct_UClass_UQosRegionManager_Statics::NewProp_bRegionForcedViaCommandline_SetBit, METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_bRegionForcedViaCommandline_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_bRegionForcedViaCommandline_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQosRegionManager_Statics::NewProp_SelectedRegionId_MetaData[] = {
		{ "Comment", "/** Value set by the game to be the current region */" },
		{ "ModuleRelativePath", "Public/QosRegionManager.h" },
		{ "ToolTip", "Value set by the game to be the current region" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UQosRegionManager_Statics::NewProp_SelectedRegionId = { "SelectedRegionId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UQosRegionManager, SelectedRegionId), METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_SelectedRegionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::NewProp_SelectedRegionId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UQosRegionManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_NumTestsPerRegion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_PingTimeout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionDefinitions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionDefinitions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_DatacenterDefinitions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_DatacenterDefinitions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_LastCheckTimestamp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_Evaluator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_QosEvalResult_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_QosEvalResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionOptions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_RegionOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_ForceRegionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_bRegionForcedViaCommandline,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UQosRegionManager_Statics::NewProp_SelectedRegionId,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UQosRegionManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UQosRegionManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UQosRegionManager_Statics::ClassParams = {
		&UQosRegionManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UQosRegionManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UQosRegionManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UQosRegionManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UQosRegionManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UQosRegionManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UQosRegionManager, 964193432);
	template<> QOS_API UClass* StaticClass<UQosRegionManager>()
	{
		return UQosRegionManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UQosRegionManager(Z_Construct_UClass_UQosRegionManager, &UQosRegionManager::StaticClass, TEXT("/Script/Qos"), TEXT("UQosRegionManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UQosRegionManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
