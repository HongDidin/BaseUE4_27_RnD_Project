// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/SocialManager.h"
#include "Engine/Classes/Engine/GameInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialManager() {}
// Cross Module References
	PARTY_API UClass* Z_Construct_UClass_USocialManager_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Party();
	PARTY_API UClass* Z_Construct_UClass_USocialToolkit_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialDebugTools_NoRegister();
// End Cross Module References
	void USocialManager::StaticRegisterNativesUSocialManager()
	{
	}
	UClass* Z_Construct_UClass_USocialManager_NoRegister()
	{
		return USocialManager::StaticClass();
	}
	struct Z_Construct_UClass_USocialManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SocialToolkits_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SocialToolkits_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SocialToolkits;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SocialDebugTools_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SocialDebugTools;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialManager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Singleton manager at the top of the social framework */" },
		{ "IncludePath", "SocialManager.h" },
		{ "ModuleRelativePath", "Public/SocialManager.h" },
		{ "ToolTip", "Singleton manager at the top of the social framework" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialManager_Statics::NewProp_SocialToolkits_Inner = { "SocialToolkits", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_USocialToolkit_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialManager_Statics::NewProp_SocialToolkits_MetaData[] = {
		{ "ModuleRelativePath", "Public/SocialManager.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USocialManager_Statics::NewProp_SocialToolkits = { "SocialToolkits", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialManager, SocialToolkits), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USocialManager_Statics::NewProp_SocialToolkits_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialManager_Statics::NewProp_SocialToolkits_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialManager_Statics::NewProp_SocialDebugTools_MetaData[] = {
		{ "ModuleRelativePath", "Public/SocialManager.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialManager_Statics::NewProp_SocialDebugTools = { "SocialDebugTools", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialManager, SocialDebugTools), Z_Construct_UClass_USocialDebugTools_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USocialManager_Statics::NewProp_SocialDebugTools_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialManager_Statics::NewProp_SocialDebugTools_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USocialManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialManager_Statics::NewProp_SocialToolkits_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialManager_Statics::NewProp_SocialToolkits,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialManager_Statics::NewProp_SocialDebugTools,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialManager_Statics::ClassParams = {
		&USocialManager::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USocialManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USocialManager_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USocialManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialManager, 523831778);
	template<> PARTY_API UClass* StaticClass<USocialManager>()
	{
		return USocialManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialManager(Z_Construct_UClass_USocialManager, &USocialManager::StaticClass, TEXT("/Script/Party"), TEXT("USocialManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
