// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/Chat/SocialPrivateMessageChannel.h"
#include "Party/Public/Chat/SocialChatManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialPrivateMessageChannel() {}
// Cross Module References
	PARTY_API UClass* Z_Construct_UClass_USocialPrivateMessageChannel_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialPrivateMessageChannel();
	PARTY_API UClass* Z_Construct_UClass_USocialChatChannel();
	UPackage* Z_Construct_UPackage__Script_Party();
	PARTY_API UClass* Z_Construct_UClass_USocialUser_NoRegister();
// End Cross Module References
	void USocialPrivateMessageChannel::StaticRegisterNativesUSocialPrivateMessageChannel()
	{
	}
	UClass* Z_Construct_UClass_USocialPrivateMessageChannel_NoRegister()
	{
		return USocialPrivateMessageChannel::StaticClass();
	}
	struct Z_Construct_UClass_USocialPrivateMessageChannel_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetUser_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetUser;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialPrivateMessageChannel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USocialChatChannel,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialPrivateMessageChannel_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A modified version of a chat room that only contains two participants - the current user and a private recipient of their messages.\n * This is equivalent to sending a \"whisper\".\n */" },
		{ "IncludePath", "Chat/SocialPrivateMessageChannel.h" },
		{ "ModuleRelativePath", "Public/Chat/SocialPrivateMessageChannel.h" },
		{ "ToolTip", "A modified version of a chat room that only contains two participants - the current user and a private recipient of their messages.\nThis is equivalent to sending a \"whisper\"." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialPrivateMessageChannel_Statics::NewProp_TargetUser_MetaData[] = {
		{ "Comment", "/** The recipient of the current user's messages */" },
		{ "ModuleRelativePath", "Public/Chat/SocialPrivateMessageChannel.h" },
		{ "ToolTip", "The recipient of the current user's messages" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USocialPrivateMessageChannel_Statics::NewProp_TargetUser = { "TargetUser", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USocialPrivateMessageChannel, TargetUser), Z_Construct_UClass_USocialUser_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USocialPrivateMessageChannel_Statics::NewProp_TargetUser_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USocialPrivateMessageChannel_Statics::NewProp_TargetUser_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USocialPrivateMessageChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USocialPrivateMessageChannel_Statics::NewProp_TargetUser,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialPrivateMessageChannel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialPrivateMessageChannel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialPrivateMessageChannel_Statics::ClassParams = {
		&USocialPrivateMessageChannel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USocialPrivateMessageChannel_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USocialPrivateMessageChannel_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USocialPrivateMessageChannel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialPrivateMessageChannel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialPrivateMessageChannel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialPrivateMessageChannel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialPrivateMessageChannel, 2556796164);
	template<> PARTY_API UClass* StaticClass<USocialPrivateMessageChannel>()
	{
		return USocialPrivateMessageChannel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialPrivateMessageChannel(Z_Construct_UClass_USocialPrivateMessageChannel, &USocialPrivateMessageChannel::StaticClass, TEXT("/Script/Party"), TEXT("USocialPrivateMessageChannel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialPrivateMessageChannel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
