// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/Party/PartyTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePartyTypes() {}
// Cross Module References
	PARTY_API UEnum* Z_Construct_UEnum_Party_EApprovalAction();
	UPackage* Z_Construct_UPackage__Script_Party();
	PARTY_API UEnum* Z_Construct_UEnum_Party_EPartyJoinDenialReason();
	PARTY_API UEnum* Z_Construct_UEnum_Party_EPartyInviteRestriction();
	PARTY_API UEnum* Z_Construct_UEnum_Party_EPartyType();
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FOnlinePartyRepDataBase();
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FPartyPrivacySettings();
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FPartyPlatformSessionInfo();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FUniqueNetIdRepl();
// End Cross Module References
	static UEnum* EApprovalAction_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Party_EApprovalAction, Z_Construct_UPackage__Script_Party(), TEXT("EApprovalAction"));
		}
		return Singleton;
	}
	template<> PARTY_API UEnum* StaticEnum<EApprovalAction>()
	{
		return EApprovalAction_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EApprovalAction(EApprovalAction_StaticEnum, TEXT("/Script/Party"), TEXT("EApprovalAction"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Party_EApprovalAction_Hash() { return 4211527586U; }
	UEnum* Z_Construct_UEnum_Party_EApprovalAction()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EApprovalAction"), 0, Get_Z_Construct_UEnum_Party_EApprovalAction_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EApprovalAction::Approve", (int64)EApprovalAction::Approve },
				{ "EApprovalAction::Enqueue", (int64)EApprovalAction::Enqueue },
				{ "EApprovalAction::EnqueueAndStartBeacon", (int64)EApprovalAction::EnqueueAndStartBeacon },
				{ "EApprovalAction::Deny", (int64)EApprovalAction::Deny },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Approve.Name", "EApprovalAction::Approve" },
				{ "Deny.Name", "EApprovalAction::Deny" },
				{ "Enqueue.Name", "EApprovalAction::Enqueue" },
				{ "EnqueueAndStartBeacon.Name", "EApprovalAction::EnqueueAndStartBeacon" },
				{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Party,
				nullptr,
				"EApprovalAction",
				"EApprovalAction",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EPartyJoinDenialReason_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Party_EPartyJoinDenialReason, Z_Construct_UPackage__Script_Party(), TEXT("EPartyJoinDenialReason"));
		}
		return Singleton;
	}
	template<> PARTY_API UEnum* StaticEnum<EPartyJoinDenialReason>()
	{
		return EPartyJoinDenialReason_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPartyJoinDenialReason(EPartyJoinDenialReason_StaticEnum, TEXT("/Script/Party"), TEXT("EPartyJoinDenialReason"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Party_EPartyJoinDenialReason_Hash() { return 3373218212U; }
	UEnum* Z_Construct_UEnum_Party_EPartyJoinDenialReason()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPartyJoinDenialReason"), 0, Get_Z_Construct_UEnum_Party_EPartyJoinDenialReason_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPartyJoinDenialReason::NoReason", (int64)EPartyJoinDenialReason::NoReason },
				{ "EPartyJoinDenialReason::JoinAttemptAborted", (int64)EPartyJoinDenialReason::JoinAttemptAborted },
				{ "EPartyJoinDenialReason::Busy", (int64)EPartyJoinDenialReason::Busy },
				{ "EPartyJoinDenialReason::OssUnavailable", (int64)EPartyJoinDenialReason::OssUnavailable },
				{ "EPartyJoinDenialReason::PartyFull", (int64)EPartyJoinDenialReason::PartyFull },
				{ "EPartyJoinDenialReason::GameFull", (int64)EPartyJoinDenialReason::GameFull },
				{ "EPartyJoinDenialReason::NotPartyLeader", (int64)EPartyJoinDenialReason::NotPartyLeader },
				{ "EPartyJoinDenialReason::PartyPrivate", (int64)EPartyJoinDenialReason::PartyPrivate },
				{ "EPartyJoinDenialReason::JoinerCrossplayRestricted", (int64)EPartyJoinDenialReason::JoinerCrossplayRestricted },
				{ "EPartyJoinDenialReason::MemberCrossplayRestricted", (int64)EPartyJoinDenialReason::MemberCrossplayRestricted },
				{ "EPartyJoinDenialReason::GameModeRestricted", (int64)EPartyJoinDenialReason::GameModeRestricted },
				{ "EPartyJoinDenialReason::Banned", (int64)EPartyJoinDenialReason::Banned },
				{ "EPartyJoinDenialReason::NotLoggedIn", (int64)EPartyJoinDenialReason::NotLoggedIn },
				{ "EPartyJoinDenialReason::CheckingForRejoin", (int64)EPartyJoinDenialReason::CheckingForRejoin },
				{ "EPartyJoinDenialReason::TargetUserMissingPresence", (int64)EPartyJoinDenialReason::TargetUserMissingPresence },
				{ "EPartyJoinDenialReason::TargetUserUnjoinable", (int64)EPartyJoinDenialReason::TargetUserUnjoinable },
				{ "EPartyJoinDenialReason::TargetUserAway", (int64)EPartyJoinDenialReason::TargetUserAway },
				{ "EPartyJoinDenialReason::AlreadyLeaderInPlatformSession", (int64)EPartyJoinDenialReason::AlreadyLeaderInPlatformSession },
				{ "EPartyJoinDenialReason::TargetUserPlayingDifferentGame", (int64)EPartyJoinDenialReason::TargetUserPlayingDifferentGame },
				{ "EPartyJoinDenialReason::TargetUserMissingPlatformSession", (int64)EPartyJoinDenialReason::TargetUserMissingPlatformSession },
				{ "EPartyJoinDenialReason::PlatformSessionMissingJoinInfo", (int64)EPartyJoinDenialReason::PlatformSessionMissingJoinInfo },
				{ "EPartyJoinDenialReason::FailedToStartFindConsoleSession", (int64)EPartyJoinDenialReason::FailedToStartFindConsoleSession },
				{ "EPartyJoinDenialReason::MissingPartyClassForTypeId", (int64)EPartyJoinDenialReason::MissingPartyClassForTypeId },
				{ "EPartyJoinDenialReason::TargetUserBlocked", (int64)EPartyJoinDenialReason::TargetUserBlocked },
				{ "EPartyJoinDenialReason::CustomReason0", (int64)EPartyJoinDenialReason::CustomReason0 },
				{ "EPartyJoinDenialReason::CustomReason1", (int64)EPartyJoinDenialReason::CustomReason1 },
				{ "EPartyJoinDenialReason::CustomReason2", (int64)EPartyJoinDenialReason::CustomReason2 },
				{ "EPartyJoinDenialReason::CustomReason3", (int64)EPartyJoinDenialReason::CustomReason3 },
				{ "EPartyJoinDenialReason::CustomReason4", (int64)EPartyJoinDenialReason::CustomReason4 },
				{ "EPartyJoinDenialReason::CustomReason5", (int64)EPartyJoinDenialReason::CustomReason5 },
				{ "EPartyJoinDenialReason::CustomReason6", (int64)EPartyJoinDenialReason::CustomReason6 },
				{ "EPartyJoinDenialReason::CustomReason7", (int64)EPartyJoinDenialReason::CustomReason7 },
				{ "EPartyJoinDenialReason::CustomReason8", (int64)EPartyJoinDenialReason::CustomReason8 },
				{ "EPartyJoinDenialReason::CustomReason9", (int64)EPartyJoinDenialReason::CustomReason9 },
				{ "EPartyJoinDenialReason::CustomReason10", (int64)EPartyJoinDenialReason::CustomReason10 },
				{ "EPartyJoinDenialReason::CustomReason11", (int64)EPartyJoinDenialReason::CustomReason11 },
				{ "EPartyJoinDenialReason::CustomReason12", (int64)EPartyJoinDenialReason::CustomReason12 },
				{ "EPartyJoinDenialReason::CustomReason13", (int64)EPartyJoinDenialReason::CustomReason13 },
				{ "EPartyJoinDenialReason::CustomReason14", (int64)EPartyJoinDenialReason::CustomReason14 },
				{ "EPartyJoinDenialReason::CustomReason15", (int64)EPartyJoinDenialReason::CustomReason15 },
				{ "EPartyJoinDenialReason::CustomReason16", (int64)EPartyJoinDenialReason::CustomReason16 },
				{ "EPartyJoinDenialReason::CustomReason17", (int64)EPartyJoinDenialReason::CustomReason17 },
				{ "EPartyJoinDenialReason::CustomReason18", (int64)EPartyJoinDenialReason::CustomReason18 },
				{ "EPartyJoinDenialReason::CustomReason19", (int64)EPartyJoinDenialReason::CustomReason19 },
				{ "EPartyJoinDenialReason::CustomReason20", (int64)EPartyJoinDenialReason::CustomReason20 },
				{ "EPartyJoinDenialReason::CustomReason21", (int64)EPartyJoinDenialReason::CustomReason21 },
				{ "EPartyJoinDenialReason::CustomReason22", (int64)EPartyJoinDenialReason::CustomReason22 },
				{ "EPartyJoinDenialReason::CustomReason23", (int64)EPartyJoinDenialReason::CustomReason23 },
				{ "EPartyJoinDenialReason::CustomReason24", (int64)EPartyJoinDenialReason::CustomReason24 },
				{ "EPartyJoinDenialReason::CustomReason25", (int64)EPartyJoinDenialReason::CustomReason25 },
				{ "EPartyJoinDenialReason::CustomReason26", (int64)EPartyJoinDenialReason::CustomReason26 },
				{ "EPartyJoinDenialReason::CustomReason27", (int64)EPartyJoinDenialReason::CustomReason27 },
				{ "EPartyJoinDenialReason::CustomReason28", (int64)EPartyJoinDenialReason::CustomReason28 },
				{ "EPartyJoinDenialReason::CustomReason29", (int64)EPartyJoinDenialReason::CustomReason29 },
				{ "EPartyJoinDenialReason::CustomReason30", (int64)EPartyJoinDenialReason::CustomReason30 },
				{ "EPartyJoinDenialReason::CustomReason31", (int64)EPartyJoinDenialReason::CustomReason31 },
				{ "EPartyJoinDenialReason::CustomReason32", (int64)EPartyJoinDenialReason::CustomReason32 },
				{ "EPartyJoinDenialReason::CustomReason33", (int64)EPartyJoinDenialReason::CustomReason33 },
				{ "EPartyJoinDenialReason::CustomReason34", (int64)EPartyJoinDenialReason::CustomReason34 },
				{ "EPartyJoinDenialReason::CustomReason35", (int64)EPartyJoinDenialReason::CustomReason35 },
				{ "EPartyJoinDenialReason::CustomReason36", (int64)EPartyJoinDenialReason::CustomReason36 },
				{ "EPartyJoinDenialReason::CustomReason37", (int64)EPartyJoinDenialReason::CustomReason37 },
				{ "EPartyJoinDenialReason::CustomReason38", (int64)EPartyJoinDenialReason::CustomReason38 },
				{ "EPartyJoinDenialReason::CustomReason39", (int64)EPartyJoinDenialReason::CustomReason39 },
				{ "EPartyJoinDenialReason::MAX", (int64)EPartyJoinDenialReason::MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AlreadyLeaderInPlatformSession.Comment", "/** We found ourself to be the leader of the friend's party according to the console session */" },
				{ "AlreadyLeaderInPlatformSession.Name", "EPartyJoinDenialReason::AlreadyLeaderInPlatformSession" },
				{ "AlreadyLeaderInPlatformSession.ToolTip", "We found ourself to be the leader of the friend's party according to the console session" },
				{ "Banned.Comment", "/** Player is currently banned */" },
				{ "Banned.Name", "EPartyJoinDenialReason::Banned" },
				{ "Banned.ToolTip", "Player is currently banned" },
				{ "Busy.Comment", "/** Party leader is busy or at inopportune time to allow joins - to be used as a fallback when there isn't a more specific reason (more specific reasons are preferred) */" },
				{ "Busy.Name", "EPartyJoinDenialReason::Busy" },
				{ "Busy.ToolTip", "Party leader is busy or at inopportune time to allow joins - to be used as a fallback when there isn't a more specific reason (more specific reasons are preferred)" },
				{ "CheckingForRejoin.Comment", "/** Unable to start joining - we are checking for a session to rejoin */" },
				{ "CheckingForRejoin.Name", "EPartyJoinDenialReason::CheckingForRejoin" },
				{ "CheckingForRejoin.ToolTip", "Unable to start joining - we are checking for a session to rejoin" },
				{ "CustomReason0.Comment", "/**\n\x09* Customizable denial reasons.\n\x09* Expected usage is to assign the entries in the custom enum to the arbitrary custom entry placeholders below.\n\x09* App level users of the system can then cast to/from their custom enum as desired.\n\x09*/" },
				{ "CustomReason0.Name", "EPartyJoinDenialReason::CustomReason0" },
				{ "CustomReason0.ToolTip", "Customizable denial reasons.\nExpected usage is to assign the entries in the custom enum to the arbitrary custom entry placeholders below.\nApp level users of the system can then cast to/from their custom enum as desired." },
				{ "CustomReason1.Name", "EPartyJoinDenialReason::CustomReason1" },
				{ "CustomReason10.Name", "EPartyJoinDenialReason::CustomReason10" },
				{ "CustomReason11.Name", "EPartyJoinDenialReason::CustomReason11" },
				{ "CustomReason12.Name", "EPartyJoinDenialReason::CustomReason12" },
				{ "CustomReason13.Name", "EPartyJoinDenialReason::CustomReason13" },
				{ "CustomReason14.Name", "EPartyJoinDenialReason::CustomReason14" },
				{ "CustomReason15.Name", "EPartyJoinDenialReason::CustomReason15" },
				{ "CustomReason16.Name", "EPartyJoinDenialReason::CustomReason16" },
				{ "CustomReason17.Name", "EPartyJoinDenialReason::CustomReason17" },
				{ "CustomReason18.Name", "EPartyJoinDenialReason::CustomReason18" },
				{ "CustomReason19.Name", "EPartyJoinDenialReason::CustomReason19" },
				{ "CustomReason2.Name", "EPartyJoinDenialReason::CustomReason2" },
				{ "CustomReason20.Name", "EPartyJoinDenialReason::CustomReason20" },
				{ "CustomReason21.Name", "EPartyJoinDenialReason::CustomReason21" },
				{ "CustomReason22.Name", "EPartyJoinDenialReason::CustomReason22" },
				{ "CustomReason23.Name", "EPartyJoinDenialReason::CustomReason23" },
				{ "CustomReason24.Name", "EPartyJoinDenialReason::CustomReason24" },
				{ "CustomReason25.Name", "EPartyJoinDenialReason::CustomReason25" },
				{ "CustomReason26.Name", "EPartyJoinDenialReason::CustomReason26" },
				{ "CustomReason27.Name", "EPartyJoinDenialReason::CustomReason27" },
				{ "CustomReason28.Name", "EPartyJoinDenialReason::CustomReason28" },
				{ "CustomReason29.Name", "EPartyJoinDenialReason::CustomReason29" },
				{ "CustomReason3.Name", "EPartyJoinDenialReason::CustomReason3" },
				{ "CustomReason30.Name", "EPartyJoinDenialReason::CustomReason30" },
				{ "CustomReason31.Name", "EPartyJoinDenialReason::CustomReason31" },
				{ "CustomReason32.Name", "EPartyJoinDenialReason::CustomReason32" },
				{ "CustomReason33.Name", "EPartyJoinDenialReason::CustomReason33" },
				{ "CustomReason34.Name", "EPartyJoinDenialReason::CustomReason34" },
				{ "CustomReason35.Name", "EPartyJoinDenialReason::CustomReason35" },
				{ "CustomReason36.Name", "EPartyJoinDenialReason::CustomReason36" },
				{ "CustomReason37.Name", "EPartyJoinDenialReason::CustomReason37" },
				{ "CustomReason38.Name", "EPartyJoinDenialReason::CustomReason38" },
				{ "CustomReason39.Name", "EPartyJoinDenialReason::CustomReason39" },
				{ "CustomReason4.Name", "EPartyJoinDenialReason::CustomReason4" },
				{ "CustomReason5.Name", "EPartyJoinDenialReason::CustomReason5" },
				{ "CustomReason6.Name", "EPartyJoinDenialReason::CustomReason6" },
				{ "CustomReason7.Name", "EPartyJoinDenialReason::CustomReason7" },
				{ "CustomReason8.Name", "EPartyJoinDenialReason::CustomReason8" },
				{ "CustomReason9.Name", "EPartyJoinDenialReason::CustomReason9" },
				{ "FailedToStartFindConsoleSession.Comment", "/** We were unable to launch the query to find the platform friend's session (platform friends only) */" },
				{ "FailedToStartFindConsoleSession.Name", "EPartyJoinDenialReason::FailedToStartFindConsoleSession" },
				{ "FailedToStartFindConsoleSession.ToolTip", "We were unable to launch the query to find the platform friend's session (platform friends only)" },
				{ "GameFull.Comment", "/** Game is full, but not party */" },
				{ "GameFull.Name", "EPartyJoinDenialReason::GameFull" },
				{ "GameFull.ToolTip", "Game is full, but not party" },
				{ "GameModeRestricted.Comment", "/** Player is in a game mode that restricts joining */" },
				{ "GameModeRestricted.Name", "EPartyJoinDenialReason::GameModeRestricted" },
				{ "GameModeRestricted.ToolTip", "Player is in a game mode that restricts joining" },
				{ "JoinAttemptAborted.Comment", "/** The local player aborted the join attempt */" },
				{ "JoinAttemptAborted.Name", "EPartyJoinDenialReason::JoinAttemptAborted" },
				{ "JoinAttemptAborted.ToolTip", "The local player aborted the join attempt" },
				{ "JoinerCrossplayRestricted.Comment", "/** Player has crossplay restriction that would be violated */" },
				{ "JoinerCrossplayRestricted.Name", "EPartyJoinDenialReason::JoinerCrossplayRestricted" },
				{ "JoinerCrossplayRestricted.ToolTip", "Player has crossplay restriction that would be violated" },
				{ "MAX.Name", "EPartyJoinDenialReason::MAX" },
				{ "MemberCrossplayRestricted.Comment", "/** Party member has crossplay restriction that would be violated */" },
				{ "MemberCrossplayRestricted.Name", "EPartyJoinDenialReason::MemberCrossplayRestricted" },
				{ "MemberCrossplayRestricted.ToolTip", "Party member has crossplay restriction that would be violated" },
				{ "MissingPartyClassForTypeId.Comment", "/** The party is of a type that the game does not support (it specified nullptr for the USocialParty class) */" },
				{ "MissingPartyClassForTypeId.Name", "EPartyJoinDenialReason::MissingPartyClassForTypeId" },
				{ "MissingPartyClassForTypeId.ToolTip", "The party is of a type that the game does not support (it specified nullptr for the USocialParty class)" },
				{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
				{ "NoReason.Comment", "/** No denial, matches success internally */" },
				{ "NoReason.Name", "EPartyJoinDenialReason::NoReason" },
				{ "NoReason.ToolTip", "No denial, matches success internally" },
				{ "NotLoggedIn.Comment", "/** Player is not yet logged in */" },
				{ "NotLoggedIn.Name", "EPartyJoinDenialReason::NotLoggedIn" },
				{ "NotLoggedIn.ToolTip", "Player is not yet logged in" },
				{ "NotPartyLeader.Comment", "/** Asked a non party leader to join game, shouldn't happen */" },
				{ "NotPartyLeader.Name", "EPartyJoinDenialReason::NotPartyLeader" },
				{ "NotPartyLeader.ToolTip", "Asked a non party leader to join game, shouldn't happen" },
				{ "OssUnavailable.Comment", "/** Either the necessary OSS itself or critical element thereof (PartyInterface, SessionInterface, etc.) is missing. */" },
				{ "OssUnavailable.Name", "EPartyJoinDenialReason::OssUnavailable" },
				{ "OssUnavailable.ToolTip", "Either the necessary OSS itself or critical element thereof (PartyInterface, SessionInterface, etc.) is missing." },
				{ "PartyFull.Comment", "/** Party is full */" },
				{ "PartyFull.Name", "EPartyJoinDenialReason::PartyFull" },
				{ "PartyFull.ToolTip", "Party is full" },
				{ "PartyPrivate.Comment", "/** Party has been marked as private and the join request is revoked */" },
				{ "PartyPrivate.Name", "EPartyJoinDenialReason::PartyPrivate" },
				{ "PartyPrivate.ToolTip", "Party has been marked as private and the join request is revoked" },
				{ "PlatformSessionMissingJoinInfo.Comment", "/** There is no party join info available in the target user's platform session */" },
				{ "PlatformSessionMissingJoinInfo.Name", "EPartyJoinDenialReason::PlatformSessionMissingJoinInfo" },
				{ "PlatformSessionMissingJoinInfo.ToolTip", "There is no party join info available in the target user's platform session" },
				{ "TargetUserAway.Comment", "/** The target user is currently Away */" },
				{ "TargetUserAway.Name", "EPartyJoinDenialReason::TargetUserAway" },
				{ "TargetUserAway.ToolTip", "The target user is currently Away" },
				{ "TargetUserBlocked.Comment", "/** The target user is blocked by the local user on one or more of the active subsystems */" },
				{ "TargetUserBlocked.Name", "EPartyJoinDenialReason::TargetUserBlocked" },
				{ "TargetUserBlocked.ToolTip", "The target user is blocked by the local user on one or more of the active subsystems" },
				{ "TargetUserMissingPlatformSession.Comment", "/** The target user's presence does not have any information about their party session (platform friends only) */" },
				{ "TargetUserMissingPlatformSession.Name", "EPartyJoinDenialReason::TargetUserMissingPlatformSession" },
				{ "TargetUserMissingPlatformSession.ToolTip", "The target user's presence does not have any information about their party session (platform friends only)" },
				{ "TargetUserMissingPresence.Comment", "/** The target user is missing presence info */" },
				{ "TargetUserMissingPresence.Name", "EPartyJoinDenialReason::TargetUserMissingPresence" },
				{ "TargetUserMissingPresence.ToolTip", "The target user is missing presence info" },
				{ "TargetUserPlayingDifferentGame.Comment", "/** The target user is not playing the same game as us */" },
				{ "TargetUserPlayingDifferentGame.Name", "EPartyJoinDenialReason::TargetUserPlayingDifferentGame" },
				{ "TargetUserPlayingDifferentGame.ToolTip", "The target user is not playing the same game as us" },
				{ "TargetUserUnjoinable.Comment", "/** The target user's presence says the user is unjoinable */" },
				{ "TargetUserUnjoinable.Name", "EPartyJoinDenialReason::TargetUserUnjoinable" },
				{ "TargetUserUnjoinable.ToolTip", "The target user's presence says the user is unjoinable" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Party,
				nullptr,
				"EPartyJoinDenialReason",
				"EPartyJoinDenialReason",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EPartyInviteRestriction_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Party_EPartyInviteRestriction, Z_Construct_UPackage__Script_Party(), TEXT("EPartyInviteRestriction"));
		}
		return Singleton;
	}
	template<> PARTY_API UEnum* StaticEnum<EPartyInviteRestriction>()
	{
		return EPartyInviteRestriction_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPartyInviteRestriction(EPartyInviteRestriction_StaticEnum, TEXT("/Script/Party"), TEXT("EPartyInviteRestriction"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Party_EPartyInviteRestriction_Hash() { return 932973295U; }
	UEnum* Z_Construct_UEnum_Party_EPartyInviteRestriction()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPartyInviteRestriction"), 0, Get_Z_Construct_UEnum_Party_EPartyInviteRestriction_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPartyInviteRestriction::AnyMember", (int64)EPartyInviteRestriction::AnyMember },
				{ "EPartyInviteRestriction::LeaderOnly", (int64)EPartyInviteRestriction::LeaderOnly },
				{ "EPartyInviteRestriction::NoInvites", (int64)EPartyInviteRestriction::NoInvites },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AnyMember.Comment", "/** Any party member can send invites */" },
				{ "AnyMember.Name", "EPartyInviteRestriction::AnyMember" },
				{ "AnyMember.ToolTip", "Any party member can send invites" },
				{ "LeaderOnly.Comment", "/** Only the leader can send invites */" },
				{ "LeaderOnly.Name", "EPartyInviteRestriction::LeaderOnly" },
				{ "LeaderOnly.ToolTip", "Only the leader can send invites" },
				{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
				{ "NoInvites.Comment", "/** Nobody can invite anyone to this party */" },
				{ "NoInvites.Name", "EPartyInviteRestriction::NoInvites" },
				{ "NoInvites.ToolTip", "Nobody can invite anyone to this party" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Party,
				nullptr,
				"EPartyInviteRestriction",
				"EPartyInviteRestriction",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EPartyType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Party_EPartyType, Z_Construct_UPackage__Script_Party(), TEXT("EPartyType"));
		}
		return Singleton;
	}
	template<> PARTY_API UEnum* StaticEnum<EPartyType>()
	{
		return EPartyType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPartyType(EPartyType_StaticEnum, TEXT("/Script/Party"), TEXT("EPartyType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Party_EPartyType_Hash() { return 2220103735U; }
	UEnum* Z_Construct_UEnum_Party_EPartyType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPartyType"), 0, Get_Z_Construct_UEnum_Party_EPartyType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPartyType::Public", (int64)EPartyType::Public },
				{ "EPartyType::FriendsOnly", (int64)EPartyType::FriendsOnly },
				{ "EPartyType::Private", (int64)EPartyType::Private },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "FriendsOnly.Comment", "/** This party is joinable by friends */" },
				{ "FriendsOnly.Name", "EPartyType::FriendsOnly" },
				{ "FriendsOnly.ToolTip", "This party is joinable by friends" },
				{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
				{ "Private.Comment", "/** This party requires an invite from someone within the party */" },
				{ "Private.Name", "EPartyType::Private" },
				{ "Private.ToolTip", "This party requires an invite from someone within the party" },
				{ "Public.Comment", "/** This party is public (not really supported right now) */" },
				{ "Public.Name", "EPartyType::Public" },
				{ "Public.ToolTip", "This party is public (not really supported right now)" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Party,
				nullptr,
				"EPartyType",
				"EPartyType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FOnlinePartyRepDataBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PARTY_API uint32 Get_Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOnlinePartyRepDataBase, Z_Construct_UPackage__Script_Party(), TEXT("OnlinePartyRepDataBase"), sizeof(FOnlinePartyRepDataBase), Get_Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Hash());
	}
	return Singleton;
}
template<> PARTY_API UScriptStruct* StaticStruct<FOnlinePartyRepDataBase>()
{
	return FOnlinePartyRepDataBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOnlinePartyRepDataBase(FOnlinePartyRepDataBase::StaticStruct, TEXT("/Script/Party"), TEXT("OnlinePartyRepDataBase"), false, nullptr, nullptr);
static struct FScriptStruct_Party_StaticRegisterNativesFOnlinePartyRepDataBase
{
	FScriptStruct_Party_StaticRegisterNativesFOnlinePartyRepDataBase()
	{
		UScriptStruct::DeferCppStructOps<FOnlinePartyRepDataBase>(FName(TEXT("OnlinePartyRepDataBase")));
	}
} ScriptStruct_Party_StaticRegisterNativesFOnlinePartyRepDataBase;
	struct Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Base for all rep data structs */" },
		{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
		{ "ToolTip", "Base for all rep data structs" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOnlinePartyRepDataBase>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
		nullptr,
		&NewStructOps,
		"OnlinePartyRepDataBase",
		sizeof(FOnlinePartyRepDataBase),
		alignof(FOnlinePartyRepDataBase),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOnlinePartyRepDataBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OnlinePartyRepDataBase"), sizeof(FOnlinePartyRepDataBase), Get_Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOnlinePartyRepDataBase_Hash() { return 4130758469U; }
class UScriptStruct* FPartyPrivacySettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PARTY_API uint32 Get_Z_Construct_UScriptStruct_FPartyPrivacySettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPartyPrivacySettings, Z_Construct_UPackage__Script_Party(), TEXT("PartyPrivacySettings"), sizeof(FPartyPrivacySettings), Get_Z_Construct_UScriptStruct_FPartyPrivacySettings_Hash());
	}
	return Singleton;
}
template<> PARTY_API UScriptStruct* StaticStruct<FPartyPrivacySettings>()
{
	return FPartyPrivacySettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPartyPrivacySettings(FPartyPrivacySettings::StaticStruct, TEXT("/Script/Party"), TEXT("PartyPrivacySettings"), false, nullptr, nullptr);
static struct FScriptStruct_Party_StaticRegisterNativesFPartyPrivacySettings
{
	FScriptStruct_Party_StaticRegisterNativesFPartyPrivacySettings()
	{
		UScriptStruct::DeferCppStructOps<FPartyPrivacySettings>(FName(TEXT("PartyPrivacySettings")));
	}
} ScriptStruct_Party_StaticRegisterNativesFPartyPrivacySettings;
	struct Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PartyType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PartyType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PartyType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PartyInviteRestriction_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PartyInviteRestriction_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PartyInviteRestriction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyLeaderFriendsCanJoin_MetaData[];
#endif
		static void NewProp_bOnlyLeaderFriendsCanJoin_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyLeaderFriendsCanJoin;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPartyPrivacySettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyType_MetaData[] = {
		{ "Comment", "/** The type of party in terms of advertised joinability restrictions */" },
		{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
		{ "ToolTip", "The type of party in terms of advertised joinability restrictions" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyType = { "PartyType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyPrivacySettings, PartyType), Z_Construct_UEnum_Party_EPartyType, METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyInviteRestriction_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyInviteRestriction_MetaData[] = {
		{ "Comment", "/** Who is allowed to send invitataions to the party? */" },
		{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
		{ "ToolTip", "Who is allowed to send invitataions to the party?" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyInviteRestriction = { "PartyInviteRestriction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyPrivacySettings, PartyInviteRestriction), Z_Construct_UEnum_Party_EPartyInviteRestriction, METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyInviteRestriction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyInviteRestriction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_bOnlyLeaderFriendsCanJoin_MetaData[] = {
		{ "Comment", "/** True to restrict the party exclusively to friends of the party leader */" },
		{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
		{ "ToolTip", "True to restrict the party exclusively to friends of the party leader" },
	};
#endif
	void Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_bOnlyLeaderFriendsCanJoin_SetBit(void* Obj)
	{
		((FPartyPrivacySettings*)Obj)->bOnlyLeaderFriendsCanJoin = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_bOnlyLeaderFriendsCanJoin = { "bOnlyLeaderFriendsCanJoin", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FPartyPrivacySettings), &Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_bOnlyLeaderFriendsCanJoin_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_bOnlyLeaderFriendsCanJoin_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_bOnlyLeaderFriendsCanJoin_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyInviteRestriction_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_PartyInviteRestriction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::NewProp_bOnlyLeaderFriendsCanJoin,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
		nullptr,
		&NewStructOps,
		"PartyPrivacySettings",
		sizeof(FPartyPrivacySettings),
		alignof(FPartyPrivacySettings),
		Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPartyPrivacySettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPartyPrivacySettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PartyPrivacySettings"), sizeof(FPartyPrivacySettings), Get_Z_Construct_UScriptStruct_FPartyPrivacySettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPartyPrivacySettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPartyPrivacySettings_Hash() { return 4134063579U; }
class UScriptStruct* FPartyPlatformSessionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PARTY_API uint32 Get_Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPartyPlatformSessionInfo, Z_Construct_UPackage__Script_Party(), TEXT("PartyPlatformSessionInfo"), sizeof(FPartyPlatformSessionInfo), Get_Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Hash());
	}
	return Singleton;
}
template<> PARTY_API UScriptStruct* StaticStruct<FPartyPlatformSessionInfo>()
{
	return FPartyPlatformSessionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPartyPlatformSessionInfo(FPartyPlatformSessionInfo::StaticStruct, TEXT("/Script/Party"), TEXT("PartyPlatformSessionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Party_StaticRegisterNativesFPartyPlatformSessionInfo
{
	FScriptStruct_Party_StaticRegisterNativesFPartyPlatformSessionInfo()
	{
		UScriptStruct::DeferCppStructOps<FPartyPlatformSessionInfo>(FName(TEXT("PartyPlatformSessionInfo")));
	}
} ScriptStruct_Party_StaticRegisterNativesFPartyPlatformSessionInfo;
	struct Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SessionType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerPrimaryId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OwnerPrimaryId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPartyPlatformSessionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_SessionType_MetaData[] = {
		{ "Comment", "/** The platform session type (because in a crossplay party, members can be in different session types) */" },
		{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
		{ "ToolTip", "The platform session type (because in a crossplay party, members can be in different session types)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_SessionType = { "SessionType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyPlatformSessionInfo, SessionType), METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_SessionType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_SessionType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_SessionId_MetaData[] = {
		{ "Comment", "/** The platform session id. Will be unset if it is not yet available to be joined. */" },
		{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
		{ "ToolTip", "The platform session id. Will be unset if it is not yet available to be joined." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyPlatformSessionInfo, SessionId), METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_OwnerPrimaryId_MetaData[] = {
		{ "Comment", "/** Primary OSS ID of the player that owns this console session */" },
		{ "ModuleRelativePath", "Public/Party/PartyTypes.h" },
		{ "ToolTip", "Primary OSS ID of the player that owns this console session" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_OwnerPrimaryId = { "OwnerPrimaryId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyPlatformSessionInfo, OwnerPrimaryId), Z_Construct_UScriptStruct_FUniqueNetIdRepl, METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_OwnerPrimaryId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_OwnerPrimaryId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_SessionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::NewProp_OwnerPrimaryId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
		nullptr,
		&NewStructOps,
		"PartyPlatformSessionInfo",
		sizeof(FPartyPlatformSessionInfo),
		alignof(FPartyPlatformSessionInfo),
		Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPartyPlatformSessionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PartyPlatformSessionInfo"), sizeof(FPartyPlatformSessionInfo), Get_Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPartyPlatformSessionInfo_Hash() { return 1604247634U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
