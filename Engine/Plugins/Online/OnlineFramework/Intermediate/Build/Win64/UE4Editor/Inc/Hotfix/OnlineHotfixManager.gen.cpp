// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Hotfix/Public/OnlineHotfixManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOnlineHotfixManager() {}
// Cross Module References
	HOTFIX_API UEnum* Z_Construct_UEnum_Hotfix_EHotfixResult();
	UPackage* Z_Construct_UPackage__Script_Hotfix();
	HOTFIX_API UClass* Z_Construct_UClass_UOnlineHotfixManager_NoRegister();
	HOTFIX_API UClass* Z_Construct_UClass_UOnlineHotfixManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	static UEnum* EHotfixResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Hotfix_EHotfixResult, Z_Construct_UPackage__Script_Hotfix(), TEXT("EHotfixResult"));
		}
		return Singleton;
	}
	template<> HOTFIX_API UEnum* StaticEnum<EHotfixResult>()
	{
		return EHotfixResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHotfixResult(EHotfixResult_StaticEnum, TEXT("/Script/Hotfix"), TEXT("EHotfixResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Hotfix_EHotfixResult_Hash() { return 1373577092U; }
	UEnum* Z_Construct_UEnum_Hotfix_EHotfixResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Hotfix();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHotfixResult"), 0, Get_Z_Construct_UEnum_Hotfix_EHotfixResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHotfixResult::Failed", (int64)EHotfixResult::Failed },
				{ "EHotfixResult::Success", (int64)EHotfixResult::Success },
				{ "EHotfixResult::SuccessNoChange", (int64)EHotfixResult::SuccessNoChange },
				{ "EHotfixResult::SuccessNeedsReload", (int64)EHotfixResult::SuccessNeedsReload },
				{ "EHotfixResult::SuccessNeedsRelaunch", (int64)EHotfixResult::SuccessNeedsRelaunch },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Failed.Comment", "/** Failed to apply the hotfix */" },
				{ "Failed.Name", "EHotfixResult::Failed" },
				{ "Failed.ToolTip", "Failed to apply the hotfix" },
				{ "ModuleRelativePath", "Public/OnlineHotfixManager.h" },
				{ "Success.Comment", "/** Hotfix succeeded and is ready to go */" },
				{ "Success.Name", "EHotfixResult::Success" },
				{ "Success.ToolTip", "Hotfix succeeded and is ready to go" },
				{ "SuccessNeedsRelaunch.Comment", "/** Hotfix succeeded and requires the process restarted to take effect */" },
				{ "SuccessNeedsRelaunch.Name", "EHotfixResult::SuccessNeedsRelaunch" },
				{ "SuccessNeedsRelaunch.ToolTip", "Hotfix succeeded and requires the process restarted to take effect" },
				{ "SuccessNeedsReload.Comment", "/** Hotfix succeeded and requires the current level to be reloaded to take effect */" },
				{ "SuccessNeedsReload.Name", "EHotfixResult::SuccessNeedsReload" },
				{ "SuccessNeedsReload.ToolTip", "Hotfix succeeded and requires the current level to be reloaded to take effect" },
				{ "SuccessNoChange.Comment", "/** Hotfix process succeeded but there were no changes applied */" },
				{ "SuccessNoChange.Name", "EHotfixResult::SuccessNoChange" },
				{ "SuccessNoChange.ToolTip", "Hotfix process succeeded but there were no changes applied" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Hotfix,
				nullptr,
				"EHotfixResult",
				"EHotfixResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UOnlineHotfixManager::execStartHotfixProcess)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartHotfixProcess();
		P_NATIVE_END;
	}
	void UOnlineHotfixManager::StaticRegisterNativesUOnlineHotfixManager()
	{
		UClass* Class = UOnlineHotfixManager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "StartHotfixProcess", &UOnlineHotfixManager::execStartHotfixProcess },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOnlineHotfixManager_StartHotfixProcess_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOnlineHotfixManager_StartHotfixProcess_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hotfix" },
		{ "Comment", "/** Starts the fetching of hotfix data from the OnlineTitleFileInterface that is registered for this game */" },
		{ "ModuleRelativePath", "Public/OnlineHotfixManager.h" },
		{ "ToolTip", "Starts the fetching of hotfix data from the OnlineTitleFileInterface that is registered for this game" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOnlineHotfixManager_StartHotfixProcess_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOnlineHotfixManager, nullptr, "StartHotfixProcess", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOnlineHotfixManager_StartHotfixProcess_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOnlineHotfixManager_StartHotfixProcess_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOnlineHotfixManager_StartHotfixProcess()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOnlineHotfixManager_StartHotfixProcess_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UOnlineHotfixManager_NoRegister()
	{
		return UOnlineHotfixManager::StaticClass();
	}
	struct Z_Construct_UClass_UOnlineHotfixManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OSSName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OSSName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HotfixManagerClassName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_HotfixManagerClassName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugPrefix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DebugPrefix;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetsHotfixedFromIniFiles_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetsHotfixedFromIniFiles_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AssetsHotfixedFromIniFiles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOnlineHotfixManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Hotfix,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOnlineHotfixManager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOnlineHotfixManager_StartHotfixProcess, "StartHotfixProcess" }, // 1584871157
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOnlineHotfixManager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * This class manages the downloading and application of hotfix data\n * Hotfix data is a set of non-executable files downloaded and applied to the game.\n * The base implementation knows how to handle INI, PAK, and locres files.\n * NOTE: Each INI/PAK file must be prefixed by the platform name they are targeted at\n */" },
		{ "IncludePath", "OnlineHotfixManager.h" },
		{ "ModuleRelativePath", "Public/OnlineHotfixManager.h" },
		{ "ToolTip", "This class manages the downloading and application of hotfix data\nHotfix data is a set of non-executable files downloaded and applied to the game.\nThe base implementation knows how to handle INI, PAK, and locres files.\nNOTE: Each INI/PAK file must be prefixed by the platform name they are targeted at" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_OSSName_MetaData[] = {
		{ "Comment", "/** Tells the hotfix manager which OSS to use. Uses the default if empty */" },
		{ "ModuleRelativePath", "Public/OnlineHotfixManager.h" },
		{ "ToolTip", "Tells the hotfix manager which OSS to use. Uses the default if empty" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_OSSName = { "OSSName", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOnlineHotfixManager, OSSName), METADATA_PARAMS(Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_OSSName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_OSSName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_HotfixManagerClassName_MetaData[] = {
		{ "Comment", "/** Tells the factory method which class to contruct */" },
		{ "ModuleRelativePath", "Public/OnlineHotfixManager.h" },
		{ "ToolTip", "Tells the factory method which class to contruct" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_HotfixManagerClassName = { "HotfixManagerClassName", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOnlineHotfixManager, HotfixManagerClassName), METADATA_PARAMS(Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_HotfixManagerClassName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_HotfixManagerClassName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_DebugPrefix_MetaData[] = {
		{ "Comment", "/** Used to prevent development work from interfering with playtests, etc. */" },
		{ "ModuleRelativePath", "Public/OnlineHotfixManager.h" },
		{ "ToolTip", "Used to prevent development work from interfering with playtests, etc." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_DebugPrefix = { "DebugPrefix", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOnlineHotfixManager, DebugPrefix), METADATA_PARAMS(Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_DebugPrefix_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_DebugPrefix_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_AssetsHotfixedFromIniFiles_Inner = { "AssetsHotfixedFromIniFiles", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_AssetsHotfixedFromIniFiles_MetaData[] = {
		{ "Comment", "/** Array of objects that we're forcing to remain resident because we've applied live hotfixes and won't get an\n\x09    opportunity to reapply changes if the object is evicted from memory. */" },
		{ "ModuleRelativePath", "Public/OnlineHotfixManager.h" },
		{ "ToolTip", "Array of objects that we're forcing to remain resident because we've applied live hotfixes and won't get an\n          opportunity to reapply changes if the object is evicted from memory." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_AssetsHotfixedFromIniFiles = { "AssetsHotfixedFromIniFiles", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOnlineHotfixManager, AssetsHotfixedFromIniFiles), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_AssetsHotfixedFromIniFiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_AssetsHotfixedFromIniFiles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOnlineHotfixManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_OSSName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_HotfixManagerClassName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_DebugPrefix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_AssetsHotfixedFromIniFiles_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOnlineHotfixManager_Statics::NewProp_AssetsHotfixedFromIniFiles,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOnlineHotfixManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOnlineHotfixManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOnlineHotfixManager_Statics::ClassParams = {
		&UOnlineHotfixManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UOnlineHotfixManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UOnlineHotfixManager_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOnlineHotfixManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOnlineHotfixManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOnlineHotfixManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOnlineHotfixManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOnlineHotfixManager, 2194177316);
	template<> HOTFIX_API UClass* StaticClass<UOnlineHotfixManager>()
	{
		return UOnlineHotfixManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOnlineHotfixManager(Z_Construct_UClass_UOnlineHotfixManager, &UOnlineHotfixManager::StaticClass, TEXT("/Script/Hotfix"), TEXT("UOnlineHotfixManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOnlineHotfixManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
