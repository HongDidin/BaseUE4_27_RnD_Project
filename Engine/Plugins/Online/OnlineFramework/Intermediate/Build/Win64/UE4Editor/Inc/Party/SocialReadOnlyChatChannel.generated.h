// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PARTY_SocialReadOnlyChatChannel_generated_h
#error "SocialReadOnlyChatChannel.generated.h already included, missing '#pragma once' in SocialReadOnlyChatChannel.h"
#endif
#define PARTY_SocialReadOnlyChatChannel_generated_h

#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_SPARSE_DATA
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_RPC_WRAPPERS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSocialReadOnlyChatChannel(); \
	friend struct Z_Construct_UClass_USocialReadOnlyChatChannel_Statics; \
public: \
	DECLARE_CLASS(USocialReadOnlyChatChannel, USocialChatChannel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialReadOnlyChatChannel)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUSocialReadOnlyChatChannel(); \
	friend struct Z_Construct_UClass_USocialReadOnlyChatChannel_Statics; \
public: \
	DECLARE_CLASS(USocialReadOnlyChatChannel, USocialChatChannel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Party"), NO_API) \
	DECLARE_SERIALIZER(USocialReadOnlyChatChannel)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialReadOnlyChatChannel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USocialReadOnlyChatChannel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialReadOnlyChatChannel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialReadOnlyChatChannel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialReadOnlyChatChannel(USocialReadOnlyChatChannel&&); \
	NO_API USocialReadOnlyChatChannel(const USocialReadOnlyChatChannel&); \
public:


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USocialReadOnlyChatChannel() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USocialReadOnlyChatChannel(USocialReadOnlyChatChannel&&); \
	NO_API USocialReadOnlyChatChannel(const USocialReadOnlyChatChannel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USocialReadOnlyChatChannel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USocialReadOnlyChatChannel); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USocialReadOnlyChatChannel)


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_13_PROLOG
#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_RPC_WRAPPERS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_INCLASS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_SPARSE_DATA \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PARTY_API UClass* StaticClass<class USocialReadOnlyChatChannel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Online_OnlineFramework_Source_Party_Public_Chat_SocialReadOnlyChatChannel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
