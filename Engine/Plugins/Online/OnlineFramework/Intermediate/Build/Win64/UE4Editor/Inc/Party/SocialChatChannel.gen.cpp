// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/Chat/SocialChatChannel.h"
#include "Party/Public/Chat/SocialChatManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSocialChatChannel() {}
// Cross Module References
	PARTY_API UEnum* Z_Construct_UEnum_Party_ESocialChannelType();
	UPackage* Z_Construct_UPackage__Script_Party();
	PARTY_API UClass* Z_Construct_UClass_USocialChatChannel_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_USocialChatChannel();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	static UEnum* ESocialChannelType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Party_ESocialChannelType, Z_Construct_UPackage__Script_Party(), TEXT("ESocialChannelType"));
		}
		return Singleton;
	}
	template<> PARTY_API UEnum* StaticEnum<ESocialChannelType>()
	{
		return ESocialChannelType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESocialChannelType(ESocialChannelType_StaticEnum, TEXT("/Script/Party"), TEXT("ESocialChannelType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Party_ESocialChannelType_Hash() { return 3917850940U; }
	UEnum* Z_Construct_UEnum_Party_ESocialChannelType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESocialChannelType"), 0, Get_Z_Construct_UEnum_Party_ESocialChannelType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESocialChannelType::General", (int64)ESocialChannelType::General },
				{ "ESocialChannelType::Founder", (int64)ESocialChannelType::Founder },
				{ "ESocialChannelType::Party", (int64)ESocialChannelType::Party },
				{ "ESocialChannelType::Team", (int64)ESocialChannelType::Team },
				{ "ESocialChannelType::System", (int64)ESocialChannelType::System },
				{ "ESocialChannelType::Private", (int64)ESocialChannelType::Private },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Founder.Name", "ESocialChannelType::Founder" },
				{ "General.Name", "ESocialChannelType::General" },
				{ "ModuleRelativePath", "Public/Chat/SocialChatChannel.h" },
				{ "Party.Name", "ESocialChannelType::Party" },
				{ "Private.Name", "ESocialChannelType::Private" },
				{ "System.Name", "ESocialChannelType::System" },
				{ "Team.Name", "ESocialChannelType::Team" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Party,
				nullptr,
				"ESocialChannelType",
				"ESocialChannelType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void USocialChatChannel::StaticRegisterNativesUSocialChatChannel()
	{
	}
	UClass* Z_Construct_UClass_USocialChatChannel_NoRegister()
	{
		return USocialChatChannel::StaticClass();
	}
	struct Z_Construct_UClass_USocialChatChannel_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USocialChatChannel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USocialChatChannel_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Base SocialCore chat channel class (partial ISocialChatChannel implementation) */" },
		{ "IncludePath", "Chat/SocialChatChannel.h" },
		{ "ModuleRelativePath", "Public/Chat/SocialChatChannel.h" },
		{ "ToolTip", "Base SocialCore chat channel class (partial ISocialChatChannel implementation)" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USocialChatChannel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USocialChatChannel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USocialChatChannel_Statics::ClassParams = {
		&USocialChatChannel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_USocialChatChannel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USocialChatChannel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USocialChatChannel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USocialChatChannel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USocialChatChannel, 4093907622);
	template<> PARTY_API UClass* StaticClass<USocialChatChannel>()
	{
		return USocialChatChannel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USocialChatChannel(Z_Construct_UClass_USocialChatChannel, &USocialChatChannel::StaticClass, TEXT("/Script/Party"), TEXT("USocialChatChannel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USocialChatChannel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
