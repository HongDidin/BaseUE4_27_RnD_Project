// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Party/Public/Party/PartyMember.h"
#include "Party/Public/Party/SocialParty.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePartyMember() {}
// Cross Module References
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FPartyMemberRepData();
	UPackage* Z_Construct_UPackage__Script_Party();
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FOnlinePartyRepDataBase();
	PARTY_API UScriptStruct* Z_Construct_UScriptStruct_FUserPlatform();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FUniqueNetIdRepl();
	PARTY_API UEnum* Z_Construct_UEnum_Party_ECrossplayPreference();
	PARTY_API UClass* Z_Construct_UClass_UPartyMember_NoRegister();
	PARTY_API UClass* Z_Construct_UClass_UPartyMember();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	PARTY_API UClass* Z_Construct_UClass_USocialUser_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FPartyMemberRepData>() == std::is_polymorphic<FOnlinePartyRepDataBase>(), "USTRUCT FPartyMemberRepData cannot be polymorphic unless super FOnlinePartyRepDataBase is polymorphic");

class UScriptStruct* FPartyMemberRepData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PARTY_API uint32 Get_Z_Construct_UScriptStruct_FPartyMemberRepData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPartyMemberRepData, Z_Construct_UPackage__Script_Party(), TEXT("PartyMemberRepData"), sizeof(FPartyMemberRepData), Get_Z_Construct_UScriptStruct_FPartyMemberRepData_Hash());
	}
	return Singleton;
}
template<> PARTY_API UScriptStruct* StaticStruct<FPartyMemberRepData>()
{
	return FPartyMemberRepData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPartyMemberRepData(FPartyMemberRepData::StaticStruct, TEXT("/Script/Party"), TEXT("PartyMemberRepData"), false, nullptr, nullptr);
static struct FScriptStruct_Party_StaticRegisterNativesFPartyMemberRepData
{
	FScriptStruct_Party_StaticRegisterNativesFPartyMemberRepData()
	{
		UScriptStruct::DeferCppStructOps<FPartyMemberRepData>(FName(TEXT("PartyMemberRepData")));
	}
} ScriptStruct_Party_StaticRegisterNativesFPartyMemberRepData;
	struct Z_Construct_UScriptStruct_FPartyMemberRepData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Platform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Platform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlatformUniqueId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PlatformUniqueId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlatformSessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PlatformSessionId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CrossplayPreference_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CrossplayPreference_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CrossplayPreference;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Base struct used to replicate data about the state of a single party member to all members. */" },
		{ "ModuleRelativePath", "Public/Party/PartyMember.h" },
		{ "ToolTip", "Base struct used to replicate data about the state of a single party member to all members." },
	};
#endif
	void* Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPartyMemberRepData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_Platform_MetaData[] = {
		{ "Comment", "/** Native platform on which this party member is playing. */" },
		{ "ModuleRelativePath", "Public/Party/PartyMember.h" },
		{ "ToolTip", "Native platform on which this party member is playing." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_Platform = { "Platform", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyMemberRepData, Platform), Z_Construct_UScriptStruct_FUserPlatform, METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_Platform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_Platform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_PlatformUniqueId_MetaData[] = {
		{ "Comment", "/** Net ID for this party member on their native platform. Blank if this member has no Platform SocialSubsystem. */" },
		{ "ModuleRelativePath", "Public/Party/PartyMember.h" },
		{ "ToolTip", "Net ID for this party member on their native platform. Blank if this member has no Platform SocialSubsystem." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_PlatformUniqueId = { "PlatformUniqueId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyMemberRepData, PlatformUniqueId), Z_Construct_UScriptStruct_FUniqueNetIdRepl, METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_PlatformUniqueId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_PlatformUniqueId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_PlatformSessionId_MetaData[] = {
		{ "Comment", "/**\n\x09 * The platform session this member is in. Can be blank for a bit while creating/joining.\n\x09 * Only relevant when this member is on a platform that requires a session backing the party.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Party/PartyMember.h" },
		{ "ToolTip", "The platform session this member is in. Can be blank for a bit while creating/joining.\nOnly relevant when this member is on a platform that requires a session backing the party." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_PlatformSessionId = { "PlatformSessionId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyMemberRepData, PlatformSessionId), METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_PlatformSessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_PlatformSessionId_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_CrossplayPreference_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_CrossplayPreference_MetaData[] = {
		{ "Comment", "/** The crossplay preference of this user. Only relevant to crossplay party scenarios. */" },
		{ "ModuleRelativePath", "Public/Party/PartyMember.h" },
		{ "ToolTip", "The crossplay preference of this user. Only relevant to crossplay party scenarios." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_CrossplayPreference = { "CrossplayPreference", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPartyMemberRepData, CrossplayPreference), Z_Construct_UEnum_Party_ECrossplayPreference, METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_CrossplayPreference_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_CrossplayPreference_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_Platform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_PlatformUniqueId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_PlatformSessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_CrossplayPreference_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::NewProp_CrossplayPreference,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
		Z_Construct_UScriptStruct_FOnlinePartyRepDataBase,
		&NewStructOps,
		"PartyMemberRepData",
		sizeof(FPartyMemberRepData),
		alignof(FPartyMemberRepData),
		Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPartyMemberRepData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPartyMemberRepData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Party();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PartyMemberRepData"), sizeof(FPartyMemberRepData), Get_Z_Construct_UScriptStruct_FPartyMemberRepData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPartyMemberRepData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPartyMemberRepData_Hash() { return 3488019139U; }
	void UPartyMember::StaticRegisterNativesUPartyMember()
	{
	}
	UClass* Z_Construct_UClass_UPartyMember_NoRegister()
	{
		return UPartyMember::StaticClass();
	}
	struct Z_Construct_UClass_UPartyMember_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SocialUser_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SocialUser;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPartyMember_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Party,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPartyMember_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Party/PartyMember.h" },
		{ "ModuleRelativePath", "Public/Party/PartyMember.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPartyMember_Statics::NewProp_SocialUser_MetaData[] = {
		{ "ModuleRelativePath", "Public/Party/PartyMember.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPartyMember_Statics::NewProp_SocialUser = { "SocialUser", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPartyMember, SocialUser), Z_Construct_UClass_USocialUser_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPartyMember_Statics::NewProp_SocialUser_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPartyMember_Statics::NewProp_SocialUser_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPartyMember_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPartyMember_Statics::NewProp_SocialUser,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPartyMember_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPartyMember>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPartyMember_Statics::ClassParams = {
		&UPartyMember::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPartyMember_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPartyMember_Statics::PropPointers),
		0,
		0x001000A9u,
		METADATA_PARAMS(Z_Construct_UClass_UPartyMember_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPartyMember_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPartyMember()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPartyMember_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPartyMember, 3982204780);
	template<> PARTY_API UClass* StaticClass<UPartyMember>()
	{
		return UPartyMember::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPartyMember(Z_Construct_UClass_UPartyMember, &UPartyMember::StaticClass, TEXT("/Script/Party"), TEXT("UPartyMember"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPartyMember);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
