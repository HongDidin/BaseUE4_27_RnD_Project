// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RigLogicModule/Public/RigUnit_RigLogic.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_RigLogic() {}
// Cross Module References
	RIGLOGICMODULE_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_RigLogic();
	UPackage* Z_Construct_UPackage__Script_RigLogicModule();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnitMutable();
	RIGLOGICMODULE_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMeshComponent_NoRegister();
	RIGLOGICMODULE_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_RigLogic>() == std::is_polymorphic<FRigUnitMutable>(), "USTRUCT FRigUnit_RigLogic cannot be polymorphic unless super FRigUnitMutable is polymorphic");

class UScriptStruct* FRigUnit_RigLogic::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RIGLOGICMODULE_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_RigLogic, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("RigUnit_RigLogic"), sizeof(FRigUnit_RigLogic), Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_RigLogic::Execute"), &FRigUnit_RigLogic::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<FRigUnit_RigLogic>()
{
	return FRigUnit_RigLogic::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_RigLogic(FRigUnit_RigLogic::StaticStruct, TEXT("/Script/RigLogicModule"), TEXT("RigUnit_RigLogic"), false, nullptr, nullptr);
static struct FScriptStruct_RigLogicModule_StaticRegisterNativesFRigUnit_RigLogic
{
	FScriptStruct_RigLogicModule_StaticRegisterNativesFRigUnit_RigLogic()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_RigLogic>(FName(TEXT("RigUnit_RigLogic")));
	}
} ScriptStruct_RigLogicModule_StaticRegisterNativesFRigUnit_RigLogic;
	struct Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::Struct_MetaDataParams[] = {
		{ "Category", "RigLogic" },
		{ "Comment", "/** RigLogic is used to translate control input curves into bone transforms and values for blend shape and\n  *  animated map multiplier curves */" },
		{ "DisplayName", "RigLogic" },
		{ "DocumentationPolicy", "Strict" },
		{ "Keywords", "Rig,RigLogic" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "RigLogic is used to translate control input curves into bone transforms and values for blend shape and\nanimated map multiplier curves" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_RigLogic>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::NewProp_Data_MetaData[] = {
		{ "Comment", "// internal work data for the unit\n" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "internal work data for the unit" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::NewProp_Data = { "Data", nullptr, (EPropertyFlags)0x0040008000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_RigLogic, Data), Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::NewProp_Data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::NewProp_Data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::NewProp_Data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RigLogicModule,
		Z_Construct_UScriptStruct_FRigUnitMutable,
		&NewStructOps,
		"RigUnit_RigLogic",
		sizeof(FRigUnit_RigLogic),
		alignof(FRigUnit_RigLogic),
		Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_RigLogic()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_RigLogic"), sizeof(FRigUnit_RigLogic), Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_Hash() { return 2155241107U; }

void FRigUnit_RigLogic::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
    StaticExecute(
		RigVMExecuteContext,
		Data,
		ExecuteContext,
		Context
	);
}

class UScriptStruct* FRigUnit_RigLogic_Data::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RIGLOGICMODULE_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("RigUnit_RigLogic_Data"), sizeof(FRigUnit_RigLogic_Data), Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Hash());
	}
	return Singleton;
}
template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<FRigUnit_RigLogic_Data>()
{
	return FRigUnit_RigLogic_Data::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_RigLogic_Data(FRigUnit_RigLogic_Data::StaticStruct, TEXT("/Script/RigLogicModule"), TEXT("RigUnit_RigLogic_Data"), false, nullptr, nullptr);
static struct FScriptStruct_RigLogicModule_StaticRegisterNativesFRigUnit_RigLogic_Data
{
	FScriptStruct_RigLogicModule_StaticRegisterNativesFRigUnit_RigLogic_Data()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_RigLogic_Data>(FName(TEXT("RigUnit_RigLogic_Data")));
	}
} ScriptStruct_RigLogicModule_StaticRegisterNativesFRigUnit_RigLogic_Data;
	struct Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkelMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_SkelMeshComponent;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InputCurveIndices_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputCurveIndices_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InputCurveIndices;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HierarchyBoneIndices_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HierarchyBoneIndices_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HierarchyBoneIndices;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MorphTargetCurveIndices_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MorphTargetCurveIndices_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MorphTargetCurveIndices;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BlendShapeIndices_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendShapeIndices_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BlendShapeIndices;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveContainerIndicesForAnimMaps_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveContainerIndicesForAnimMaps_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CurveContainerIndicesForAnimMaps;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RigLogicIndicesForAnimMaps_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RigLogicIndicesForAnimMaps_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RigLogicIndicesForAnimMaps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentLOD_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_CurrentLOD;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/* The work data used by the FRigUnit_RigLogic */" },
		{ "DocumentationPolicy", "Strict" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "The work data used by the FRigUnit_RigLogic" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_RigLogic_Data>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_SkelMeshComponent_MetaData[] = {
		{ "Comment", "/** Cached Skeletal Mesh Component **/" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "Cached Skeletal Mesh Component *" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_SkelMeshComponent = { "SkelMeshComponent", nullptr, (EPropertyFlags)0x0014000000082008, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_RigLogic_Data, SkelMeshComponent), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_SkelMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_SkelMeshComponent_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_InputCurveIndices_Inner = { "InputCurveIndices", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_InputCurveIndices_MetaData[] = {
		{ "Comment", "/** RL input index to ControlRig's input curve index for each LOD **/" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "RL input index to ControlRig's input curve index for each LOD *" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_InputCurveIndices = { "InputCurveIndices", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_RigLogic_Data, InputCurveIndices), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_InputCurveIndices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_InputCurveIndices_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_HierarchyBoneIndices_Inner = { "HierarchyBoneIndices", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_HierarchyBoneIndices_MetaData[] = {
		{ "Comment", "/** RL joint index to ControlRig's hierarchy bone index **/" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "RL joint index to ControlRig's hierarchy bone index *" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_HierarchyBoneIndices = { "HierarchyBoneIndices", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_RigLogic_Data, HierarchyBoneIndices), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_HierarchyBoneIndices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_HierarchyBoneIndices_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_MorphTargetCurveIndices_Inner = { "MorphTargetCurveIndices", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_MorphTargetCurveIndices_MetaData[] = {
		{ "Comment", "/** RL mesh blend shape index to ControlRig's output blendshape curve index for each LOD **/" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "RL mesh blend shape index to ControlRig's output blendshape curve index for each LOD *" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_MorphTargetCurveIndices = { "MorphTargetCurveIndices", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_RigLogic_Data, MorphTargetCurveIndices), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_MorphTargetCurveIndices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_MorphTargetCurveIndices_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_BlendShapeIndices_Inner = { "BlendShapeIndices", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_BlendShapeIndices_MetaData[] = {
		{ "Comment", "/** RL mesh+blend shape array index to RL blend shape index for each LOD **/" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "RL mesh+blend shape array index to RL blend shape index for each LOD *" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_BlendShapeIndices = { "BlendShapeIndices", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_RigLogic_Data, BlendShapeIndices), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_BlendShapeIndices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_BlendShapeIndices_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurveContainerIndicesForAnimMaps_Inner = { "CurveContainerIndicesForAnimMaps", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurveContainerIndicesForAnimMaps_MetaData[] = {
		{ "Comment", "/** RL animated map index to ControlRig's output anim map curve index for each LOD **/" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "RL animated map index to ControlRig's output anim map curve index for each LOD *" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurveContainerIndicesForAnimMaps = { "CurveContainerIndicesForAnimMaps", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_RigLogic_Data, CurveContainerIndicesForAnimMaps), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurveContainerIndicesForAnimMaps_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurveContainerIndicesForAnimMaps_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_RigLogicIndicesForAnimMaps_Inner = { "RigLogicIndicesForAnimMaps", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_RigLogicIndicesForAnimMaps_MetaData[] = {
		{ "Comment", "/** RL animated map index to RL anim map curve index for each LOD **/" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "RL animated map index to RL anim map curve index for each LOD *" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_RigLogicIndicesForAnimMaps = { "RigLogicIndicesForAnimMaps", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_RigLogic_Data, RigLogicIndicesForAnimMaps), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_RigLogicIndicesForAnimMaps_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_RigLogicIndicesForAnimMaps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurrentLOD_MetaData[] = {
		{ "Comment", "/** LOD for which the model is rendered **/" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "LOD for which the model is rendered *" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurrentLOD = { "CurrentLOD", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_RigLogic_Data, CurrentLOD), METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurrentLOD_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurrentLOD_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_SkelMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_InputCurveIndices_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_InputCurveIndices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_HierarchyBoneIndices_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_HierarchyBoneIndices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_MorphTargetCurveIndices_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_MorphTargetCurveIndices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_BlendShapeIndices_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_BlendShapeIndices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurveContainerIndicesForAnimMaps_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurveContainerIndicesForAnimMaps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_RigLogicIndicesForAnimMaps_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_RigLogicIndicesForAnimMaps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::NewProp_CurrentLOD,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RigLogicModule,
		nullptr,
		&NewStructOps,
		"RigUnit_RigLogic_Data",
		sizeof(FRigUnit_RigLogic_Data),
		alignof(FRigUnit_RigLogic_Data),
		Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_RigLogic_Data"), sizeof(FRigUnit_RigLogic_Data), Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Hash() { return 3988595017U; }
class UScriptStruct* FRigUnit_RigLogic_IntArray::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RIGLOGICMODULE_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("RigUnit_RigLogic_IntArray"), sizeof(FRigUnit_RigLogic_IntArray), Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Hash());
	}
	return Singleton;
}
template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<FRigUnit_RigLogic_IntArray>()
{
	return FRigUnit_RigLogic_IntArray::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_RigLogic_IntArray(FRigUnit_RigLogic_IntArray::StaticStruct, TEXT("/Script/RigLogicModule"), TEXT("RigUnit_RigLogic_IntArray"), false, nullptr, nullptr);
static struct FScriptStruct_RigLogicModule_StaticRegisterNativesFRigUnit_RigLogic_IntArray
{
	FScriptStruct_RigLogicModule_StaticRegisterNativesFRigUnit_RigLogic_IntArray()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_RigLogic_IntArray>(FName(TEXT("RigUnit_RigLogic_IntArray")));
	}
} ScriptStruct_RigLogicModule_StaticRegisterNativesFRigUnit_RigLogic_IntArray;
	struct Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Values_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Values_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Values;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/* A helper struct used inside of the RigUnit_RigLogic to store arrays of arrays of integers. */" },
		{ "DocumentationPolicy", "Strict" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "A helper struct used inside of the RigUnit_RigLogic to store arrays of arrays of integers." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_RigLogic_IntArray>();
	}
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::NewProp_Values_Inner = { "Values", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::NewProp_Values_MetaData[] = {
		{ "Comment", "// The values stored within this array.\n" },
		{ "ModuleRelativePath", "Public/RigUnit_RigLogic.h" },
		{ "ToolTip", "The values stored within this array." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::NewProp_Values = { "Values", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_RigLogic_IntArray, Values), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::NewProp_Values_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::NewProp_Values_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::NewProp_Values_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::NewProp_Values,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RigLogicModule,
		nullptr,
		&NewStructOps,
		"RigUnit_RigLogic_IntArray",
		sizeof(FRigUnit_RigLogic_IntArray),
		alignof(FRigUnit_RigLogic_IntArray),
		Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_RigLogic_IntArray"), sizeof(FRigUnit_RigLogic_IntArray), Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Hash() { return 213243805U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
