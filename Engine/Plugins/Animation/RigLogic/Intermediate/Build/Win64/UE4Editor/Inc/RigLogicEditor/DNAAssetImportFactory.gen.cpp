// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RigLogicEditor/Public/DNAAssetImportFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDNAAssetImportFactory() {}
// Cross Module References
	RIGLOGICEDITOR_API UClass* Z_Construct_UClass_UDNAAssetImportFactory_NoRegister();
	RIGLOGICEDITOR_API UClass* Z_Construct_UClass_UDNAAssetImportFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_RigLogicEditor();
	RIGLOGICEDITOR_API UClass* Z_Construct_UClass_UDNAAssetImportUI_NoRegister();
// End Cross Module References
	void UDNAAssetImportFactory::StaticRegisterNativesUDNAAssetImportFactory()
	{
	}
	UClass* Z_Construct_UClass_UDNAAssetImportFactory_NoRegister()
	{
		return UDNAAssetImportFactory::StaticClass();
	}
	struct Z_Construct_UClass_UDNAAssetImportFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportUI_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportUI;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalImportUI_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OriginalImportUI;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDNAAssetImportFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_RigLogicEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDNAAssetImportFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Factory responsible for importing DNA file and attaching DNA data into SkeletalMesh\n */" },
		{ "IncludePath", "DNAAssetImportFactory.h" },
		{ "ModuleRelativePath", "Public/DNAAssetImportFactory.h" },
		{ "ToolTip", "Factory responsible for importing DNA file and attaching DNA data into SkeletalMesh" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDNAAssetImportFactory_Statics::NewProp_ImportUI_MetaData[] = {
		{ "ModuleRelativePath", "Public/DNAAssetImportFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDNAAssetImportFactory_Statics::NewProp_ImportUI = { "ImportUI", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDNAAssetImportFactory, ImportUI), Z_Construct_UClass_UDNAAssetImportUI_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDNAAssetImportFactory_Statics::NewProp_ImportUI_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAssetImportFactory_Statics::NewProp_ImportUI_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDNAAssetImportFactory_Statics::NewProp_OriginalImportUI_MetaData[] = {
		{ "Comment", "/** Prevent garbage collection of original when overriding ImportUI property */" },
		{ "ModuleRelativePath", "Public/DNAAssetImportFactory.h" },
		{ "ToolTip", "Prevent garbage collection of original when overriding ImportUI property" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDNAAssetImportFactory_Statics::NewProp_OriginalImportUI = { "OriginalImportUI", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDNAAssetImportFactory, OriginalImportUI), Z_Construct_UClass_UDNAAssetImportUI_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDNAAssetImportFactory_Statics::NewProp_OriginalImportUI_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAssetImportFactory_Statics::NewProp_OriginalImportUI_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDNAAssetImportFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDNAAssetImportFactory_Statics::NewProp_ImportUI,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDNAAssetImportFactory_Statics::NewProp_OriginalImportUI,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDNAAssetImportFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDNAAssetImportFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDNAAssetImportFactory_Statics::ClassParams = {
		&UDNAAssetImportFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDNAAssetImportFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAssetImportFactory_Statics::PropPointers),
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDNAAssetImportFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAssetImportFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDNAAssetImportFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDNAAssetImportFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDNAAssetImportFactory, 3464565609);
	template<> RIGLOGICEDITOR_API UClass* StaticClass<UDNAAssetImportFactory>()
	{
		return UDNAAssetImportFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDNAAssetImportFactory(Z_Construct_UClass_UDNAAssetImportFactory, &UDNAAssetImportFactory::StaticClass, TEXT("/Script/RigLogicEditor"), TEXT("UDNAAssetImportFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDNAAssetImportFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
