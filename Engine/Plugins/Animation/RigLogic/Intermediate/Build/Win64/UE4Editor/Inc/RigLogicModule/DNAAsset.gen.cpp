// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RigLogicModule/Public/DNAAsset.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDNAAsset() {}
// Cross Module References
	RIGLOGICMODULE_API UClass* Z_Construct_UClass_UDNAAsset_NoRegister();
	RIGLOGICMODULE_API UClass* Z_Construct_UClass_UDNAAsset();
	ENGINE_API UClass* Z_Construct_UClass_UAssetUserData();
	UPackage* Z_Construct_UPackage__Script_RigLogicModule();
	ENGINE_API UClass* Z_Construct_UClass_UAssetImportData_NoRegister();
// End Cross Module References
	void UDNAAsset::StaticRegisterNativesUDNAAsset()
	{
	}
	UClass* Z_Construct_UClass_UDNAAsset_NoRegister()
	{
		return UDNAAsset::StaticClass();
	}
	struct Z_Construct_UClass_UDNAAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetImportData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetImportData;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DNAFileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DNAFileName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDNAAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetUserData,
		(UObject* (*)())Z_Construct_UPackage__Script_RigLogicModule,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDNAAsset_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** An asset holding the data needed to generate/update/animate a RigLogic character\n  * It is imported from character's DNA file as a bit stream, and separated out it into runtime (behavior) and design-time chunks;\n  * Currently, the design-time part still loads the geometry, as it is needed for the skeletal mesh update; once SkeletalMeshDNAReader is\n  * fully implemented, it will be able to read the geometry directly from the SkeletalMesh and won't load it into this asset \n  **/" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "DNAAsset.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/DNAAsset.h" },
		{ "ToolTip", "An asset holding the data needed to generate/update/animate a RigLogic character\nIt is imported from character's DNA file as a bit stream, and separated out it into runtime (behavior) and design-time chunks;\nCurrently, the design-time part still loads the geometry, as it is needed for the skeletal mesh update; once SkeletalMeshDNAReader is\nfully implemented, it will be able to read the geometry directly from the SkeletalMesh and won't load it into this asset" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDNAAsset_Statics::NewProp_AssetImportData_MetaData[] = {
		{ "Category", "ImportSettings" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DNAAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDNAAsset_Statics::NewProp_AssetImportData = { "AssetImportData", nullptr, (EPropertyFlags)0x00120008000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDNAAsset, AssetImportData), Z_Construct_UClass_UAssetImportData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDNAAsset_Statics::NewProp_AssetImportData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAsset_Statics::NewProp_AssetImportData_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDNAAsset_Statics::NewProp_DNAFileName_MetaData[] = {
		{ "ModuleRelativePath", "Public/DNAAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDNAAsset_Statics::NewProp_DNAFileName = { "DNAFileName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDNAAsset, DNAFileName), METADATA_PARAMS(Z_Construct_UClass_UDNAAsset_Statics::NewProp_DNAFileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAsset_Statics::NewProp_DNAFileName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDNAAsset_Statics::PropPointers[] = {
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDNAAsset_Statics::NewProp_AssetImportData,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDNAAsset_Statics::NewProp_DNAFileName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDNAAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDNAAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDNAAsset_Statics::ClassParams = {
		&UDNAAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDNAAsset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAsset_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDNAAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDNAAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDNAAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDNAAsset, 2045629941);
	template<> RIGLOGICMODULE_API UClass* StaticClass<UDNAAsset>()
	{
		return UDNAAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDNAAsset(Z_Construct_UClass_UDNAAsset, &UDNAAsset::StaticClass, TEXT("/Script/RigLogicModule"), TEXT("UDNAAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDNAAsset);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDNAAsset)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
