// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RIGLOGICMODULE_DNACommon_generated_h
#error "DNACommon.generated.h already included, missing '#pragma once' in DNACommon.h"
#endif
#define RIGLOGICMODULE_DNACommon_generated_h

#define Engine_Plugins_Animation_RigLogic_Source_RigLogicModule_Public_DNACommon_h_105_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVertexLayout_Statics; \
	RIGLOGICMODULE_API static class UScriptStruct* StaticStruct();


template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<struct FVertexLayout>();

#define Engine_Plugins_Animation_RigLogic_Source_RigLogicModule_Public_DNACommon_h_94_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTextureCoordinate_Statics; \
	RIGLOGICMODULE_API static class UScriptStruct* StaticStruct();


template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<struct FTextureCoordinate>();

#define Engine_Plugins_Animation_RigLogic_Source_RigLogicModule_Public_DNACommon_h_83_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics; \
	RIGLOGICMODULE_API static class UScriptStruct* StaticStruct();


template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<struct FMeshBlendShapeChannelMapping>();

#define Engine_Plugins_Animation_RigLogic_Source_RigLogicModule_Public_DNACommon_h_70_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCoordinateSystem_Statics; \
	RIGLOGICMODULE_API static class UScriptStruct* StaticStruct();


template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<struct FCoordinateSystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_RigLogic_Source_RigLogicModule_Public_DNACommon_h


#define FOREACH_ENUM_EDNADATALAYER(op) \
	op(EDNADataLayer::Descriptor) \
	op(EDNADataLayer::Definition) \
	op(EDNADataLayer::Behavior) \
	op(EDNADataLayer::Geometry) \
	op(EDNADataLayer::GeometryWithoutBlendShapes) \
	op(EDNADataLayer::AllWithoutBlendShapes) \
	op(EDNADataLayer::All) 

enum class EDNADataLayer : uint8;
template<> RIGLOGICMODULE_API UEnum* StaticEnum<EDNADataLayer>();

#define FOREACH_ENUM_EDIRECTION(op) \
	op(EDirection::Left) \
	op(EDirection::Right) \
	op(EDirection::Up) \
	op(EDirection::Down) \
	op(EDirection::Front) \
	op(EDirection::Back) 

enum class EDirection : uint8;
template<> RIGLOGICMODULE_API UEnum* StaticEnum<EDirection>();

#define FOREACH_ENUM_EROTATIONUNIT(op) \
	op(ERotationUnit::Degrees) \
	op(ERotationUnit::Radians) 

enum class ERotationUnit : uint8;
template<> RIGLOGICMODULE_API UEnum* StaticEnum<ERotationUnit>();

#define FOREACH_ENUM_ETRANSLATIONUNIT(op) \
	op(ETranslationUnit::CM) \
	op(ETranslationUnit::M) 

enum class ETranslationUnit : uint8;
template<> RIGLOGICMODULE_API UEnum* StaticEnum<ETranslationUnit>();

#define FOREACH_ENUM_EGENDER(op) \
	op(EGender::Male) \
	op(EGender::Female) \
	op(EGender::Other) 

enum class EGender : uint8;
template<> RIGLOGICMODULE_API UEnum* StaticEnum<EGender>();

#define FOREACH_ENUM_EARCHETYPE(op) \
	op(EArchetype::Asian) \
	op(EArchetype::Black) \
	op(EArchetype::Caucasian) \
	op(EArchetype::Hispanic) \
	op(EArchetype::Alien) \
	op(EArchetype::Other) 

enum class EArchetype : uint8;
template<> RIGLOGICMODULE_API UEnum* StaticEnum<EArchetype>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
