// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RIGLOGICEDITOR_DNAAssetImportFactory_generated_h
#error "DNAAssetImportFactory.generated.h already included, missing '#pragma once' in DNAAssetImportFactory.h"
#endif
#define RIGLOGICEDITOR_DNAAssetImportFactory_generated_h

#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_SPARSE_DATA
#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_RPC_WRAPPERS
#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDNAAssetImportFactory(); \
	friend struct Z_Construct_UClass_UDNAAssetImportFactory_Statics; \
public: \
	DECLARE_CLASS(UDNAAssetImportFactory, UFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/RigLogicEditor"), NO_API) \
	DECLARE_SERIALIZER(UDNAAssetImportFactory)


#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUDNAAssetImportFactory(); \
	friend struct Z_Construct_UClass_UDNAAssetImportFactory_Statics; \
public: \
	DECLARE_CLASS(UDNAAssetImportFactory, UFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/RigLogicEditor"), NO_API) \
	DECLARE_SERIALIZER(UDNAAssetImportFactory)


#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDNAAssetImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDNAAssetImportFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDNAAssetImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDNAAssetImportFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDNAAssetImportFactory(UDNAAssetImportFactory&&); \
	NO_API UDNAAssetImportFactory(const UDNAAssetImportFactory&); \
public:


#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDNAAssetImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDNAAssetImportFactory(UDNAAssetImportFactory&&); \
	NO_API UDNAAssetImportFactory(const UDNAAssetImportFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDNAAssetImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDNAAssetImportFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDNAAssetImportFactory)


#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_16_PROLOG
#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_SPARSE_DATA \
	Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_RPC_WRAPPERS \
	Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_INCLASS \
	Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_SPARSE_DATA \
	Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h_19_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class DNAAssetImportFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RIGLOGICEDITOR_API UClass* StaticClass<class UDNAAssetImportFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_RigLogic_Source_RigLogicEditor_Public_DNAAssetImportFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
