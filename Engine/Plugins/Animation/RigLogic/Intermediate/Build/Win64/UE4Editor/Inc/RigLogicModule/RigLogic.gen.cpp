// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RigLogicModule/Public/RigLogic.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigLogic() {}
// Cross Module References
	RIGLOGICMODULE_API UEnum* Z_Construct_UEnum_RigLogicModule_ERigLogicCalculationType();
	UPackage* Z_Construct_UPackage__Script_RigLogicModule();
// End Cross Module References
	static UEnum* ERigLogicCalculationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RigLogicModule_ERigLogicCalculationType, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("ERigLogicCalculationType"));
		}
		return Singleton;
	}
	template<> RIGLOGICMODULE_API UEnum* StaticEnum<ERigLogicCalculationType>()
	{
		return ERigLogicCalculationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERigLogicCalculationType(ERigLogicCalculationType_StaticEnum, TEXT("/Script/RigLogicModule"), TEXT("ERigLogicCalculationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RigLogicModule_ERigLogicCalculationType_Hash() { return 3605378923U; }
	UEnum* Z_Construct_UEnum_RigLogicModule_ERigLogicCalculationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERigLogicCalculationType"), 0, Get_Z_Construct_UEnum_RigLogicModule_ERigLogicCalculationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERigLogicCalculationType::Scalar", (int64)ERigLogicCalculationType::Scalar },
				{ "ERigLogicCalculationType::SSE", (int64)ERigLogicCalculationType::SSE },
				{ "ERigLogicCalculationType::AVX", (int64)ERigLogicCalculationType::AVX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AVX.Name", "ERigLogicCalculationType::AVX" },
				{ "BlueprintType", "true" },
				{ "Comment", "// namespace rl4\n" },
				{ "ModuleRelativePath", "Public/RigLogic.h" },
				{ "Scalar.Name", "ERigLogicCalculationType::Scalar" },
				{ "SSE.Name", "ERigLogicCalculationType::SSE" },
				{ "ToolTip", "namespace rl4" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RigLogicModule,
				nullptr,
				"ERigLogicCalculationType",
				"ERigLogicCalculationType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
