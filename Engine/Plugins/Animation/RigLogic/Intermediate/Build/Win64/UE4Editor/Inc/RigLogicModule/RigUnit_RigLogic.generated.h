// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RIGLOGICMODULE_RigUnit_RigLogic_generated_h
#error "RigUnit_RigLogic.generated.h already included, missing '#pragma once' in RigUnit_RigLogic.h"
#endif
#define RIGLOGICMODULE_RigUnit_RigLogic_generated_h


#define FRigUnit_RigLogic_Execute() \
	void FRigUnit_RigLogic::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigUnit_RigLogic_Data& Data, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Animation_RigLogic_Source_RigLogicModule_Public_RigUnit_RigLogic_h_141_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_RigLogic_Statics; \
	RIGLOGICMODULE_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigUnit_RigLogic_Data& Data, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		FRigVMDynamicArray<FRigUnit_RigLogic_Data> Data_0_Array(*((FRigVMByteArray*)RigVMMemoryHandles[0].GetData(0, false))); \
		Data_0_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_RigLogic_Data& Data = Data_0_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[1].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Data, \
			ExecuteContext, \
			Context \
		); \
	} \
	FORCEINLINE static uint32 __PPO__Data() { return STRUCT_OFFSET(FRigUnit_RigLogic, Data); } \
	typedef FRigUnitMutable Super;


template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<struct FRigUnit_RigLogic>();

#define Engine_Plugins_Animation_RigLogic_Source_RigLogicModule_Public_RigUnit_RigLogic_h_54_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_RigLogic_Data_Statics; \
	RIGLOGICMODULE_API static class UScriptStruct* StaticStruct();


template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<struct FRigUnit_RigLogic_Data>();

#define Engine_Plugins_Animation_RigLogic_Source_RigLogicModule_Public_RigUnit_RigLogic_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_RigLogic_IntArray_Statics; \
	RIGLOGICMODULE_API static class UScriptStruct* StaticStruct();


template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<struct FRigUnit_RigLogic_IntArray>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_RigLogic_Source_RigLogicModule_Public_RigUnit_RigLogic_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
