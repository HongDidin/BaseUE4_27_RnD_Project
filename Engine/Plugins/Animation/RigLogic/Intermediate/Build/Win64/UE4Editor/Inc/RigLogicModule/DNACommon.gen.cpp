// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RigLogicModule/Public/DNACommon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDNACommon() {}
// Cross Module References
	RIGLOGICMODULE_API UEnum* Z_Construct_UEnum_RigLogicModule_EDNADataLayer();
	UPackage* Z_Construct_UPackage__Script_RigLogicModule();
	RIGLOGICMODULE_API UEnum* Z_Construct_UEnum_RigLogicModule_EDirection();
	RIGLOGICMODULE_API UEnum* Z_Construct_UEnum_RigLogicModule_ERotationUnit();
	RIGLOGICMODULE_API UEnum* Z_Construct_UEnum_RigLogicModule_ETranslationUnit();
	RIGLOGICMODULE_API UEnum* Z_Construct_UEnum_RigLogicModule_EGender();
	RIGLOGICMODULE_API UEnum* Z_Construct_UEnum_RigLogicModule_EArchetype();
	RIGLOGICMODULE_API UScriptStruct* Z_Construct_UScriptStruct_FVertexLayout();
	RIGLOGICMODULE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureCoordinate();
	RIGLOGICMODULE_API UScriptStruct* Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping();
	RIGLOGICMODULE_API UScriptStruct* Z_Construct_UScriptStruct_FCoordinateSystem();
// End Cross Module References
	static UEnum* EDNADataLayer_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RigLogicModule_EDNADataLayer, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("EDNADataLayer"));
		}
		return Singleton;
	}
	template<> RIGLOGICMODULE_API UEnum* StaticEnum<EDNADataLayer>()
	{
		return EDNADataLayer_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDNADataLayer(EDNADataLayer_StaticEnum, TEXT("/Script/RigLogicModule"), TEXT("EDNADataLayer"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RigLogicModule_EDNADataLayer_Hash() { return 38121874U; }
	UEnum* Z_Construct_UEnum_RigLogicModule_EDNADataLayer()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDNADataLayer"), 0, Get_Z_Construct_UEnum_RigLogicModule_EDNADataLayer_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDNADataLayer::Descriptor", (int64)EDNADataLayer::Descriptor },
				{ "EDNADataLayer::Definition", (int64)EDNADataLayer::Definition },
				{ "EDNADataLayer::Behavior", (int64)EDNADataLayer::Behavior },
				{ "EDNADataLayer::Geometry", (int64)EDNADataLayer::Geometry },
				{ "EDNADataLayer::GeometryWithoutBlendShapes", (int64)EDNADataLayer::GeometryWithoutBlendShapes },
				{ "EDNADataLayer::AllWithoutBlendShapes", (int64)EDNADataLayer::AllWithoutBlendShapes },
				{ "EDNADataLayer::All", (int64)EDNADataLayer::All },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "All.Comment", "// Includes everything except blend shapes from Geometry\n" },
				{ "All.Name", "EDNADataLayer::All" },
				{ "All.ToolTip", "Includes everything except blend shapes from Geometry" },
				{ "AllWithoutBlendShapes.Comment", "// Includes Descriptor and Definition\n" },
				{ "AllWithoutBlendShapes.Name", "EDNADataLayer::AllWithoutBlendShapes" },
				{ "AllWithoutBlendShapes.ToolTip", "Includes Descriptor and Definition" },
				{ "Behavior.Comment", "// Includes Descriptor\n" },
				{ "Behavior.Name", "EDNADataLayer::Behavior" },
				{ "Behavior.ToolTip", "Includes Descriptor" },
				{ "BlueprintType", "true" },
				{ "Definition.Name", "EDNADataLayer::Definition" },
				{ "Descriptor.Name", "EDNADataLayer::Descriptor" },
				{ "Geometry.Comment", "// Includes Descriptor and Definition\n" },
				{ "Geometry.Name", "EDNADataLayer::Geometry" },
				{ "Geometry.ToolTip", "Includes Descriptor and Definition" },
				{ "GeometryWithoutBlendShapes.Comment", "// Includes Descriptor and Definition\n" },
				{ "GeometryWithoutBlendShapes.Name", "EDNADataLayer::GeometryWithoutBlendShapes" },
				{ "GeometryWithoutBlendShapes.ToolTip", "Includes Descriptor and Definition" },
				{ "ModuleRelativePath", "Public/DNACommon.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RigLogicModule,
				nullptr,
				"EDNADataLayer",
				"EDNADataLayer",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDirection_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RigLogicModule_EDirection, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("EDirection"));
		}
		return Singleton;
	}
	template<> RIGLOGICMODULE_API UEnum* StaticEnum<EDirection>()
	{
		return EDirection_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDirection(EDirection_StaticEnum, TEXT("/Script/RigLogicModule"), TEXT("EDirection"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RigLogicModule_EDirection_Hash() { return 2249746420U; }
	UEnum* Z_Construct_UEnum_RigLogicModule_EDirection()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDirection"), 0, Get_Z_Construct_UEnum_RigLogicModule_EDirection_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDirection::Left", (int64)EDirection::Left },
				{ "EDirection::Right", (int64)EDirection::Right },
				{ "EDirection::Up", (int64)EDirection::Up },
				{ "EDirection::Down", (int64)EDirection::Down },
				{ "EDirection::Front", (int64)EDirection::Front },
				{ "EDirection::Back", (int64)EDirection::Back },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Back.Name", "EDirection::Back" },
				{ "BlueprintType", "true" },
				{ "Down.Name", "EDirection::Down" },
				{ "Front.Name", "EDirection::Front" },
				{ "Left.Name", "EDirection::Left" },
				{ "ModuleRelativePath", "Public/DNACommon.h" },
				{ "Right.Name", "EDirection::Right" },
				{ "Up.Name", "EDirection::Up" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RigLogicModule,
				nullptr,
				"EDirection",
				"EDirection",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERotationUnit_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RigLogicModule_ERotationUnit, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("ERotationUnit"));
		}
		return Singleton;
	}
	template<> RIGLOGICMODULE_API UEnum* StaticEnum<ERotationUnit>()
	{
		return ERotationUnit_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERotationUnit(ERotationUnit_StaticEnum, TEXT("/Script/RigLogicModule"), TEXT("ERotationUnit"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RigLogicModule_ERotationUnit_Hash() { return 2150331983U; }
	UEnum* Z_Construct_UEnum_RigLogicModule_ERotationUnit()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERotationUnit"), 0, Get_Z_Construct_UEnum_RigLogicModule_ERotationUnit_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERotationUnit::Degrees", (int64)ERotationUnit::Degrees },
				{ "ERotationUnit::Radians", (int64)ERotationUnit::Radians },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Degrees.Name", "ERotationUnit::Degrees" },
				{ "ModuleRelativePath", "Public/DNACommon.h" },
				{ "Radians.Name", "ERotationUnit::Radians" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RigLogicModule,
				nullptr,
				"ERotationUnit",
				"ERotationUnit",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETranslationUnit_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RigLogicModule_ETranslationUnit, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("ETranslationUnit"));
		}
		return Singleton;
	}
	template<> RIGLOGICMODULE_API UEnum* StaticEnum<ETranslationUnit>()
	{
		return ETranslationUnit_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETranslationUnit(ETranslationUnit_StaticEnum, TEXT("/Script/RigLogicModule"), TEXT("ETranslationUnit"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RigLogicModule_ETranslationUnit_Hash() { return 2704617001U; }
	UEnum* Z_Construct_UEnum_RigLogicModule_ETranslationUnit()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETranslationUnit"), 0, Get_Z_Construct_UEnum_RigLogicModule_ETranslationUnit_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETranslationUnit::CM", (int64)ETranslationUnit::CM },
				{ "ETranslationUnit::M", (int64)ETranslationUnit::M },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "CM.Name", "ETranslationUnit::CM" },
				{ "M.Name", "ETranslationUnit::M" },
				{ "ModuleRelativePath", "Public/DNACommon.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RigLogicModule,
				nullptr,
				"ETranslationUnit",
				"ETranslationUnit",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EGender_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RigLogicModule_EGender, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("EGender"));
		}
		return Singleton;
	}
	template<> RIGLOGICMODULE_API UEnum* StaticEnum<EGender>()
	{
		return EGender_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGender(EGender_StaticEnum, TEXT("/Script/RigLogicModule"), TEXT("EGender"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RigLogicModule_EGender_Hash() { return 1317764140U; }
	UEnum* Z_Construct_UEnum_RigLogicModule_EGender()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGender"), 0, Get_Z_Construct_UEnum_RigLogicModule_EGender_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGender::Male", (int64)EGender::Male },
				{ "EGender::Female", (int64)EGender::Female },
				{ "EGender::Other", (int64)EGender::Other },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Female.Name", "EGender::Female" },
				{ "Male.Name", "EGender::Male" },
				{ "ModuleRelativePath", "Public/DNACommon.h" },
				{ "Other.Name", "EGender::Other" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RigLogicModule,
				nullptr,
				"EGender",
				"EGender",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EArchetype_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RigLogicModule_EArchetype, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("EArchetype"));
		}
		return Singleton;
	}
	template<> RIGLOGICMODULE_API UEnum* StaticEnum<EArchetype>()
	{
		return EArchetype_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EArchetype(EArchetype_StaticEnum, TEXT("/Script/RigLogicModule"), TEXT("EArchetype"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RigLogicModule_EArchetype_Hash() { return 2539362474U; }
	UEnum* Z_Construct_UEnum_RigLogicModule_EArchetype()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EArchetype"), 0, Get_Z_Construct_UEnum_RigLogicModule_EArchetype_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EArchetype::Asian", (int64)EArchetype::Asian },
				{ "EArchetype::Black", (int64)EArchetype::Black },
				{ "EArchetype::Caucasian", (int64)EArchetype::Caucasian },
				{ "EArchetype::Hispanic", (int64)EArchetype::Hispanic },
				{ "EArchetype::Alien", (int64)EArchetype::Alien },
				{ "EArchetype::Other", (int64)EArchetype::Other },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Alien.Name", "EArchetype::Alien" },
				{ "Asian.Name", "EArchetype::Asian" },
				{ "Black.Name", "EArchetype::Black" },
				{ "BlueprintType", "true" },
				{ "Caucasian.Name", "EArchetype::Caucasian" },
				{ "Hispanic.Name", "EArchetype::Hispanic" },
				{ "ModuleRelativePath", "Public/DNACommon.h" },
				{ "Other.Name", "EArchetype::Other" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RigLogicModule,
				nullptr,
				"EArchetype",
				"EArchetype",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FVertexLayout::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RIGLOGICMODULE_API uint32 Get_Z_Construct_UScriptStruct_FVertexLayout_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVertexLayout, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("VertexLayout"), sizeof(FVertexLayout), Get_Z_Construct_UScriptStruct_FVertexLayout_Hash());
	}
	return Singleton;
}
template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<FVertexLayout>()
{
	return FVertexLayout::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVertexLayout(FVertexLayout::StaticStruct, TEXT("/Script/RigLogicModule"), TEXT("VertexLayout"), false, nullptr, nullptr);
static struct FScriptStruct_RigLogicModule_StaticRegisterNativesFVertexLayout
{
	FScriptStruct_RigLogicModule_StaticRegisterNativesFVertexLayout()
	{
		UScriptStruct::DeferCppStructOps<FVertexLayout>(FName(TEXT("VertexLayout")));
	}
} ScriptStruct_RigLogicModule_StaticRegisterNativesFVertexLayout;
	struct Z_Construct_UScriptStruct_FVertexLayout_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TextureCoordinate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Normal_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Normal;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVertexLayout_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVertexLayout_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVertexLayout>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_Position_MetaData[] = {
		{ "Category", "RigLogic" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVertexLayout, Position), METADATA_PARAMS(Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_TextureCoordinate_MetaData[] = {
		{ "Category", "RigLogic" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_TextureCoordinate = { "TextureCoordinate", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVertexLayout, TextureCoordinate), METADATA_PARAMS(Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_TextureCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_TextureCoordinate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_Normal_MetaData[] = {
		{ "Category", "RigLogic" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_Normal = { "Normal", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVertexLayout, Normal), METADATA_PARAMS(Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_Normal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_Normal_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVertexLayout_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_TextureCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVertexLayout_Statics::NewProp_Normal,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVertexLayout_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RigLogicModule,
		nullptr,
		&NewStructOps,
		"VertexLayout",
		sizeof(FVertexLayout),
		alignof(FVertexLayout),
		Z_Construct_UScriptStruct_FVertexLayout_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVertexLayout_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVertexLayout_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVertexLayout_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVertexLayout()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVertexLayout_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VertexLayout"), sizeof(FVertexLayout), Get_Z_Construct_UScriptStruct_FVertexLayout_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVertexLayout_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVertexLayout_Hash() { return 4024202579U; }
class UScriptStruct* FTextureCoordinate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RIGLOGICMODULE_API uint32 Get_Z_Construct_UScriptStruct_FTextureCoordinate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTextureCoordinate, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("TextureCoordinate"), sizeof(FTextureCoordinate), Get_Z_Construct_UScriptStruct_FTextureCoordinate_Hash());
	}
	return Singleton;
}
template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<FTextureCoordinate>()
{
	return FTextureCoordinate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTextureCoordinate(FTextureCoordinate::StaticStruct, TEXT("/Script/RigLogicModule"), TEXT("TextureCoordinate"), false, nullptr, nullptr);
static struct FScriptStruct_RigLogicModule_StaticRegisterNativesFTextureCoordinate
{
	FScriptStruct_RigLogicModule_StaticRegisterNativesFTextureCoordinate()
	{
		UScriptStruct::DeferCppStructOps<FTextureCoordinate>(FName(TEXT("TextureCoordinate")));
	}
} ScriptStruct_RigLogicModule_StaticRegisterNativesFTextureCoordinate;
	struct Z_Construct_UScriptStruct_FTextureCoordinate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_U_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_U;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_V_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_V;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureCoordinate_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTextureCoordinate>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewProp_U_MetaData[] = {
		{ "Category", "RigLogic" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewProp_U = { "U", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureCoordinate, U), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewProp_U_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewProp_U_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewProp_V_MetaData[] = {
		{ "Category", "RigLogic" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewProp_V = { "V", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureCoordinate, V), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewProp_V_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewProp_V_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTextureCoordinate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewProp_U,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureCoordinate_Statics::NewProp_V,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTextureCoordinate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RigLogicModule,
		nullptr,
		&NewStructOps,
		"TextureCoordinate",
		sizeof(FTextureCoordinate),
		alignof(FTextureCoordinate),
		Z_Construct_UScriptStruct_FTextureCoordinate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCoordinate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureCoordinate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCoordinate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTextureCoordinate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTextureCoordinate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TextureCoordinate"), sizeof(FTextureCoordinate), Get_Z_Construct_UScriptStruct_FTextureCoordinate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTextureCoordinate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTextureCoordinate_Hash() { return 3200183955U; }
class UScriptStruct* FMeshBlendShapeChannelMapping::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RIGLOGICMODULE_API uint32 Get_Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("MeshBlendShapeChannelMapping"), sizeof(FMeshBlendShapeChannelMapping), Get_Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Hash());
	}
	return Singleton;
}
template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<FMeshBlendShapeChannelMapping>()
{
	return FMeshBlendShapeChannelMapping::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMeshBlendShapeChannelMapping(FMeshBlendShapeChannelMapping::StaticStruct, TEXT("/Script/RigLogicModule"), TEXT("MeshBlendShapeChannelMapping"), false, nullptr, nullptr);
static struct FScriptStruct_RigLogicModule_StaticRegisterNativesFMeshBlendShapeChannelMapping
{
	FScriptStruct_RigLogicModule_StaticRegisterNativesFMeshBlendShapeChannelMapping()
	{
		UScriptStruct::DeferCppStructOps<FMeshBlendShapeChannelMapping>(FName(TEXT("MeshBlendShapeChannelMapping")));
	}
} ScriptStruct_RigLogicModule_StaticRegisterNativesFMeshBlendShapeChannelMapping;
	struct Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MeshIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendShapeChannelIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BlendShapeChannelIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMeshBlendShapeChannelMapping>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewProp_MeshIndex_MetaData[] = {
		{ "Category", "RigLogic" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewProp_MeshIndex = { "MeshIndex", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMeshBlendShapeChannelMapping, MeshIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewProp_MeshIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewProp_MeshIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewProp_BlendShapeChannelIndex_MetaData[] = {
		{ "Category", "RigLogic" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewProp_BlendShapeChannelIndex = { "BlendShapeChannelIndex", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMeshBlendShapeChannelMapping, BlendShapeChannelIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewProp_BlendShapeChannelIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewProp_BlendShapeChannelIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewProp_MeshIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::NewProp_BlendShapeChannelIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RigLogicModule,
		nullptr,
		&NewStructOps,
		"MeshBlendShapeChannelMapping",
		sizeof(FMeshBlendShapeChannelMapping),
		alignof(FMeshBlendShapeChannelMapping),
		Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MeshBlendShapeChannelMapping"), sizeof(FMeshBlendShapeChannelMapping), Get_Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMeshBlendShapeChannelMapping_Hash() { return 2985586517U; }
class UScriptStruct* FCoordinateSystem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RIGLOGICMODULE_API uint32 Get_Z_Construct_UScriptStruct_FCoordinateSystem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCoordinateSystem, Z_Construct_UPackage__Script_RigLogicModule(), TEXT("CoordinateSystem"), sizeof(FCoordinateSystem), Get_Z_Construct_UScriptStruct_FCoordinateSystem_Hash());
	}
	return Singleton;
}
template<> RIGLOGICMODULE_API UScriptStruct* StaticStruct<FCoordinateSystem>()
{
	return FCoordinateSystem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCoordinateSystem(FCoordinateSystem::StaticStruct, TEXT("/Script/RigLogicModule"), TEXT("CoordinateSystem"), false, nullptr, nullptr);
static struct FScriptStruct_RigLogicModule_StaticRegisterNativesFCoordinateSystem
{
	FScriptStruct_RigLogicModule_StaticRegisterNativesFCoordinateSystem()
	{
		UScriptStruct::DeferCppStructOps<FCoordinateSystem>(FName(TEXT("CoordinateSystem")));
	}
} ScriptStruct_RigLogicModule_StaticRegisterNativesFCoordinateSystem;
	struct Z_Construct_UScriptStruct_FCoordinateSystem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_XAxis_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_XAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_XAxis;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_YAxis_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_YAxis;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ZAxis_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ZAxis;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoordinateSystem_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCoordinateSystem>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_XAxis_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_XAxis_MetaData[] = {
		{ "Category", "RigLogic" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_XAxis = { "XAxis", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCoordinateSystem, XAxis), Z_Construct_UEnum_RigLogicModule_EDirection, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_XAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_XAxis_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_YAxis_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_YAxis_MetaData[] = {
		{ "Category", "RigLogic" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_YAxis = { "YAxis", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCoordinateSystem, YAxis), Z_Construct_UEnum_RigLogicModule_EDirection, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_YAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_YAxis_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_ZAxis_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_ZAxis_MetaData[] = {
		{ "Category", "RigLogic" },
		{ "ModuleRelativePath", "Public/DNACommon.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_ZAxis = { "ZAxis", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCoordinateSystem, ZAxis), Z_Construct_UEnum_RigLogicModule_EDirection, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_ZAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_ZAxis_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCoordinateSystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_XAxis_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_XAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_YAxis_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_YAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_ZAxis_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoordinateSystem_Statics::NewProp_ZAxis,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCoordinateSystem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RigLogicModule,
		nullptr,
		&NewStructOps,
		"CoordinateSystem",
		sizeof(FCoordinateSystem),
		alignof(FCoordinateSystem),
		Z_Construct_UScriptStruct_FCoordinateSystem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoordinateSystem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCoordinateSystem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoordinateSystem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCoordinateSystem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCoordinateSystem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RigLogicModule();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CoordinateSystem"), sizeof(FCoordinateSystem), Get_Z_Construct_UScriptStruct_FCoordinateSystem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCoordinateSystem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCoordinateSystem_Hash() { return 3841500047U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
