// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RigLogicEditor/Public/DNAAssetImportUI.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDNAAssetImportUI() {}
// Cross Module References
	RIGLOGICEDITOR_API UClass* Z_Construct_UClass_UDNAAssetImportUI_NoRegister();
	RIGLOGICEDITOR_API UClass* Z_Construct_UClass_UDNAAssetImportUI();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_RigLogicEditor();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMesh_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UDNAAssetImportUI::execResetToDefault)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetToDefault();
		P_NATIVE_END;
	}
	void UDNAAssetImportUI::StaticRegisterNativesUDNAAssetImportUI()
	{
		UClass* Class = UDNAAssetImportUI::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ResetToDefault", &UDNAAssetImportUI::execResetToDefault },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDNAAssetImportUI_ResetToDefault_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDNAAssetImportUI_ResetToDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Miscellaneous" },
		{ "ModuleRelativePath", "Public/DNAAssetImportUI.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDNAAssetImportUI_ResetToDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDNAAssetImportUI, nullptr, "ResetToDefault", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDNAAssetImportUI_ResetToDefault_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDNAAssetImportUI_ResetToDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDNAAssetImportUI_ResetToDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDNAAssetImportUI_ResetToDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDNAAssetImportUI_NoRegister()
	{
		return UDNAAssetImportUI::StaticClass();
	}
	struct Z_Construct_UClass_UDNAAssetImportUI_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileCreator_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileCreator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDNAAssetImportUI_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RigLogicEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDNAAssetImportUI_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDNAAssetImportUI_ResetToDefault, "ResetToDefault" }, // 3447222199
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDNAAssetImportUI_Statics::Class_MetaDataParams[] = {
		{ "AutoExpandCategories", "Mesh" },
		{ "IncludePath", "DNAAssetImportUI.h" },
		{ "ModuleRelativePath", "Public/DNAAssetImportUI.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_SkeletalMesh_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** Skeletal mesh to use for imported DNA asset. When importing DNA, leaving this as \"None\" will generate new skeletal mesh. */" },
		{ "ImportType", "DNAAsset" },
		{ "ModuleRelativePath", "Public/DNAAssetImportUI.h" },
		{ "ToolTip", "Skeletal mesh to use for imported DNA asset. When importing DNA, leaving this as \"None\" will generate new skeletal mesh." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDNAAssetImportUI, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_SkeletalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_SkeletalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_FileVersion_MetaData[] = {
		{ "Category", "DNAFileInformation" },
		{ "Comment", "/* The DNA file version */" },
		{ "DisplayName", "File Version" },
		{ "ImportType", "DNAAsset" },
		{ "ModuleRelativePath", "Public/DNAAssetImportUI.h" },
		{ "ToolTip", "The DNA file version" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_FileVersion = { "FileVersion", nullptr, (EPropertyFlags)0x0010000000022001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDNAAssetImportUI, FileVersion), METADATA_PARAMS(Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_FileVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_FileVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_FileCreator_MetaData[] = {
		{ "Category", "DNAFileInformation" },
		{ "Comment", "/* The file creator information */" },
		{ "DisplayName", "File Creator" },
		{ "ImportType", "DNAAsset" },
		{ "ModuleRelativePath", "Public/DNAAssetImportUI.h" },
		{ "ToolTip", "The file creator information" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_FileCreator = { "FileCreator", nullptr, (EPropertyFlags)0x0010000000022001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDNAAssetImportUI, FileCreator), METADATA_PARAMS(Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_FileCreator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_FileCreator_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDNAAssetImportUI_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_FileVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDNAAssetImportUI_Statics::NewProp_FileCreator,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDNAAssetImportUI_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDNAAssetImportUI>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDNAAssetImportUI_Statics::ClassParams = {
		&UDNAAssetImportUI::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDNAAssetImportUI_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAssetImportUI_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDNAAssetImportUI_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDNAAssetImportUI_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDNAAssetImportUI()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDNAAssetImportUI_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDNAAssetImportUI, 4205702203);
	template<> RIGLOGICEDITOR_API UClass* StaticClass<UDNAAssetImportUI>()
	{
		return UDNAAssetImportUI::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDNAAssetImportUI(Z_Construct_UClass_UDNAAssetImportUI, &UDNAAssetImportUI::StaticClass, TEXT("/Script/RigLogicEditor"), TEXT("UDNAAssetImportUI"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDNAAssetImportUI);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
