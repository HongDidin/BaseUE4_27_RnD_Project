// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkCurveDebugUI/Public/LiveLinkDebuggerBlueprintLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkDebuggerBlueprintLibrary() {}
// Cross Module References
	LIVELINKCURVEDEBUGUI_API UClass* Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_NoRegister();
	LIVELINKCURVEDEBUGUI_API UClass* Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_LiveLinkCurveDebugUI();
// End Cross Module References
	DEFINE_FUNCTION(ULiveLinkDebuggerBlueprintLibrary::execHideLiveLinkDebugger)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		ULiveLinkDebuggerBlueprintLibrary::HideLiveLinkDebugger();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULiveLinkDebuggerBlueprintLibrary::execDisplayLiveLinkDebugger)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_SubjectName);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULiveLinkDebuggerBlueprintLibrary::DisplayLiveLinkDebugger(Z_Param_SubjectName);
		P_NATIVE_END;
	}
	void ULiveLinkDebuggerBlueprintLibrary::StaticRegisterNativesULiveLinkDebuggerBlueprintLibrary()
	{
		UClass* Class = ULiveLinkDebuggerBlueprintLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DisplayLiveLinkDebugger", &ULiveLinkDebuggerBlueprintLibrary::execDisplayLiveLinkDebugger },
			{ "HideLiveLinkDebugger", &ULiveLinkDebuggerBlueprintLibrary::execHideLiveLinkDebugger },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics
	{
		struct LiveLinkDebuggerBlueprintLibrary_eventDisplayLiveLinkDebugger_Parms
		{
			FString SubjectName;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SubjectName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics::NewProp_SubjectName = { "SubjectName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LiveLinkDebuggerBlueprintLibrary_eventDisplayLiveLinkDebugger_Parms, SubjectName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics::NewProp_SubjectName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics::Function_MetaDataParams[] = {
		{ "Category", "LiveLinkDebug" },
		{ "ModuleRelativePath", "Public/LiveLinkDebuggerBlueprintLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary, nullptr, "DisplayLiveLinkDebugger", nullptr, nullptr, sizeof(LiveLinkDebuggerBlueprintLibrary_eventDisplayLiveLinkDebugger_Parms), Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_HideLiveLinkDebugger_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_HideLiveLinkDebugger_Statics::Function_MetaDataParams[] = {
		{ "Category", "LiveLinkDebug" },
		{ "ModuleRelativePath", "Public/LiveLinkDebuggerBlueprintLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_HideLiveLinkDebugger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary, nullptr, "HideLiveLinkDebugger", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_HideLiveLinkDebugger_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_HideLiveLinkDebugger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_HideLiveLinkDebugger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_HideLiveLinkDebugger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_NoRegister()
	{
		return ULiveLinkDebuggerBlueprintLibrary::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkCurveDebugUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_DisplayLiveLinkDebugger, "DisplayLiveLinkDebugger" }, // 493726506
		{ &Z_Construct_UFunction_ULiveLinkDebuggerBlueprintLibrary_HideLiveLinkDebugger, "HideLiveLinkDebugger" }, // 3070438387
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LiveLinkDebuggerBlueprintLibrary.h" },
		{ "ModuleRelativePath", "Public/LiveLinkDebuggerBlueprintLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkDebuggerBlueprintLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics::ClassParams = {
		&ULiveLinkDebuggerBlueprintLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkDebuggerBlueprintLibrary, 123960705);
	template<> LIVELINKCURVEDEBUGUI_API UClass* StaticClass<ULiveLinkDebuggerBlueprintLibrary>()
	{
		return ULiveLinkDebuggerBlueprintLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkDebuggerBlueprintLibrary(Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary, &ULiveLinkDebuggerBlueprintLibrary::StaticClass, TEXT("/Script/LiveLinkCurveDebugUI"), TEXT("ULiveLinkDebuggerBlueprintLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkDebuggerBlueprintLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
