// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkCurveDebugUI/Public/LiveLinkDebuggerSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkDebuggerSettings() {}
// Cross Module References
	LIVELINKCURVEDEBUGUI_API UClass* Z_Construct_UClass_ULiveLinkDebuggerSettings_NoRegister();
	LIVELINKCURVEDEBUGUI_API UClass* Z_Construct_UClass_ULiveLinkDebuggerSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LiveLinkCurveDebugUI();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSlateColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
// End Cross Module References
	DEFINE_FUNCTION(ULiveLinkDebuggerSettings::execGetBarColorForCurveValue)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_CurveValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FSlateColor*)Z_Param__Result=P_THIS->GetBarColorForCurveValue(Z_Param_CurveValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULiveLinkDebuggerSettings::execGetDPIScaleBasedOnSize)
	{
		P_GET_STRUCT(FIntPoint,Z_Param_Size);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetDPIScaleBasedOnSize(Z_Param_Size);
		P_NATIVE_END;
	}
	void ULiveLinkDebuggerSettings::StaticRegisterNativesULiveLinkDebuggerSettings()
	{
		UClass* Class = ULiveLinkDebuggerSettings::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetBarColorForCurveValue", &ULiveLinkDebuggerSettings::execGetBarColorForCurveValue },
			{ "GetDPIScaleBasedOnSize", &ULiveLinkDebuggerSettings::execGetDPIScaleBasedOnSize },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics
	{
		struct LiveLinkDebuggerSettings_eventGetBarColorForCurveValue_Parms
		{
			float CurveValue;
			FSlateColor ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurveValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::NewProp_CurveValue = { "CurveValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LiveLinkDebuggerSettings_eventGetBarColorForCurveValue_Parms, CurveValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LiveLinkDebuggerSettings_eventGetBarColorForCurveValue_Parms, ReturnValue), Z_Construct_UScriptStruct_FSlateColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::NewProp_CurveValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "LiveLinkDebugger" },
		{ "ModuleRelativePath", "Public/LiveLinkDebuggerSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULiveLinkDebuggerSettings, nullptr, "GetBarColorForCurveValue", nullptr, nullptr, sizeof(LiveLinkDebuggerSettings_eventGetBarColorForCurveValue_Parms), Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics
	{
		struct LiveLinkDebuggerSettings_eventGetDPIScaleBasedOnSize_Parms
		{
			FIntPoint Size;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LiveLinkDebuggerSettings_eventGetDPIScaleBasedOnSize_Parms, Size), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LiveLinkDebuggerSettings_eventGetDPIScaleBasedOnSize_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::NewProp_Size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "LiveLinkDebugger" },
		{ "ModuleRelativePath", "Public/LiveLinkDebuggerSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULiveLinkDebuggerSettings, nullptr, "GetDPIScaleBasedOnSize", nullptr, nullptr, sizeof(LiveLinkDebuggerSettings_eventGetDPIScaleBasedOnSize_Parms), Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULiveLinkDebuggerSettings_NoRegister()
	{
		return ULiveLinkDebuggerSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinBarColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MinBarColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxBarColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MaxBarColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DPIScaleMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DPIScaleMultiplier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkCurveDebugUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetBarColorForCurveValue, "GetBarColorForCurveValue" }, // 1099154015
		{ &Z_Construct_UFunction_ULiveLinkDebuggerSettings_GetDPIScaleBasedOnSize, "GetDPIScaleBasedOnSize" }, // 2088583373
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LiveLinkDebuggerSettings.h" },
		{ "ModuleRelativePath", "Public/LiveLinkDebuggerSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_MinBarColor_MetaData[] = {
		{ "Category", "LiveLinkDebugger" },
		{ "Comment", "//Color used when the CurveValue bar is at 0\n" },
		{ "DisplayName", "Minimum Bar Color Value" },
		{ "ModuleRelativePath", "Public/LiveLinkDebuggerSettings.h" },
		{ "ToolTip", "Color used when the CurveValue bar is at 0" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_MinBarColor = { "MinBarColor", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkDebuggerSettings, MinBarColor), Z_Construct_UScriptStruct_FSlateColor, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_MinBarColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_MinBarColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_MaxBarColor_MetaData[] = {
		{ "Category", "LiveLinkDebugger" },
		{ "Comment", "//Color used when the CurveValueBar is at 1.0\n" },
		{ "DisplayName", "Maximum Bar Color Value" },
		{ "ModuleRelativePath", "Public/LiveLinkDebuggerSettings.h" },
		{ "ToolTip", "Color used when the CurveValueBar is at 1.0" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_MaxBarColor = { "MaxBarColor", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkDebuggerSettings, MaxBarColor), Z_Construct_UScriptStruct_FSlateColor, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_MaxBarColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_MaxBarColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_DPIScaleMultiplier_MetaData[] = {
		{ "Category", "LiveLinkDebugger" },
		{ "Comment", "//This multiplier is used on the Viewport Widget version (IE: In Game) as it needs to be slightly more aggresive then the PC version\n" },
		{ "DisplayName", "DPI Scale Multiplier" },
		{ "ModuleRelativePath", "Public/LiveLinkDebuggerSettings.h" },
		{ "ToolTip", "This multiplier is used on the Viewport Widget version (IE: In Game) as it needs to be slightly more aggresive then the PC version" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_DPIScaleMultiplier = { "DPIScaleMultiplier", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkDebuggerSettings, DPIScaleMultiplier), METADATA_PARAMS(Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_DPIScaleMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_DPIScaleMultiplier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_MinBarColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_MaxBarColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::NewProp_DPIScaleMultiplier,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkDebuggerSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::ClassParams = {
		&ULiveLinkDebuggerSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkDebuggerSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkDebuggerSettings, 1913463677);
	template<> LIVELINKCURVEDEBUGUI_API UClass* StaticClass<ULiveLinkDebuggerSettings>()
	{
		return ULiveLinkDebuggerSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkDebuggerSettings(Z_Construct_UClass_ULiveLinkDebuggerSettings, &ULiveLinkDebuggerSettings::StaticClass, TEXT("/Script/LiveLinkCurveDebugUI"), TEXT("ULiveLinkDebuggerSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkDebuggerSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
