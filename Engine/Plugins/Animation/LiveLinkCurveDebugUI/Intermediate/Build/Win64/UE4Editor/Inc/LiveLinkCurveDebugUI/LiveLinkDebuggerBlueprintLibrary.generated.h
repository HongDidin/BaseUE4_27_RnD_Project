// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKCURVEDEBUGUI_LiveLinkDebuggerBlueprintLibrary_generated_h
#error "LiveLinkDebuggerBlueprintLibrary.generated.h already included, missing '#pragma once' in LiveLinkDebuggerBlueprintLibrary.h"
#endif
#define LIVELINKCURVEDEBUGUI_LiveLinkDebuggerBlueprintLibrary_generated_h

#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHideLiveLinkDebugger); \
	DECLARE_FUNCTION(execDisplayLiveLinkDebugger);


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHideLiveLinkDebugger); \
	DECLARE_FUNCTION(execDisplayLiveLinkDebugger);


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkDebuggerBlueprintLibrary(); \
	friend struct Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkDebuggerBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkCurveDebugUI"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkDebuggerBlueprintLibrary)


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkDebuggerBlueprintLibrary(); \
	friend struct Z_Construct_UClass_ULiveLinkDebuggerBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkDebuggerBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkCurveDebugUI"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkDebuggerBlueprintLibrary)


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkDebuggerBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkDebuggerBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkDebuggerBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkDebuggerBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkDebuggerBlueprintLibrary(ULiveLinkDebuggerBlueprintLibrary&&); \
	NO_API ULiveLinkDebuggerBlueprintLibrary(const ULiveLinkDebuggerBlueprintLibrary&); \
public:


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkDebuggerBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkDebuggerBlueprintLibrary(ULiveLinkDebuggerBlueprintLibrary&&); \
	NO_API ULiveLinkDebuggerBlueprintLibrary(const ULiveLinkDebuggerBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkDebuggerBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkDebuggerBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkDebuggerBlueprintLibrary)


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_9_PROLOG
#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_INCLASS \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKCURVEDEBUGUI_API UClass* StaticClass<class ULiveLinkDebuggerBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
