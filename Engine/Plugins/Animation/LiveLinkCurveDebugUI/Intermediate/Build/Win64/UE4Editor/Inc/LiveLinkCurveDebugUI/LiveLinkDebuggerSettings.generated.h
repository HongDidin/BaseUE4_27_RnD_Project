// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FSlateColor;
struct FIntPoint;
#ifdef LIVELINKCURVEDEBUGUI_LiveLinkDebuggerSettings_generated_h
#error "LiveLinkDebuggerSettings.generated.h already included, missing '#pragma once' in LiveLinkDebuggerSettings.h"
#endif
#define LIVELINKCURVEDEBUGUI_LiveLinkDebuggerSettings_generated_h

#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetBarColorForCurveValue); \
	DECLARE_FUNCTION(execGetDPIScaleBasedOnSize);


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetBarColorForCurveValue); \
	DECLARE_FUNCTION(execGetDPIScaleBasedOnSize);


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkDebuggerSettings(); \
	friend struct Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkDebuggerSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/LiveLinkCurveDebugUI"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkDebuggerSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkDebuggerSettings(); \
	friend struct Z_Construct_UClass_ULiveLinkDebuggerSettings_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkDebuggerSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/LiveLinkCurveDebugUI"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkDebuggerSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkDebuggerSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkDebuggerSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkDebuggerSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkDebuggerSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkDebuggerSettings(ULiveLinkDebuggerSettings&&); \
	NO_API ULiveLinkDebuggerSettings(const ULiveLinkDebuggerSettings&); \
public:


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkDebuggerSettings(ULiveLinkDebuggerSettings&&); \
	NO_API ULiveLinkDebuggerSettings(const ULiveLinkDebuggerSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkDebuggerSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkDebuggerSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULiveLinkDebuggerSettings)


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_14_PROLOG
#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_INCLASS \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h_19_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKCURVEDEBUGUI_API UClass* StaticClass<class ULiveLinkDebuggerSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLinkCurveDebugUI_Source_LiveLinkCurveDebugUI_Public_LiveLinkDebuggerSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
