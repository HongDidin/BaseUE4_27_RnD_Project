// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkEditor/Private/Sequencer/LiveLinkSequencerFilters.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkSequencerFilters() {}
// Cross Module References
	LIVELINKEDITOR_API UClass* Z_Construct_UClass_ULiveLinkSequencerTrackFilter_NoRegister();
	LIVELINKEDITOR_API UClass* Z_Construct_UClass_ULiveLinkSequencerTrackFilter();
	SEQUENCER_API UClass* Z_Construct_UClass_USequencerTrackFilterExtension();
	UPackage* Z_Construct_UPackage__Script_LiveLinkEditor();
// End Cross Module References
	void ULiveLinkSequencerTrackFilter::StaticRegisterNativesULiveLinkSequencerTrackFilter()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkSequencerTrackFilter_NoRegister()
	{
		return ULiveLinkSequencerTrackFilter::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkSequencerTrackFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkSequencerTrackFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USequencerTrackFilterExtension,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkSequencerTrackFilter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sequencer/LiveLinkSequencerFilters.h" },
		{ "ModuleRelativePath", "Private/Sequencer/LiveLinkSequencerFilters.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkSequencerTrackFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkSequencerTrackFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkSequencerTrackFilter_Statics::ClassParams = {
		&ULiveLinkSequencerTrackFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkSequencerTrackFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkSequencerTrackFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkSequencerTrackFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkSequencerTrackFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkSequencerTrackFilter, 1100475511);
	template<> LIVELINKEDITOR_API UClass* StaticClass<ULiveLinkSequencerTrackFilter>()
	{
		return ULiveLinkSequencerTrackFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkSequencerTrackFilter(Z_Construct_UClass_ULiveLinkSequencerTrackFilter, &ULiveLinkSequencerTrackFilter::StaticClass, TEXT("/Script/LiveLinkEditor"), TEXT("ULiveLinkSequencerTrackFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkSequencerTrackFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
