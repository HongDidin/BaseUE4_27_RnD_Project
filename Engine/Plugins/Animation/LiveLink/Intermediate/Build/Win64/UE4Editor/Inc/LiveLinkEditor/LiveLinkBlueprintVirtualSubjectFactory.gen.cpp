// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkEditor/Public/VirtualSubjects/LiveLinkBlueprintVirtualSubjectFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkBlueprintVirtualSubjectFactory() {}
// Cross Module References
	LIVELINKEDITOR_API UClass* Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_NoRegister();
	LIVELINKEDITOR_API UClass* Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_LiveLinkEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	LIVELINKINTERFACE_API UClass* Z_Construct_UClass_ULiveLinkRole_NoRegister();
// End Cross Module References
	void ULiveLinkBlueprintVirtualSubjectFactory::StaticRegisterNativesULiveLinkBlueprintVirtualSubjectFactory()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_NoRegister()
	{
		return ULiveLinkBlueprintVirtualSubjectFactory::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ParentClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Role_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Role;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "VirtualSubjects/LiveLinkBlueprintVirtualSubjectFactory.h" },
		{ "ModuleRelativePath", "Public/VirtualSubjects/LiveLinkBlueprintVirtualSubjectFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::NewProp_ParentClass_MetaData[] = {
		{ "AllowAbstract", "" },
		{ "BlueprintBaseOnly", "" },
		{ "Category", "BlueprintVirtualSubjectFactory" },
		{ "Comment", "// The parent class of the created blueprint\n" },
		{ "ModuleRelativePath", "Public/VirtualSubjects/LiveLinkBlueprintVirtualSubjectFactory.h" },
		{ "ToolTip", "The parent class of the created blueprint" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::NewProp_ParentClass = { "ParentClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkBlueprintVirtualSubjectFactory, ParentClass), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::NewProp_ParentClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::NewProp_ParentClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::NewProp_Role_MetaData[] = {
		{ "Category", "Live Link Blueprint Virtual Subject Factory" },
		{ "ModuleRelativePath", "Public/VirtualSubjects/LiveLinkBlueprintVirtualSubjectFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::NewProp_Role = { "Role", nullptr, (EPropertyFlags)0x0014000000000004, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkBlueprintVirtualSubjectFactory, Role), Z_Construct_UClass_ULiveLinkRole_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::NewProp_Role_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::NewProp_Role_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::NewProp_ParentClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::NewProp_Role,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkBlueprintVirtualSubjectFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::ClassParams = {
		&ULiveLinkBlueprintVirtualSubjectFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkBlueprintVirtualSubjectFactory, 4257359447);
	template<> LIVELINKEDITOR_API UClass* StaticClass<ULiveLinkBlueprintVirtualSubjectFactory>()
	{
		return ULiveLinkBlueprintVirtualSubjectFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkBlueprintVirtualSubjectFactory(Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory, &ULiveLinkBlueprintVirtualSubjectFactory::StaticClass, TEXT("/Script/LiveLinkEditor"), TEXT("ULiveLinkBlueprintVirtualSubjectFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkBlueprintVirtualSubjectFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
