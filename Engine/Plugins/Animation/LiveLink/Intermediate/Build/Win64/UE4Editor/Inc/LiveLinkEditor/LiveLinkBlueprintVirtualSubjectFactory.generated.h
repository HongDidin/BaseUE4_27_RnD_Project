// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKEDITOR_LiveLinkBlueprintVirtualSubjectFactory_generated_h
#error "LiveLinkBlueprintVirtualSubjectFactory.generated.h already included, missing '#pragma once' in LiveLinkBlueprintVirtualSubjectFactory.h"
#endif
#define LIVELINKEDITOR_LiveLinkBlueprintVirtualSubjectFactory_generated_h

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_RPC_WRAPPERS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkBlueprintVirtualSubjectFactory(); \
	friend struct Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkBlueprintVirtualSubjectFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkEditor"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkBlueprintVirtualSubjectFactory)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkBlueprintVirtualSubjectFactory(); \
	friend struct Z_Construct_UClass_ULiveLinkBlueprintVirtualSubjectFactory_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkBlueprintVirtualSubjectFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkEditor"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkBlueprintVirtualSubjectFactory)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkBlueprintVirtualSubjectFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkBlueprintVirtualSubjectFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkBlueprintVirtualSubjectFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkBlueprintVirtualSubjectFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkBlueprintVirtualSubjectFactory(ULiveLinkBlueprintVirtualSubjectFactory&&); \
	NO_API ULiveLinkBlueprintVirtualSubjectFactory(const ULiveLinkBlueprintVirtualSubjectFactory&); \
public:


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkBlueprintVirtualSubjectFactory(ULiveLinkBlueprintVirtualSubjectFactory&&); \
	NO_API ULiveLinkBlueprintVirtualSubjectFactory(const ULiveLinkBlueprintVirtualSubjectFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkBlueprintVirtualSubjectFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkBlueprintVirtualSubjectFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULiveLinkBlueprintVirtualSubjectFactory)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_13_PROLOG
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_INCLASS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKEDITOR_API UClass* StaticClass<class ULiveLinkBlueprintVirtualSubjectFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Public_VirtualSubjects_LiveLinkBlueprintVirtualSubjectFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
