// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkGraphNode/Public/K2Node_EvaluateLiveLinkFrame.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_EvaluateLiveLinkFrame() {}
// Cross Module References
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_NoRegister();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_LiveLinkGraphNode();
// End Cross Module References
	void UK2Node_EvaluateLiveLinkFrame::StaticRegisterNativesUK2Node_EvaluateLiveLinkFrame()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_NoRegister()
	{
		return UK2Node_EvaluateLiveLinkFrame::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkGraphNode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_EvaluateLiveLinkFrame.h" },
		{ "ModuleRelativePath", "Public/K2Node_EvaluateLiveLinkFrame.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_EvaluateLiveLinkFrame>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_Statics::ClassParams = {
		&UK2Node_EvaluateLiveLinkFrame::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_EvaluateLiveLinkFrame, 2545171857);
	template<> LIVELINKGRAPHNODE_API UClass* StaticClass<UK2Node_EvaluateLiveLinkFrame>()
	{
		return UK2Node_EvaluateLiveLinkFrame::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_EvaluateLiveLinkFrame(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame, &UK2Node_EvaluateLiveLinkFrame::StaticClass, TEXT("/Script/LiveLinkGraphNode"), TEXT("UK2Node_EvaluateLiveLinkFrame"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_EvaluateLiveLinkFrame);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
