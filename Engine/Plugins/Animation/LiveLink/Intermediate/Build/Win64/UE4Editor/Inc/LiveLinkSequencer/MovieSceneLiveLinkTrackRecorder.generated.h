// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKSEQUENCER_MovieSceneLiveLinkTrackRecorder_generated_h
#error "MovieSceneLiveLinkTrackRecorder.generated.h already included, missing '#pragma once' in MovieSceneLiveLinkTrackRecorder.h"
#endif
#define LIVELINKSEQUENCER_MovieSceneLiveLinkTrackRecorder_generated_h

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_RPC_WRAPPERS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneLiveLinkTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneLiveLinkTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneLiveLinkTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/LiveLinkSequencer"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneLiveLinkTrackRecorder)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneLiveLinkTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneLiveLinkTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneLiveLinkTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/LiveLinkSequencer"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneLiveLinkTrackRecorder)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneLiveLinkTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneLiveLinkTrackRecorder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneLiveLinkTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneLiveLinkTrackRecorder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneLiveLinkTrackRecorder(UMovieSceneLiveLinkTrackRecorder&&); \
	NO_API UMovieSceneLiveLinkTrackRecorder(const UMovieSceneLiveLinkTrackRecorder&); \
public:


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneLiveLinkTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneLiveLinkTrackRecorder(UMovieSceneLiveLinkTrackRecorder&&); \
	NO_API UMovieSceneLiveLinkTrackRecorder(const UMovieSceneLiveLinkTrackRecorder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneLiveLinkTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneLiveLinkTrackRecorder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneLiveLinkTrackRecorder)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_28_PROLOG
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_INCLASS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKSEQUENCER_API UClass* StaticClass<class UMovieSceneLiveLinkTrackRecorder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkTrackRecorder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
