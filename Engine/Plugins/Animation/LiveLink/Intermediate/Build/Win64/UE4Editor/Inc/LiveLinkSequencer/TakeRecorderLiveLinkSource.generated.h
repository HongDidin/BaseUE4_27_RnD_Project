// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKSEQUENCER_TakeRecorderLiveLinkSource_generated_h
#error "TakeRecorderLiveLinkSource.generated.h already included, missing '#pragma once' in TakeRecorderLiveLinkSource.h"
#endif
#define LIVELINKSEQUENCER_TakeRecorderLiveLinkSource_generated_h

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics; \
	LIVELINKSEQUENCER_API static class UScriptStruct* StaticStruct();


template<> LIVELINKSEQUENCER_API UScriptStruct* StaticStruct<struct FLiveLinkSubjectProperty>();

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_RPC_WRAPPERS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkSubjectProperties(); \
	friend struct Z_Construct_UClass_ULiveLinkSubjectProperties_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkSubjectProperties, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkSequencer"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkSubjectProperties)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkSubjectProperties(); \
	friend struct Z_Construct_UClass_ULiveLinkSubjectProperties_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkSubjectProperties, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkSequencer"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkSubjectProperties)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkSubjectProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkSubjectProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkSubjectProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkSubjectProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkSubjectProperties(ULiveLinkSubjectProperties&&); \
	NO_API ULiveLinkSubjectProperties(const ULiveLinkSubjectProperties&); \
public:


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkSubjectProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkSubjectProperties(ULiveLinkSubjectProperties&&); \
	NO_API ULiveLinkSubjectProperties(const ULiveLinkSubjectProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkSubjectProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkSubjectProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkSubjectProperties)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_37_PROLOG
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_INCLASS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_40_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKSEQUENCER_API UClass* StaticClass<class ULiveLinkSubjectProperties>();

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_RPC_WRAPPERS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderLiveLinkSource(); \
	friend struct Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderLiveLinkSource, UTakeRecorderSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkSequencer"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderLiveLinkSource) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorSettings");} \



#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderLiveLinkSource(); \
	friend struct Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderLiveLinkSource, UTakeRecorderSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkSequencer"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderLiveLinkSource) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorSettings");} \



#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderLiveLinkSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderLiveLinkSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderLiveLinkSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderLiveLinkSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderLiveLinkSource(UTakeRecorderLiveLinkSource&&); \
	NO_API UTakeRecorderLiveLinkSource(const UTakeRecorderLiveLinkSource&); \
public:


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderLiveLinkSource(UTakeRecorderLiveLinkSource&&); \
	NO_API UTakeRecorderLiveLinkSource(const UTakeRecorderLiveLinkSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderLiveLinkSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderLiveLinkSource); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderLiveLinkSource)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_49_PROLOG
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_INCLASS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h_54_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKSEQUENCER_API UClass* StaticClass<class UTakeRecorderLiveLinkSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Private_TakeRecorderSource_TakeRecorderLiveLinkSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
