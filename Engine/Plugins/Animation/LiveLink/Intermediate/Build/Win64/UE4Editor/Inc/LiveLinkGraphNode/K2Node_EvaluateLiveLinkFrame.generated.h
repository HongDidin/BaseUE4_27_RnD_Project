// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKGRAPHNODE_K2Node_EvaluateLiveLinkFrame_generated_h
#error "K2Node_EvaluateLiveLinkFrame.generated.h already included, missing '#pragma once' in K2Node_EvaluateLiveLinkFrame.h"
#endif
#define LIVELINKGRAPHNODE_K2Node_EvaluateLiveLinkFrame_generated_h

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_RPC_WRAPPERS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_EvaluateLiveLinkFrame(); \
	friend struct Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_Statics; \
public: \
	DECLARE_CLASS(UK2Node_EvaluateLiveLinkFrame, UK2Node, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/LiveLinkGraphNode"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_EvaluateLiveLinkFrame)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_EvaluateLiveLinkFrame(); \
	friend struct Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame_Statics; \
public: \
	DECLARE_CLASS(UK2Node_EvaluateLiveLinkFrame, UK2Node, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/LiveLinkGraphNode"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_EvaluateLiveLinkFrame)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_EvaluateLiveLinkFrame(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_EvaluateLiveLinkFrame) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_EvaluateLiveLinkFrame); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_EvaluateLiveLinkFrame); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_EvaluateLiveLinkFrame(UK2Node_EvaluateLiveLinkFrame&&); \
	NO_API UK2Node_EvaluateLiveLinkFrame(const UK2Node_EvaluateLiveLinkFrame&); \
public:


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_EvaluateLiveLinkFrame(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_EvaluateLiveLinkFrame(UK2Node_EvaluateLiveLinkFrame&&); \
	NO_API UK2Node_EvaluateLiveLinkFrame(const UK2Node_EvaluateLiveLinkFrame&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_EvaluateLiveLinkFrame); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_EvaluateLiveLinkFrame); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_EvaluateLiveLinkFrame)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_21_PROLOG
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_INCLASS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKGRAPHNODE_API UClass* StaticClass<class UK2Node_EvaluateLiveLinkFrame>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_EvaluateLiveLinkFrame_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
