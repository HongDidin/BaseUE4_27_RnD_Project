// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKEDITOR_LiveLinkTest_generated_h
#error "LiveLinkTest.generated.h already included, missing '#pragma once' in LiveLinkTest.h"
#endif
#define LIVELINKEDITOR_LiveLinkTest_generated_h

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkTest_h_38_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics; \
	LIVELINKEDITOR_API static class UScriptStruct* StaticStruct(); \
	typedef FLiveLinkBaseFrameData Super;


template<> LIVELINKEDITOR_API UScriptStruct* StaticStruct<struct FLiveLinkTestFrameDataInternal>();

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkTest_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics; \
	LIVELINKEDITOR_API static class UScriptStruct* StaticStruct();


template<> LIVELINKEDITOR_API UScriptStruct* StaticStruct<struct FLiveLinkInnerTestInternal>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
