// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkGraphNode/Public/K2Node_UpdateVirtualSubjectDataBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_UpdateVirtualSubjectDataBase() {}
// Cross Module References
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_NoRegister();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_LiveLinkGraphNode();
// End Cross Module References
	void UK2Node_UpdateVirtualSubjectDataBase::StaticRegisterNativesUK2Node_UpdateVirtualSubjectDataBase()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_NoRegister()
	{
		return UK2Node_UpdateVirtualSubjectDataBase::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkGraphNode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_UpdateVirtualSubjectDataBase.h" },
		{ "ModuleRelativePath", "Public/K2Node_UpdateVirtualSubjectDataBase.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_UpdateVirtualSubjectDataBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_Statics::ClassParams = {
		&UK2Node_UpdateVirtualSubjectDataBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_UpdateVirtualSubjectDataBase, 838169499);
	template<> LIVELINKGRAPHNODE_API UClass* StaticClass<UK2Node_UpdateVirtualSubjectDataBase>()
	{
		return UK2Node_UpdateVirtualSubjectDataBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_UpdateVirtualSubjectDataBase(Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase, &UK2Node_UpdateVirtualSubjectDataBase::StaticClass, TEXT("/Script/LiveLinkGraphNode"), TEXT("UK2Node_UpdateVirtualSubjectDataBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_UpdateVirtualSubjectDataBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
