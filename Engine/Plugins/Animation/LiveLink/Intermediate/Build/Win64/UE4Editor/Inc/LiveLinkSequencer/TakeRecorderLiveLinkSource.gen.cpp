// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkSequencer/Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderLiveLinkSource() {}
// Cross Module References
	LIVELINKSEQUENCER_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkSubjectProperty();
	UPackage* Z_Construct_UPackage__Script_LiveLinkSequencer();
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_ULiveLinkSubjectProperties_NoRegister();
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_ULiveLinkSubjectProperties();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_UTakeRecorderLiveLinkSource_NoRegister();
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_UTakeRecorderLiveLinkSource();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkTrackRecorder_NoRegister();
// End Cross Module References
class UScriptStruct* FLiveLinkSubjectProperty::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKSEQUENCER_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkSubjectProperty, Z_Construct_UPackage__Script_LiveLinkSequencer(), TEXT("LiveLinkSubjectProperty"), sizeof(FLiveLinkSubjectProperty), Get_Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Hash());
	}
	return Singleton;
}
template<> LIVELINKSEQUENCER_API UScriptStruct* StaticStruct<FLiveLinkSubjectProperty>()
{
	return FLiveLinkSubjectProperty::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkSubjectProperty(FLiveLinkSubjectProperty::StaticStruct, TEXT("/Script/LiveLinkSequencer"), TEXT("LiveLinkSubjectProperty"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkSequencer_StaticRegisterNativesFLiveLinkSubjectProperty
{
	FScriptStruct_LiveLinkSequencer_StaticRegisterNativesFLiveLinkSubjectProperty()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkSubjectProperty>(FName(TEXT("LiveLinkSubjectProperty")));
	}
} ScriptStruct_LiveLinkSequencer_StaticRegisterNativesFLiveLinkSubjectProperty;
	struct Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SubjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkSubjectProperty>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_SubjectName_MetaData[] = {
		{ "Category", "Subjects" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_SubjectName = { "SubjectName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkSubjectProperty, SubjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_SubjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_SubjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "Subjects" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FLiveLinkSubjectProperty*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkSubjectProperty), &Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_bEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_SubjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::NewProp_bEnabled,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkSequencer,
		nullptr,
		&NewStructOps,
		"LiveLinkSubjectProperty",
		sizeof(FLiveLinkSubjectProperty),
		alignof(FLiveLinkSubjectProperty),
		Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkSubjectProperty()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkSequencer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkSubjectProperty"), sizeof(FLiveLinkSubjectProperty), Get_Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkSubjectProperty_Hash() { return 1746407325U; }
	void ULiveLinkSubjectProperties::StaticRegisterNativesULiveLinkSubjectProperties()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkSubjectProperties_NoRegister()
	{
		return ULiveLinkSubjectProperties::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkSubjectProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Properties_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Properties_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Properties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkSequencer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::NewProp_Properties_Inner = { "Properties", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLiveLinkSubjectProperty, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::NewProp_Properties_MetaData[] = {
		{ "Category", "Subjects" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::NewProp_Properties = { "Properties", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkSubjectProperties, Properties), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::NewProp_Properties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::NewProp_Properties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::NewProp_Properties_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::NewProp_Properties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkSubjectProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::ClassParams = {
		&ULiveLinkSubjectProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkSubjectProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkSubjectProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkSubjectProperties, 2528445817);
	template<> LIVELINKSEQUENCER_API UClass* StaticClass<ULiveLinkSubjectProperties>()
	{
		return ULiveLinkSubjectProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkSubjectProperties(Z_Construct_UClass_ULiveLinkSubjectProperties, &ULiveLinkSubjectProperties::StaticClass, TEXT("/Script/LiveLinkSequencer"), TEXT("ULiveLinkSubjectProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkSubjectProperties);
	void UTakeRecorderLiveLinkSource::StaticRegisterNativesUTakeRecorderLiveLinkSource()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderLiveLinkSource_NoRegister()
	{
		return UTakeRecorderLiveLinkSource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReduceKeys_MetaData[];
#endif
		static void NewProp_bReduceKeys_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReduceKeys;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SubjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSaveSubjectSettings_MetaData[];
#endif
		static void NewProp_bSaveSubjectSettings_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSaveSubjectSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseSourceTimecode_MetaData[];
#endif
		static void NewProp_bUseSourceTimecode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseSourceTimecode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDiscardSamplesBeforeStart_MetaData[];
#endif
		static void NewProp_bDiscardSamplesBeforeStart_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDiscardSamplesBeforeStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackRecorder_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TrackRecorder;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderSource,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkSequencer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::Class_MetaDataParams[] = {
		{ "Category", "Live Link" },
		{ "Comment", "/** A recording source that records LiveLink */" },
		{ "IncludePath", "TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "TakeRecorderDisplayName", "Live Link" },
		{ "ToolTip", "A recording source that records LiveLink" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bReduceKeys_MetaData[] = {
		{ "Category", "Live Link Source" },
		{ "Comment", "/** Whether to perform key-reduction algorithms as part of the recording */" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
		{ "ToolTip", "Whether to perform key-reduction algorithms as part of the recording" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bReduceKeys_SetBit(void* Obj)
	{
		((UTakeRecorderLiveLinkSource*)Obj)->bReduceKeys = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bReduceKeys = { "bReduceKeys", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderLiveLinkSource), &Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bReduceKeys_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bReduceKeys_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bReduceKeys_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_SubjectName_MetaData[] = {
		{ "Category", "Subjects" },
		{ "Comment", "/** Name of the subject to record */" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
		{ "ToolTip", "Name of the subject to record" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_SubjectName = { "SubjectName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderLiveLinkSource, SubjectName), METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_SubjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_SubjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bSaveSubjectSettings_MetaData[] = {
		{ "Category", "Subjects" },
		{ "Comment", "/** Whether we should save subject settings in the the live link section. If not, we'll create one with subject information with no settings */" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
		{ "ToolTip", "Whether we should save subject settings in the the live link section. If not, we'll create one with subject information with no settings" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bSaveSubjectSettings_SetBit(void* Obj)
	{
		((UTakeRecorderLiveLinkSource*)Obj)->bSaveSubjectSettings = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bSaveSubjectSettings = { "bSaveSubjectSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderLiveLinkSource), &Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bSaveSubjectSettings_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bSaveSubjectSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bSaveSubjectSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bUseSourceTimecode_MetaData[] = {
		{ "Category", "Subjects" },
		{ "Comment", "/** If true we use timecode even if not synchronized, else we use world time*/" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
		{ "ToolTip", "If true we use timecode even if not synchronized, else we use world time" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bUseSourceTimecode_SetBit(void* Obj)
	{
		((UTakeRecorderLiveLinkSource*)Obj)->bUseSourceTimecode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bUseSourceTimecode = { "bUseSourceTimecode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderLiveLinkSource), &Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bUseSourceTimecode_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bUseSourceTimecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bUseSourceTimecode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bDiscardSamplesBeforeStart_MetaData[] = {
		{ "Category", "Subjects" },
		{ "Comment", "/** If true discard livelink samples with timecode that occurs before the start of recording*/" },
		{ "EditCondition", "bUseSourceTimecode" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
		{ "ToolTip", "If true discard livelink samples with timecode that occurs before the start of recording" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bDiscardSamplesBeforeStart_SetBit(void* Obj)
	{
		((UTakeRecorderLiveLinkSource*)Obj)->bDiscardSamplesBeforeStart = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bDiscardSamplesBeforeStart = { "bDiscardSamplesBeforeStart", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderLiveLinkSource), &Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bDiscardSamplesBeforeStart_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bDiscardSamplesBeforeStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bDiscardSamplesBeforeStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_TrackRecorder_MetaData[] = {
		{ "Comment", "/**\n\x09* The master track recorder we created.\n\x09*/" },
		{ "ModuleRelativePath", "Private/TakeRecorderSource/TakeRecorderLiveLinkSource.h" },
		{ "ToolTip", "The master track recorder we created." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_TrackRecorder = { "TrackRecorder", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderLiveLinkSource, TrackRecorder), Z_Construct_UClass_UMovieSceneLiveLinkTrackRecorder_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_TrackRecorder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_TrackRecorder_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bReduceKeys,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_SubjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bSaveSubjectSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bUseSourceTimecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_bDiscardSamplesBeforeStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::NewProp_TrackRecorder,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderLiveLinkSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::ClassParams = {
		&UTakeRecorderLiveLinkSource::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderLiveLinkSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderLiveLinkSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderLiveLinkSource, 3162056563);
	template<> LIVELINKSEQUENCER_API UClass* StaticClass<UTakeRecorderLiveLinkSource>()
	{
		return UTakeRecorderLiveLinkSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderLiveLinkSource(Z_Construct_UClass_UTakeRecorderLiveLinkSource, &UTakeRecorderLiveLinkSource::StaticClass, TEXT("/Script/LiveLinkSequencer"), TEXT("UTakeRecorderLiveLinkSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderLiveLinkSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
