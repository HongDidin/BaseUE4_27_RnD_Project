// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKGRAPHNODE_AnimGraphNode_LiveLinkPose_generated_h
#error "AnimGraphNode_LiveLinkPose.generated.h already included, missing '#pragma once' in AnimGraphNode_LiveLinkPose.h"
#endif
#define LIVELINKGRAPHNODE_AnimGraphNode_LiveLinkPose_generated_h

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_RPC_WRAPPERS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnimGraphNode_LiveLinkPose(); \
	friend struct Z_Construct_UClass_UAnimGraphNode_LiveLinkPose_Statics; \
public: \
	DECLARE_CLASS(UAnimGraphNode_LiveLinkPose, UAnimGraphNode_Base, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkGraphNode"), NO_API) \
	DECLARE_SERIALIZER(UAnimGraphNode_LiveLinkPose)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUAnimGraphNode_LiveLinkPose(); \
	friend struct Z_Construct_UClass_UAnimGraphNode_LiveLinkPose_Statics; \
public: \
	DECLARE_CLASS(UAnimGraphNode_LiveLinkPose, UAnimGraphNode_Base, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkGraphNode"), NO_API) \
	DECLARE_SERIALIZER(UAnimGraphNode_LiveLinkPose)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnimGraphNode_LiveLinkPose(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimGraphNode_LiveLinkPose) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnimGraphNode_LiveLinkPose); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimGraphNode_LiveLinkPose); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnimGraphNode_LiveLinkPose(UAnimGraphNode_LiveLinkPose&&); \
	NO_API UAnimGraphNode_LiveLinkPose(const UAnimGraphNode_LiveLinkPose&); \
public:


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnimGraphNode_LiveLinkPose(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnimGraphNode_LiveLinkPose(UAnimGraphNode_LiveLinkPose&&); \
	NO_API UAnimGraphNode_LiveLinkPose(const UAnimGraphNode_LiveLinkPose&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnimGraphNode_LiveLinkPose); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimGraphNode_LiveLinkPose); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimGraphNode_LiveLinkPose)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_12_PROLOG
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_INCLASS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKGRAPHNODE_API UClass* StaticClass<class UAnimGraphNode_LiveLinkPose>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Private_AnimGraphNode_LiveLinkPose_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
