// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKEDITOR_LiveLinkPreviewController_generated_h
#error "LiveLinkPreviewController.generated.h already included, missing '#pragma once' in LiveLinkPreviewController.h"
#endif
#define LIVELINKEDITOR_LiveLinkPreviewController_generated_h

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_RPC_WRAPPERS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(ULiveLinkPreviewController, NO_API)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkPreviewController(); \
	friend struct Z_Construct_UClass_ULiveLinkPreviewController_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkPreviewController, UPersonaPreviewSceneController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkEditor"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkPreviewController) \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_ARCHIVESERIALIZER


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkPreviewController(); \
	friend struct Z_Construct_UClass_ULiveLinkPreviewController_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkPreviewController, UPersonaPreviewSceneController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkEditor"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkPreviewController) \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_ARCHIVESERIALIZER


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkPreviewController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkPreviewController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkPreviewController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkPreviewController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkPreviewController(ULiveLinkPreviewController&&); \
	NO_API ULiveLinkPreviewController(const ULiveLinkPreviewController&); \
public:


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkPreviewController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkPreviewController(ULiveLinkPreviewController&&); \
	NO_API ULiveLinkPreviewController(const ULiveLinkPreviewController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkPreviewController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkPreviewController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkPreviewController)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_14_PROLOG
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_INCLASS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h_18_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKEDITOR_API UClass* StaticClass<class ULiveLinkPreviewController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLink_Source_LiveLinkEditor_Private_LiveLinkPreviewController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
