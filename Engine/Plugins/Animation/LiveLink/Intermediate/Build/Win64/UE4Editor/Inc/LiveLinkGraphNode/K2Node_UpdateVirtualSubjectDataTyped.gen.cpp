// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkGraphNode/Private/K2Node_UpdateVirtualSubjectDataTyped.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_UpdateVirtualSubjectDataTyped() {}
// Cross Module References
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData_NoRegister();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase();
	UPackage* Z_Construct_UPackage__Script_LiveLinkGraphNode();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData_NoRegister();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData();
// End Cross Module References
	void UK2Node_UpdateVirtualSubjectStaticData::StaticRegisterNativesUK2Node_UpdateVirtualSubjectStaticData()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData_NoRegister()
	{
		return UK2Node_UpdateVirtualSubjectStaticData::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkGraphNode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_UpdateVirtualSubjectDataTyped.h" },
		{ "ModuleRelativePath", "Private/K2Node_UpdateVirtualSubjectDataTyped.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_UpdateVirtualSubjectStaticData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData_Statics::ClassParams = {
		&UK2Node_UpdateVirtualSubjectStaticData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_UpdateVirtualSubjectStaticData, 597463029);
	template<> LIVELINKGRAPHNODE_API UClass* StaticClass<UK2Node_UpdateVirtualSubjectStaticData>()
	{
		return UK2Node_UpdateVirtualSubjectStaticData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_UpdateVirtualSubjectStaticData(Z_Construct_UClass_UK2Node_UpdateVirtualSubjectStaticData, &UK2Node_UpdateVirtualSubjectStaticData::StaticClass, TEXT("/Script/LiveLinkGraphNode"), TEXT("UK2Node_UpdateVirtualSubjectStaticData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_UpdateVirtualSubjectStaticData);
	void UK2Node_UpdateVirtualSubjectFrameData::StaticRegisterNativesUK2Node_UpdateVirtualSubjectFrameData()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData_NoRegister()
	{
		return UK2Node_UpdateVirtualSubjectFrameData::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkGraphNode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_UpdateVirtualSubjectDataTyped.h" },
		{ "ModuleRelativePath", "Private/K2Node_UpdateVirtualSubjectDataTyped.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_UpdateVirtualSubjectFrameData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData_Statics::ClassParams = {
		&UK2Node_UpdateVirtualSubjectFrameData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_UpdateVirtualSubjectFrameData, 2152632783);
	template<> LIVELINKGRAPHNODE_API UClass* StaticClass<UK2Node_UpdateVirtualSubjectFrameData>()
	{
		return UK2Node_UpdateVirtualSubjectFrameData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_UpdateVirtualSubjectFrameData(Z_Construct_UClass_UK2Node_UpdateVirtualSubjectFrameData, &UK2Node_UpdateVirtualSubjectFrameData::StaticClass, TEXT("/Script/LiveLinkGraphNode"), TEXT("UK2Node_UpdateVirtualSubjectFrameData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_UpdateVirtualSubjectFrameData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
