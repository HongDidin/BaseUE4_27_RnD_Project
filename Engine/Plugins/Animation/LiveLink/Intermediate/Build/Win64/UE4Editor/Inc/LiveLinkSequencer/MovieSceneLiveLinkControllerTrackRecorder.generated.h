// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKSEQUENCER_MovieSceneLiveLinkControllerTrackRecorder_generated_h
#error "MovieSceneLiveLinkControllerTrackRecorder.generated.h already included, missing '#pragma once' in MovieSceneLiveLinkControllerTrackRecorder.h"
#endif
#define LIVELINKSEQUENCER_MovieSceneLiveLinkControllerTrackRecorder_generated_h

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_RPC_WRAPPERS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneLiveLinkControllerTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneLiveLinkControllerTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient), CASTCLASS_None, TEXT("/Script/LiveLinkSequencer"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneLiveLinkControllerTrackRecorder)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneLiveLinkControllerTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneLiveLinkControllerTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient), CASTCLASS_None, TEXT("/Script/LiveLinkSequencer"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneLiveLinkControllerTrackRecorder)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneLiveLinkControllerTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneLiveLinkControllerTrackRecorder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneLiveLinkControllerTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneLiveLinkControllerTrackRecorder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneLiveLinkControllerTrackRecorder(UMovieSceneLiveLinkControllerTrackRecorder&&); \
	NO_API UMovieSceneLiveLinkControllerTrackRecorder(const UMovieSceneLiveLinkControllerTrackRecorder&); \
public:


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneLiveLinkControllerTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneLiveLinkControllerTrackRecorder(UMovieSceneLiveLinkControllerTrackRecorder&&); \
	NO_API UMovieSceneLiveLinkControllerTrackRecorder(const UMovieSceneLiveLinkControllerTrackRecorder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneLiveLinkControllerTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneLiveLinkControllerTrackRecorder); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneLiveLinkControllerTrackRecorder)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LiveLinkControllerToRecord() { return STRUCT_OFFSET(UMovieSceneLiveLinkControllerTrackRecorder, LiveLinkControllerToRecord); }


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_12_PROLOG
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_INCLASS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKSEQUENCER_API UClass* StaticClass<class UMovieSceneLiveLinkControllerTrackRecorder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerTrackRecorder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
