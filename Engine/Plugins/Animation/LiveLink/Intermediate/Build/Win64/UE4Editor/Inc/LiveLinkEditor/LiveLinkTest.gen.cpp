// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkEditor/Private/LiveLinkTest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkTest() {}
// Cross Module References
	LIVELINKEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal();
	UPackage* Z_Construct_UPackage__Script_LiveLinkEditor();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkBaseFrameData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	LIVELINKEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal();
// End Cross Module References

static_assert(std::is_polymorphic<FLiveLinkTestFrameDataInternal>() == std::is_polymorphic<FLiveLinkBaseFrameData>(), "USTRUCT FLiveLinkTestFrameDataInternal cannot be polymorphic unless super FLiveLinkBaseFrameData is polymorphic");

class UScriptStruct* FLiveLinkTestFrameDataInternal::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal, Z_Construct_UPackage__Script_LiveLinkEditor(), TEXT("LiveLinkTestFrameDataInternal"), sizeof(FLiveLinkTestFrameDataInternal), Get_Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Hash());
	}
	return Singleton;
}
template<> LIVELINKEDITOR_API UScriptStruct* StaticStruct<FLiveLinkTestFrameDataInternal>()
{
	return FLiveLinkTestFrameDataInternal::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkTestFrameDataInternal(FLiveLinkTestFrameDataInternal::StaticStruct, TEXT("/Script/LiveLinkEditor"), TEXT("LiveLinkTestFrameDataInternal"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkEditor_StaticRegisterNativesFLiveLinkTestFrameDataInternal
{
	FScriptStruct_LiveLinkEditor_StaticRegisterNativesFLiveLinkTestFrameDataInternal()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkTestFrameDataInternal>(FName(TEXT("LiveLinkTestFrameDataInternal")));
	}
} ScriptStruct_LiveLinkEditor_StaticRegisterNativesFLiveLinkTestFrameDataInternal;
	struct Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NotInterpolated_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NotInterpolated;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SingleVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SingleVector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SingleStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SingleStruct;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SingleFloat_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SingleFloat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SingleInt_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SingleInt;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VectorArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VectorArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_VectorArray;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StructArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StructArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StructArray;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FloatArray;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IntArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkTestFrameDataInternal>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_NotInterpolated_MetaData[] = {
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_NotInterpolated = { "NotInterpolated", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkTestFrameDataInternal, NotInterpolated), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_NotInterpolated_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_NotInterpolated_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleVector_MetaData[] = {
		{ "Category", "Test" },
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleVector = { "SingleVector", nullptr, (EPropertyFlags)0x0010000200000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkTestFrameDataInternal, SingleVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleVector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleStruct_MetaData[] = {
		{ "Category", "Test" },
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleStruct = { "SingleStruct", nullptr, (EPropertyFlags)0x0010000200000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkTestFrameDataInternal, SingleStruct), Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleStruct_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleFloat_MetaData[] = {
		{ "Category", "Test" },
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleFloat = { "SingleFloat", nullptr, (EPropertyFlags)0x0010000200000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkTestFrameDataInternal, SingleFloat), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleFloat_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleFloat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleInt_MetaData[] = {
		{ "Category", "Test" },
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleInt = { "SingleInt", nullptr, (EPropertyFlags)0x0010000200000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkTestFrameDataInternal, SingleInt), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleInt_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleInt_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_VectorArray_Inner = { "VectorArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_VectorArray_MetaData[] = {
		{ "Category", "Test" },
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_VectorArray = { "VectorArray", nullptr, (EPropertyFlags)0x0010000200000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkTestFrameDataInternal, VectorArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_VectorArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_VectorArray_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_StructArray_Inner = { "StructArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_StructArray_MetaData[] = {
		{ "Category", "Test" },
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_StructArray = { "StructArray", nullptr, (EPropertyFlags)0x0010000200000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkTestFrameDataInternal, StructArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_StructArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_StructArray_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_FloatArray_Inner = { "FloatArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_FloatArray_MetaData[] = {
		{ "Category", "Test" },
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_FloatArray = { "FloatArray", nullptr, (EPropertyFlags)0x0010000200000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkTestFrameDataInternal, FloatArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_FloatArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_FloatArray_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_IntArray_Inner = { "IntArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_IntArray_MetaData[] = {
		{ "Category", "Test" },
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_IntArray = { "IntArray", nullptr, (EPropertyFlags)0x0010000200000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkTestFrameDataInternal, IntArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_IntArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_IntArray_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_NotInterpolated,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleStruct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleFloat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_SingleInt,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_VectorArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_VectorArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_StructArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_StructArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_FloatArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_FloatArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_IntArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::NewProp_IntArray,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkEditor,
		Z_Construct_UScriptStruct_FLiveLinkBaseFrameData,
		&NewStructOps,
		"LiveLinkTestFrameDataInternal",
		sizeof(FLiveLinkTestFrameDataInternal),
		alignof(FLiveLinkTestFrameDataInternal),
		Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkTestFrameDataInternal"), sizeof(FLiveLinkTestFrameDataInternal), Get_Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkTestFrameDataInternal_Hash() { return 3319683277U; }
class UScriptStruct* FLiveLinkInnerTestInternal::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal, Z_Construct_UPackage__Script_LiveLinkEditor(), TEXT("LiveLinkInnerTestInternal"), sizeof(FLiveLinkInnerTestInternal), Get_Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Hash());
	}
	return Singleton;
}
template<> LIVELINKEDITOR_API UScriptStruct* StaticStruct<FLiveLinkInnerTestInternal>()
{
	return FLiveLinkInnerTestInternal::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkInnerTestInternal(FLiveLinkInnerTestInternal::StaticStruct, TEXT("/Script/LiveLinkEditor"), TEXT("LiveLinkInnerTestInternal"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkEditor_StaticRegisterNativesFLiveLinkInnerTestInternal
{
	FScriptStruct_LiveLinkEditor_StaticRegisterNativesFLiveLinkInnerTestInternal()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkInnerTestInternal>(FName(TEXT("LiveLinkInnerTestInternal")));
	}
} ScriptStruct_LiveLinkEditor_StaticRegisterNativesFLiveLinkInnerTestInternal;
	struct Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerSingleFloat_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InnerSingleFloat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerSingleInt_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InnerSingleInt;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerVectorDim_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InnerVectorDim;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerFloatDim_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InnerFloatDim;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerIntDim_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InnerIntDim;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InnerIntArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerIntArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InnerIntArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkInnerTestInternal>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerSingleFloat_MetaData[] = {
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerSingleFloat = { "InnerSingleFloat", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkInnerTestInternal, InnerSingleFloat), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerSingleFloat_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerSingleFloat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerSingleInt_MetaData[] = {
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerSingleInt = { "InnerSingleInt", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkInnerTestInternal, InnerSingleInt), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerSingleInt_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerSingleInt_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerVectorDim_MetaData[] = {
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerVectorDim = { "InnerVectorDim", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(InnerVectorDim, FLiveLinkInnerTestInternal), STRUCT_OFFSET(FLiveLinkInnerTestInternal, InnerVectorDim), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerVectorDim_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerVectorDim_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerFloatDim_MetaData[] = {
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerFloatDim = { "InnerFloatDim", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(InnerFloatDim, FLiveLinkInnerTestInternal), STRUCT_OFFSET(FLiveLinkInnerTestInternal, InnerFloatDim), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerFloatDim_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerFloatDim_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntDim_MetaData[] = {
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntDim = { "InnerIntDim", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(InnerIntDim, FLiveLinkInnerTestInternal), STRUCT_OFFSET(FLiveLinkInnerTestInternal, InnerIntDim), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntDim_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntDim_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntArray_Inner = { "InnerIntArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntArray_MetaData[] = {
		{ "ModuleRelativePath", "Private/LiveLinkTest.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntArray = { "InnerIntArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkInnerTestInternal, InnerIntArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntArray_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerSingleFloat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerSingleInt,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerVectorDim,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerFloatDim,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntDim,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::NewProp_InnerIntArray,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkEditor,
		nullptr,
		&NewStructOps,
		"LiveLinkInnerTestInternal",
		sizeof(FLiveLinkInnerTestInternal),
		alignof(FLiveLinkInnerTestInternal),
		Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkInnerTestInternal"), sizeof(FLiveLinkInnerTestInternal), Get_Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkInnerTestInternal_Hash() { return 1868298186U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
