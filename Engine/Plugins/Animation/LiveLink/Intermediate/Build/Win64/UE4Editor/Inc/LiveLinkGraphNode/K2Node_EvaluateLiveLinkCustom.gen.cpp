// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkGraphNode/Private/K2Node_EvaluateLiveLinkCustom.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_EvaluateLiveLinkCustom() {}
// Cross Module References
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole_NoRegister();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame();
	UPackage* Z_Construct_UPackage__Script_LiveLinkGraphNode();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime_NoRegister();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime_NoRegister();
	LIVELINKGRAPHNODE_API UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime();
// End Cross Module References
	void UK2Node_EvaluateLiveLinkFrameWithSpecificRole::StaticRegisterNativesUK2Node_EvaluateLiveLinkFrameWithSpecificRole()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole_NoRegister()
	{
		return UK2Node_EvaluateLiveLinkFrameWithSpecificRole::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkGraphNode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_EvaluateLiveLinkCustom.h" },
		{ "ModuleRelativePath", "Private/K2Node_EvaluateLiveLinkCustom.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_EvaluateLiveLinkFrameWithSpecificRole>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole_Statics::ClassParams = {
		&UK2Node_EvaluateLiveLinkFrameWithSpecificRole::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_EvaluateLiveLinkFrameWithSpecificRole, 3234866235);
	template<> LIVELINKGRAPHNODE_API UClass* StaticClass<UK2Node_EvaluateLiveLinkFrameWithSpecificRole>()
	{
		return UK2Node_EvaluateLiveLinkFrameWithSpecificRole::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameWithSpecificRole, &UK2Node_EvaluateLiveLinkFrameWithSpecificRole::StaticClass, TEXT("/Script/LiveLinkGraphNode"), TEXT("UK2Node_EvaluateLiveLinkFrameWithSpecificRole"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_EvaluateLiveLinkFrameWithSpecificRole);
	void UK2Node_EvaluateLiveLinkFrameAtWorldTime::StaticRegisterNativesUK2Node_EvaluateLiveLinkFrameAtWorldTime()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime_NoRegister()
	{
		return UK2Node_EvaluateLiveLinkFrameAtWorldTime::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkGraphNode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_EvaluateLiveLinkCustom.h" },
		{ "ModuleRelativePath", "Private/K2Node_EvaluateLiveLinkCustom.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_EvaluateLiveLinkFrameAtWorldTime>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime_Statics::ClassParams = {
		&UK2Node_EvaluateLiveLinkFrameAtWorldTime::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_EvaluateLiveLinkFrameAtWorldTime, 1729699725);
	template<> LIVELINKGRAPHNODE_API UClass* StaticClass<UK2Node_EvaluateLiveLinkFrameAtWorldTime>()
	{
		return UK2Node_EvaluateLiveLinkFrameAtWorldTime::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtWorldTime, &UK2Node_EvaluateLiveLinkFrameAtWorldTime::StaticClass, TEXT("/Script/LiveLinkGraphNode"), TEXT("UK2Node_EvaluateLiveLinkFrameAtWorldTime"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_EvaluateLiveLinkFrameAtWorldTime);
	void UK2Node_EvaluateLiveLinkFrameAtSceneTime::StaticRegisterNativesUK2Node_EvaluateLiveLinkFrameAtSceneTime()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime_NoRegister()
	{
		return UK2Node_EvaluateLiveLinkFrameAtSceneTime::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrame,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkGraphNode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_EvaluateLiveLinkCustom.h" },
		{ "ModuleRelativePath", "Private/K2Node_EvaluateLiveLinkCustom.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_EvaluateLiveLinkFrameAtSceneTime>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime_Statics::ClassParams = {
		&UK2Node_EvaluateLiveLinkFrameAtSceneTime::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_EvaluateLiveLinkFrameAtSceneTime, 2424542851);
	template<> LIVELINKGRAPHNODE_API UClass* StaticClass<UK2Node_EvaluateLiveLinkFrameAtSceneTime>()
	{
		return UK2Node_EvaluateLiveLinkFrameAtSceneTime::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime(Z_Construct_UClass_UK2Node_EvaluateLiveLinkFrameAtSceneTime, &UK2Node_EvaluateLiveLinkFrameAtSceneTime::StaticClass, TEXT("/Script/LiveLinkGraphNode"), TEXT("UK2Node_EvaluateLiveLinkFrameAtSceneTime"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_EvaluateLiveLinkFrameAtSceneTime);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
