// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkSequencer/Public/LiveLinkSequencerSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkSequencerSettings() {}
// Cross Module References
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_ULiveLinkSequencerSettings_NoRegister();
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_ULiveLinkSequencerSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_LiveLinkSequencer();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_NoRegister();
	LIVELINKCOMPONENTS_API UClass* Z_Construct_UClass_ULiveLinkControllerBase_NoRegister();
// End Cross Module References
	void ULiveLinkSequencerSettings::StaticRegisterNativesULiveLinkSequencerSettings()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkSequencerSettings_NoRegister()
	{
		return ULiveLinkSequencerSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkSequencerSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DefaultTrackRecordersForController_ValueProp;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DefaultTrackRecordersForController_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultTrackRecordersForController_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_DefaultTrackRecordersForController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkSequencer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for LiveLink Sequencer\n */" },
		{ "IncludePath", "LiveLinkSequencerSettings.h" },
		{ "ModuleRelativePath", "Public/LiveLinkSequencerSettings.h" },
		{ "ToolTip", "Settings for LiveLink Sequencer" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::NewProp_DefaultTrackRecordersForController_ValueProp = { "DefaultTrackRecordersForController", nullptr, (EPropertyFlags)0x0004000000004001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::NewProp_DefaultTrackRecordersForController_Key_KeyProp = { "DefaultTrackRecordersForController_Key", nullptr, (EPropertyFlags)0x0004000000004001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ULiveLinkControllerBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::NewProp_DefaultTrackRecordersForController_MetaData[] = {
		{ "AllowAbstract", "false" },
		{ "Category", "LiveLink" },
		{ "Comment", "/** Default Track Recorder class to use for the specified LiveLink controller */" },
		{ "ModuleRelativePath", "Public/LiveLinkSequencerSettings.h" },
		{ "ToolTip", "Default Track Recorder class to use for the specified LiveLink controller" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::NewProp_DefaultTrackRecordersForController = { "DefaultTrackRecordersForController", nullptr, (EPropertyFlags)0x0014000000004001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkSequencerSettings, DefaultTrackRecordersForController), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::NewProp_DefaultTrackRecordersForController_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::NewProp_DefaultTrackRecordersForController_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::NewProp_DefaultTrackRecordersForController_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::NewProp_DefaultTrackRecordersForController_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::NewProp_DefaultTrackRecordersForController,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkSequencerSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::ClassParams = {
		&ULiveLinkSequencerSettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkSequencerSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkSequencerSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkSequencerSettings, 3853706689);
	template<> LIVELINKSEQUENCER_API UClass* StaticClass<ULiveLinkSequencerSettings>()
	{
		return ULiveLinkSequencerSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkSequencerSettings(Z_Construct_UClass_ULiveLinkSequencerSettings, &ULiveLinkSequencerSettings::StaticClass, TEXT("/Script/LiveLinkSequencer"), TEXT("ULiveLinkSequencerSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkSequencerSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
