// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKGRAPHNODE_K2Node_UpdateVirtualSubjectDataBase_generated_h
#error "K2Node_UpdateVirtualSubjectDataBase.generated.h already included, missing '#pragma once' in K2Node_UpdateVirtualSubjectDataBase.h"
#endif
#define LIVELINKGRAPHNODE_K2Node_UpdateVirtualSubjectDataBase_generated_h

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_RPC_WRAPPERS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_UpdateVirtualSubjectDataBase(); \
	friend struct Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_Statics; \
public: \
	DECLARE_CLASS(UK2Node_UpdateVirtualSubjectDataBase, UK2Node, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/LiveLinkGraphNode"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_UpdateVirtualSubjectDataBase)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_UpdateVirtualSubjectDataBase(); \
	friend struct Z_Construct_UClass_UK2Node_UpdateVirtualSubjectDataBase_Statics; \
public: \
	DECLARE_CLASS(UK2Node_UpdateVirtualSubjectDataBase, UK2Node, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/LiveLinkGraphNode"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_UpdateVirtualSubjectDataBase)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_UpdateVirtualSubjectDataBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_UpdateVirtualSubjectDataBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_UpdateVirtualSubjectDataBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_UpdateVirtualSubjectDataBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_UpdateVirtualSubjectDataBase(UK2Node_UpdateVirtualSubjectDataBase&&); \
	NO_API UK2Node_UpdateVirtualSubjectDataBase(const UK2Node_UpdateVirtualSubjectDataBase&); \
public:


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_UpdateVirtualSubjectDataBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_UpdateVirtualSubjectDataBase(UK2Node_UpdateVirtualSubjectDataBase&&); \
	NO_API UK2Node_UpdateVirtualSubjectDataBase(const UK2Node_UpdateVirtualSubjectDataBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_UpdateVirtualSubjectDataBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_UpdateVirtualSubjectDataBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_UpdateVirtualSubjectDataBase)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_18_PROLOG
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_INCLASS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKGRAPHNODE_API UClass* StaticClass<class UK2Node_UpdateVirtualSubjectDataBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLink_Source_LiveLinkGraphNode_Public_K2Node_UpdateVirtualSubjectDataBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
