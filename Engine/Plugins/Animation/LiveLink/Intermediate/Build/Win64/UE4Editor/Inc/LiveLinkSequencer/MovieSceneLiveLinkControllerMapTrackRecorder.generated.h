// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKSEQUENCER_MovieSceneLiveLinkControllerMapTrackRecorder_generated_h
#error "MovieSceneLiveLinkControllerMapTrackRecorder.generated.h already included, missing '#pragma once' in MovieSceneLiveLinkControllerMapTrackRecorder.h"
#endif
#define LIVELINKSEQUENCER_MovieSceneLiveLinkControllerMapTrackRecorder_generated_h

#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_SPARSE_DATA
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_RPC_WRAPPERS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneLiveLinkControllerMapTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneLiveLinkControllerMapTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneLiveLinkControllerMapTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/LiveLinkSequencer"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneLiveLinkControllerMapTrackRecorder)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneLiveLinkControllerMapTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneLiveLinkControllerMapTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneLiveLinkControllerMapTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/LiveLinkSequencer"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneLiveLinkControllerMapTrackRecorder)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneLiveLinkControllerMapTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneLiveLinkControllerMapTrackRecorder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneLiveLinkControllerMapTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneLiveLinkControllerMapTrackRecorder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneLiveLinkControllerMapTrackRecorder(UMovieSceneLiveLinkControllerMapTrackRecorder&&); \
	NO_API UMovieSceneLiveLinkControllerMapTrackRecorder(const UMovieSceneLiveLinkControllerMapTrackRecorder&); \
public:


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneLiveLinkControllerMapTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneLiveLinkControllerMapTrackRecorder(UMovieSceneLiveLinkControllerMapTrackRecorder&&); \
	NO_API UMovieSceneLiveLinkControllerMapTrackRecorder(const UMovieSceneLiveLinkControllerMapTrackRecorder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneLiveLinkControllerMapTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneLiveLinkControllerMapTrackRecorder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneLiveLinkControllerMapTrackRecorder)


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_30_PROLOG
#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_RPC_WRAPPERS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_INCLASS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_SPARSE_DATA \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKSEQUENCER_API UClass* StaticClass<class UMovieSceneLiveLinkControllerMapTrackRecorder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_LiveLink_Source_LiveLinkSequencer_Public_MovieSceneLiveLinkControllerMapTrackRecorder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
