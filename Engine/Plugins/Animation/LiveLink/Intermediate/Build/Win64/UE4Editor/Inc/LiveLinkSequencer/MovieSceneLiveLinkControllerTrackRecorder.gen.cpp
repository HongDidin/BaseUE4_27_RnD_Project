// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkSequencer/Public/MovieSceneLiveLinkControllerTrackRecorder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneLiveLinkControllerTrackRecorder() {}
// Cross Module References
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_NoRegister();
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder();
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneTrackRecorder();
	UPackage* Z_Construct_UPackage__Script_LiveLinkSequencer();
	LIVELINKCOMPONENTS_API UClass* Z_Construct_UClass_ULiveLinkControllerBase_NoRegister();
// End Cross Module References
	void UMovieSceneLiveLinkControllerTrackRecorder::StaticRegisterNativesUMovieSceneLiveLinkControllerTrackRecorder()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_NoRegister()
	{
		return UMovieSceneLiveLinkControllerTrackRecorder::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LiveLinkControllerToRecord_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LiveLinkControllerToRecord;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneTrackRecorder,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkSequencer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Abstract based for movie scene track recorders that can record LiveLink Controllers */" },
		{ "IncludePath", "MovieSceneLiveLinkControllerTrackRecorder.h" },
		{ "ModuleRelativePath", "Public/MovieSceneLiveLinkControllerTrackRecorder.h" },
		{ "ToolTip", "Abstract based for movie scene track recorders that can record LiveLink Controllers" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::NewProp_LiveLinkControllerToRecord_MetaData[] = {
		{ "Comment", "/** The LiveLink controller that this track record will record properties from */" },
		{ "ModuleRelativePath", "Public/MovieSceneLiveLinkControllerTrackRecorder.h" },
		{ "ToolTip", "The LiveLink controller that this track record will record properties from" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::NewProp_LiveLinkControllerToRecord = { "LiveLinkControllerToRecord", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneLiveLinkControllerTrackRecorder, LiveLinkControllerToRecord), Z_Construct_UClass_ULiveLinkControllerBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::NewProp_LiveLinkControllerToRecord_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::NewProp_LiveLinkControllerToRecord_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::NewProp_LiveLinkControllerToRecord,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneLiveLinkControllerTrackRecorder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::ClassParams = {
		&UMovieSceneLiveLinkControllerTrackRecorder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::PropPointers),
		0,
		0x001000A9u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneLiveLinkControllerTrackRecorder, 4179623548);
	template<> LIVELINKSEQUENCER_API UClass* StaticClass<UMovieSceneLiveLinkControllerTrackRecorder>()
	{
		return UMovieSceneLiveLinkControllerTrackRecorder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneLiveLinkControllerTrackRecorder(Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder, &UMovieSceneLiveLinkControllerTrackRecorder::StaticClass, TEXT("/Script/LiveLinkSequencer"), TEXT("UMovieSceneLiveLinkControllerTrackRecorder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneLiveLinkControllerTrackRecorder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
