// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkEditor/Private/LiveLinkPreviewController.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkPreviewController() {}
// Cross Module References
	LIVELINKEDITOR_API UClass* Z_Construct_UClass_ULiveLinkPreviewController_NoRegister();
	LIVELINKEDITOR_API UClass* Z_Construct_UClass_ULiveLinkPreviewController();
	PERSONA_API UClass* Z_Construct_UClass_UPersonaPreviewSceneController();
	UPackage* Z_Construct_UPackage__Script_LiveLinkEditor();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkSubjectName();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	LIVELINK_API UClass* Z_Construct_UClass_ULiveLinkRetargetAsset_NoRegister();
// End Cross Module References
	void ULiveLinkPreviewController::StaticRegisterNativesULiveLinkPreviewController()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkPreviewController_NoRegister()
	{
		return ULiveLinkPreviewController::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkPreviewController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SubjectName;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LiveLinkSubjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LiveLinkSubjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableCameraSync_MetaData[];
#endif
		static void NewProp_bEnableCameraSync_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableCameraSync;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RetargetAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_RetargetAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkPreviewController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPersonaPreviewSceneController,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPreviewController_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LiveLinkPreviewController.h" },
		{ "ModuleRelativePath", "Private/LiveLinkPreviewController.h" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_SubjectName_MetaData[] = {
		{ "ModuleRelativePath", "Private/LiveLinkPreviewController.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_SubjectName = { "SubjectName", nullptr, (EPropertyFlags)0x0010000820000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkPreviewController, SubjectName_DEPRECATED), METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_SubjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_SubjectName_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_LiveLinkSubjectName_MetaData[] = {
		{ "Category", "Live Link" },
		{ "ModuleRelativePath", "Private/LiveLinkPreviewController.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_LiveLinkSubjectName = { "LiveLinkSubjectName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkPreviewController, LiveLinkSubjectName), Z_Construct_UScriptStruct_FLiveLinkSubjectName, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_LiveLinkSubjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_LiveLinkSubjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_bEnableCameraSync_MetaData[] = {
		{ "Category", "Live Link" },
		{ "ModuleRelativePath", "Private/LiveLinkPreviewController.h" },
	};
#endif
	void Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_bEnableCameraSync_SetBit(void* Obj)
	{
		((ULiveLinkPreviewController*)Obj)->bEnableCameraSync = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_bEnableCameraSync = { "bEnableCameraSync", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULiveLinkPreviewController), &Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_bEnableCameraSync_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_bEnableCameraSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_bEnableCameraSync_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_RetargetAsset_MetaData[] = {
		{ "Category", "Live Link" },
		{ "ModuleRelativePath", "Private/LiveLinkPreviewController.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_RetargetAsset = { "RetargetAsset", nullptr, (EPropertyFlags)0x0014000002000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkPreviewController, RetargetAsset), Z_Construct_UClass_ULiveLinkRetargetAsset_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_RetargetAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_RetargetAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULiveLinkPreviewController_Statics::PropPointers[] = {
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_SubjectName,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_LiveLinkSubjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_bEnableCameraSync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkPreviewController_Statics::NewProp_RetargetAsset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkPreviewController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkPreviewController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkPreviewController_Statics::ClassParams = {
		&ULiveLinkPreviewController::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULiveLinkPreviewController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPreviewController_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPreviewController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPreviewController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkPreviewController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkPreviewController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkPreviewController, 71530885);
	template<> LIVELINKEDITOR_API UClass* StaticClass<ULiveLinkPreviewController>()
	{
		return ULiveLinkPreviewController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkPreviewController(Z_Construct_UClass_ULiveLinkPreviewController, &ULiveLinkPreviewController::StaticClass, TEXT("/Script/LiveLinkEditor"), TEXT("ULiveLinkPreviewController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkPreviewController);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(ULiveLinkPreviewController)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
