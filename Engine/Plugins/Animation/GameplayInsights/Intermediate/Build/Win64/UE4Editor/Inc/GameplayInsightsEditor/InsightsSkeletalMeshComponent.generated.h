// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEPLAYINSIGHTSEDITOR_InsightsSkeletalMeshComponent_generated_h
#error "InsightsSkeletalMeshComponent.generated.h already included, missing '#pragma once' in InsightsSkeletalMeshComponent.h"
#endif
#define GAMEPLAYINSIGHTSEDITOR_InsightsSkeletalMeshComponent_generated_h

#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_SPARSE_DATA
#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_RPC_WRAPPERS
#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInsightsSkeletalMeshComponent(); \
	friend struct Z_Construct_UClass_UInsightsSkeletalMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UInsightsSkeletalMeshComponent, USkeletalMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GameplayInsightsEditor"), NO_API) \
	DECLARE_SERIALIZER(UInsightsSkeletalMeshComponent)


#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUInsightsSkeletalMeshComponent(); \
	friend struct Z_Construct_UClass_UInsightsSkeletalMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UInsightsSkeletalMeshComponent, USkeletalMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GameplayInsightsEditor"), NO_API) \
	DECLARE_SERIALIZER(UInsightsSkeletalMeshComponent)


#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInsightsSkeletalMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInsightsSkeletalMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInsightsSkeletalMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInsightsSkeletalMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInsightsSkeletalMeshComponent(UInsightsSkeletalMeshComponent&&); \
	NO_API UInsightsSkeletalMeshComponent(const UInsightsSkeletalMeshComponent&); \
public:


#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInsightsSkeletalMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInsightsSkeletalMeshComponent(UInsightsSkeletalMeshComponent&&); \
	NO_API UInsightsSkeletalMeshComponent(const UInsightsSkeletalMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInsightsSkeletalMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInsightsSkeletalMeshComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInsightsSkeletalMeshComponent)


#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_13_PROLOG
#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_INCLASS \
	Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEPLAYINSIGHTSEDITOR_API UClass* StaticClass<class UInsightsSkeletalMeshComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Animation_GameplayInsights_Source_GameplayInsightsEditor_Public_InsightsSkeletalMeshComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
