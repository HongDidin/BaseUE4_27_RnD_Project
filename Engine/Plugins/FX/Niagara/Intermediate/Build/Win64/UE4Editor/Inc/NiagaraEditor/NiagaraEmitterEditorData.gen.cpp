// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraEmitterEditorData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraEmitterEditorData() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraEmitterEditorData_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraEmitterEditorData();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraEditorDataBase();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEditorData_NoRegister();
// End Cross Module References
	void UNiagaraEmitterEditorData::StaticRegisterNativesUNiagaraEmitterEditorData()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraEmitterEditorData_NoRegister()
	{
		return UNiagaraEmitterEditorData::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraEmitterEditorData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StackEditorData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StackEditorData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaybackRangeMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlaybackRangeMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaybackRangeMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlaybackRangeMax;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraEditorDataBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Editor only UI data for emitters. */" },
		{ "IncludePath", "NiagaraEmitterEditorData.h" },
		{ "ModuleRelativePath", "Private/NiagaraEmitterEditorData.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Editor only UI data for emitters." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_StackEditorData_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/NiagaraEmitterEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_StackEditorData = { "StackEditorData", nullptr, (EPropertyFlags)0x0042000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEmitterEditorData, StackEditorData), Z_Construct_UClass_UNiagaraStackEditorData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_StackEditorData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_StackEditorData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_PlaybackRangeMin_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraEmitterEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_PlaybackRangeMin = { "PlaybackRangeMin", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEmitterEditorData, PlaybackRangeMin), METADATA_PARAMS(Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_PlaybackRangeMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_PlaybackRangeMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_PlaybackRangeMax_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraEmitterEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_PlaybackRangeMax = { "PlaybackRangeMax", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEmitterEditorData, PlaybackRangeMax), METADATA_PARAMS(Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_PlaybackRangeMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_PlaybackRangeMax_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_StackEditorData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_PlaybackRangeMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::NewProp_PlaybackRangeMax,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraEmitterEditorData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::ClassParams = {
		&UNiagaraEmitterEditorData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraEmitterEditorData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraEmitterEditorData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraEmitterEditorData, 3794006746);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraEmitterEditorData>()
	{
		return UNiagaraEmitterEditorData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraEmitterEditorData(Z_Construct_UClass_UNiagaraEmitterEditorData, &UNiagaraEmitterEditorData::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraEmitterEditorData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraEmitterEditorData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
