// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackRoot_generated_h
#error "NiagaraStackRoot.generated.h already included, missing '#pragma once' in NiagaraStackRoot.h"
#endif
#define NIAGARAEDITOR_NiagaraStackRoot_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackRoot(); \
	friend struct Z_Construct_UClass_UNiagaraStackRoot_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackRoot, UNiagaraStackEntry, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackRoot)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackRoot(); \
	friend struct Z_Construct_UClass_UNiagaraStackRoot_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackRoot, UNiagaraStackEntry, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackRoot)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackRoot(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackRoot) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackRoot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackRoot); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackRoot(UNiagaraStackRoot&&); \
	NO_API UNiagaraStackRoot(const UNiagaraStackRoot&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackRoot(UNiagaraStackRoot&&); \
	NO_API UNiagaraStackRoot(const UNiagaraStackRoot&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackRoot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackRoot); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackRoot)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SystemSettingsGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, SystemSettingsGroup); } \
	FORCEINLINE static uint32 __PPO__SystemSpawnGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, SystemSpawnGroup); } \
	FORCEINLINE static uint32 __PPO__SystemUpdateGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, SystemUpdateGroup); } \
	FORCEINLINE static uint32 __PPO__EmitterSettingsGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, EmitterSettingsGroup); } \
	FORCEINLINE static uint32 __PPO__EmitterSpawnGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, EmitterSpawnGroup); } \
	FORCEINLINE static uint32 __PPO__EmitterUpdateGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, EmitterUpdateGroup); } \
	FORCEINLINE static uint32 __PPO__ParticleSpawnGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, ParticleSpawnGroup); } \
	FORCEINLINE static uint32 __PPO__ParticleUpdateGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, ParticleUpdateGroup); } \
	FORCEINLINE static uint32 __PPO__AddEventHandlerGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, AddEventHandlerGroup); } \
	FORCEINLINE static uint32 __PPO__AddSimulationStageGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, AddSimulationStageGroup); } \
	FORCEINLINE static uint32 __PPO__RenderGroup() { return STRUCT_OFFSET(UNiagaraStackRoot, RenderGroup); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_16_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackRoot>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRoot_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
