// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackFunctionInput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackFunctionInput() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackFunctionInput_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackFunctionInput();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackItemContent();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraStackFunctionInput::StaticRegisterNativesUNiagaraStackFunctionInput()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackFunctionInput_NoRegister()
	{
		return UNiagaraStackFunctionInput::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackFunctionInput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackFunctionInput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackItemContent,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackFunctionInput_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Represents a single module input in the module stack view model. */" },
		{ "IncludePath", "ViewModels/Stack/NiagaraStackFunctionInput.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackFunctionInput.h" },
		{ "ToolTip", "Represents a single module input in the module stack view model." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackFunctionInput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackFunctionInput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackFunctionInput_Statics::ClassParams = {
		&UNiagaraStackFunctionInput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackFunctionInput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackFunctionInput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackFunctionInput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackFunctionInput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackFunctionInput, 3653488836);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackFunctionInput>()
	{
		return UNiagaraStackFunctionInput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackFunctionInput(Z_Construct_UClass_UNiagaraStackFunctionInput, &UNiagaraStackFunctionInput::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackFunctionInput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackFunctionInput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
