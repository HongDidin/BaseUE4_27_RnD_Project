// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraParameterDefinitions_generated_h
#error "NiagaraParameterDefinitions.generated.h already included, missing '#pragma once' in NiagaraParameterDefinitions.h"
#endif
#define NIAGARAEDITOR_NiagaraParameterDefinitions_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_32_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FParameterDefinitionsBindingNameSubscription>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FScriptVarBindingNameSubscription>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraParameterDefinitions(); \
	friend struct Z_Construct_UClass_UNiagaraParameterDefinitions_Statics; \
public: \
	DECLARE_CLASS(UNiagaraParameterDefinitions, UNiagaraParameterDefinitionsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraParameterDefinitions)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraParameterDefinitions(); \
	friend struct Z_Construct_UClass_UNiagaraParameterDefinitions_Statics; \
public: \
	DECLARE_CLASS(UNiagaraParameterDefinitions, UNiagaraParameterDefinitionsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraParameterDefinitions)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraParameterDefinitions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraParameterDefinitions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraParameterDefinitions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraParameterDefinitions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraParameterDefinitions(UNiagaraParameterDefinitions&&); \
	NIAGARAEDITOR_API UNiagaraParameterDefinitions(const UNiagaraParameterDefinitions&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraParameterDefinitions(UNiagaraParameterDefinitions&&); \
	NIAGARAEDITOR_API UNiagaraParameterDefinitions(const UNiagaraParameterDefinitions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraParameterDefinitions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraParameterDefinitions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraParameterDefinitions)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bPromoteToTopInAddMenus() { return STRUCT_OFFSET(UNiagaraParameterDefinitions, bPromoteToTopInAddMenus); } \
	FORCEINLINE static uint32 __PPO__MenuSortOrder() { return STRUCT_OFFSET(UNiagaraParameterDefinitions, MenuSortOrder); } \
	FORCEINLINE static uint32 __PPO__ScriptVariables() { return STRUCT_OFFSET(UNiagaraParameterDefinitions, ScriptVariables); } \
	FORCEINLINE static uint32 __PPO__ExternalParameterDefinitionsSubscriptions() { return STRUCT_OFFSET(UNiagaraParameterDefinitions, ExternalParameterDefinitionsSubscriptions); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_48_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h_52_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraParameterDefinitions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterDefinitions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
