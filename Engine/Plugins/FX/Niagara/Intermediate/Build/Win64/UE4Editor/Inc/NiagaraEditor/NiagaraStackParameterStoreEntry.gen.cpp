// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackParameterStoreEntry.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackParameterStoreEntry() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackParameterStoreEntry_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackParameterStoreEntry();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackItemContent();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackObject_NoRegister();
// End Cross Module References
	void UNiagaraStackParameterStoreEntry::StaticRegisterNativesUNiagaraStackParameterStoreEntry()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackParameterStoreEntry_NoRegister()
	{
		return UNiagaraStackParameterStoreEntry::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValueObjectEntry_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ValueObjectEntry;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackItemContent,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Represents a single module input in the module stack view model. */" },
		{ "IncludePath", "ViewModels/Stack/NiagaraStackParameterStoreEntry.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackParameterStoreEntry.h" },
		{ "ToolTip", "Represents a single module input in the module stack view model." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::NewProp_ValueObjectEntry_MetaData[] = {
		{ "Comment", "/** The stack entry for displaying the value object. */" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackParameterStoreEntry.h" },
		{ "ToolTip", "The stack entry for displaying the value object." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::NewProp_ValueObjectEntry = { "ValueObjectEntry", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackParameterStoreEntry, ValueObjectEntry), Z_Construct_UClass_UNiagaraStackObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::NewProp_ValueObjectEntry_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::NewProp_ValueObjectEntry_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::NewProp_ValueObjectEntry,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackParameterStoreEntry>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::ClassParams = {
		&UNiagaraStackParameterStoreEntry::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackParameterStoreEntry()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackParameterStoreEntry, 451143248);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackParameterStoreEntry>()
	{
		return UNiagaraStackParameterStoreEntry::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackParameterStoreEntry(Z_Construct_UClass_UNiagaraStackParameterStoreEntry, &UNiagaraStackParameterStoreEntry::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackParameterStoreEntry"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackParameterStoreEntry);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
