// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_EdGraphSchema_NiagaraSystemOverview_generated_h
#error "EdGraphSchema_NiagaraSystemOverview.generated.h already included, missing '#pragma once' in EdGraphSchema_NiagaraSystemOverview.h"
#endif
#define NIAGARAEDITOR_EdGraphSchema_NiagaraSystemOverview_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEdGraphSchema_NiagaraSystemOverview(); \
	friend struct Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_Statics; \
public: \
	DECLARE_CLASS(UEdGraphSchema_NiagaraSystemOverview, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UEdGraphSchema_NiagaraSystemOverview)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUEdGraphSchema_NiagaraSystemOverview(); \
	friend struct Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_Statics; \
public: \
	DECLARE_CLASS(UEdGraphSchema_NiagaraSystemOverview, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UEdGraphSchema_NiagaraSystemOverview)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEdGraphSchema_NiagaraSystemOverview(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEdGraphSchema_NiagaraSystemOverview) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEdGraphSchema_NiagaraSystemOverview); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEdGraphSchema_NiagaraSystemOverview); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEdGraphSchema_NiagaraSystemOverview(UEdGraphSchema_NiagaraSystemOverview&&); \
	NO_API UEdGraphSchema_NiagaraSystemOverview(const UEdGraphSchema_NiagaraSystemOverview&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEdGraphSchema_NiagaraSystemOverview(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEdGraphSchema_NiagaraSystemOverview(UEdGraphSchema_NiagaraSystemOverview&&); \
	NO_API UEdGraphSchema_NiagaraSystemOverview(const UEdGraphSchema_NiagaraSystemOverview&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEdGraphSchema_NiagaraSystemOverview); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEdGraphSchema_NiagaraSystemOverview); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEdGraphSchema_NiagaraSystemOverview)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_12_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class EdGraphSchema_NiagaraSystemOverview."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UEdGraphSchema_NiagaraSystemOverview>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_NiagaraSystemOverview_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
