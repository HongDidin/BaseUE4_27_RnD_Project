// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackRendererItem_generated_h
#error "NiagaraStackRendererItem.generated.h already included, missing '#pragma once' in NiagaraStackRendererItem.h"
#endif
#define NIAGARAEDITOR_NiagaraStackRendererItem_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackRendererItem(); \
	friend struct Z_Construct_UClass_UNiagaraStackRendererItem_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackRendererItem, UNiagaraStackItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackRendererItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackRendererItem(); \
	friend struct Z_Construct_UClass_UNiagaraStackRendererItem_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackRendererItem, UNiagaraStackItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackRendererItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackRendererItem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackRendererItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackRendererItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackRendererItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackRendererItem(UNiagaraStackRendererItem&&); \
	NO_API UNiagaraStackRendererItem(const UNiagaraStackRendererItem&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackRendererItem(UNiagaraStackRendererItem&&); \
	NO_API UNiagaraStackRendererItem(const UNiagaraStackRendererItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackRendererItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackRendererItem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackRendererItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RendererObject() { return STRUCT_OFFSET(UNiagaraStackRendererItem, RendererObject); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_14_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackRendererItem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRendererItem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
