// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackRoot.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackRoot() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackRoot_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackRoot();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEntry();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackScriptItemGroup_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEventHandlerGroup_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackSimulationStagesGroup_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackRenderItemGroup_NoRegister();
// End Cross Module References
	void UNiagaraStackRoot::StaticRegisterNativesUNiagaraStackRoot()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackRoot_NoRegister()
	{
		return UNiagaraStackRoot::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackRoot_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SystemSettingsGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SystemSettingsGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SystemSpawnGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SystemSpawnGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SystemUpdateGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SystemUpdateGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmitterSettingsGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EmitterSettingsGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmitterSpawnGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EmitterSpawnGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmitterUpdateGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EmitterUpdateGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParticleSpawnGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParticleSpawnGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParticleUpdateGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParticleUpdateGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AddEventHandlerGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AddEventHandlerGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AddSimulationStageGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AddSimulationStageGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderGroup;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackRoot_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackEntry,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackRoot.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemSettingsGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemSettingsGroup = { "SystemSettingsGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, SystemSettingsGroup), Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemSettingsGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemSettingsGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemSpawnGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemSpawnGroup = { "SystemSpawnGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, SystemSpawnGroup), Z_Construct_UClass_UNiagaraStackScriptItemGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemSpawnGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemSpawnGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemUpdateGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemUpdateGroup = { "SystemUpdateGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, SystemUpdateGroup), Z_Construct_UClass_UNiagaraStackScriptItemGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemUpdateGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemUpdateGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterSettingsGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterSettingsGroup = { "EmitterSettingsGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, EmitterSettingsGroup), Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterSettingsGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterSettingsGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterSpawnGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterSpawnGroup = { "EmitterSpawnGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, EmitterSpawnGroup), Z_Construct_UClass_UNiagaraStackScriptItemGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterSpawnGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterSpawnGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterUpdateGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterUpdateGroup = { "EmitterUpdateGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, EmitterUpdateGroup), Z_Construct_UClass_UNiagaraStackScriptItemGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterUpdateGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterUpdateGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_ParticleSpawnGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_ParticleSpawnGroup = { "ParticleSpawnGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, ParticleSpawnGroup), Z_Construct_UClass_UNiagaraStackScriptItemGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_ParticleSpawnGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_ParticleSpawnGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_ParticleUpdateGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_ParticleUpdateGroup = { "ParticleUpdateGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, ParticleUpdateGroup), Z_Construct_UClass_UNiagaraStackScriptItemGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_ParticleUpdateGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_ParticleUpdateGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_AddEventHandlerGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_AddEventHandlerGroup = { "AddEventHandlerGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, AddEventHandlerGroup), Z_Construct_UClass_UNiagaraStackEventHandlerGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_AddEventHandlerGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_AddEventHandlerGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_AddSimulationStageGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_AddSimulationStageGroup = { "AddSimulationStageGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, AddSimulationStageGroup), Z_Construct_UClass_UNiagaraStackSimulationStagesGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_AddSimulationStageGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_AddSimulationStageGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_RenderGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackRoot.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_RenderGroup = { "RenderGroup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackRoot, RenderGroup), Z_Construct_UClass_UNiagaraStackRenderItemGroup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_RenderGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_RenderGroup_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraStackRoot_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemSettingsGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemSpawnGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_SystemUpdateGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterSettingsGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterSpawnGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_EmitterUpdateGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_ParticleSpawnGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_ParticleUpdateGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_AddEventHandlerGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_AddSimulationStageGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackRoot_Statics::NewProp_RenderGroup,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackRoot_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackRoot>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackRoot_Statics::ClassParams = {
		&UNiagaraStackRoot::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraStackRoot_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackRoot_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackRoot_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackRoot()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackRoot_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackRoot, 1336645742);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackRoot>()
	{
		return UNiagaraStackRoot::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackRoot(Z_Construct_UClass_UNiagaraStackRoot, &UNiagaraStackRoot::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackRoot"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackRoot);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
