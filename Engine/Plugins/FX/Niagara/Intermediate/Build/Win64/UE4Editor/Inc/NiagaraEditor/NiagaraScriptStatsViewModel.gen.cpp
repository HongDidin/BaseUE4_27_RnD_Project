// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/ViewModels/NiagaraScriptStatsViewModel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraScriptStatsViewModel() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScripStatsViewModelSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraScripStatsViewModelSettings::StaticRegisterNativesUNiagaraScripStatsViewModelSettings()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_NoRegister()
	{
		return UNiagaraScripStatsViewModelSettings::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_EnabledPlatforms_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnabledPlatforms_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnabledPlatforms;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "ViewModels/NiagaraScriptStatsViewModel.h" },
		{ "ModuleRelativePath", "Private/ViewModels/NiagaraScriptStatsViewModel.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::NewProp_EnabledPlatforms_Inner = { "EnabledPlatforms", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::NewProp_EnabledPlatforms_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Private/ViewModels/NiagaraScriptStatsViewModel.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::NewProp_EnabledPlatforms = { "EnabledPlatforms", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraScripStatsViewModelSettings, EnabledPlatforms), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::NewProp_EnabledPlatforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::NewProp_EnabledPlatforms_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::NewProp_EnabledPlatforms_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::NewProp_EnabledPlatforms,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraScripStatsViewModelSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::ClassParams = {
		&UNiagaraScripStatsViewModelSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraScripStatsViewModelSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraScripStatsViewModelSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraScripStatsViewModelSettings, 1107118449);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraScripStatsViewModelSettings>()
	{
		return UNiagaraScripStatsViewModelSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraScripStatsViewModelSettings(Z_Construct_UClass_UNiagaraScripStatsViewModelSettings, &UNiagaraScripStatsViewModelSettings::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraScripStatsViewModelSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraScripStatsViewModelSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
