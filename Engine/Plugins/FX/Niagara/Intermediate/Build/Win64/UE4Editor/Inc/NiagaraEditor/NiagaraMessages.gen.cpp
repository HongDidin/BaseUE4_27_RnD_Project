// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraMessages.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraMessages() {}
// Cross Module References
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraMessageSeverity();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraStackMessage();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraMessageData_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraMessageData();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraMessageDataBase();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraMessageDataText_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraMessageDataText();
// End Cross Module References
	static UEnum* ENiagaraMessageSeverity_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraMessageSeverity, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraMessageSeverity"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraMessageSeverity>()
	{
		return ENiagaraMessageSeverity_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraMessageSeverity(ENiagaraMessageSeverity_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraMessageSeverity"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraMessageSeverity_Hash() { return 1238346870U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraMessageSeverity()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraMessageSeverity"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraMessageSeverity_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraMessageSeverity::CriticalError", (int64)ENiagaraMessageSeverity::CriticalError },
				{ "ENiagaraMessageSeverity::Error", (int64)ENiagaraMessageSeverity::Error },
				{ "ENiagaraMessageSeverity::PerformanceWarning", (int64)ENiagaraMessageSeverity::PerformanceWarning },
				{ "ENiagaraMessageSeverity::Warning", (int64)ENiagaraMessageSeverity::Warning },
				{ "ENiagaraMessageSeverity::Info", (int64)ENiagaraMessageSeverity::Info },
				{ "ENiagaraMessageSeverity::CustomNote", (int64)ENiagaraMessageSeverity::CustomNote },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CriticalError.Name", "ENiagaraMessageSeverity::CriticalError" },
				{ "CustomNote.Name", "ENiagaraMessageSeverity::CustomNote" },
				{ "Error.Name", "ENiagaraMessageSeverity::Error" },
				{ "Info.Name", "ENiagaraMessageSeverity::Info" },
				{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
				{ "PerformanceWarning.Name", "ENiagaraMessageSeverity::PerformanceWarning" },
				{ "Warning.Name", "ENiagaraMessageSeverity::Warning" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraMessageSeverity",
				"ENiagaraMessageSeverity",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FNiagaraStackMessage::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraStackMessage_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraStackMessage, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraStackMessage"), sizeof(FNiagaraStackMessage), Get_Z_Construct_UScriptStruct_FNiagaraStackMessage_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraStackMessage>()
{
	return FNiagaraStackMessage::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraStackMessage(FNiagaraStackMessage::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraStackMessage"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraStackMessage
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraStackMessage()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraStackMessage>(FName(TEXT("NiagaraStackMessage")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraStackMessage;
	struct Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageText_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_MessageText;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShortDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ShortDescription;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MessageSeverity_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageSeverity_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MessageSeverity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowDismissal_MetaData[];
#endif
		static void NewProp_bAllowDismissal_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowDismissal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Guid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Guid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraStackMessage>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageText_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageText = { "MessageText", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraStackMessage, MessageText), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageText_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageText_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_ShortDescription_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_ShortDescription = { "ShortDescription", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraStackMessage, ShortDescription), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_ShortDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_ShortDescription_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageSeverity_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageSeverity_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageSeverity = { "MessageSeverity", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraStackMessage, MessageSeverity), Z_Construct_UEnum_NiagaraEditor_ENiagaraMessageSeverity, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageSeverity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageSeverity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_bAllowDismissal_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_bAllowDismissal_SetBit(void* Obj)
	{
		((FNiagaraStackMessage*)Obj)->bAllowDismissal = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_bAllowDismissal = { "bAllowDismissal", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNiagaraStackMessage), &Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_bAllowDismissal_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_bAllowDismissal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_bAllowDismissal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_Guid_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_Guid = { "Guid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraStackMessage, Guid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_Guid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_Guid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageText,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_ShortDescription,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageSeverity_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_MessageSeverity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_bAllowDismissal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::NewProp_Guid,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraStackMessage",
		sizeof(FNiagaraStackMessage),
		alignof(FNiagaraStackMessage),
		Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraStackMessage()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraStackMessage_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraStackMessage"), sizeof(FNiagaraStackMessage), Get_Z_Construct_UScriptStruct_FNiagaraStackMessage_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraStackMessage_Hash() { return 2794661212U; }
	void UNiagaraMessageData::StaticRegisterNativesUNiagaraMessageData()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraMessageData_NoRegister()
	{
		return UNiagaraMessageData::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraMessageData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraMessageData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraMessageDataBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraMessageData_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraMessages.h" },
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraMessageData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraMessageData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraMessageData_Statics::ClassParams = {
		&UNiagaraMessageData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraMessageData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraMessageData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraMessageData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraMessageData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraMessageData, 411358210);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraMessageData>()
	{
		return UNiagaraMessageData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraMessageData(Z_Construct_UClass_UNiagaraMessageData, &UNiagaraMessageData::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraMessageData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraMessageData);
	void UNiagaraMessageDataText::StaticRegisterNativesUNiagaraMessageDataText()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraMessageDataText_NoRegister()
	{
		return UNiagaraMessageDataText::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraMessageDataText_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageText_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_MessageText;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShortDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ShortDescription;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MessageSeverity_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageSeverity_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MessageSeverity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowDismissal_MetaData[];
#endif
		static void NewProp_bAllowDismissal_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowDismissal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TopicName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TopicName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraMessageDataText_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraMessageData,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraMessageDataText_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraMessages.h" },
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageText_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageText = { "MessageText", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraMessageDataText, MessageText), METADATA_PARAMS(Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageText_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageText_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_ShortDescription_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_ShortDescription = { "ShortDescription", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraMessageDataText, ShortDescription), METADATA_PARAMS(Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_ShortDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_ShortDescription_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageSeverity_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageSeverity_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageSeverity = { "MessageSeverity", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraMessageDataText, MessageSeverity), Z_Construct_UEnum_NiagaraEditor_ENiagaraMessageSeverity, METADATA_PARAMS(Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageSeverity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageSeverity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_bAllowDismissal_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	void Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_bAllowDismissal_SetBit(void* Obj)
	{
		((UNiagaraMessageDataText*)Obj)->bAllowDismissal = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_bAllowDismissal = { "bAllowDismissal", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraMessageDataText), &Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_bAllowDismissal_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_bAllowDismissal_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_bAllowDismissal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_TopicName_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_TopicName = { "TopicName", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraMessageDataText, TopicName), METADATA_PARAMS(Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_TopicName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_TopicName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraMessageDataText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageText,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_ShortDescription,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageSeverity_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_MessageSeverity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_bAllowDismissal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraMessageDataText_Statics::NewProp_TopicName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraMessageDataText_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraMessageDataText>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraMessageDataText_Statics::ClassParams = {
		&UNiagaraMessageDataText::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraMessageDataText_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraMessageDataText_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraMessageDataText_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraMessageDataText_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraMessageDataText()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraMessageDataText_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraMessageDataText, 710779959);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraMessageDataText>()
	{
		return UNiagaraMessageDataText::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraMessageDataText(Z_Construct_UClass_UNiagaraMessageDataText, &UNiagaraMessageDataText::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraMessageDataText"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraMessageDataText);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
