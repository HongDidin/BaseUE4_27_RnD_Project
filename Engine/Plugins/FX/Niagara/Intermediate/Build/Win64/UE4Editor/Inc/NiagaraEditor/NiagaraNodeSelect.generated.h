// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraNodeSelect_generated_h
#error "NiagaraNodeSelect.generated.h already included, missing '#pragma once' in NiagaraNodeSelect.h"
#endif
#define NIAGARAEDITOR_NiagaraNodeSelect_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraNodeSelect(); \
	friend struct Z_Construct_UClass_UNiagaraNodeSelect_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeSelect, UNiagaraNodeUsageSelector, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeSelect)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraNodeSelect(); \
	friend struct Z_Construct_UClass_UNiagaraNodeSelect_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeSelect, UNiagaraNodeUsageSelector, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeSelect)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeSelect(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeSelect) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeSelect); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeSelect); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeSelect(UNiagaraNodeSelect&&); \
	NIAGARAEDITOR_API UNiagaraNodeSelect(const UNiagaraNodeSelect&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeSelect(UNiagaraNodeSelect&&); \
	NIAGARAEDITOR_API UNiagaraNodeSelect(const UNiagaraNodeSelect&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeSelect); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeSelect); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraNodeSelect)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_14_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraNodeSelect>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeSelect_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
