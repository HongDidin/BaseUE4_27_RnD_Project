// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraEffectTypeFactoryNew_generated_h
#error "NiagaraEffectTypeFactoryNew.generated.h already included, missing '#pragma once' in NiagaraEffectTypeFactoryNew.h"
#endif
#define NIAGARAEDITOR_NiagaraEffectTypeFactoryNew_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraEffectTypeFactoryNew(); \
	friend struct Z_Construct_UClass_UNiagaraEffectTypeFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UNiagaraEffectTypeFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraEffectTypeFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraEffectTypeFactoryNew(); \
	friend struct Z_Construct_UClass_UNiagaraEffectTypeFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UNiagaraEffectTypeFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraEffectTypeFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraEffectTypeFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraEffectTypeFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraEffectTypeFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraEffectTypeFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraEffectTypeFactoryNew(UNiagaraEffectTypeFactoryNew&&); \
	NO_API UNiagaraEffectTypeFactoryNew(const UNiagaraEffectTypeFactoryNew&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraEffectTypeFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraEffectTypeFactoryNew(UNiagaraEffectTypeFactoryNew&&); \
	NO_API UNiagaraEffectTypeFactoryNew(const UNiagaraEffectTypeFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraEffectTypeFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraEffectTypeFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraEffectTypeFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_10_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraEffectTypeFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraEffectTypeFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEffectTypeFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
