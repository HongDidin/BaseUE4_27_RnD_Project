// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraScriptVariable_generated_h
#error "NiagaraScriptVariable.generated.h already included, missing '#pragma once' in NiagaraScriptVariable.h"
#endif
#define NIAGARAEDITOR_NiagaraScriptVariable_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraScriptVariable(); \
	friend struct Z_Construct_UClass_UNiagaraScriptVariable_Statics; \
public: \
	DECLARE_CLASS(UNiagaraScriptVariable, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraScriptVariable)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraScriptVariable(); \
	friend struct Z_Construct_UClass_UNiagaraScriptVariable_Statics; \
public: \
	DECLARE_CLASS(UNiagaraScriptVariable, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraScriptVariable)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraScriptVariable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraScriptVariable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraScriptVariable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraScriptVariable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraScriptVariable(UNiagaraScriptVariable&&); \
	NO_API UNiagaraScriptVariable(const UNiagaraScriptVariable&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraScriptVariable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraScriptVariable(UNiagaraScriptVariable&&); \
	NO_API UNiagaraScriptVariable(const UNiagaraScriptVariable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraScriptVariable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraScriptVariable); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraScriptVariable)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DefaultValueVariant() { return STRUCT_OFFSET(UNiagaraScriptVariable, DefaultValueVariant); } \
	FORCEINLINE static uint32 __PPO__StaticSwitchDefaultValue() { return STRUCT_OFFSET(UNiagaraScriptVariable, StaticSwitchDefaultValue); } \
	FORCEINLINE static uint32 __PPO__bIsStaticSwitch() { return STRUCT_OFFSET(UNiagaraScriptVariable, bIsStaticSwitch); } \
	FORCEINLINE static uint32 __PPO__bSubscribedToParameterDefinitions() { return STRUCT_OFFSET(UNiagaraScriptVariable, bSubscribedToParameterDefinitions); } \
	FORCEINLINE static uint32 __PPO__ChangeId() { return STRUCT_OFFSET(UNiagaraScriptVariable, ChangeId); } \
	FORCEINLINE static uint32 __PPO__bOverrideParameterDefinitionsDefaultValue() { return STRUCT_OFFSET(UNiagaraScriptVariable, bOverrideParameterDefinitionsDefaultValue); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_13_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraScriptVariable."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraScriptVariable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptVariable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
