// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraNodeCustomHlsl.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeCustomHlsl() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeCustomHlsl_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeCustomHlsl();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeFunctionCall();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARA_API UEnum* Z_Construct_UEnum_Niagara_ENiagaraScriptUsage();
// End Cross Module References
	void UNiagaraNodeCustomHlsl::StaticRegisterNativesUNiagaraNodeCustomHlsl()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeCustomHlsl_NoRegister()
	{
		return UNiagaraNodeCustomHlsl::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ScriptUsage_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScriptUsage_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ScriptUsage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomHlsl_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CustomHlsl;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNodeFunctionCall,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraNodeCustomHlsl.h" },
		{ "ModuleRelativePath", "Public/NiagaraNodeCustomHlsl.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_ScriptUsage_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_ScriptUsage_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraNodeCustomHlsl.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_ScriptUsage = { "ScriptUsage", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeCustomHlsl, ScriptUsage), Z_Construct_UEnum_Niagara_ENiagaraScriptUsage, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_ScriptUsage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_ScriptUsage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_CustomHlsl_MetaData[] = {
		{ "Category", "Function" },
		{ "ModuleRelativePath", "Public/NiagaraNodeCustomHlsl.h" },
		{ "MultiLine", "TRUE" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_CustomHlsl = { "CustomHlsl", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeCustomHlsl, CustomHlsl), METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_CustomHlsl_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_CustomHlsl_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_ScriptUsage_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_ScriptUsage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::NewProp_CustomHlsl,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeCustomHlsl>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::ClassParams = {
		&UNiagaraNodeCustomHlsl::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeCustomHlsl()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeCustomHlsl, 3571544273);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeCustomHlsl>()
	{
		return UNiagaraNodeCustomHlsl::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeCustomHlsl(Z_Construct_UClass_UNiagaraNodeCustomHlsl, &UNiagaraNodeCustomHlsl::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeCustomHlsl"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeCustomHlsl);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
