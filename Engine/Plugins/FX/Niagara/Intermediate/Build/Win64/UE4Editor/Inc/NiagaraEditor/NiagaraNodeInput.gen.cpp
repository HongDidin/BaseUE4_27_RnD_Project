// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraNodeInput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeInput() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraInputExposureOptions();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeInput_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeInput();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNode();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariable();
	NIAGARA_API UEnum* Z_Construct_UEnum_Niagara_ENiagaraInputNodeUsage();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraDataInterface_NoRegister();
// End Cross Module References
class UScriptStruct* FNiagaraInputExposureOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraInputExposureOptions"), sizeof(FNiagaraInputExposureOptions), Get_Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraInputExposureOptions>()
{
	return FNiagaraInputExposureOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraInputExposureOptions(FNiagaraInputExposureOptions::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraInputExposureOptions"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraInputExposureOptions
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraInputExposureOptions()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraInputExposureOptions>(FName(TEXT("NiagaraInputExposureOptions")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraInputExposureOptions;
	struct Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExposed_MetaData[];
#endif
		static void NewProp_bExposed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExposed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRequired_MetaData[];
#endif
		static void NewProp_bRequired_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRequired;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCanAutoBind_MetaData[];
#endif
		static void NewProp_bCanAutoBind_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCanAutoBind;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHidden_MetaData[];
#endif
		static void NewProp_bHidden_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHidden;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraInputExposureOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bExposed_MetaData[] = {
		{ "Category", "Exposure" },
		{ "Comment", "/** If this input is exposed to it's caller. */" },
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
		{ "ToolTip", "If this input is exposed to it's caller." },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bExposed_SetBit(void* Obj)
	{
		((FNiagaraInputExposureOptions*)Obj)->bExposed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bExposed = { "bExposed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FNiagaraInputExposureOptions), &Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bExposed_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bExposed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bExposed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bRequired_MetaData[] = {
		{ "Category", "Exposure" },
		{ "Comment", "/** If this input is required to be bound. */" },
		{ "editcondition", "bExposed" },
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
		{ "ToolTip", "If this input is required to be bound." },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bRequired_SetBit(void* Obj)
	{
		((FNiagaraInputExposureOptions*)Obj)->bRequired = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bRequired = { "bRequired", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FNiagaraInputExposureOptions), &Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bRequired_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bRequired_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bRequired_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bCanAutoBind_MetaData[] = {
		{ "Category", "Exposure" },
		{ "Comment", "/** If this input can auto-bind to system parameters and emitter attributes. Will never auto bind to custom parameters. */" },
		{ "editcondition", "bExposed" },
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
		{ "ToolTip", "If this input can auto-bind to system parameters and emitter attributes. Will never auto bind to custom parameters." },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bCanAutoBind_SetBit(void* Obj)
	{
		((FNiagaraInputExposureOptions*)Obj)->bCanAutoBind = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bCanAutoBind = { "bCanAutoBind", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FNiagaraInputExposureOptions), &Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bCanAutoBind_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bCanAutoBind_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bCanAutoBind_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bHidden_MetaData[] = {
		{ "Category", "Exposure" },
		{ "Comment", "/** If this input should be hidden in the advanced pin section of it's caller. */" },
		{ "editcondition", "bExposed" },
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
		{ "ToolTip", "If this input should be hidden in the advanced pin section of it's caller." },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bHidden_SetBit(void* Obj)
	{
		((FNiagaraInputExposureOptions*)Obj)->bHidden = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bHidden = { "bHidden", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FNiagaraInputExposureOptions), &Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bHidden_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bHidden_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bHidden_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bExposed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bRequired,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bCanAutoBind,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::NewProp_bHidden,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraInputExposureOptions",
		sizeof(FNiagaraInputExposureOptions),
		alignof(FNiagaraInputExposureOptions),
		Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraInputExposureOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraInputExposureOptions"), sizeof(FNiagaraInputExposureOptions), Get_Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Hash() { return 11543625U; }
	void UNiagaraNodeInput::StaticRegisterNativesUNiagaraNodeInput()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeInput_NoRegister()
	{
		return UNiagaraNodeInput::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeInput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Usage_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Usage_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Usage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CallSortPriority_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CallSortPriority;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExposureOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExposureOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataInterface_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DataInterface;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeInput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNode,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeInput_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraNodeInput.h" },
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Input_MetaData[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
		{ "SkipForCompileHash", "true" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeInput, Input), Z_Construct_UScriptStruct_FNiagaraVariable, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Input_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Usage_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Usage_MetaData[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Usage = { "Usage", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeInput, Usage), Z_Construct_UEnum_Niagara_ENiagaraInputNodeUsage, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Usage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Usage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_CallSortPriority_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Controls where this input is relative to others in the calling node. */" },
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
		{ "SkipForCompileHash", "true" },
		{ "ToolTip", "Controls where this input is relative to others in the calling node." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_CallSortPriority = { "CallSortPriority", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeInput, CallSortPriority), METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_CallSortPriority_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_CallSortPriority_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_ExposureOptions_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Controls this inputs exposure to callers. */" },
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
		{ "ToolTip", "Controls this inputs exposure to callers." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_ExposureOptions = { "ExposureOptions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeInput, ExposureOptions), Z_Construct_UScriptStruct_FNiagaraInputExposureOptions, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_ExposureOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_ExposureOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_DataInterface_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraNodeInput.h" },
		{ "SkipForCompileHash", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_DataInterface = { "DataInterface", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeInput, DataInterface), Z_Construct_UClass_UNiagaraDataInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_DataInterface_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_DataInterface_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNodeInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Input,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Usage_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_Usage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_CallSortPriority,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_ExposureOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeInput_Statics::NewProp_DataInterface,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeInput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeInput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeInput_Statics::ClassParams = {
		&UNiagaraNodeInput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNodeInput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeInput_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeInput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeInput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeInput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeInput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeInput, 3271312928);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeInput>()
	{
		return UNiagaraNodeInput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeInput(Z_Construct_UClass_UNiagaraNodeInput, &UNiagaraNodeInput::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeInput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeInput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
