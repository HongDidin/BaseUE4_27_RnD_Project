// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraEditorModule_generated_h
#error "NiagaraEditorModule.generated.h already included, missing '#pragma once' in NiagaraEditorModule.h"
#endif
#define NIAGARAEDITOR_NiagaraEditorModule_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_336_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FReservedParameterArray_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FReservedParameterArray>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_64_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FReservedParameter_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Parameter() { return STRUCT_OFFSET(FReservedParameter, Parameter); } \
	FORCEINLINE static uint32 __PPO__ReservingDefinitionsAsset() { return STRUCT_OFFSET(FReservedParameter, ReservingDefinitionsAsset); }


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FReservedParameter>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraReservedParametersManager(); \
	friend struct Z_Construct_UClass_UNiagaraReservedParametersManager_Statics; \
public: \
	DECLARE_CLASS(UNiagaraReservedParametersManager, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraReservedParametersManager)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraReservedParametersManager(); \
	friend struct Z_Construct_UClass_UNiagaraReservedParametersManager_Statics; \
public: \
	DECLARE_CLASS(UNiagaraReservedParametersManager, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraReservedParametersManager)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraReservedParametersManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraReservedParametersManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraReservedParametersManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraReservedParametersManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraReservedParametersManager(UNiagaraReservedParametersManager&&); \
	NO_API UNiagaraReservedParametersManager(const UNiagaraReservedParametersManager&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraReservedParametersManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraReservedParametersManager(UNiagaraReservedParametersManager&&); \
	NO_API UNiagaraReservedParametersManager(const UNiagaraReservedParametersManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraReservedParametersManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraReservedParametersManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraReservedParametersManager)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ReservedParameters() { return STRUCT_OFFSET(UNiagaraReservedParametersManager, ReservedParameters); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_346_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h_349_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraReservedParametersManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorModule_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
