// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraConvertInPlaceEmitterAndSystemState.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraConvertInPlaceEmitterAndSystemState() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraConvertInPlaceUtilityBase();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraConvertInPlaceEmitterAndSystemState::StaticRegisterNativesUNiagaraConvertInPlaceEmitterAndSystemState()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_NoRegister()
	{
		return UNiagaraConvertInPlaceEmitterAndSystemState::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraConvertInPlaceUtilityBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraConvertInPlaceEmitterAndSystemState.h" },
		{ "ModuleRelativePath", "Private/NiagaraConvertInPlaceEmitterAndSystemState.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraConvertInPlaceEmitterAndSystemState>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_Statics::ClassParams = {
		&UNiagaraConvertInPlaceEmitterAndSystemState::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraConvertInPlaceEmitterAndSystemState, 2976414113);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraConvertInPlaceEmitterAndSystemState>()
	{
		return UNiagaraConvertInPlaceEmitterAndSystemState::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraConvertInPlaceEmitterAndSystemState(Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState, &UNiagaraConvertInPlaceEmitterAndSystemState::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraConvertInPlaceEmitterAndSystemState"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraConvertInPlaceEmitterAndSystemState);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
