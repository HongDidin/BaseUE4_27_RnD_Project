// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackErrorItem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackErrorItem() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackErrorItem_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackErrorItem();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEntry();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackErrorItemLongDescription_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackErrorItemLongDescription();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackErrorItemFix_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackErrorItemFix();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackErrorItemDismiss_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackErrorItemDismiss();
// End Cross Module References
	void UNiagaraStackErrorItem::StaticRegisterNativesUNiagaraStackErrorItem()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackErrorItem_NoRegister()
	{
		return UNiagaraStackErrorItem::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackErrorItem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackErrorItem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackEntry,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackErrorItem_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackErrorItem.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackErrorItem.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackErrorItem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackErrorItem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackErrorItem_Statics::ClassParams = {
		&UNiagaraStackErrorItem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackErrorItem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackErrorItem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackErrorItem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackErrorItem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackErrorItem, 1453035465);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackErrorItem>()
	{
		return UNiagaraStackErrorItem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackErrorItem(Z_Construct_UClass_UNiagaraStackErrorItem, &UNiagaraStackErrorItem::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackErrorItem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackErrorItem);
	void UNiagaraStackErrorItemLongDescription::StaticRegisterNativesUNiagaraStackErrorItemLongDescription()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackErrorItemLongDescription_NoRegister()
	{
		return UNiagaraStackErrorItemLongDescription::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackErrorItemLongDescription_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackErrorItemLongDescription_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackEntry,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackErrorItemLongDescription_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackErrorItem.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackErrorItem.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackErrorItemLongDescription_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackErrorItemLongDescription>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackErrorItemLongDescription_Statics::ClassParams = {
		&UNiagaraStackErrorItemLongDescription::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackErrorItemLongDescription_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackErrorItemLongDescription_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackErrorItemLongDescription()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackErrorItemLongDescription_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackErrorItemLongDescription, 1909666960);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackErrorItemLongDescription>()
	{
		return UNiagaraStackErrorItemLongDescription::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackErrorItemLongDescription(Z_Construct_UClass_UNiagaraStackErrorItemLongDescription, &UNiagaraStackErrorItemLongDescription::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackErrorItemLongDescription"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackErrorItemLongDescription);
	void UNiagaraStackErrorItemFix::StaticRegisterNativesUNiagaraStackErrorItemFix()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackErrorItemFix_NoRegister()
	{
		return UNiagaraStackErrorItemFix::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackErrorItemFix_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackErrorItemFix_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackEntry,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackErrorItemFix_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackErrorItem.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackErrorItem.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackErrorItemFix_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackErrorItemFix>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackErrorItemFix_Statics::ClassParams = {
		&UNiagaraStackErrorItemFix::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackErrorItemFix_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackErrorItemFix_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackErrorItemFix()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackErrorItemFix_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackErrorItemFix, 2190530078);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackErrorItemFix>()
	{
		return UNiagaraStackErrorItemFix::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackErrorItemFix(Z_Construct_UClass_UNiagaraStackErrorItemFix, &UNiagaraStackErrorItemFix::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackErrorItemFix"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackErrorItemFix);
	void UNiagaraStackErrorItemDismiss::StaticRegisterNativesUNiagaraStackErrorItemDismiss()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackErrorItemDismiss_NoRegister()
	{
		return UNiagaraStackErrorItemDismiss::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackErrorItemDismiss_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackErrorItemDismiss_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackErrorItemFix,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackErrorItemDismiss_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackErrorItem.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackErrorItem.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackErrorItemDismiss_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackErrorItemDismiss>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackErrorItemDismiss_Statics::ClassParams = {
		&UNiagaraStackErrorItemDismiss::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackErrorItemDismiss_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackErrorItemDismiss_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackErrorItemDismiss()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackErrorItemDismiss_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackErrorItemDismiss, 2857081895);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackErrorItemDismiss>()
	{
		return UNiagaraStackErrorItemDismiss::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackErrorItemDismiss(Z_Construct_UClass_UNiagaraStackErrorItemDismiss, &UNiagaraStackErrorItemDismiss::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackErrorItemDismiss"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackErrorItemDismiss);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
