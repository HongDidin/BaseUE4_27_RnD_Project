// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraEmitterFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraEmitterFactoryNew() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraEmitterFactoryNew_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraEmitterFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraEmitterFactoryNew::StaticRegisterNativesUNiagaraEmitterFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraEmitterFactoryNew_NoRegister()
	{
		return UNiagaraEmitterFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraEmitterFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraEmitterFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEmitterFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A factory for niagara emitter assets. */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "NiagaraEmitterFactoryNew.h" },
		{ "ModuleRelativePath", "Public/NiagaraEmitterFactoryNew.h" },
		{ "ToolTip", "A factory for niagara emitter assets." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraEmitterFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraEmitterFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraEmitterFactoryNew_Statics::ClassParams = {
		&UNiagaraEmitterFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraEmitterFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEmitterFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraEmitterFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraEmitterFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraEmitterFactoryNew, 59601721);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraEmitterFactoryNew>()
	{
		return UNiagaraEmitterFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraEmitterFactoryNew(Z_Construct_UClass_UNiagaraEmitterFactoryNew, &UNiagaraEmitterFactoryNew::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraEmitterFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraEmitterFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
