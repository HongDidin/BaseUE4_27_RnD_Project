// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackInputCategory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackInputCategory() {}
// Cross Module References
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_EStackParameterBehavior();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackInputCategory_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackInputCategory();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackItemContent();
// End Cross Module References
	static UEnum* EStackParameterBehavior_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_EStackParameterBehavior, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("EStackParameterBehavior"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<EStackParameterBehavior>()
	{
		return EStackParameterBehavior_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EStackParameterBehavior(EStackParameterBehavior_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("EStackParameterBehavior"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_EStackParameterBehavior_Hash() { return 3995577114U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_EStackParameterBehavior()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EStackParameterBehavior"), 0, Get_Z_Construct_UEnum_NiagaraEditor_EStackParameterBehavior_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EStackParameterBehavior::Dynamic", (int64)EStackParameterBehavior::Dynamic },
				{ "EStackParameterBehavior::Static", (int64)EStackParameterBehavior::Static },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Dynamic.Name", "EStackParameterBehavior::Dynamic" },
				{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackInputCategory.h" },
				{ "Static.Name", "EStackParameterBehavior::Static" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"EStackParameterBehavior",
				"EStackParameterBehavior",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UNiagaraStackInputCategory::StaticRegisterNativesUNiagaraStackInputCategory()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackInputCategory_NoRegister()
	{
		return UNiagaraStackInputCategory::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackInputCategory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackInputCategory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackItemContent,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackInputCategory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackInputCategory.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackInputCategory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackInputCategory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackInputCategory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackInputCategory_Statics::ClassParams = {
		&UNiagaraStackInputCategory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackInputCategory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackInputCategory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackInputCategory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackInputCategory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackInputCategory, 988685121);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackInputCategory>()
	{
		return UNiagaraStackInputCategory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackInputCategory(Z_Construct_UClass_UNiagaraStackInputCategory, &UNiagaraStackInputCategory::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackInputCategory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackInputCategory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
