// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraParameterDefinitions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraParameterDefinitions() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraParameterDefinitions_NoRegister();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraParameterDefinitions();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraParameterDefinitionsBase();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScriptVariable_NoRegister();
// End Cross Module References
class UScriptStruct* FParameterDefinitionsBindingNameSubscription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ParameterDefinitionsBindingNameSubscription"), sizeof(FParameterDefinitionsBindingNameSubscription), Get_Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FParameterDefinitionsBindingNameSubscription>()
{
	return FParameterDefinitionsBindingNameSubscription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FParameterDefinitionsBindingNameSubscription(FParameterDefinitionsBindingNameSubscription::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("ParameterDefinitionsBindingNameSubscription"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFParameterDefinitionsBindingNameSubscription
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFParameterDefinitionsBindingNameSubscription()
	{
		UScriptStruct::DeferCppStructOps<FParameterDefinitionsBindingNameSubscription>(FName(TEXT("ParameterDefinitionsBindingNameSubscription")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFParameterDefinitionsBindingNameSubscription;
	struct Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubscribedParameterDefinitions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SubscribedParameterDefinitions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BindingNameSubscriptions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BindingNameSubscriptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BindingNameSubscriptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FParameterDefinitionsBindingNameSubscription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_SubscribedParameterDefinitions_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_SubscribedParameterDefinitions = { "SubscribedParameterDefinitions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FParameterDefinitionsBindingNameSubscription, SubscribedParameterDefinitions), Z_Construct_UClass_UNiagaraParameterDefinitions_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_SubscribedParameterDefinitions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_SubscribedParameterDefinitions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_BindingNameSubscriptions_Inner = { "BindingNameSubscriptions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_BindingNameSubscriptions_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_BindingNameSubscriptions = { "BindingNameSubscriptions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FParameterDefinitionsBindingNameSubscription, BindingNameSubscriptions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_BindingNameSubscriptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_BindingNameSubscriptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_SubscribedParameterDefinitions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_BindingNameSubscriptions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::NewProp_BindingNameSubscriptions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"ParameterDefinitionsBindingNameSubscription",
		sizeof(FParameterDefinitionsBindingNameSubscription),
		alignof(FParameterDefinitionsBindingNameSubscription),
		Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ParameterDefinitionsBindingNameSubscription"), sizeof(FParameterDefinitionsBindingNameSubscription), Get_Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription_Hash() { return 4294410359U; }
class UScriptStruct* FScriptVarBindingNameSubscription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ScriptVarBindingNameSubscription"), sizeof(FScriptVarBindingNameSubscription), Get_Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FScriptVarBindingNameSubscription>()
{
	return FScriptVarBindingNameSubscription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FScriptVarBindingNameSubscription(FScriptVarBindingNameSubscription::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("ScriptVarBindingNameSubscription"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFScriptVarBindingNameSubscription
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFScriptVarBindingNameSubscription()
	{
		UScriptStruct::DeferCppStructOps<FScriptVarBindingNameSubscription>(FName(TEXT("ScriptVarBindingNameSubscription")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFScriptVarBindingNameSubscription;
	struct Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExternalScriptVarId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExternalScriptVarId;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InternalScriptVarIds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InternalScriptVarIds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InternalScriptVarIds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Helper structs to map between external parameter libraries and a set of their script variables to local script variables. */" },
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
		{ "ToolTip", "Helper structs to map between external parameter libraries and a set of their script variables to local script variables." },
	};
#endif
	void* Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FScriptVarBindingNameSubscription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_ExternalScriptVarId_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_ExternalScriptVarId = { "ExternalScriptVarId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FScriptVarBindingNameSubscription, ExternalScriptVarId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_ExternalScriptVarId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_ExternalScriptVarId_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_InternalScriptVarIds_Inner = { "InternalScriptVarIds", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_InternalScriptVarIds_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_InternalScriptVarIds = { "InternalScriptVarIds", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FScriptVarBindingNameSubscription, InternalScriptVarIds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_InternalScriptVarIds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_InternalScriptVarIds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_ExternalScriptVarId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_InternalScriptVarIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::NewProp_InternalScriptVarIds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"ScriptVarBindingNameSubscription",
		sizeof(FScriptVarBindingNameSubscription),
		alignof(FScriptVarBindingNameSubscription),
		Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ScriptVarBindingNameSubscription"), sizeof(FScriptVarBindingNameSubscription), Get_Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FScriptVarBindingNameSubscription_Hash() { return 805679308U; }
	void UNiagaraParameterDefinitions::StaticRegisterNativesUNiagaraParameterDefinitions()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraParameterDefinitions_NoRegister()
	{
		return UNiagaraParameterDefinitions::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraParameterDefinitions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPromoteToTopInAddMenus_MetaData[];
#endif
		static void NewProp_bPromoteToTopInAddMenus_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPromoteToTopInAddMenus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MenuSortOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MenuSortOrder;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ScriptVariables_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScriptVariables_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ScriptVariables;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExternalParameterDefinitionsSubscriptions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExternalParameterDefinitionsSubscriptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExternalParameterDefinitionsSubscriptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraParameterDefinitionsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Collection of UNiagaraScriptVariables to synchronize between UNiagaraScripts. */" },
		{ "IncludePath", "NiagaraParameterDefinitions.h" },
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Collection of UNiagaraScriptVariables to synchronize between UNiagaraScripts." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_bPromoteToTopInAddMenus_MetaData[] = {
		{ "Category", "Definition Preferences" },
		{ "Comment", "// If true then these parameters will appear as top level entry in add menus (e.g. in the module editor)\n" },
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
		{ "ToolTip", "If true then these parameters will appear as top level entry in add menus (e.g. in the module editor)" },
	};
#endif
	void Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_bPromoteToTopInAddMenus_SetBit(void* Obj)
	{
		((UNiagaraParameterDefinitions*)Obj)->bPromoteToTopInAddMenus = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_bPromoteToTopInAddMenus = { "bPromoteToTopInAddMenus", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraParameterDefinitions), &Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_bPromoteToTopInAddMenus_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_bPromoteToTopInAddMenus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_bPromoteToTopInAddMenus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_MenuSortOrder_MetaData[] = {
		{ "Category", "Definition Preferences" },
		{ "Comment", "// Defines the sort order in add menus. Entries with smaller numbers are displayed first.\n" },
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
		{ "ToolTip", "Defines the sort order in add menus. Entries with smaller numbers are displayed first." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_MenuSortOrder = { "MenuSortOrder", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraParameterDefinitions, MenuSortOrder), METADATA_PARAMS(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_MenuSortOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_MenuSortOrder_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ScriptVariables_Inner = { "ScriptVariables", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraScriptVariable_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ScriptVariables_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ScriptVariables = { "ScriptVariables", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraParameterDefinitions, ScriptVariables), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ScriptVariables_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ScriptVariables_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ExternalParameterDefinitionsSubscriptions_Inner = { "ExternalParameterDefinitionsSubscriptions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FParameterDefinitionsBindingNameSubscription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ExternalParameterDefinitionsSubscriptions_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraParameterDefinitions.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ExternalParameterDefinitionsSubscriptions = { "ExternalParameterDefinitionsSubscriptions", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraParameterDefinitions, ExternalParameterDefinitionsSubscriptions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ExternalParameterDefinitionsSubscriptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ExternalParameterDefinitionsSubscriptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_bPromoteToTopInAddMenus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_MenuSortOrder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ScriptVariables_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ScriptVariables,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ExternalParameterDefinitionsSubscriptions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::NewProp_ExternalParameterDefinitionsSubscriptions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraParameterDefinitions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::ClassParams = {
		&UNiagaraParameterDefinitions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraParameterDefinitions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraParameterDefinitions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraParameterDefinitions, 2031891695);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraParameterDefinitions>()
	{
		return UNiagaraParameterDefinitions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraParameterDefinitions(Z_Construct_UClass_UNiagaraParameterDefinitions, &UNiagaraParameterDefinitions::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraParameterDefinitions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraParameterDefinitions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
