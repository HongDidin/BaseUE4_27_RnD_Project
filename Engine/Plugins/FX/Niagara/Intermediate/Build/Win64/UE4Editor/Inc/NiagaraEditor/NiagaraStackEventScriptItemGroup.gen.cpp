// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackEventScriptItemGroup.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackEventScriptItemGroup() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackItem();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackObject_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEventScriptItemGroup();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackScriptItemGroup();
// End Cross Module References
	void UNiagaraStackEventHandlerPropertiesItem::StaticRegisterNativesUNiagaraStackEventHandlerPropertiesItem()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_NoRegister()
	{
		return UNiagaraStackEventHandlerPropertiesItem::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmitterObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EmitterObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackItem,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackEventScriptItemGroup.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackEventScriptItemGroup.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::NewProp_EmitterObject_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackEventScriptItemGroup.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::NewProp_EmitterObject = { "EmitterObject", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackEventHandlerPropertiesItem, EmitterObject), Z_Construct_UClass_UNiagaraStackObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::NewProp_EmitterObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::NewProp_EmitterObject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::NewProp_EmitterObject,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackEventHandlerPropertiesItem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::ClassParams = {
		&UNiagaraStackEventHandlerPropertiesItem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackEventHandlerPropertiesItem, 3559583563);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackEventHandlerPropertiesItem>()
	{
		return UNiagaraStackEventHandlerPropertiesItem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackEventHandlerPropertiesItem(Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem, &UNiagaraStackEventHandlerPropertiesItem::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackEventHandlerPropertiesItem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackEventHandlerPropertiesItem);
	void UNiagaraStackEventScriptItemGroup::StaticRegisterNativesUNiagaraStackEventScriptItemGroup()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_NoRegister()
	{
		return UNiagaraStackEventScriptItemGroup::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventHandlerProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EventHandlerProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackScriptItemGroup,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackEventScriptItemGroup.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackEventScriptItemGroup.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::NewProp_EventHandlerProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackEventScriptItemGroup.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::NewProp_EventHandlerProperties = { "EventHandlerProperties", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackEventScriptItemGroup, EventHandlerProperties), Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::NewProp_EventHandlerProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::NewProp_EventHandlerProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::NewProp_EventHandlerProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackEventScriptItemGroup>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::ClassParams = {
		&UNiagaraStackEventScriptItemGroup::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackEventScriptItemGroup()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackEventScriptItemGroup, 688361564);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackEventScriptItemGroup>()
	{
		return UNiagaraStackEventScriptItemGroup::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackEventScriptItemGroup(Z_Construct_UClass_UNiagaraStackEventScriptItemGroup, &UNiagaraStackEventScriptItemGroup::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackEventScriptItemGroup"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackEventScriptItemGroup);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
