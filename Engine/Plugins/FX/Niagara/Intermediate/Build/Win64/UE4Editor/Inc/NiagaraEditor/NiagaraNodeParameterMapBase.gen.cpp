// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraNodeParameterMapBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeParameterMapBase() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeParameterMapBase_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeParameterMapBase();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeWithDynamicPins();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraNodeParameterMapBase::StaticRegisterNativesUNiagaraNodeParameterMapBase()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeParameterMapBase_NoRegister()
	{
		return UNiagaraNodeParameterMapBase::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeParameterMapBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeParameterMapBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNodeWithDynamicPins,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeParameterMapBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A node which allows the user to build a set of arbitrary output types from an arbitrary set of input types by connecting their inner components. */" },
		{ "IncludePath", "NiagaraNodeParameterMapBase.h" },
		{ "ModuleRelativePath", "Private/NiagaraNodeParameterMapBase.h" },
		{ "ToolTip", "A node which allows the user to build a set of arbitrary output types from an arbitrary set of input types by connecting their inner components." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeParameterMapBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeParameterMapBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeParameterMapBase_Statics::ClassParams = {
		&UNiagaraNodeParameterMapBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeParameterMapBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeParameterMapBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeParameterMapBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeParameterMapBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeParameterMapBase, 1215292408);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeParameterMapBase>()
	{
		return UNiagaraNodeParameterMapBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeParameterMapBase(Z_Construct_UClass_UNiagaraNodeParameterMapBase, &UNiagaraNodeParameterMapBase::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeParameterMapBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeParameterMapBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
