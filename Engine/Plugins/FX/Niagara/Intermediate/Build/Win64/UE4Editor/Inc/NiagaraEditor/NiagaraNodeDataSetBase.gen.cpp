// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraNodeDataSetBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeDataSetBase() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeDataSetBase_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeDataSetBase();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNode();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraDataSetID();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariable();
	COREUOBJECT_API UClass* Z_Construct_UClass_UStruct();
// End Cross Module References
	void UNiagaraNodeDataSetBase::StaticRegisterNativesUNiagaraNodeDataSetBase()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeDataSetBase_NoRegister()
	{
		return UNiagaraNodeDataSetBase::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DataSet;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Variables_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Variables_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Variables;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_VariableFriendlyNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VariableFriendlyNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_VariableFriendlyNames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExternalStructAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExternalStructAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNode,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraNodeDataSetBase.h" },
		{ "ModuleRelativePath", "Public/NiagaraNodeDataSetBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_DataSet_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraNodeDataSetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_DataSet = { "DataSet", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeDataSetBase, DataSet), Z_Construct_UScriptStruct_FNiagaraDataSetID, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_DataSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_DataSet_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_Variables_Inner = { "Variables", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraVariable, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_Variables_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraNodeDataSetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_Variables = { "Variables", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeDataSetBase, Variables), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_Variables_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_Variables_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_VariableFriendlyNames_Inner = { "VariableFriendlyNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_VariableFriendlyNames_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraNodeDataSetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_VariableFriendlyNames = { "VariableFriendlyNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeDataSetBase, VariableFriendlyNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_VariableFriendlyNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_VariableFriendlyNames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_ExternalStructAsset_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraNodeDataSetBase.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_ExternalStructAsset = { "ExternalStructAsset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeDataSetBase, ExternalStructAsset), Z_Construct_UClass_UStruct, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_ExternalStructAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_ExternalStructAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_DataSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_Variables_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_Variables,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_VariableFriendlyNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_VariableFriendlyNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::NewProp_ExternalStructAsset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeDataSetBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::ClassParams = {
		&UNiagaraNodeDataSetBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeDataSetBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeDataSetBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeDataSetBase, 2946187888);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeDataSetBase>()
	{
		return UNiagaraNodeDataSetBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeDataSetBase(Z_Construct_UClass_UNiagaraNodeDataSetBase, &UNiagaraNodeDataSetBase::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeDataSetBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeDataSetBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
