// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/UpgradeNiagaraScriptResults.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUpgradeNiagaraScriptResults() {}
// Cross Module References
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraPythonScriptInputSource();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraPythonScriptModuleInput_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraPythonScriptModuleInput();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector4();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UUpgradeNiagaraScriptResults_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UUpgradeNiagaraScriptResults();
// End Cross Module References
	static UEnum* ENiagaraPythonScriptInputSource_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraPythonScriptInputSource, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraPythonScriptInputSource"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraPythonScriptInputSource>()
	{
		return ENiagaraPythonScriptInputSource_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraPythonScriptInputSource(ENiagaraPythonScriptInputSource_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraPythonScriptInputSource"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraPythonScriptInputSource_Hash() { return 3249089478U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraPythonScriptInputSource()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraPythonScriptInputSource"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraPythonScriptInputSource_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraPythonScriptInputSource::Input", (int64)ENiagaraPythonScriptInputSource::Input },
				{ "ENiagaraPythonScriptInputSource::Output", (int64)ENiagaraPythonScriptInputSource::Output },
				{ "ENiagaraPythonScriptInputSource::Local", (int64)ENiagaraPythonScriptInputSource::Local },
				{ "ENiagaraPythonScriptInputSource::InputOutput", (int64)ENiagaraPythonScriptInputSource::InputOutput },
				{ "ENiagaraPythonScriptInputSource::InitialValueInput", (int64)ENiagaraPythonScriptInputSource::InitialValueInput },
				{ "ENiagaraPythonScriptInputSource::None", (int64)ENiagaraPythonScriptInputSource::None },
				{ "ENiagaraPythonScriptInputSource::Num", (int64)ENiagaraPythonScriptInputSource::Num },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "InitialValueInput.Name", "ENiagaraPythonScriptInputSource::InitialValueInput" },
				{ "Input.Name", "ENiagaraPythonScriptInputSource::Input" },
				{ "InputOutput.Name", "ENiagaraPythonScriptInputSource::InputOutput" },
				{ "Local.Name", "ENiagaraPythonScriptInputSource::Local" },
				{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
				{ "None.Comment", "// insert new script parameter usages before\n" },
				{ "None.Hidden", "" },
				{ "None.Name", "ENiagaraPythonScriptInputSource::None" },
				{ "None.ToolTip", "insert new script parameter usages before" },
				{ "Num.Hidden", "" },
				{ "Num.Name", "ENiagaraPythonScriptInputSource::Num" },
				{ "Output.Name", "ENiagaraPythonScriptInputSource::Output" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraPythonScriptInputSource",
				"ENiagaraPythonScriptInputSource",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execAsEnum)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->AsEnum();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execAsQuat)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FQuat*)Z_Param__Result=P_THIS->AsQuat();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execAsColor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FLinearColor*)Z_Param__Result=P_THIS->AsColor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execAsVec4)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector4*)Z_Param__Result=P_THIS->AsVec4();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execAsVec3)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=P_THIS->AsVec3();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execAsVec2)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector2D*)Z_Param__Result=P_THIS->AsVec2();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execAsBool)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->AsBool();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execAsInt)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->AsInt();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execAsFloat)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->AsFloat();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execIsLocalValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsLocalValue();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraPythonScriptModuleInput::execIsSet)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsSet();
		P_NATIVE_END;
	}
	void UNiagaraPythonScriptModuleInput::StaticRegisterNativesUNiagaraPythonScriptModuleInput()
	{
		UClass* Class = UNiagaraPythonScriptModuleInput::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AsBool", &UNiagaraPythonScriptModuleInput::execAsBool },
			{ "AsColor", &UNiagaraPythonScriptModuleInput::execAsColor },
			{ "AsEnum", &UNiagaraPythonScriptModuleInput::execAsEnum },
			{ "AsFloat", &UNiagaraPythonScriptModuleInput::execAsFloat },
			{ "AsInt", &UNiagaraPythonScriptModuleInput::execAsInt },
			{ "AsQuat", &UNiagaraPythonScriptModuleInput::execAsQuat },
			{ "AsVec2", &UNiagaraPythonScriptModuleInput::execAsVec2 },
			{ "AsVec3", &UNiagaraPythonScriptModuleInput::execAsVec3 },
			{ "AsVec4", &UNiagaraPythonScriptModuleInput::execAsVec4 },
			{ "IsLocalValue", &UNiagaraPythonScriptModuleInput::execIsLocalValue },
			{ "IsSet", &UNiagaraPythonScriptModuleInput::execIsSet },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventAsBool_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((NiagaraPythonScriptModuleInput_eventAsBool_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraPythonScriptModuleInput_eventAsBool_Parms), &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "AsBool", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventAsBool_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventAsColor_Parms
		{
			FLinearColor ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraPythonScriptModuleInput_eventAsColor_Parms, ReturnValue), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "AsColor", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventAsColor_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventAsEnum_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraPythonScriptModuleInput_eventAsEnum_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "AsEnum", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventAsEnum_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventAsFloat_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraPythonScriptModuleInput_eventAsFloat_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "AsFloat", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventAsFloat_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventAsInt_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraPythonScriptModuleInput_eventAsInt_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "AsInt", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventAsInt_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventAsQuat_Parms
		{
			FQuat ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraPythonScriptModuleInput_eventAsQuat_Parms, ReturnValue), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "AsQuat", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventAsQuat_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventAsVec2_Parms
		{
			FVector2D ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraPythonScriptModuleInput_eventAsVec2_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "AsVec2", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventAsVec2_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventAsVec3_Parms
		{
			FVector ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraPythonScriptModuleInput_eventAsVec3_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "AsVec3", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventAsVec3_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventAsVec4_Parms
		{
			FVector4 ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraPythonScriptModuleInput_eventAsVec4_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector4, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "AsVec4", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventAsVec4_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventIsLocalValue_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((NiagaraPythonScriptModuleInput_eventIsLocalValue_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraPythonScriptModuleInput_eventIsLocalValue_Parms), &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "IsLocalValue", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventIsLocalValue_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics
	{
		struct NiagaraPythonScriptModuleInput_eventIsSet_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((NiagaraPythonScriptModuleInput_eventIsSet_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraPythonScriptModuleInput_eventIsSet_Parms), &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraPythonScriptModuleInput, nullptr, "IsSet", nullptr, nullptr, sizeof(NiagaraPythonScriptModuleInput_eventIsSet_Parms), Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UNiagaraPythonScriptModuleInput_NoRegister()
	{
		return UNiagaraPythonScriptModuleInput::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Input;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsBool, "AsBool" }, // 226588028
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsColor, "AsColor" }, // 689466330
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsEnum, "AsEnum" }, // 2227093819
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsFloat, "AsFloat" }, // 493912155
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsInt, "AsInt" }, // 2308458571
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsQuat, "AsQuat" }, // 1920011189
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec2, "AsVec2" }, // 428923992
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec3, "AsVec3" }, // 990751441
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_AsVec4, "AsVec4" }, // 2964372992
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsLocalValue, "IsLocalValue" }, // 433919425
		{ &Z_Construct_UFunction_UNiagaraPythonScriptModuleInput_IsSet, "IsSet" }, // 2052670744
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::Class_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "BlueprintType", "true" },
		{ "Comment", "/** Wrapper for setting the value on a parameter of a UNiagaraScript, applied through a UUpgradeNiagaraScriptResults. */" },
		{ "IncludePath", "UpgradeNiagaraScriptResults.h" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
		{ "ToolTip", "Wrapper for setting the value on a parameter of a UNiagaraScript, applied through a UUpgradeNiagaraScriptResults." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::NewProp_Input_MetaData[] = {
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraPythonScriptModuleInput, Input), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::NewProp_Input_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::NewProp_Input,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraPythonScriptModuleInput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::ClassParams = {
		&UNiagaraPythonScriptModuleInput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraPythonScriptModuleInput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraPythonScriptModuleInput, 1657920040);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraPythonScriptModuleInput>()
	{
		return UNiagaraPythonScriptModuleInput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraPythonScriptModuleInput(Z_Construct_UClass_UNiagaraPythonScriptModuleInput, &UNiagaraPythonScriptModuleInput::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraPythonScriptModuleInput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraPythonScriptModuleInput);
	DEFINE_FUNCTION(UUpgradeNiagaraScriptResults::execSetEnumInput)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputName);
		P_GET_PROPERTY(FStrProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetEnumInput(Z_Param_InputName,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUpgradeNiagaraScriptResults::execSetQuatInput)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputName);
		P_GET_STRUCT(FQuat,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetQuatInput(Z_Param_InputName,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUpgradeNiagaraScriptResults::execSetColorInput)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputName);
		P_GET_STRUCT(FLinearColor,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetColorInput(Z_Param_InputName,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUpgradeNiagaraScriptResults::execSetVec4Input)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputName);
		P_GET_STRUCT(FVector4,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetVec4Input(Z_Param_InputName,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUpgradeNiagaraScriptResults::execSetVec3Input)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputName);
		P_GET_STRUCT(FVector,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetVec3Input(Z_Param_InputName,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUpgradeNiagaraScriptResults::execSetVec2Input)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputName);
		P_GET_STRUCT(FVector2D,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetVec2Input(Z_Param_InputName,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUpgradeNiagaraScriptResults::execSetBoolInput)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputName);
		P_GET_UBOOL(Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetBoolInput(Z_Param_InputName,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUpgradeNiagaraScriptResults::execSetIntInput)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputName);
		P_GET_PROPERTY(FIntProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetIntInput(Z_Param_InputName,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUpgradeNiagaraScriptResults::execSetFloatInput)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputName);
		P_GET_PROPERTY(FFloatProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFloatInput(Z_Param_InputName,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUpgradeNiagaraScriptResults::execGetOldInput)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraPythonScriptModuleInput**)Z_Param__Result=P_THIS->GetOldInput(Z_Param_InputName);
		P_NATIVE_END;
	}
	void UUpgradeNiagaraScriptResults::StaticRegisterNativesUUpgradeNiagaraScriptResults()
	{
		UClass* Class = UUpgradeNiagaraScriptResults::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetOldInput", &UUpgradeNiagaraScriptResults::execGetOldInput },
			{ "SetBoolInput", &UUpgradeNiagaraScriptResults::execSetBoolInput },
			{ "SetColorInput", &UUpgradeNiagaraScriptResults::execSetColorInput },
			{ "SetEnumInput", &UUpgradeNiagaraScriptResults::execSetEnumInput },
			{ "SetFloatInput", &UUpgradeNiagaraScriptResults::execSetFloatInput },
			{ "SetIntInput", &UUpgradeNiagaraScriptResults::execSetIntInput },
			{ "SetQuatInput", &UUpgradeNiagaraScriptResults::execSetQuatInput },
			{ "SetVec2Input", &UUpgradeNiagaraScriptResults::execSetVec2Input },
			{ "SetVec3Input", &UUpgradeNiagaraScriptResults::execSetVec3Input },
			{ "SetVec4Input", &UUpgradeNiagaraScriptResults::execSetVec4Input },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics
	{
		struct UpgradeNiagaraScriptResults_eventGetOldInput_Parms
		{
			FString InputName;
			UNiagaraPythonScriptModuleInput* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputName;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::NewProp_InputName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventGetOldInput_Parms, InputName), METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::NewProp_InputName_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventGetOldInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraPythonScriptModuleInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUpgradeNiagaraScriptResults, nullptr, "GetOldInput", nullptr, nullptr, sizeof(UpgradeNiagaraScriptResults_eventGetOldInput_Parms), Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics
	{
		struct UpgradeNiagaraScriptResults_eventSetBoolInput_Parms
		{
			FString InputName;
			bool Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputName;
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::NewProp_InputName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetBoolInput_Parms, InputName), METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::NewProp_InputName_MetaData)) };
	void Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((UpgradeNiagaraScriptResults_eventSetBoolInput_Parms*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UpgradeNiagaraScriptResults_eventSetBoolInput_Parms), &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::NewProp_Value_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUpgradeNiagaraScriptResults, nullptr, "SetBoolInput", nullptr, nullptr, sizeof(UpgradeNiagaraScriptResults_eventSetBoolInput_Parms), Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics
	{
		struct UpgradeNiagaraScriptResults_eventSetColorInput_Parms
		{
			FString InputName;
			FLinearColor Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::NewProp_InputName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetColorInput_Parms, InputName), METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::NewProp_InputName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetColorInput_Parms, Value), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUpgradeNiagaraScriptResults, nullptr, "SetColorInput", nullptr, nullptr, sizeof(UpgradeNiagaraScriptResults_eventSetColorInput_Parms), Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics
	{
		struct UpgradeNiagaraScriptResults_eventSetEnumInput_Parms
		{
			FString InputName;
			FString Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::NewProp_InputName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetEnumInput_Parms, InputName), METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::NewProp_InputName_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetEnumInput_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUpgradeNiagaraScriptResults, nullptr, "SetEnumInput", nullptr, nullptr, sizeof(UpgradeNiagaraScriptResults_eventSetEnumInput_Parms), Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics
	{
		struct UpgradeNiagaraScriptResults_eventSetFloatInput_Parms
		{
			FString InputName;
			float Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputName;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::NewProp_InputName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetFloatInput_Parms, InputName), METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::NewProp_InputName_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetFloatInput_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUpgradeNiagaraScriptResults, nullptr, "SetFloatInput", nullptr, nullptr, sizeof(UpgradeNiagaraScriptResults_eventSetFloatInput_Parms), Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics
	{
		struct UpgradeNiagaraScriptResults_eventSetIntInput_Parms
		{
			FString InputName;
			int32 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputName;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::NewProp_InputName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetIntInput_Parms, InputName), METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::NewProp_InputName_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetIntInput_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUpgradeNiagaraScriptResults, nullptr, "SetIntInput", nullptr, nullptr, sizeof(UpgradeNiagaraScriptResults_eventSetIntInput_Parms), Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics
	{
		struct UpgradeNiagaraScriptResults_eventSetQuatInput_Parms
		{
			FString InputName;
			FQuat Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::NewProp_InputName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetQuatInput_Parms, InputName), METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::NewProp_InputName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetQuatInput_Parms, Value), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUpgradeNiagaraScriptResults, nullptr, "SetQuatInput", nullptr, nullptr, sizeof(UpgradeNiagaraScriptResults_eventSetQuatInput_Parms), Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics
	{
		struct UpgradeNiagaraScriptResults_eventSetVec2Input_Parms
		{
			FString InputName;
			FVector2D Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::NewProp_InputName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetVec2Input_Parms, InputName), METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::NewProp_InputName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetVec2Input_Parms, Value), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUpgradeNiagaraScriptResults, nullptr, "SetVec2Input", nullptr, nullptr, sizeof(UpgradeNiagaraScriptResults_eventSetVec2Input_Parms), Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics
	{
		struct UpgradeNiagaraScriptResults_eventSetVec3Input_Parms
		{
			FString InputName;
			FVector Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::NewProp_InputName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetVec3Input_Parms, InputName), METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::NewProp_InputName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetVec3Input_Parms, Value), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUpgradeNiagaraScriptResults, nullptr, "SetVec3Input", nullptr, nullptr, sizeof(UpgradeNiagaraScriptResults_eventSetVec3Input_Parms), Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics
	{
		struct UpgradeNiagaraScriptResults_eventSetVec4Input_Parms
		{
			FString InputName;
			FVector4 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::NewProp_InputName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetVec4Input_Parms, InputName), METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::NewProp_InputName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UpgradeNiagaraScriptResults_eventSetVec4Input_Parms, Value), Z_Construct_UScriptStruct_FVector4, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::Function_MetaDataParams[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUpgradeNiagaraScriptResults, nullptr, "SetVec4Input", nullptr, nullptr, sizeof(UpgradeNiagaraScriptResults_eventSetVec4Input_Parms), Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UUpgradeNiagaraScriptResults_NoRegister()
	{
		return UUpgradeNiagaraScriptResults::StaticClass();
	}
	struct Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCancelledByPythonError_MetaData[];
#endif
		static void NewProp_bCancelledByPythonError_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCancelledByPythonError;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OldInputs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OldInputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OldInputs;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewInputs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewInputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NewInputs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DummyInput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DummyInput;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_GetOldInput, "GetOldInput" }, // 271345506
		{ &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetBoolInput, "SetBoolInput" }, // 2646068369
		{ &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetColorInput, "SetColorInput" }, // 181586752
		{ &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetEnumInput, "SetEnumInput" }, // 2226059514
		{ &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetFloatInput, "SetFloatInput" }, // 1816212627
		{ &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetIntInput, "SetIntInput" }, // 4037490474
		{ &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetQuatInput, "SetQuatInput" }, // 458493013
		{ &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec2Input, "SetVec2Input" }, // 1522523339
		{ &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec3Input, "SetVec3Input" }, // 788140338
		{ &Z_Construct_UFunction_UUpgradeNiagaraScriptResults_SetVec4Input, "SetVec4Input" }, // 2710961627
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Wrapper class for passing results back from the version upgrade python script.\n */" },
		{ "IncludePath", "UpgradeNiagaraScriptResults.h" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
		{ "ToolTip", "Wrapper class for passing results back from the version upgrade python script." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_bCancelledByPythonError_MetaData[] = {
		{ "Category", "Scripting" },
		{ "Comment", "// Whether the converter process was cancelled due to an unrecoverable error in the python script process.\n" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
		{ "ToolTip", "Whether the converter process was cancelled due to an unrecoverable error in the python script process." },
	};
#endif
	void Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_bCancelledByPythonError_SetBit(void* Obj)
	{
		((UUpgradeNiagaraScriptResults*)Obj)->bCancelledByPythonError = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_bCancelledByPythonError = { "bCancelledByPythonError", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUpgradeNiagaraScriptResults), &Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_bCancelledByPythonError_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_bCancelledByPythonError_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_bCancelledByPythonError_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_OldInputs_Inner = { "OldInputs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraPythonScriptModuleInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_OldInputs_MetaData[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_OldInputs = { "OldInputs", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpgradeNiagaraScriptResults, OldInputs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_OldInputs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_OldInputs_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_NewInputs_Inner = { "NewInputs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraPythonScriptModuleInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_NewInputs_MetaData[] = {
		{ "Category", "Scripting" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_NewInputs = { "NewInputs", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpgradeNiagaraScriptResults, NewInputs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_NewInputs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_NewInputs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_DummyInput_MetaData[] = {
		{ "Comment", "// This is used as a placeholder object for python to interact with when a module input could not be found. Returning a nullptr instead would crash the script.\n" },
		{ "ModuleRelativePath", "Public/UpgradeNiagaraScriptResults.h" },
		{ "ToolTip", "This is used as a placeholder object for python to interact with when a module input could not be found. Returning a nullptr instead would crash the script." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_DummyInput = { "DummyInput", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUpgradeNiagaraScriptResults, DummyInput), Z_Construct_UClass_UNiagaraPythonScriptModuleInput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_DummyInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_DummyInput_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_bCancelledByPythonError,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_OldInputs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_OldInputs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_NewInputs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_NewInputs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::NewProp_DummyInput,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUpgradeNiagaraScriptResults>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::ClassParams = {
		&UUpgradeNiagaraScriptResults::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUpgradeNiagaraScriptResults()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUpgradeNiagaraScriptResults, 2253116572);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UUpgradeNiagaraScriptResults>()
	{
		return UUpgradeNiagaraScriptResults::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUpgradeNiagaraScriptResults(Z_Construct_UClass_UUpgradeNiagaraScriptResults, &UUpgradeNiagaraScriptResults::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UUpgradeNiagaraScriptResults"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUpgradeNiagaraScriptResults);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
