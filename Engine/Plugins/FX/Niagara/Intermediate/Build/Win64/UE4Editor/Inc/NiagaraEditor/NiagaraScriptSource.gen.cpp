// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraScriptSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraScriptSource() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScriptSource_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScriptSource();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraScriptSourceBase();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraGraph_NoRegister();
// End Cross Module References
	void UNiagaraScriptSource::StaticRegisterNativesUNiagaraScriptSource()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraScriptSource_NoRegister()
	{
		return UNiagaraScriptSource::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraScriptSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeGraph_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NodeGraph;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraScriptSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraScriptSourceBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptSource_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraScriptSource.h" },
		{ "ModuleRelativePath", "Public/NiagaraScriptSource.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptSource_Statics::NewProp_NodeGraph_MetaData[] = {
		{ "Comment", "/** Graph for particle update expression */" },
		{ "ModuleRelativePath", "Public/NiagaraScriptSource.h" },
		{ "ToolTip", "Graph for particle update expression" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraScriptSource_Statics::NewProp_NodeGraph = { "NodeGraph", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraScriptSource, NodeGraph), Z_Construct_UClass_UNiagaraGraph_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptSource_Statics::NewProp_NodeGraph_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptSource_Statics::NewProp_NodeGraph_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraScriptSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptSource_Statics::NewProp_NodeGraph,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraScriptSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraScriptSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraScriptSource_Statics::ClassParams = {
		&UNiagaraScriptSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraScriptSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptSource_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraScriptSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraScriptSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraScriptSource, 4189579061);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraScriptSource>()
	{
		return UNiagaraScriptSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraScriptSource(Z_Construct_UClass_UNiagaraScriptSource, &UNiagaraScriptSource::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraScriptSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraScriptSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
