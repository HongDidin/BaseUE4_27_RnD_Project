// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/Customizations/NiagaraTypeCustomizations.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraTypeCustomizations() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphSchemaAction();
// End Cross Module References

static_assert(std::is_polymorphic<FNiagaraStackAssetAction_VarBind>() == std::is_polymorphic<FEdGraphSchemaAction>(), "USTRUCT FNiagaraStackAssetAction_VarBind cannot be polymorphic unless super FEdGraphSchemaAction is polymorphic");

class UScriptStruct* FNiagaraStackAssetAction_VarBind::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraStackAssetAction_VarBind"), sizeof(FNiagaraStackAssetAction_VarBind), Get_Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraStackAssetAction_VarBind>()
{
	return FNiagaraStackAssetAction_VarBind::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraStackAssetAction_VarBind(FNiagaraStackAssetAction_VarBind::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraStackAssetAction_VarBind"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraStackAssetAction_VarBind
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraStackAssetAction_VarBind()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraStackAssetAction_VarBind>(FName(TEXT("NiagaraStackAssetAction_VarBind")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraStackAssetAction_VarBind;
	struct Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Customizations/NiagaraTypeCustomizations.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraStackAssetAction_VarBind>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		Z_Construct_UScriptStruct_FEdGraphSchemaAction,
		&NewStructOps,
		"NiagaraStackAssetAction_VarBind",
		sizeof(FNiagaraStackAssetAction_VarBind),
		alignof(FNiagaraStackAssetAction_VarBind),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraStackAssetAction_VarBind"), sizeof(FNiagaraStackAssetAction_VarBind), Get_Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraStackAssetAction_VarBind_Hash() { return 2719297977U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
