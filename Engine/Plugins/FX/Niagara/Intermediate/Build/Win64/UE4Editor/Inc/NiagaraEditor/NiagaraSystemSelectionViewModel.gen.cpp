// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/NiagaraSystemSelectionViewModel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraSystemSelectionViewModel() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraSystemSelectionViewModel_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraSystemSelectionViewModel();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackSelection_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackViewModel_NoRegister();
// End Cross Module References
	void UNiagaraSystemSelectionViewModel::StaticRegisterNativesUNiagaraSystemSelectionViewModel()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraSystemSelectionViewModel_NoRegister()
	{
		return UNiagaraSystemSelectionViewModel::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StackSelection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StackSelection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionStackViewModel_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectionStackViewModel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/NiagaraSystemSelectionViewModel.h" },
		{ "ModuleRelativePath", "Public/ViewModels/NiagaraSystemSelectionViewModel.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::NewProp_StackSelection_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/NiagaraSystemSelectionViewModel.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::NewProp_StackSelection = { "StackSelection", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemSelectionViewModel, StackSelection), Z_Construct_UClass_UNiagaraStackSelection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::NewProp_StackSelection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::NewProp_StackSelection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::NewProp_SelectionStackViewModel_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/NiagaraSystemSelectionViewModel.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::NewProp_SelectionStackViewModel = { "SelectionStackViewModel", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemSelectionViewModel, SelectionStackViewModel), Z_Construct_UClass_UNiagaraStackViewModel_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::NewProp_SelectionStackViewModel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::NewProp_SelectionStackViewModel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::NewProp_StackSelection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::NewProp_SelectionStackViewModel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraSystemSelectionViewModel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::ClassParams = {
		&UNiagaraSystemSelectionViewModel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraSystemSelectionViewModel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraSystemSelectionViewModel, 2896310492);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraSystemSelectionViewModel>()
	{
		return UNiagaraSystemSelectionViewModel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraSystemSelectionViewModel(Z_Construct_UClass_UNiagaraSystemSelectionViewModel, &UNiagaraSystemSelectionViewModel::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraSystemSelectionViewModel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraSystemSelectionViewModel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
