// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackModuleItemLinkedInputCollection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackModuleItemLinkedInputCollection() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEntry();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraStackModuleItemLinkedInputCollection::StaticRegisterNativesUNiagaraStackModuleItemLinkedInputCollection()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection_NoRegister()
	{
		return UNiagaraStackModuleItemLinkedInputCollection::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackEntry,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackModuleItemLinkedInputCollection.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackModuleItemLinkedInputCollection.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackModuleItemLinkedInputCollection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection_Statics::ClassParams = {
		&UNiagaraStackModuleItemLinkedInputCollection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackModuleItemLinkedInputCollection, 2644400995);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackModuleItemLinkedInputCollection>()
	{
		return UNiagaraStackModuleItemLinkedInputCollection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackModuleItemLinkedInputCollection(Z_Construct_UClass_UNiagaraStackModuleItemLinkedInputCollection, &UNiagaraStackModuleItemLinkedInputCollection::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackModuleItemLinkedInputCollection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackModuleItemLinkedInputCollection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
