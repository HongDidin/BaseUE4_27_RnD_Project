// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraNodeUsageSelector.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeUsageSelector() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeUsageSelector_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeUsageSelector();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeWithDynamicPins();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariable();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	void UNiagaraNodeUsageSelector::StaticRegisterNativesUNiagaraNodeUsageSelector()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeUsageSelector_NoRegister()
	{
		return UNiagaraNodeUsageSelector::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputVars_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputVars_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutputVars;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputVarGuids_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputVarGuids_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutputVarGuids;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumOptionsPerVariable_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumOptionsPerVariable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNodeWithDynamicPins,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraNodeUsageSelector.h" },
		{ "ModuleRelativePath", "Private/NiagaraNodeUsageSelector.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVars_Inner = { "OutputVars", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraVariable, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVars_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeUsageSelector.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVars = { "OutputVars", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeUsageSelector, OutputVars), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVars_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVars_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVarGuids_Inner = { "OutputVarGuids", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVarGuids_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeUsageSelector.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVarGuids = { "OutputVarGuids", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeUsageSelector, OutputVarGuids), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVarGuids_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVarGuids_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_NumOptionsPerVariable_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeUsageSelector.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_NumOptionsPerVariable = { "NumOptionsPerVariable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeUsageSelector, NumOptionsPerVariable), METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_NumOptionsPerVariable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_NumOptionsPerVariable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVars_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVars,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVarGuids_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_OutputVarGuids,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::NewProp_NumOptionsPerVariable,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeUsageSelector>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::ClassParams = {
		&UNiagaraNodeUsageSelector::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeUsageSelector()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeUsageSelector, 712517810);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeUsageSelector>()
	{
		return UNiagaraNodeUsageSelector::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeUsageSelector(Z_Construct_UClass_UNiagaraNodeUsageSelector, &UNiagaraNodeUsageSelector::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeUsageSelector"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeUsageSelector);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
