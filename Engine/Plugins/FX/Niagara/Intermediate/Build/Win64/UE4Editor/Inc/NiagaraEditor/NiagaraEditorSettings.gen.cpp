// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraEditorSettings() {}
// Cross Module References
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraNamespaceMetadataOptions();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraActionColors();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraCurveTemplate();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraEditorSettings_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraEditorSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraSpawnShortcut();
// End Cross Module References
	static UEnum* ENiagaraNamespaceMetadataOptions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraNamespaceMetadataOptions, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraNamespaceMetadataOptions"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraNamespaceMetadataOptions>()
	{
		return ENiagaraNamespaceMetadataOptions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraNamespaceMetadataOptions(ENiagaraNamespaceMetadataOptions_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraNamespaceMetadataOptions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraNamespaceMetadataOptions_Hash() { return 2662647009U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraNamespaceMetadataOptions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraNamespaceMetadataOptions"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraNamespaceMetadataOptions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraNamespaceMetadataOptions::HideInScript", (int64)ENiagaraNamespaceMetadataOptions::HideInScript },
				{ "ENiagaraNamespaceMetadataOptions::HideInSystem", (int64)ENiagaraNamespaceMetadataOptions::HideInSystem },
				{ "ENiagaraNamespaceMetadataOptions::AdvancedInScript", (int64)ENiagaraNamespaceMetadataOptions::AdvancedInScript },
				{ "ENiagaraNamespaceMetadataOptions::AdvancedInSystem", (int64)ENiagaraNamespaceMetadataOptions::AdvancedInSystem },
				{ "ENiagaraNamespaceMetadataOptions::PreventEditingNamespace", (int64)ENiagaraNamespaceMetadataOptions::PreventEditingNamespace },
				{ "ENiagaraNamespaceMetadataOptions::PreventEditingNamespaceModifier", (int64)ENiagaraNamespaceMetadataOptions::PreventEditingNamespaceModifier },
				{ "ENiagaraNamespaceMetadataOptions::PreventEditingName", (int64)ENiagaraNamespaceMetadataOptions::PreventEditingName },
				{ "ENiagaraNamespaceMetadataOptions::PreventCreatingInSystemEditor", (int64)ENiagaraNamespaceMetadataOptions::PreventCreatingInSystemEditor },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AdvancedInScript.Name", "ENiagaraNamespaceMetadataOptions::AdvancedInScript" },
				{ "AdvancedInSystem.Name", "ENiagaraNamespaceMetadataOptions::AdvancedInSystem" },
				{ "HideInScript.Name", "ENiagaraNamespaceMetadataOptions::HideInScript" },
				{ "HideInSystem.Name", "ENiagaraNamespaceMetadataOptions::HideInSystem" },
				{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
				{ "PreventCreatingInSystemEditor.Name", "ENiagaraNamespaceMetadataOptions::PreventCreatingInSystemEditor" },
				{ "PreventEditingName.Name", "ENiagaraNamespaceMetadataOptions::PreventEditingName" },
				{ "PreventEditingNamespace.Name", "ENiagaraNamespaceMetadataOptions::PreventEditingNamespace" },
				{ "PreventEditingNamespaceModifier.Name", "ENiagaraNamespaceMetadataOptions::PreventEditingNamespaceModifier" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraNamespaceMetadataOptions",
				"ENiagaraNamespaceMetadataOptions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FNiagaraActionColors::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraActionColors_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraActionColors, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraActionColors"), sizeof(FNiagaraActionColors), Get_Z_Construct_UScriptStruct_FNiagaraActionColors_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraActionColors>()
{
	return FNiagaraActionColors::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraActionColors(FNiagaraActionColors::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraActionColors"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraActionColors
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraActionColors()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraActionColors>(FName(TEXT("NiagaraActionColors")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraActionColors;
	struct Z_Construct_UScriptStruct_FNiagaraActionColors_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NiagaraColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NiagaraColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GameColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PluginColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PluginColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeveloperColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DeveloperColor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraActionColors>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_NiagaraColor_MetaData[] = {
		{ "Category", "Color" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_NiagaraColor = { "NiagaraColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraActionColors, NiagaraColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_NiagaraColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_NiagaraColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_GameColor_MetaData[] = {
		{ "Category", "Color" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_GameColor = { "GameColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraActionColors, GameColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_GameColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_GameColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_PluginColor_MetaData[] = {
		{ "Category", "Color" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_PluginColor = { "PluginColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraActionColors, PluginColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_PluginColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_PluginColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_DeveloperColor_MetaData[] = {
		{ "Category", "Color" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_DeveloperColor = { "DeveloperColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraActionColors, DeveloperColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_DeveloperColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_DeveloperColor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_NiagaraColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_GameColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_PluginColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::NewProp_DeveloperColor,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraActionColors",
		sizeof(FNiagaraActionColors),
		alignof(FNiagaraActionColors),
		Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraActionColors()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraActionColors_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraActionColors"), sizeof(FNiagaraActionColors), Get_Z_Construct_UScriptStruct_FNiagaraActionColors_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraActionColors_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraActionColors_Hash() { return 3756800272U; }
class UScriptStruct* FNiagaraCurveTemplate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraCurveTemplate, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraCurveTemplate"), sizeof(FNiagaraCurveTemplate), Get_Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraCurveTemplate>()
{
	return FNiagaraCurveTemplate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraCurveTemplate(FNiagaraCurveTemplate::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraCurveTemplate"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraCurveTemplate
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraCurveTemplate()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraCurveTemplate>(FName(TEXT("NiagaraCurveTemplate")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraCurveTemplate;
	struct Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayNameOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayNameOverride;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraCurveTemplate>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewProp_DisplayNameOverride_MetaData[] = {
		{ "Category", "Curve" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewProp_DisplayNameOverride = { "DisplayNameOverride", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraCurveTemplate, DisplayNameOverride), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewProp_DisplayNameOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewProp_DisplayNameOverride_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewProp_CurveAsset_MetaData[] = {
		{ "AllowedClasses", "CurveFloat" },
		{ "Category", "Curve" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewProp_CurveAsset = { "CurveAsset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraCurveTemplate, CurveAsset), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewProp_CurveAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewProp_CurveAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewProp_DisplayNameOverride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::NewProp_CurveAsset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraCurveTemplate",
		sizeof(FNiagaraCurveTemplate),
		alignof(FNiagaraCurveTemplate),
		Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraCurveTemplate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraCurveTemplate"), sizeof(FNiagaraCurveTemplate), Get_Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Hash() { return 3899075227U; }
class UScriptStruct* FNiagaraNamespaceMetadata::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraNamespaceMetadata"), sizeof(FNiagaraNamespaceMetadata), Get_Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraNamespaceMetadata>()
{
	return FNiagaraNamespaceMetadata::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraNamespaceMetadata(FNiagaraNamespaceMetadata::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraNamespaceMetadata"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraNamespaceMetadata
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraNamespaceMetadata()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraNamespaceMetadata>(FName(TEXT("NiagaraNamespaceMetadata")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraNamespaceMetadata;
	struct Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Namespaces_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Namespaces_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Namespaces;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequiredNamespaceModifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RequiredNamespaceModifier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayNameLong_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_DisplayNameLong;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BackgroundColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BackgroundColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForegroundStyle_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ForegroundStyle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SortId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SortId;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_OptionalNamespaceModifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OptionalNamespaceModifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OptionalNamespaceModifiers;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Options_Inner_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Options_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Options_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Options;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Guid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Guid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraNamespaceMetadata>();
	}
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Namespaces_Inner = { "Namespaces", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Namespaces_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Namespaces = { "Namespaces", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, Namespaces), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Namespaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Namespaces_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_RequiredNamespaceModifier_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_RequiredNamespaceModifier = { "RequiredNamespaceModifier", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, RequiredNamespaceModifier), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_RequiredNamespaceModifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_RequiredNamespaceModifier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_DisplayName_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_DisplayNameLong_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_DisplayNameLong = { "DisplayNameLong", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, DisplayNameLong), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_DisplayNameLong_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_DisplayNameLong_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Description_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_BackgroundColor_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_BackgroundColor = { "BackgroundColor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, BackgroundColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_BackgroundColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_BackgroundColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_ForegroundStyle_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_ForegroundStyle = { "ForegroundStyle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, ForegroundStyle), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_ForegroundStyle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_ForegroundStyle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_SortId_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_SortId = { "SortId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, SortId), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_SortId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_SortId_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_OptionalNamespaceModifiers_Inner = { "OptionalNamespaceModifiers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_OptionalNamespaceModifiers_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_OptionalNamespaceModifiers = { "OptionalNamespaceModifiers", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, OptionalNamespaceModifiers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_OptionalNamespaceModifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_OptionalNamespaceModifiers_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Options_Inner_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Options_Inner = { "Options", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UEnum_NiagaraEditor_ENiagaraNamespaceMetadataOptions, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Options_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, Options), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Options_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Options_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Guid_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Guid = { "Guid", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNamespaceMetadata, Guid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Guid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Guid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Namespaces_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Namespaces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_RequiredNamespaceModifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_DisplayNameLong,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_BackgroundColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_ForegroundStyle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_SortId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_OptionalNamespaceModifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_OptionalNamespaceModifiers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Options_Inner_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Options_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Options,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::NewProp_Guid,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraNamespaceMetadata",
		sizeof(FNiagaraNamespaceMetadata),
		alignof(FNiagaraNamespaceMetadata),
		Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraNamespaceMetadata"), sizeof(FNiagaraNamespaceMetadata), Get_Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Hash() { return 1679138109U; }
class UScriptStruct* FNiagaraNewAssetDialogConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraNewAssetDialogConfig"), sizeof(FNiagaraNewAssetDialogConfig), Get_Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraNewAssetDialogConfig>()
{
	return FNiagaraNewAssetDialogConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraNewAssetDialogConfig(FNiagaraNewAssetDialogConfig::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraNewAssetDialogConfig"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraNewAssetDialogConfig
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraNewAssetDialogConfig()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraNewAssetDialogConfig>(FName(TEXT("NiagaraNewAssetDialogConfig")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraNewAssetDialogConfig;
	struct Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedOptionIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SelectedOptionIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WindowSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WindowSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraNewAssetDialogConfig>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewProp_SelectedOptionIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewProp_SelectedOptionIndex = { "SelectedOptionIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNewAssetDialogConfig, SelectedOptionIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewProp_SelectedOptionIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewProp_SelectedOptionIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewProp_WindowSize_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewProp_WindowSize = { "WindowSize", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraNewAssetDialogConfig, WindowSize), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewProp_WindowSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewProp_WindowSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewProp_SelectedOptionIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::NewProp_WindowSize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraNewAssetDialogConfig",
		sizeof(FNiagaraNewAssetDialogConfig),
		alignof(FNiagaraNewAssetDialogConfig),
		Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraNewAssetDialogConfig"), sizeof(FNiagaraNewAssetDialogConfig), Get_Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Hash() { return 2944567710U; }
	void UNiagaraEditorSettings::StaticRegisterNativesUNiagaraEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraEditorSettings_NoRegister()
	{
		return UNiagaraEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultScript_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultScript;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultDynamicInputScript_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultDynamicInputScript;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultFunctionScript_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultFunctionScript;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultModuleScript_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultModuleScript;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequiredSystemUpdateScript_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RequiredSystemUpdateScript;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GraphCreationShortcuts_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GraphCreationShortcuts_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GraphCreationShortcuts;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableBaker_MetaData[];
#endif
		static void NewProp_bEnableBaker_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableBaker;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoCompile_MetaData[];
#endif
		static void NewProp_bAutoCompile_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoCompile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoPlay_MetaData[];
#endif
		static void NewProp_bAutoPlay_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoPlay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetSimulationOnChange_MetaData[];
#endif
		static void NewProp_bResetSimulationOnChange_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetSimulationOnChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResimulateOnChangeWhilePaused_MetaData[];
#endif
		static void NewProp_bResimulateOnChangeWhilePaused_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResimulateOnChangeWhilePaused;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetDependentSystemsWhenEditingEmitters_MetaData[];
#endif
		static void NewProp_bResetDependentSystemsWhenEditingEmitters_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetDependentSystemsWhenEditingEmitters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayAdvancedParameterPanelCategories_MetaData[];
#endif
		static void NewProp_bDisplayAdvancedParameterPanelCategories_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayAdvancedParameterPanelCategories;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlaybackSpeeds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaybackSpeeds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PlaybackSpeeds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActionColors_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActionColors;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewAssetDialogConfigMap_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewAssetDialogConfigMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewAssetDialogConfigMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_NewAssetDialogConfigMap;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_HLSLKeywordReplacements_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_HLSLKeywordReplacements_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HLSLKeywordReplacements_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_HLSLKeywordReplacements;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NamespaceMetadata_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NamespaceMetadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NamespaceMetadata;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NamespaceModifierMetadata_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NamespaceModifierMetadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NamespaceModifierMetadata;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultNamespaceMetadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultNamespaceMetadata;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultNamespaceModifierMetadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultNamespaceModifierMetadata;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveTemplates_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveTemplates_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CurveTemplates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Niagara" },
		{ "IncludePath", "NiagaraEditorSettings.h" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultScript_MetaData[] = {
		{ "Category", "Niagara" },
		{ "Comment", "/** Niagara script to duplicate as the base of all new script assets created. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Niagara script to duplicate as the base of all new script assets created." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultScript = { "DefaultScript", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, DefaultScript), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultScript_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultScript_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultDynamicInputScript_MetaData[] = {
		{ "Category", "Niagara" },
		{ "Comment", "/** Niagara script to duplicate as the base of all new script assets created. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Niagara script to duplicate as the base of all new script assets created." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultDynamicInputScript = { "DefaultDynamicInputScript", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, DefaultDynamicInputScript), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultDynamicInputScript_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultDynamicInputScript_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultFunctionScript_MetaData[] = {
		{ "Category", "Niagara" },
		{ "Comment", "/** Niagara script to duplicate as the base of all new script assets created. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Niagara script to duplicate as the base of all new script assets created." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultFunctionScript = { "DefaultFunctionScript", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, DefaultFunctionScript), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultFunctionScript_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultFunctionScript_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultModuleScript_MetaData[] = {
		{ "Category", "Niagara" },
		{ "Comment", "/** Niagara script to duplicate as the base of all new script assets created. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Niagara script to duplicate as the base of all new script assets created." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultModuleScript = { "DefaultModuleScript", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, DefaultModuleScript), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultModuleScript_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultModuleScript_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_RequiredSystemUpdateScript_MetaData[] = {
		{ "Category", "Niagara" },
		{ "Comment", "/** Niagara script which is required in the system update script to control system state. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Niagara script which is required in the system update script to control system state." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_RequiredSystemUpdateScript = { "RequiredSystemUpdateScript", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, RequiredSystemUpdateScript), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_RequiredSystemUpdateScript_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_RequiredSystemUpdateScript_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_GraphCreationShortcuts_Inner = { "GraphCreationShortcuts", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraSpawnShortcut, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_GraphCreationShortcuts_MetaData[] = {
		{ "Category", "Niagara" },
		{ "Comment", "/** Shortcut key bindings that if held down while doing a mouse click, will spawn the specified type of Niagara node.*/" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Shortcut key bindings that if held down while doing a mouse click, will spawn the specified type of Niagara node." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_GraphCreationShortcuts = { "GraphCreationShortcuts", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, GraphCreationShortcuts), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_GraphCreationShortcuts_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_GraphCreationShortcuts_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bEnableBaker_MetaData[] = {
		{ "Category", "Experimental" },
		{ "Comment", "/** Enables the Niagara Baker to be used within the system editor. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Enables the Niagara Baker to be used within the system editor." },
	};
#endif
	void Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bEnableBaker_SetBit(void* Obj)
	{
		((UNiagaraEditorSettings*)Obj)->bEnableBaker = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bEnableBaker = { "bEnableBaker", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraEditorSettings), &Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bEnableBaker_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bEnableBaker_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bEnableBaker_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoCompile_MetaData[] = {
		{ "Category", "Niagara" },
		{ "Comment", "/** Whether or not auto-compile is enabled in the editors. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Whether or not auto-compile is enabled in the editors." },
	};
#endif
	void Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoCompile_SetBit(void* Obj)
	{
		((UNiagaraEditorSettings*)Obj)->bAutoCompile = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoCompile = { "bAutoCompile", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraEditorSettings), &Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoCompile_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoCompile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoCompile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoPlay_MetaData[] = {
		{ "Category", "SimulationOptions" },
		{ "Comment", "/** Whether or not simulations should start playing automatically when the emitter or system editor is opened, or when the data is changed in the editor. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Whether or not simulations should start playing automatically when the emitter or system editor is opened, or when the data is changed in the editor." },
	};
#endif
	void Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoPlay_SetBit(void* Obj)
	{
		((UNiagaraEditorSettings*)Obj)->bAutoPlay = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoPlay = { "bAutoPlay", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraEditorSettings), &Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoPlay_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoPlay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoPlay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetSimulationOnChange_MetaData[] = {
		{ "Category", "SimulationOptions" },
		{ "Comment", "/** Whether or not the simulation should reset when a value on the emitter or system is changed. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Whether or not the simulation should reset when a value on the emitter or system is changed." },
	};
#endif
	void Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetSimulationOnChange_SetBit(void* Obj)
	{
		((UNiagaraEditorSettings*)Obj)->bResetSimulationOnChange = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetSimulationOnChange = { "bResetSimulationOnChange", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraEditorSettings), &Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetSimulationOnChange_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetSimulationOnChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetSimulationOnChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResimulateOnChangeWhilePaused_MetaData[] = {
		{ "Category", "SimulationOptions" },
		{ "Comment", "/** Whether or not to rerun the simulation to the current time when making modifications while paused. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Whether or not to rerun the simulation to the current time when making modifications while paused." },
	};
#endif
	void Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResimulateOnChangeWhilePaused_SetBit(void* Obj)
	{
		((UNiagaraEditorSettings*)Obj)->bResimulateOnChangeWhilePaused = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResimulateOnChangeWhilePaused = { "bResimulateOnChangeWhilePaused", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraEditorSettings), &Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResimulateOnChangeWhilePaused_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResimulateOnChangeWhilePaused_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResimulateOnChangeWhilePaused_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetDependentSystemsWhenEditingEmitters_MetaData[] = {
		{ "Category", "SimulationOptions" },
		{ "Comment", "/** Whether or not to reset all components that include the system currently being reset. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Whether or not to reset all components that include the system currently being reset." },
	};
#endif
	void Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetDependentSystemsWhenEditingEmitters_SetBit(void* Obj)
	{
		((UNiagaraEditorSettings*)Obj)->bResetDependentSystemsWhenEditingEmitters = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetDependentSystemsWhenEditingEmitters = { "bResetDependentSystemsWhenEditingEmitters", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraEditorSettings), &Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetDependentSystemsWhenEditingEmitters_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetDependentSystemsWhenEditingEmitters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetDependentSystemsWhenEditingEmitters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bDisplayAdvancedParameterPanelCategories_MetaData[] = {
		{ "Category", "Niagara" },
		{ "Comment", "/** Whether or not to display advanced categories for the parameter panel. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Whether or not to display advanced categories for the parameter panel." },
	};
#endif
	void Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bDisplayAdvancedParameterPanelCategories_SetBit(void* Obj)
	{
		((UNiagaraEditorSettings*)Obj)->bDisplayAdvancedParameterPanelCategories = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bDisplayAdvancedParameterPanelCategories = { "bDisplayAdvancedParameterPanelCategories", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraEditorSettings), &Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bDisplayAdvancedParameterPanelCategories_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bDisplayAdvancedParameterPanelCategories_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bDisplayAdvancedParameterPanelCategories_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_PlaybackSpeeds_Inner = { "PlaybackSpeeds", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_PlaybackSpeeds_MetaData[] = {
		{ "Category", "Niagara" },
		{ "Comment", "/** Speeds used for slowing down and speeding up the playback speeds */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
		{ "ToolTip", "Speeds used for slowing down and speeding up the playback speeds" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_PlaybackSpeeds = { "PlaybackSpeeds", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, PlaybackSpeeds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_PlaybackSpeeds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_PlaybackSpeeds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_ActionColors_MetaData[] = {
		{ "Category", "Niagara Colors" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_ActionColors = { "ActionColors", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, ActionColors), Z_Construct_UScriptStruct_FNiagaraActionColors, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_ActionColors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_ActionColors_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NewAssetDialogConfigMap_ValueProp = { "NewAssetDialogConfigMap", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NewAssetDialogConfigMap_Key_KeyProp = { "NewAssetDialogConfigMap_Key", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NewAssetDialogConfigMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NewAssetDialogConfigMap = { "NewAssetDialogConfigMap", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, NewAssetDialogConfigMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NewAssetDialogConfigMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NewAssetDialogConfigMap_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_HLSLKeywordReplacements_ValueProp = { "HLSLKeywordReplacements", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_HLSLKeywordReplacements_Key_KeyProp = { "HLSLKeywordReplacements_Key", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_HLSLKeywordReplacements_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_HLSLKeywordReplacements = { "HLSLKeywordReplacements", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, HLSLKeywordReplacements), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_HLSLKeywordReplacements_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_HLSLKeywordReplacements_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceMetadata_Inner = { "NamespaceMetadata", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceMetadata_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceMetadata = { "NamespaceMetadata", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, NamespaceMetadata), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceMetadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceMetadata_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceModifierMetadata_Inner = { "NamespaceModifierMetadata", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceModifierMetadata_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceModifierMetadata = { "NamespaceModifierMetadata", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, NamespaceModifierMetadata), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceModifierMetadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceModifierMetadata_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultNamespaceMetadata_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultNamespaceMetadata = { "DefaultNamespaceMetadata", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, DefaultNamespaceMetadata), Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultNamespaceMetadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultNamespaceMetadata_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultNamespaceModifierMetadata_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultNamespaceModifierMetadata = { "DefaultNamespaceModifierMetadata", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, DefaultNamespaceModifierMetadata), Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultNamespaceModifierMetadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultNamespaceModifierMetadata_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_CurveTemplates_Inner = { "CurveTemplates", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraCurveTemplate, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_CurveTemplates_MetaData[] = {
		{ "Category", "Niagara" },
		{ "ModuleRelativePath", "Public/NiagaraEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_CurveTemplates = { "CurveTemplates", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorSettings, CurveTemplates), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_CurveTemplates_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_CurveTemplates_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultScript,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultDynamicInputScript,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultFunctionScript,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultModuleScript,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_RequiredSystemUpdateScript,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_GraphCreationShortcuts_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_GraphCreationShortcuts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bEnableBaker,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoCompile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bAutoPlay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetSimulationOnChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResimulateOnChangeWhilePaused,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bResetDependentSystemsWhenEditingEmitters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_bDisplayAdvancedParameterPanelCategories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_PlaybackSpeeds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_PlaybackSpeeds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_ActionColors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NewAssetDialogConfigMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NewAssetDialogConfigMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NewAssetDialogConfigMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_HLSLKeywordReplacements_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_HLSLKeywordReplacements_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_HLSLKeywordReplacements,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceMetadata_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceMetadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceModifierMetadata_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_NamespaceModifierMetadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultNamespaceMetadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_DefaultNamespaceModifierMetadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_CurveTemplates_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorSettings_Statics::NewProp_CurveTemplates,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraEditorSettings_Statics::ClassParams = {
		&UNiagaraEditorSettings::StaticClass,
		"Niagara",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraEditorSettings, 424905855);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraEditorSettings>()
	{
		return UNiagaraEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraEditorSettings(Z_Construct_UClass_UNiagaraEditorSettings, &UNiagaraEditorSettings::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
