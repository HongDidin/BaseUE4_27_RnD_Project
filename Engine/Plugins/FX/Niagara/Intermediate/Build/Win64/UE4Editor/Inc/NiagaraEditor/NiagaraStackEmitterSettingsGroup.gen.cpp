// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackEmitterSettingsGroup.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackEmitterSettingsGroup() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackItem();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackObject_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackItemGroup();
// End Cross Module References
	void UNiagaraStackEmitterPropertiesItem::StaticRegisterNativesUNiagaraStackEmitterPropertiesItem()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_NoRegister()
	{
		return UNiagaraStackEmitterPropertiesItem::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmitterObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EmitterObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackItem,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackEmitterSettingsGroup.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackEmitterSettingsGroup.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::NewProp_EmitterObject_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackEmitterSettingsGroup.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::NewProp_EmitterObject = { "EmitterObject", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackEmitterPropertiesItem, EmitterObject), Z_Construct_UClass_UNiagaraStackObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::NewProp_EmitterObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::NewProp_EmitterObject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::NewProp_EmitterObject,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackEmitterPropertiesItem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::ClassParams = {
		&UNiagaraStackEmitterPropertiesItem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackEmitterPropertiesItem, 3008329177);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackEmitterPropertiesItem>()
	{
		return UNiagaraStackEmitterPropertiesItem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackEmitterPropertiesItem(Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem, &UNiagaraStackEmitterPropertiesItem::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackEmitterPropertiesItem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackEmitterPropertiesItem);
	void UNiagaraStackEmitterSettingsGroup::StaticRegisterNativesUNiagaraStackEmitterSettingsGroup()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_NoRegister()
	{
		return UNiagaraStackEmitterSettingsGroup::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertiesItem_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PropertiesItem;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackItemGroup,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackEmitterSettingsGroup.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackEmitterSettingsGroup.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::NewProp_PropertiesItem_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackEmitterSettingsGroup.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::NewProp_PropertiesItem = { "PropertiesItem", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackEmitterSettingsGroup, PropertiesItem), Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::NewProp_PropertiesItem_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::NewProp_PropertiesItem_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::NewProp_PropertiesItem,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackEmitterSettingsGroup>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::ClassParams = {
		&UNiagaraStackEmitterSettingsGroup::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackEmitterSettingsGroup, 3956822426);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackEmitterSettingsGroup>()
	{
		return UNiagaraStackEmitterSettingsGroup::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackEmitterSettingsGroup(Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup, &UNiagaraStackEmitterSettingsGroup::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackEmitterSettingsGroup"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackEmitterSettingsGroup);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
