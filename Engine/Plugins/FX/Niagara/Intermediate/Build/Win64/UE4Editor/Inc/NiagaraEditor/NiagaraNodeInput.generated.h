// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraNodeInput_generated_h
#error "NiagaraNodeInput.generated.h already included, missing '#pragma once' in NiagaraNodeInput.h"
#endif
#define NIAGARAEDITOR_NiagaraNodeInput_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraInputExposureOptions_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraInputExposureOptions>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraNodeInput(); \
	friend struct Z_Construct_UClass_UNiagaraNodeInput_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeInput, UNiagaraNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeInput)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraNodeInput(); \
	friend struct Z_Construct_UClass_UNiagaraNodeInput_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeInput, UNiagaraNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeInput)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeInput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeInput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeInput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeInput(UNiagaraNodeInput&&); \
	NIAGARAEDITOR_API UNiagaraNodeInput(const UNiagaraNodeInput&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeInput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeInput(UNiagaraNodeInput&&); \
	NIAGARAEDITOR_API UNiagaraNodeInput(const UNiagaraNodeInput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeInput); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeInput)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DataInterface() { return STRUCT_OFFSET(UNiagaraNodeInput, DataInterface); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_46_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h_49_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraNodeInput."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraNodeInput>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeInput_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
