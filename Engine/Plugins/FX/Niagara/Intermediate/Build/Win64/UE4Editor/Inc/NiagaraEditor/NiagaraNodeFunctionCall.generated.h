// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraNodeFunctionCall_generated_h
#error "NiagaraNodeFunctionCall.generated.h already included, missing '#pragma once' in NiagaraNodeFunctionCall.h"
#endif
#define NIAGARAEDITOR_NiagaraNodeFunctionCall_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraPropagatedVariable_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraPropagatedVariable>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraNodeFunctionCall(); \
	friend struct Z_Construct_UClass_UNiagaraNodeFunctionCall_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeFunctionCall, UNiagaraNodeWithDynamicPins, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeFunctionCall)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraNodeFunctionCall(); \
	friend struct Z_Construct_UClass_UNiagaraNodeFunctionCall_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeFunctionCall, UNiagaraNodeWithDynamicPins, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeFunctionCall)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeFunctionCall(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeFunctionCall) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeFunctionCall); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeFunctionCall); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeFunctionCall(UNiagaraNodeFunctionCall&&); \
	NIAGARAEDITOR_API UNiagaraNodeFunctionCall(const UNiagaraNodeFunctionCall&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeFunctionCall(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeFunctionCall(UNiagaraNodeFunctionCall&&); \
	NIAGARAEDITOR_API UNiagaraNodeFunctionCall(const UNiagaraNodeFunctionCall&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeFunctionCall); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeFunctionCall); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeFunctionCall)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CachedChangeId() { return STRUCT_OFFSET(UNiagaraNodeFunctionCall, CachedChangeId); } \
	FORCEINLINE static uint32 __PPO__InvalidScriptVersionReference() { return STRUCT_OFFSET(UNiagaraNodeFunctionCall, InvalidScriptVersionReference); } \
	FORCEINLINE static uint32 __PPO__FunctionDisplayName() { return STRUCT_OFFSET(UNiagaraNodeFunctionCall, FunctionDisplayName); } \
	FORCEINLINE static uint32 __PPO__MessageKeyToMessageMap() { return STRUCT_OFFSET(UNiagaraNodeFunctionCall, MessageKeyToMessageMap); } \
	FORCEINLINE static uint32 __PPO__StackMessages() { return STRUCT_OFFSET(UNiagaraNodeFunctionCall, StackMessages); } \
	FORCEINLINE static uint32 __PPO__BoundPinNames() { return STRUCT_OFFSET(UNiagaraNodeFunctionCall, BoundPinNames); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_52_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h_55_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraNodeFunctionCall>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeFunctionCall_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
