// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraHlslTranslator.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraHlslTranslator() {}
// Cross Module References
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraDataSetAccessMode();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	static UEnum* ENiagaraDataSetAccessMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraDataSetAccessMode, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraDataSetAccessMode"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraDataSetAccessMode>()
	{
		return ENiagaraDataSetAccessMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraDataSetAccessMode(ENiagaraDataSetAccessMode_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraDataSetAccessMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraDataSetAccessMode_Hash() { return 907221433U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraDataSetAccessMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraDataSetAccessMode"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraDataSetAccessMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraDataSetAccessMode::AppendConsume", (int64)ENiagaraDataSetAccessMode::AppendConsume },
				{ "ENiagaraDataSetAccessMode::Direct", (int64)ENiagaraDataSetAccessMode::Direct },
				{ "ENiagaraDataSetAccessMode::Num", (int64)ENiagaraDataSetAccessMode::Num },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AppendConsume.Comment", "/** Data set reads and writes use shared counters to add and remove the end of available data. Writes are conditional and read */" },
				{ "AppendConsume.Name", "ENiagaraDataSetAccessMode::AppendConsume" },
				{ "AppendConsume.ToolTip", "Data set reads and writes use shared counters to add and remove the end of available data. Writes are conditional and read" },
				{ "Direct.Comment", "/** Data set is accessed directly at a specific index. */" },
				{ "Direct.Name", "ENiagaraDataSetAccessMode::Direct" },
				{ "Direct.ToolTip", "Data set is accessed directly at a specific index." },
				{ "ModuleRelativePath", "Private/NiagaraHlslTranslator.h" },
				{ "Num.Hidden", "" },
				{ "Num.Name", "ENiagaraDataSetAccessMode::Num" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraDataSetAccessMode",
				"ENiagaraDataSetAccessMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
