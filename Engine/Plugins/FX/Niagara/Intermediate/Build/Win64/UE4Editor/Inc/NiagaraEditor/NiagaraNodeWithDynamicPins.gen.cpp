// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraNodeWithDynamicPins.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeWithDynamicPins() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeWithDynamicPins_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeWithDynamicPins();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNode();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraNodeWithDynamicPins::StaticRegisterNativesUNiagaraNodeWithDynamicPins()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeWithDynamicPins_NoRegister()
	{
		return UNiagaraNodeWithDynamicPins::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeWithDynamicPins_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeWithDynamicPins_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNode,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeWithDynamicPins_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A base node for niagara nodes with pins which can be dynamically added and removed by the user. */" },
		{ "IncludePath", "NiagaraNodeWithDynamicPins.h" },
		{ "ModuleRelativePath", "Public/NiagaraNodeWithDynamicPins.h" },
		{ "ToolTip", "A base node for niagara nodes with pins which can be dynamically added and removed by the user." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeWithDynamicPins_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeWithDynamicPins>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeWithDynamicPins_Statics::ClassParams = {
		&UNiagaraNodeWithDynamicPins::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeWithDynamicPins_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeWithDynamicPins_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeWithDynamicPins()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeWithDynamicPins_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeWithDynamicPins, 1951830931);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeWithDynamicPins>()
	{
		return UNiagaraNodeWithDynamicPins::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeWithDynamicPins(Z_Construct_UClass_UNiagaraNodeWithDynamicPins, &UNiagaraNodeWithDynamicPins::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeWithDynamicPins"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeWithDynamicPins);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
