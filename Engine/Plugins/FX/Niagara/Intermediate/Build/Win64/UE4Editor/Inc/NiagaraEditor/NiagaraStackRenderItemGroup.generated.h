// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackRenderItemGroup_generated_h
#error "NiagaraStackRenderItemGroup.generated.h already included, missing '#pragma once' in NiagaraStackRenderItemGroup.h"
#endif
#define NIAGARAEDITOR_NiagaraStackRenderItemGroup_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackRenderItemGroup(); \
	friend struct Z_Construct_UClass_UNiagaraStackRenderItemGroup_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackRenderItemGroup, UNiagaraStackItemGroup, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackRenderItemGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackRenderItemGroup(); \
	friend struct Z_Construct_UClass_UNiagaraStackRenderItemGroup_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackRenderItemGroup, UNiagaraStackItemGroup, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackRenderItemGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackRenderItemGroup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackRenderItemGroup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackRenderItemGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackRenderItemGroup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackRenderItemGroup(UNiagaraStackRenderItemGroup&&); \
	NO_API UNiagaraStackRenderItemGroup(const UNiagaraStackRenderItemGroup&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackRenderItemGroup() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackRenderItemGroup(UNiagaraStackRenderItemGroup&&); \
	NO_API UNiagaraStackRenderItemGroup(const UNiagaraStackRenderItemGroup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackRenderItemGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackRenderItemGroup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackRenderItemGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_12_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackRenderItemGroup>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackRenderItemGroup_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
