// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraThumbnailRenderer_generated_h
#error "NiagaraThumbnailRenderer.generated.h already included, missing '#pragma once' in NiagaraThumbnailRenderer.h"
#endif
#define NIAGARAEDITOR_NiagaraThumbnailRenderer_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraThumbnailRendererBase(); \
	friend struct Z_Construct_UClass_UNiagaraThumbnailRendererBase_Statics; \
public: \
	DECLARE_CLASS(UNiagaraThumbnailRendererBase, UTextureThumbnailRenderer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraThumbnailRendererBase)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraThumbnailRendererBase(); \
	friend struct Z_Construct_UClass_UNiagaraThumbnailRendererBase_Statics; \
public: \
	DECLARE_CLASS(UNiagaraThumbnailRendererBase, UTextureThumbnailRenderer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraThumbnailRendererBase)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraThumbnailRendererBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraThumbnailRendererBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraThumbnailRendererBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraThumbnailRendererBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraThumbnailRendererBase(UNiagaraThumbnailRendererBase&&); \
	NO_API UNiagaraThumbnailRendererBase(const UNiagaraThumbnailRendererBase&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraThumbnailRendererBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraThumbnailRendererBase(UNiagaraThumbnailRendererBase&&); \
	NO_API UNiagaraThumbnailRendererBase(const UNiagaraThumbnailRendererBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraThumbnailRendererBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraThumbnailRendererBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraThumbnailRendererBase)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_10_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraThumbnailRendererBase>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraEmitterThumbnailRenderer(); \
	friend struct Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_Statics; \
public: \
	DECLARE_CLASS(UNiagaraEmitterThumbnailRenderer, UNiagaraThumbnailRendererBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraEmitterThumbnailRenderer)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraEmitterThumbnailRenderer(); \
	friend struct Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_Statics; \
public: \
	DECLARE_CLASS(UNiagaraEmitterThumbnailRenderer, UNiagaraThumbnailRendererBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraEmitterThumbnailRenderer)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraEmitterThumbnailRenderer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraEmitterThumbnailRenderer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraEmitterThumbnailRenderer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraEmitterThumbnailRenderer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraEmitterThumbnailRenderer(UNiagaraEmitterThumbnailRenderer&&); \
	NO_API UNiagaraEmitterThumbnailRenderer(const UNiagaraEmitterThumbnailRenderer&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraEmitterThumbnailRenderer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraEmitterThumbnailRenderer(UNiagaraEmitterThumbnailRenderer&&); \
	NO_API UNiagaraEmitterThumbnailRenderer(const UNiagaraEmitterThumbnailRenderer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraEmitterThumbnailRenderer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraEmitterThumbnailRenderer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraEmitterThumbnailRenderer)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_25_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraEmitterThumbnailRenderer>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraSystemThumbnailRenderer(); \
	friend struct Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSystemThumbnailRenderer, UNiagaraThumbnailRendererBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSystemThumbnailRenderer)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraSystemThumbnailRenderer(); \
	friend struct Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSystemThumbnailRenderer, UNiagaraThumbnailRendererBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSystemThumbnailRenderer)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSystemThumbnailRenderer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSystemThumbnailRenderer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSystemThumbnailRenderer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSystemThumbnailRenderer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSystemThumbnailRenderer(UNiagaraSystemThumbnailRenderer&&); \
	NO_API UNiagaraSystemThumbnailRenderer(const UNiagaraSystemThumbnailRenderer&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSystemThumbnailRenderer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSystemThumbnailRenderer(UNiagaraSystemThumbnailRenderer&&); \
	NO_API UNiagaraSystemThumbnailRenderer(const UNiagaraSystemThumbnailRenderer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSystemThumbnailRenderer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSystemThumbnailRenderer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSystemThumbnailRenderer)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_34_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraSystemThumbnailRenderer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraThumbnailRenderer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
