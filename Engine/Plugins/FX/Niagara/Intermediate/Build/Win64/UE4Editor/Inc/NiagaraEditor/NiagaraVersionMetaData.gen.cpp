// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraVersionMetaData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraVersionMetaData() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraVersionMetaData_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraVersionMetaData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	NIAGARA_API UEnum* Z_Construct_UEnum_Niagara_ENiagaraPythonUpdateScriptReference();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FFilePath();
// End Cross Module References
	void UNiagaraVersionMetaData::StaticRegisterNativesUNiagaraVersionMetaData()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraVersionMetaData_NoRegister()
	{
		return UNiagaraVersionMetaData::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraVersionMetaData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsExposedVersion_MetaData[];
#endif
		static void NewProp_bIsExposedVersion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsExposedVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChangeDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ChangeDescription;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsVisibleInVersionSelector_MetaData[];
#endif
		static void NewProp_bIsVisibleInVersionSelector_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsVisibleInVersionSelector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VersionGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VersionGuid;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_UpdateScriptExecution_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpdateScriptExecution_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_UpdateScriptExecution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PythonUpdateScript_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PythonUpdateScript;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScriptAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ScriptAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraVersionMetaData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraVersionMetaData_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraVersionMetaData.h" },
		{ "ModuleRelativePath", "Private/NiagaraVersionMetaData.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsExposedVersion_MetaData[] = {
		{ "Category", "Version Details" },
		{ "Comment", "/** If true then this version is exposed to the user and is used as the default version for new assets. */" },
		{ "EditCondition", "!bIsExposedVersion" },
		{ "ModuleRelativePath", "Private/NiagaraVersionMetaData.h" },
		{ "ToolTip", "If true then this version is exposed to the user and is used as the default version for new assets." },
	};
#endif
	void Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsExposedVersion_SetBit(void* Obj)
	{
		((UNiagaraVersionMetaData*)Obj)->bIsExposedVersion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsExposedVersion = { "bIsExposedVersion", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraVersionMetaData), &Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsExposedVersion_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsExposedVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsExposedVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_ChangeDescription_MetaData[] = {
		{ "Category", "Version Details" },
		{ "Comment", "/** Changelist displayed to the user when upgrading to a new script version. */" },
		{ "ModuleRelativePath", "Private/NiagaraVersionMetaData.h" },
		{ "MultiLine", "TRUE" },
		{ "ToolTip", "Changelist displayed to the user when upgrading to a new script version." },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_ChangeDescription = { "ChangeDescription", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraVersionMetaData, ChangeDescription), METADATA_PARAMS(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_ChangeDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_ChangeDescription_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsVisibleInVersionSelector_MetaData[] = {
		{ "Category", "Version Details" },
		{ "Comment", "/** If false then this version is not visible in the version selector dropdown menu of the stack. This is useful to hide work in progress versions without removing the module from the search menu.\n\x09 *  The exposed version is always visible to users. */" },
		{ "EditCondition", "!bIsExposedVersion" },
		{ "ModuleRelativePath", "Private/NiagaraVersionMetaData.h" },
		{ "ToolTip", "If false then this version is not visible in the version selector dropdown menu of the stack. This is useful to hide work in progress versions without removing the module from the search menu.\nThe exposed version is always visible to users." },
	};
#endif
	void Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsVisibleInVersionSelector_SetBit(void* Obj)
	{
		((UNiagaraVersionMetaData*)Obj)->bIsVisibleInVersionSelector = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsVisibleInVersionSelector = { "bIsVisibleInVersionSelector", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraVersionMetaData), &Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsVisibleInVersionSelector_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsVisibleInVersionSelector_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsVisibleInVersionSelector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_VersionGuid_MetaData[] = {
		{ "Category", "Version Details" },
		{ "Comment", "/** Internal version guid, mainly useful for debugging version conflicts. */" },
		{ "ModuleRelativePath", "Private/NiagaraVersionMetaData.h" },
		{ "ToolTip", "Internal version guid, mainly useful for debugging version conflicts." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_VersionGuid = { "VersionGuid", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraVersionMetaData, VersionGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_VersionGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_VersionGuid_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_UpdateScriptExecution_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_UpdateScriptExecution_MetaData[] = {
		{ "Category", "Scripting" },
		{ "Comment", "/** Reference to a python script that is executed when the user updates from a previous version to this version. */" },
		{ "ModuleRelativePath", "Private/NiagaraVersionMetaData.h" },
		{ "ToolTip", "Reference to a python script that is executed when the user updates from a previous version to this version." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_UpdateScriptExecution = { "UpdateScriptExecution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraVersionMetaData, UpdateScriptExecution), Z_Construct_UEnum_Niagara_ENiagaraPythonUpdateScriptReference, METADATA_PARAMS(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_UpdateScriptExecution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_UpdateScriptExecution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_PythonUpdateScript_MetaData[] = {
		{ "Category", "Scripting" },
		{ "Comment", "/** Python script to run when updating to this script version. */" },
		{ "EditCondition", "UpdateScriptExecution == ENiagaraPythonUpdateScriptReference::DirectTextEntry" },
		{ "ModuleRelativePath", "Private/NiagaraVersionMetaData.h" },
		{ "MultiLine", "TRUE" },
		{ "ToolTip", "Python script to run when updating to this script version." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_PythonUpdateScript = { "PythonUpdateScript", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraVersionMetaData, PythonUpdateScript), METADATA_PARAMS(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_PythonUpdateScript_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_PythonUpdateScript_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_ScriptAsset_MetaData[] = {
		{ "Category", "Scripting" },
		{ "Comment", "/** Asset reference to a python script to run when updating to this script version. */" },
		{ "EditCondition", "UpdateScriptExecution == ENiagaraPythonUpdateScriptReference::ScriptAsset" },
		{ "ModuleRelativePath", "Private/NiagaraVersionMetaData.h" },
		{ "ToolTip", "Asset reference to a python script to run when updating to this script version." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_ScriptAsset = { "ScriptAsset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraVersionMetaData, ScriptAsset), Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_ScriptAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_ScriptAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraVersionMetaData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsExposedVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_ChangeDescription,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_bIsVisibleInVersionSelector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_VersionGuid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_UpdateScriptExecution_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_UpdateScriptExecution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_PythonUpdateScript,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraVersionMetaData_Statics::NewProp_ScriptAsset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraVersionMetaData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraVersionMetaData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraVersionMetaData_Statics::ClassParams = {
		&UNiagaraVersionMetaData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraVersionMetaData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraVersionMetaData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraVersionMetaData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraVersionMetaData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraVersionMetaData, 2494167850);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraVersionMetaData>()
	{
		return UNiagaraVersionMetaData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraVersionMetaData(Z_Construct_UClass_UNiagaraVersionMetaData, &UNiagaraVersionMetaData::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraVersionMetaData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraVersionMetaData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
