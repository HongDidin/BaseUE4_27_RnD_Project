// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/Sequencer/NiagaraSequence/NiagaraSequencerFilters.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraSequencerFilters() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraSequencerTrackFilter_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraSequencerTrackFilter();
	SEQUENCER_API UClass* Z_Construct_UClass_USequencerTrackFilterExtension();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraSequencerTrackFilter::StaticRegisterNativesUNiagaraSequencerTrackFilter()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraSequencerTrackFilter_NoRegister()
	{
		return UNiagaraSequencerTrackFilter::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraSequencerTrackFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraSequencerTrackFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USequencerTrackFilterExtension,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSequencerTrackFilter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sequencer/NiagaraSequence/NiagaraSequencerFilters.h" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/NiagaraSequencerFilters.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraSequencerTrackFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraSequencerTrackFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraSequencerTrackFilter_Statics::ClassParams = {
		&UNiagaraSequencerTrackFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraSequencerTrackFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSequencerTrackFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraSequencerTrackFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraSequencerTrackFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraSequencerTrackFilter, 3230603540);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraSequencerTrackFilter>()
	{
		return UNiagaraSequencerTrackFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraSequencerTrackFilter(Z_Construct_UClass_UNiagaraSequencerTrackFilter, &UNiagaraSequencerTrackFilter::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraSequencerTrackFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraSequencerTrackFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
