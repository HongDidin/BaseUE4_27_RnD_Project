// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraNodeAssignment_generated_h
#error "NiagaraNodeAssignment.generated.h already included, missing '#pragma once' in NiagaraNodeAssignment.h"
#endif
#define NIAGARAEDITOR_NiagaraNodeAssignment_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraNodeAssignment(); \
	friend struct Z_Construct_UClass_UNiagaraNodeAssignment_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeAssignment, UNiagaraNodeFunctionCall, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraNodeAssignment)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraNodeAssignment(); \
	friend struct Z_Construct_UClass_UNiagaraNodeAssignment_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeAssignment, UNiagaraNodeFunctionCall, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraNodeAssignment)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraNodeAssignment(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeAssignment) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraNodeAssignment); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeAssignment); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraNodeAssignment(UNiagaraNodeAssignment&&); \
	NO_API UNiagaraNodeAssignment(const UNiagaraNodeAssignment&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraNodeAssignment(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraNodeAssignment(UNiagaraNodeAssignment&&); \
	NO_API UNiagaraNodeAssignment(const UNiagaraNodeAssignment&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraNodeAssignment); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeAssignment); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeAssignment)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AssignmentTarget_DEPRECATED() { return STRUCT_OFFSET(UNiagaraNodeAssignment, AssignmentTarget_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__AssignmentDefaultValue_DEPRECATED() { return STRUCT_OFFSET(UNiagaraNodeAssignment, AssignmentDefaultValue_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__AssignmentTargets() { return STRUCT_OFFSET(UNiagaraNodeAssignment, AssignmentTargets); } \
	FORCEINLINE static uint32 __PPO__AssignmentDefaultValues() { return STRUCT_OFFSET(UNiagaraNodeAssignment, AssignmentDefaultValues); } \
	FORCEINLINE static uint32 __PPO__OldFunctionCallName() { return STRUCT_OFFSET(UNiagaraNodeAssignment, OldFunctionCallName); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_14_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraNodeAssignment>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeAssignment_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
