// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraEditorSettings_generated_h
#error "NiagaraEditorSettings.generated.h already included, missing '#pragma once' in NiagaraEditorSettings.h"
#endif
#define NIAGARAEDITOR_NiagaraEditorSettings_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_197_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraActionColors_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraActionColors>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_185_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraCurveTemplate_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraCurveTemplate>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_66_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraNamespaceMetadata_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Guid() { return STRUCT_OFFSET(FNiagaraNamespaceMetadata, Guid); }


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraNamespaceMetadata>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_35_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraNewAssetDialogConfig_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraNewAssetDialogConfig>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraEditorSettings(); \
	friend struct Z_Construct_UClass_UNiagaraEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UNiagaraEditorSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Niagara");} \



#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraEditorSettings(); \
	friend struct Z_Construct_UClass_UNiagaraEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UNiagaraEditorSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Niagara");} \



#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraEditorSettings(UNiagaraEditorSettings&&); \
	NO_API UNiagaraEditorSettings(const UNiagaraEditorSettings&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraEditorSettings(UNiagaraEditorSettings&&); \
	NO_API UNiagaraEditorSettings(const UNiagaraEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraEditorSettings)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bAutoCompile() { return STRUCT_OFFSET(UNiagaraEditorSettings, bAutoCompile); } \
	FORCEINLINE static uint32 __PPO__bAutoPlay() { return STRUCT_OFFSET(UNiagaraEditorSettings, bAutoPlay); } \
	FORCEINLINE static uint32 __PPO__bResetSimulationOnChange() { return STRUCT_OFFSET(UNiagaraEditorSettings, bResetSimulationOnChange); } \
	FORCEINLINE static uint32 __PPO__bResimulateOnChangeWhilePaused() { return STRUCT_OFFSET(UNiagaraEditorSettings, bResimulateOnChangeWhilePaused); } \
	FORCEINLINE static uint32 __PPO__bResetDependentSystemsWhenEditingEmitters() { return STRUCT_OFFSET(UNiagaraEditorSettings, bResetDependentSystemsWhenEditingEmitters); } \
	FORCEINLINE static uint32 __PPO__bDisplayAdvancedParameterPanelCategories() { return STRUCT_OFFSET(UNiagaraEditorSettings, bDisplayAdvancedParameterPanelCategories); } \
	FORCEINLINE static uint32 __PPO__PlaybackSpeeds() { return STRUCT_OFFSET(UNiagaraEditorSettings, PlaybackSpeeds); } \
	FORCEINLINE static uint32 __PPO__ActionColors() { return STRUCT_OFFSET(UNiagaraEditorSettings, ActionColors); } \
	FORCEINLINE static uint32 __PPO__NewAssetDialogConfigMap() { return STRUCT_OFFSET(UNiagaraEditorSettings, NewAssetDialogConfigMap); } \
	FORCEINLINE static uint32 __PPO__HLSLKeywordReplacements() { return STRUCT_OFFSET(UNiagaraEditorSettings, HLSLKeywordReplacements); } \
	FORCEINLINE static uint32 __PPO__NamespaceMetadata() { return STRUCT_OFFSET(UNiagaraEditorSettings, NamespaceMetadata); } \
	FORCEINLINE static uint32 __PPO__NamespaceModifierMetadata() { return STRUCT_OFFSET(UNiagaraEditorSettings, NamespaceModifierMetadata); } \
	FORCEINLINE static uint32 __PPO__DefaultNamespaceMetadata() { return STRUCT_OFFSET(UNiagaraEditorSettings, DefaultNamespaceMetadata); } \
	FORCEINLINE static uint32 __PPO__DefaultNamespaceModifierMetadata() { return STRUCT_OFFSET(UNiagaraEditorSettings, DefaultNamespaceModifierMetadata); } \
	FORCEINLINE static uint32 __PPO__CurveTemplates() { return STRUCT_OFFSET(UNiagaraEditorSettings, CurveTemplates); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_219_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h_223_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraEditorSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraEditorSettings_h


#define FOREACH_ENUM_ENIAGARANAMESPACEMETADATAOPTIONS(op) \
	op(ENiagaraNamespaceMetadataOptions::HideInScript) \
	op(ENiagaraNamespaceMetadataOptions::HideInSystem) \
	op(ENiagaraNamespaceMetadataOptions::AdvancedInScript) \
	op(ENiagaraNamespaceMetadataOptions::AdvancedInSystem) \
	op(ENiagaraNamespaceMetadataOptions::PreventEditingNamespace) \
	op(ENiagaraNamespaceMetadataOptions::PreventEditingNamespaceModifier) \
	op(ENiagaraNamespaceMetadataOptions::PreventEditingName) \
	op(ENiagaraNamespaceMetadataOptions::PreventCreatingInSystemEditor) 

enum class ENiagaraNamespaceMetadataOptions;
template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraNamespaceMetadataOptions>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
