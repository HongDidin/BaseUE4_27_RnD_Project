// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraEmitterEditorData_generated_h
#error "NiagaraEmitterEditorData.generated.h already included, missing '#pragma once' in NiagaraEmitterEditorData.h"
#endif
#define NIAGARAEDITOR_NiagaraEmitterEditorData_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraEmitterEditorData(); \
	friend struct Z_Construct_UClass_UNiagaraEmitterEditorData_Statics; \
public: \
	DECLARE_CLASS(UNiagaraEmitterEditorData, UNiagaraEditorDataBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraEmitterEditorData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraEmitterEditorData(); \
	friend struct Z_Construct_UClass_UNiagaraEmitterEditorData_Statics; \
public: \
	DECLARE_CLASS(UNiagaraEmitterEditorData, UNiagaraEditorDataBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraEmitterEditorData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraEmitterEditorData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraEmitterEditorData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraEmitterEditorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraEmitterEditorData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraEmitterEditorData(UNiagaraEmitterEditorData&&); \
	NO_API UNiagaraEmitterEditorData(const UNiagaraEmitterEditorData&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraEmitterEditorData(UNiagaraEmitterEditorData&&); \
	NO_API UNiagaraEmitterEditorData(const UNiagaraEmitterEditorData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraEmitterEditorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraEmitterEditorData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraEmitterEditorData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StackEditorData() { return STRUCT_OFFSET(UNiagaraEmitterEditorData, StackEditorData); } \
	FORCEINLINE static uint32 __PPO__PlaybackRangeMin() { return STRUCT_OFFSET(UNiagaraEmitterEditorData, PlaybackRangeMin); } \
	FORCEINLINE static uint32 __PPO__PlaybackRangeMax() { return STRUCT_OFFSET(UNiagaraEmitterEditorData, PlaybackRangeMax); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_12_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraEmitterEditorData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraEmitterEditorData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
