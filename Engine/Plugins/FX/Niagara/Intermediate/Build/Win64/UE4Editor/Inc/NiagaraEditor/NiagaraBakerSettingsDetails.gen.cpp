// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/Customizations/NiagaraBakerSettingsDetails.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraBakerSettingsDetails() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphSchemaAction();
// End Cross Module References

static_assert(std::is_polymorphic<FNiagaraBakerTextureSourceAction>() == std::is_polymorphic<FEdGraphSchemaAction>(), "USTRUCT FNiagaraBakerTextureSourceAction cannot be polymorphic unless super FEdGraphSchemaAction is polymorphic");

class UScriptStruct* FNiagaraBakerTextureSourceAction::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraBakerTextureSourceAction"), sizeof(FNiagaraBakerTextureSourceAction), Get_Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraBakerTextureSourceAction>()
{
	return FNiagaraBakerTextureSourceAction::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraBakerTextureSourceAction(FNiagaraBakerTextureSourceAction::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraBakerTextureSourceAction"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraBakerTextureSourceAction
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraBakerTextureSourceAction()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraBakerTextureSourceAction>(FName(TEXT("NiagaraBakerTextureSourceAction")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraBakerTextureSourceAction;
	struct Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Customizations/NiagaraBakerSettingsDetails.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraBakerTextureSourceAction>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		Z_Construct_UScriptStruct_FEdGraphSchemaAction,
		&NewStructOps,
		"NiagaraBakerTextureSourceAction",
		sizeof(FNiagaraBakerTextureSourceAction),
		alignof(FNiagaraBakerTextureSourceAction),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraBakerTextureSourceAction"), sizeof(FNiagaraBakerTextureSourceAction), Get_Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraBakerTextureSourceAction_Hash() { return 4262372257U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
