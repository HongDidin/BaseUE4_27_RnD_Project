// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraOverviewNode_generated_h
#error "NiagaraOverviewNode.generated.h already included, missing '#pragma once' in NiagaraOverviewNode.h"
#endif
#define NIAGARAEDITOR_NiagaraOverviewNode_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraOverviewNode(); \
	friend struct Z_Construct_UClass_UNiagaraOverviewNode_Statics; \
public: \
	DECLARE_CLASS(UNiagaraOverviewNode, UEdGraphNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraOverviewNode)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraOverviewNode(); \
	friend struct Z_Construct_UClass_UNiagaraOverviewNode_Statics; \
public: \
	DECLARE_CLASS(UNiagaraOverviewNode, UEdGraphNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraOverviewNode)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraOverviewNode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraOverviewNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraOverviewNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraOverviewNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraOverviewNode(UNiagaraOverviewNode&&); \
	NO_API UNiagaraOverviewNode(const UNiagaraOverviewNode&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraOverviewNode(UNiagaraOverviewNode&&); \
	NO_API UNiagaraOverviewNode(const UNiagaraOverviewNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraOverviewNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraOverviewNode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraOverviewNode)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OwningSystem() { return STRUCT_OFFSET(UNiagaraOverviewNode, OwningSystem); } \
	FORCEINLINE static uint32 __PPO__EmitterHandleGuid() { return STRUCT_OFFSET(UNiagaraOverviewNode, EmitterHandleGuid); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_10_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraOverviewNode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraOverviewNode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
