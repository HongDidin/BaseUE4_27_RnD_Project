// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackParameterStoreEntry_generated_h
#error "NiagaraStackParameterStoreEntry.generated.h already included, missing '#pragma once' in NiagaraStackParameterStoreEntry.h"
#endif
#define NIAGARAEDITOR_NiagaraStackParameterStoreEntry_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackParameterStoreEntry(); \
	friend struct Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackParameterStoreEntry, UNiagaraStackItemContent, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackParameterStoreEntry)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackParameterStoreEntry(); \
	friend struct Z_Construct_UClass_UNiagaraStackParameterStoreEntry_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackParameterStoreEntry, UNiagaraStackItemContent, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackParameterStoreEntry)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackParameterStoreEntry(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackParameterStoreEntry) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackParameterStoreEntry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackParameterStoreEntry); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackParameterStoreEntry(UNiagaraStackParameterStoreEntry&&); \
	NO_API UNiagaraStackParameterStoreEntry(const UNiagaraStackParameterStoreEntry&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackParameterStoreEntry(UNiagaraStackParameterStoreEntry&&); \
	NO_API UNiagaraStackParameterStoreEntry(const UNiagaraStackParameterStoreEntry&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackParameterStoreEntry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackParameterStoreEntry); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackParameterStoreEntry)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ValueObjectEntry() { return STRUCT_OFFSET(UNiagaraStackParameterStoreEntry, ValueObjectEntry); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_20_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackParameterStoreEntry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackParameterStoreEntry_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
