// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraOverviewNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraOverviewNode() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraOverviewNode_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraOverviewNode();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphNode();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraSystem_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	void UNiagaraOverviewNode::StaticRegisterNativesUNiagaraOverviewNode()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraOverviewNode_NoRegister()
	{
		return UNiagaraOverviewNode::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraOverviewNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwningSystem_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwningSystem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmitterHandleGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EmitterHandleGuid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraOverviewNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraOverviewNode_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraOverviewNode.h" },
		{ "ModuleRelativePath", "Public/NiagaraOverviewNode.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraOverviewNode_Statics::NewProp_OwningSystem_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraOverviewNode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraOverviewNode_Statics::NewProp_OwningSystem = { "OwningSystem", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraOverviewNode, OwningSystem), Z_Construct_UClass_UNiagaraSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraOverviewNode_Statics::NewProp_OwningSystem_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraOverviewNode_Statics::NewProp_OwningSystem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraOverviewNode_Statics::NewProp_EmitterHandleGuid_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraOverviewNode.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraOverviewNode_Statics::NewProp_EmitterHandleGuid = { "EmitterHandleGuid", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraOverviewNode, EmitterHandleGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UNiagaraOverviewNode_Statics::NewProp_EmitterHandleGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraOverviewNode_Statics::NewProp_EmitterHandleGuid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraOverviewNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraOverviewNode_Statics::NewProp_OwningSystem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraOverviewNode_Statics::NewProp_EmitterHandleGuid,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraOverviewNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraOverviewNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraOverviewNode_Statics::ClassParams = {
		&UNiagaraOverviewNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraOverviewNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraOverviewNode_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraOverviewNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraOverviewNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraOverviewNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraOverviewNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraOverviewNode, 2649941422);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraOverviewNode>()
	{
		return UNiagaraOverviewNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraOverviewNode(Z_Construct_UClass_UNiagaraOverviewNode, &UNiagaraOverviewNode::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraOverviewNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraOverviewNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
