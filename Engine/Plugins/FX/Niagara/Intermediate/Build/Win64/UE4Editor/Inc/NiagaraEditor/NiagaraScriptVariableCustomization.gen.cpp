// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/Customizations/NiagaraScriptVariableCustomization.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraScriptVariableCustomization() {}
// Cross Module References
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraLibrarySourceDefaultMode();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraLibrarySynchronizedDefaultMode();
// End Cross Module References
	static UEnum* ENiagaraLibrarySourceDefaultMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraLibrarySourceDefaultMode, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraLibrarySourceDefaultMode"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraLibrarySourceDefaultMode>()
	{
		return ENiagaraLibrarySourceDefaultMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraLibrarySourceDefaultMode(ENiagaraLibrarySourceDefaultMode_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraLibrarySourceDefaultMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraLibrarySourceDefaultMode_Hash() { return 1974850782U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraLibrarySourceDefaultMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraLibrarySourceDefaultMode"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraLibrarySourceDefaultMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraLibrarySourceDefaultMode::Value", (int64)ENiagaraLibrarySourceDefaultMode::Value },
				{ "ENiagaraLibrarySourceDefaultMode::Binding", (int64)ENiagaraLibrarySourceDefaultMode::Binding },
				{ "ENiagaraLibrarySourceDefaultMode::FailIfPreviouslyNotSet", (int64)ENiagaraLibrarySourceDefaultMode::FailIfPreviouslyNotSet },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Binding.Comment", "// Default initialize using a dropdown widget in the Selected Details panel.\n" },
				{ "Binding.Name", "ENiagaraLibrarySourceDefaultMode::Binding" },
				{ "Binding.ToolTip", "Default initialize using a dropdown widget in the Selected Details panel." },
				{ "FailIfPreviouslyNotSet.Comment", "// Fail compilation if this value has not been set previously in the stack.\n" },
				{ "FailIfPreviouslyNotSet.Name", "ENiagaraLibrarySourceDefaultMode::FailIfPreviouslyNotSet" },
				{ "FailIfPreviouslyNotSet.ToolTip", "Fail compilation if this value has not been set previously in the stack." },
				{ "ModuleRelativePath", "Private/Customizations/NiagaraScriptVariableCustomization.h" },
				{ "Value.Comment", "// Default initialize using a value widget in the Selected Details panel.\n" },
				{ "Value.Name", "ENiagaraLibrarySourceDefaultMode::Value" },
				{ "Value.ToolTip", "Default initialize using a value widget in the Selected Details panel." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraLibrarySourceDefaultMode",
				"ENiagaraLibrarySourceDefaultMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENiagaraLibrarySynchronizedDefaultMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraLibrarySynchronizedDefaultMode, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraLibrarySynchronizedDefaultMode"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraLibrarySynchronizedDefaultMode>()
	{
		return ENiagaraLibrarySynchronizedDefaultMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraLibrarySynchronizedDefaultMode(ENiagaraLibrarySynchronizedDefaultMode_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraLibrarySynchronizedDefaultMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraLibrarySynchronizedDefaultMode_Hash() { return 1502575350U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraLibrarySynchronizedDefaultMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraLibrarySynchronizedDefaultMode"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraLibrarySynchronizedDefaultMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraLibrarySynchronizedDefaultMode::Definition", (int64)ENiagaraLibrarySynchronizedDefaultMode::Definition },
				{ "ENiagaraLibrarySynchronizedDefaultMode::Value", (int64)ENiagaraLibrarySynchronizedDefaultMode::Value },
				{ "ENiagaraLibrarySynchronizedDefaultMode::Binding", (int64)ENiagaraLibrarySynchronizedDefaultMode::Binding },
				{ "ENiagaraLibrarySynchronizedDefaultMode::Custom", (int64)ENiagaraLibrarySynchronizedDefaultMode::Custom },
				{ "ENiagaraLibrarySynchronizedDefaultMode::FailIfPreviouslyNotSet", (int64)ENiagaraLibrarySynchronizedDefaultMode::FailIfPreviouslyNotSet },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Binding.Comment", "// Default initialize using a dropdown widget in the Selected Details panel. Overrides the parameter definition default value.\n" },
				{ "Binding.Name", "ENiagaraLibrarySynchronizedDefaultMode::Binding" },
				{ "Binding.ToolTip", "Default initialize using a dropdown widget in the Selected Details panel. Overrides the parameter definition default value." },
				{ "Comment", "/** Intermediate representations for default mode set on parameter definition script variables. Maps to ENiagaraDefaultMode and bOverrideParameterDefinitionsDefaultValue of UNiagaraScriptVariable. */" },
				{ "Custom.Comment", "// Default initialization is done using a sub-graph. Overrides the parameter definition default value.\n" },
				{ "Custom.Name", "ENiagaraLibrarySynchronizedDefaultMode::Custom" },
				{ "Custom.ToolTip", "Default initialization is done using a sub-graph. Overrides the parameter definition default value." },
				{ "Definition.Comment", "// Synchronize with the default value as defined in the synchronized parameter definitions.\n" },
				{ "Definition.Name", "ENiagaraLibrarySynchronizedDefaultMode::Definition" },
				{ "Definition.ToolTip", "Synchronize with the default value as defined in the synchronized parameter definitions." },
				{ "FailIfPreviouslyNotSet.Comment", "// Fail compilation if this value has not been set previously in the stack. Overrides the parameter definition default value.\n" },
				{ "FailIfPreviouslyNotSet.Name", "ENiagaraLibrarySynchronizedDefaultMode::FailIfPreviouslyNotSet" },
				{ "FailIfPreviouslyNotSet.ToolTip", "Fail compilation if this value has not been set previously in the stack. Overrides the parameter definition default value." },
				{ "ModuleRelativePath", "Private/Customizations/NiagaraScriptVariableCustomization.h" },
				{ "ToolTip", "Intermediate representations for default mode set on parameter definition script variables. Maps to ENiagaraDefaultMode and bOverrideParameterDefinitionsDefaultValue of UNiagaraScriptVariable." },
				{ "Value.Comment", "// Default initialize using a value widget in the Selected Details panel. Overrides the parameter definition default value.\n" },
				{ "Value.Name", "ENiagaraLibrarySynchronizedDefaultMode::Value" },
				{ "Value.ToolTip", "Default initialize using a value widget in the Selected Details panel. Overrides the parameter definition default value." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraLibrarySynchronizedDefaultMode",
				"ENiagaraLibrarySynchronizedDefaultMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
