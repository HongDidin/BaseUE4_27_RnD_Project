// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackSimulationStageGroup.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackSimulationStageGroup() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackItem();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackObject_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackSimulationStageGroup_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackSimulationStageGroup();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackScriptItemGroup();
// End Cross Module References
	void UNiagaraStackSimulationStagePropertiesItem::StaticRegisterNativesUNiagaraStackSimulationStagePropertiesItem()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_NoRegister()
	{
		return UNiagaraStackSimulationStagePropertiesItem::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimulationStageObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SimulationStageObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackItem,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackSimulationStageGroup.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackSimulationStageGroup.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::NewProp_SimulationStageObject_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackSimulationStageGroup.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::NewProp_SimulationStageObject = { "SimulationStageObject", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackSimulationStagePropertiesItem, SimulationStageObject), Z_Construct_UClass_UNiagaraStackObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::NewProp_SimulationStageObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::NewProp_SimulationStageObject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::NewProp_SimulationStageObject,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackSimulationStagePropertiesItem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::ClassParams = {
		&UNiagaraStackSimulationStagePropertiesItem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackSimulationStagePropertiesItem, 267800103);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackSimulationStagePropertiesItem>()
	{
		return UNiagaraStackSimulationStagePropertiesItem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackSimulationStagePropertiesItem(Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem, &UNiagaraStackSimulationStagePropertiesItem::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackSimulationStagePropertiesItem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackSimulationStagePropertiesItem);
	void UNiagaraStackSimulationStageGroup::StaticRegisterNativesUNiagaraStackSimulationStageGroup()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackSimulationStageGroup_NoRegister()
	{
		return UNiagaraStackSimulationStageGroup::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimulationStageProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SimulationStageProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackScriptItemGroup,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackSimulationStageGroup.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackSimulationStageGroup.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::NewProp_SimulationStageProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackSimulationStageGroup.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::NewProp_SimulationStageProperties = { "SimulationStageProperties", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackSimulationStageGroup, SimulationStageProperties), Z_Construct_UClass_UNiagaraStackSimulationStagePropertiesItem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::NewProp_SimulationStageProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::NewProp_SimulationStageProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::NewProp_SimulationStageProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackSimulationStageGroup>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::ClassParams = {
		&UNiagaraStackSimulationStageGroup::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackSimulationStageGroup()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackSimulationStageGroup_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackSimulationStageGroup, 4117872671);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackSimulationStageGroup>()
	{
		return UNiagaraStackSimulationStageGroup::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackSimulationStageGroup(Z_Construct_UClass_UNiagaraStackSimulationStageGroup, &UNiagaraStackSimulationStageGroup::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackSimulationStageGroup"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackSimulationStageGroup);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
