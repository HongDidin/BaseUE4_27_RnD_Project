// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackSystemPropertiesItem_generated_h
#error "NiagaraStackSystemPropertiesItem.generated.h already included, missing '#pragma once' in NiagaraStackSystemPropertiesItem.h"
#endif
#define NIAGARAEDITOR_NiagaraStackSystemPropertiesItem_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackSystemPropertiesItem(); \
	friend struct Z_Construct_UClass_UNiagaraStackSystemPropertiesItem_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackSystemPropertiesItem, UNiagaraStackItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackSystemPropertiesItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackSystemPropertiesItem(); \
	friend struct Z_Construct_UClass_UNiagaraStackSystemPropertiesItem_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackSystemPropertiesItem, UNiagaraStackItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackSystemPropertiesItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackSystemPropertiesItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackSystemPropertiesItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackSystemPropertiesItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackSystemPropertiesItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackSystemPropertiesItem(UNiagaraStackSystemPropertiesItem&&); \
	NO_API UNiagaraStackSystemPropertiesItem(const UNiagaraStackSystemPropertiesItem&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackSystemPropertiesItem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackSystemPropertiesItem(UNiagaraStackSystemPropertiesItem&&); \
	NO_API UNiagaraStackSystemPropertiesItem(const UNiagaraStackSystemPropertiesItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackSystemPropertiesItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackSystemPropertiesItem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackSystemPropertiesItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SystemObject() { return STRUCT_OFFSET(UNiagaraStackSystemPropertiesItem, SystemObject); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_11_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackSystemPropertiesItem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemPropertiesItem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
