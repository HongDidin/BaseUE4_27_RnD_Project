// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackItemGroup_generated_h
#error "NiagaraStackItemGroup.generated.h already included, missing '#pragma once' in NiagaraStackItemGroup.h"
#endif
#define NIAGARAEDITOR_NiagaraStackItemGroup_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackItemGroup(); \
	friend struct Z_Construct_UClass_UNiagaraStackItemGroup_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackItemGroup, UNiagaraStackEntry, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackItemGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackItemGroup(); \
	friend struct Z_Construct_UClass_UNiagaraStackItemGroup_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackItemGroup, UNiagaraStackEntry, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackItemGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackItemGroup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackItemGroup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackItemGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackItemGroup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackItemGroup(UNiagaraStackItemGroup&&); \
	NO_API UNiagaraStackItemGroup(const UNiagaraStackItemGroup&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackItemGroup() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackItemGroup(UNiagaraStackItemGroup&&); \
	NO_API UNiagaraStackItemGroup(const UNiagaraStackItemGroup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackItemGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackItemGroup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackItemGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GroupFooter() { return STRUCT_OFFSET(UNiagaraStackItemGroup, GroupFooter); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_13_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackItemGroup>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackItemGroupFooter(); \
	friend struct Z_Construct_UClass_UNiagaraStackItemGroupFooter_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackItemGroupFooter, UNiagaraStackEntry, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackItemGroupFooter)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackItemGroupFooter(); \
	friend struct Z_Construct_UClass_UNiagaraStackItemGroupFooter_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackItemGroupFooter, UNiagaraStackEntry, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackItemGroupFooter)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackItemGroupFooter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackItemGroupFooter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackItemGroupFooter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackItemGroupFooter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackItemGroupFooter(UNiagaraStackItemGroupFooter&&); \
	NO_API UNiagaraStackItemGroupFooter(const UNiagaraStackItemGroupFooter&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackItemGroupFooter() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackItemGroupFooter(UNiagaraStackItemGroupFooter&&); \
	NO_API UNiagaraStackItemGroupFooter(const UNiagaraStackItemGroupFooter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackItemGroupFooter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackItemGroupFooter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackItemGroupFooter)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_64_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h_67_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackItemGroupFooter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackItemGroup_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
