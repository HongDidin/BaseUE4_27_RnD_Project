// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/Customizations/NiagaraOutlinerCustomization.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraOutlinerCustomization() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceData();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceData();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerSystemData();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerWorldData();
// End Cross Module References
class UScriptStruct* FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraOutlinerEmitterInstanceDataCustomizationWrapper"), sizeof(FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper>()
{
	return FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper(FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraOutlinerEmitterInstanceDataCustomizationWrapper"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerEmitterInstanceDataCustomizationWrapper
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerEmitterInstanceDataCustomizationWrapper()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper>(FName(TEXT("NiagaraOutlinerEmitterInstanceDataCustomizationWrapper")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerEmitterInstanceDataCustomizationWrapper;
	struct Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Customizations/NiagaraOutlinerCustomization.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::NewProp_Data_MetaData[] = {
		{ "Category", "Emitter" },
		{ "ModuleRelativePath", "Private/Customizations/NiagaraOutlinerCustomization.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::NewProp_Data = { "Data", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper, Data), Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceData, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::NewProp_Data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::NewProp_Data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::NewProp_Data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraOutlinerEmitterInstanceDataCustomizationWrapper",
		sizeof(FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper),
		alignof(FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper),
		Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraOutlinerEmitterInstanceDataCustomizationWrapper"), sizeof(FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Hash() { return 982872633U; }
class UScriptStruct* FNiagaraOutlinerSystemInstanceDataCustomizationWrapper::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraOutlinerSystemInstanceDataCustomizationWrapper"), sizeof(FNiagaraOutlinerSystemInstanceDataCustomizationWrapper), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraOutlinerSystemInstanceDataCustomizationWrapper>()
{
	return FNiagaraOutlinerSystemInstanceDataCustomizationWrapper::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper(FNiagaraOutlinerSystemInstanceDataCustomizationWrapper::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraOutlinerSystemInstanceDataCustomizationWrapper"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerSystemInstanceDataCustomizationWrapper
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerSystemInstanceDataCustomizationWrapper()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraOutlinerSystemInstanceDataCustomizationWrapper>(FName(TEXT("NiagaraOutlinerSystemInstanceDataCustomizationWrapper")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerSystemInstanceDataCustomizationWrapper;
	struct Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Customizations/NiagaraOutlinerCustomization.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraOutlinerSystemInstanceDataCustomizationWrapper>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::NewProp_Data_MetaData[] = {
		{ "Category", "Instance" },
		{ "ModuleRelativePath", "Private/Customizations/NiagaraOutlinerCustomization.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::NewProp_Data = { "Data", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerSystemInstanceDataCustomizationWrapper, Data), Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceData, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::NewProp_Data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::NewProp_Data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::NewProp_Data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraOutlinerSystemInstanceDataCustomizationWrapper",
		sizeof(FNiagaraOutlinerSystemInstanceDataCustomizationWrapper),
		alignof(FNiagaraOutlinerSystemInstanceDataCustomizationWrapper),
		Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraOutlinerSystemInstanceDataCustomizationWrapper"), sizeof(FNiagaraOutlinerSystemInstanceDataCustomizationWrapper), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Hash() { return 2127724643U; }
class UScriptStruct* FNiagaraOutlinerSystemDataCustomizationWrapper::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraOutlinerSystemDataCustomizationWrapper"), sizeof(FNiagaraOutlinerSystemDataCustomizationWrapper), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraOutlinerSystemDataCustomizationWrapper>()
{
	return FNiagaraOutlinerSystemDataCustomizationWrapper::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper(FNiagaraOutlinerSystemDataCustomizationWrapper::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraOutlinerSystemDataCustomizationWrapper"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerSystemDataCustomizationWrapper
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerSystemDataCustomizationWrapper()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraOutlinerSystemDataCustomizationWrapper>(FName(TEXT("NiagaraOutlinerSystemDataCustomizationWrapper")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerSystemDataCustomizationWrapper;
	struct Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Customizations/NiagaraOutlinerCustomization.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraOutlinerSystemDataCustomizationWrapper>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::NewProp_Data_MetaData[] = {
		{ "Category", "System" },
		{ "ModuleRelativePath", "Private/Customizations/NiagaraOutlinerCustomization.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::NewProp_Data = { "Data", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerSystemDataCustomizationWrapper, Data), Z_Construct_UScriptStruct_FNiagaraOutlinerSystemData, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::NewProp_Data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::NewProp_Data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::NewProp_Data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraOutlinerSystemDataCustomizationWrapper",
		sizeof(FNiagaraOutlinerSystemDataCustomizationWrapper),
		alignof(FNiagaraOutlinerSystemDataCustomizationWrapper),
		Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraOutlinerSystemDataCustomizationWrapper"), sizeof(FNiagaraOutlinerSystemDataCustomizationWrapper), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Hash() { return 685456229U; }
class UScriptStruct* FNiagaraOutlinerWorldDataCustomizationWrapper::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraOutlinerWorldDataCustomizationWrapper"), sizeof(FNiagaraOutlinerWorldDataCustomizationWrapper), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraOutlinerWorldDataCustomizationWrapper>()
{
	return FNiagaraOutlinerWorldDataCustomizationWrapper::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper(FNiagaraOutlinerWorldDataCustomizationWrapper::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraOutlinerWorldDataCustomizationWrapper"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerWorldDataCustomizationWrapper
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerWorldDataCustomizationWrapper()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraOutlinerWorldDataCustomizationWrapper>(FName(TEXT("NiagaraOutlinerWorldDataCustomizationWrapper")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerWorldDataCustomizationWrapper;
	struct Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Due to limitations of the structure details view, we need to wrap up structs we wish to customize. */" },
		{ "ModuleRelativePath", "Private/Customizations/NiagaraOutlinerCustomization.h" },
		{ "ToolTip", "Due to limitations of the structure details view, we need to wrap up structs we wish to customize." },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraOutlinerWorldDataCustomizationWrapper>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::NewProp_Data_MetaData[] = {
		{ "Category", "World" },
		{ "ModuleRelativePath", "Private/Customizations/NiagaraOutlinerCustomization.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::NewProp_Data = { "Data", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerWorldDataCustomizationWrapper, Data), Z_Construct_UScriptStruct_FNiagaraOutlinerWorldData, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::NewProp_Data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::NewProp_Data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::NewProp_Data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraOutlinerWorldDataCustomizationWrapper",
		sizeof(FNiagaraOutlinerWorldDataCustomizationWrapper),
		alignof(FNiagaraOutlinerWorldDataCustomizationWrapper),
		Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraOutlinerWorldDataCustomizationWrapper"), sizeof(FNiagaraOutlinerWorldDataCustomizationWrapper), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Hash() { return 1676901191U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
