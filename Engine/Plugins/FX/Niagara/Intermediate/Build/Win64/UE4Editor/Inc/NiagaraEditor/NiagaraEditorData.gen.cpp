// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraEditorData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraEditorData() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraEditorParametersAdapter_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraEditorParametersAdapter();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraEditorParametersAdapterBase();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScriptVariable_NoRegister();
// End Cross Module References
	void UNiagaraEditorParametersAdapter::StaticRegisterNativesUNiagaraEditorParametersAdapter()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraEditorParametersAdapter_NoRegister()
	{
		return UNiagaraEditorParametersAdapter::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditorOnlyScriptVars_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditorOnlyScriptVars_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EditorOnlyScriptVars;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraEditorParametersAdapterBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** An interface for editor only data which owns UNiagaraScriptVariables and supports synchronizing them with definitions. */" },
		{ "IncludePath", "NiagaraEditorData.h" },
		{ "ModuleRelativePath", "Public/NiagaraEditorData.h" },
		{ "ToolTip", "An interface for editor only data which owns UNiagaraScriptVariables and supports synchronizing them with definitions." },
	};
#endif
#if WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::NewProp_EditorOnlyScriptVars_Inner = { "EditorOnlyScriptVars", nullptr, (EPropertyFlags)0x0000000800000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraScriptVariable_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::NewProp_EditorOnlyScriptVars_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::NewProp_EditorOnlyScriptVars = { "EditorOnlyScriptVars", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraEditorParametersAdapter, EditorOnlyScriptVars), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::NewProp_EditorOnlyScriptVars_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::NewProp_EditorOnlyScriptVars_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::NewProp_EditorOnlyScriptVars_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::NewProp_EditorOnlyScriptVars,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraEditorParametersAdapter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::ClassParams = {
		&UNiagaraEditorParametersAdapter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::PropPointers), 0),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraEditorParametersAdapter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraEditorParametersAdapter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraEditorParametersAdapter, 2037349656);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraEditorParametersAdapter>()
	{
		return UNiagaraEditorParametersAdapter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraEditorParametersAdapter(Z_Construct_UClass_UNiagaraEditorParametersAdapter, &UNiagaraEditorParametersAdapter::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraEditorParametersAdapter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraEditorParametersAdapter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
