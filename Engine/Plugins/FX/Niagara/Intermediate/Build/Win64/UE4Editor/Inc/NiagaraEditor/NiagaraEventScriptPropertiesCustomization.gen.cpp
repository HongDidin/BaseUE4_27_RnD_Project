// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/Customizations/NiagaraEventScriptPropertiesCustomization.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraEventScriptPropertiesCustomization() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphSchemaAction();
// End Cross Module References

static_assert(std::is_polymorphic<FNiagaraStackAssetAction_EventSource>() == std::is_polymorphic<FEdGraphSchemaAction>(), "USTRUCT FNiagaraStackAssetAction_EventSource cannot be polymorphic unless super FEdGraphSchemaAction is polymorphic");

class UScriptStruct* FNiagaraStackAssetAction_EventSource::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraStackAssetAction_EventSource"), sizeof(FNiagaraStackAssetAction_EventSource), Get_Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraStackAssetAction_EventSource>()
{
	return FNiagaraStackAssetAction_EventSource::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraStackAssetAction_EventSource(FNiagaraStackAssetAction_EventSource::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraStackAssetAction_EventSource"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraStackAssetAction_EventSource
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraStackAssetAction_EventSource()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraStackAssetAction_EventSource>(FName(TEXT("NiagaraStackAssetAction_EventSource")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraStackAssetAction_EventSource;
	struct Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Customizations/NiagaraEventScriptPropertiesCustomization.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraStackAssetAction_EventSource>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		Z_Construct_UScriptStruct_FEdGraphSchemaAction,
		&NewStructOps,
		"NiagaraStackAssetAction_EventSource",
		sizeof(FNiagaraStackAssetAction_EventSource),
		alignof(FNiagaraStackAssetAction_EventSource),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraStackAssetAction_EventSource"), sizeof(FNiagaraStackAssetAction_EventSource), Get_Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraStackAssetAction_EventSource_Hash() { return 3820290029U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
