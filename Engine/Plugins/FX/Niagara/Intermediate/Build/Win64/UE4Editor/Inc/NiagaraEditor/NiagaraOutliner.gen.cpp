// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraOutliner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraOutliner() {}
// Cross Module References
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerTimeUnits();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerSortMode();
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerViewModes();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings();
	NIAGARA_API UEnum* Z_Construct_UEnum_Niagara_ENiagaraExecutionState();
	NIAGARA_API UEnum* Z_Construct_UEnum_Niagara_ENiagaraSimTarget();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraOutliner_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraOutliner();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerCaptureSettings();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerData();
// End Cross Module References
	static UEnum* ENiagaraOutlinerTimeUnits_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerTimeUnits, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraOutlinerTimeUnits"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraOutlinerTimeUnits>()
	{
		return ENiagaraOutlinerTimeUnits_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraOutlinerTimeUnits(ENiagaraOutlinerTimeUnits_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraOutlinerTimeUnits"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerTimeUnits_Hash() { return 412703251U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerTimeUnits()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraOutlinerTimeUnits"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerTimeUnits_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraOutlinerTimeUnits::Microseconds", (int64)ENiagaraOutlinerTimeUnits::Microseconds },
				{ "ENiagaraOutlinerTimeUnits::Milliseconds", (int64)ENiagaraOutlinerTimeUnits::Milliseconds },
				{ "ENiagaraOutlinerTimeUnits::Seconds", (int64)ENiagaraOutlinerTimeUnits::Seconds },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Microseconds.DisplayName", "us" },
				{ "Microseconds.Name", "ENiagaraOutlinerTimeUnits::Microseconds" },
				{ "Microseconds.ToolTip", "Microseconds" },
				{ "Milliseconds.DisplayName", "ms" },
				{ "Milliseconds.Name", "ENiagaraOutlinerTimeUnits::Milliseconds" },
				{ "Milliseconds.ToolTip", "Milliseconds" },
				{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
				{ "Seconds.DisplayName", "s" },
				{ "Seconds.Name", "ENiagaraOutlinerTimeUnits::Seconds" },
				{ "Seconds.ToolTip", "Seconds" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraOutlinerTimeUnits",
				"ENiagaraOutlinerTimeUnits",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENiagaraOutlinerSortMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerSortMode, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraOutlinerSortMode"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraOutlinerSortMode>()
	{
		return ENiagaraOutlinerSortMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraOutlinerSortMode(ENiagaraOutlinerSortMode_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraOutlinerSortMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerSortMode_Hash() { return 2424766783U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerSortMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraOutlinerSortMode"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerSortMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraOutlinerSortMode::Auto", (int64)ENiagaraOutlinerSortMode::Auto },
				{ "ENiagaraOutlinerSortMode::FilterMatches", (int64)ENiagaraOutlinerSortMode::FilterMatches },
				{ "ENiagaraOutlinerSortMode::AverageTime", (int64)ENiagaraOutlinerSortMode::AverageTime },
				{ "ENiagaraOutlinerSortMode::MaxTime", (int64)ENiagaraOutlinerSortMode::MaxTime },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Auto.Comment", "/** Context dependent default sorting. In State view mode this will sort by filter matches. In Performance mode this will sort by average time. */" },
				{ "Auto.Name", "ENiagaraOutlinerSortMode::Auto" },
				{ "Auto.ToolTip", "Context dependent default sorting. In State view mode this will sort by filter matches. In Performance mode this will sort by average time." },
				{ "AverageTime.Comment", "/** Sort by the average game thread time. */" },
				{ "AverageTime.Name", "ENiagaraOutlinerSortMode::AverageTime" },
				{ "AverageTime.ToolTip", "Sort by the average game thread time." },
				{ "FilterMatches.Comment", "/** Sort by the number of items matching the current filters. */" },
				{ "FilterMatches.Name", "ENiagaraOutlinerSortMode::FilterMatches" },
				{ "FilterMatches.ToolTip", "Sort by the number of items matching the current filters." },
				{ "MaxTime.Comment", "/** Sort by the maximum game thread time. */" },
				{ "MaxTime.Name", "ENiagaraOutlinerSortMode::MaxTime" },
				{ "MaxTime.ToolTip", "Sort by the maximum game thread time." },
				{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraOutlinerSortMode",
				"ENiagaraOutlinerSortMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENiagaraOutlinerViewModes_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerViewModes, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraOutlinerViewModes"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraOutlinerViewModes>()
	{
		return ENiagaraOutlinerViewModes_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraOutlinerViewModes(ENiagaraOutlinerViewModes_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraOutlinerViewModes"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerViewModes_Hash() { return 2486306784U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerViewModes()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraOutlinerViewModes"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerViewModes_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraOutlinerViewModes::State", (int64)ENiagaraOutlinerViewModes::State },
				{ "ENiagaraOutlinerViewModes::Performance", (int64)ENiagaraOutlinerViewModes::Performance },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
				{ "Performance.Comment", "/** Outliner displays performance data for each item. */" },
				{ "Performance.Name", "ENiagaraOutlinerViewModes::Performance" },
				{ "Performance.ToolTip", "Outliner displays performance data for each item." },
				{ "State.Comment", "/** Outliner displays the main state data for each item. */" },
				{ "State.Name", "ENiagaraOutlinerViewModes::State" },
				{ "State.ToolTip", "Outliner displays the main state data for each item." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraOutlinerViewModes",
				"ENiagaraOutlinerViewModes",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FNiagaraOutlinerViewSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraOutlinerViewSettings"), sizeof(FNiagaraOutlinerViewSettings), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraOutlinerViewSettings>()
{
	return FNiagaraOutlinerViewSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraOutlinerViewSettings(FNiagaraOutlinerViewSettings::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraOutlinerViewSettings"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerViewSettings
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerViewSettings()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraOutlinerViewSettings>(FName(TEXT("NiagaraOutlinerViewSettings")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerViewSettings;
	struct Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ViewMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ViewMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilterSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSortDescending_MetaData[];
#endif
		static void NewProp_bSortDescending_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSortDescending;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SortMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SortMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SortMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TimeUnits_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeUnits_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TimeUnits;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** View settings used in the Niagara Outliner. */" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
		{ "ToolTip", "View settings used in the Niagara Outliner." },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraOutlinerViewSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_ViewMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_ViewMode_MetaData[] = {
		{ "Category", "View" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_ViewMode = { "ViewMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerViewSettings, ViewMode), Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerViewModes, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_ViewMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_ViewMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_FilterSettings_MetaData[] = {
		{ "Category", "View" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_FilterSettings = { "FilterSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerViewSettings, FilterSettings), Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_FilterSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_FilterSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_bSortDescending_MetaData[] = {
		{ "Category", "View" },
		{ "Comment", "/** Whether to sort ascending or descending. */" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
		{ "ToolTip", "Whether to sort ascending or descending." },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_bSortDescending_SetBit(void* Obj)
	{
		((FNiagaraOutlinerViewSettings*)Obj)->bSortDescending = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_bSortDescending = { "bSortDescending", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNiagaraOutlinerViewSettings), &Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_bSortDescending_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_bSortDescending_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_bSortDescending_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_SortMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_SortMode_MetaData[] = {
		{ "Category", "View" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_SortMode = { "SortMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerViewSettings, SortMode), Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerSortMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_SortMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_SortMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_TimeUnits_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_TimeUnits_MetaData[] = {
		{ "Category", "View" },
		{ "Comment", "/** Units used to display time data in performance view mode. */" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
		{ "ToolTip", "Units used to display time data in performance view mode." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_TimeUnits = { "TimeUnits", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerViewSettings, TimeUnits), Z_Construct_UEnum_NiagaraEditor_ENiagaraOutlinerTimeUnits, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_TimeUnits_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_TimeUnits_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_ViewMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_ViewMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_FilterSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_bSortDescending,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_SortMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_SortMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_TimeUnits_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::NewProp_TimeUnits,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraOutlinerViewSettings",
		sizeof(FNiagaraOutlinerViewSettings),
		alignof(FNiagaraOutlinerViewSettings),
		Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraOutlinerViewSettings"), sizeof(FNiagaraOutlinerViewSettings), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings_Hash() { return 1168283470U; }
class UScriptStruct* FNiagaraOutlinerFilterSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraOutlinerFilterSettings"), sizeof(FNiagaraOutlinerFilterSettings), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraOutlinerFilterSettings>()
{
	return FNiagaraOutlinerFilterSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraOutlinerFilterSettings(FNiagaraOutlinerFilterSettings::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraOutlinerFilterSettings"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerFilterSettings
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerFilterSettings()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraOutlinerFilterSettings>(FName(TEXT("NiagaraOutlinerFilterSettings")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraOutlinerFilterSettings;
	struct Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFilterBySystemExecutionState_MetaData[];
#endif
		static void NewProp_bFilterBySystemExecutionState_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFilterBySystemExecutionState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFilterByEmitterExecutionState_MetaData[];
#endif
		static void NewProp_bFilterByEmitterExecutionState_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFilterByEmitterExecutionState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFilterByEmitterSimTarget_MetaData[];
#endif
		static void NewProp_bFilterByEmitterSimTarget_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFilterByEmitterSimTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFilterBySystemCullState_MetaData[];
#endif
		static void NewProp_bFilterBySystemCullState_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFilterBySystemCullState;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_SystemExecutionState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SystemExecutionState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SystemExecutionState;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_EmitterExecutionState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmitterExecutionState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EmitterExecutionState;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EmitterSimTarget_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmitterSimTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EmitterSimTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSystemCullState_MetaData[];
#endif
		static void NewProp_bSystemCullState_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSystemCullState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** View settings used in the Niagara Outliner. */" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
		{ "ToolTip", "View settings used in the Niagara Outliner." },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraOutlinerFilterSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemExecutionState_MetaData[] = {
		{ "Category", "Filters" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemExecutionState_SetBit(void* Obj)
	{
		((FNiagaraOutlinerFilterSettings*)Obj)->bFilterBySystemExecutionState = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemExecutionState = { "bFilterBySystemExecutionState", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FNiagaraOutlinerFilterSettings), &Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemExecutionState_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemExecutionState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemExecutionState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterExecutionState_MetaData[] = {
		{ "Category", "Filters" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterExecutionState_SetBit(void* Obj)
	{
		((FNiagaraOutlinerFilterSettings*)Obj)->bFilterByEmitterExecutionState = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterExecutionState = { "bFilterByEmitterExecutionState", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FNiagaraOutlinerFilterSettings), &Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterExecutionState_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterExecutionState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterExecutionState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterSimTarget_MetaData[] = {
		{ "Category", "Filters" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterSimTarget_SetBit(void* Obj)
	{
		((FNiagaraOutlinerFilterSettings*)Obj)->bFilterByEmitterSimTarget = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterSimTarget = { "bFilterByEmitterSimTarget", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FNiagaraOutlinerFilterSettings), &Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterSimTarget_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterSimTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterSimTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemCullState_MetaData[] = {
		{ "Category", "Filters" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemCullState_SetBit(void* Obj)
	{
		((FNiagaraOutlinerFilterSettings*)Obj)->bFilterBySystemCullState = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemCullState = { "bFilterBySystemCullState", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FNiagaraOutlinerFilterSettings), &Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemCullState_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemCullState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemCullState_MetaData)) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_SystemExecutionState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_SystemExecutionState_MetaData[] = {
		{ "Category", "Filters" },
		{ "Comment", "/** Only show systems with the following execution state. */" },
		{ "EditCondition", "bFilterBySystemExecutionState" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
		{ "ToolTip", "Only show systems with the following execution state." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_SystemExecutionState = { "SystemExecutionState", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerFilterSettings, SystemExecutionState), Z_Construct_UEnum_Niagara_ENiagaraExecutionState, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_SystemExecutionState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_SystemExecutionState_MetaData)) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterExecutionState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterExecutionState_MetaData[] = {
		{ "Category", "Filters" },
		{ "Comment", "/** Only show emitters with the following execution state. */" },
		{ "EditCondition", "bFilterByEmitterExecutionState" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
		{ "ToolTip", "Only show emitters with the following execution state." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterExecutionState = { "EmitterExecutionState", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerFilterSettings, EmitterExecutionState), Z_Construct_UEnum_Niagara_ENiagaraExecutionState, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterExecutionState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterExecutionState_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterSimTarget_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterSimTarget_MetaData[] = {
		{ "Category", "Filters" },
		{ "Comment", "/** Only show emitters with this SimTarget. */" },
		{ "EditCondition", "bFilterByEmitterSimTarget" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
		{ "ToolTip", "Only show emitters with this SimTarget." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterSimTarget = { "EmitterSimTarget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraOutlinerFilterSettings, EmitterSimTarget), Z_Construct_UEnum_Niagara_ENiagaraSimTarget, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterSimTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterSimTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bSystemCullState_MetaData[] = {
		{ "Category", "Filters" },
		{ "Comment", "/** Only show system instances with this cull state. */" },
		{ "EditCondition", "bFilterBySystemCullState" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
		{ "ToolTip", "Only show system instances with this cull state." },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bSystemCullState_SetBit(void* Obj)
	{
		((FNiagaraOutlinerFilterSettings*)Obj)->bSystemCullState = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bSystemCullState = { "bSystemCullState", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNiagaraOutlinerFilterSettings), &Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bSystemCullState_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bSystemCullState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bSystemCullState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemExecutionState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterExecutionState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterByEmitterSimTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bFilterBySystemCullState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_SystemExecutionState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_SystemExecutionState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterExecutionState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterExecutionState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterSimTarget_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_EmitterSimTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::NewProp_bSystemCullState,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraOutlinerFilterSettings",
		sizeof(FNiagaraOutlinerFilterSettings),
		alignof(FNiagaraOutlinerFilterSettings),
		Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraOutlinerFilterSettings"), sizeof(FNiagaraOutlinerFilterSettings), Get_Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraOutlinerFilterSettings_Hash() { return 2057995551U; }
	void UNiagaraOutliner::StaticRegisterNativesUNiagaraOutliner()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraOutliner_NoRegister()
	{
		return UNiagaraOutliner::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraOutliner_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraOutliner_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraOutliner_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraOutliner.h" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_CaptureSettings_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_CaptureSettings = { "CaptureSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraOutliner, CaptureSettings), Z_Construct_UScriptStruct_FNiagaraOutlinerCaptureSettings, METADATA_PARAMS(Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_CaptureSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_CaptureSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_ViewSettings_MetaData[] = {
		{ "Category", "Filters" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_ViewSettings = { "ViewSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraOutliner, ViewSettings), Z_Construct_UScriptStruct_FNiagaraOutlinerViewSettings, METADATA_PARAMS(Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_ViewSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_ViewSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_Data_MetaData[] = {
		{ "Category", "Outliner" },
		{ "ModuleRelativePath", "Private/NiagaraOutliner.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_Data = { "Data", nullptr, (EPropertyFlags)0x0010000000022001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraOutliner, Data), Z_Construct_UScriptStruct_FNiagaraOutlinerData, METADATA_PARAMS(Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_Data_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_Data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraOutliner_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_CaptureSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_ViewSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraOutliner_Statics::NewProp_Data,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraOutliner_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraOutliner>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraOutliner_Statics::ClassParams = {
		&UNiagaraOutliner::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraOutliner_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraOutliner_Statics::PropPointers),
		0,
		0x000000A2u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraOutliner_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraOutliner_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraOutliner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraOutliner_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraOutliner, 1215379141);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraOutliner>()
	{
		return UNiagaraOutliner::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraOutliner(Z_Construct_UClass_UNiagaraOutliner, &UNiagaraOutliner::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraOutliner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraOutliner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
