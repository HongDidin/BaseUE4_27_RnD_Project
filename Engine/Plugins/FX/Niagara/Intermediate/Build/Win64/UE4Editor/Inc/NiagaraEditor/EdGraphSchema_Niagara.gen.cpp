// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/EdGraphSchema_Niagara.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEdGraphSchema_Niagara() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphSchemaAction();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphNode_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UEdGraphSchema_Niagara_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UEdGraphSchema_Niagara();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphSchema();
// End Cross Module References

static_assert(std::is_polymorphic<FNiagaraSchemaAction_NewComment>() == std::is_polymorphic<FEdGraphSchemaAction>(), "USTRUCT FNiagaraSchemaAction_NewComment cannot be polymorphic unless super FEdGraphSchemaAction is polymorphic");

class UScriptStruct* FNiagaraSchemaAction_NewComment::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraSchemaAction_NewComment"), sizeof(FNiagaraSchemaAction_NewComment), Get_Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraSchemaAction_NewComment>()
{
	return FNiagaraSchemaAction_NewComment::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraSchemaAction_NewComment(FNiagaraSchemaAction_NewComment::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraSchemaAction_NewComment"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraSchemaAction_NewComment
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraSchemaAction_NewComment()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraSchemaAction_NewComment>(FName(TEXT("NiagaraSchemaAction_NewComment")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraSchemaAction_NewComment;
	struct Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EdGraphSchema_Niagara.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraSchemaAction_NewComment>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		Z_Construct_UScriptStruct_FEdGraphSchemaAction,
		&NewStructOps,
		"NiagaraSchemaAction_NewComment",
		sizeof(FNiagaraSchemaAction_NewComment),
		alignof(FNiagaraSchemaAction_NewComment),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraSchemaAction_NewComment"), sizeof(FNiagaraSchemaAction_NewComment), Get_Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Hash() { return 3394604395U; }

static_assert(std::is_polymorphic<FNiagaraSchemaAction_NewNode>() == std::is_polymorphic<FEdGraphSchemaAction>(), "USTRUCT FNiagaraSchemaAction_NewNode cannot be polymorphic unless super FEdGraphSchemaAction is polymorphic");

class UScriptStruct* FNiagaraSchemaAction_NewNode::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraSchemaAction_NewNode"), sizeof(FNiagaraSchemaAction_NewNode), Get_Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraSchemaAction_NewNode>()
{
	return FNiagaraSchemaAction_NewNode::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraSchemaAction_NewNode(FNiagaraSchemaAction_NewNode::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraSchemaAction_NewNode"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraSchemaAction_NewNode
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraSchemaAction_NewNode()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraSchemaAction_NewNode>(FName(TEXT("NiagaraSchemaAction_NewNode")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraSchemaAction_NewNode;
	struct Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeTemplate_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NodeTemplate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InternalName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InternalName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Action to add a node to the graph */" },
		{ "ModuleRelativePath", "Public/EdGraphSchema_Niagara.h" },
		{ "ToolTip", "Action to add a node to the graph" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraSchemaAction_NewNode>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewProp_NodeTemplate_MetaData[] = {
		{ "Comment", "/** Template of node we want to create */" },
		{ "ModuleRelativePath", "Public/EdGraphSchema_Niagara.h" },
		{ "ToolTip", "Template of node we want to create" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewProp_NodeTemplate = { "NodeTemplate", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraSchemaAction_NewNode, NodeTemplate), Z_Construct_UClass_UEdGraphNode_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewProp_NodeTemplate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewProp_NodeTemplate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewProp_InternalName_MetaData[] = {
		{ "ModuleRelativePath", "Public/EdGraphSchema_Niagara.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewProp_InternalName = { "InternalName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraSchemaAction_NewNode, InternalName), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewProp_InternalName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewProp_InternalName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewProp_NodeTemplate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::NewProp_InternalName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		Z_Construct_UScriptStruct_FEdGraphSchemaAction,
		&NewStructOps,
		"NiagaraSchemaAction_NewNode",
		sizeof(FNiagaraSchemaAction_NewNode),
		alignof(FNiagaraSchemaAction_NewNode),
		Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraSchemaAction_NewNode"), sizeof(FNiagaraSchemaAction_NewNode), Get_Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Hash() { return 2363564519U; }
	void UEdGraphSchema_Niagara::StaticRegisterNativesUEdGraphSchema_Niagara()
	{
	}
	UClass* Z_Construct_UClass_UEdGraphSchema_Niagara_NoRegister()
	{
		return UEdGraphSchema_Niagara::StaticClass();
	}
	struct Z_Construct_UClass_UEdGraphSchema_Niagara_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEdGraphSchema_Niagara_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphSchema,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdGraphSchema_Niagara_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EdGraphSchema_Niagara.h" },
		{ "ModuleRelativePath", "Public/EdGraphSchema_Niagara.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEdGraphSchema_Niagara_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEdGraphSchema_Niagara>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEdGraphSchema_Niagara_Statics::ClassParams = {
		&UEdGraphSchema_Niagara::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEdGraphSchema_Niagara_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEdGraphSchema_Niagara_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEdGraphSchema_Niagara()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEdGraphSchema_Niagara_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEdGraphSchema_Niagara, 2680350616);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UEdGraphSchema_Niagara>()
	{
		return UEdGraphSchema_Niagara::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEdGraphSchema_Niagara(Z_Construct_UClass_UEdGraphSchema_Niagara, &UEdGraphSchema_Niagara::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UEdGraphSchema_Niagara"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEdGraphSchema_Niagara);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
