// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraNodeCustomHlsl_generated_h
#error "NiagaraNodeCustomHlsl.generated.h already included, missing '#pragma once' in NiagaraNodeCustomHlsl.h"
#endif
#define NIAGARAEDITOR_NiagaraNodeCustomHlsl_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraNodeCustomHlsl(); \
	friend struct Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeCustomHlsl, UNiagaraNodeFunctionCall, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeCustomHlsl)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraNodeCustomHlsl(); \
	friend struct Z_Construct_UClass_UNiagaraNodeCustomHlsl_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeCustomHlsl, UNiagaraNodeFunctionCall, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeCustomHlsl)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeCustomHlsl(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeCustomHlsl) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeCustomHlsl); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeCustomHlsl); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeCustomHlsl(UNiagaraNodeCustomHlsl&&); \
	NIAGARAEDITOR_API UNiagaraNodeCustomHlsl(const UNiagaraNodeCustomHlsl&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeCustomHlsl(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeCustomHlsl(UNiagaraNodeCustomHlsl&&); \
	NIAGARAEDITOR_API UNiagaraNodeCustomHlsl(const UNiagaraNodeCustomHlsl&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeCustomHlsl); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeCustomHlsl); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeCustomHlsl)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CustomHlsl() { return STRUCT_OFFSET(UNiagaraNodeCustomHlsl, CustomHlsl); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_14_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraNodeCustomHlsl."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraNodeCustomHlsl>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeCustomHlsl_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
