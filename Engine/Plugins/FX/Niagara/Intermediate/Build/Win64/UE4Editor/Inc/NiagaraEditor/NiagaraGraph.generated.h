// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraGraph_generated_h
#error "NiagaraGraph.generated.h already included, missing '#pragma once' in NiagaraGraph.h"
#endif
#define NIAGARAEDITOR_NiagaraGraph_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_84_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__DataHash_DEPRECATED() { return STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, DataHash_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__GeneratedCompileId_DEPRECATED() { return STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, GeneratedCompileId_DEPRECATED); }


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraGraphScriptUsageInfo>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_59_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__bCreatedByUser() { return STRUCT_OFFSET(FNiagaraGraphParameterReferenceCollection, bCreatedByUser); }


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraGraphParameterReferenceCollection>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_34_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraGraphParameterReference>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraGraph(); \
	friend struct Z_Construct_UClass_UNiagaraGraph_Statics; \
public: \
	DECLARE_CLASS(UNiagaraGraph, UEdGraph, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraGraph)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraGraph(); \
	friend struct Z_Construct_UClass_UNiagaraGraph_Statics; \
public: \
	DECLARE_CLASS(UNiagaraGraph, UEdGraph, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraGraph)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraGraph(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraGraph) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraGraph); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraGraph(UNiagaraGraph&&); \
	NIAGARAEDITOR_API UNiagaraGraph(const UNiagaraGraph&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraGraph(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraGraph(UNiagaraGraph&&); \
	NIAGARAEDITOR_API UNiagaraGraph(const UNiagaraGraph&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraGraph); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraGraph)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ChangeId() { return STRUCT_OFFSET(UNiagaraGraph, ChangeId); } \
	FORCEINLINE static uint32 __PPO__ForceRebuildId() { return STRUCT_OFFSET(UNiagaraGraph, ForceRebuildId); } \
	FORCEINLINE static uint32 __PPO__LastBuiltTraversalDataChangeId() { return STRUCT_OFFSET(UNiagaraGraph, LastBuiltTraversalDataChangeId); } \
	FORCEINLINE static uint32 __PPO__CachedUsageInfo() { return STRUCT_OFFSET(UNiagaraGraph, CachedUsageInfo); } \
	FORCEINLINE static uint32 __PPO__VariableToMetaData_DEPRECATED() { return STRUCT_OFFSET(UNiagaraGraph, VariableToMetaData_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__VariableToScriptVariable() { return STRUCT_OFFSET(UNiagaraGraph, VariableToScriptVariable); } \
	FORCEINLINE static uint32 __PPO__ParameterToReferencesMap() { return STRUCT_OFFSET(UNiagaraGraph, ParameterToReferencesMap); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_137_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h_140_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraGraph."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraGraph>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraGraph_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
