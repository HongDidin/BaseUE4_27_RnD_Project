// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackEventScriptItemGroup_generated_h
#error "NiagaraStackEventScriptItemGroup.generated.h already included, missing '#pragma once' in NiagaraStackEventScriptItemGroup.h"
#endif
#define NIAGARAEDITOR_NiagaraStackEventScriptItemGroup_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackEventHandlerPropertiesItem(); \
	friend struct Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEventHandlerPropertiesItem, UNiagaraStackItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEventHandlerPropertiesItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackEventHandlerPropertiesItem(); \
	friend struct Z_Construct_UClass_UNiagaraStackEventHandlerPropertiesItem_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEventHandlerPropertiesItem, UNiagaraStackItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEventHandlerPropertiesItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackEventHandlerPropertiesItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackEventHandlerPropertiesItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEventHandlerPropertiesItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEventHandlerPropertiesItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEventHandlerPropertiesItem(UNiagaraStackEventHandlerPropertiesItem&&); \
	NO_API UNiagaraStackEventHandlerPropertiesItem(const UNiagaraStackEventHandlerPropertiesItem&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackEventHandlerPropertiesItem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEventHandlerPropertiesItem(UNiagaraStackEventHandlerPropertiesItem&&); \
	NO_API UNiagaraStackEventHandlerPropertiesItem(const UNiagaraStackEventHandlerPropertiesItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEventHandlerPropertiesItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEventHandlerPropertiesItem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackEventHandlerPropertiesItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__EmitterObject() { return STRUCT_OFFSET(UNiagaraStackEventHandlerPropertiesItem, EmitterObject); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_16_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackEventHandlerPropertiesItem>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackEventScriptItemGroup(); \
	friend struct Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEventScriptItemGroup, UNiagaraStackScriptItemGroup, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEventScriptItemGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackEventScriptItemGroup(); \
	friend struct Z_Construct_UClass_UNiagaraStackEventScriptItemGroup_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEventScriptItemGroup, UNiagaraStackScriptItemGroup, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEventScriptItemGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackEventScriptItemGroup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackEventScriptItemGroup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEventScriptItemGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEventScriptItemGroup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEventScriptItemGroup(UNiagaraStackEventScriptItemGroup&&); \
	NO_API UNiagaraStackEventScriptItemGroup(const UNiagaraStackEventScriptItemGroup&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackEventScriptItemGroup() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEventScriptItemGroup(UNiagaraStackEventScriptItemGroup&&); \
	NO_API UNiagaraStackEventScriptItemGroup(const UNiagaraStackEventScriptItemGroup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEventScriptItemGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEventScriptItemGroup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackEventScriptItemGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__EventHandlerProperties() { return STRUCT_OFFSET(UNiagaraStackEventScriptItemGroup, EventHandlerProperties); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_57_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h_61_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackEventScriptItemGroup>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEventScriptItemGroup_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
