// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UNiagaraNodeFunctionCall;
class UObject;
class UNiagaraScript;
class UNiagaraClipboardFunctionInput;
class UNiagaraDataInterface;
class UUserDefinedEnum;
class UUserDefinedStruct;
struct FVector;
struct FVector2D;
#ifdef NIAGARAEDITOR_NiagaraClipboard_generated_h
#error "NiagaraClipboard.generated.h already included, missing '#pragma once' in NiagaraClipboard.h"
#endif
#define NIAGARAEDITOR_NiagaraClipboard_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_95_DELEGATE \
struct NiagaraClipboardFunction_eventOnPastedFunctionCallNode_Parms \
{ \
	UNiagaraNodeFunctionCall* PastedFunctionCall; \
}; \
static inline void FOnPastedFunctionCallNode_DelegateWrapper(const FScriptDelegate& OnPastedFunctionCallNode, UNiagaraNodeFunctionCall* PastedFunctionCall) \
{ \
	NiagaraClipboardFunction_eventOnPastedFunctionCallNode_Parms Parms; \
	Parms.PastedFunctionCall=PastedFunctionCall; \
	OnPastedFunctionCallNode.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraClipboardFunctionInput(); \
	friend struct Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics; \
public: \
	DECLARE_CLASS(UNiagaraClipboardFunctionInput, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraClipboardFunctionInput)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraClipboardFunctionInput(); \
	friend struct Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics; \
public: \
	DECLARE_CLASS(UNiagaraClipboardFunctionInput, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraClipboardFunctionInput)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraClipboardFunctionInput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraClipboardFunctionInput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraClipboardFunctionInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraClipboardFunctionInput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraClipboardFunctionInput(UNiagaraClipboardFunctionInput&&); \
	NO_API UNiagaraClipboardFunctionInput(const UNiagaraClipboardFunctionInput&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraClipboardFunctionInput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraClipboardFunctionInput(UNiagaraClipboardFunctionInput&&); \
	NO_API UNiagaraClipboardFunctionInput(const UNiagaraClipboardFunctionInput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraClipboardFunctionInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraClipboardFunctionInput); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraClipboardFunctionInput)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_30_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraClipboardFunctionInput>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraClipboardFunction(); \
	friend struct Z_Construct_UClass_UNiagaraClipboardFunction_Statics; \
public: \
	DECLARE_CLASS(UNiagaraClipboardFunction, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraClipboardFunction)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraClipboardFunction(); \
	friend struct Z_Construct_UClass_UNiagaraClipboardFunction_Statics; \
public: \
	DECLARE_CLASS(UNiagaraClipboardFunction, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraClipboardFunction)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraClipboardFunction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraClipboardFunction) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraClipboardFunction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraClipboardFunction); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraClipboardFunction(UNiagaraClipboardFunction&&); \
	NO_API UNiagaraClipboardFunction(const UNiagaraClipboardFunction&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraClipboardFunction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraClipboardFunction(UNiagaraClipboardFunction&&); \
	NO_API UNiagaraClipboardFunction(const UNiagaraClipboardFunction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraClipboardFunction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraClipboardFunction); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraClipboardFunction)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_89_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_92_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraClipboardFunction>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraClipboardContent(); \
	friend struct Z_Construct_UClass_UNiagaraClipboardContent_Statics; \
public: \
	DECLARE_CLASS(UNiagaraClipboardContent, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraClipboardContent)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraClipboardContent(); \
	friend struct Z_Construct_UClass_UNiagaraClipboardContent_Statics; \
public: \
	DECLARE_CLASS(UNiagaraClipboardContent, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraClipboardContent)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraClipboardContent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraClipboardContent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraClipboardContent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraClipboardContent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraClipboardContent(UNiagaraClipboardContent&&); \
	NO_API UNiagaraClipboardContent(const UNiagaraClipboardContent&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraClipboardContent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraClipboardContent(UNiagaraClipboardContent&&); \
	NO_API UNiagaraClipboardContent(const UNiagaraClipboardContent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraClipboardContent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraClipboardContent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraClipboardContent)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_132_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_135_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraClipboardContent>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCreateDynamicValueInput); \
	DECLARE_FUNCTION(execCreateExpressionValueInput); \
	DECLARE_FUNCTION(execCreateDataValueInput); \
	DECLARE_FUNCTION(execCreateLinkedValueInput); \
	DECLARE_FUNCTION(execCreateEnumLocalValueInput); \
	DECLARE_FUNCTION(execCreateStructLocalValueInput); \
	DECLARE_FUNCTION(execCreateBoolLocalValueInput); \
	DECLARE_FUNCTION(execCreateIntLocalValueInput); \
	DECLARE_FUNCTION(execCreateVec3LocalValueInput); \
	DECLARE_FUNCTION(execCreateVec2LocalValueInput); \
	DECLARE_FUNCTION(execCreateFloatLocalValueInput); \
	DECLARE_FUNCTION(execGetTypeName); \
	DECLARE_FUNCTION(execTrySetLocalValueAsInt); \
	DECLARE_FUNCTION(execTryGetLocalValueAsInt); \
	DECLARE_FUNCTION(execTryGetLocalValueAsFloat); \
	DECLARE_FUNCTION(execTryGetInputByName);


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCreateDynamicValueInput); \
	DECLARE_FUNCTION(execCreateExpressionValueInput); \
	DECLARE_FUNCTION(execCreateDataValueInput); \
	DECLARE_FUNCTION(execCreateLinkedValueInput); \
	DECLARE_FUNCTION(execCreateEnumLocalValueInput); \
	DECLARE_FUNCTION(execCreateStructLocalValueInput); \
	DECLARE_FUNCTION(execCreateBoolLocalValueInput); \
	DECLARE_FUNCTION(execCreateIntLocalValueInput); \
	DECLARE_FUNCTION(execCreateVec3LocalValueInput); \
	DECLARE_FUNCTION(execCreateVec2LocalValueInput); \
	DECLARE_FUNCTION(execCreateFloatLocalValueInput); \
	DECLARE_FUNCTION(execGetTypeName); \
	DECLARE_FUNCTION(execTrySetLocalValueAsInt); \
	DECLARE_FUNCTION(execTryGetLocalValueAsInt); \
	DECLARE_FUNCTION(execTryGetLocalValueAsFloat); \
	DECLARE_FUNCTION(execTryGetInputByName);


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraClipboardEditorScriptingUtilities(); \
	friend struct Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics; \
public: \
	DECLARE_CLASS(UNiagaraClipboardEditorScriptingUtilities, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraClipboardEditorScriptingUtilities)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraClipboardEditorScriptingUtilities(); \
	friend struct Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics; \
public: \
	DECLARE_CLASS(UNiagaraClipboardEditorScriptingUtilities, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraClipboardEditorScriptingUtilities)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraClipboardEditorScriptingUtilities(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraClipboardEditorScriptingUtilities) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraClipboardEditorScriptingUtilities); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraClipboardEditorScriptingUtilities); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraClipboardEditorScriptingUtilities(UNiagaraClipboardEditorScriptingUtilities&&); \
	NO_API UNiagaraClipboardEditorScriptingUtilities(const UNiagaraClipboardEditorScriptingUtilities&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraClipboardEditorScriptingUtilities(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraClipboardEditorScriptingUtilities(UNiagaraClipboardEditorScriptingUtilities&&); \
	NO_API UNiagaraClipboardEditorScriptingUtilities(const UNiagaraClipboardEditorScriptingUtilities&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraClipboardEditorScriptingUtilities); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraClipboardEditorScriptingUtilities); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraClipboardEditorScriptingUtilities)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_166_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h_169_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraClipboardEditorScriptingUtilities>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraClipboard_h


#define FOREACH_ENUM_ENIAGARACLIPBOARDFUNCTIONSCRIPTMODE(op) \
	op(ENiagaraClipboardFunctionScriptMode::ScriptAsset) \
	op(ENiagaraClipboardFunctionScriptMode::Assignment) 

enum class ENiagaraClipboardFunctionScriptMode;
template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraClipboardFunctionScriptMode>();

#define FOREACH_ENUM_ENIAGARACLIPBOARDFUNCTIONINPUTVALUEMODE(op) \
	op(ENiagaraClipboardFunctionInputValueMode::Local) \
	op(ENiagaraClipboardFunctionInputValueMode::Linked) \
	op(ENiagaraClipboardFunctionInputValueMode::Data) \
	op(ENiagaraClipboardFunctionInputValueMode::Expression) \
	op(ENiagaraClipboardFunctionInputValueMode::Dynamic) 

enum class ENiagaraClipboardFunctionInputValueMode;
template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraClipboardFunctionInputValueMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
