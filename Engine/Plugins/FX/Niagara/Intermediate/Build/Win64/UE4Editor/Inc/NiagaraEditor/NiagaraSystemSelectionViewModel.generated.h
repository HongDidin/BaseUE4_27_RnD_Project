// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraSystemSelectionViewModel_generated_h
#error "NiagaraSystemSelectionViewModel.generated.h already included, missing '#pragma once' in NiagaraSystemSelectionViewModel.h"
#endif
#define NIAGARAEDITOR_NiagaraSystemSelectionViewModel_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraSystemSelectionViewModel(); \
	friend struct Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSystemSelectionViewModel, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSystemSelectionViewModel)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraSystemSelectionViewModel(); \
	friend struct Z_Construct_UClass_UNiagaraSystemSelectionViewModel_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSystemSelectionViewModel, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSystemSelectionViewModel)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSystemSelectionViewModel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSystemSelectionViewModel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSystemSelectionViewModel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSystemSelectionViewModel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSystemSelectionViewModel(UNiagaraSystemSelectionViewModel&&); \
	NO_API UNiagaraSystemSelectionViewModel(const UNiagaraSystemSelectionViewModel&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSystemSelectionViewModel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSystemSelectionViewModel(UNiagaraSystemSelectionViewModel&&); \
	NO_API UNiagaraSystemSelectionViewModel(const UNiagaraSystemSelectionViewModel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSystemSelectionViewModel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSystemSelectionViewModel); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSystemSelectionViewModel)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StackSelection() { return STRUCT_OFFSET(UNiagaraSystemSelectionViewModel, StackSelection); } \
	FORCEINLINE static uint32 __PPO__SelectionStackViewModel() { return STRUCT_OFFSET(UNiagaraSystemSelectionViewModel, SelectionStackViewModel); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_14_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h_31_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraSystemSelectionViewModel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraSystemSelectionViewModel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
