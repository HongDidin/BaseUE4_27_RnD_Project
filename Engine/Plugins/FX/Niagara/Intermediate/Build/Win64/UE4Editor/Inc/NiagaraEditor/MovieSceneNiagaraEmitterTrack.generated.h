// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_MovieSceneNiagaraEmitterTrack_generated_h
#error "MovieSceneNiagaraEmitterTrack.generated.h already included, missing '#pragma once' in MovieSceneNiagaraEmitterTrack.h"
#endif
#define NIAGARAEDITOR_MovieSceneNiagaraEmitterTrack_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneNiagaraEmitterSectionBase(); \
	friend struct Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneNiagaraEmitterSectionBase, UMovieSceneSection, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UMovieSceneNiagaraEmitterSectionBase)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneNiagaraEmitterSectionBase(); \
	friend struct Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneNiagaraEmitterSectionBase, UMovieSceneSection, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UMovieSceneNiagaraEmitterSectionBase)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterSectionBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneNiagaraEmitterSectionBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UMovieSceneNiagaraEmitterSectionBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneNiagaraEmitterSectionBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterSectionBase(UMovieSceneNiagaraEmitterSectionBase&&); \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterSectionBase(const UMovieSceneNiagaraEmitterSectionBase&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterSectionBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterSectionBase(UMovieSceneNiagaraEmitterSectionBase&&); \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterSectionBase(const UMovieSceneNiagaraEmitterSectionBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UMovieSceneNiagaraEmitterSectionBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneNiagaraEmitterSectionBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneNiagaraEmitterSectionBase)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_15_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UMovieSceneNiagaraEmitterSectionBase>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneNiagaraEmitterTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneNiagaraEmitterTrack, UMovieSceneNameableTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UMovieSceneNiagaraEmitterTrack)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneNiagaraEmitterTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneNiagaraEmitterTrack, UMovieSceneNameableTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UMovieSceneNiagaraEmitterTrack)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterTrack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneNiagaraEmitterTrack) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UMovieSceneNiagaraEmitterTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneNiagaraEmitterTrack); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterTrack(UMovieSceneNiagaraEmitterTrack&&); \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterTrack(const UMovieSceneNiagaraEmitterTrack&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterTrack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterTrack(UMovieSceneNiagaraEmitterTrack&&); \
	NIAGARAEDITOR_API UMovieSceneNiagaraEmitterTrack(const UMovieSceneNiagaraEmitterTrack&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UMovieSceneNiagaraEmitterTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneNiagaraEmitterTrack); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneNiagaraEmitterTrack)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Sections() { return STRUCT_OFFSET(UMovieSceneNiagaraEmitterTrack, Sections); } \
	FORCEINLINE static uint32 __PPO__bSectionsWereModified() { return STRUCT_OFFSET(UMovieSceneNiagaraEmitterTrack, bSectionsWereModified); } \
	FORCEINLINE static uint32 __PPO__EmitterHandleId() { return STRUCT_OFFSET(UMovieSceneNiagaraEmitterTrack, EmitterHandleId); } \
	FORCEINLINE static uint32 __PPO__SystemPath() { return STRUCT_OFFSET(UMovieSceneNiagaraEmitterTrack, SystemPath); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_52_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h_56_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MovieSceneNiagaraEmitterTrack."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UMovieSceneNiagaraEmitterTrack>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_MovieSceneNiagaraEmitterTrack_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
