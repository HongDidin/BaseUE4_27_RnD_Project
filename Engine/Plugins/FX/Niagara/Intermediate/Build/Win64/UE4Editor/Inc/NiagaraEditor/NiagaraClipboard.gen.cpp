// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraClipboard.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraClipboard() {}
// Cross Module References
	NIAGARAEDITOR_API UFunction* Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraClipboardFunction();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeFunctionCall_NoRegister();
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionScriptMode();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionInputValueMode();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraClipboardFunctionInput();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraTypeDefinition();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraDataInterface_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraClipboardFunction_NoRegister();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraScript_NoRegister();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariable();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraStackMessage();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraClipboardContent_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraClipboardContent();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraRendererProperties_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScriptVariable_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UUserDefinedEnum_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UUserDefinedStruct_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics
	{
		struct NiagaraClipboardFunction_eventOnPastedFunctionCallNode_Parms
		{
			UNiagaraNodeFunctionCall* PastedFunctionCall;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PastedFunctionCall;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics::NewProp_PastedFunctionCall = { "PastedFunctionCall", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardFunction_eventOnPastedFunctionCallNode_Parms, PastedFunctionCall), Z_Construct_UClass_UNiagaraNodeFunctionCall_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics::NewProp_PastedFunctionCall,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardFunction, nullptr, "OnPastedFunctionCallNode__DelegateSignature", nullptr, nullptr, sizeof(NiagaraClipboardFunction_eventOnPastedFunctionCallNode_Parms), Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* ENiagaraClipboardFunctionScriptMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionScriptMode, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraClipboardFunctionScriptMode"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraClipboardFunctionScriptMode>()
	{
		return ENiagaraClipboardFunctionScriptMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraClipboardFunctionScriptMode(ENiagaraClipboardFunctionScriptMode_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraClipboardFunctionScriptMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionScriptMode_Hash() { return 1186966493U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionScriptMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraClipboardFunctionScriptMode"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionScriptMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraClipboardFunctionScriptMode::ScriptAsset", (int64)ENiagaraClipboardFunctionScriptMode::ScriptAsset },
				{ "ENiagaraClipboardFunctionScriptMode::Assignment", (int64)ENiagaraClipboardFunctionScriptMode::Assignment },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Assignment.Name", "ENiagaraClipboardFunctionScriptMode::Assignment" },
				{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
				{ "ScriptAsset.Name", "ENiagaraClipboardFunctionScriptMode::ScriptAsset" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraClipboardFunctionScriptMode",
				"ENiagaraClipboardFunctionScriptMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENiagaraClipboardFunctionInputValueMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionInputValueMode, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraClipboardFunctionInputValueMode"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraClipboardFunctionInputValueMode>()
	{
		return ENiagaraClipboardFunctionInputValueMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraClipboardFunctionInputValueMode(ENiagaraClipboardFunctionInputValueMode_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraClipboardFunctionInputValueMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionInputValueMode_Hash() { return 1521889351U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionInputValueMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraClipboardFunctionInputValueMode"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionInputValueMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraClipboardFunctionInputValueMode::Local", (int64)ENiagaraClipboardFunctionInputValueMode::Local },
				{ "ENiagaraClipboardFunctionInputValueMode::Linked", (int64)ENiagaraClipboardFunctionInputValueMode::Linked },
				{ "ENiagaraClipboardFunctionInputValueMode::Data", (int64)ENiagaraClipboardFunctionInputValueMode::Data },
				{ "ENiagaraClipboardFunctionInputValueMode::Expression", (int64)ENiagaraClipboardFunctionInputValueMode::Expression },
				{ "ENiagaraClipboardFunctionInputValueMode::Dynamic", (int64)ENiagaraClipboardFunctionInputValueMode::Dynamic },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Data.Name", "ENiagaraClipboardFunctionInputValueMode::Data" },
				{ "Dynamic.Name", "ENiagaraClipboardFunctionInputValueMode::Dynamic" },
				{ "Expression.Name", "ENiagaraClipboardFunctionInputValueMode::Expression" },
				{ "Linked.Name", "ENiagaraClipboardFunctionInputValueMode::Linked" },
				{ "Local.Name", "ENiagaraClipboardFunctionInputValueMode::Local" },
				{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraClipboardFunctionInputValueMode",
				"ENiagaraClipboardFunctionInputValueMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UNiagaraClipboardFunctionInput::StaticRegisterNativesUNiagaraClipboardFunctionInput()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister()
	{
		return UNiagaraClipboardFunctionInput::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InputName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHasEditCondition_MetaData[];
#endif
		static void NewProp_bHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHasEditCondition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEditConditionValue_MetaData[];
#endif
		static void NewProp_bEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEditConditionValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ValueMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValueMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ValueMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Local_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Local_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Local;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Linked_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Linked;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Data_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Data;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Expression_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Expression;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Dynamic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Dynamic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraClipboard.h" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_InputName_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_InputName = { "InputName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunctionInput, InputName), METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_InputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_InputName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_InputType_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_InputType = { "InputType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunctionInput, InputType), Z_Construct_UScriptStruct_FNiagaraTypeDefinition, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_InputType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_InputType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bHasEditCondition_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	void Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bHasEditCondition_SetBit(void* Obj)
	{
		((UNiagaraClipboardFunctionInput*)Obj)->bHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bHasEditCondition = { "bHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraClipboardFunctionInput), &Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bHasEditCondition_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bHasEditCondition_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bHasEditCondition_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bEditConditionValue_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	void Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bEditConditionValue_SetBit(void* Obj)
	{
		((UNiagaraClipboardFunctionInput*)Obj)->bEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bEditConditionValue = { "bEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraClipboardFunctionInput), &Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bEditConditionValue_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bEditConditionValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bEditConditionValue_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_ValueMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_ValueMode_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_ValueMode = { "ValueMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunctionInput, ValueMode), Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionInputValueMode, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_ValueMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_ValueMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Local_Inner = { "Local", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Local_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Local = { "Local", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunctionInput, Local), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Local_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Local_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Linked_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Linked = { "Linked", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunctionInput, Linked), METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Linked_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Linked_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Data_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Data = { "Data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunctionInput, Data), Z_Construct_UClass_UNiagaraDataInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Data_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Data_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Expression_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Expression = { "Expression", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunctionInput, Expression), METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Expression_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Expression_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Dynamic_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Dynamic = { "Dynamic", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunctionInput, Dynamic), Z_Construct_UClass_UNiagaraClipboardFunction_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Dynamic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Dynamic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_InputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_InputType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_bEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_ValueMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_ValueMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Local_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Local,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Linked,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Data,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Expression,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::NewProp_Dynamic,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraClipboardFunctionInput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::ClassParams = {
		&UNiagaraClipboardFunctionInput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraClipboardFunctionInput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraClipboardFunctionInput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraClipboardFunctionInput, 1373706535);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraClipboardFunctionInput>()
	{
		return UNiagaraClipboardFunctionInput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraClipboardFunctionInput(Z_Construct_UClass_UNiagaraClipboardFunctionInput, &UNiagaraClipboardFunctionInput::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraClipboardFunctionInput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraClipboardFunctionInput);
	void UNiagaraClipboardFunction::StaticRegisterNativesUNiagaraClipboardFunction()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraClipboardFunction_NoRegister()
	{
		return UNiagaraClipboardFunction::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraClipboardFunction_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_EDITOR
		static const FClassFunctionLinkInfo FuncInfo[];
#endif //WITH_EDITOR
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FunctionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_DisplayName;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ScriptMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScriptMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ScriptMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Script_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_Script;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AssignmentTargets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssignmentTargets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AssignmentTargets;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssignmentDefaults_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssignmentDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AssignmentDefaults;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Inputs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Inputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Inputs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPastedFunctionCallNodeDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_OnPastedFunctionCallNodeDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScriptVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ScriptVersion;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Messages_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Messages_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Messages;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraClipboardFunction_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_EDITOR
	const FClassFunctionLinkInfo Z_Construct_UClass_UNiagaraClipboardFunction_Statics::FuncInfo[] = {
		{ &Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature, "OnPastedFunctionCallNode__DelegateSignature" }, // 4167829973
	};
#endif //WITH_EDITOR
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraClipboard.h" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_FunctionName_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_FunctionName = { "FunctionName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunction, FunctionName), METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_FunctionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_FunctionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_DisplayName_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunction, DisplayName), METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_DisplayName_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptMode_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptMode = { "ScriptMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunction, ScriptMode), Z_Construct_UEnum_NiagaraEditor_ENiagaraClipboardFunctionScriptMode, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Script_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Script = { "Script", nullptr, (EPropertyFlags)0x0014000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunction, Script), Z_Construct_UClass_UNiagaraScript_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Script_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Script_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentTargets_Inner = { "AssignmentTargets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraVariable, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentTargets_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentTargets = { "AssignmentTargets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunction, AssignmentTargets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentTargets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentTargets_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentDefaults_Inner = { "AssignmentDefaults", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentDefaults_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentDefaults = { "AssignmentDefaults", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunction, AssignmentDefaults), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentDefaults_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Inputs_Inner = { "Inputs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Inputs_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
		{ "NativeConstTemplateArg", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Inputs = { "Inputs", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunction, Inputs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Inputs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Inputs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_OnPastedFunctionCallNodeDelegate_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_OnPastedFunctionCallNodeDelegate = { "OnPastedFunctionCallNodeDelegate", nullptr, (EPropertyFlags)0x0010000000080000, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunction, OnPastedFunctionCallNodeDelegate), Z_Construct_UDelegateFunction_UNiagaraClipboardFunction_OnPastedFunctionCallNode__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_OnPastedFunctionCallNodeDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_OnPastedFunctionCallNodeDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptVersion_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptVersion = { "ScriptVersion", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunction, ScriptVersion), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptVersion_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Messages_Inner = { "Messages", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraStackMessage, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Messages_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Messages = { "Messages", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardFunction, Messages), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Messages_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Messages_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraClipboardFunction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_FunctionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Script,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentTargets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentTargets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentDefaults_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_AssignmentDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Inputs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Inputs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_OnPastedFunctionCallNodeDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_ScriptVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Messages_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardFunction_Statics::NewProp_Messages,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraClipboardFunction_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraClipboardFunction>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraClipboardFunction_Statics::ClassParams = {
		&UNiagaraClipboardFunction::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		IF_WITH_EDITOR(FuncInfo, nullptr),
		Z_Construct_UClass_UNiagaraClipboardFunction_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		IF_WITH_EDITOR(UE_ARRAY_COUNT(FuncInfo), 0),
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardFunction_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraClipboardFunction()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraClipboardFunction_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraClipboardFunction, 2150192201);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraClipboardFunction>()
	{
		return UNiagaraClipboardFunction::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraClipboardFunction(Z_Construct_UClass_UNiagaraClipboardFunction, &UNiagaraClipboardFunction::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraClipboardFunction"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraClipboardFunction);
	void UNiagaraClipboardContent::StaticRegisterNativesUNiagaraClipboardContent()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraClipboardContent_NoRegister()
	{
		return UNiagaraClipboardContent::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraClipboardContent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Functions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Functions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Functions;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FunctionInputs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionInputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FunctionInputs;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Renderers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Renderers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Renderers;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Scripts_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scripts_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Scripts;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ScriptVariables_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScriptVariables_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ScriptVariables;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraClipboardContent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardContent_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraClipboard.h" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Functions_Inner = { "Functions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraClipboardFunction_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Functions_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
		{ "NativeConstTemplateArg", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Functions = { "Functions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardContent, Functions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Functions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Functions_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_FunctionInputs_Inner = { "FunctionInputs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_FunctionInputs_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
		{ "NativeConstTemplateArg", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_FunctionInputs = { "FunctionInputs", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardContent, FunctionInputs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_FunctionInputs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_FunctionInputs_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Renderers_Inner = { "Renderers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraRendererProperties_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Renderers_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
		{ "NativeConstTemplateArg", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Renderers = { "Renderers", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardContent, Renderers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Renderers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Renderers_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Scripts_Inner = { "Scripts", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraScript_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Scripts_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
		{ "NativeConstTemplateArg", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Scripts = { "Scripts", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardContent, Scripts), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Scripts_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Scripts_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_ScriptVariables_Inner = { "ScriptVariables", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraScriptVariable_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_ScriptVariables_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
		{ "NativeConstTemplateArg", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_ScriptVariables = { "ScriptVariables", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraClipboardContent, ScriptVariables), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_ScriptVariables_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_ScriptVariables_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraClipboardContent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Functions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Functions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_FunctionInputs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_FunctionInputs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Renderers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Renderers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Scripts_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_Scripts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_ScriptVariables_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraClipboardContent_Statics::NewProp_ScriptVariables,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraClipboardContent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraClipboardContent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraClipboardContent_Statics::ClassParams = {
		&UNiagaraClipboardContent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraClipboardContent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardContent_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardContent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardContent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraClipboardContent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraClipboardContent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraClipboardContent, 3510788324);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraClipboardContent>()
	{
		return UNiagaraClipboardContent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraClipboardContent(Z_Construct_UClass_UNiagaraClipboardContent, &UNiagaraClipboardContent::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraClipboardContent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraClipboardContent);
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateDynamicValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputTypeName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditConditionValue);
		P_GET_PROPERTY(FStrProperty,Z_Param_InDynamicValueName);
		P_GET_OBJECT(UNiagaraScript,Z_Param_InDynamicValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateDynamicValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_InInputTypeName,Z_Param_bInHasEditCondition,Z_Param_bInEditConditionValue,Z_Param_InDynamicValueName,Z_Param_InDynamicValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateExpressionValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputTypeName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditConditionValue);
		P_GET_PROPERTY(FStrProperty,Z_Param_InExpressionValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateExpressionValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_InInputTypeName,Z_Param_bInHasEditCondition,Z_Param_bInEditConditionValue,Z_Param_InExpressionValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateDataValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditConditionValue);
		P_GET_OBJECT(UNiagaraDataInterface,Z_Param_InDataValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateDataValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_bInHasEditCondition,Z_Param_bInEditConditionValue,Z_Param_InDataValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateLinkedValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputTypeName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditConditionValue);
		P_GET_PROPERTY(FNameProperty,Z_Param_InLinkedValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateLinkedValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_InInputTypeName,Z_Param_bInHasEditCondition,Z_Param_bInEditConditionValue,Z_Param_InLinkedValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateEnumLocalValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditCoditionValue);
		P_GET_OBJECT(UUserDefinedEnum,Z_Param_InEnumType);
		P_GET_PROPERTY(FIntProperty,Z_Param_InEnumValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateEnumLocalValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_bInHasEditCondition,Z_Param_bInEditCoditionValue,Z_Param_InEnumType,Z_Param_InEnumValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateStructLocalValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditConditionValue);
		P_GET_OBJECT(UUserDefinedStruct,Z_Param_InStructValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateStructLocalValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_bInHasEditCondition,Z_Param_bInEditConditionValue,Z_Param_InStructValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateBoolLocalValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditConditionValue);
		P_GET_UBOOL(Z_Param_InBoolValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateBoolLocalValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_bInHasEditCondition,Z_Param_bInEditConditionValue,Z_Param_InBoolValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateIntLocalValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditConditionValue);
		P_GET_PROPERTY(FIntProperty,Z_Param_InLocalValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateIntLocalValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_bInHasEditCondition,Z_Param_bInEditConditionValue,Z_Param_InLocalValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateVec3LocalValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditConditionValue);
		P_GET_STRUCT(FVector,Z_Param_InVec3Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateVec3LocalValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_bInHasEditCondition,Z_Param_bInEditConditionValue,Z_Param_InVec3Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateVec2LocalValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditConditionValue);
		P_GET_STRUCT(FVector2D,Z_Param_InVec2Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateVec2LocalValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_bInHasEditCondition,Z_Param_bInEditConditionValue,Z_Param_InVec2Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execCreateFloatLocalValueInput)
	{
		P_GET_OBJECT(UObject,Z_Param_InOuter);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_UBOOL(Z_Param_bInHasEditCondition);
		P_GET_UBOOL(Z_Param_bInEditConditionValue);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InLocalValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UNiagaraClipboardFunctionInput**)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::CreateFloatLocalValueInput(Z_Param_InOuter,Z_Param_InInputName,Z_Param_bInHasEditCondition,Z_Param_bInEditConditionValue,Z_Param_InLocalValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execGetTypeName)
	{
		P_GET_OBJECT(UNiagaraClipboardFunctionInput,Z_Param_InInput);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FName*)Z_Param__Result=UNiagaraClipboardEditorScriptingUtilities::GetTypeName(Z_Param_InInput);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execTrySetLocalValueAsInt)
	{
		P_GET_OBJECT(UNiagaraClipboardFunctionInput,Z_Param_InInput);
		P_GET_UBOOL_REF(Z_Param_Out_bOutSucceeded);
		P_GET_PROPERTY(FIntProperty,Z_Param_InValue);
		P_GET_UBOOL(Z_Param_bLooseTyping);
		P_FINISH;
		P_NATIVE_BEGIN;
		UNiagaraClipboardEditorScriptingUtilities::TrySetLocalValueAsInt(Z_Param_InInput,Z_Param_Out_bOutSucceeded,Z_Param_InValue,Z_Param_bLooseTyping);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execTryGetLocalValueAsInt)
	{
		P_GET_OBJECT(UNiagaraClipboardFunctionInput,Z_Param_InInput);
		P_GET_UBOOL_REF(Z_Param_Out_bOutSucceeded);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_OutValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		UNiagaraClipboardEditorScriptingUtilities::TryGetLocalValueAsInt(Z_Param_InInput,Z_Param_Out_bOutSucceeded,Z_Param_Out_OutValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execTryGetLocalValueAsFloat)
	{
		P_GET_OBJECT(UNiagaraClipboardFunctionInput,Z_Param_InInput);
		P_GET_UBOOL_REF(Z_Param_Out_bOutSucceeded);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_OutValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		UNiagaraClipboardEditorScriptingUtilities::TryGetLocalValueAsFloat(Z_Param_InInput,Z_Param_Out_bOutSucceeded,Z_Param_Out_OutValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNiagaraClipboardEditorScriptingUtilities::execTryGetInputByName)
	{
		P_GET_TARRAY_REF(UNiagaraClipboardFunctionInput*,Z_Param_Out_InInputs);
		P_GET_PROPERTY(FNameProperty,Z_Param_InInputName);
		P_GET_UBOOL_REF(Z_Param_Out_bOutSucceeded);
		P_GET_OBJECT_REF(UNiagaraClipboardFunctionInput,Z_Param_Out_OutInput);
		P_FINISH;
		P_NATIVE_BEGIN;
		UNiagaraClipboardEditorScriptingUtilities::TryGetInputByName(Z_Param_Out_InInputs,Z_Param_InInputName,Z_Param_Out_bOutSucceeded,Z_Param_Out_OutInput);
		P_NATIVE_END;
	}
	void UNiagaraClipboardEditorScriptingUtilities::StaticRegisterNativesUNiagaraClipboardEditorScriptingUtilities()
	{
		UClass* Class = UNiagaraClipboardEditorScriptingUtilities::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateBoolLocalValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateBoolLocalValueInput },
			{ "CreateDataValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateDataValueInput },
			{ "CreateDynamicValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateDynamicValueInput },
			{ "CreateEnumLocalValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateEnumLocalValueInput },
			{ "CreateExpressionValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateExpressionValueInput },
			{ "CreateFloatLocalValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateFloatLocalValueInput },
			{ "CreateIntLocalValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateIntLocalValueInput },
			{ "CreateLinkedValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateLinkedValueInput },
			{ "CreateStructLocalValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateStructLocalValueInput },
			{ "CreateVec2LocalValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateVec2LocalValueInput },
			{ "CreateVec3LocalValueInput", &UNiagaraClipboardEditorScriptingUtilities::execCreateVec3LocalValueInput },
			{ "GetTypeName", &UNiagaraClipboardEditorScriptingUtilities::execGetTypeName },
			{ "TryGetInputByName", &UNiagaraClipboardEditorScriptingUtilities::execTryGetInputByName },
			{ "TryGetLocalValueAsFloat", &UNiagaraClipboardEditorScriptingUtilities::execTryGetLocalValueAsFloat },
			{ "TryGetLocalValueAsInt", &UNiagaraClipboardEditorScriptingUtilities::execTryGetLocalValueAsInt },
			{ "TrySetLocalValueAsInt", &UNiagaraClipboardEditorScriptingUtilities::execTrySetLocalValueAsInt },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			bool bInHasEditCondition;
			bool bInEditConditionValue;
			bool InBoolValue;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditConditionValue;
		static void NewProp_InBoolValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_InBoolValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms*)Obj)->bInEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_bInEditConditionValue = { "bInEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_InBoolValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms*)Obj)->InBoolValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_InBoolValue = { "InBoolValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_InBoolValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_bInEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_InBoolValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateBoolLocalValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateBoolLocalValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateDataValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			bool bInHasEditCondition;
			bool bInEditConditionValue;
			UNiagaraDataInterface* InDataValue;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditConditionValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InDataValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateDataValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateDataValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateDataValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateDataValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_bInEditConditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateDataValueInput_Parms*)Obj)->bInEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_bInEditConditionValue = { "bInEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateDataValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_bInEditConditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_InDataValue = { "InDataValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateDataValueInput_Parms, InDataValue), Z_Construct_UClass_UNiagaraDataInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateDataValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_bInEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_InDataValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateDataValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateDataValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			FName InInputTypeName;
			bool bInHasEditCondition;
			bool bInEditConditionValue;
			FString InDynamicValueName;
			UNiagaraScript* InDynamicValue;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputTypeName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditConditionValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InDynamicValueName;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InDynamicValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_InInputTypeName = { "InInputTypeName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms, InInputTypeName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_bInEditConditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms*)Obj)->bInEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_bInEditConditionValue = { "bInEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_bInEditConditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_InDynamicValueName = { "InDynamicValueName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms, InDynamicValueName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_InDynamicValue = { "InDynamicValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms, InDynamicValue), Z_Construct_UClass_UNiagaraScript_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_InInputTypeName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_bInEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_InDynamicValueName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_InDynamicValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateDynamicValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateDynamicValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			bool bInHasEditCondition;
			bool bInEditCoditionValue;
			UUserDefinedEnum* InEnumType;
			int32 InEnumValue;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditCoditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditCoditionValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InEnumType;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InEnumValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_bInEditCoditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms*)Obj)->bInEditCoditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_bInEditCoditionValue = { "bInEditCoditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_bInEditCoditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_InEnumType = { "InEnumType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms, InEnumType), Z_Construct_UClass_UUserDefinedEnum_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_InEnumValue = { "InEnumValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms, InEnumValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_bInEditCoditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_InEnumType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_InEnumValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateEnumLocalValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateEnumLocalValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			FName InInputTypeName;
			bool bInHasEditCondition;
			bool bInEditConditionValue;
			FString InExpressionValue;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputTypeName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditConditionValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InExpressionValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InExpressionValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InInputTypeName = { "InInputTypeName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms, InInputTypeName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_bInEditConditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms*)Obj)->bInEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_bInEditConditionValue = { "bInEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_bInEditConditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InExpressionValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InExpressionValue = { "InExpressionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms, InExpressionValue), METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InExpressionValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InExpressionValue_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InInputTypeName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_bInEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_InExpressionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateExpressionValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateExpressionValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateFloatLocalValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			bool bInHasEditCondition;
			bool bInEditConditionValue;
			float InLocalValue;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditConditionValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InLocalValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateFloatLocalValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateFloatLocalValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateFloatLocalValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateFloatLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateFloatLocalValueInput_Parms*)Obj)->bInEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_bInEditConditionValue = { "bInEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateFloatLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_InLocalValue = { "InLocalValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateFloatLocalValueInput_Parms, InLocalValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateFloatLocalValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_bInEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_InLocalValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateFloatLocalValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateFloatLocalValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateIntLocalValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			bool bInHasEditCondition;
			bool bInEditConditionValue;
			int32 InLocalValue;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditConditionValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InLocalValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateIntLocalValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateIntLocalValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateIntLocalValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateIntLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateIntLocalValueInput_Parms*)Obj)->bInEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_bInEditConditionValue = { "bInEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateIntLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_InLocalValue = { "InLocalValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateIntLocalValueInput_Parms, InLocalValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateIntLocalValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_bInEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_InLocalValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateIntLocalValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateIntLocalValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			FName InInputTypeName;
			bool bInHasEditCondition;
			bool bInEditConditionValue;
			FName InLinkedValue;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputTypeName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditConditionValue;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InLinkedValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_InInputTypeName = { "InInputTypeName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms, InInputTypeName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_bInEditConditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms*)Obj)->bInEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_bInEditConditionValue = { "bInEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_bInEditConditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_InLinkedValue = { "InLinkedValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms, InLinkedValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_InInputTypeName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_bInEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_InLinkedValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateLinkedValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateLinkedValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateStructLocalValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			bool bInHasEditCondition;
			bool bInEditConditionValue;
			UUserDefinedStruct* InStructValue;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditConditionValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InStructValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateStructLocalValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateStructLocalValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateStructLocalValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateStructLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateStructLocalValueInput_Parms*)Obj)->bInEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_bInEditConditionValue = { "bInEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateStructLocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_InStructValue = { "InStructValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateStructLocalValueInput_Parms, InStructValue), Z_Construct_UClass_UUserDefinedStruct_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateStructLocalValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_bInEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_InStructValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateStructLocalValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateStructLocalValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateVec2LocalValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			bool bInHasEditCondition;
			bool bInEditConditionValue;
			FVector2D InVec2Value;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditConditionValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InVec2Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateVec2LocalValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateVec2LocalValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateVec2LocalValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateVec2LocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateVec2LocalValueInput_Parms*)Obj)->bInEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_bInEditConditionValue = { "bInEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateVec2LocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_InVec2Value = { "InVec2Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateVec2LocalValueInput_Parms, InVec2Value), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateVec2LocalValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_bInEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_InVec2Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateVec2LocalValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateVec2LocalValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventCreateVec3LocalValueInput_Parms
		{
			UObject* InOuter;
			FName InInputName;
			bool bInHasEditCondition;
			bool bInEditConditionValue;
			FVector InVec3Value;
			UNiagaraClipboardFunctionInput* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InOuter;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static void NewProp_bInHasEditCondition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInHasEditCondition;
		static void NewProp_bInEditConditionValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInEditConditionValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InVec3Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_InOuter = { "InOuter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateVec3LocalValueInput_Parms, InOuter), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateVec3LocalValueInput_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateVec3LocalValueInput_Parms*)Obj)->bInHasEditCondition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_bInHasEditCondition = { "bInHasEditCondition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateVec3LocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_bInHasEditCondition_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventCreateVec3LocalValueInput_Parms*)Obj)->bInEditConditionValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_bInEditConditionValue = { "bInEditConditionValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateVec3LocalValueInput_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_bInEditConditionValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_InVec3Value = { "InVec3Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateVec3LocalValueInput_Parms, InVec3Value), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventCreateVec3LocalValueInput_Parms, ReturnValue), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_InOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_bInHasEditCondition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_bInEditConditionValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_InVec3Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "CreateVec3LocalValueInput", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventCreateVec3LocalValueInput_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventGetTypeName_Parms
		{
			const UNiagaraClipboardFunctionInput* InInput;
			FName ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InInput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InInput;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::NewProp_InInput_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::NewProp_InInput = { "InInput", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventGetTypeName_Parms, InInput), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::NewProp_InInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::NewProp_InInput_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventGetTypeName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::NewProp_InInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "GetTypeName", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventGetTypeName_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventTryGetInputByName_Parms
		{
			TArray<UNiagaraClipboardFunctionInput*> InInputs;
			FName InInputName;
			bool bOutSucceeded;
			UNiagaraClipboardFunctionInput* OutInput;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InInputs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InInputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InInputs;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InInputName;
		static void NewProp_bOutSucceeded_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOutSucceeded;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutInput;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_InInputs_Inner = { "InInputs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_InInputs_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_InInputs = { "InInputs", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventTryGetInputByName_Parms, InInputs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_InInputs_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_InInputs_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_InInputName = { "InInputName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventTryGetInputByName_Parms, InInputName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_bOutSucceeded_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventTryGetInputByName_Parms*)Obj)->bOutSucceeded = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_bOutSucceeded = { "bOutSucceeded", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventTryGetInputByName_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_bOutSucceeded_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_OutInput = { "OutInput", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventTryGetInputByName_Parms, OutInput), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_InInputs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_InInputs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_InInputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_bOutSucceeded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::NewProp_OutInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "TryGetInputByName", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventTryGetInputByName_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsFloat_Parms
		{
			const UNiagaraClipboardFunctionInput* InInput;
			bool bOutSucceeded;
			float OutValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InInput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InInput;
		static void NewProp_bOutSucceeded_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOutSucceeded;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_InInput_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_InInput = { "InInput", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsFloat_Parms, InInput), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_InInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_InInput_MetaData)) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_bOutSucceeded_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsFloat_Parms*)Obj)->bOutSucceeded = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_bOutSucceeded = { "bOutSucceeded", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsFloat_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_bOutSucceeded_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_OutValue = { "OutValue", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsFloat_Parms, OutValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_InInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_bOutSucceeded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::NewProp_OutValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "TryGetLocalValueAsFloat", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsFloat_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsInt_Parms
		{
			const UNiagaraClipboardFunctionInput* InInput;
			bool bOutSucceeded;
			int32 OutValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InInput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InInput;
		static void NewProp_bOutSucceeded_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOutSucceeded;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OutValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_InInput_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_InInput = { "InInput", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsInt_Parms, InInput), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_InInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_InInput_MetaData)) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_bOutSucceeded_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsInt_Parms*)Obj)->bOutSucceeded = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_bOutSucceeded = { "bOutSucceeded", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsInt_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_bOutSucceeded_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_OutValue = { "OutValue", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsInt_Parms, OutValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_InInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_bOutSucceeded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::NewProp_OutValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "TryGetLocalValueAsInt", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventTryGetLocalValueAsInt_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics
	{
		struct NiagaraClipboardEditorScriptingUtilities_eventTrySetLocalValueAsInt_Parms
		{
			UNiagaraClipboardFunctionInput* InInput;
			bool bOutSucceeded;
			int32 InValue;
			bool bLooseTyping;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InInput;
		static void NewProp_bOutSucceeded_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOutSucceeded;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InValue;
		static void NewProp_bLooseTyping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLooseTyping;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_InInput = { "InInput", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventTrySetLocalValueAsInt_Parms, InInput), Z_Construct_UClass_UNiagaraClipboardFunctionInput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_bOutSucceeded_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventTrySetLocalValueAsInt_Parms*)Obj)->bOutSucceeded = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_bOutSucceeded = { "bOutSucceeded", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventTrySetLocalValueAsInt_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_bOutSucceeded_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraClipboardEditorScriptingUtilities_eventTrySetLocalValueAsInt_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_bLooseTyping_SetBit(void* Obj)
	{
		((NiagaraClipboardEditorScriptingUtilities_eventTrySetLocalValueAsInt_Parms*)Obj)->bLooseTyping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_bLooseTyping = { "bLooseTyping", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NiagaraClipboardEditorScriptingUtilities_eventTrySetLocalValueAsInt_Parms), &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_bLooseTyping_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_InInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_bOutSucceeded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::NewProp_bLooseTyping,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "CPP_Default_bLooseTyping", "true" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, nullptr, "TrySetLocalValueAsInt", nullptr, nullptr, sizeof(NiagaraClipboardEditorScriptingUtilities_eventTrySetLocalValueAsInt_Parms), Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_NoRegister()
	{
		return UNiagaraClipboardEditorScriptingUtilities::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateBoolLocalValueInput, "CreateBoolLocalValueInput" }, // 2165681785
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDataValueInput, "CreateDataValueInput" }, // 1109456193
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateDynamicValueInput, "CreateDynamicValueInput" }, // 247815539
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateEnumLocalValueInput, "CreateEnumLocalValueInput" }, // 1030700498
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateExpressionValueInput, "CreateExpressionValueInput" }, // 3439230406
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateFloatLocalValueInput, "CreateFloatLocalValueInput" }, // 313704322
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateIntLocalValueInput, "CreateIntLocalValueInput" }, // 1289331457
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateLinkedValueInput, "CreateLinkedValueInput" }, // 600735322
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateStructLocalValueInput, "CreateStructLocalValueInput" }, // 523991979
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec2LocalValueInput, "CreateVec2LocalValueInput" }, // 4029638936
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_CreateVec3LocalValueInput, "CreateVec3LocalValueInput" }, // 3002256118
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_GetTypeName, "GetTypeName" }, // 4271952608
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetInputByName, "TryGetInputByName" }, // 4076453124
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsFloat, "TryGetLocalValueAsFloat" }, // 2191337323
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TryGetLocalValueAsInt, "TryGetLocalValueAsInt" }, // 3914459724
		{ &Z_Construct_UFunction_UNiagaraClipboardEditorScriptingUtilities_TrySetLocalValueAsInt, "TrySetLocalValueAsInt" }, // 505651328
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraClipboard.h" },
		{ "ModuleRelativePath", "Public/NiagaraClipboard.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraClipboardEditorScriptingUtilities>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics::ClassParams = {
		&UNiagaraClipboardEditorScriptingUtilities::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraClipboardEditorScriptingUtilities, 294351599);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraClipboardEditorScriptingUtilities>()
	{
		return UNiagaraClipboardEditorScriptingUtilities::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraClipboardEditorScriptingUtilities(Z_Construct_UClass_UNiagaraClipboardEditorScriptingUtilities, &UNiagaraClipboardEditorScriptingUtilities::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraClipboardEditorScriptingUtilities"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraClipboardEditorScriptingUtilities);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
