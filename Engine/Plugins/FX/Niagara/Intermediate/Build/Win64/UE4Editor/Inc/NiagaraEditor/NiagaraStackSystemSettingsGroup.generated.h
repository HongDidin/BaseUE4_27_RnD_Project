// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackSystemSettingsGroup_generated_h
#error "NiagaraStackSystemSettingsGroup.generated.h already included, missing '#pragma once' in NiagaraStackSystemSettingsGroup.h"
#endif
#define NIAGARAEDITOR_NiagaraStackSystemSettingsGroup_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackSystemSettingsGroup(); \
	friend struct Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackSystemSettingsGroup, UNiagaraStackItemGroup, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackSystemSettingsGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackSystemSettingsGroup(); \
	friend struct Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackSystemSettingsGroup, UNiagaraStackItemGroup, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackSystemSettingsGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackSystemSettingsGroup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackSystemSettingsGroup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackSystemSettingsGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackSystemSettingsGroup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackSystemSettingsGroup(UNiagaraStackSystemSettingsGroup&&); \
	NO_API UNiagaraStackSystemSettingsGroup(const UNiagaraStackSystemSettingsGroup&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackSystemSettingsGroup() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackSystemSettingsGroup(UNiagaraStackSystemSettingsGroup&&); \
	NO_API UNiagaraStackSystemSettingsGroup(const UNiagaraStackSystemSettingsGroup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackSystemSettingsGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackSystemSettingsGroup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackSystemSettingsGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_14_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackSystemSettingsGroup>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackParameterStoreItem(); \
	friend struct Z_Construct_UClass_UNiagaraStackParameterStoreItem_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackParameterStoreItem, UNiagaraStackItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackParameterStoreItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackParameterStoreItem(); \
	friend struct Z_Construct_UClass_UNiagaraStackParameterStoreItem_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackParameterStoreItem, UNiagaraStackItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackParameterStoreItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackParameterStoreItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackParameterStoreItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackParameterStoreItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackParameterStoreItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackParameterStoreItem(UNiagaraStackParameterStoreItem&&); \
	NO_API UNiagaraStackParameterStoreItem(const UNiagaraStackParameterStoreItem&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackParameterStoreItem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackParameterStoreItem(UNiagaraStackParameterStoreItem&&); \
	NO_API UNiagaraStackParameterStoreItem(const UNiagaraStackParameterStoreItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackParameterStoreItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackParameterStoreItem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackParameterStoreItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_35_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h_38_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackParameterStoreItem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackSystemSettingsGroup_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
