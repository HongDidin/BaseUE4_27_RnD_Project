// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraActions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraActions() {}
// Cross Module References
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_EScriptSource();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraMenuSections();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraAction_NewNode();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphNode_NoRegister();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraMenuAction_Base();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraMenuAction();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphSchemaAction();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraActionSourceData();
// End Cross Module References
	static UEnum* EScriptSource_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_EScriptSource, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("EScriptSource"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<EScriptSource>()
	{
		return EScriptSource_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EScriptSource(EScriptSource_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("EScriptSource"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_EScriptSource_Hash() { return 2847039336U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_EScriptSource()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EScriptSource"), 0, Get_Z_Construct_UEnum_NiagaraEditor_EScriptSource_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EScriptSource::Niagara", (int64)EScriptSource::Niagara },
				{ "EScriptSource::Game", (int64)EScriptSource::Game },
				{ "EScriptSource::Plugins", (int64)EScriptSource::Plugins },
				{ "EScriptSource::Developer", (int64)EScriptSource::Developer },
				{ "EScriptSource::Unknown", (int64)EScriptSource::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Developer.Name", "EScriptSource::Developer" },
				{ "Game.Name", "EScriptSource::Game" },
				{ "ModuleRelativePath", "Public/NiagaraActions.h" },
				{ "Niagara.Name", "EScriptSource::Niagara" },
				{ "Plugins.Name", "EScriptSource::Plugins" },
				{ "Unknown.Name", "EScriptSource::Unknown" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"EScriptSource",
				"EScriptSource",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENiagaraMenuSections_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraMenuSections, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraMenuSections"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraMenuSections>()
	{
		return ENiagaraMenuSections_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraMenuSections(ENiagaraMenuSections_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraMenuSections"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraMenuSections_Hash() { return 998338023U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraMenuSections()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraMenuSections"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraMenuSections_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraMenuSections::Suggested", (int64)ENiagaraMenuSections::Suggested },
				{ "ENiagaraMenuSections::General", (int64)ENiagaraMenuSections::General },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "General.Name", "ENiagaraMenuSections::General" },
				{ "ModuleRelativePath", "Public/NiagaraActions.h" },
				{ "Suggested.Name", "ENiagaraMenuSections::Suggested" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraMenuSections",
				"ENiagaraMenuSections",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FNiagaraAction_NewNode>() == std::is_polymorphic<FNiagaraMenuAction_Generic>(), "USTRUCT FNiagaraAction_NewNode cannot be polymorphic unless super FNiagaraMenuAction_Generic is polymorphic");

class UScriptStruct* FNiagaraAction_NewNode::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraAction_NewNode, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraAction_NewNode"), sizeof(FNiagaraAction_NewNode), Get_Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraAction_NewNode>()
{
	return FNiagaraAction_NewNode::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraAction_NewNode(FNiagaraAction_NewNode::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraAction_NewNode"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraAction_NewNode
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraAction_NewNode()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraAction_NewNode>(FName(TEXT("NiagaraAction_NewNode")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraAction_NewNode;
	struct Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeTemplate_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NodeTemplate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraActions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraAction_NewNode>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::NewProp_NodeTemplate_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraActions.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::NewProp_NodeTemplate = { "NodeTemplate", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraAction_NewNode, NodeTemplate), Z_Construct_UClass_UEdGraphNode_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::NewProp_NodeTemplate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::NewProp_NodeTemplate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::NewProp_NodeTemplate,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic,
		&NewStructOps,
		"NiagaraAction_NewNode",
		sizeof(FNiagaraAction_NewNode),
		alignof(FNiagaraAction_NewNode),
		Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraAction_NewNode()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraAction_NewNode"), sizeof(FNiagaraAction_NewNode), Get_Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraAction_NewNode_Hash() { return 1247740431U; }

static_assert(std::is_polymorphic<FNiagaraMenuAction_Generic>() == std::is_polymorphic<FNiagaraMenuAction_Base>(), "USTRUCT FNiagaraMenuAction_Generic cannot be polymorphic unless super FNiagaraMenuAction_Base is polymorphic");

class UScriptStruct* FNiagaraMenuAction_Generic::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraMenuAction_Generic"), sizeof(FNiagaraMenuAction_Generic), Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraMenuAction_Generic>()
{
	return FNiagaraMenuAction_Generic::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraMenuAction_Generic(FNiagaraMenuAction_Generic::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraMenuAction_Generic"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraMenuAction_Generic
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraMenuAction_Generic()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraMenuAction_Generic>(FName(TEXT("NiagaraMenuAction_Generic")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraMenuAction_Generic;
	struct Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraActions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraMenuAction_Generic>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		Z_Construct_UScriptStruct_FNiagaraMenuAction_Base,
		&NewStructOps,
		"NiagaraMenuAction_Generic",
		sizeof(FNiagaraMenuAction_Generic),
		alignof(FNiagaraMenuAction_Generic),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraMenuAction_Generic"), sizeof(FNiagaraMenuAction_Generic), Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Generic_Hash() { return 2931763295U; }
class UScriptStruct* FNiagaraMenuAction_Base::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraMenuAction_Base, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraMenuAction_Base"), sizeof(FNiagaraMenuAction_Base), Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraMenuAction_Base>()
{
	return FNiagaraMenuAction_Base::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraMenuAction_Base(FNiagaraMenuAction_Base::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraMenuAction_Base"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraMenuAction_Base
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraMenuAction_Base()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraMenuAction_Base>(FName(TEXT("NiagaraMenuAction_Base")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraMenuAction_Base;
	struct Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// new action hierarchy for the new menus. Prefer inheriting from this rather than the above\n// this action does not have any use; inherit from it and provide your own functionality\n" },
		{ "ModuleRelativePath", "Public/NiagaraActions.h" },
		{ "ToolTip", "new action hierarchy for the new menus. Prefer inheriting from this rather than the above\nthis action does not have any use; inherit from it and provide your own functionality" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraMenuAction_Base>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraMenuAction_Base",
		sizeof(FNiagaraMenuAction_Base),
		alignof(FNiagaraMenuAction_Base),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraMenuAction_Base()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraMenuAction_Base"), sizeof(FNiagaraMenuAction_Base), Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Base_Hash() { return 15119470U; }

static_assert(std::is_polymorphic<FNiagaraMenuAction>() == std::is_polymorphic<FEdGraphSchemaAction>(), "USTRUCT FNiagaraMenuAction cannot be polymorphic unless super FEdGraphSchemaAction is polymorphic");

class UScriptStruct* FNiagaraMenuAction::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraMenuAction, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraMenuAction"), sizeof(FNiagaraMenuAction), Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraMenuAction>()
{
	return FNiagaraMenuAction::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraMenuAction(FNiagaraMenuAction::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraMenuAction"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraMenuAction
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraMenuAction()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraMenuAction>(FName(TEXT("NiagaraMenuAction")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraMenuAction;
	struct Z_Construct_UScriptStruct_FNiagaraMenuAction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraMenuAction_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraActions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraMenuAction_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraMenuAction>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraMenuAction_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		Z_Construct_UScriptStruct_FEdGraphSchemaAction,
		&NewStructOps,
		"NiagaraMenuAction",
		sizeof(FNiagaraMenuAction),
		alignof(FNiagaraMenuAction),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraMenuAction_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraMenuAction_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraMenuAction()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraMenuAction"), sizeof(FNiagaraMenuAction), Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraMenuAction_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraMenuAction_Hash() { return 2818806892U; }
class UScriptStruct* FNiagaraActionSourceData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraActionSourceData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraActionSourceData, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraActionSourceData"), sizeof(FNiagaraActionSourceData), Get_Z_Construct_UScriptStruct_FNiagaraActionSourceData_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraActionSourceData>()
{
	return FNiagaraActionSourceData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraActionSourceData(FNiagaraActionSourceData::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraActionSourceData"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraActionSourceData
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraActionSourceData()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraActionSourceData>(FName(TEXT("NiagaraActionSourceData")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraActionSourceData;
	struct Z_Construct_UScriptStruct_FNiagaraActionSourceData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraActionSourceData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraActions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraActionSourceData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraActionSourceData>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraActionSourceData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraActionSourceData",
		sizeof(FNiagaraActionSourceData),
		alignof(FNiagaraActionSourceData),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraActionSourceData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraActionSourceData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraActionSourceData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraActionSourceData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraActionSourceData"), sizeof(FNiagaraActionSourceData), Get_Z_Construct_UScriptStruct_FNiagaraActionSourceData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraActionSourceData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraActionSourceData_Hash() { return 238784227U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
