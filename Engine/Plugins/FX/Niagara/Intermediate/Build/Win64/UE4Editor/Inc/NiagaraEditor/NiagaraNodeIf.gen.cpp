// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraNodeIf.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeIf() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FPinGuidsForPath();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeIf_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeIf();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeWithDynamicPins();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariable();
// End Cross Module References
class UScriptStruct* FPinGuidsForPath::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FPinGuidsForPath_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPinGuidsForPath, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("PinGuidsForPath"), sizeof(FPinGuidsForPath), Get_Z_Construct_UScriptStruct_FPinGuidsForPath_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FPinGuidsForPath>()
{
	return FPinGuidsForPath::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPinGuidsForPath(FPinGuidsForPath::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("PinGuidsForPath"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFPinGuidsForPath
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFPinGuidsForPath()
	{
		UScriptStruct::DeferCppStructOps<FPinGuidsForPath>(FName(TEXT("PinGuidsForPath")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFPinGuidsForPath;
	struct Z_Construct_UScriptStruct_FPinGuidsForPath_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputPinGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputPinGuid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputTruePinGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputTruePinGuid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputFalsePinGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputFalsePinGuid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeIf.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPinGuidsForPath>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_OutputPinGuid_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeIf.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_OutputPinGuid = { "OutputPinGuid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPinGuidsForPath, OutputPinGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_OutputPinGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_OutputPinGuid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_InputTruePinGuid_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeIf.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_InputTruePinGuid = { "InputTruePinGuid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPinGuidsForPath, InputTruePinGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_InputTruePinGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_InputTruePinGuid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_InputFalsePinGuid_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeIf.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_InputFalsePinGuid = { "InputFalsePinGuid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPinGuidsForPath, InputFalsePinGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_InputFalsePinGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_InputFalsePinGuid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_OutputPinGuid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_InputTruePinGuid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::NewProp_InputFalsePinGuid,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"PinGuidsForPath",
		sizeof(FPinGuidsForPath),
		alignof(FPinGuidsForPath),
		Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPinGuidsForPath()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPinGuidsForPath_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PinGuidsForPath"), sizeof(FPinGuidsForPath), Get_Z_Construct_UScriptStruct_FPinGuidsForPath_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPinGuidsForPath_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPinGuidsForPath_Hash() { return 3046921600U; }
	void UNiagaraNodeIf::StaticRegisterNativesUNiagaraNodeIf()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeIf_NoRegister()
	{
		return UNiagaraNodeIf::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeIf_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputVars_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputVars_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutputVars;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PathAssociatedPinGuids_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathAssociatedPinGuids_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PathAssociatedPinGuids;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConditionPinGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConditionPinGuid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeIf_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNodeWithDynamicPins,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeIf_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraNodeIf.h" },
		{ "ModuleRelativePath", "Private/NiagaraNodeIf.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_OutputVars_Inner = { "OutputVars", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraVariable, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_OutputVars_MetaData[] = {
		{ "Comment", "/** Outputs of this branch. */" },
		{ "ModuleRelativePath", "Private/NiagaraNodeIf.h" },
		{ "ToolTip", "Outputs of this branch." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_OutputVars = { "OutputVars", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeIf, OutputVars), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_OutputVars_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_OutputVars_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_PathAssociatedPinGuids_Inner = { "PathAssociatedPinGuids", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPinGuidsForPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_PathAssociatedPinGuids_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeIf.h" },
		{ "SkipForCompileHash", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_PathAssociatedPinGuids = { "PathAssociatedPinGuids", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeIf, PathAssociatedPinGuids), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_PathAssociatedPinGuids_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_PathAssociatedPinGuids_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_ConditionPinGuid_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeIf.h" },
		{ "SkipForCompileHash", "true" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_ConditionPinGuid = { "ConditionPinGuid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeIf, ConditionPinGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_ConditionPinGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_ConditionPinGuid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNodeIf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_OutputVars_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_OutputVars,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_PathAssociatedPinGuids_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_PathAssociatedPinGuids,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeIf_Statics::NewProp_ConditionPinGuid,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeIf_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeIf>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeIf_Statics::ClassParams = {
		&UNiagaraNodeIf::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNodeIf_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeIf_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeIf_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeIf_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeIf()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeIf_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeIf, 1084955397);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeIf>()
	{
		return UNiagaraNodeIf::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeIf(Z_Construct_UClass_UNiagaraNodeIf, &UNiagaraNodeIf::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeIf"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeIf);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
