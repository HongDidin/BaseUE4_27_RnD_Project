// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraSystemEditorData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraSystemEditorData() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraSystemEditorFolder_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraSystemEditorFolder();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraSystemEditorData_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraSystemEditorData();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraEditorDataBase();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEditorData_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraph_NoRegister();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraGraphViewSettings();
// End Cross Module References
	void UNiagaraSystemEditorFolder::StaticRegisterNativesUNiagaraSystemEditorFolder()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraSystemEditorFolder_NoRegister()
	{
		return UNiagaraSystemEditorFolder::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FolderName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_FolderName;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ChildFolders_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChildFolders_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ChildFolders;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChildEmitterHandleIds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChildEmitterHandleIds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ChildEmitterHandleIds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Editor only folder data for emitters in a system. */" },
		{ "IncludePath", "NiagaraSystemEditorData.h" },
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
		{ "ToolTip", "Editor only folder data for emitters in a system." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_FolderName_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_FolderName = { "FolderName", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemEditorFolder, FolderName), METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_FolderName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_FolderName_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildFolders_Inner = { "ChildFolders", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraSystemEditorFolder_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildFolders_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildFolders = { "ChildFolders", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemEditorFolder, ChildFolders), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildFolders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildFolders_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildEmitterHandleIds_Inner = { "ChildEmitterHandleIds", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildEmitterHandleIds_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildEmitterHandleIds = { "ChildEmitterHandleIds", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemEditorFolder, ChildEmitterHandleIds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildEmitterHandleIds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildEmitterHandleIds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_FolderName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildFolders_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildFolders,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildEmitterHandleIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::NewProp_ChildEmitterHandleIds,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraSystemEditorFolder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::ClassParams = {
		&UNiagaraSystemEditorFolder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraSystemEditorFolder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraSystemEditorFolder, 4147291693);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraSystemEditorFolder>()
	{
		return UNiagaraSystemEditorFolder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraSystemEditorFolder(Z_Construct_UClass_UNiagaraSystemEditorFolder, &UNiagaraSystemEditorFolder::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraSystemEditorFolder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraSystemEditorFolder);
	void UNiagaraSystemEditorData::StaticRegisterNativesUNiagaraSystemEditorData()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraSystemEditorData_NoRegister()
	{
		return UNiagaraSystemEditorData::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraSystemEditorData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSetOrbitModeByAsset_MetaData[];
#endif
		static void NewProp_bSetOrbitModeByAsset_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSetOrbitModeByAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSystemViewportInOrbitMode_MetaData[];
#endif
		static void NewProp_bSystemViewportInOrbitMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSystemViewportInOrbitMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootFolder_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RootFolder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StackEditorData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StackEditorData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OwnerTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaybackRangeMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlaybackRangeMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaybackRangeMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlaybackRangeMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SystemOverviewGraph_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SystemOverviewGraph;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverviewGraphViewSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OverviewGraphViewSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSystemIsPlaceholder_MetaData[];
#endif
		static void NewProp_bSystemIsPlaceholder_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSystemIsPlaceholder;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraSystemEditorData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraEditorDataBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Editor only data for systems. */" },
		{ "IncludePath", "NiagaraSystemEditorData.h" },
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Editor only data for systems." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSetOrbitModeByAsset_MetaData[] = {
		{ "Comment", "// If true then the preview viewport's orbit setting is saved in the asset data\n" },
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
		{ "ToolTip", "If true then the preview viewport's orbit setting is saved in the asset data" },
	};
#endif
	void Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSetOrbitModeByAsset_SetBit(void* Obj)
	{
		((UNiagaraSystemEditorData*)Obj)->bSetOrbitModeByAsset = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSetOrbitModeByAsset = { "bSetOrbitModeByAsset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraSystemEditorData), &Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSetOrbitModeByAsset_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSetOrbitModeByAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSetOrbitModeByAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemViewportInOrbitMode_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	void Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemViewportInOrbitMode_SetBit(void* Obj)
	{
		((UNiagaraSystemEditorData*)Obj)->bSystemViewportInOrbitMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemViewportInOrbitMode = { "bSystemViewportInOrbitMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraSystemEditorData), &Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemViewportInOrbitMode_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemViewportInOrbitMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemViewportInOrbitMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_RootFolder_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_RootFolder = { "RootFolder", nullptr, (EPropertyFlags)0x0042000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemEditorData, RootFolder), Z_Construct_UClass_UNiagaraSystemEditorFolder_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_RootFolder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_RootFolder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_StackEditorData_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_StackEditorData = { "StackEditorData", nullptr, (EPropertyFlags)0x0042000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemEditorData, StackEditorData), Z_Construct_UClass_UNiagaraStackEditorData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_StackEditorData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_StackEditorData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_OwnerTransform_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_OwnerTransform = { "OwnerTransform", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemEditorData, OwnerTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_OwnerTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_OwnerTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_PlaybackRangeMin_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_PlaybackRangeMin = { "PlaybackRangeMin", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemEditorData, PlaybackRangeMin), METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_PlaybackRangeMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_PlaybackRangeMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_PlaybackRangeMax_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_PlaybackRangeMax = { "PlaybackRangeMax", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemEditorData, PlaybackRangeMax), METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_PlaybackRangeMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_PlaybackRangeMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_SystemOverviewGraph_MetaData[] = {
		{ "Comment", "/** Graph presenting overview of the current system and its emitters. */" },
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
		{ "ToolTip", "Graph presenting overview of the current system and its emitters." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_SystemOverviewGraph = { "SystemOverviewGraph", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemEditorData, SystemOverviewGraph), Z_Construct_UClass_UEdGraph_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_SystemOverviewGraph_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_SystemOverviewGraph_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_OverviewGraphViewSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_OverviewGraphViewSettings = { "OverviewGraphViewSettings", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraSystemEditorData, OverviewGraphViewSettings), Z_Construct_UScriptStruct_FNiagaraGraphViewSettings, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_OverviewGraphViewSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_OverviewGraphViewSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemIsPlaceholder_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraSystemEditorData.h" },
	};
#endif
	void Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemIsPlaceholder_SetBit(void* Obj)
	{
		((UNiagaraSystemEditorData*)Obj)->bSystemIsPlaceholder = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemIsPlaceholder = { "bSystemIsPlaceholder", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraSystemEditorData), &Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemIsPlaceholder_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemIsPlaceholder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemIsPlaceholder_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraSystemEditorData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSetOrbitModeByAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemViewportInOrbitMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_RootFolder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_StackEditorData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_OwnerTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_PlaybackRangeMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_PlaybackRangeMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_SystemOverviewGraph,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_OverviewGraphViewSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraSystemEditorData_Statics::NewProp_bSystemIsPlaceholder,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraSystemEditorData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraSystemEditorData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraSystemEditorData_Statics::ClassParams = {
		&UNiagaraSystemEditorData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraSystemEditorData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemEditorData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraSystemEditorData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraSystemEditorData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraSystemEditorData, 202278030);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraSystemEditorData>()
	{
		return UNiagaraSystemEditorData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraSystemEditorData(Z_Construct_UClass_UNiagaraSystemEditorData, &UNiagaraSystemEditorData::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraSystemEditorData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraSystemEditorData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
