// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraThumbnailRenderer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraThumbnailRenderer() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraThumbnailRendererBase_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraThumbnailRendererBase();
	UNREALED_API UClass* Z_Construct_UClass_UTextureThumbnailRenderer();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraSystemThumbnailRenderer();
// End Cross Module References
	void UNiagaraThumbnailRendererBase::StaticRegisterNativesUNiagaraThumbnailRendererBase()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraThumbnailRendererBase_NoRegister()
	{
		return UNiagaraThumbnailRendererBase::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraThumbnailRendererBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraThumbnailRendererBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTextureThumbnailRenderer,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraThumbnailRendererBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraThumbnailRenderer.h" },
		{ "ModuleRelativePath", "Private/NiagaraThumbnailRenderer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraThumbnailRendererBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraThumbnailRendererBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraThumbnailRendererBase_Statics::ClassParams = {
		&UNiagaraThumbnailRendererBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraThumbnailRendererBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraThumbnailRendererBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraThumbnailRendererBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraThumbnailRendererBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraThumbnailRendererBase, 276367049);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraThumbnailRendererBase>()
	{
		return UNiagaraThumbnailRendererBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraThumbnailRendererBase(Z_Construct_UClass_UNiagaraThumbnailRendererBase, &UNiagaraThumbnailRendererBase::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraThumbnailRendererBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraThumbnailRendererBase);
	void UNiagaraEmitterThumbnailRenderer::StaticRegisterNativesUNiagaraEmitterThumbnailRenderer()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_NoRegister()
	{
		return UNiagaraEmitterThumbnailRenderer::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraThumbnailRendererBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraThumbnailRenderer.h" },
		{ "ModuleRelativePath", "Private/NiagaraThumbnailRenderer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraEmitterThumbnailRenderer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_Statics::ClassParams = {
		&UNiagaraEmitterThumbnailRenderer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraEmitterThumbnailRenderer, 1115514832);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraEmitterThumbnailRenderer>()
	{
		return UNiagaraEmitterThumbnailRenderer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraEmitterThumbnailRenderer(Z_Construct_UClass_UNiagaraEmitterThumbnailRenderer, &UNiagaraEmitterThumbnailRenderer::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraEmitterThumbnailRenderer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraEmitterThumbnailRenderer);
	void UNiagaraSystemThumbnailRenderer::StaticRegisterNativesUNiagaraSystemThumbnailRenderer()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_NoRegister()
	{
		return UNiagaraSystemThumbnailRenderer::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraThumbnailRendererBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraThumbnailRenderer.h" },
		{ "ModuleRelativePath", "Private/NiagaraThumbnailRenderer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraSystemThumbnailRenderer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_Statics::ClassParams = {
		&UNiagaraSystemThumbnailRenderer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraSystemThumbnailRenderer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraSystemThumbnailRenderer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraSystemThumbnailRenderer, 2565700951);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraSystemThumbnailRenderer>()
	{
		return UNiagaraSystemThumbnailRenderer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraSystemThumbnailRenderer(Z_Construct_UClass_UNiagaraSystemThumbnailRenderer, &UNiagaraSystemThumbnailRenderer::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraSystemThumbnailRenderer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraSystemThumbnailRenderer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
