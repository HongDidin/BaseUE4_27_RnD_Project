// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/NiagaraCurveSelectionViewModel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraCurveSelectionViewModel() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraCurveSelectionViewModel_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraCurveSelectionViewModel();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraCurveSelectionViewModel::StaticRegisterNativesUNiagaraCurveSelectionViewModel()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraCurveSelectionViewModel_NoRegister()
	{
		return UNiagaraCurveSelectionViewModel::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraCurveSelectionViewModel_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraCurveSelectionViewModel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraCurveSelectionViewModel_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/NiagaraCurveSelectionViewModel.h" },
		{ "ModuleRelativePath", "Public/ViewModels/NiagaraCurveSelectionViewModel.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraCurveSelectionViewModel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraCurveSelectionViewModel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraCurveSelectionViewModel_Statics::ClassParams = {
		&UNiagaraCurveSelectionViewModel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraCurveSelectionViewModel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraCurveSelectionViewModel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraCurveSelectionViewModel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraCurveSelectionViewModel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraCurveSelectionViewModel, 1028813024);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraCurveSelectionViewModel>()
	{
		return UNiagaraCurveSelectionViewModel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraCurveSelectionViewModel(Z_Construct_UClass_UNiagaraCurveSelectionViewModel, &UNiagaraCurveSelectionViewModel::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraCurveSelectionViewModel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraCurveSelectionViewModel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
