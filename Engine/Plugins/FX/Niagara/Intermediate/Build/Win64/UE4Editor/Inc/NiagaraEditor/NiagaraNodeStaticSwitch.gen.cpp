// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraNodeStaticSwitch.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeStaticSwitch() {}
// Cross Module References
	NIAGARAEDITOR_API UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraStaticSwitchType();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FStaticSwitchTypeData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UEnum();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeStaticSwitch_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeStaticSwitch();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeUsageSelector();
// End Cross Module References
	static UEnum* ENiagaraStaticSwitchType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NiagaraEditor_ENiagaraStaticSwitchType, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ENiagaraStaticSwitchType"));
		}
		return Singleton;
	}
	template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraStaticSwitchType>()
	{
		return ENiagaraStaticSwitchType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENiagaraStaticSwitchType(ENiagaraStaticSwitchType_StaticEnum, TEXT("/Script/NiagaraEditor"), TEXT("ENiagaraStaticSwitchType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraStaticSwitchType_Hash() { return 3445879013U; }
	UEnum* Z_Construct_UEnum_NiagaraEditor_ENiagaraStaticSwitchType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENiagaraStaticSwitchType"), 0, Get_Z_Construct_UEnum_NiagaraEditor_ENiagaraStaticSwitchType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENiagaraStaticSwitchType::Bool", (int64)ENiagaraStaticSwitchType::Bool },
				{ "ENiagaraStaticSwitchType::Integer", (int64)ENiagaraStaticSwitchType::Integer },
				{ "ENiagaraStaticSwitchType::Enum", (int64)ENiagaraStaticSwitchType::Enum },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Bool.Name", "ENiagaraStaticSwitchType::Bool" },
				{ "Enum.Name", "ENiagaraStaticSwitchType::Enum" },
				{ "Integer.Name", "ENiagaraStaticSwitchType::Integer" },
				{ "ModuleRelativePath", "Private/NiagaraNodeStaticSwitch.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NiagaraEditor,
				nullptr,
				"ENiagaraStaticSwitchType",
				"ENiagaraStaticSwitchType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FStaticSwitchTypeData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FStaticSwitchTypeData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FStaticSwitchTypeData, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("StaticSwitchTypeData"), sizeof(FStaticSwitchTypeData), Get_Z_Construct_UScriptStruct_FStaticSwitchTypeData_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FStaticSwitchTypeData>()
{
	return FStaticSwitchTypeData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FStaticSwitchTypeData(FStaticSwitchTypeData::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("StaticSwitchTypeData"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFStaticSwitchTypeData
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFStaticSwitchTypeData()
	{
		UScriptStruct::DeferCppStructOps<FStaticSwitchTypeData>(FName(TEXT("StaticSwitchTypeData")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFStaticSwitchTypeData;
	struct Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SwitchType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SwitchType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SwitchType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Enum_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Enum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SwitchConstant_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SwitchConstant;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoRefreshEnabled_MetaData[];
#endif
		static void NewProp_bAutoRefreshEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoRefreshEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeStaticSwitch.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FStaticSwitchTypeData>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchType_MetaData[] = {
		{ "Comment", "/** This determines how the switch value is interpreted */" },
		{ "ModuleRelativePath", "Private/NiagaraNodeStaticSwitch.h" },
		{ "ToolTip", "This determines how the switch value is interpreted" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchType = { "SwitchType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStaticSwitchTypeData, SwitchType), Z_Construct_UEnum_NiagaraEditor_ENiagaraStaticSwitchType, METADATA_PARAMS(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_Enum_MetaData[] = {
		{ "Comment", "/** If the type is enum, this is the enum being switched on, otherwise it holds no sensible value */" },
		{ "ModuleRelativePath", "Private/NiagaraNodeStaticSwitch.h" },
		{ "ToolTip", "If the type is enum, this is the enum being switched on, otherwise it holds no sensible value" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_Enum = { "Enum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStaticSwitchTypeData, Enum), Z_Construct_UClass_UEnum, METADATA_PARAMS(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_Enum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_Enum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchConstant_MetaData[] = {
		{ "Comment", "/** If set, then this switch is not exposed but will rather be evaluated by the given compile-time constant */" },
		{ "ModuleRelativePath", "Private/NiagaraNodeStaticSwitch.h" },
		{ "ToolTip", "If set, then this switch is not exposed but will rather be evaluated by the given compile-time constant" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchConstant = { "SwitchConstant", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStaticSwitchTypeData, SwitchConstant), METADATA_PARAMS(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchConstant_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchConstant_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_bAutoRefreshEnabled_MetaData[] = {
		{ "Comment", "/** If true, a node will auto refresh under certain circumstances, like in post load or if the assigned enum changes */" },
		{ "ModuleRelativePath", "Private/NiagaraNodeStaticSwitch.h" },
		{ "ToolTip", "If true, a node will auto refresh under certain circumstances, like in post load or if the assigned enum changes" },
	};
#endif
	void Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_bAutoRefreshEnabled_SetBit(void* Obj)
	{
		((FStaticSwitchTypeData*)Obj)->bAutoRefreshEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_bAutoRefreshEnabled = { "bAutoRefreshEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FStaticSwitchTypeData), &Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_bAutoRefreshEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_bAutoRefreshEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_bAutoRefreshEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_Enum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_SwitchConstant,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::NewProp_bAutoRefreshEnabled,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"StaticSwitchTypeData",
		sizeof(FStaticSwitchTypeData),
		alignof(FStaticSwitchTypeData),
		Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FStaticSwitchTypeData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FStaticSwitchTypeData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("StaticSwitchTypeData"), sizeof(FStaticSwitchTypeData), Get_Z_Construct_UScriptStruct_FStaticSwitchTypeData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FStaticSwitchTypeData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FStaticSwitchTypeData_Hash() { return 1146205571U; }
	void UNiagaraNodeStaticSwitch::StaticRegisterNativesUNiagaraNodeStaticSwitch()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeStaticSwitch_NoRegister()
	{
		return UNiagaraNodeStaticSwitch::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputParameterName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InputParameterName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SwitchTypeData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SwitchTypeData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNodeUsageSelector,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraNodeStaticSwitch.h" },
		{ "ModuleRelativePath", "Private/NiagaraNodeStaticSwitch.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::NewProp_InputParameterName_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeStaticSwitch.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::NewProp_InputParameterName = { "InputParameterName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeStaticSwitch, InputParameterName), METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::NewProp_InputParameterName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::NewProp_InputParameterName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::NewProp_SwitchTypeData_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeStaticSwitch.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::NewProp_SwitchTypeData = { "SwitchTypeData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeStaticSwitch, SwitchTypeData), Z_Construct_UScriptStruct_FStaticSwitchTypeData, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::NewProp_SwitchTypeData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::NewProp_SwitchTypeData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::NewProp_InputParameterName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::NewProp_SwitchTypeData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeStaticSwitch>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::ClassParams = {
		&UNiagaraNodeStaticSwitch::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeStaticSwitch()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeStaticSwitch_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeStaticSwitch, 990184638);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeStaticSwitch>()
	{
		return UNiagaraNodeStaticSwitch::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeStaticSwitch(Z_Construct_UClass_UNiagaraNodeStaticSwitch, &UNiagaraNodeStaticSwitch::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeStaticSwitch"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeStaticSwitch);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
