// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackEditorData_generated_h
#error "NiagaraStackEditorData.generated.h already included, missing '#pragma once' in NiagaraStackEditorData.h"
#endif
#define NIAGARAEDITOR_NiagaraStackEditorData_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackEditorData(); \
	friend struct Z_Construct_UClass_UNiagaraStackEditorData_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEditorData, UNiagaraEditorDataBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEditorData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackEditorData(); \
	friend struct Z_Construct_UClass_UNiagaraStackEditorData_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEditorData, UNiagaraEditorDataBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEditorData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackEditorData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackEditorData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEditorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEditorData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEditorData(UNiagaraStackEditorData&&); \
	NO_API UNiagaraStackEditorData(const UNiagaraStackEditorData&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackEditorData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEditorData(UNiagaraStackEditorData&&); \
	NO_API UNiagaraStackEditorData(const UNiagaraStackEditorData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEditorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEditorData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackEditorData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StackEntryKeyToDisplayName() { return STRUCT_OFFSET(UNiagaraStackEditorData, StackEntryKeyToDisplayName); } \
	FORCEINLINE static uint32 __PPO__DismissedStackIssueIds() { return STRUCT_OFFSET(UNiagaraStackEditorData, DismissedStackIssueIds); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_11_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackEditorData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraStackEditorData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
