// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_EdGraphSchema_Niagara_generated_h
#error "EdGraphSchema_Niagara.generated.h already included, missing '#pragma once' in EdGraphSchema_Niagara.h"
#endif
#define NIAGARAEDITOR_EdGraphSchema_Niagara_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_62_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewComment_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FEdGraphSchemaAction Super;


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraSchemaAction_NewComment>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_23_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraSchemaAction_NewNode_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FEdGraphSchemaAction Super;


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraSchemaAction_NewNode>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEdGraphSchema_Niagara(); \
	friend struct Z_Construct_UClass_UEdGraphSchema_Niagara_Statics; \
public: \
	DECLARE_CLASS(UEdGraphSchema_Niagara, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UEdGraphSchema_Niagara)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_INCLASS \
private: \
	static void StaticRegisterNativesUEdGraphSchema_Niagara(); \
	friend struct Z_Construct_UClass_UEdGraphSchema_Niagara_Statics; \
public: \
	DECLARE_CLASS(UEdGraphSchema_Niagara, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UEdGraphSchema_Niagara)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEdGraphSchema_Niagara(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEdGraphSchema_Niagara) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEdGraphSchema_Niagara); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEdGraphSchema_Niagara); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEdGraphSchema_Niagara(UEdGraphSchema_Niagara&&); \
	NO_API UEdGraphSchema_Niagara(const UEdGraphSchema_Niagara&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEdGraphSchema_Niagara(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEdGraphSchema_Niagara(UEdGraphSchema_Niagara&&); \
	NO_API UEdGraphSchema_Niagara(const UEdGraphSchema_Niagara&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEdGraphSchema_Niagara); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEdGraphSchema_Niagara); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEdGraphSchema_Niagara)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_83_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h_86_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class EdGraphSchema_Niagara."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UEdGraphSchema_Niagara>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_EdGraphSchema_Niagara_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
