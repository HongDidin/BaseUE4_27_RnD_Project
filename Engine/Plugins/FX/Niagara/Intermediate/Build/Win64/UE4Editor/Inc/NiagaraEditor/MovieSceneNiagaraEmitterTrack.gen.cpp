// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/Sequencer/NiagaraSequence/MovieSceneNiagaraEmitterTrack.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneNiagaraEmitterTrack() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneNameableTrack();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	void UMovieSceneNiagaraEmitterSectionBase::StaticRegisterNativesUMovieSceneNiagaraEmitterSectionBase()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_NoRegister()
	{
		return UMovieSceneNiagaraEmitterSectionBase::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneSection,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sequencer/NiagaraSequence/MovieSceneNiagaraEmitterTrack.h" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/MovieSceneNiagaraEmitterTrack.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneNiagaraEmitterSectionBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_Statics::ClassParams = {
		&UMovieSceneNiagaraEmitterSectionBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x002800A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneNiagaraEmitterSectionBase, 3518405089);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UMovieSceneNiagaraEmitterSectionBase>()
	{
		return UMovieSceneNiagaraEmitterSectionBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneNiagaraEmitterSectionBase(Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase, &UMovieSceneNiagaraEmitterSectionBase::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UMovieSceneNiagaraEmitterSectionBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneNiagaraEmitterSectionBase);
	void UMovieSceneNiagaraEmitterTrack::StaticRegisterNativesUMovieSceneNiagaraEmitterTrack()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_NoRegister()
	{
		return UMovieSceneNiagaraEmitterTrack::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Sections_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sections_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Sections;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSectionsWereModified_MetaData[];
#endif
		static void NewProp_bSectionsWereModified_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSectionsWereModified;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmitterHandleId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EmitterHandleId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SystemPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SystemPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneNameableTrack,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n*\x09""A track representing an emitter in the niagara effect editor timeline.\n*/" },
		{ "IncludePath", "Sequencer/NiagaraSequence/MovieSceneNiagaraEmitterTrack.h" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/MovieSceneNiagaraEmitterTrack.h" },
		{ "ToolTip", "A track representing an emitter in the niagara effect editor timeline." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_Sections_Inner = { "Sections", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneSection_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_Sections_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/MovieSceneNiagaraEmitterTrack.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_Sections = { "Sections", nullptr, (EPropertyFlags)0x0040008000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneNiagaraEmitterTrack, Sections), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_Sections_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_Sections_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_bSectionsWereModified_MetaData[] = {
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/MovieSceneNiagaraEmitterTrack.h" },
	};
#endif
	void Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_bSectionsWereModified_SetBit(void* Obj)
	{
		((UMovieSceneNiagaraEmitterTrack*)Obj)->bSectionsWereModified = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_bSectionsWereModified = { "bSectionsWereModified", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMovieSceneNiagaraEmitterTrack), &Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_bSectionsWereModified_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_bSectionsWereModified_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_bSectionsWereModified_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_EmitterHandleId_MetaData[] = {
		{ "Comment", "// Used for detecting copy/paste \n" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/MovieSceneNiagaraEmitterTrack.h" },
		{ "ToolTip", "Used for detecting copy/paste" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_EmitterHandleId = { "EmitterHandleId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneNiagaraEmitterTrack, EmitterHandleId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_EmitterHandleId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_EmitterHandleId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_SystemPath_MetaData[] = {
		{ "Comment", "// Used for detecting copy/paste\n" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/MovieSceneNiagaraEmitterTrack.h" },
		{ "ToolTip", "Used for detecting copy/paste" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_SystemPath = { "SystemPath", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneNiagaraEmitterTrack, SystemPath), METADATA_PARAMS(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_SystemPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_SystemPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_Sections_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_Sections,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_bSectionsWereModified,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_EmitterHandleId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::NewProp_SystemPath,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneNiagaraEmitterTrack>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::ClassParams = {
		&UMovieSceneNiagaraEmitterTrack::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::PropPointers),
		0,
		0x00A800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneNiagaraEmitterTrack, 1740833811);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UMovieSceneNiagaraEmitterTrack>()
	{
		return UMovieSceneNiagaraEmitterTrack::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneNiagaraEmitterTrack(Z_Construct_UClass_UMovieSceneNiagaraEmitterTrack, &UMovieSceneNiagaraEmitterTrack::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UMovieSceneNiagaraEmitterTrack"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneNiagaraEmitterTrack);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
