// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FQuat;
struct FLinearColor;
struct FVector4;
struct FVector;
struct FVector2D;
class UNiagaraPythonScriptModuleInput;
#ifdef NIAGARAEDITOR_UpgradeNiagaraScriptResults_generated_h
#error "UpgradeNiagaraScriptResults.generated.h already included, missing '#pragma once' in UpgradeNiagaraScriptResults.h"
#endif
#define NIAGARAEDITOR_UpgradeNiagaraScriptResults_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAsEnum); \
	DECLARE_FUNCTION(execAsQuat); \
	DECLARE_FUNCTION(execAsColor); \
	DECLARE_FUNCTION(execAsVec4); \
	DECLARE_FUNCTION(execAsVec3); \
	DECLARE_FUNCTION(execAsVec2); \
	DECLARE_FUNCTION(execAsBool); \
	DECLARE_FUNCTION(execAsInt); \
	DECLARE_FUNCTION(execAsFloat); \
	DECLARE_FUNCTION(execIsLocalValue); \
	DECLARE_FUNCTION(execIsSet);


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAsEnum); \
	DECLARE_FUNCTION(execAsQuat); \
	DECLARE_FUNCTION(execAsColor); \
	DECLARE_FUNCTION(execAsVec4); \
	DECLARE_FUNCTION(execAsVec3); \
	DECLARE_FUNCTION(execAsVec2); \
	DECLARE_FUNCTION(execAsBool); \
	DECLARE_FUNCTION(execAsInt); \
	DECLARE_FUNCTION(execAsFloat); \
	DECLARE_FUNCTION(execIsLocalValue); \
	DECLARE_FUNCTION(execIsSet);


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraPythonScriptModuleInput(); \
	friend struct Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics; \
public: \
	DECLARE_CLASS(UNiagaraPythonScriptModuleInput, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraPythonScriptModuleInput)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraPythonScriptModuleInput(); \
	friend struct Z_Construct_UClass_UNiagaraPythonScriptModuleInput_Statics; \
public: \
	DECLARE_CLASS(UNiagaraPythonScriptModuleInput, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraPythonScriptModuleInput)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraPythonScriptModuleInput(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraPythonScriptModuleInput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraPythonScriptModuleInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraPythonScriptModuleInput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraPythonScriptModuleInput(UNiagaraPythonScriptModuleInput&&); \
	NO_API UNiagaraPythonScriptModuleInput(const UNiagaraPythonScriptModuleInput&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraPythonScriptModuleInput(UNiagaraPythonScriptModuleInput&&); \
	NO_API UNiagaraPythonScriptModuleInput(const UNiagaraPythonScriptModuleInput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraPythonScriptModuleInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraPythonScriptModuleInput); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraPythonScriptModuleInput)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_34_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraPythonScriptModuleInput>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetEnumInput); \
	DECLARE_FUNCTION(execSetQuatInput); \
	DECLARE_FUNCTION(execSetColorInput); \
	DECLARE_FUNCTION(execSetVec4Input); \
	DECLARE_FUNCTION(execSetVec3Input); \
	DECLARE_FUNCTION(execSetVec2Input); \
	DECLARE_FUNCTION(execSetBoolInput); \
	DECLARE_FUNCTION(execSetIntInput); \
	DECLARE_FUNCTION(execSetFloatInput); \
	DECLARE_FUNCTION(execGetOldInput);


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetEnumInput); \
	DECLARE_FUNCTION(execSetQuatInput); \
	DECLARE_FUNCTION(execSetColorInput); \
	DECLARE_FUNCTION(execSetVec4Input); \
	DECLARE_FUNCTION(execSetVec3Input); \
	DECLARE_FUNCTION(execSetVec2Input); \
	DECLARE_FUNCTION(execSetBoolInput); \
	DECLARE_FUNCTION(execSetIntInput); \
	DECLARE_FUNCTION(execSetFloatInput); \
	DECLARE_FUNCTION(execGetOldInput);


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUpgradeNiagaraScriptResults(); \
	friend struct Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics; \
public: \
	DECLARE_CLASS(UUpgradeNiagaraScriptResults, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UUpgradeNiagaraScriptResults)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_INCLASS \
private: \
	static void StaticRegisterNativesUUpgradeNiagaraScriptResults(); \
	friend struct Z_Construct_UClass_UUpgradeNiagaraScriptResults_Statics; \
public: \
	DECLARE_CLASS(UUpgradeNiagaraScriptResults, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UUpgradeNiagaraScriptResults)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUpgradeNiagaraScriptResults(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUpgradeNiagaraScriptResults) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUpgradeNiagaraScriptResults); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUpgradeNiagaraScriptResults); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUpgradeNiagaraScriptResults(UUpgradeNiagaraScriptResults&&); \
	NO_API UUpgradeNiagaraScriptResults(const UUpgradeNiagaraScriptResults&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUpgradeNiagaraScriptResults(UUpgradeNiagaraScriptResults&&); \
	NO_API UUpgradeNiagaraScriptResults(const UUpgradeNiagaraScriptResults&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUpgradeNiagaraScriptResults); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUpgradeNiagaraScriptResults); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UUpgradeNiagaraScriptResults)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DummyInput() { return STRUCT_OFFSET(UUpgradeNiagaraScriptResults, DummyInput); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_82_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h_85_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UUpgradeNiagaraScriptResults>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_UpgradeNiagaraScriptResults_h


#define FOREACH_ENUM_ENIAGARAPYTHONSCRIPTINPUTSOURCE(op) \
	op(ENiagaraPythonScriptInputSource::Input) \
	op(ENiagaraPythonScriptInputSource::Output) \
	op(ENiagaraPythonScriptInputSource::Local) \
	op(ENiagaraPythonScriptInputSource::InputOutput) \
	op(ENiagaraPythonScriptInputSource::InitialValueInput) \
	op(ENiagaraPythonScriptInputSource::None) \
	op(ENiagaraPythonScriptInputSource::Num) 

enum class ENiagaraPythonScriptInputSource : uint32;
template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraPythonScriptInputSource>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
