// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraNodeWriteDataSet_generated_h
#error "NiagaraNodeWriteDataSet.generated.h already included, missing '#pragma once' in NiagaraNodeWriteDataSet.h"
#endif
#define NIAGARAEDITOR_NiagaraNodeWriteDataSet_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraNodeWriteDataSet(); \
	friend struct Z_Construct_UClass_UNiagaraNodeWriteDataSet_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeWriteDataSet, UNiagaraNodeDataSetBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeWriteDataSet)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraNodeWriteDataSet(); \
	friend struct Z_Construct_UClass_UNiagaraNodeWriteDataSet_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeWriteDataSet, UNiagaraNodeDataSetBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeWriteDataSet)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeWriteDataSet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeWriteDataSet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeWriteDataSet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeWriteDataSet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeWriteDataSet(UNiagaraNodeWriteDataSet&&); \
	NIAGARAEDITOR_API UNiagaraNodeWriteDataSet(const UNiagaraNodeWriteDataSet&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeWriteDataSet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeWriteDataSet(UNiagaraNodeWriteDataSet&&); \
	NIAGARAEDITOR_API UNiagaraNodeWriteDataSet(const UNiagaraNodeWriteDataSet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeWriteDataSet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeWriteDataSet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeWriteDataSet)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_12_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraNodeWriteDataSet."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraNodeWriteDataSet>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraNodeWriteDataSet_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
