// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraNodeUsageSelector_generated_h
#error "NiagaraNodeUsageSelector.generated.h already included, missing '#pragma once' in NiagaraNodeUsageSelector.h"
#endif
#define NIAGARAEDITOR_NiagaraNodeUsageSelector_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraNodeUsageSelector(); \
	friend struct Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeUsageSelector, UNiagaraNodeWithDynamicPins, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeUsageSelector)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraNodeUsageSelector(); \
	friend struct Z_Construct_UClass_UNiagaraNodeUsageSelector_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeUsageSelector, UNiagaraNodeWithDynamicPins, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UNiagaraNodeUsageSelector)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeUsageSelector(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeUsageSelector) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeUsageSelector); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeUsageSelector); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeUsageSelector(UNiagaraNodeUsageSelector&&); \
	NIAGARAEDITOR_API UNiagaraNodeUsageSelector(const UNiagaraNodeUsageSelector&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UNiagaraNodeUsageSelector(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UNiagaraNodeUsageSelector(UNiagaraNodeUsageSelector&&); \
	NIAGARAEDITOR_API UNiagaraNodeUsageSelector(const UNiagaraNodeUsageSelector&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UNiagaraNodeUsageSelector); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeUsageSelector); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeUsageSelector)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_8_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h_11_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraNodeUsageSelector."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraNodeUsageSelector>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeUsageSelector_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
