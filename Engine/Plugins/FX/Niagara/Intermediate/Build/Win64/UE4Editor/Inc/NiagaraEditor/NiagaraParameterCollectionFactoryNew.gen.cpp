// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraParameterCollectionFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraParameterCollectionFactoryNew() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew();
// End Cross Module References
	void UNiagaraParameterCollectionFactoryNew::StaticRegisterNativesUNiagaraParameterCollectionFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_NoRegister()
	{
		return UNiagaraParameterCollectionFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "NiagaraParameterCollectionFactoryNew.h" },
		{ "ModuleRelativePath", "Public/NiagaraParameterCollectionFactoryNew.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraParameterCollectionFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_Statics::ClassParams = {
		&UNiagaraParameterCollectionFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraParameterCollectionFactoryNew, 3306976833);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraParameterCollectionFactoryNew>()
	{
		return UNiagaraParameterCollectionFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraParameterCollectionFactoryNew(Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew, &UNiagaraParameterCollectionFactoryNew::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraParameterCollectionFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraParameterCollectionFactoryNew);
	void UNiagaraParameterCollectionInstanceFactoryNew::StaticRegisterNativesUNiagaraParameterCollectionInstanceFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_NoRegister()
	{
		return UNiagaraParameterCollectionInstanceFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "NiagaraParameterCollectionFactoryNew.h" },
		{ "ModuleRelativePath", "Public/NiagaraParameterCollectionFactoryNew.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraParameterCollectionInstanceFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_Statics::ClassParams = {
		&UNiagaraParameterCollectionInstanceFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraParameterCollectionInstanceFactoryNew, 1304589297);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraParameterCollectionInstanceFactoryNew>()
	{
		return UNiagaraParameterCollectionInstanceFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraParameterCollectionInstanceFactoryNew(Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew, &UNiagaraParameterCollectionInstanceFactoryNew::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraParameterCollectionInstanceFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraParameterCollectionInstanceFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
