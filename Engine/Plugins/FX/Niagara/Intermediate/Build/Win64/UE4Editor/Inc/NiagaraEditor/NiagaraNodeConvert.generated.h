// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraNodeConvert_generated_h
#error "NiagaraNodeConvert.generated.h already included, missing '#pragma once' in NiagaraNodeConvert.h"
#endif
#define NIAGARAEDITOR_NiagaraNodeConvert_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_51_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraConvertConnection>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraConvertPinRecord>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraNodeConvert(); \
	friend struct Z_Construct_UClass_UNiagaraNodeConvert_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeConvert, UNiagaraNodeWithDynamicPins, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraNodeConvert)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraNodeConvert(); \
	friend struct Z_Construct_UClass_UNiagaraNodeConvert_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeConvert, UNiagaraNodeWithDynamicPins, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraNodeConvert)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraNodeConvert(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeConvert) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraNodeConvert); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeConvert); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraNodeConvert(UNiagaraNodeConvert&&); \
	NO_API UNiagaraNodeConvert(const UNiagaraNodeConvert&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraNodeConvert(UNiagaraNodeConvert&&); \
	NO_API UNiagaraNodeConvert(const UNiagaraNodeConvert&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraNodeConvert); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeConvert); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraNodeConvert)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AutowireSwizzle() { return STRUCT_OFFSET(UNiagaraNodeConvert, AutowireSwizzle); } \
	FORCEINLINE static uint32 __PPO__AutowireMakeType() { return STRUCT_OFFSET(UNiagaraNodeConvert, AutowireMakeType); } \
	FORCEINLINE static uint32 __PPO__AutowireBreakType() { return STRUCT_OFFSET(UNiagaraNodeConvert, AutowireBreakType); } \
	FORCEINLINE static uint32 __PPO__Connections() { return STRUCT_OFFSET(UNiagaraNodeConvert, Connections); } \
	FORCEINLINE static uint32 __PPO__bIsWiringShown() { return STRUCT_OFFSET(UNiagaraNodeConvert, bIsWiringShown); } \
	FORCEINLINE static uint32 __PPO__ExpandedItems() { return STRUCT_OFFSET(UNiagaraNodeConvert, ExpandedItems); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_82_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h_86_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraNodeConvert>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeConvert_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
