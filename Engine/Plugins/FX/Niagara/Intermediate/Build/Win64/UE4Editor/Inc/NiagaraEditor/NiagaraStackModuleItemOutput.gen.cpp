// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackModuleItemOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackModuleItemOutput() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackModuleItemOutput_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackModuleItemOutput();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEntry();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraStackModuleItemOutput::StaticRegisterNativesUNiagaraStackModuleItemOutput()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackModuleItemOutput_NoRegister()
	{
		return UNiagaraStackModuleItemOutput::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackModuleItemOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackModuleItemOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackEntry,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackModuleItemOutput_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Represents a single module Output in the module stack view model. */" },
		{ "IncludePath", "ViewModels/Stack/NiagaraStackModuleItemOutput.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackModuleItemOutput.h" },
		{ "ToolTip", "Represents a single module Output in the module stack view model." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackModuleItemOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackModuleItemOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackModuleItemOutput_Statics::ClassParams = {
		&UNiagaraStackModuleItemOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackModuleItemOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackModuleItemOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackModuleItemOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackModuleItemOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackModuleItemOutput, 3633280255);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackModuleItemOutput>()
	{
		return UNiagaraStackModuleItemOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackModuleItemOutput(Z_Construct_UClass_UNiagaraStackModuleItemOutput, &UNiagaraStackModuleItemOutput::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackModuleItemOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackModuleItemOutput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
