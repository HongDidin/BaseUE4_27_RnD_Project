// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraGraph.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraGraph() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	NIAGARA_API UEnum* Z_Construct_UEnum_Niagara_ENiagaraScriptUsage();
	NIAGARACORE_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraCompileHash();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraCompileHashVisitorDebugInfo();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNode_NoRegister();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraGraphParameterReference();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraGraph_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraGraph();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraph();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariableMetaData();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariable();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScriptVariable_NoRegister();
// End Cross Module References
class UScriptStruct* FNiagaraGraphScriptUsageInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraGraphScriptUsageInfo"), sizeof(FNiagaraGraphScriptUsageInfo), Get_Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraGraphScriptUsageInfo>()
{
	return FNiagaraGraphScriptUsageInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraGraphScriptUsageInfo(FNiagaraGraphScriptUsageInfo::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraGraphScriptUsageInfo"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraGraphScriptUsageInfo
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraGraphScriptUsageInfo()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraGraphScriptUsageInfo>(FName(TEXT("NiagaraGraphScriptUsageInfo")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraGraphScriptUsageInfo;
	struct Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BaseId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_UsageType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UsageType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_UsageType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UsageId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UsageId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CompileHash_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CompileHash;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CompileHashFromGraph_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CompileHashFromGraph;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CompileLastObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CompileLastObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CompileLastObjects;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Traversal_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Traversal_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Traversal;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DataHash_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataHash_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DataHash;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeneratedCompileId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeneratedCompileId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Container for UNiagaraGraph cached data for managing CompileIds and Traversals.*/" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "Container for UNiagaraGraph cached data for managing CompileIds and Traversals." },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraGraphScriptUsageInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_BaseId_MetaData[] = {
		{ "Comment", "/** A guid which is generated when this usage info is created.  Allows for forced recompiling when the cached ids are invalidated. */" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "A guid which is generated when this usage info is created.  Allows for forced recompiling when the cached ids are invalidated." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_BaseId = { "BaseId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, BaseId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_BaseId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_BaseId_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageType_MetaData[] = {
		{ "Comment", "/** The context in which this sub-graph traversal will be used.*/" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "The context in which this sub-graph traversal will be used." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageType = { "UsageType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, UsageType), Z_Construct_UEnum_Niagara_ENiagaraScriptUsage, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageId_MetaData[] = {
		{ "Comment", "/** The particular instance of the usage type. Event scripts, for example, have potentially multiple graphs.*/" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "The particular instance of the usage type. Event scripts, for example, have potentially multiple graphs." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageId = { "UsageId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, UsageId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileHash_MetaData[] = {
		{ "Comment", "/** The hash that we calculated last traversal. */" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "The hash that we calculated last traversal." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileHash = { "CompileHash", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, CompileHash), Z_Construct_UScriptStruct_FNiagaraCompileHash, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileHash_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileHash_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileHashFromGraph_MetaData[] = {
		{ "Comment", "/** The hash that we calculated last traversal. */" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "The hash that we calculated last traversal." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileHashFromGraph = { "CompileHashFromGraph", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, CompileHashFromGraph), Z_Construct_UScriptStruct_FNiagaraCompileHash, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileHashFromGraph_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileHashFromGraph_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileLastObjects_Inner = { "CompileLastObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraCompileHashVisitorDebugInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileLastObjects_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileLastObjects = { "CompileLastObjects", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, CompileLastObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileLastObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileLastObjects_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_Traversal_Inner = { "Traversal", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraNode_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_Traversal_MetaData[] = {
		{ "Comment", "/** The traversal of output to input nodes for this graph. This is not a recursive traversal, it just includes nodes from this graph.*/" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "The traversal of output to input nodes for this graph. This is not a recursive traversal, it just includes nodes from this graph." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_Traversal = { "Traversal", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, Traversal), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_Traversal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_Traversal_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_DataHash_Inner = { "DataHash", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_DataHash_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_DataHash = { "DataHash", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, DataHash_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_DataHash_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_DataHash_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_GeneratedCompileId_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_GeneratedCompileId = { "GeneratedCompileId", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphScriptUsageInfo, GeneratedCompileId_DEPRECATED), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_GeneratedCompileId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_GeneratedCompileId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_BaseId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_UsageId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileHash,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileHashFromGraph,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileLastObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_CompileLastObjects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_Traversal_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_Traversal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_DataHash_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_DataHash,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::NewProp_GeneratedCompileId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraGraphScriptUsageInfo",
		sizeof(FNiagaraGraphScriptUsageInfo),
		alignof(FNiagaraGraphScriptUsageInfo),
		Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraGraphScriptUsageInfo"), sizeof(FNiagaraGraphScriptUsageInfo), Get_Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo_Hash() { return 2201632538U; }
class UScriptStruct* FNiagaraGraphParameterReferenceCollection::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraGraphParameterReferenceCollection"), sizeof(FNiagaraGraphParameterReferenceCollection), Get_Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraGraphParameterReferenceCollection>()
{
	return FNiagaraGraphParameterReferenceCollection::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraGraphParameterReferenceCollection(FNiagaraGraphParameterReferenceCollection::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraGraphParameterReferenceCollection"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraGraphParameterReferenceCollection
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraGraphParameterReferenceCollection()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraGraphParameterReferenceCollection>(FName(TEXT("NiagaraGraphParameterReferenceCollection")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraGraphParameterReferenceCollection;
	struct Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParameterReferences_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterReferences_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ParameterReferences;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Graph_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Graph;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCreatedByUser_MetaData[];
#endif
		static void NewProp_bCreatedByUser_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCreatedByUser;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraGraphParameterReferenceCollection>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_ParameterReferences_Inner = { "ParameterReferences", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraGraphParameterReference, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_ParameterReferences_MetaData[] = {
		{ "Comment", "/** All the references in the graph. */" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "All the references in the graph." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_ParameterReferences = { "ParameterReferences", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphParameterReferenceCollection, ParameterReferences), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_ParameterReferences_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_ParameterReferences_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_Graph_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_Graph = { "Graph", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphParameterReferenceCollection, Graph), Z_Construct_UClass_UNiagaraGraph_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_Graph_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_Graph_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_bCreatedByUser_MetaData[] = {
		{ "Comment", "/** Whether this parameter was initially created by the user. */" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "Whether this parameter was initially created by the user." },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_bCreatedByUser_SetBit(void* Obj)
	{
		((FNiagaraGraphParameterReferenceCollection*)Obj)->bCreatedByUser = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_bCreatedByUser = { "bCreatedByUser", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNiagaraGraphParameterReferenceCollection), &Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_bCreatedByUser_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_bCreatedByUser_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_bCreatedByUser_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_ParameterReferences_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_ParameterReferences,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_Graph,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::NewProp_bCreatedByUser,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraGraphParameterReferenceCollection",
		sizeof(FNiagaraGraphParameterReferenceCollection),
		alignof(FNiagaraGraphParameterReferenceCollection),
		Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraGraphParameterReferenceCollection"), sizeof(FNiagaraGraphParameterReferenceCollection), Get_Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection_Hash() { return 1227497433U; }
class UScriptStruct* FNiagaraGraphParameterReference::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraGraphParameterReference, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraGraphParameterReference"), sizeof(FNiagaraGraphParameterReference), Get_Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraGraphParameterReference>()
{
	return FNiagaraGraphParameterReference::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraGraphParameterReference(FNiagaraGraphParameterReference::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraGraphParameterReference"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraGraphParameterReference
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraGraphParameterReference()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraGraphParameterReference>(FName(TEXT("NiagaraGraphParameterReference")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraGraphParameterReference;
	struct Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsUserFacing_MetaData[];
#endif
		static void NewProp_bIsUserFacing_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsUserFacing;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraGraphParameterReference>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_Key_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphParameterReference, Key), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_Key_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_Value_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0014000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraGraphParameterReference, Value), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_bIsUserFacing_MetaData[] = {
		{ "Comment", "// If false then it is just a technical reference (e.g. setting the default value)\n" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "If false then it is just a technical reference (e.g. setting the default value)" },
	};
#endif
	void Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_bIsUserFacing_SetBit(void* Obj)
	{
		((FNiagaraGraphParameterReference*)Obj)->bIsUserFacing = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_bIsUserFacing = { "bIsUserFacing", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNiagaraGraphParameterReference), &Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_bIsUserFacing_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_bIsUserFacing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_bIsUserFacing_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::NewProp_bIsUserFacing,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraGraphParameterReference",
		sizeof(FNiagaraGraphParameterReference),
		alignof(FNiagaraGraphParameterReference),
		Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraGraphParameterReference()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraGraphParameterReference"), sizeof(FNiagaraGraphParameterReference), Get_Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraGraphParameterReference_Hash() { return 412613155U; }
	void UNiagaraGraph::StaticRegisterNativesUNiagaraGraph()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraGraph_NoRegister()
	{
		return UNiagaraGraph::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraGraph_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChangeId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChangeId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForceRebuildId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ForceRebuildId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastBuiltTraversalDataChangeId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastBuiltTraversalDataChangeId;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedUsageInfo_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedUsageInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CachedUsageInfo;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VariableToMetaData_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VariableToMetaData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VariableToMetaData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_VariableToMetaData;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VariableToScriptVariable_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VariableToScriptVariable_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VariableToScriptVariable_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_VariableToScriptVariable;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParameterToReferencesMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParameterToReferencesMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterToReferencesMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ParameterToReferencesMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraGraph_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraph,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraGraph_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraGraph.h" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ChangeId_MetaData[] = {
		{ "Comment", "/** The current change identifier for this graph overall. Used to sync status with UNiagaraScripts.*/" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "The current change identifier for this graph overall. Used to sync status with UNiagaraScripts." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ChangeId = { "ChangeId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraGraph, ChangeId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ChangeId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ChangeId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ForceRebuildId_MetaData[] = {
		{ "Comment", "/** Internal value used to invalidate a DDC key for the script no matter what.*/" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "Internal value used to invalidate a DDC key for the script no matter what." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ForceRebuildId = { "ForceRebuildId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraGraph, ForceRebuildId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ForceRebuildId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ForceRebuildId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_LastBuiltTraversalDataChangeId_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_LastBuiltTraversalDataChangeId = { "LastBuiltTraversalDataChangeId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraGraph, LastBuiltTraversalDataChangeId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_LastBuiltTraversalDataChangeId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_LastBuiltTraversalDataChangeId_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_CachedUsageInfo_Inner = { "CachedUsageInfo", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraGraphScriptUsageInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_CachedUsageInfo_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_CachedUsageInfo = { "CachedUsageInfo", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraGraph, CachedUsageInfo), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_CachedUsageInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_CachedUsageInfo_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToMetaData_ValueProp = { "VariableToMetaData", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FNiagaraVariableMetaData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToMetaData_Key_KeyProp = { "VariableToMetaData_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraVariable, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToMetaData_MetaData[] = {
		{ "Comment", "/** Storage of meta-data for variables defined for use explicitly with this graph.*/" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "Storage of meta-data for variables defined for use explicitly with this graph." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToMetaData = { "VariableToMetaData", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraGraph, VariableToMetaData_DEPRECATED), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToMetaData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToMetaData_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToScriptVariable_ValueProp = { "VariableToScriptVariable", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UNiagaraScriptVariable_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToScriptVariable_Key_KeyProp = { "VariableToScriptVariable_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraVariable, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToScriptVariable_MetaData[] = {
		{ "Comment", "/** Storage of variables defined for use with this graph.*/" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "Storage of variables defined for use with this graph." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToScriptVariable = { "VariableToScriptVariable", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraGraph, VariableToScriptVariable), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToScriptVariable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToScriptVariable_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ParameterToReferencesMap_ValueProp = { "ParameterToReferencesMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FNiagaraGraphParameterReferenceCollection, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ParameterToReferencesMap_Key_KeyProp = { "ParameterToReferencesMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraVariable, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ParameterToReferencesMap_MetaData[] = {
		{ "Comment", "/** A map of parameters in the graph to their referencers. */" },
		{ "ModuleRelativePath", "Public/NiagaraGraph.h" },
		{ "ToolTip", "A map of parameters in the graph to their referencers." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ParameterToReferencesMap = { "ParameterToReferencesMap", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraGraph, ParameterToReferencesMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ParameterToReferencesMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ParameterToReferencesMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraGraph_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ChangeId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ForceRebuildId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_LastBuiltTraversalDataChangeId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_CachedUsageInfo_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_CachedUsageInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToMetaData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToMetaData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToMetaData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToScriptVariable_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToScriptVariable_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_VariableToScriptVariable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ParameterToReferencesMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ParameterToReferencesMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraGraph_Statics::NewProp_ParameterToReferencesMap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraGraph_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraGraph>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraGraph_Statics::ClassParams = {
		&UNiagaraGraph::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraGraph_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraGraph_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraGraph_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraGraph_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraGraph()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraGraph_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraGraph, 3045753108);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraGraph>()
	{
		return UNiagaraGraph::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraGraph(Z_Construct_UClass_UNiagaraGraph, &UNiagaraGraph::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraGraph"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraGraph);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
