// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackEntry_generated_h
#error "NiagaraStackEntry.generated.h already included, missing '#pragma once' in NiagaraStackEntry.h"
#endif
#define NIAGARAEDITOR_NiagaraStackEntry_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackEntry(); \
	friend struct Z_Construct_UClass_UNiagaraStackEntry_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEntry, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEntry)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackEntry(); \
	friend struct Z_Construct_UClass_UNiagaraStackEntry_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEntry, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEntry)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackEntry(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackEntry) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEntry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEntry); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEntry(UNiagaraStackEntry&&); \
	NO_API UNiagaraStackEntry(const UNiagaraStackEntry&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEntry(UNiagaraStackEntry&&); \
	NO_API UNiagaraStackEntry(const UNiagaraStackEntry&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEntry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEntry); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackEntry)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StackEditorData() { return STRUCT_OFFSET(UNiagaraStackEntry, StackEditorData); } \
	FORCEINLINE static uint32 __PPO__Children() { return STRUCT_OFFSET(UNiagaraStackEntry, Children); } \
	FORCEINLINE static uint32 __PPO__ErrorChildren() { return STRUCT_OFFSET(UNiagaraStackEntry, ErrorChildren); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_48_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h_51_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackEntry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEntry_h


#define FOREACH_ENUM_ESTACKISSUESEVERITY(op) \
	op(EStackIssueSeverity::Error) \
	op(EStackIssueSeverity::Warning) \
	op(EStackIssueSeverity::Info) \
	op(EStackIssueSeverity::CustomNote) \
	op(EStackIssueSeverity::None) 

enum class EStackIssueSeverity : uint8;
template<> NIAGARAEDITOR_API UEnum* StaticEnum<EStackIssueSeverity>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
