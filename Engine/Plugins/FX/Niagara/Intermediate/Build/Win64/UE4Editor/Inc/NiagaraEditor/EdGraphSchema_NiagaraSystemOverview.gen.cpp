// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/EdGraphSchema_NiagaraSystemOverview.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEdGraphSchema_NiagaraSystemOverview() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphSchema();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UEdGraphSchema_NiagaraSystemOverview::StaticRegisterNativesUEdGraphSchema_NiagaraSystemOverview()
	{
	}
	UClass* Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_NoRegister()
	{
		return UEdGraphSchema_NiagaraSystemOverview::StaticClass();
	}
	struct Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphSchema,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EdGraphSchema_NiagaraSystemOverview.h" },
		{ "ModuleRelativePath", "Public/EdGraphSchema_NiagaraSystemOverview.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEdGraphSchema_NiagaraSystemOverview>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_Statics::ClassParams = {
		&UEdGraphSchema_NiagaraSystemOverview::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEdGraphSchema_NiagaraSystemOverview, 91544498);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UEdGraphSchema_NiagaraSystemOverview>()
	{
		return UEdGraphSchema_NiagaraSystemOverview::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEdGraphSchema_NiagaraSystemOverview(Z_Construct_UClass_UEdGraphSchema_NiagaraSystemOverview, &UEdGraphSchema_NiagaraSystemOverview::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UEdGraphSchema_NiagaraSystemOverview"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEdGraphSchema_NiagaraSystemOverview);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
