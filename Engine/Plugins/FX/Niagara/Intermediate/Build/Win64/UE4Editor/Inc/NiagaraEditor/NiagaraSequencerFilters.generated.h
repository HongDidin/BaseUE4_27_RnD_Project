// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraSequencerFilters_generated_h
#error "NiagaraSequencerFilters.generated.h already included, missing '#pragma once' in NiagaraSequencerFilters.h"
#endif
#define NIAGARAEDITOR_NiagaraSequencerFilters_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraSequencerTrackFilter(); \
	friend struct Z_Construct_UClass_UNiagaraSequencerTrackFilter_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSequencerTrackFilter, USequencerTrackFilterExtension, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSequencerTrackFilter)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraSequencerTrackFilter(); \
	friend struct Z_Construct_UClass_UNiagaraSequencerTrackFilter_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSequencerTrackFilter, USequencerTrackFilterExtension, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSequencerTrackFilter)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSequencerTrackFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSequencerTrackFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSequencerTrackFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSequencerTrackFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSequencerTrackFilter(UNiagaraSequencerTrackFilter&&); \
	NO_API UNiagaraSequencerTrackFilter(const UNiagaraSequencerTrackFilter&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSequencerTrackFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSequencerTrackFilter(UNiagaraSequencerTrackFilter&&); \
	NO_API UNiagaraSequencerTrackFilter(const UNiagaraSequencerTrackFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSequencerTrackFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSequencerTrackFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSequencerTrackFilter)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_11_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h_15_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraSequencerTrackFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_NiagaraSequencerFilters_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
