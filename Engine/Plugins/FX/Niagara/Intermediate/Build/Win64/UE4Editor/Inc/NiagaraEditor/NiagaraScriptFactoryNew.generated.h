// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraScriptFactoryNew_generated_h
#error "NiagaraScriptFactoryNew.generated.h already included, missing '#pragma once' in NiagaraScriptFactoryNew.h"
#endif
#define NIAGARAEDITOR_NiagaraScriptFactoryNew_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraScriptFactoryNew(); \
	friend struct Z_Construct_UClass_UNiagaraScriptFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UNiagaraScriptFactoryNew, UFactory, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraScriptFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraScriptFactoryNew(); \
	friend struct Z_Construct_UClass_UNiagaraScriptFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UNiagaraScriptFactoryNew, UFactory, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraScriptFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraScriptFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraScriptFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraScriptFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraScriptFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraScriptFactoryNew(UNiagaraScriptFactoryNew&&); \
	NO_API UNiagaraScriptFactoryNew(const UNiagaraScriptFactoryNew&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraScriptFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraScriptFactoryNew(UNiagaraScriptFactoryNew&&); \
	NO_API UNiagaraScriptFactoryNew(const UNiagaraScriptFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraScriptFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraScriptFactoryNew); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraScriptFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_12_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraScriptFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraScriptFactoryNew>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraModuleScriptFactory(); \
	friend struct Z_Construct_UClass_UNiagaraModuleScriptFactory_Statics; \
public: \
	DECLARE_CLASS(UNiagaraModuleScriptFactory, UNiagaraScriptFactoryNew, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraModuleScriptFactory)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraModuleScriptFactory(); \
	friend struct Z_Construct_UClass_UNiagaraModuleScriptFactory_Statics; \
public: \
	DECLARE_CLASS(UNiagaraModuleScriptFactory, UNiagaraScriptFactoryNew, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraModuleScriptFactory)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraModuleScriptFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraModuleScriptFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraModuleScriptFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraModuleScriptFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraModuleScriptFactory(UNiagaraModuleScriptFactory&&); \
	NO_API UNiagaraModuleScriptFactory(const UNiagaraModuleScriptFactory&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraModuleScriptFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraModuleScriptFactory(UNiagaraModuleScriptFactory&&); \
	NO_API UNiagaraModuleScriptFactory(const UNiagaraModuleScriptFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraModuleScriptFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraModuleScriptFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraModuleScriptFactory)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_40_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_43_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraModuleScriptFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraModuleScriptFactory>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraFunctionScriptFactory(); \
	friend struct Z_Construct_UClass_UNiagaraFunctionScriptFactory_Statics; \
public: \
	DECLARE_CLASS(UNiagaraFunctionScriptFactory, UNiagaraScriptFactoryNew, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraFunctionScriptFactory)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraFunctionScriptFactory(); \
	friend struct Z_Construct_UClass_UNiagaraFunctionScriptFactory_Statics; \
public: \
	DECLARE_CLASS(UNiagaraFunctionScriptFactory, UNiagaraScriptFactoryNew, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraFunctionScriptFactory)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraFunctionScriptFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraFunctionScriptFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraFunctionScriptFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraFunctionScriptFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraFunctionScriptFactory(UNiagaraFunctionScriptFactory&&); \
	NO_API UNiagaraFunctionScriptFactory(const UNiagaraFunctionScriptFactory&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraFunctionScriptFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraFunctionScriptFactory(UNiagaraFunctionScriptFactory&&); \
	NO_API UNiagaraFunctionScriptFactory(const UNiagaraFunctionScriptFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraFunctionScriptFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraFunctionScriptFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraFunctionScriptFactory)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_60_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_63_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraFunctionScriptFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraFunctionScriptFactory>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraDynamicInputScriptFactory(); \
	friend struct Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_Statics; \
public: \
	DECLARE_CLASS(UNiagaraDynamicInputScriptFactory, UNiagaraScriptFactoryNew, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraDynamicInputScriptFactory)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraDynamicInputScriptFactory(); \
	friend struct Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_Statics; \
public: \
	DECLARE_CLASS(UNiagaraDynamicInputScriptFactory, UNiagaraScriptFactoryNew, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraDynamicInputScriptFactory)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraDynamicInputScriptFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraDynamicInputScriptFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraDynamicInputScriptFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraDynamicInputScriptFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraDynamicInputScriptFactory(UNiagaraDynamicInputScriptFactory&&); \
	NO_API UNiagaraDynamicInputScriptFactory(const UNiagaraDynamicInputScriptFactory&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraDynamicInputScriptFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraDynamicInputScriptFactory(UNiagaraDynamicInputScriptFactory&&); \
	NO_API UNiagaraDynamicInputScriptFactory(const UNiagaraDynamicInputScriptFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraDynamicInputScriptFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraDynamicInputScriptFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraDynamicInputScriptFactory)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_79_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h_82_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraDynamicInputScriptFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraDynamicInputScriptFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraScriptFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
