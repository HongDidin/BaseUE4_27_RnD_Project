// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraEditorModule.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraEditorModule() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FReservedParameterArray();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FReservedParameter();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariableBase();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraParameterDefinitions_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraReservedParametersManager_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraReservedParametersManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FReservedParameterArray::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FReservedParameterArray_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FReservedParameterArray, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ReservedParameterArray"), sizeof(FReservedParameterArray), Get_Z_Construct_UScriptStruct_FReservedParameterArray_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FReservedParameterArray>()
{
	return FReservedParameterArray::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FReservedParameterArray(FReservedParameterArray::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("ReservedParameterArray"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFReservedParameterArray
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFReservedParameterArray()
	{
		UScriptStruct::DeferCppStructOps<FReservedParameterArray>(FName(TEXT("ReservedParameterArray")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFReservedParameterArray;
	struct Z_Construct_UScriptStruct_FReservedParameterArray_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Arr_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Arr_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Arr;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FReservedParameterArray_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorModule.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FReservedParameterArray_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FReservedParameterArray>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FReservedParameterArray_Statics::NewProp_Arr_Inner = { "Arr", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FReservedParameter, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FReservedParameterArray_Statics::NewProp_Arr_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorModule.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FReservedParameterArray_Statics::NewProp_Arr = { "Arr", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FReservedParameterArray, Arr), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FReservedParameterArray_Statics::NewProp_Arr_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FReservedParameterArray_Statics::NewProp_Arr_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FReservedParameterArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FReservedParameterArray_Statics::NewProp_Arr_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FReservedParameterArray_Statics::NewProp_Arr,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FReservedParameterArray_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"ReservedParameterArray",
		sizeof(FReservedParameterArray),
		alignof(FReservedParameterArray),
		Z_Construct_UScriptStruct_FReservedParameterArray_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FReservedParameterArray_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FReservedParameterArray_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FReservedParameterArray_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FReservedParameterArray()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FReservedParameterArray_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ReservedParameterArray"), sizeof(FReservedParameterArray), Get_Z_Construct_UScriptStruct_FReservedParameterArray_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FReservedParameterArray_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FReservedParameterArray_Hash() { return 249192603U; }
class UScriptStruct* FReservedParameter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FReservedParameter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FReservedParameter, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("ReservedParameter"), sizeof(FReservedParameter), Get_Z_Construct_UScriptStruct_FReservedParameter_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FReservedParameter>()
{
	return FReservedParameter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FReservedParameter(FReservedParameter::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("ReservedParameter"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFReservedParameter
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFReservedParameter()
	{
		UScriptStruct::DeferCppStructOps<FReservedParameter>(FName(TEXT("ReservedParameter")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFReservedParameter;
	struct Z_Construct_UScriptStruct_FReservedParameter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Parameter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReservingDefinitionsAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReservingDefinitionsAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FReservedParameter_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/* Wrapper struct for tracking parameters that are reserved by parameter definitions assets. */" },
		{ "ModuleRelativePath", "Public/NiagaraEditorModule.h" },
		{ "ToolTip", "Wrapper struct for tracking parameters that are reserved by parameter definitions assets." },
	};
#endif
	void* Z_Construct_UScriptStruct_FReservedParameter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FReservedParameter>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FReservedParameter_Statics::NewProp_Parameter_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorModule.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FReservedParameter_Statics::NewProp_Parameter = { "Parameter", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FReservedParameter, Parameter), Z_Construct_UScriptStruct_FNiagaraVariableBase, METADATA_PARAMS(Z_Construct_UScriptStruct_FReservedParameter_Statics::NewProp_Parameter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FReservedParameter_Statics::NewProp_Parameter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FReservedParameter_Statics::NewProp_ReservingDefinitionsAsset_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorModule.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FReservedParameter_Statics::NewProp_ReservingDefinitionsAsset = { "ReservingDefinitionsAsset", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FReservedParameter, ReservingDefinitionsAsset), Z_Construct_UClass_UNiagaraParameterDefinitions_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FReservedParameter_Statics::NewProp_ReservingDefinitionsAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FReservedParameter_Statics::NewProp_ReservingDefinitionsAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FReservedParameter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FReservedParameter_Statics::NewProp_Parameter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FReservedParameter_Statics::NewProp_ReservingDefinitionsAsset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FReservedParameter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"ReservedParameter",
		sizeof(FReservedParameter),
		alignof(FReservedParameter),
		Z_Construct_UScriptStruct_FReservedParameter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FReservedParameter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FReservedParameter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FReservedParameter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FReservedParameter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FReservedParameter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ReservedParameter"), sizeof(FReservedParameter), Get_Z_Construct_UScriptStruct_FReservedParameter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FReservedParameter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FReservedParameter_Hash() { return 3550518327U; }
	void UNiagaraReservedParametersManager::StaticRegisterNativesUNiagaraReservedParametersManager()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraReservedParametersManager_NoRegister()
	{
		return UNiagaraReservedParametersManager::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraReservedParametersManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReservedParameters_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReservedParameters_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReservedParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReservedParameters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Manager singleton for tracking parameters that are reserved by parameter definitions assets.\n *  Implements UObject to support undo/redo transactions on the map of definitions asset ptrs to parameter names.\n */" },
		{ "IncludePath", "NiagaraEditorModule.h" },
		{ "ModuleRelativePath", "Public/NiagaraEditorModule.h" },
		{ "ToolTip", "Manager singleton for tracking parameters that are reserved by parameter definitions assets.\nImplements UObject to support undo/redo transactions on the map of definitions asset ptrs to parameter names." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::NewProp_ReservedParameters_ValueProp = { "ReservedParameters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FReservedParameterArray, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::NewProp_ReservedParameters_Key_KeyProp = { "ReservedParameters_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::NewProp_ReservedParameters_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraEditorModule.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::NewProp_ReservedParameters = { "ReservedParameters", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraReservedParametersManager, ReservedParameters), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::NewProp_ReservedParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::NewProp_ReservedParameters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::NewProp_ReservedParameters_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::NewProp_ReservedParameters_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::NewProp_ReservedParameters,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraReservedParametersManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::ClassParams = {
		&UNiagaraReservedParametersManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraReservedParametersManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraReservedParametersManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraReservedParametersManager, 2868361114);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraReservedParametersManager>()
	{
		return UNiagaraReservedParametersManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraReservedParametersManager(Z_Construct_UClass_UNiagaraReservedParametersManager, &UNiagaraReservedParametersManager::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraReservedParametersManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraReservedParametersManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
