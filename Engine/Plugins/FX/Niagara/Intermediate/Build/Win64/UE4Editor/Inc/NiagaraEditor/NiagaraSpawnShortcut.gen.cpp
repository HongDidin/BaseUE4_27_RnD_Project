// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraSpawnShortcut.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraSpawnShortcut() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraSpawnShortcut();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	SLATE_API UScriptStruct* Z_Construct_UScriptStruct_FInputChord();
// End Cross Module References
class UScriptStruct* FNiagaraSpawnShortcut::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraSpawnShortcut, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraSpawnShortcut"), sizeof(FNiagaraSpawnShortcut), Get_Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraSpawnShortcut>()
{
	return FNiagaraSpawnShortcut::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraSpawnShortcut(FNiagaraSpawnShortcut::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraSpawnShortcut"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraSpawnShortcut
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraSpawnShortcut()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraSpawnShortcut>(FName(TEXT("NiagaraSpawnShortcut")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraSpawnShortcut;
	struct Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Input_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraSpawnShortcut.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraSpawnShortcut>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Shortcut" },
		{ "ModuleRelativePath", "Public/NiagaraSpawnShortcut.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraSpawnShortcut, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewProp_Input_MetaData[] = {
		{ "Category", "Shortcut" },
		{ "ModuleRelativePath", "Public/NiagaraSpawnShortcut.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraSpawnShortcut, Input), Z_Construct_UScriptStruct_FInputChord, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewProp_Input_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewProp_Input_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::NewProp_Input,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraSpawnShortcut",
		sizeof(FNiagaraSpawnShortcut),
		alignof(FNiagaraSpawnShortcut),
		Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraSpawnShortcut()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraSpawnShortcut"), sizeof(FNiagaraSpawnShortcut), Get_Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraSpawnShortcut_Hash() { return 1228948251U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
