// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraStackEditorData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackEditorData() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEditorData_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackEditorData();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraEditorDataBase();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UNiagaraStackEditorData::StaticRegisterNativesUNiagaraStackEditorData()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackEditorData_NoRegister()
	{
		return UNiagaraStackEditorData::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackEditorData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_StackEntryKeyToDisplayName_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StackEntryKeyToDisplayName_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StackEntryKeyToDisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_StackEntryKeyToDisplayName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DismissedStackIssueIds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DismissedStackIssueIds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DismissedStackIssueIds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackEditorData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraEditorDataBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEditorData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Editor only UI data for emitters. */" },
		{ "IncludePath", "NiagaraStackEditorData.h" },
		{ "ModuleRelativePath", "Public/NiagaraStackEditorData.h" },
		{ "ToolTip", "Editor only UI data for emitters." },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_StackEntryKeyToDisplayName_ValueProp = { "StackEntryKeyToDisplayName", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_StackEntryKeyToDisplayName_Key_KeyProp = { "StackEntryKeyToDisplayName_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_StackEntryKeyToDisplayName_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraStackEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_StackEntryKeyToDisplayName = { "StackEntryKeyToDisplayName", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackEditorData, StackEntryKeyToDisplayName), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_StackEntryKeyToDisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_StackEntryKeyToDisplayName_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_DismissedStackIssueIds_Inner = { "DismissedStackIssueIds", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_DismissedStackIssueIds_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraStackEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_DismissedStackIssueIds = { "DismissedStackIssueIds", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraStackEditorData, DismissedStackIssueIds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_DismissedStackIssueIds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_DismissedStackIssueIds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraStackEditorData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_StackEntryKeyToDisplayName_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_StackEntryKeyToDisplayName_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_StackEntryKeyToDisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_DismissedStackIssueIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraStackEditorData_Statics::NewProp_DismissedStackIssueIds,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackEditorData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackEditorData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackEditorData_Statics::ClassParams = {
		&UNiagaraStackEditorData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraStackEditorData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEditorData_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackEditorData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackEditorData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackEditorData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackEditorData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackEditorData, 292289849);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackEditorData>()
	{
		return UNiagaraStackEditorData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackEditorData(Z_Construct_UClass_UNiagaraStackEditorData, &UNiagaraStackEditorData::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackEditorData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackEditorData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
