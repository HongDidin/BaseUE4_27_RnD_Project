// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraParameterCollectionFactoryNew_generated_h
#error "NiagaraParameterCollectionFactoryNew.generated.h already included, missing '#pragma once' in NiagaraParameterCollectionFactoryNew.h"
#endif
#define NIAGARAEDITOR_NiagaraParameterCollectionFactoryNew_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraParameterCollectionFactoryNew(); \
	friend struct Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UNiagaraParameterCollectionFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraParameterCollectionFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraParameterCollectionFactoryNew(); \
	friend struct Z_Construct_UClass_UNiagaraParameterCollectionFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UNiagaraParameterCollectionFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraParameterCollectionFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraParameterCollectionFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraParameterCollectionFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraParameterCollectionFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraParameterCollectionFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraParameterCollectionFactoryNew(UNiagaraParameterCollectionFactoryNew&&); \
	NO_API UNiagaraParameterCollectionFactoryNew(const UNiagaraParameterCollectionFactoryNew&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraParameterCollectionFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraParameterCollectionFactoryNew(UNiagaraParameterCollectionFactoryNew&&); \
	NO_API UNiagaraParameterCollectionFactoryNew(const UNiagaraParameterCollectionFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraParameterCollectionFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraParameterCollectionFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraParameterCollectionFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_10_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraParameterCollectionFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraParameterCollectionFactoryNew>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraParameterCollectionInstanceFactoryNew(); \
	friend struct Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UNiagaraParameterCollectionInstanceFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraParameterCollectionInstanceFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraParameterCollectionInstanceFactoryNew(); \
	friend struct Z_Construct_UClass_UNiagaraParameterCollectionInstanceFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UNiagaraParameterCollectionInstanceFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraParameterCollectionInstanceFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraParameterCollectionInstanceFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraParameterCollectionInstanceFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraParameterCollectionInstanceFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraParameterCollectionInstanceFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraParameterCollectionInstanceFactoryNew(UNiagaraParameterCollectionInstanceFactoryNew&&); \
	NO_API UNiagaraParameterCollectionInstanceFactoryNew(const UNiagaraParameterCollectionInstanceFactoryNew&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraParameterCollectionInstanceFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraParameterCollectionInstanceFactoryNew(UNiagaraParameterCollectionInstanceFactoryNew&&); \
	NO_API UNiagaraParameterCollectionInstanceFactoryNew(const UNiagaraParameterCollectionInstanceFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraParameterCollectionInstanceFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraParameterCollectionInstanceFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraParameterCollectionInstanceFactoryNew)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_20_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h_23_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraParameterCollectionInstanceFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraParameterCollectionInstanceFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraParameterCollectionFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
