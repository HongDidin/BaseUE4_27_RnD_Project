// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_MovieSceneNiagaraEmitterSection_generated_h
#error "MovieSceneNiagaraEmitterSection.generated.h already included, missing '#pragma once' in MovieSceneNiagaraEmitterSection.h"
#endif
#define NIAGARAEDITOR_MovieSceneNiagaraEmitterSection_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_44_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct(); \
	typedef FMovieSceneChannel Super;


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FMovieSceneNiagaraEmitterChannel>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraEmitterSectionKey>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneNiagaraEmitterSection(); \
	friend struct Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneNiagaraEmitterSection, UMovieSceneNiagaraEmitterSectionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneNiagaraEmitterSection)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneNiagaraEmitterSection(); \
	friend struct Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneNiagaraEmitterSection, UMovieSceneNiagaraEmitterSectionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneNiagaraEmitterSection)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneNiagaraEmitterSection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneNiagaraEmitterSection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneNiagaraEmitterSection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneNiagaraEmitterSection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneNiagaraEmitterSection(UMovieSceneNiagaraEmitterSection&&); \
	NO_API UMovieSceneNiagaraEmitterSection(const UMovieSceneNiagaraEmitterSection&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneNiagaraEmitterSection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneNiagaraEmitterSection(UMovieSceneNiagaraEmitterSection&&); \
	NO_API UMovieSceneNiagaraEmitterSection(const UMovieSceneNiagaraEmitterSection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneNiagaraEmitterSection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneNiagaraEmitterSection); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneNiagaraEmitterSection)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__NumLoops() { return STRUCT_OFFSET(UMovieSceneNiagaraEmitterSection, NumLoops); } \
	FORCEINLINE static uint32 __PPO__bStartTimeIncludedInFirstLoopOnly() { return STRUCT_OFFSET(UMovieSceneNiagaraEmitterSection, bStartTimeIncludedInFirstLoopOnly); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_126_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h_129_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UMovieSceneNiagaraEmitterSection>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Sequencer_NiagaraSequence_Sections_MovieSceneNiagaraEmitterSection_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
