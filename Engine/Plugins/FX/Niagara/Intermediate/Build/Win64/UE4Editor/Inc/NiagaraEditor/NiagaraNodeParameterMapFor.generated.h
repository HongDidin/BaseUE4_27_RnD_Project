// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraNodeParameterMapFor_generated_h
#error "NiagaraNodeParameterMapFor.generated.h already included, missing '#pragma once' in NiagaraNodeParameterMapFor.h"
#endif
#define NIAGARAEDITOR_NiagaraNodeParameterMapFor_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraNodeParameterMapFor(); \
	friend struct Z_Construct_UClass_UNiagaraNodeParameterMapFor_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeParameterMapFor, UNiagaraNodeParameterMapSet, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraNodeParameterMapFor)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraNodeParameterMapFor(); \
	friend struct Z_Construct_UClass_UNiagaraNodeParameterMapFor_Statics; \
public: \
	DECLARE_CLASS(UNiagaraNodeParameterMapFor, UNiagaraNodeParameterMapSet, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraNodeParameterMapFor)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraNodeParameterMapFor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraNodeParameterMapFor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraNodeParameterMapFor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeParameterMapFor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraNodeParameterMapFor(UNiagaraNodeParameterMapFor&&); \
	NO_API UNiagaraNodeParameterMapFor(const UNiagaraNodeParameterMapFor&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraNodeParameterMapFor(UNiagaraNodeParameterMapFor&&); \
	NO_API UNiagaraNodeParameterMapFor(const UNiagaraNodeParameterMapFor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraNodeParameterMapFor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraNodeParameterMapFor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraNodeParameterMapFor)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_10_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h_14_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraNodeParameterMapFor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraNodeParameterMapFor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
