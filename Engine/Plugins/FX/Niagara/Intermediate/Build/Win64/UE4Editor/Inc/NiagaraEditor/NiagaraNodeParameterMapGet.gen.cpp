// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraNodeParameterMapGet.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeParameterMapGet() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeParameterMapGet_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeParameterMapGet();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeParameterMapBase();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	void UNiagaraNodeParameterMapGet::StaticRegisterNativesUNiagaraNodeParameterMapGet()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeParameterMapGet_NoRegister()
	{
		return UNiagaraNodeParameterMapGet::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PinOutputToPinDefaultPersistentId_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PinOutputToPinDefaultPersistentId_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PinOutputToPinDefaultPersistentId_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PinOutputToPinDefaultPersistentId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNodeParameterMapBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A node that allows a user to get multiple values from a parameter map. */" },
		{ "IncludePath", "NiagaraNodeParameterMapGet.h" },
		{ "ModuleRelativePath", "Private/NiagaraNodeParameterMapGet.h" },
		{ "ToolTip", "A node that allows a user to get multiple values from a parameter map." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::NewProp_PinOutputToPinDefaultPersistentId_ValueProp = { "PinOutputToPinDefaultPersistentId", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::NewProp_PinOutputToPinDefaultPersistentId_Key_KeyProp = { "PinOutputToPinDefaultPersistentId_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::NewProp_PinOutputToPinDefaultPersistentId_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeParameterMapGet.h" },
		{ "SkipForCompileHash", "true" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::NewProp_PinOutputToPinDefaultPersistentId = { "PinOutputToPinDefaultPersistentId", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeParameterMapGet, PinOutputToPinDefaultPersistentId), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::NewProp_PinOutputToPinDefaultPersistentId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::NewProp_PinOutputToPinDefaultPersistentId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::NewProp_PinOutputToPinDefaultPersistentId_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::NewProp_PinOutputToPinDefaultPersistentId_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::NewProp_PinOutputToPinDefaultPersistentId,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeParameterMapGet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::ClassParams = {
		&UNiagaraNodeParameterMapGet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeParameterMapGet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeParameterMapGet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeParameterMapGet, 1921191675);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeParameterMapGet>()
	{
		return UNiagaraNodeParameterMapGet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeParameterMapGet(Z_Construct_UClass_UNiagaraNodeParameterMapGet, &UNiagaraNodeParameterMapGet::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeParameterMapGet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeParameterMapGet);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
