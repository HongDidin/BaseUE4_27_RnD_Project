// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraSystemEditorData_generated_h
#error "NiagaraSystemEditorData.generated.h already included, missing '#pragma once' in NiagaraSystemEditorData.h"
#endif
#define NIAGARAEDITOR_NiagaraSystemEditorData_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraSystemEditorFolder(); \
	friend struct Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSystemEditorFolder, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSystemEditorFolder)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraSystemEditorFolder(); \
	friend struct Z_Construct_UClass_UNiagaraSystemEditorFolder_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSystemEditorFolder, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSystemEditorFolder)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSystemEditorFolder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSystemEditorFolder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSystemEditorFolder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSystemEditorFolder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSystemEditorFolder(UNiagaraSystemEditorFolder&&); \
	NO_API UNiagaraSystemEditorFolder(const UNiagaraSystemEditorFolder&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSystemEditorFolder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSystemEditorFolder(UNiagaraSystemEditorFolder&&); \
	NO_API UNiagaraSystemEditorFolder(const UNiagaraSystemEditorFolder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSystemEditorFolder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSystemEditorFolder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSystemEditorFolder)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FolderName() { return STRUCT_OFFSET(UNiagaraSystemEditorFolder, FolderName); } \
	FORCEINLINE static uint32 __PPO__ChildFolders() { return STRUCT_OFFSET(UNiagaraSystemEditorFolder, ChildFolders); } \
	FORCEINLINE static uint32 __PPO__ChildEmitterHandleIds() { return STRUCT_OFFSET(UNiagaraSystemEditorFolder, ChildEmitterHandleIds); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_14_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraSystemEditorFolder>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraSystemEditorData(); \
	friend struct Z_Construct_UClass_UNiagaraSystemEditorData_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSystemEditorData, UNiagaraEditorDataBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSystemEditorData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraSystemEditorData(); \
	friend struct Z_Construct_UClass_UNiagaraSystemEditorData_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSystemEditorData, UNiagaraEditorDataBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSystemEditorData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSystemEditorData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSystemEditorData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSystemEditorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSystemEditorData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSystemEditorData(UNiagaraSystemEditorData&&); \
	NO_API UNiagaraSystemEditorData(const UNiagaraSystemEditorData&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSystemEditorData(UNiagaraSystemEditorData&&); \
	NO_API UNiagaraSystemEditorData(const UNiagaraSystemEditorData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSystemEditorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSystemEditorData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSystemEditorData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RootFolder() { return STRUCT_OFFSET(UNiagaraSystemEditorData, RootFolder); } \
	FORCEINLINE static uint32 __PPO__StackEditorData() { return STRUCT_OFFSET(UNiagaraSystemEditorData, StackEditorData); } \
	FORCEINLINE static uint32 __PPO__OwnerTransform() { return STRUCT_OFFSET(UNiagaraSystemEditorData, OwnerTransform); } \
	FORCEINLINE static uint32 __PPO__PlaybackRangeMin() { return STRUCT_OFFSET(UNiagaraSystemEditorData, PlaybackRangeMin); } \
	FORCEINLINE static uint32 __PPO__PlaybackRangeMax() { return STRUCT_OFFSET(UNiagaraSystemEditorData, PlaybackRangeMax); } \
	FORCEINLINE static uint32 __PPO__SystemOverviewGraph() { return STRUCT_OFFSET(UNiagaraSystemEditorData, SystemOverviewGraph); } \
	FORCEINLINE static uint32 __PPO__OverviewGraphViewSettings() { return STRUCT_OFFSET(UNiagaraSystemEditorData, OverviewGraphViewSettings); } \
	FORCEINLINE static uint32 __PPO__bSystemIsPlaceholder() { return STRUCT_OFFSET(UNiagaraSystemEditorData, bSystemIsPlaceholder); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_48_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h_51_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraSystemEditorData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraSystemEditorData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
