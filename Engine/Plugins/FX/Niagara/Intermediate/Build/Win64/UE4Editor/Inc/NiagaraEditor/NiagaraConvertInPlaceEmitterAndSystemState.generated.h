// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraConvertInPlaceEmitterAndSystemState_generated_h
#error "NiagaraConvertInPlaceEmitterAndSystemState.generated.h already included, missing '#pragma once' in NiagaraConvertInPlaceEmitterAndSystemState.h"
#endif
#define NIAGARAEDITOR_NiagaraConvertInPlaceEmitterAndSystemState_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraConvertInPlaceEmitterAndSystemState(); \
	friend struct Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_Statics; \
public: \
	DECLARE_CLASS(UNiagaraConvertInPlaceEmitterAndSystemState, UNiagaraConvertInPlaceUtilityBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraConvertInPlaceEmitterAndSystemState)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraConvertInPlaceEmitterAndSystemState(); \
	friend struct Z_Construct_UClass_UNiagaraConvertInPlaceEmitterAndSystemState_Statics; \
public: \
	DECLARE_CLASS(UNiagaraConvertInPlaceEmitterAndSystemState, UNiagaraConvertInPlaceUtilityBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraConvertInPlaceEmitterAndSystemState)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraConvertInPlaceEmitterAndSystemState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraConvertInPlaceEmitterAndSystemState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraConvertInPlaceEmitterAndSystemState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraConvertInPlaceEmitterAndSystemState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraConvertInPlaceEmitterAndSystemState(UNiagaraConvertInPlaceEmitterAndSystemState&&); \
	NO_API UNiagaraConvertInPlaceEmitterAndSystemState(const UNiagaraConvertInPlaceEmitterAndSystemState&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraConvertInPlaceEmitterAndSystemState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraConvertInPlaceEmitterAndSystemState(UNiagaraConvertInPlaceEmitterAndSystemState&&); \
	NO_API UNiagaraConvertInPlaceEmitterAndSystemState(const UNiagaraConvertInPlaceEmitterAndSystemState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraConvertInPlaceEmitterAndSystemState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraConvertInPlaceEmitterAndSystemState); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraConvertInPlaceEmitterAndSystemState)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_10_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraConvertInPlaceEmitterAndSystemState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_NiagaraConvertInPlaceEmitterAndSystemState_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
