// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraOutlinerCustomization_generated_h
#error "NiagaraOutlinerCustomization.generated.h already included, missing '#pragma once' in NiagaraOutlinerCustomization.h"
#endif
#define NIAGARAEDITOR_NiagaraOutlinerCustomization_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Customizations_NiagaraOutlinerCustomization_h_307_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraOutlinerEmitterInstanceDataCustomizationWrapper>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Customizations_NiagaraOutlinerCustomization_h_300_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraOutlinerSystemInstanceDataCustomizationWrapper_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraOutlinerSystemInstanceDataCustomizationWrapper>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Customizations_NiagaraOutlinerCustomization_h_293_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraOutlinerSystemDataCustomizationWrapper_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraOutlinerSystemDataCustomizationWrapper>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Customizations_NiagaraOutlinerCustomization_h_286_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraOutlinerWorldDataCustomizationWrapper_Statics; \
	NIAGARAEDITOR_API static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraOutlinerWorldDataCustomizationWrapper>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Private_Customizations_NiagaraOutlinerCustomization_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
