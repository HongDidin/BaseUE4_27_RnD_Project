// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_ActorFactoryNiagara_generated_h
#error "ActorFactoryNiagara.generated.h already included, missing '#pragma once' in ActorFactoryNiagara.h"
#endif
#define NIAGARAEDITOR_ActorFactoryNiagara_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorFactoryNiagara(); \
	friend struct Z_Construct_UClass_UActorFactoryNiagara_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryNiagara, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UActorFactoryNiagara)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUActorFactoryNiagara(); \
	friend struct Z_Construct_UClass_UActorFactoryNiagara_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryNiagara, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NIAGARAEDITOR_API) \
	DECLARE_SERIALIZER(UActorFactoryNiagara)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UActorFactoryNiagara(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryNiagara) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UActorFactoryNiagara); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryNiagara); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UActorFactoryNiagara(UActorFactoryNiagara&&); \
	NIAGARAEDITOR_API UActorFactoryNiagara(const UActorFactoryNiagara&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NIAGARAEDITOR_API UActorFactoryNiagara(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NIAGARAEDITOR_API UActorFactoryNiagara(UActorFactoryNiagara&&); \
	NIAGARAEDITOR_API UActorFactoryNiagara(const UActorFactoryNiagara&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NIAGARAEDITOR_API, UActorFactoryNiagara); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryNiagara); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryNiagara)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_14_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ActorFactoryNiagara."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UActorFactoryNiagara>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ActorFactoryNiagara_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
