// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraStackEmitterSettingsGroup_generated_h
#error "NiagaraStackEmitterSettingsGroup.generated.h already included, missing '#pragma once' in NiagaraStackEmitterSettingsGroup.h"
#endif
#define NIAGARAEDITOR_NiagaraStackEmitterSettingsGroup_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackEmitterPropertiesItem(); \
	friend struct Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEmitterPropertiesItem, UNiagaraStackItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEmitterPropertiesItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackEmitterPropertiesItem(); \
	friend struct Z_Construct_UClass_UNiagaraStackEmitterPropertiesItem_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEmitterPropertiesItem, UNiagaraStackItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEmitterPropertiesItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackEmitterPropertiesItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackEmitterPropertiesItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEmitterPropertiesItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEmitterPropertiesItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEmitterPropertiesItem(UNiagaraStackEmitterPropertiesItem&&); \
	NO_API UNiagaraStackEmitterPropertiesItem(const UNiagaraStackEmitterPropertiesItem&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackEmitterPropertiesItem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEmitterPropertiesItem(UNiagaraStackEmitterPropertiesItem&&); \
	NO_API UNiagaraStackEmitterPropertiesItem(const UNiagaraStackEmitterPropertiesItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEmitterPropertiesItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEmitterPropertiesItem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackEmitterPropertiesItem)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__EmitterObject() { return STRUCT_OFFSET(UNiagaraStackEmitterPropertiesItem, EmitterObject); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_13_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackEmitterPropertiesItem>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraStackEmitterSettingsGroup(); \
	friend struct Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEmitterSettingsGroup, UNiagaraStackItemGroup, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEmitterSettingsGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraStackEmitterSettingsGroup(); \
	friend struct Z_Construct_UClass_UNiagaraStackEmitterSettingsGroup_Statics; \
public: \
	DECLARE_CLASS(UNiagaraStackEmitterSettingsGroup, UNiagaraStackItemGroup, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraStackEmitterSettingsGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraStackEmitterSettingsGroup(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraStackEmitterSettingsGroup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEmitterSettingsGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEmitterSettingsGroup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEmitterSettingsGroup(UNiagaraStackEmitterSettingsGroup&&); \
	NO_API UNiagaraStackEmitterSettingsGroup(const UNiagaraStackEmitterSettingsGroup&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraStackEmitterSettingsGroup(UNiagaraStackEmitterSettingsGroup&&); \
	NO_API UNiagaraStackEmitterSettingsGroup(const UNiagaraStackEmitterSettingsGroup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraStackEmitterSettingsGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraStackEmitterSettingsGroup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraStackEmitterSettingsGroup)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PropertiesItem() { return STRUCT_OFFSET(UNiagaraStackEmitterSettingsGroup, PropertiesItem); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_52_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h_55_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraStackEmitterSettingsGroup>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_Stack_NiagaraStackEmitterSettingsGroup_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
