// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraNodeOutputTag.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeOutputTag() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeOutputTag_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeOutputTag();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeWithDynamicPins();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARASHADER_API UEnum* Z_Construct_UEnum_NiagaraShader_FNiagaraCompileEventSeverity();
// End Cross Module References
	void UNiagaraNodeOutputTag::StaticRegisterNativesUNiagaraNodeOutputTag()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeOutputTag_NoRegister()
	{
		return UNiagaraNodeOutputTag::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeOutputTag_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEmitMessageOnFailure_MetaData[];
#endif
		static void NewProp_bEmitMessageOnFailure_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEmitMessageOnFailure;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FailureSeverity_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FailureSeverity_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FailureSeverity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNodeWithDynamicPins,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraNodeOutputTag.h" },
		{ "ModuleRelativePath", "Private/NiagaraNodeOutputTag.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_bEmitMessageOnFailure_MetaData[] = {
		{ "Category", "Tag" },
		{ "ModuleRelativePath", "Private/NiagaraNodeOutputTag.h" },
	};
#endif
	void Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_bEmitMessageOnFailure_SetBit(void* Obj)
	{
		((UNiagaraNodeOutputTag*)Obj)->bEmitMessageOnFailure = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_bEmitMessageOnFailure = { "bEmitMessageOnFailure", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraNodeOutputTag), &Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_bEmitMessageOnFailure_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_bEmitMessageOnFailure_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_bEmitMessageOnFailure_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_FailureSeverity_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_FailureSeverity_MetaData[] = {
		{ "Category", "Tag" },
		{ "ModuleRelativePath", "Private/NiagaraNodeOutputTag.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_FailureSeverity = { "FailureSeverity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeOutputTag, FailureSeverity), Z_Construct_UEnum_NiagaraShader_FNiagaraCompileEventSeverity, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_FailureSeverity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_FailureSeverity_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_bEmitMessageOnFailure,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_FailureSeverity_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::NewProp_FailureSeverity,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeOutputTag>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::ClassParams = {
		&UNiagaraNodeOutputTag::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeOutputTag()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeOutputTag_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeOutputTag, 3254425574);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeOutputTag>()
	{
		return UNiagaraNodeOutputTag::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeOutputTag(Z_Construct_UClass_UNiagaraNodeOutputTag, &UNiagaraNodeOutputTag::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeOutputTag"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeOutputTag);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
