// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNode() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNode_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNode();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphNode();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	void UNiagaraNode::StaticRegisterNativesUNiagaraNode()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNode_NoRegister()
	{
		return UNiagaraNode::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChangeId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChangeId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNode_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraNode.h" },
		{ "ModuleRelativePath", "Public/NiagaraNode.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNode_Statics::NewProp_ChangeId_MetaData[] = {
		{ "Comment", "/** The current change identifier for this node. Used to sync status with UNiagaraScripts.*/" },
		{ "ModuleRelativePath", "Public/NiagaraNode.h" },
		{ "ToolTip", "The current change identifier for this node. Used to sync status with UNiagaraScripts." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNode_Statics::NewProp_ChangeId = { "ChangeId", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNode, ChangeId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNode_Statics::NewProp_ChangeId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNode_Statics::NewProp_ChangeId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNode_Statics::NewProp_ChangeId,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNode_Statics::ClassParams = {
		&UNiagaraNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNode_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNode, 775422018);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNode>()
	{
		return UNiagaraNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNode(Z_Construct_UClass_UNiagaraNode, &UNiagaraNode::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
