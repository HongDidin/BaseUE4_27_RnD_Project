// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ActorFactoryNiagara.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorFactoryNiagara() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UActorFactoryNiagara_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UActorFactoryNiagara();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
// End Cross Module References
	void UActorFactoryNiagara::StaticRegisterNativesUActorFactoryNiagara()
	{
	}
	UClass* Z_Construct_UClass_UActorFactoryNiagara_NoRegister()
	{
		return UActorFactoryNiagara::StaticClass();
	}
	struct Z_Construct_UClass_UActorFactoryNiagara_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorFactoryNiagara_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorFactoryNiagara_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "ActorFactoryNiagara.h" },
		{ "ModuleRelativePath", "Public/ActorFactoryNiagara.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorFactoryNiagara_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorFactoryNiagara>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorFactoryNiagara_Statics::ClassParams = {
		&UActorFactoryNiagara::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UActorFactoryNiagara_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorFactoryNiagara_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorFactoryNiagara()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorFactoryNiagara_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorFactoryNiagara, 1370978119);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UActorFactoryNiagara>()
	{
		return UActorFactoryNiagara::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorFactoryNiagara(Z_Construct_UClass_UActorFactoryNiagara, &UActorFactoryNiagara::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UActorFactoryNiagara"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorFactoryNiagara);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
