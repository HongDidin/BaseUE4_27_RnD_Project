// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/Sequencer/NiagaraSequence/Sections/MovieSceneNiagaraEmitterSection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneNiagaraEmitterSection() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneChannel();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariable();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterSection();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase();
// End Cross Module References

static_assert(std::is_polymorphic<FMovieSceneNiagaraEmitterChannel>() == std::is_polymorphic<FMovieSceneChannel>(), "USTRUCT FMovieSceneNiagaraEmitterChannel cannot be polymorphic unless super FMovieSceneChannel is polymorphic");

class UScriptStruct* FMovieSceneNiagaraEmitterChannel::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("MovieSceneNiagaraEmitterChannel"), sizeof(FMovieSceneNiagaraEmitterChannel), Get_Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FMovieSceneNiagaraEmitterChannel>()
{
	return FMovieSceneNiagaraEmitterChannel::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneNiagaraEmitterChannel(FMovieSceneNiagaraEmitterChannel::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("MovieSceneNiagaraEmitterChannel"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFMovieSceneNiagaraEmitterChannel
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFMovieSceneNiagaraEmitterChannel()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneNiagaraEmitterChannel>(FName(TEXT("MovieSceneNiagaraEmitterChannel")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFMovieSceneNiagaraEmitterChannel;
	struct Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/Sections/MovieSceneNiagaraEmitterSection.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneNiagaraEmitterChannel>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		Z_Construct_UScriptStruct_FMovieSceneChannel,
		&NewStructOps,
		"MovieSceneNiagaraEmitterChannel",
		sizeof(FMovieSceneNiagaraEmitterChannel),
		alignof(FMovieSceneNiagaraEmitterChannel),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneNiagaraEmitterChannel"), sizeof(FMovieSceneNiagaraEmitterChannel), Get_Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneNiagaraEmitterChannel_Hash() { return 1233538442U; }
class UScriptStruct* FNiagaraEmitterSectionKey::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraEmitterSectionKey"), sizeof(FNiagaraEmitterSectionKey), Get_Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraEmitterSectionKey>()
{
	return FNiagaraEmitterSectionKey::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraEmitterSectionKey(FNiagaraEmitterSectionKey::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraEmitterSectionKey"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraEmitterSectionKey
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraEmitterSectionKey()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraEmitterSectionKey>(FName(TEXT("NiagaraEmitterSectionKey")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraEmitterSectionKey;
	struct Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModuleId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModuleId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Defines data for keys in this emitter section. */" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/Sections/MovieSceneNiagaraEmitterSection.h" },
		{ "ToolTip", "Defines data for keys in this emitter section." },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraEmitterSectionKey>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewProp_ModuleId_MetaData[] = {
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/Sections/MovieSceneNiagaraEmitterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewProp_ModuleId = { "ModuleId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraEmitterSectionKey, ModuleId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewProp_ModuleId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewProp_ModuleId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewProp_Value_MetaData[] = {
		{ "Category", "Value" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/Sections/MovieSceneNiagaraEmitterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraEmitterSectionKey, Value), Z_Construct_UScriptStruct_FNiagaraVariable, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewProp_ModuleId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraEmitterSectionKey",
		sizeof(FNiagaraEmitterSectionKey),
		alignof(FNiagaraEmitterSectionKey),
		Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraEmitterSectionKey"), sizeof(FNiagaraEmitterSectionKey), Get_Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraEmitterSectionKey_Hash() { return 3012107452U; }
	void UMovieSceneNiagaraEmitterSection::StaticRegisterNativesUMovieSceneNiagaraEmitterSection()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_NoRegister()
	{
		return UMovieSceneNiagaraEmitterSection::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumLoops_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumLoops;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bStartTimeIncludedInFirstLoopOnly_MetaData[];
#endif
		static void NewProp_bStartTimeIncludedInFirstLoopOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bStartTimeIncludedInFirstLoopOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneNiagaraEmitterSectionBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n *\x09Niagara editor movie scene section; represents one emitter in the timeline\n */" },
		{ "IncludePath", "Sequencer/NiagaraSequence/Sections/MovieSceneNiagaraEmitterSection.h" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/Sections/MovieSceneNiagaraEmitterSection.h" },
		{ "ToolTip", "Niagara editor movie scene section; represents one emitter in the timeline" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_NumLoops_MetaData[] = {
		{ "Category", "Looping" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/Sections/MovieSceneNiagaraEmitterSection.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_NumLoops = { "NumLoops", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneNiagaraEmitterSection, NumLoops), METADATA_PARAMS(Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_NumLoops_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_NumLoops_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_bStartTimeIncludedInFirstLoopOnly_MetaData[] = {
		{ "Category", "Looping" },
		{ "ModuleRelativePath", "Private/Sequencer/NiagaraSequence/Sections/MovieSceneNiagaraEmitterSection.h" },
	};
#endif
	void Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_bStartTimeIncludedInFirstLoopOnly_SetBit(void* Obj)
	{
		((UMovieSceneNiagaraEmitterSection*)Obj)->bStartTimeIncludedInFirstLoopOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_bStartTimeIncludedInFirstLoopOnly = { "bStartTimeIncludedInFirstLoopOnly", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMovieSceneNiagaraEmitterSection), &Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_bStartTimeIncludedInFirstLoopOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_bStartTimeIncludedInFirstLoopOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_bStartTimeIncludedInFirstLoopOnly_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_NumLoops,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::NewProp_bStartTimeIncludedInFirstLoopOnly,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneNiagaraEmitterSection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::ClassParams = {
		&UMovieSceneNiagaraEmitterSection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::PropPointers),
		0,
		0x002000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneNiagaraEmitterSection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneNiagaraEmitterSection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneNiagaraEmitterSection, 2050757451);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UMovieSceneNiagaraEmitterSection>()
	{
		return UMovieSceneNiagaraEmitterSection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneNiagaraEmitterSection(Z_Construct_UClass_UMovieSceneNiagaraEmitterSection, &UMovieSceneNiagaraEmitterSection::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UMovieSceneNiagaraEmitterSection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneNiagaraEmitterSection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
