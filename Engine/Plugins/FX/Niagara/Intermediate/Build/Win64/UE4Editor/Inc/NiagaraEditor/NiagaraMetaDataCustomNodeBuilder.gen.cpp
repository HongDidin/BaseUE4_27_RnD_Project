// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/Customizations/NiagaraMetaDataCustomNodeBuilder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraMetaDataCustomNodeBuilder() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariableMetaData();
// End Cross Module References
class UScriptStruct* FNiagaraVariableMetaDataContainer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraVariableMetaDataContainer"), sizeof(FNiagaraVariableMetaDataContainer), Get_Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraVariableMetaDataContainer>()
{
	return FNiagaraVariableMetaDataContainer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraVariableMetaDataContainer(FNiagaraVariableMetaDataContainer::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraVariableMetaDataContainer"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraVariableMetaDataContainer
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraVariableMetaDataContainer()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraVariableMetaDataContainer>(FName(TEXT("NiagaraVariableMetaDataContainer")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraVariableMetaDataContainer;
	struct Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MetaData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MetaData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Customizations/NiagaraMetaDataCustomNodeBuilder.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraVariableMetaDataContainer>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::NewProp_MetaData_MetaData[] = {
		{ "Category", "MetaData" },
		{ "ModuleRelativePath", "Private/Customizations/NiagaraMetaDataCustomNodeBuilder.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::NewProp_MetaData = { "MetaData", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraVariableMetaDataContainer, MetaData), Z_Construct_UScriptStruct_FNiagaraVariableMetaData, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::NewProp_MetaData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::NewProp_MetaData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::NewProp_MetaData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraVariableMetaDataContainer",
		sizeof(FNiagaraVariableMetaDataContainer),
		alignof(FNiagaraVariableMetaDataContainer),
		Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraVariableMetaDataContainer"), sizeof(FNiagaraVariableMetaDataContainer), Get_Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraVariableMetaDataContainer_Hash() { return 3203703083U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
