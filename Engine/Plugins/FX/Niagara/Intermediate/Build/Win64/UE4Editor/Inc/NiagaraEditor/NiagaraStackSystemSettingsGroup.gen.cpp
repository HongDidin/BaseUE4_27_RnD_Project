// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/ViewModels/Stack/NiagaraStackSystemSettingsGroup.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraStackSystemSettingsGroup() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackSystemSettingsGroup();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackItemGroup();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackParameterStoreItem_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackParameterStoreItem();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraStackItem();
// End Cross Module References
	void UNiagaraStackSystemSettingsGroup::StaticRegisterNativesUNiagaraStackSystemSettingsGroup()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_NoRegister()
	{
		return UNiagaraStackSystemSettingsGroup::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackItemGroup,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackSystemSettingsGroup.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackSystemSettingsGroup.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackSystemSettingsGroup>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_Statics::ClassParams = {
		&UNiagaraStackSystemSettingsGroup::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackSystemSettingsGroup()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackSystemSettingsGroup_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackSystemSettingsGroup, 3160556567);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackSystemSettingsGroup>()
	{
		return UNiagaraStackSystemSettingsGroup::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackSystemSettingsGroup(Z_Construct_UClass_UNiagaraStackSystemSettingsGroup, &UNiagaraStackSystemSettingsGroup::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackSystemSettingsGroup"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackSystemSettingsGroup);
	void UNiagaraStackParameterStoreItem::StaticRegisterNativesUNiagaraStackParameterStoreItem()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraStackParameterStoreItem_NoRegister()
	{
		return UNiagaraStackParameterStoreItem::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraStackParameterStoreItem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraStackParameterStoreItem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraStackItem,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraStackParameterStoreItem_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ViewModels/Stack/NiagaraStackSystemSettingsGroup.h" },
		{ "ModuleRelativePath", "Public/ViewModels/Stack/NiagaraStackSystemSettingsGroup.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraStackParameterStoreItem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraStackParameterStoreItem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraStackParameterStoreItem_Statics::ClassParams = {
		&UNiagaraStackParameterStoreItem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraStackParameterStoreItem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraStackParameterStoreItem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraStackParameterStoreItem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraStackParameterStoreItem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraStackParameterStoreItem, 1956391104);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraStackParameterStoreItem>()
	{
		return UNiagaraStackParameterStoreItem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraStackParameterStoreItem(Z_Construct_UClass_UNiagaraStackParameterStoreItem, &UNiagaraStackParameterStoreItem::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraStackParameterStoreItem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraStackParameterStoreItem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
