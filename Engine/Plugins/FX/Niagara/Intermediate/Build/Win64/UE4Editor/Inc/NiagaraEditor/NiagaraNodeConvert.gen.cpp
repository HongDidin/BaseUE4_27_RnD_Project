// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Private/NiagaraNodeConvert.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeConvert() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraConvertConnection();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraConvertPinRecord();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeConvert_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeConvert();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeWithDynamicPins();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraTypeDefinition();
// End Cross Module References
class UScriptStruct* FNiagaraConvertConnection::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraConvertConnection_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraConvertConnection, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraConvertConnection"), sizeof(FNiagaraConvertConnection), Get_Z_Construct_UScriptStruct_FNiagaraConvertConnection_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraConvertConnection>()
{
	return FNiagaraConvertConnection::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraConvertConnection(FNiagaraConvertConnection::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraConvertConnection"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraConvertConnection
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraConvertConnection()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraConvertConnection>(FName(TEXT("NiagaraConvertConnection")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraConvertConnection;
	struct Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourcePinId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SourcePinId;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SourcePath_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourcePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SourcePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestinationPinId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DestinationPinId;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DestinationPath_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestinationPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DestinationPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Helper struct that stores a connection between two sockets.*/" },
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
		{ "ToolTip", "Helper struct that stores a connection between two sockets." },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraConvertConnection>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePinId_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePinId = { "SourcePinId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraConvertConnection, SourcePinId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePinId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePinId_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePath_Inner = { "SourcePath", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePath_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePath = { "SourcePath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraConvertConnection, SourcePath), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPinId_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPinId = { "DestinationPinId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraConvertConnection, DestinationPinId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPinId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPinId_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPath_Inner = { "DestinationPath", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPath_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPath = { "DestinationPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraConvertConnection, DestinationPath), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePinId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePath_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_SourcePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPinId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPath_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::NewProp_DestinationPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraConvertConnection",
		sizeof(FNiagaraConvertConnection),
		alignof(FNiagaraConvertConnection),
		Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraConvertConnection()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraConvertConnection_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraConvertConnection"), sizeof(FNiagaraConvertConnection), Get_Z_Construct_UScriptStruct_FNiagaraConvertConnection_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraConvertConnection_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraConvertConnection_Hash() { return 3017108644U; }
class UScriptStruct* FNiagaraConvertPinRecord::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNiagaraConvertPinRecord, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("NiagaraConvertPinRecord"), sizeof(FNiagaraConvertPinRecord), Get_Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FNiagaraConvertPinRecord>()
{
	return FNiagaraConvertPinRecord::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNiagaraConvertPinRecord(FNiagaraConvertPinRecord::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("NiagaraConvertPinRecord"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraConvertPinRecord
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraConvertPinRecord()
	{
		UScriptStruct::DeferCppStructOps<FNiagaraConvertPinRecord>(FName(TEXT("NiagaraConvertPinRecord")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFNiagaraConvertPinRecord;
	struct Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PinId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PinId;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Path_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Path_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Path;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Helper struct that stores the location of a socket.*/" },
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
		{ "ToolTip", "Helper struct that stores the location of a socket." },
	};
#endif
	void* Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNiagaraConvertPinRecord>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_PinId_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_PinId = { "PinId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraConvertPinRecord, PinId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_PinId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_PinId_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_Path_Inner = { "Path", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_Path_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_Path = { "Path", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNiagaraConvertPinRecord, Path), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_Path_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_Path_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_PinId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_Path_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::NewProp_Path,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"NiagaraConvertPinRecord",
		sizeof(FNiagaraConvertPinRecord),
		alignof(FNiagaraConvertPinRecord),
		Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNiagaraConvertPinRecord()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NiagaraConvertPinRecord"), sizeof(FNiagaraConvertPinRecord), Get_Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNiagaraConvertPinRecord_Hash() { return 3552643417U; }
	void UNiagaraNodeConvert::StaticRegisterNativesUNiagaraNodeConvert()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeConvert_NoRegister()
	{
		return UNiagaraNodeConvert::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeConvert_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AutowireSwizzle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AutowireSwizzle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AutowireMakeType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AutowireMakeType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AutowireBreakType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AutowireBreakType;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Connections_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Connections_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Connections;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsWiringShown_MetaData[];
#endif
		static void NewProp_bIsWiringShown_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsWiringShown;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExpandedItems_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExpandedItems_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExpandedItems;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeConvert_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNodeWithDynamicPins,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeConvert_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A node which allows the user to build a set of arbitrary output types from an arbitrary set of input types by connecting their inner components. */" },
		{ "IncludePath", "NiagaraNodeConvert.h" },
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
		{ "ToolTip", "A node which allows the user to build a set of arbitrary output types from an arbitrary set of input types by connecting their inner components." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireSwizzle_MetaData[] = {
		{ "Comment", "//A swizzle string set externally to instruct the autowiring code.\n" },
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
		{ "ToolTip", "A swizzle string set externally to instruct the autowiring code." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireSwizzle = { "AutowireSwizzle", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeConvert, AutowireSwizzle), METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireSwizzle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireSwizzle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireMakeType_MetaData[] = {
		{ "Comment", "//A type def used when auto wiring up the convert node.\n" },
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
		{ "ToolTip", "A type def used when auto wiring up the convert node." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireMakeType = { "AutowireMakeType", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeConvert, AutowireMakeType), Z_Construct_UScriptStruct_FNiagaraTypeDefinition, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireMakeType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireMakeType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireBreakType_MetaData[] = {
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireBreakType = { "AutowireBreakType", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeConvert, AutowireBreakType), Z_Construct_UScriptStruct_FNiagaraTypeDefinition, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireBreakType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireBreakType_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_Connections_Inner = { "Connections", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraConvertConnection, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_Connections_MetaData[] = {
		{ "Comment", "/** The internal connections for this node. */" },
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
		{ "ToolTip", "The internal connections for this node." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_Connections = { "Connections", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeConvert, Connections), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_Connections_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_Connections_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_bIsWiringShown_MetaData[] = {
		{ "Comment", "/** Is the switcboard UI shown?*/" },
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
		{ "ToolTip", "Is the switcboard UI shown?" },
	};
#endif
	void Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_bIsWiringShown_SetBit(void* Obj)
	{
		((UNiagaraNodeConvert*)Obj)->bIsWiringShown = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_bIsWiringShown = { "bIsWiringShown", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraNodeConvert), &Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_bIsWiringShown_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_bIsWiringShown_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_bIsWiringShown_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_ExpandedItems_Inner = { "ExpandedItems", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNiagaraConvertPinRecord, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_ExpandedItems_MetaData[] = {
		{ "Comment", "/** Store of all sockets that are expanded.*/" },
		{ "ModuleRelativePath", "Private/NiagaraNodeConvert.h" },
		{ "ToolTip", "Store of all sockets that are expanded." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_ExpandedItems = { "ExpandedItems", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeConvert, ExpandedItems), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_ExpandedItems_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_ExpandedItems_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNodeConvert_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireSwizzle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireMakeType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_AutowireBreakType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_Connections_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_Connections,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_bIsWiringShown,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_ExpandedItems_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeConvert_Statics::NewProp_ExpandedItems,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeConvert_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeConvert>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeConvert_Statics::ClassParams = {
		&UNiagaraNodeConvert::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNodeConvert_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeConvert_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeConvert_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeConvert_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeConvert()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeConvert_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeConvert, 3228830460);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeConvert>()
	{
		return UNiagaraNodeConvert::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeConvert(Z_Construct_UClass_UNiagaraNodeConvert, &UNiagaraNodeConvert::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeConvert"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeConvert);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
