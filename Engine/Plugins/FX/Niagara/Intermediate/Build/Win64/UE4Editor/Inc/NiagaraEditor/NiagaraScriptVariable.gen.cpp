// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraScriptVariable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraScriptVariable() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScriptVariable_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScriptVariable();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARA_API UEnum* Z_Construct_UEnum_Niagara_ENiagaraDefaultMode();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraScriptVariableBinding();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariable();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariableMetaData();
	NIAGARA_API UScriptStruct* Z_Construct_UScriptStruct_FNiagaraVariant();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	void UNiagaraScriptVariable::StaticRegisterNativesUNiagaraScriptVariable()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraScriptVariable_NoRegister()
	{
		return UNiagaraScriptVariable::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraScriptVariable_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DefaultMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DefaultMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultBinding_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultBinding;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Variable_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Variable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Metadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Metadata;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValueVariant_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultValueVariant;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticSwitchDefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StaticSwitchDefaultValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsStaticSwitch_MetaData[];
#endif
		static void NewProp_bIsStaticSwitch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsStaticSwitch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSubscribedToParameterDefinitions_MetaData[];
#endif
		static void NewProp_bSubscribedToParameterDefinitions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSubscribedToParameterDefinitions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChangeId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChangeId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideParameterDefinitionsDefaultValue_MetaData[];
#endif
		static void NewProp_bOverrideParameterDefinitionsDefaultValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideParameterDefinitionsDefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraScriptVariable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/*\n* Used to store variable data and metadata per graph. \n*/" },
		{ "IncludePath", "NiagaraScriptVariable.h" },
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
		{ "ToolTip", "* Used to store variable data and metadata per graph." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultMode_MetaData[] = {
		{ "Category", "Default Value" },
		{ "Comment", "/** The default mode. Can be Value, Binding or Custom. */" },
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
		{ "ToolTip", "The default mode. Can be Value, Binding or Custom." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultMode = { "DefaultMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraScriptVariable, DefaultMode), Z_Construct_UEnum_Niagara_ENiagaraDefaultMode, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultBinding_MetaData[] = {
		{ "Category", "Default Value" },
		{ "Comment", "/** The default binding. Only used if DefaultMode == ENiagaraDefaultMode::Binding. */" },
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
		{ "ToolTip", "The default binding. Only used if DefaultMode == ENiagaraDefaultMode::Binding." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultBinding = { "DefaultBinding", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraScriptVariable, DefaultBinding), Z_Construct_UScriptStruct_FNiagaraScriptVariableBinding, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultBinding_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultBinding_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_Variable_MetaData[] = {
		{ "Comment", "/** Variable type, name and data. The data is not persistent, but used as a buffer when interfacing elsewhere. */" },
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
		{ "ToolTip", "Variable type, name and data. The data is not persistent, but used as a buffer when interfacing elsewhere." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_Variable = { "Variable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraScriptVariable, Variable), Z_Construct_UScriptStruct_FNiagaraVariable, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_Variable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_Variable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_Metadata_MetaData[] = {
		{ "Category", "Variable" },
		{ "Comment", "/** The metadata associated with this script variable. */" },
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "The metadata associated with this script variable." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_Metadata = { "Metadata", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraScriptVariable, Metadata), Z_Construct_UScriptStruct_FNiagaraVariableMetaData, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_Metadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_Metadata_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultValueVariant_MetaData[] = {
		{ "Category", "Hidden" },
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultValueVariant = { "DefaultValueVariant", nullptr, (EPropertyFlags)0x0040008000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraScriptVariable, DefaultValueVariant), Z_Construct_UScriptStruct_FNiagaraVariant, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultValueVariant_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultValueVariant_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_StaticSwitchDefaultValue_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_StaticSwitchDefaultValue = { "StaticSwitchDefaultValue", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraScriptVariable, StaticSwitchDefaultValue), METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_StaticSwitchDefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_StaticSwitchDefaultValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bIsStaticSwitch_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
	};
#endif
	void Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bIsStaticSwitch_SetBit(void* Obj)
	{
		((UNiagaraScriptVariable*)Obj)->bIsStaticSwitch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bIsStaticSwitch = { "bIsStaticSwitch", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraScriptVariable), &Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bIsStaticSwitch_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bIsStaticSwitch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bIsStaticSwitch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bSubscribedToParameterDefinitions_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
	};
#endif
	void Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bSubscribedToParameterDefinitions_SetBit(void* Obj)
	{
		((UNiagaraScriptVariable*)Obj)->bSubscribedToParameterDefinitions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bSubscribedToParameterDefinitions = { "bSubscribedToParameterDefinitions", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraScriptVariable), &Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bSubscribedToParameterDefinitions_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bSubscribedToParameterDefinitions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bSubscribedToParameterDefinitions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_ChangeId_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
		{ "SkipForCompileHash", "true" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_ChangeId = { "ChangeId", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraScriptVariable, ChangeId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_ChangeId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_ChangeId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bOverrideParameterDefinitionsDefaultValue_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraScriptVariable.h" },
	};
#endif
	void Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bOverrideParameterDefinitionsDefaultValue_SetBit(void* Obj)
	{
		((UNiagaraScriptVariable*)Obj)->bOverrideParameterDefinitionsDefaultValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bOverrideParameterDefinitionsDefaultValue = { "bOverrideParameterDefinitionsDefaultValue", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNiagaraScriptVariable), &Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bOverrideParameterDefinitionsDefaultValue_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bOverrideParameterDefinitionsDefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bOverrideParameterDefinitionsDefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraScriptVariable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultBinding,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_Variable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_Metadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_DefaultValueVariant,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_StaticSwitchDefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bIsStaticSwitch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bSubscribedToParameterDefinitions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_ChangeId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraScriptVariable_Statics::NewProp_bOverrideParameterDefinitionsDefaultValue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraScriptVariable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraScriptVariable>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraScriptVariable_Statics::ClassParams = {
		&UNiagaraScriptVariable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraScriptVariable_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptVariable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptVariable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraScriptVariable()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraScriptVariable_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraScriptVariable, 3348672364);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraScriptVariable>()
	{
		return UNiagaraScriptVariable::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraScriptVariable(Z_Construct_UClass_UNiagaraScriptVariable, &UNiagaraScriptVariable::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraScriptVariable"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraScriptVariable);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
