// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraScratchPadViewModel_generated_h
#error "NiagaraScratchPadViewModel.generated.h already included, missing '#pragma once' in NiagaraScratchPadViewModel.h"
#endif
#define NIAGARAEDITOR_NiagaraScratchPadViewModel_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraScratchPadViewModel(); \
	friend struct Z_Construct_UClass_UNiagaraScratchPadViewModel_Statics; \
public: \
	DECLARE_CLASS(UNiagaraScratchPadViewModel, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraScratchPadViewModel)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraScratchPadViewModel(); \
	friend struct Z_Construct_UClass_UNiagaraScratchPadViewModel_Statics; \
public: \
	DECLARE_CLASS(UNiagaraScratchPadViewModel, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraScratchPadViewModel)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraScratchPadViewModel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraScratchPadViewModel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraScratchPadViewModel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraScratchPadViewModel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraScratchPadViewModel(UNiagaraScratchPadViewModel&&); \
	NO_API UNiagaraScratchPadViewModel(const UNiagaraScratchPadViewModel&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraScratchPadViewModel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraScratchPadViewModel(UNiagaraScratchPadViewModel&&); \
	NO_API UNiagaraScratchPadViewModel(const UNiagaraScratchPadViewModel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraScratchPadViewModel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraScratchPadViewModel); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraScratchPadViewModel)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_14_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraScratchPadViewModel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_ViewModels_NiagaraScratchPadViewModel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
