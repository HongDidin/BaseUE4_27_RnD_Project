// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraScriptFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraScriptFactoryNew() {}
// Cross Module References
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScriptFactoryNew_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraScriptFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraModuleScriptFactory_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraModuleScriptFactory();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraFunctionScriptFactory_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraFunctionScriptFactory();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraDynamicInputScriptFactory();
// End Cross Module References
	void UNiagaraScriptFactoryNew::StaticRegisterNativesUNiagaraScriptFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraScriptFactoryNew_NoRegister()
	{
		return UNiagaraScriptFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraScriptFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraScriptFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraScriptFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "NiagaraScriptFactoryNew.h" },
		{ "ModuleRelativePath", "Public/NiagaraScriptFactoryNew.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraScriptFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraScriptFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraScriptFactoryNew_Statics::ClassParams = {
		&UNiagaraScriptFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraScriptFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraScriptFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraScriptFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraScriptFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraScriptFactoryNew, 898382713);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraScriptFactoryNew>()
	{
		return UNiagaraScriptFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraScriptFactoryNew(Z_Construct_UClass_UNiagaraScriptFactoryNew, &UNiagaraScriptFactoryNew::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraScriptFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraScriptFactoryNew);
	void UNiagaraModuleScriptFactory::StaticRegisterNativesUNiagaraModuleScriptFactory()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraModuleScriptFactory_NoRegister()
	{
		return UNiagaraModuleScriptFactory::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraModuleScriptFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraModuleScriptFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraScriptFactoryNew,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraModuleScriptFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Niagara module script factory.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "NiagaraScriptFactoryNew.h" },
		{ "ModuleRelativePath", "Public/NiagaraScriptFactoryNew.h" },
		{ "ToolTip", "Niagara module script factory." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraModuleScriptFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraModuleScriptFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraModuleScriptFactory_Statics::ClassParams = {
		&UNiagaraModuleScriptFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraModuleScriptFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraModuleScriptFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraModuleScriptFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraModuleScriptFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraModuleScriptFactory, 3619683284);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraModuleScriptFactory>()
	{
		return UNiagaraModuleScriptFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraModuleScriptFactory(Z_Construct_UClass_UNiagaraModuleScriptFactory, &UNiagaraModuleScriptFactory::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraModuleScriptFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraModuleScriptFactory);
	void UNiagaraFunctionScriptFactory::StaticRegisterNativesUNiagaraFunctionScriptFactory()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraFunctionScriptFactory_NoRegister()
	{
		return UNiagaraFunctionScriptFactory::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraFunctionScriptFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraFunctionScriptFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraScriptFactoryNew,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraFunctionScriptFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Niagara function factory.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "NiagaraScriptFactoryNew.h" },
		{ "ModuleRelativePath", "Public/NiagaraScriptFactoryNew.h" },
		{ "ToolTip", "Niagara function factory." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraFunctionScriptFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraFunctionScriptFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraFunctionScriptFactory_Statics::ClassParams = {
		&UNiagaraFunctionScriptFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraFunctionScriptFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraFunctionScriptFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraFunctionScriptFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraFunctionScriptFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraFunctionScriptFactory, 3773483450);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraFunctionScriptFactory>()
	{
		return UNiagaraFunctionScriptFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraFunctionScriptFactory(Z_Construct_UClass_UNiagaraFunctionScriptFactory, &UNiagaraFunctionScriptFactory::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraFunctionScriptFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraFunctionScriptFactory);
	void UNiagaraDynamicInputScriptFactory::StaticRegisterNativesUNiagaraDynamicInputScriptFactory()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_NoRegister()
	{
		return UNiagaraDynamicInputScriptFactory::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraScriptFactoryNew,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Niagara dynamic input script factory.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "NiagaraScriptFactoryNew.h" },
		{ "ModuleRelativePath", "Public/NiagaraScriptFactoryNew.h" },
		{ "ToolTip", "Niagara dynamic input script factory." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraDynamicInputScriptFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_Statics::ClassParams = {
		&UNiagaraDynamicInputScriptFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraDynamicInputScriptFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraDynamicInputScriptFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraDynamicInputScriptFactory, 1190452738);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraDynamicInputScriptFactory>()
	{
		return UNiagaraDynamicInputScriptFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraDynamicInputScriptFactory(Z_Construct_UClass_UNiagaraDynamicInputScriptFactory, &UNiagaraDynamicInputScriptFactory::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraDynamicInputScriptFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraDynamicInputScriptFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
