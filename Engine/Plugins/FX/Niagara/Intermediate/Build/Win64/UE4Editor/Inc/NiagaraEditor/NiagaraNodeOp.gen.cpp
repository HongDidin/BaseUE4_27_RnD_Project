// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NiagaraEditor/Public/NiagaraNodeOp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraNodeOp() {}
// Cross Module References
	NIAGARAEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FAddedPinData();
	UPackage* Z_Construct_UPackage__Script_NiagaraEditor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphPinType();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeOp_NoRegister();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeOp();
	NIAGARAEDITOR_API UClass* Z_Construct_UClass_UNiagaraNodeWithDynamicPins();
// End Cross Module References
class UScriptStruct* FAddedPinData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NIAGARAEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FAddedPinData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAddedPinData, Z_Construct_UPackage__Script_NiagaraEditor(), TEXT("AddedPinData"), sizeof(FAddedPinData), Get_Z_Construct_UScriptStruct_FAddedPinData_Hash());
	}
	return Singleton;
}
template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<FAddedPinData>()
{
	return FAddedPinData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAddedPinData(FAddedPinData::StaticStruct, TEXT("/Script/NiagaraEditor"), TEXT("AddedPinData"), false, nullptr, nullptr);
static struct FScriptStruct_NiagaraEditor_StaticRegisterNativesFAddedPinData
{
	FScriptStruct_NiagaraEditor_StaticRegisterNativesFAddedPinData()
	{
		UScriptStruct::DeferCppStructOps<FAddedPinData>(FName(TEXT("AddedPinData")));
	}
} ScriptStruct_NiagaraEditor_StaticRegisterNativesFAddedPinData;
	struct Z_Construct_UScriptStruct_FAddedPinData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PinType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PinType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PinName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PinName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAddedPinData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NiagaraNodeOp.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAddedPinData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAddedPinData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAddedPinData_Statics::NewProp_PinType_MetaData[] = {
		{ "Comment", "/** The data type of the added pin */" },
		{ "ModuleRelativePath", "Public/NiagaraNodeOp.h" },
		{ "ToolTip", "The data type of the added pin" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAddedPinData_Statics::NewProp_PinType = { "PinType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAddedPinData, PinType), Z_Construct_UScriptStruct_FEdGraphPinType, METADATA_PARAMS(Z_Construct_UScriptStruct_FAddedPinData_Statics::NewProp_PinType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAddedPinData_Statics::NewProp_PinType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAddedPinData_Statics::NewProp_PinName_MetaData[] = {
		{ "Comment", "/** The name type of the added pin */" },
		{ "ModuleRelativePath", "Public/NiagaraNodeOp.h" },
		{ "ToolTip", "The name type of the added pin" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAddedPinData_Statics::NewProp_PinName = { "PinName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAddedPinData, PinName), METADATA_PARAMS(Z_Construct_UScriptStruct_FAddedPinData_Statics::NewProp_PinName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAddedPinData_Statics::NewProp_PinName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAddedPinData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAddedPinData_Statics::NewProp_PinType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAddedPinData_Statics::NewProp_PinName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAddedPinData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
		nullptr,
		&NewStructOps,
		"AddedPinData",
		sizeof(FAddedPinData),
		alignof(FAddedPinData),
		Z_Construct_UScriptStruct_FAddedPinData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAddedPinData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAddedPinData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAddedPinData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAddedPinData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAddedPinData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NiagaraEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AddedPinData"), sizeof(FAddedPinData), Get_Z_Construct_UScriptStruct_FAddedPinData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAddedPinData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAddedPinData_Hash() { return 3498972638U; }
	void UNiagaraNodeOp::StaticRegisterNativesUNiagaraNodeOp()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraNodeOp_NoRegister()
	{
		return UNiagaraNodeOp::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraNodeOp_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OpName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_OpName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AddedPins_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AddedPins_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AddedPins;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraNodeOp_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraNodeWithDynamicPins,
		(UObject* (*)())Z_Construct_UPackage__Script_NiagaraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeOp_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraNodeOp.h" },
		{ "ModuleRelativePath", "Public/NiagaraNodeOp.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_OpName_MetaData[] = {
		{ "Comment", "/** Name of operation */" },
		{ "ModuleRelativePath", "Public/NiagaraNodeOp.h" },
		{ "ToolTip", "Name of operation" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_OpName = { "OpName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeOp, OpName), METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_OpName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_OpName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_AddedPins_Inner = { "AddedPins", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAddedPinData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_AddedPins_MetaData[] = {
		{ "ModuleRelativePath", "Public/NiagaraNodeOp.h" },
		{ "SkipForCompileHash", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_AddedPins = { "AddedPins", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraNodeOp, AddedPins), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_AddedPins_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_AddedPins_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraNodeOp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_OpName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_AddedPins_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraNodeOp_Statics::NewProp_AddedPins,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraNodeOp_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraNodeOp>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraNodeOp_Statics::ClassParams = {
		&UNiagaraNodeOp::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraNodeOp_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeOp_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraNodeOp_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraNodeOp_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraNodeOp()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraNodeOp_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraNodeOp, 1049180464);
	template<> NIAGARAEDITOR_API UClass* StaticClass<UNiagaraNodeOp>()
	{
		return UNiagaraNodeOp::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraNodeOp(Z_Construct_UClass_UNiagaraNodeOp, &UNiagaraNodeOp::StaticClass, TEXT("/Script/NiagaraEditor"), TEXT("UNiagaraNodeOp"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraNodeOp);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
