// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraSystemAuditCommandlet_generated_h
#error "NiagaraSystemAuditCommandlet.generated.h already included, missing '#pragma once' in NiagaraSystemAuditCommandlet.h"
#endif
#define NIAGARAEDITOR_NiagaraSystemAuditCommandlet_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraSystemAuditCommandlet(); \
	friend struct Z_Construct_UClass_UNiagaraSystemAuditCommandlet_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSystemAuditCommandlet, UCommandlet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSystemAuditCommandlet) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraSystemAuditCommandlet(); \
	friend struct Z_Construct_UClass_UNiagaraSystemAuditCommandlet_Statics; \
public: \
	DECLARE_CLASS(UNiagaraSystemAuditCommandlet, UCommandlet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraSystemAuditCommandlet) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSystemAuditCommandlet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSystemAuditCommandlet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSystemAuditCommandlet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSystemAuditCommandlet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSystemAuditCommandlet(UNiagaraSystemAuditCommandlet&&); \
	NO_API UNiagaraSystemAuditCommandlet(const UNiagaraSystemAuditCommandlet&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraSystemAuditCommandlet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraSystemAuditCommandlet(UNiagaraSystemAuditCommandlet&&); \
	NO_API UNiagaraSystemAuditCommandlet(const UNiagaraSystemAuditCommandlet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraSystemAuditCommandlet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraSystemAuditCommandlet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraSystemAuditCommandlet)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_10_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraSystemAuditCommandlet."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraSystemAuditCommandlet>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_Commandlets_NiagaraSystemAuditCommandlet_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
