// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NIAGARAEDITOR_NiagaraMessages_generated_h
#error "NiagaraMessages.generated.h already included, missing '#pragma once' in NiagaraMessages.h"
#endif
#define NIAGARAEDITOR_NiagaraMessages_generated_h

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_240_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNiagaraStackMessage_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NIAGARAEDITOR_API UScriptStruct* StaticStruct<struct FNiagaraStackMessage>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraMessageData(); \
	friend struct Z_Construct_UClass_UNiagaraMessageData_Statics; \
public: \
	DECLARE_CLASS(UNiagaraMessageData, UNiagaraMessageDataBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraMessageData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraMessageData(); \
	friend struct Z_Construct_UClass_UNiagaraMessageData_Statics; \
public: \
	DECLARE_CLASS(UNiagaraMessageData, UNiagaraMessageDataBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraMessageData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraMessageData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraMessageData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraMessageData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraMessageData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraMessageData(UNiagaraMessageData&&); \
	NO_API UNiagaraMessageData(const UNiagaraMessageData&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraMessageData() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraMessageData(UNiagaraMessageData&&); \
	NO_API UNiagaraMessageData(const UNiagaraMessageData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraMessageData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraMessageData); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UNiagaraMessageData)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_277_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_280_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraMessageData>();

#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_SPARSE_DATA
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_RPC_WRAPPERS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraMessageDataText(); \
	friend struct Z_Construct_UClass_UNiagaraMessageDataText_Statics; \
public: \
	DECLARE_CLASS(UNiagaraMessageDataText, UNiagaraMessageData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraMessageDataText)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraMessageDataText(); \
	friend struct Z_Construct_UClass_UNiagaraMessageDataText_Statics; \
public: \
	DECLARE_CLASS(UNiagaraMessageDataText, UNiagaraMessageData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NiagaraEditor"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraMessageDataText)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraMessageDataText(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraMessageDataText) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraMessageDataText); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraMessageDataText); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraMessageDataText(UNiagaraMessageDataText&&); \
	NO_API UNiagaraMessageDataText(const UNiagaraMessageDataText&); \
public:


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraMessageDataText() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraMessageDataText(UNiagaraMessageDataText&&); \
	NO_API UNiagaraMessageDataText(const UNiagaraMessageDataText&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraMessageDataText); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraMessageDataText); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraMessageDataText)


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MessageText() { return STRUCT_OFFSET(UNiagaraMessageDataText, MessageText); } \
	FORCEINLINE static uint32 __PPO__ShortDescription() { return STRUCT_OFFSET(UNiagaraMessageDataText, ShortDescription); } \
	FORCEINLINE static uint32 __PPO__MessageSeverity() { return STRUCT_OFFSET(UNiagaraMessageDataText, MessageSeverity); } \
	FORCEINLINE static uint32 __PPO__bAllowDismissal() { return STRUCT_OFFSET(UNiagaraMessageDataText, bAllowDismissal); } \
	FORCEINLINE static uint32 __PPO__TopicName() { return STRUCT_OFFSET(UNiagaraMessageDataText, TopicName); }


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_286_PROLOG
#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_RPC_WRAPPERS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_INCLASS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_SPARSE_DATA \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h_289_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NIAGARAEDITOR_API UClass* StaticClass<class UNiagaraMessageDataText>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_FX_Niagara_Source_NiagaraEditor_Public_NiagaraMessages_h


#define FOREACH_ENUM_ENIAGARAMESSAGESEVERITY(op) \
	op(ENiagaraMessageSeverity::CriticalError) \
	op(ENiagaraMessageSeverity::Error) \
	op(ENiagaraMessageSeverity::PerformanceWarning) \
	op(ENiagaraMessageSeverity::Warning) \
	op(ENiagaraMessageSeverity::Info) \
	op(ENiagaraMessageSeverity::CustomNote) 

enum class ENiagaraMessageSeverity : uint8;
template<> NIAGARAEDITOR_API UEnum* StaticEnum<ENiagaraMessageSeverity>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
