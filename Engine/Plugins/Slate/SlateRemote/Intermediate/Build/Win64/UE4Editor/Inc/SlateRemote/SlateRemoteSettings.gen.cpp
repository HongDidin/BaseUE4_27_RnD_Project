// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SlateRemote/Private/Shared/SlateRemoteSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSlateRemoteSettings() {}
// Cross Module References
	SLATEREMOTE_API UClass* Z_Construct_UClass_USlateRemoteSettings_NoRegister();
	SLATEREMOTE_API UClass* Z_Construct_UClass_USlateRemoteSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_SlateRemote();
// End Cross Module References
	void USlateRemoteSettings::StaticRegisterNativesUSlateRemoteSettings()
	{
	}
	UClass* Z_Construct_UClass_USlateRemoteSettings_NoRegister()
	{
		return USlateRemoteSettings::StaticClass();
	}
	struct Z_Construct_UClass_USlateRemoteSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnableRemoteServer_MetaData[];
#endif
		static void NewProp_EnableRemoteServer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_EnableRemoteServer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditorServerEndpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_EditorServerEndpoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameServerEndpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_GameServerEndpoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USlateRemoteSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_SlateRemote,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlateRemoteSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements the settings for the Slate Remote plug-in.\n */" },
		{ "IncludePath", "Shared/SlateRemoteSettings.h" },
		{ "ModuleRelativePath", "Private/Shared/SlateRemoteSettings.h" },
		{ "ToolTip", "Implements the settings for the Slate Remote plug-in." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EnableRemoteServer_MetaData[] = {
		{ "Category", "RemoteServer" },
		{ "Comment", "/** Whether the Slate Remote server is enabled. */" },
		{ "ModuleRelativePath", "Private/Shared/SlateRemoteSettings.h" },
		{ "ToolTip", "Whether the Slate Remote server is enabled." },
	};
#endif
	void Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EnableRemoteServer_SetBit(void* Obj)
	{
		((USlateRemoteSettings*)Obj)->EnableRemoteServer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EnableRemoteServer = { "EnableRemoteServer", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USlateRemoteSettings), &Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EnableRemoteServer_SetBit, METADATA_PARAMS(Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EnableRemoteServer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EnableRemoteServer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EditorServerEndpoint_MetaData[] = {
		{ "Category", "RemoteServer" },
		{ "Comment", "/** The IP endpoint to listen to when the Remote Server runs in the Editor. */" },
		{ "ModuleRelativePath", "Private/Shared/SlateRemoteSettings.h" },
		{ "ToolTip", "The IP endpoint to listen to when the Remote Server runs in the Editor." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EditorServerEndpoint = { "EditorServerEndpoint", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlateRemoteSettings, EditorServerEndpoint), METADATA_PARAMS(Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EditorServerEndpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EditorServerEndpoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_GameServerEndpoint_MetaData[] = {
		{ "Category", "RemoteServer" },
		{ "Comment", "/** The IP endpoint to listen to when the Remote Server runs in a game. */" },
		{ "ModuleRelativePath", "Private/Shared/SlateRemoteSettings.h" },
		{ "ToolTip", "The IP endpoint to listen to when the Remote Server runs in a game." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_GameServerEndpoint = { "GameServerEndpoint", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USlateRemoteSettings, GameServerEndpoint), METADATA_PARAMS(Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_GameServerEndpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_GameServerEndpoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USlateRemoteSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EnableRemoteServer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_EditorServerEndpoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USlateRemoteSettings_Statics::NewProp_GameServerEndpoint,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USlateRemoteSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USlateRemoteSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USlateRemoteSettings_Statics::ClassParams = {
		&USlateRemoteSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USlateRemoteSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USlateRemoteSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_USlateRemoteSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USlateRemoteSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USlateRemoteSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USlateRemoteSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USlateRemoteSettings, 2586064544);
	template<> SLATEREMOTE_API UClass* StaticClass<USlateRemoteSettings>()
	{
		return USlateRemoteSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USlateRemoteSettings(Z_Construct_UClass_USlateRemoteSettings, &USlateRemoteSettings::StaticClass, TEXT("/Script/SlateRemote"), TEXT("USlateRemoteSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USlateRemoteSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
