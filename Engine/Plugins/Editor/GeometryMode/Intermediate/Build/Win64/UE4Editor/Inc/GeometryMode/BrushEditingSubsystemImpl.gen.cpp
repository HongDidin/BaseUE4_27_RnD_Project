// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryMode/Private/BrushEditingSubsystemImpl.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBrushEditingSubsystemImpl() {}
// Cross Module References
	GEOMETRYMODE_API UClass* Z_Construct_UClass_UBrushEditingSubsystemImpl_NoRegister();
	GEOMETRYMODE_API UClass* Z_Construct_UClass_UBrushEditingSubsystemImpl();
	UNREALED_API UClass* Z_Construct_UClass_UBrushEditingSubsystem();
	UPackage* Z_Construct_UPackage__Script_GeometryMode();
// End Cross Module References
	void UBrushEditingSubsystemImpl::StaticRegisterNativesUBrushEditingSubsystemImpl()
	{
	}
	UClass* Z_Construct_UClass_UBrushEditingSubsystemImpl_NoRegister()
	{
		return UBrushEditingSubsystemImpl::StaticClass();
	}
	struct Z_Construct_UClass_UBrushEditingSubsystemImpl_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBrushEditingSubsystemImpl_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBrushEditingSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBrushEditingSubsystemImpl_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BrushEditingSubsystemImpl.h" },
		{ "ModuleRelativePath", "Private/BrushEditingSubsystemImpl.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBrushEditingSubsystemImpl_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBrushEditingSubsystemImpl>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBrushEditingSubsystemImpl_Statics::ClassParams = {
		&UBrushEditingSubsystemImpl::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBrushEditingSubsystemImpl_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBrushEditingSubsystemImpl_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBrushEditingSubsystemImpl()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBrushEditingSubsystemImpl_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBrushEditingSubsystemImpl, 703602257);
	template<> GEOMETRYMODE_API UClass* StaticClass<UBrushEditingSubsystemImpl>()
	{
		return UBrushEditingSubsystemImpl::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBrushEditingSubsystemImpl(Z_Construct_UClass_UBrushEditingSubsystemImpl, &UBrushEditingSubsystemImpl::StaticClass, TEXT("/Script/GeometryMode"), TEXT("UBrushEditingSubsystemImpl"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBrushEditingSubsystemImpl);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
