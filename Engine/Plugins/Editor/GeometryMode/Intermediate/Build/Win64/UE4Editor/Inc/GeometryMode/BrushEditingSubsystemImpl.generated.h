// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GEOMETRYMODE_BrushEditingSubsystemImpl_generated_h
#error "BrushEditingSubsystemImpl.generated.h already included, missing '#pragma once' in BrushEditingSubsystemImpl.h"
#endif
#define GEOMETRYMODE_BrushEditingSubsystemImpl_generated_h

#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_SPARSE_DATA
#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_RPC_WRAPPERS
#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBrushEditingSubsystemImpl(); \
	friend struct Z_Construct_UClass_UBrushEditingSubsystemImpl_Statics; \
public: \
	DECLARE_CLASS(UBrushEditingSubsystemImpl, UBrushEditingSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GeometryMode"), NO_API) \
	DECLARE_SERIALIZER(UBrushEditingSubsystemImpl)


#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUBrushEditingSubsystemImpl(); \
	friend struct Z_Construct_UClass_UBrushEditingSubsystemImpl_Statics; \
public: \
	DECLARE_CLASS(UBrushEditingSubsystemImpl, UBrushEditingSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GeometryMode"), NO_API) \
	DECLARE_SERIALIZER(UBrushEditingSubsystemImpl)


#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBrushEditingSubsystemImpl(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBrushEditingSubsystemImpl) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBrushEditingSubsystemImpl); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBrushEditingSubsystemImpl); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBrushEditingSubsystemImpl(UBrushEditingSubsystemImpl&&); \
	NO_API UBrushEditingSubsystemImpl(const UBrushEditingSubsystemImpl&); \
public:


#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBrushEditingSubsystemImpl(UBrushEditingSubsystemImpl&&); \
	NO_API UBrushEditingSubsystemImpl(const UBrushEditingSubsystemImpl&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBrushEditingSubsystemImpl); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBrushEditingSubsystemImpl); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBrushEditingSubsystemImpl)


#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_15_PROLOG
#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_SPARSE_DATA \
	Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_RPC_WRAPPERS \
	Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_INCLASS \
	Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_SPARSE_DATA \
	Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GEOMETRYMODE_API UClass* StaticClass<class UBrushEditingSubsystemImpl>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_GeometryMode_Source_GeometryMode_Private_BrushEditingSubsystemImpl_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
