// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STYLUSINPUT_IStylusInputModule_generated_h
#error "IStylusInputModule.generated.h already included, missing '#pragma once' in IStylusInputModule.h"
#endif
#define STYLUSINPUT_IStylusInputModule_generated_h

#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_SPARSE_DATA
#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_RPC_WRAPPERS
#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUStylusInputSubsystem(); \
	friend struct Z_Construct_UClass_UStylusInputSubsystem_Statics; \
public: \
	DECLARE_CLASS(UStylusInputSubsystem, UEditorSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StylusInput"), NO_API) \
	DECLARE_SERIALIZER(UStylusInputSubsystem)


#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_INCLASS \
private: \
	static void StaticRegisterNativesUStylusInputSubsystem(); \
	friend struct Z_Construct_UClass_UStylusInputSubsystem_Statics; \
public: \
	DECLARE_CLASS(UStylusInputSubsystem, UEditorSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StylusInput"), NO_API) \
	DECLARE_SERIALIZER(UStylusInputSubsystem)


#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStylusInputSubsystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStylusInputSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStylusInputSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStylusInputSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStylusInputSubsystem(UStylusInputSubsystem&&); \
	NO_API UStylusInputSubsystem(const UStylusInputSubsystem&); \
public:


#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStylusInputSubsystem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStylusInputSubsystem(UStylusInputSubsystem&&); \
	NO_API UStylusInputSubsystem(const UStylusInputSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStylusInputSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStylusInputSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UStylusInputSubsystem)


#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_54_PROLOG
#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_SPARSE_DATA \
	Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_RPC_WRAPPERS \
	Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_INCLASS \
	Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_SPARSE_DATA \
	Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h_59_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STYLUSINPUT_API UClass* StaticClass<class UStylusInputSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_StylusInput_Source_StylusInput_Public_IStylusInputModule_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
