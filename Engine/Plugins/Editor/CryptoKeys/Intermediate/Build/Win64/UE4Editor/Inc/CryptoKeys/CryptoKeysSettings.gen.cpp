// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CryptoKeys/Classes/CryptoKeysSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCryptoKeysSettings() {}
// Cross Module References
	CRYPTOKEYS_API UScriptStruct* Z_Construct_UScriptStruct_FCryptoEncryptionKey();
	UPackage* Z_Construct_UPackage__Script_CryptoKeys();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	CRYPTOKEYS_API UClass* Z_Construct_UClass_UCryptoKeysSettings_NoRegister();
	CRYPTOKEYS_API UClass* Z_Construct_UClass_UCryptoKeysSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FCryptoEncryptionKey::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CRYPTOKEYS_API uint32 Get_Z_Construct_UScriptStruct_FCryptoEncryptionKey_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCryptoEncryptionKey, Z_Construct_UPackage__Script_CryptoKeys(), TEXT("CryptoEncryptionKey"), sizeof(FCryptoEncryptionKey), Get_Z_Construct_UScriptStruct_FCryptoEncryptionKey_Hash());
	}
	return Singleton;
}
template<> CRYPTOKEYS_API UScriptStruct* StaticStruct<FCryptoEncryptionKey>()
{
	return FCryptoEncryptionKey::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCryptoEncryptionKey(FCryptoEncryptionKey::StaticStruct, TEXT("/Script/CryptoKeys"), TEXT("CryptoEncryptionKey"), false, nullptr, nullptr);
static struct FScriptStruct_CryptoKeys_StaticRegisterNativesFCryptoEncryptionKey
{
	FScriptStruct_CryptoKeys_StaticRegisterNativesFCryptoEncryptionKey()
	{
		UScriptStruct::DeferCppStructOps<FCryptoEncryptionKey>(FName(TEXT("CryptoEncryptionKey")));
	}
} ScriptStruct_CryptoKeys_StaticRegisterNativesFCryptoEncryptionKey;
	struct Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Guid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Guid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* UStruct representing a named encryption key\n*/" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "UStruct representing a named encryption key" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCryptoEncryptionKey>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Guid_MetaData[] = {
		{ "Category", "Encryption" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Guid = { "Guid", nullptr, (EPropertyFlags)0x0010000000024001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCryptoEncryptionKey, Guid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Guid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Guid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Encryption" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCryptoEncryptionKey, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Key_MetaData[] = {
		{ "Category", "Encryption" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000024001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCryptoEncryptionKey, Key), METADATA_PARAMS(Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Guid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::NewProp_Key,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CryptoKeys,
		nullptr,
		&NewStructOps,
		"CryptoEncryptionKey",
		sizeof(FCryptoEncryptionKey),
		alignof(FCryptoEncryptionKey),
		Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCryptoEncryptionKey()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCryptoEncryptionKey_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CryptoKeys();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CryptoEncryptionKey"), sizeof(FCryptoEncryptionKey), Get_Z_Construct_UScriptStruct_FCryptoEncryptionKey_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCryptoEncryptionKey_Hash() { return 2732348815U; }
	void UCryptoKeysSettings::StaticRegisterNativesUCryptoKeysSettings()
	{
	}
	UClass* Z_Construct_UClass_UCryptoKeysSettings_NoRegister()
	{
		return UCryptoKeysSettings::StaticClass();
	}
	struct Z_Construct_UClass_UCryptoKeysSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EncryptionKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_EncryptionKey;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SecondaryEncryptionKeys_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondaryEncryptionKeys_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SecondaryEncryptionKeys;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEncryptPakIniFiles_MetaData[];
#endif
		static void NewProp_bEncryptPakIniFiles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEncryptPakIniFiles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEncryptPakIndex_MetaData[];
#endif
		static void NewProp_bEncryptPakIndex_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEncryptPakIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEncryptUAssetFiles_MetaData[];
#endif
		static void NewProp_bEncryptUAssetFiles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEncryptUAssetFiles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEncryptAllAssetFiles_MetaData[];
#endif
		static void NewProp_bEncryptAllAssetFiles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEncryptAllAssetFiles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SigningPublicExponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SigningPublicExponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SigningModulus_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SigningModulus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SigningPrivateExponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SigningPrivateExponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnablePakSigning_MetaData[];
#endif
		static void NewProp_bEnablePakSigning_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnablePakSigning;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCryptoKeysSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CryptoKeys,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Implements the settings for imported Paper2D assets, such as sprite sheet textures.\n*/" },
		{ "IncludePath", "CryptoKeysSettings.h" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "Implements the settings for imported Paper2D assets, such as sprite sheet textures." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_EncryptionKey_MetaData[] = {
		{ "Category", "Encryption" },
		{ "Comment", "// The default encryption key used to protect pak files\n" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "The default encryption key used to protect pak files" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_EncryptionKey = { "EncryptionKey", nullptr, (EPropertyFlags)0x0010000000024001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCryptoKeysSettings, EncryptionKey), METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_EncryptionKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_EncryptionKey_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SecondaryEncryptionKeys_Inner = { "SecondaryEncryptionKeys", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCryptoEncryptionKey, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SecondaryEncryptionKeys_MetaData[] = {
		{ "Category", "Encryption" },
		{ "Comment", "// Secondary encryption keys that can be selected for use on different assets. Games are required to make these keys available to the pak platform file at runtime in order to access the data they protect.\n" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "Secondary encryption keys that can be selected for use on different assets. Games are required to make these keys available to the pak platform file at runtime in order to access the data they protect." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SecondaryEncryptionKeys = { "SecondaryEncryptionKeys", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCryptoKeysSettings, SecondaryEncryptionKeys), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SecondaryEncryptionKeys_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SecondaryEncryptionKeys_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIniFiles_MetaData[] = {
		{ "Category", "Encryption" },
		{ "Comment", "// Encrypts all ini files in the pak. Gives security to the most common sources of mineable information, with minimal runtime IO cost\n" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "Encrypts all ini files in the pak. Gives security to the most common sources of mineable information, with minimal runtime IO cost" },
	};
#endif
	void Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIniFiles_SetBit(void* Obj)
	{
		((UCryptoKeysSettings*)Obj)->bEncryptPakIniFiles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIniFiles = { "bEncryptPakIniFiles", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCryptoKeysSettings), &Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIniFiles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIniFiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIniFiles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIndex_MetaData[] = {
		{ "Category", "Encryption" },
		{ "Comment", "// Encrypt the pak index, making it impossible to use unrealpak to manipulate the pak file without the encryption key\n" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "Encrypt the pak index, making it impossible to use unrealpak to manipulate the pak file without the encryption key" },
	};
#endif
	void Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIndex_SetBit(void* Obj)
	{
		((UCryptoKeysSettings*)Obj)->bEncryptPakIndex = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIndex = { "bEncryptPakIndex", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCryptoKeysSettings), &Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIndex_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptUAssetFiles_MetaData[] = {
		{ "Category", "Encryption" },
		{ "Comment", "// Encrypts the uasset file in cooked data. Less runtime IO cost, and protection to package header information, including most string data, but still leaves the bulk of the data unencrypted. \n" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "Encrypts the uasset file in cooked data. Less runtime IO cost, and protection to package header information, including most string data, but still leaves the bulk of the data unencrypted." },
	};
#endif
	void Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptUAssetFiles_SetBit(void* Obj)
	{
		((UCryptoKeysSettings*)Obj)->bEncryptUAssetFiles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptUAssetFiles = { "bEncryptUAssetFiles", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCryptoKeysSettings), &Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptUAssetFiles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptUAssetFiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptUAssetFiles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptAllAssetFiles_MetaData[] = {
		{ "Category", "Encryption" },
		{ "Comment", "// Encrypt all files in the pak file. Secure, but will cause some slowdown to runtime IO performance, and high entropy to packaged data which will be bad for patching\n" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "Encrypt all files in the pak file. Secure, but will cause some slowdown to runtime IO performance, and high entropy to packaged data which will be bad for patching" },
	};
#endif
	void Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptAllAssetFiles_SetBit(void* Obj)
	{
		((UCryptoKeysSettings*)Obj)->bEncryptAllAssetFiles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptAllAssetFiles = { "bEncryptAllAssetFiles", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCryptoKeysSettings), &Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptAllAssetFiles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptAllAssetFiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptAllAssetFiles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningPublicExponent_MetaData[] = {
		{ "Category", "Signing" },
		{ "Comment", "// The RSA key public exponent used for signing a pak file\n" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "The RSA key public exponent used for signing a pak file" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningPublicExponent = { "SigningPublicExponent", nullptr, (EPropertyFlags)0x0010000000024001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCryptoKeysSettings, SigningPublicExponent), METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningPublicExponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningPublicExponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningModulus_MetaData[] = {
		{ "Category", "Signing" },
		{ "Comment", "// The RSA key modulus used for signing a pak file\n" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "The RSA key modulus used for signing a pak file" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningModulus = { "SigningModulus", nullptr, (EPropertyFlags)0x0010000000024001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCryptoKeysSettings, SigningModulus), METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningModulus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningModulus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningPrivateExponent_MetaData[] = {
		{ "Category", "Signing" },
		{ "Comment", "// The RSA key private exponent used for signing a pak file\n" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "The RSA key private exponent used for signing a pak file" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningPrivateExponent = { "SigningPrivateExponent", nullptr, (EPropertyFlags)0x0010000000024001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCryptoKeysSettings, SigningPrivateExponent), METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningPrivateExponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningPrivateExponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEnablePakSigning_MetaData[] = {
		{ "Category", "Signing" },
		{ "Comment", "// Enable signing of pak files, to prevent tampering of the data\n" },
		{ "ModuleRelativePath", "Classes/CryptoKeysSettings.h" },
		{ "ToolTip", "Enable signing of pak files, to prevent tampering of the data" },
	};
#endif
	void Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEnablePakSigning_SetBit(void* Obj)
	{
		((UCryptoKeysSettings*)Obj)->bEnablePakSigning = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEnablePakSigning = { "bEnablePakSigning", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCryptoKeysSettings), &Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEnablePakSigning_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEnablePakSigning_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEnablePakSigning_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCryptoKeysSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_EncryptionKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SecondaryEncryptionKeys_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SecondaryEncryptionKeys,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIniFiles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptPakIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptUAssetFiles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEncryptAllAssetFiles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningPublicExponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningModulus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_SigningPrivateExponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCryptoKeysSettings_Statics::NewProp_bEnablePakSigning,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCryptoKeysSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCryptoKeysSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCryptoKeysSettings_Statics::ClassParams = {
		&UCryptoKeysSettings::StaticClass,
		"Crypto",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCryptoKeysSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UCryptoKeysSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCryptoKeysSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCryptoKeysSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCryptoKeysSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCryptoKeysSettings, 1342663555);
	template<> CRYPTOKEYS_API UClass* StaticClass<UCryptoKeysSettings>()
	{
		return UCryptoKeysSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCryptoKeysSettings(Z_Construct_UClass_UCryptoKeysSettings, &UCryptoKeysSettings::StaticClass, TEXT("/Script/CryptoKeys"), TEXT("UCryptoKeysSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCryptoKeysSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
