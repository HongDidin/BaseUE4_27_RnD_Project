// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CRYPTOKEYS_CryptoKeysSettings_generated_h
#error "CryptoKeysSettings.generated.h already included, missing '#pragma once' in CryptoKeysSettings.h"
#endif
#define CRYPTOKEYS_CryptoKeysSettings_generated_h

#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCryptoEncryptionKey_Statics; \
	CRYPTOKEYS_API static class UScriptStruct* StaticStruct();


template<> CRYPTOKEYS_API UScriptStruct* StaticStruct<struct FCryptoEncryptionKey>();

#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_SPARSE_DATA
#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_RPC_WRAPPERS
#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCryptoKeysSettings(); \
	friend struct Z_Construct_UClass_UCryptoKeysSettings_Statics; \
public: \
	DECLARE_CLASS(UCryptoKeysSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CryptoKeys"), NO_API) \
	DECLARE_SERIALIZER(UCryptoKeysSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Crypto");} \



#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_INCLASS \
private: \
	static void StaticRegisterNativesUCryptoKeysSettings(); \
	friend struct Z_Construct_UClass_UCryptoKeysSettings_Statics; \
public: \
	DECLARE_CLASS(UCryptoKeysSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CryptoKeys"), NO_API) \
	DECLARE_SERIALIZER(UCryptoKeysSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Crypto");} \



#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCryptoKeysSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCryptoKeysSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCryptoKeysSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCryptoKeysSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCryptoKeysSettings(UCryptoKeysSettings&&); \
	NO_API UCryptoKeysSettings(const UCryptoKeysSettings&); \
public:


#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCryptoKeysSettings(UCryptoKeysSettings&&); \
	NO_API UCryptoKeysSettings(const UCryptoKeysSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCryptoKeysSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCryptoKeysSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCryptoKeysSettings)


#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_32_PROLOG
#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_SPARSE_DATA \
	Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_RPC_WRAPPERS \
	Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_INCLASS \
	Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_SPARSE_DATA \
	Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CRYPTOKEYS_API UClass* StaticClass<class UCryptoKeysSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_CryptoKeys_Source_CryptoKeys_Classes_CryptoKeysSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
