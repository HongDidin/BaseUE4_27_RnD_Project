// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSETSEARCH_SearchUserSettings_generated_h
#error "SearchUserSettings.generated.h already included, missing '#pragma once' in SearchUserSettings.h"
#endif
#define ASSETSEARCH_SearchUserSettings_generated_h

#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSearchPerformance_Statics; \
	ASSETSEARCH_API static class UScriptStruct* StaticStruct();


template<> ASSETSEARCH_API UScriptStruct* StaticStruct<struct FSearchPerformance>();

#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_SPARSE_DATA
#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_RPC_WRAPPERS
#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSearchUserSettings(); \
	friend struct Z_Construct_UClass_USearchUserSettings_Statics; \
public: \
	DECLARE_CLASS(USearchUserSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AssetSearch"), NO_API) \
	DECLARE_SERIALIZER(USearchUserSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUSearchUserSettings(); \
	friend struct Z_Construct_UClass_USearchUserSettings_Statics; \
public: \
	DECLARE_CLASS(USearchUserSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AssetSearch"), NO_API) \
	DECLARE_SERIALIZER(USearchUserSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USearchUserSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USearchUserSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USearchUserSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USearchUserSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USearchUserSettings(USearchUserSettings&&); \
	NO_API USearchUserSettings(const USearchUserSettings&); \
public:


#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USearchUserSettings(USearchUserSettings&&); \
	NO_API USearchUserSettings(const USearchUserSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USearchUserSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USearchUserSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USearchUserSettings)


#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_25_PROLOG
#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_SPARSE_DATA \
	Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_RPC_WRAPPERS \
	Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_INCLASS \
	Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_SPARSE_DATA \
	Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSETSEARCH_API UClass* StaticClass<class USearchUserSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_AssetSearch_Source_Private_Settings_SearchUserSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
