// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/Settings/SearchUserSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSearchUserSettings() {}
// Cross Module References
	ASSETSEARCH_API UScriptStruct* Z_Construct_UScriptStruct_FSearchPerformance();
	UPackage* Z_Construct_UPackage__Script_AssetSearch();
	ASSETSEARCH_API UClass* Z_Construct_UClass_USearchUserSettings_NoRegister();
	ASSETSEARCH_API UClass* Z_Construct_UClass_USearchUserSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
class UScriptStruct* FSearchPerformance::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ASSETSEARCH_API uint32 Get_Z_Construct_UScriptStruct_FSearchPerformance_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSearchPerformance, Z_Construct_UPackage__Script_AssetSearch(), TEXT("SearchPerformance"), sizeof(FSearchPerformance), Get_Z_Construct_UScriptStruct_FSearchPerformance_Hash());
	}
	return Singleton;
}
template<> ASSETSEARCH_API UScriptStruct* StaticStruct<FSearchPerformance>()
{
	return FSearchPerformance::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSearchPerformance(FSearchPerformance::StaticStruct, TEXT("/Script/AssetSearch"), TEXT("SearchPerformance"), false, nullptr, nullptr);
static struct FScriptStruct_AssetSearch_StaticRegisterNativesFSearchPerformance
{
	FScriptStruct_AssetSearch_StaticRegisterNativesFSearchPerformance()
	{
		UScriptStruct::DeferCppStructOps<FSearchPerformance>(FName(TEXT("SearchPerformance")));
	}
} ScriptStruct_AssetSearch_StaticRegisterNativesFSearchPerformance;
	struct Z_Construct_UScriptStruct_FSearchPerformance_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParallelDownloads_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ParallelDownloads;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DownloadProcessRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DownloadProcessRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetScanRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AssetScanRate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchPerformance_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSearchPerformance>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_ParallelDownloads_MetaData[] = {
		{ "Category", "Performance" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_ParallelDownloads = { "ParallelDownloads", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchPerformance, ParallelDownloads), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_ParallelDownloads_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_ParallelDownloads_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_DownloadProcessRate_MetaData[] = {
		{ "Category", "Performance" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_DownloadProcessRate = { "DownloadProcessRate", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchPerformance, DownloadProcessRate), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_DownloadProcessRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_DownloadProcessRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_AssetScanRate_MetaData[] = {
		{ "Category", "Performance" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_AssetScanRate = { "AssetScanRate", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchPerformance, AssetScanRate), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_AssetScanRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_AssetScanRate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSearchPerformance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_ParallelDownloads,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_DownloadProcessRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchPerformance_Statics::NewProp_AssetScanRate,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSearchPerformance_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AssetSearch,
		nullptr,
		&NewStructOps,
		"SearchPerformance",
		sizeof(FSearchPerformance),
		alignof(FSearchPerformance),
		Z_Construct_UScriptStruct_FSearchPerformance_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchPerformance_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchPerformance_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchPerformance_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSearchPerformance()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSearchPerformance_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AssetSearch();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SearchPerformance"), sizeof(FSearchPerformance), Get_Z_Construct_UScriptStruct_FSearchPerformance_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSearchPerformance_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSearchPerformance_Hash() { return 4034455556U; }
	void USearchUserSettings::StaticRegisterNativesUSearchUserSettings()
	{
	}
	UClass* Z_Construct_UClass_USearchUserSettings_NoRegister()
	{
		return USearchUserSettings::StaticClass();
	}
	struct Z_Construct_UClass_USearchUserSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableSearch_MetaData[];
#endif
		static void NewProp_bEnableSearch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableSearch;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IgnoredPaths_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IgnoredPaths_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IgnoredPaths;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowMissingAssets_MetaData[];
#endif
		static void NewProp_bShowMissingAssets_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowMissingAssets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoExpandAssets_MetaData[];
#endif
		static void NewProp_bAutoExpandAssets_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoExpandAssets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bThrottleInBackground_MetaData[];
#endif
		static void NewProp_bThrottleInBackground_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bThrottleInBackground;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BackgroundtOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BackgroundtOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SearchInForeground_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SearchInForeground;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USearchUserSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_AssetSearch,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USearchUserSettings_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Search" },
		{ "IncludePath", "Settings/SearchUserSettings.h" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bEnableSearch_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Enable this to begin using search. */" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
		{ "ToolTip", "Enable this to begin using search." },
	};
#endif
	void Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bEnableSearch_SetBit(void* Obj)
	{
		((USearchUserSettings*)Obj)->bEnableSearch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bEnableSearch = { "bEnableSearch", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USearchUserSettings), &Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bEnableSearch_SetBit, METADATA_PARAMS(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bEnableSearch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bEnableSearch_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USearchUserSettings_Statics::NewProp_IgnoredPaths_Inner = { "IgnoredPaths", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USearchUserSettings_Statics::NewProp_IgnoredPaths_MetaData[] = {
		{ "Category", "General" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USearchUserSettings_Statics::NewProp_IgnoredPaths = { "IgnoredPaths", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USearchUserSettings, IgnoredPaths), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_IgnoredPaths_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_IgnoredPaths_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bShowMissingAssets_MetaData[] = {
		{ "Category", "General" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	void Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bShowMissingAssets_SetBit(void* Obj)
	{
		((USearchUserSettings*)Obj)->bShowMissingAssets = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bShowMissingAssets = { "bShowMissingAssets", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USearchUserSettings), &Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bShowMissingAssets_SetBit, METADATA_PARAMS(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bShowMissingAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bShowMissingAssets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bAutoExpandAssets_MetaData[] = {
		{ "Category", "General" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	void Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bAutoExpandAssets_SetBit(void* Obj)
	{
		((USearchUserSettings*)Obj)->bAutoExpandAssets = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bAutoExpandAssets = { "bAutoExpandAssets", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USearchUserSettings), &Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bAutoExpandAssets_SetBit, METADATA_PARAMS(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bAutoExpandAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bAutoExpandAssets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bThrottleInBackground_MetaData[] = {
		{ "Category", "Performance" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	void Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bThrottleInBackground_SetBit(void* Obj)
	{
		((USearchUserSettings*)Obj)->bThrottleInBackground = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bThrottleInBackground = { "bThrottleInBackground", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USearchUserSettings), &Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bThrottleInBackground_SetBit, METADATA_PARAMS(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bThrottleInBackground_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bThrottleInBackground_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USearchUserSettings_Statics::NewProp_DefaultOptions_MetaData[] = {
		{ "Category", "Performance" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USearchUserSettings_Statics::NewProp_DefaultOptions = { "DefaultOptions", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USearchUserSettings, DefaultOptions), Z_Construct_UScriptStruct_FSearchPerformance, METADATA_PARAMS(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_DefaultOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_DefaultOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USearchUserSettings_Statics::NewProp_BackgroundtOptions_MetaData[] = {
		{ "Category", "Performance" },
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USearchUserSettings_Statics::NewProp_BackgroundtOptions = { "BackgroundtOptions", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USearchUserSettings, BackgroundtOptions), Z_Construct_UScriptStruct_FSearchPerformance, METADATA_PARAMS(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_BackgroundtOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_BackgroundtOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USearchUserSettings_Statics::NewProp_SearchInForeground_MetaData[] = {
		{ "ModuleRelativePath", "Private/Settings/SearchUserSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_USearchUserSettings_Statics::NewProp_SearchInForeground = { "SearchInForeground", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USearchUserSettings, SearchInForeground), METADATA_PARAMS(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_SearchInForeground_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USearchUserSettings_Statics::NewProp_SearchInForeground_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USearchUserSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bEnableSearch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USearchUserSettings_Statics::NewProp_IgnoredPaths_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USearchUserSettings_Statics::NewProp_IgnoredPaths,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bShowMissingAssets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bAutoExpandAssets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USearchUserSettings_Statics::NewProp_bThrottleInBackground,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USearchUserSettings_Statics::NewProp_DefaultOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USearchUserSettings_Statics::NewProp_BackgroundtOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USearchUserSettings_Statics::NewProp_SearchInForeground,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USearchUserSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USearchUserSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USearchUserSettings_Statics::ClassParams = {
		&USearchUserSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USearchUserSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USearchUserSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_USearchUserSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USearchUserSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USearchUserSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USearchUserSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USearchUserSettings, 3428993967);
	template<> ASSETSEARCH_API UClass* StaticClass<USearchUserSettings>()
	{
		return USearchUserSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USearchUserSettings(Z_Construct_UClass_USearchUserSettings, &USearchUserSettings::StaticClass, TEXT("/Script/AssetSearch"), TEXT("USearchUserSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USearchUserSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
