// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataValidation/Public/EditorValidatorBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditorValidatorBase() {}
// Cross Module References
	DATAVALIDATION_API UClass* Z_Construct_UClass_UEditorValidatorBase_NoRegister();
	DATAVALIDATION_API UClass* Z_Construct_UClass_UEditorValidatorBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DataValidation();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EDataValidationResult();
// End Cross Module References
	DEFINE_FUNCTION(UEditorValidatorBase::execGetValidationResult)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EDataValidationResult*)Z_Param__Result=P_THIS->GetValidationResult();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorValidatorBase::execAssetWarning)
	{
		P_GET_OBJECT(UObject,Z_Param_InAsset);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_InMessage);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AssetWarning(Z_Param_InAsset,Z_Param_Out_InMessage);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorValidatorBase::execAssetPasses)
	{
		P_GET_OBJECT(UObject,Z_Param_InAsset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AssetPasses(Z_Param_InAsset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorValidatorBase::execAssetFails)
	{
		P_GET_OBJECT(UObject,Z_Param_InAsset);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_InMessage);
		P_GET_TARRAY_REF(FText,Z_Param_Out_ValidationErrors);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AssetFails(Z_Param_InAsset,Z_Param_Out_InMessage,Z_Param_Out_ValidationErrors);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorValidatorBase::execValidateLoadedAsset)
	{
		P_GET_OBJECT(UObject,Z_Param_InAsset);
		P_GET_TARRAY_REF(FText,Z_Param_Out_ValidationErrors);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EDataValidationResult*)Z_Param__Result=P_THIS->ValidateLoadedAsset_Implementation(Z_Param_InAsset,Z_Param_Out_ValidationErrors);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorValidatorBase::execCanValidateAsset)
	{
		P_GET_OBJECT(UObject,Z_Param_InAsset);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CanValidateAsset_Implementation(Z_Param_InAsset);
		P_NATIVE_END;
	}
	static FName NAME_UEditorValidatorBase_CanValidateAsset = FName(TEXT("CanValidateAsset"));
	bool UEditorValidatorBase::CanValidateAsset(UObject* InAsset) const
	{
		EditorValidatorBase_eventCanValidateAsset_Parms Parms;
		Parms.InAsset=InAsset;
		const_cast<UEditorValidatorBase*>(this)->ProcessEvent(FindFunctionChecked(NAME_UEditorValidatorBase_CanValidateAsset),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_UEditorValidatorBase_ValidateLoadedAsset = FName(TEXT("ValidateLoadedAsset"));
	EDataValidationResult UEditorValidatorBase::ValidateLoadedAsset(UObject* InAsset, TArray<FText>& ValidationErrors)
	{
		EditorValidatorBase_eventValidateLoadedAsset_Parms Parms;
		Parms.InAsset=InAsset;
		Parms.ValidationErrors=ValidationErrors;
		ProcessEvent(FindFunctionChecked(NAME_UEditorValidatorBase_ValidateLoadedAsset),&Parms);
		ValidationErrors=Parms.ValidationErrors;
		return Parms.ReturnValue;
	}
	void UEditorValidatorBase::StaticRegisterNativesUEditorValidatorBase()
	{
		UClass* Class = UEditorValidatorBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AssetFails", &UEditorValidatorBase::execAssetFails },
			{ "AssetPasses", &UEditorValidatorBase::execAssetPasses },
			{ "AssetWarning", &UEditorValidatorBase::execAssetWarning },
			{ "CanValidateAsset", &UEditorValidatorBase::execCanValidateAsset },
			{ "GetValidationResult", &UEditorValidatorBase::execGetValidationResult },
			{ "ValidateLoadedAsset", &UEditorValidatorBase::execValidateLoadedAsset },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics
	{
		struct EditorValidatorBase_eventAssetFails_Parms
		{
			UObject* InAsset;
			FText InMessage;
			TArray<FText> ValidationErrors;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_InMessage;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ValidationErrors_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ValidationErrors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_InAsset = { "InAsset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventAssetFails_Parms, InAsset), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_InMessage_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_InMessage = { "InMessage", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventAssetFails_Parms, InMessage), METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_InMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_InMessage_MetaData)) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_ValidationErrors_Inner = { "ValidationErrors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_ValidationErrors = { "ValidationErrors", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventAssetFails_Parms, ValidationErrors), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_InAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_InMessage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_ValidationErrors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::NewProp_ValidationErrors,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::Function_MetaDataParams[] = {
		{ "Category", "Asset Validation" },
		{ "ModuleRelativePath", "Public/EditorValidatorBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorValidatorBase, nullptr, "AssetFails", nullptr, nullptr, sizeof(EditorValidatorBase_eventAssetFails_Parms), Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorValidatorBase_AssetFails()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorValidatorBase_AssetFails_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics
	{
		struct EditorValidatorBase_eventAssetPasses_Parms
		{
			UObject* InAsset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics::NewProp_InAsset = { "InAsset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventAssetPasses_Parms, InAsset), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics::NewProp_InAsset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics::Function_MetaDataParams[] = {
		{ "Category", "Asset Validation" },
		{ "ModuleRelativePath", "Public/EditorValidatorBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorValidatorBase, nullptr, "AssetPasses", nullptr, nullptr, sizeof(EditorValidatorBase_eventAssetPasses_Parms), Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorValidatorBase_AssetPasses()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorValidatorBase_AssetPasses_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics
	{
		struct EditorValidatorBase_eventAssetWarning_Parms
		{
			UObject* InAsset;
			FText InMessage;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_InMessage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::NewProp_InAsset = { "InAsset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventAssetWarning_Parms, InAsset), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::NewProp_InMessage_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::NewProp_InMessage = { "InMessage", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventAssetWarning_Parms, InMessage), METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::NewProp_InMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::NewProp_InMessage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::NewProp_InAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::NewProp_InMessage,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::Function_MetaDataParams[] = {
		{ "Category", "Asset Validation" },
		{ "ModuleRelativePath", "Public/EditorValidatorBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorValidatorBase, nullptr, "AssetWarning", nullptr, nullptr, sizeof(EditorValidatorBase_eventAssetWarning_Parms), Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorValidatorBase_AssetWarning()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorValidatorBase_AssetWarning_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InAsset;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::NewProp_InAsset = { "InAsset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventCanValidateAsset_Parms, InAsset), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorValidatorBase_eventCanValidateAsset_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorValidatorBase_eventCanValidateAsset_Parms), &Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::NewProp_InAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Asset Validation" },
		{ "Comment", "/** Override this to determine whether or not you can validate a given asset with this validator */" },
		{ "ModuleRelativePath", "Public/EditorValidatorBase.h" },
		{ "ToolTip", "Override this to determine whether or not you can validate a given asset with this validator" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorValidatorBase, nullptr, "CanValidateAsset", nullptr, nullptr, sizeof(EditorValidatorBase_eventCanValidateAsset_Parms), Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics
	{
		struct EditorValidatorBase_eventGetValidationResult_Parms
		{
			EDataValidationResult ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventGetValidationResult_Parms, ReturnValue), Z_Construct_UEnum_CoreUObject_EDataValidationResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::Function_MetaDataParams[] = {
		{ "Category", "Asset Validation" },
		{ "ModuleRelativePath", "Public/EditorValidatorBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorValidatorBase, nullptr, "GetValidationResult", nullptr, nullptr, sizeof(EditorValidatorBase_eventGetValidationResult_Parms), Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InAsset;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ValidationErrors_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ValidationErrors;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::NewProp_InAsset = { "InAsset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventValidateLoadedAsset_Parms, InAsset), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::NewProp_ValidationErrors_Inner = { "ValidationErrors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::NewProp_ValidationErrors = { "ValidationErrors", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventValidateLoadedAsset_Parms, ValidationErrors), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorBase_eventValidateLoadedAsset_Parms, ReturnValue), Z_Construct_UEnum_CoreUObject_EDataValidationResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::NewProp_InAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::NewProp_ValidationErrors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::NewProp_ValidationErrors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Asset Validation" },
		{ "ModuleRelativePath", "Public/EditorValidatorBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorValidatorBase, nullptr, "ValidateLoadedAsset", nullptr, nullptr, sizeof(EditorValidatorBase_eventValidateLoadedAsset_Parms), Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditorValidatorBase_NoRegister()
	{
		return UEditorValidatorBase::StaticClass();
	}
	struct Z_Construct_UClass_UEditorValidatorBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditorValidatorBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataValidation,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditorValidatorBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditorValidatorBase_AssetFails, "AssetFails" }, // 1826028954
		{ &Z_Construct_UFunction_UEditorValidatorBase_AssetPasses, "AssetPasses" }, // 3868921881
		{ &Z_Construct_UFunction_UEditorValidatorBase_AssetWarning, "AssetWarning" }, // 4074325030
		{ &Z_Construct_UFunction_UEditorValidatorBase_CanValidateAsset, "CanValidateAsset" }, // 2872113899
		{ &Z_Construct_UFunction_UEditorValidatorBase_GetValidationResult, "GetValidationResult" }, // 1270066321
		{ &Z_Construct_UFunction_UEditorValidatorBase_ValidateLoadedAsset, "ValidateLoadedAsset" }, // 1949836316
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorValidatorBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/*\n* The EditorValidatorBase is a class which verifies that an asset meets a specific ruleset.\n* It should be used when checking engine-level classes, as UObject::IsDataValid requires\n* overriding the base class. You can create project-specific version of the validator base,\n* with custom logging and enabled logic.\n*\n* C++ and Blueprint validators will be gathered on editor start, while python validators need\n* to register themselves\n*/" },
		{ "IncludePath", "EditorValidatorBase.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/EditorValidatorBase.h" },
		{ "ShowWorldContextPin", "" },
		{ "ToolTip", "* The EditorValidatorBase is a class which verifies that an asset meets a specific ruleset.\n* It should be used when checking engine-level classes, as UObject::IsDataValid requires\n* overriding the base class. You can create project-specific version of the validator base,\n* with custom logging and enabled logic.\n*\n* C++ and Blueprint validators will be gathered on editor start, while python validators need\n* to register themselves" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorValidatorBase_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Asset Validation" },
		{ "ModuleRelativePath", "Public/EditorValidatorBase.h" },
	};
#endif
	void Z_Construct_UClass_UEditorValidatorBase_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((UEditorValidatorBase*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEditorValidatorBase_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEditorValidatorBase), &Z_Construct_UClass_UEditorValidatorBase_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEditorValidatorBase_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidatorBase_Statics::NewProp_bIsEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditorValidatorBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorValidatorBase_Statics::NewProp_bIsEnabled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditorValidatorBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditorValidatorBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditorValidatorBase_Statics::ClassParams = {
		&UEditorValidatorBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UEditorValidatorBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidatorBase_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UEditorValidatorBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidatorBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditorValidatorBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditorValidatorBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditorValidatorBase, 4001231239);
	template<> DATAVALIDATION_API UClass* StaticClass<UEditorValidatorBase>()
	{
		return UEditorValidatorBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditorValidatorBase(Z_Construct_UClass_UEditorValidatorBase, &UEditorValidatorBase::StaticClass, TEXT("/Script/DataValidation"), TEXT("UEditorValidatorBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditorValidatorBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
