// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAVALIDATION_EditorValidator_Localization_generated_h
#error "EditorValidator_Localization.generated.h already included, missing '#pragma once' in EditorValidator_Localization.h"
#endif
#define DATAVALIDATION_EditorValidator_Localization_generated_h

#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_SPARSE_DATA
#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_RPC_WRAPPERS
#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditorValidator_Localization(); \
	friend struct Z_Construct_UClass_UEditorValidator_Localization_Statics; \
public: \
	DECLARE_CLASS(UEditorValidator_Localization, UEditorValidatorBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataValidation"), NO_API) \
	DECLARE_SERIALIZER(UEditorValidator_Localization)


#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUEditorValidator_Localization(); \
	friend struct Z_Construct_UClass_UEditorValidator_Localization_Statics; \
public: \
	DECLARE_CLASS(UEditorValidator_Localization, UEditorValidatorBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataValidation"), NO_API) \
	DECLARE_SERIALIZER(UEditorValidator_Localization)


#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditorValidator_Localization(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditorValidator_Localization) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditorValidator_Localization); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditorValidator_Localization); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditorValidator_Localization(UEditorValidator_Localization&&); \
	NO_API UEditorValidator_Localization(const UEditorValidator_Localization&); \
public:


#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditorValidator_Localization(UEditorValidator_Localization&&); \
	NO_API UEditorValidator_Localization(const UEditorValidator_Localization&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditorValidator_Localization); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditorValidator_Localization); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEditorValidator_Localization)


#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_12_PROLOG
#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_RPC_WRAPPERS \
	Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_INCLASS \
	Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAVALIDATION_API UClass* StaticClass<class UEditorValidator_Localization>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_DataValidation_Source_DataValidation_Public_EditorValidator_Localization_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
