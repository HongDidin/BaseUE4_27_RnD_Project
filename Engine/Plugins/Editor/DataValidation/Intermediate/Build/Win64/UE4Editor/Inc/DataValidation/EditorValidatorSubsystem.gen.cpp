// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataValidation/Public/EditorValidatorSubsystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditorValidatorSubsystem() {}
// Cross Module References
	DATAVALIDATION_API UClass* Z_Construct_UClass_UDataValidationSettings_NoRegister();
	DATAVALIDATION_API UClass* Z_Construct_UClass_UDataValidationSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DataValidation();
	DATAVALIDATION_API UClass* Z_Construct_UClass_UEditorValidatorSubsystem_NoRegister();
	DATAVALIDATION_API UClass* Z_Construct_UClass_UEditorValidatorSubsystem();
	EDITORSUBSYSTEM_API UClass* Z_Construct_UClass_UEditorSubsystem();
	DATAVALIDATION_API UClass* Z_Construct_UClass_UEditorValidatorBase_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FAssetData();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EDataValidationResult();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	void UDataValidationSettings::StaticRegisterNativesUDataValidationSettings()
	{
	}
	UClass* Z_Construct_UClass_UDataValidationSettings_NoRegister()
	{
		return UDataValidationSettings::StaticClass();
	}
	struct Z_Construct_UClass_UDataValidationSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bValidateOnSave_MetaData[];
#endif
		static void NewProp_bValidateOnSave_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bValidateOnSave;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataValidationSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataValidation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataValidationSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Implements the settings for Data Validation \n*/" },
		{ "IncludePath", "EditorValidatorSubsystem.h" },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "Implements the settings for Data Validation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataValidationSettings_Statics::NewProp_bValidateOnSave_MetaData[] = {
		{ "Category", "Validation Scenarios" },
		{ "Comment", "/** Whether or not to validate assets on save */" },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "Whether or not to validate assets on save" },
	};
#endif
	void Z_Construct_UClass_UDataValidationSettings_Statics::NewProp_bValidateOnSave_SetBit(void* Obj)
	{
		((UDataValidationSettings*)Obj)->bValidateOnSave = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataValidationSettings_Statics::NewProp_bValidateOnSave = { "bValidateOnSave", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(UDataValidationSettings), &Z_Construct_UClass_UDataValidationSettings_Statics::NewProp_bValidateOnSave_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataValidationSettings_Statics::NewProp_bValidateOnSave_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataValidationSettings_Statics::NewProp_bValidateOnSave_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataValidationSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataValidationSettings_Statics::NewProp_bValidateOnSave,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataValidationSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataValidationSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataValidationSettings_Statics::ClassParams = {
		&UDataValidationSettings::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataValidationSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataValidationSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDataValidationSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataValidationSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataValidationSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataValidationSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataValidationSettings, 3706267282);
	template<> DATAVALIDATION_API UClass* StaticClass<UDataValidationSettings>()
	{
		return UDataValidationSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataValidationSettings(Z_Construct_UClass_UDataValidationSettings, &UDataValidationSettings::StaticClass, TEXT("/Script/DataValidation"), TEXT("UDataValidationSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataValidationSettings);
	DEFINE_FUNCTION(UEditorValidatorSubsystem::execValidateAssets)
	{
		P_GET_TARRAY(FAssetData,Z_Param_AssetDataList);
		P_GET_UBOOL(Z_Param_bSkipExcludedDirectories);
		P_GET_UBOOL(Z_Param_bShowIfNoFailures);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->ValidateAssets(Z_Param_AssetDataList,Z_Param_bSkipExcludedDirectories,Z_Param_bShowIfNoFailures);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorValidatorSubsystem::execIsAssetValid)
	{
		P_GET_STRUCT_REF(FAssetData,Z_Param_Out_AssetData);
		P_GET_TARRAY_REF(FText,Z_Param_Out_ValidationErrors);
		P_GET_TARRAY_REF(FText,Z_Param_Out_ValidationWarnings);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EDataValidationResult*)Z_Param__Result=P_THIS->IsAssetValid(Z_Param_Out_AssetData,Z_Param_Out_ValidationErrors,Z_Param_Out_ValidationWarnings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorValidatorSubsystem::execIsObjectValid)
	{
		P_GET_OBJECT(UObject,Z_Param_InObject);
		P_GET_TARRAY_REF(FText,Z_Param_Out_ValidationErrors);
		P_GET_TARRAY_REF(FText,Z_Param_Out_ValidationWarnings);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EDataValidationResult*)Z_Param__Result=P_THIS->IsObjectValid(Z_Param_InObject,Z_Param_Out_ValidationErrors,Z_Param_Out_ValidationWarnings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorValidatorSubsystem::execAddValidator)
	{
		P_GET_OBJECT(UEditorValidatorBase,Z_Param_InValidator);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddValidator(Z_Param_InValidator);
		P_NATIVE_END;
	}
	void UEditorValidatorSubsystem::StaticRegisterNativesUEditorValidatorSubsystem()
	{
		UClass* Class = UEditorValidatorSubsystem::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddValidator", &UEditorValidatorSubsystem::execAddValidator },
			{ "IsAssetValid", &UEditorValidatorSubsystem::execIsAssetValid },
			{ "IsObjectValid", &UEditorValidatorSubsystem::execIsObjectValid },
			{ "ValidateAssets", &UEditorValidatorSubsystem::execValidateAssets },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics
	{
		struct EditorValidatorSubsystem_eventAddValidator_Parms
		{
			UEditorValidatorBase* InValidator;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InValidator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics::NewProp_InValidator = { "InValidator", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventAddValidator_Parms, InValidator), Z_Construct_UClass_UEditorValidatorBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics::NewProp_InValidator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics::Function_MetaDataParams[] = {
		{ "Category", "Validation" },
		{ "Comment", "/*\n\x09* Adds a validator to the list, making sure it is a unique instance\n\x09*/" },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "* Adds a validator to the list, making sure it is a unique instance" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorValidatorSubsystem, nullptr, "AddValidator", nullptr, nullptr, sizeof(EditorValidatorSubsystem_eventAddValidator_Parms), Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics
	{
		struct EditorValidatorSubsystem_eventIsAssetValid_Parms
		{
			FAssetData AssetData;
			TArray<FText> ValidationErrors;
			TArray<FText> ValidationWarnings;
			EDataValidationResult ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AssetData;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ValidationErrors_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ValidationErrors;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ValidationWarnings_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ValidationWarnings;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_AssetData = { "AssetData", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventIsAssetValid_Parms, AssetData), Z_Construct_UScriptStruct_FAssetData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ValidationErrors_Inner = { "ValidationErrors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ValidationErrors = { "ValidationErrors", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventIsAssetValid_Parms, ValidationErrors), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ValidationWarnings_Inner = { "ValidationWarnings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ValidationWarnings = { "ValidationWarnings", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventIsAssetValid_Parms, ValidationWarnings), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventIsAssetValid_Parms, ReturnValue), Z_Construct_UEnum_CoreUObject_EDataValidationResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_AssetData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ValidationErrors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ValidationErrors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ValidationWarnings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ValidationWarnings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::Function_MetaDataParams[] = {
		{ "Category", "Asset Validation" },
		{ "Comment", "/**\n\x09 * @return Returns Valid if the object pointed to by AssetData contains valid data; returns Invalid if the object contains invalid data or does not exist; returns NotValidated if no validations was performed on the object\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "@return Returns Valid if the object pointed to by AssetData contains valid data; returns Invalid if the object contains invalid data or does not exist; returns NotValidated if no validations was performed on the object" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorValidatorSubsystem, nullptr, "IsAssetValid", nullptr, nullptr, sizeof(EditorValidatorSubsystem_eventIsAssetValid_Parms), Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics
	{
		struct EditorValidatorSubsystem_eventIsObjectValid_Parms
		{
			UObject* InObject;
			TArray<FText> ValidationErrors;
			TArray<FText> ValidationWarnings;
			EDataValidationResult ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InObject;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ValidationErrors_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ValidationErrors;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ValidationWarnings_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ValidationWarnings;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_InObject = { "InObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventIsObjectValid_Parms, InObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ValidationErrors_Inner = { "ValidationErrors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ValidationErrors = { "ValidationErrors", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventIsObjectValid_Parms, ValidationErrors), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ValidationWarnings_Inner = { "ValidationWarnings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ValidationWarnings = { "ValidationWarnings", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventIsObjectValid_Parms, ValidationWarnings), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventIsObjectValid_Parms, ReturnValue), Z_Construct_UEnum_CoreUObject_EDataValidationResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_InObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ValidationErrors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ValidationErrors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ValidationWarnings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ValidationWarnings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::Function_MetaDataParams[] = {
		{ "Category", "Asset Validation" },
		{ "Comment", "/**\n\x09 * @return Returns Valid if the object contains valid data; returns Invalid if the object contains invalid data; returns NotValidated if no validations was performed on the object\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "@return Returns Valid if the object contains valid data; returns Invalid if the object contains invalid data; returns NotValidated if no validations was performed on the object" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorValidatorSubsystem, nullptr, "IsObjectValid", nullptr, nullptr, sizeof(EditorValidatorSubsystem_eventIsObjectValid_Parms), Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics
	{
		struct EditorValidatorSubsystem_eventValidateAssets_Parms
		{
			TArray<FAssetData> AssetDataList;
			bool bSkipExcludedDirectories;
			bool bShowIfNoFailures;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AssetDataList_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AssetDataList;
		static void NewProp_bSkipExcludedDirectories_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSkipExcludedDirectories;
		static void NewProp_bShowIfNoFailures_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowIfNoFailures;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_AssetDataList_Inner = { "AssetDataList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAssetData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_AssetDataList = { "AssetDataList", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventValidateAssets_Parms, AssetDataList), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_bSkipExcludedDirectories_SetBit(void* Obj)
	{
		((EditorValidatorSubsystem_eventValidateAssets_Parms*)Obj)->bSkipExcludedDirectories = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_bSkipExcludedDirectories = { "bSkipExcludedDirectories", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorValidatorSubsystem_eventValidateAssets_Parms), &Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_bSkipExcludedDirectories_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_bShowIfNoFailures_SetBit(void* Obj)
	{
		((EditorValidatorSubsystem_eventValidateAssets_Parms*)Obj)->bShowIfNoFailures = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_bShowIfNoFailures = { "bShowIfNoFailures", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorValidatorSubsystem_eventValidateAssets_Parms), &Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_bShowIfNoFailures_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorValidatorSubsystem_eventValidateAssets_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_AssetDataList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_AssetDataList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_bSkipExcludedDirectories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_bShowIfNoFailures,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::Function_MetaDataParams[] = {
		{ "Category", "Asset Validation" },
		{ "Comment", "/**\n\x09 * Called to validate assets from either the UI or a commandlet\n\x09 * @param bSkipExcludedDirectories If true, will not validate files in excluded directories\n\x09 * @param bShowIfNoFailures If true, will add notifications for files with no validation and display even if everything passes\n\x09 * @returns Number of assets with validation failures or warnings\n\x09 */" },
		{ "CPP_Default_bShowIfNoFailures", "true" },
		{ "CPP_Default_bSkipExcludedDirectories", "true" },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "Called to validate assets from either the UI or a commandlet\n@param bSkipExcludedDirectories If true, will not validate files in excluded directories\n@param bShowIfNoFailures If true, will add notifications for files with no validation and display even if everything passes\n@returns Number of assets with validation failures or warnings" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorValidatorSubsystem, nullptr, "ValidateAssets", nullptr, nullptr, sizeof(EditorValidatorSubsystem_eventValidateAssets_Parms), Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditorValidatorSubsystem_NoRegister()
	{
		return UEditorValidatorSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UEditorValidatorSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExcludedDirectories_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExcludedDirectories_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExcludedDirectories;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bValidateOnSave_MetaData[];
#endif
		static void NewProp_bValidateOnSave_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bValidateOnSave;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Validators_ValueProp;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Validators_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Validators_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Validators;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bValidateAssetsWhileSavingForCook_MetaData[];
#endif
		static void NewProp_bValidateAssetsWhileSavingForCook_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bValidateAssetsWhileSavingForCook;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowBlueprintValidators_MetaData[];
#endif
		static void NewProp_bAllowBlueprintValidators_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowBlueprintValidators;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditorValidatorSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditorSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_DataValidation,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditorValidatorSubsystem_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditorValidatorSubsystem_AddValidator, "AddValidator" }, // 2859293657
		{ &Z_Construct_UFunction_UEditorValidatorSubsystem_IsAssetValid, "IsAssetValid" }, // 3959502746
		{ &Z_Construct_UFunction_UEditorValidatorSubsystem_IsObjectValid, "IsObjectValid" }, // 3437817789
		{ &Z_Construct_UFunction_UEditorValidatorSubsystem_ValidateAssets, "ValidateAssets" }, // 1872348734
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorValidatorSubsystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UEditorValidatorSubsystem manages all the asset validation in the engine. \n * The first validation handled is UObject::IsDataValid and its overridden functions.\n * Those validations require custom classes and are most suited to project-specific\n * classes. The next validation set is of all registered UEditorValidationBases.\n * These validators have a function to determine if they can validate a given asset,\n * and if they are currently enabled. They are good candidates for validating engine\n * classes or very specific project logic.\n */" },
		{ "IncludePath", "EditorValidatorSubsystem.h" },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "UEditorValidatorSubsystem manages all the asset validation in the engine.\nThe first validation handled is UObject::IsDataValid and its overridden functions.\nThose validations require custom classes and are most suited to project-specific\nclasses. The next validation set is of all registered UEditorValidationBases.\nThese validators have a function to determine if they can validate a given asset,\nand if they are currently enabled. They are good candidates for validating engine\nclasses or very specific project logic." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_ExcludedDirectories_Inner = { "ExcludedDirectories", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_ExcludedDirectories_MetaData[] = {
		{ "Comment", "/**\n\x09 * Directories to ignore for data validation. Useful for test assets\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "Directories to ignore for data validation. Useful for test assets" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_ExcludedDirectories = { "ExcludedDirectories", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditorValidatorSubsystem, ExcludedDirectories), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_ExcludedDirectories_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_ExcludedDirectories_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateOnSave_MetaData[] = {
		{ "Comment", "/**\n\x09 * Whether it should validate assets on save inside the editor\n\x09 */" },
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Use bValidateOnSave on UDataValidationSettings instead." },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "Whether it should validate assets on save inside the editor" },
	};
#endif
	void Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateOnSave_SetBit(void* Obj)
	{
		((UEditorValidatorSubsystem*)Obj)->bValidateOnSave = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateOnSave = { "bValidateOnSave", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEditorValidatorSubsystem), &Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateOnSave_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateOnSave_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateOnSave_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_Validators_ValueProp = { "Validators", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UEditorValidatorBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_Validators_Key_KeyProp = { "Validators_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_Validators_MetaData[] = {
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_Validators = { "Validators", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditorValidatorSubsystem, Validators), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_Validators_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_Validators_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateAssetsWhileSavingForCook_MetaData[] = {
		{ "Comment", "/** Specifies whether or not to validate assets on save when saving for a cook */" },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "Specifies whether or not to validate assets on save when saving for a cook" },
	};
#endif
	void Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateAssetsWhileSavingForCook_SetBit(void* Obj)
	{
		((UEditorValidatorSubsystem*)Obj)->bValidateAssetsWhileSavingForCook = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateAssetsWhileSavingForCook = { "bValidateAssetsWhileSavingForCook", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEditorValidatorSubsystem), &Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateAssetsWhileSavingForCook_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateAssetsWhileSavingForCook_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateAssetsWhileSavingForCook_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bAllowBlueprintValidators_MetaData[] = {
		{ "Comment", "/** Specifies whether or not to allow Blueprint validators */" },
		{ "ModuleRelativePath", "Public/EditorValidatorSubsystem.h" },
		{ "ToolTip", "Specifies whether or not to allow Blueprint validators" },
	};
#endif
	void Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bAllowBlueprintValidators_SetBit(void* Obj)
	{
		((UEditorValidatorSubsystem*)Obj)->bAllowBlueprintValidators = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bAllowBlueprintValidators = { "bAllowBlueprintValidators", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEditorValidatorSubsystem), &Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bAllowBlueprintValidators_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bAllowBlueprintValidators_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bAllowBlueprintValidators_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditorValidatorSubsystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_ExcludedDirectories_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_ExcludedDirectories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateOnSave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_Validators_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_Validators_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_Validators,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bValidateAssetsWhileSavingForCook,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorValidatorSubsystem_Statics::NewProp_bAllowBlueprintValidators,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditorValidatorSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditorValidatorSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditorValidatorSubsystem_Statics::ClassParams = {
		&UEditorValidatorSubsystem::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UEditorValidatorSubsystem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidatorSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditorValidatorSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditorValidatorSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditorValidatorSubsystem, 3950590155);
	template<> DATAVALIDATION_API UClass* StaticClass<UEditorValidatorSubsystem>()
	{
		return UEditorValidatorSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditorValidatorSubsystem(Z_Construct_UClass_UEditorValidatorSubsystem, &UEditorValidatorSubsystem::StaticClass, TEXT("/Script/DataValidation"), TEXT("UEditorValidatorSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditorValidatorSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
