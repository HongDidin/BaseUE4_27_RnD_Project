// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataValidation/Public/DataValidationManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataValidationManager() {}
// Cross Module References
	DATAVALIDATION_API UClass* Z_Construct_UClass_UDEPRECATED_DataValidationManager_NoRegister();
	DATAVALIDATION_API UClass* Z_Construct_UClass_UDEPRECATED_DataValidationManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DataValidation();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
// End Cross Module References
	void UDEPRECATED_DataValidationManager::StaticRegisterNativesUDEPRECATED_DataValidationManager()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_DataValidationManager_NoRegister()
	{
		return UDEPRECATED_DataValidationManager::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExcludedDirectories_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExcludedDirectories_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExcludedDirectories;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bValidateOnSave_MetaData[];
#endif
		static void NewProp_bValidateOnSave_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bValidateOnSave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataValidationManagerClassName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DataValidationManagerClassName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataValidation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Manages centralized execution and tracking of data validation, as well as handling console commands,\n * and some misc tasks like local log hooking\n */" },
		{ "IncludePath", "DataValidationManager.h" },
		{ "ModuleRelativePath", "Public/DataValidationManager.h" },
		{ "ToolTip", "Manages centralized execution and tracking of data validation, as well as handling console commands,\nand some misc tasks like local log hooking" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_ExcludedDirectories_Inner = { "ExcludedDirectories", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_ExcludedDirectories_MetaData[] = {
		{ "Comment", "/**\n\x09 * Directories to ignore for data validation. Useful for test assets\n\x09 */" },
		{ "Deprecated", "" },
		{ "DeprecationMessage", "UDataValidationManager's ExcludedDirectories is deprecated, use UEditorValidatorSubsystem's ExcludedDirectories instead." },
		{ "ModuleRelativePath", "Public/DataValidationManager.h" },
		{ "ToolTip", "Directories to ignore for data validation. Useful for test assets" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_ExcludedDirectories = { "ExcludedDirectories", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_DataValidationManager, ExcludedDirectories), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_ExcludedDirectories_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_ExcludedDirectories_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_bValidateOnSave_MetaData[] = {
		{ "Comment", "/**\n\x09 * Rather it should validate assets on save inside the editor\n\x09 */" },
		{ "Deprecated", "" },
		{ "DeprecationMessage", "UDataValidationManager's bValidateOnSave is deprecated, use UEditorValidatorSubsystem's bValidateOnSave instead." },
		{ "ModuleRelativePath", "Public/DataValidationManager.h" },
		{ "ToolTip", "Rather it should validate assets on save inside the editor" },
	};
#endif
	void Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_bValidateOnSave_SetBit(void* Obj)
	{
		((UDEPRECATED_DataValidationManager*)Obj)->bValidateOnSave = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_bValidateOnSave = { "bValidateOnSave", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDEPRECATED_DataValidationManager), &Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_bValidateOnSave_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_bValidateOnSave_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_bValidateOnSave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_DataValidationManagerClassName_MetaData[] = {
		{ "Comment", "/** The class to instantiate as the manager object. Defaults to this class but can be overridden */" },
		{ "ModuleRelativePath", "Public/DataValidationManager.h" },
		{ "ToolTip", "The class to instantiate as the manager object. Defaults to this class but can be overridden" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_DataValidationManagerClassName = { "DataValidationManagerClassName", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_DataValidationManager, DataValidationManagerClassName), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_DataValidationManagerClassName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_DataValidationManagerClassName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_ExcludedDirectories_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_ExcludedDirectories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_bValidateOnSave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::NewProp_DataValidationManagerClassName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_DataValidationManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::ClassParams = {
		&UDEPRECATED_DataValidationManager::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::PropPointers),
		0,
		0x021002A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_DataValidationManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_DataValidationManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_DataValidationManager, 1722209876);
	template<> DATAVALIDATION_API UClass* StaticClass<UDEPRECATED_DataValidationManager>()
	{
		return UDEPRECATED_DataValidationManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_DataValidationManager(Z_Construct_UClass_UDEPRECATED_DataValidationManager, &UDEPRECATED_DataValidationManager::StaticClass, TEXT("/Script/DataValidation"), TEXT("UDEPRECATED_DataValidationManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_DataValidationManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
