// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataValidation/Public/EditorValidator_Localization.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditorValidator_Localization() {}
// Cross Module References
	DATAVALIDATION_API UClass* Z_Construct_UClass_UEditorValidator_Localization_NoRegister();
	DATAVALIDATION_API UClass* Z_Construct_UClass_UEditorValidator_Localization();
	DATAVALIDATION_API UClass* Z_Construct_UClass_UEditorValidatorBase();
	UPackage* Z_Construct_UPackage__Script_DataValidation();
// End Cross Module References
	void UEditorValidator_Localization::StaticRegisterNativesUEditorValidator_Localization()
	{
	}
	UClass* Z_Construct_UClass_UEditorValidator_Localization_NoRegister()
	{
		return UEditorValidator_Localization::StaticClass();
	}
	struct Z_Construct_UClass_UEditorValidator_Localization_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditorValidator_Localization_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditorValidatorBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DataValidation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorValidator_Localization_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/*\n* Validates that localized assets (within the L10N folder) conform to a corresponding source asset of the correct type.\n* Localized assets that fail this validation will never be loaded as localized variants at runtime.\n*/" },
		{ "IncludePath", "EditorValidator_Localization.h" },
		{ "ModuleRelativePath", "Public/EditorValidator_Localization.h" },
		{ "ToolTip", "* Validates that localized assets (within the L10N folder) conform to a corresponding source asset of the correct type.\n* Localized assets that fail this validation will never be loaded as localized variants at runtime." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditorValidator_Localization_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditorValidator_Localization>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditorValidator_Localization_Statics::ClassParams = {
		&UEditorValidator_Localization::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEditorValidator_Localization_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorValidator_Localization_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditorValidator_Localization()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditorValidator_Localization_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditorValidator_Localization, 2984888555);
	template<> DATAVALIDATION_API UClass* StaticClass<UEditorValidator_Localization>()
	{
		return UEditorValidator_Localization::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditorValidator_Localization(Z_Construct_UClass_UEditorValidator_Localization, &UEditorValidator_Localization::StaticClass, TEXT("/Script/DataValidation"), TEXT("UEditorValidator_Localization"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditorValidator_Localization);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
