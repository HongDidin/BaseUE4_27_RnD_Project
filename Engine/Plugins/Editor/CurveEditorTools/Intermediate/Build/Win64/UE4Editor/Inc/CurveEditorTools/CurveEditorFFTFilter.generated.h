// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CURVEEDITORTOOLS_CurveEditorFFTFilter_generated_h
#error "CurveEditorFFTFilter.generated.h already included, missing '#pragma once' in CurveEditorFFTFilter.h"
#endif
#define CURVEEDITORTOOLS_CurveEditorFFTFilter_generated_h

#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_SPARSE_DATA
#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_RPC_WRAPPERS
#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCurveEditorFFTFilter(); \
	friend struct Z_Construct_UClass_UCurveEditorFFTFilter_Statics; \
public: \
	DECLARE_CLASS(UCurveEditorFFTFilter, UCurveEditorFilterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CurveEditorTools"), NO_API) \
	DECLARE_SERIALIZER(UCurveEditorFFTFilter)


#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUCurveEditorFFTFilter(); \
	friend struct Z_Construct_UClass_UCurveEditorFFTFilter_Statics; \
public: \
	DECLARE_CLASS(UCurveEditorFFTFilter, UCurveEditorFilterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CurveEditorTools"), NO_API) \
	DECLARE_SERIALIZER(UCurveEditorFFTFilter)


#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCurveEditorFFTFilter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCurveEditorFFTFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCurveEditorFFTFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCurveEditorFFTFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCurveEditorFFTFilter(UCurveEditorFFTFilter&&); \
	NO_API UCurveEditorFFTFilter(const UCurveEditorFFTFilter&); \
public:


#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCurveEditorFFTFilter(UCurveEditorFFTFilter&&); \
	NO_API UCurveEditorFFTFilter(const UCurveEditorFFTFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCurveEditorFFTFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCurveEditorFFTFilter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCurveEditorFFTFilter)


#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_23_PROLOG
#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_SPARSE_DATA \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_RPC_WRAPPERS \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_INCLASS \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_SPARSE_DATA \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CURVEEDITORTOOLS_API UClass* StaticClass<class UCurveEditorFFTFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Filters_CurveEditorFFTFilter_h


#define FOREACH_ENUM_ECURVEEDITORFFTFILTERTYPE(op) \
	op(ECurveEditorFFTFilterType::Lowpass) \
	op(ECurveEditorFFTFilterType::Highpass) 

enum class ECurveEditorFFTFilterType : uint8;
template<> CURVEEDITORTOOLS_API UEnum* StaticEnum<ECurveEditorFFTFilterType>();

#define FOREACH_ENUM_ECURVEEDITORFFTFILTERCLASS(op) \
	op(ECurveEditorFFTFilterClass::Butterworth) \
	op(ECurveEditorFFTFilterClass::Chebyshev) 

enum class ECurveEditorFFTFilterClass : uint8;
template<> CURVEEDITORTOOLS_API UEnum* StaticEnum<ECurveEditorFFTFilterClass>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
