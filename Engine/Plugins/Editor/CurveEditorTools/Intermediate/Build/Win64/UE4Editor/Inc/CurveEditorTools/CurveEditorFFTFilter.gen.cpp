// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CurveEditorTools/Private/Filters/CurveEditorFFTFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCurveEditorFFTFilter() {}
// Cross Module References
	CURVEEDITORTOOLS_API UEnum* Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterType();
	UPackage* Z_Construct_UPackage__Script_CurveEditorTools();
	CURVEEDITORTOOLS_API UEnum* Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterClass();
	CURVEEDITORTOOLS_API UClass* Z_Construct_UClass_UCurveEditorFFTFilter_NoRegister();
	CURVEEDITORTOOLS_API UClass* Z_Construct_UClass_UCurveEditorFFTFilter();
	CURVEEDITOR_API UClass* Z_Construct_UClass_UCurveEditorFilterBase();
// End Cross Module References
	static UEnum* ECurveEditorFFTFilterType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterType, Z_Construct_UPackage__Script_CurveEditorTools(), TEXT("ECurveEditorFFTFilterType"));
		}
		return Singleton;
	}
	template<> CURVEEDITORTOOLS_API UEnum* StaticEnum<ECurveEditorFFTFilterType>()
	{
		return ECurveEditorFFTFilterType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECurveEditorFFTFilterType(ECurveEditorFFTFilterType_StaticEnum, TEXT("/Script/CurveEditorTools"), TEXT("ECurveEditorFFTFilterType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterType_Hash() { return 1226270606U; }
	UEnum* Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CurveEditorTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECurveEditorFFTFilterType"), 0, Get_Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECurveEditorFFTFilterType::Lowpass", (int64)ECurveEditorFFTFilterType::Lowpass },
				{ "ECurveEditorFFTFilterType::Highpass", (int64)ECurveEditorFFTFilterType::Highpass },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Highpass.Name", "ECurveEditorFFTFilterType::Highpass" },
				{ "Lowpass.Name", "ECurveEditorFFTFilterType::Lowpass" },
				{ "ModuleRelativePath", "Private/Filters/CurveEditorFFTFilter.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CurveEditorTools,
				nullptr,
				"ECurveEditorFFTFilterType",
				"ECurveEditorFFTFilterType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ECurveEditorFFTFilterClass_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterClass, Z_Construct_UPackage__Script_CurveEditorTools(), TEXT("ECurveEditorFFTFilterClass"));
		}
		return Singleton;
	}
	template<> CURVEEDITORTOOLS_API UEnum* StaticEnum<ECurveEditorFFTFilterClass>()
	{
		return ECurveEditorFFTFilterClass_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECurveEditorFFTFilterClass(ECurveEditorFFTFilterClass_StaticEnum, TEXT("/Script/CurveEditorTools"), TEXT("ECurveEditorFFTFilterClass"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterClass_Hash() { return 235745046U; }
	UEnum* Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterClass()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CurveEditorTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECurveEditorFFTFilterClass"), 0, Get_Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterClass_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECurveEditorFFTFilterClass::Butterworth", (int64)ECurveEditorFFTFilterClass::Butterworth },
				{ "ECurveEditorFFTFilterClass::Chebyshev", (int64)ECurveEditorFFTFilterClass::Chebyshev },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Butterworth.Name", "ECurveEditorFFTFilterClass::Butterworth" },
				{ "Chebyshev.Name", "ECurveEditorFFTFilterClass::Chebyshev" },
				{ "ModuleRelativePath", "Private/Filters/CurveEditorFFTFilter.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CurveEditorTools,
				nullptr,
				"ECurveEditorFFTFilterClass",
				"ECurveEditorFFTFilterClass",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UCurveEditorFFTFilter::StaticRegisterNativesUCurveEditorFFTFilter()
	{
	}
	UClass* Z_Construct_UClass_UCurveEditorFFTFilter_NoRegister()
	{
		return UCurveEditorFFTFilter::StaticClass();
	}
	struct Z_Construct_UClass_UCurveEditorFFTFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutoffFrequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CutoffFrequency;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Response_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Response_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Response;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Order_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Order;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCurveEditorFFTFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCurveEditorFilterBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CurveEditorTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCurveEditorFFTFilter_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Fourier Transform (FFT)" },
		{ "IncludePath", "Filters/CurveEditorFFTFilter.h" },
		{ "ModuleRelativePath", "Private/Filters/CurveEditorFFTFilter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_CutoffFrequency_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Normalized between 0-1. In a low pass filter, the lower the value is the smoother the output. In a high pass filter the higher the value the smoother the output.*/" },
		{ "ModuleRelativePath", "Private/Filters/CurveEditorFFTFilter.h" },
		{ "ToolTip", "Normalized between 0-1. In a low pass filter, the lower the value is the smoother the output. In a high pass filter the higher the value the smoother the output." },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_CutoffFrequency = { "CutoffFrequency", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCurveEditorFFTFilter, CutoffFrequency), METADATA_PARAMS(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_CutoffFrequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_CutoffFrequency_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Which frequencies are allowed through. For example, low-pass will let low frequency through and remove high frequency noise. */" },
		{ "ModuleRelativePath", "Private/Filters/CurveEditorFFTFilter.h" },
		{ "ToolTip", "Which frequencies are allowed through. For example, low-pass will let low frequency through and remove high frequency noise." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCurveEditorFFTFilter, Type), Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterType, METADATA_PARAMS(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Response_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Response_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Which FFT filter implementation to use. */" },
		{ "ModuleRelativePath", "Private/Filters/CurveEditorFFTFilter.h" },
		{ "ToolTip", "Which FFT filter implementation to use." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Response = { "Response", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCurveEditorFFTFilter, Response), Z_Construct_UEnum_CurveEditorTools_ECurveEditorFFTFilterClass, METADATA_PARAMS(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Response_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Response_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Order_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** The number of samples used to filter in the time domain. It maps how steep the roll off is for the filter. */" },
		{ "ModuleRelativePath", "Private/Filters/CurveEditorFFTFilter.h" },
		{ "ToolTip", "The number of samples used to filter in the time domain. It maps how steep the roll off is for the filter." },
		{ "UIMax", "8" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Order = { "Order", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCurveEditorFFTFilter, Order), METADATA_PARAMS(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Order_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Order_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCurveEditorFFTFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_CutoffFrequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Response_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Response,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCurveEditorFFTFilter_Statics::NewProp_Order,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCurveEditorFFTFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCurveEditorFFTFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCurveEditorFFTFilter_Statics::ClassParams = {
		&UCurveEditorFFTFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCurveEditorFFTFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCurveEditorFFTFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCurveEditorFFTFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCurveEditorFFTFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCurveEditorFFTFilter, 150279387);
	template<> CURVEEDITORTOOLS_API UClass* StaticClass<UCurveEditorFFTFilter>()
	{
		return UCurveEditorFFTFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCurveEditorFFTFilter(Z_Construct_UClass_UCurveEditorFFTFilter, &UCurveEditorFFTFilter::StaticClass, TEXT("/Script/CurveEditorTools"), TEXT("UCurveEditorFFTFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCurveEditorFFTFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
