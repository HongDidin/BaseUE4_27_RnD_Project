// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CurveEditorTools/Private/Tools/CurveEditorMultiScaleTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCurveEditorMultiScaleTool() {}
// Cross Module References
	CURVEEDITORTOOLS_API UEnum* Z_Construct_UEnum_CurveEditorTools_EMultiScalePivotType();
	UPackage* Z_Construct_UPackage__Script_CurveEditorTools();
	CURVEEDITORTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FMultiScaleToolOptions();
// End Cross Module References
	static UEnum* EMultiScalePivotType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CurveEditorTools_EMultiScalePivotType, Z_Construct_UPackage__Script_CurveEditorTools(), TEXT("EMultiScalePivotType"));
		}
		return Singleton;
	}
	template<> CURVEEDITORTOOLS_API UEnum* StaticEnum<EMultiScalePivotType>()
	{
		return EMultiScalePivotType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMultiScalePivotType(EMultiScalePivotType_StaticEnum, TEXT("/Script/CurveEditorTools"), TEXT("EMultiScalePivotType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CurveEditorTools_EMultiScalePivotType_Hash() { return 51980320U; }
	UEnum* Z_Construct_UEnum_CurveEditorTools_EMultiScalePivotType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CurveEditorTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMultiScalePivotType"), 0, Get_Z_Construct_UEnum_CurveEditorTools_EMultiScalePivotType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMultiScalePivotType::Average", (int64)EMultiScalePivotType::Average },
				{ "EMultiScalePivotType::BoundCenter", (int64)EMultiScalePivotType::BoundCenter },
				{ "EMultiScalePivotType::FirstKey", (int64)EMultiScalePivotType::FirstKey },
				{ "EMultiScalePivotType::LastKey", (int64)EMultiScalePivotType::LastKey },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Average.Name", "EMultiScalePivotType::Average" },
				{ "BoundCenter.Name", "EMultiScalePivotType::BoundCenter" },
				{ "FirstKey.Name", "EMultiScalePivotType::FirstKey" },
				{ "LastKey.Name", "EMultiScalePivotType::LastKey" },
				{ "ModuleRelativePath", "Private/Tools/CurveEditorMultiScaleTool.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CurveEditorTools,
				nullptr,
				"EMultiScalePivotType",
				"EMultiScalePivotType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMultiScaleToolOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CURVEEDITORTOOLS_API uint32 Get_Z_Construct_UScriptStruct_FMultiScaleToolOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMultiScaleToolOptions, Z_Construct_UPackage__Script_CurveEditorTools(), TEXT("MultiScaleToolOptions"), sizeof(FMultiScaleToolOptions), Get_Z_Construct_UScriptStruct_FMultiScaleToolOptions_Hash());
	}
	return Singleton;
}
template<> CURVEEDITORTOOLS_API UScriptStruct* StaticStruct<FMultiScaleToolOptions>()
{
	return FMultiScaleToolOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMultiScaleToolOptions(FMultiScaleToolOptions::StaticStruct, TEXT("/Script/CurveEditorTools"), TEXT("MultiScaleToolOptions"), false, nullptr, nullptr);
static struct FScriptStruct_CurveEditorTools_StaticRegisterNativesFMultiScaleToolOptions
{
	FScriptStruct_CurveEditorTools_StaticRegisterNativesFMultiScaleToolOptions()
	{
		UScriptStruct::DeferCppStructOps<FMultiScaleToolOptions>(FName(TEXT("MultiScaleToolOptions")));
	}
} ScriptStruct_CurveEditorTools_StaticRegisterNativesFMultiScaleToolOptions;
	struct Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_XScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_XScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_YScale;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PivotType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PivotType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PivotType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Tools/CurveEditorMultiScaleTool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMultiScaleToolOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_XScale_MetaData[] = {
		{ "Category", "ToolOptions" },
		{ "ModuleRelativePath", "Private/Tools/CurveEditorMultiScaleTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_XScale = { "XScale", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiScaleToolOptions, XScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_XScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_XScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_YScale_MetaData[] = {
		{ "Category", "ToolOptions" },
		{ "ModuleRelativePath", "Private/Tools/CurveEditorMultiScaleTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_YScale = { "YScale", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiScaleToolOptions, YScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_YScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_YScale_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_PivotType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_PivotType_MetaData[] = {
		{ "Category", "ToolOptions" },
		{ "ModuleRelativePath", "Private/Tools/CurveEditorMultiScaleTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_PivotType = { "PivotType", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiScaleToolOptions, PivotType), Z_Construct_UEnum_CurveEditorTools_EMultiScalePivotType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_PivotType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_PivotType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_XScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_YScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_PivotType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::NewProp_PivotType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CurveEditorTools,
		nullptr,
		&NewStructOps,
		"MultiScaleToolOptions",
		sizeof(FMultiScaleToolOptions),
		alignof(FMultiScaleToolOptions),
		Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMultiScaleToolOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMultiScaleToolOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CurveEditorTools();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MultiScaleToolOptions"), sizeof(FMultiScaleToolOptions), Get_Z_Construct_UScriptStruct_FMultiScaleToolOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMultiScaleToolOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMultiScaleToolOptions_Hash() { return 253707458U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
