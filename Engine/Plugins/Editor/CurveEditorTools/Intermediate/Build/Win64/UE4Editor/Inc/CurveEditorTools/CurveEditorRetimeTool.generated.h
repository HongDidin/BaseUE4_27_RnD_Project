// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CURVEEDITORTOOLS_CurveEditorRetimeTool_generated_h
#error "CurveEditorRetimeTool.generated.h already included, missing '#pragma once' in CurveEditorRetimeTool.h"
#endif
#define CURVEEDITORTOOLS_CurveEditorRetimeTool_generated_h

#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_22_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCurveEditorRetimeAnchor_Statics; \
	CURVEEDITORTOOLS_API static class UScriptStruct* StaticStruct();


template<> CURVEEDITORTOOLS_API UScriptStruct* StaticStruct<struct FCurveEditorRetimeAnchor>();

#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_SPARSE_DATA
#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_RPC_WRAPPERS
#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCurveEditorRetimeToolData(); \
	friend struct Z_Construct_UClass_UCurveEditorRetimeToolData_Statics; \
public: \
	DECLARE_CLASS(UCurveEditorRetimeToolData, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CurveEditorTools"), NO_API) \
	DECLARE_SERIALIZER(UCurveEditorRetimeToolData)


#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_INCLASS \
private: \
	static void StaticRegisterNativesUCurveEditorRetimeToolData(); \
	friend struct Z_Construct_UClass_UCurveEditorRetimeToolData_Statics; \
public: \
	DECLARE_CLASS(UCurveEditorRetimeToolData, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CurveEditorTools"), NO_API) \
	DECLARE_SERIALIZER(UCurveEditorRetimeToolData)


#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCurveEditorRetimeToolData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCurveEditorRetimeToolData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCurveEditorRetimeToolData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCurveEditorRetimeToolData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCurveEditorRetimeToolData(UCurveEditorRetimeToolData&&); \
	NO_API UCurveEditorRetimeToolData(const UCurveEditorRetimeToolData&); \
public:


#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCurveEditorRetimeToolData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCurveEditorRetimeToolData(UCurveEditorRetimeToolData&&); \
	NO_API UCurveEditorRetimeToolData(const UCurveEditorRetimeToolData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCurveEditorRetimeToolData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCurveEditorRetimeToolData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCurveEditorRetimeToolData)


#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_58_PROLOG
#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_SPARSE_DATA \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_RPC_WRAPPERS \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_INCLASS \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_SPARSE_DATA \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h_61_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CURVEEDITORTOOLS_API UClass* StaticClass<class UCurveEditorRetimeToolData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_CurveEditorTools_Source_CurveEditorTools_Private_Tools_CurveEditorRetimeTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
