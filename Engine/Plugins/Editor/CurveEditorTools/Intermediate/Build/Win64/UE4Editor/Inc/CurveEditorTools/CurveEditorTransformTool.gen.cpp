// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CurveEditorTools/Private/Tools/CurveEditorTransformTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCurveEditorTransformTool() {}
// Cross Module References
	CURVEEDITORTOOLS_API UEnum* Z_Construct_UEnum_CurveEditorTools_EToolTransformInterpType();
	UPackage* Z_Construct_UPackage__Script_CurveEditorTools();
	CURVEEDITORTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FTransformToolOptions();
// End Cross Module References
	static UEnum* EToolTransformInterpType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CurveEditorTools_EToolTransformInterpType, Z_Construct_UPackage__Script_CurveEditorTools(), TEXT("EToolTransformInterpType"));
		}
		return Singleton;
	}
	template<> CURVEEDITORTOOLS_API UEnum* StaticEnum<EToolTransformInterpType>()
	{
		return EToolTransformInterpType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EToolTransformInterpType(EToolTransformInterpType_StaticEnum, TEXT("/Script/CurveEditorTools"), TEXT("EToolTransformInterpType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CurveEditorTools_EToolTransformInterpType_Hash() { return 443472073U; }
	UEnum* Z_Construct_UEnum_CurveEditorTools_EToolTransformInterpType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CurveEditorTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EToolTransformInterpType"), 0, Get_Z_Construct_UEnum_CurveEditorTools_EToolTransformInterpType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EToolTransformInterpType::Linear", (int64)EToolTransformInterpType::Linear },
				{ "EToolTransformInterpType::Sinusoidal", (int64)EToolTransformInterpType::Sinusoidal },
				{ "EToolTransformInterpType::Cubic", (int64)EToolTransformInterpType::Cubic },
				{ "EToolTransformInterpType::CircularIn", (int64)EToolTransformInterpType::CircularIn },
				{ "EToolTransformInterpType::CircularOut", (int64)EToolTransformInterpType::CircularOut },
				{ "EToolTransformInterpType::ExpIn", (int64)EToolTransformInterpType::ExpIn },
				{ "EToolTransformInterpType::ExpOut", (int64)EToolTransformInterpType::ExpOut },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CircularIn.Name", "EToolTransformInterpType::CircularIn" },
				{ "CircularOut.Name", "EToolTransformInterpType::CircularOut" },
				{ "Cubic.Name", "EToolTransformInterpType::Cubic" },
				{ "ExpIn.Name", "EToolTransformInterpType::ExpIn" },
				{ "ExpOut.Name", "EToolTransformInterpType::ExpOut" },
				{ "Linear.Name", "EToolTransformInterpType::Linear" },
				{ "ModuleRelativePath", "Private/Tools/CurveEditorTransformTool.h" },
				{ "Sinusoidal.Name", "EToolTransformInterpType::Sinusoidal" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CurveEditorTools,
				nullptr,
				"EToolTransformInterpType",
				"EToolTransformInterpType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FTransformToolOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CURVEEDITORTOOLS_API uint32 Get_Z_Construct_UScriptStruct_FTransformToolOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTransformToolOptions, Z_Construct_UPackage__Script_CurveEditorTools(), TEXT("TransformToolOptions"), sizeof(FTransformToolOptions), Get_Z_Construct_UScriptStruct_FTransformToolOptions_Hash());
	}
	return Singleton;
}
template<> CURVEEDITORTOOLS_API UScriptStruct* StaticStruct<FTransformToolOptions>()
{
	return FTransformToolOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTransformToolOptions(FTransformToolOptions::StaticStruct, TEXT("/Script/CurveEditorTools"), TEXT("TransformToolOptions"), false, nullptr, nullptr);
static struct FScriptStruct_CurveEditorTools_StaticRegisterNativesFTransformToolOptions
{
	FScriptStruct_CurveEditorTools_StaticRegisterNativesFTransformToolOptions()
	{
		UScriptStruct::DeferCppStructOps<FTransformToolOptions>(FName(TEXT("TransformToolOptions")));
	}
} ScriptStruct_CurveEditorTools_StaticRegisterNativesFTransformToolOptions;
	struct Z_Construct_UScriptStruct_FTransformToolOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UpperBound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerBound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LeftBound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RightBound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScaleCenterX_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScaleCenterX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScaleCenterY_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScaleCenterY;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FalloffInterpType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffInterpType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FalloffInterpType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformToolOptions_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Tools/CurveEditorTransformTool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTransformToolOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_UpperBound_MetaData[] = {
		{ "Category", "ToolOptions" },
		{ "ModuleRelativePath", "Private/Tools/CurveEditorTransformTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_UpperBound = { "UpperBound", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransformToolOptions, UpperBound), METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_UpperBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_UpperBound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_LowerBound_MetaData[] = {
		{ "Category", "ToolOptions" },
		{ "ModuleRelativePath", "Private/Tools/CurveEditorTransformTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_LowerBound = { "LowerBound", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransformToolOptions, LowerBound), METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_LowerBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_LowerBound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_LeftBound_MetaData[] = {
		{ "Category", "ToolOptions" },
		{ "ModuleRelativePath", "Private/Tools/CurveEditorTransformTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_LeftBound = { "LeftBound", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransformToolOptions, LeftBound), METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_LeftBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_LeftBound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_RightBound_MetaData[] = {
		{ "Category", "ToolOptions" },
		{ "ModuleRelativePath", "Private/Tools/CurveEditorTransformTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_RightBound = { "RightBound", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransformToolOptions, RightBound), METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_RightBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_RightBound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_ScaleCenterX_MetaData[] = {
		{ "Category", "ToolOptions" },
		{ "ModuleRelativePath", "Private/Tools/CurveEditorTransformTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_ScaleCenterX = { "ScaleCenterX", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransformToolOptions, ScaleCenterX), METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_ScaleCenterX_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_ScaleCenterX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_ScaleCenterY_MetaData[] = {
		{ "Category", "ToolOptions" },
		{ "ModuleRelativePath", "Private/Tools/CurveEditorTransformTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_ScaleCenterY = { "ScaleCenterY", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransformToolOptions, ScaleCenterY), METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_ScaleCenterY_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_ScaleCenterY_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_FalloffInterpType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_FalloffInterpType_MetaData[] = {
		{ "Category", "ToolOptions" },
		{ "ModuleRelativePath", "Private/Tools/CurveEditorTransformTool.h" },
		{ "ToolTip", "Interpolation type for soft selection (activate by holding ctrl)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_FalloffInterpType = { "FalloffInterpType", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransformToolOptions, FalloffInterpType), Z_Construct_UEnum_CurveEditorTools_EToolTransformInterpType, METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_FalloffInterpType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_FalloffInterpType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTransformToolOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_UpperBound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_LowerBound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_LeftBound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_RightBound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_ScaleCenterX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_ScaleCenterY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_FalloffInterpType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransformToolOptions_Statics::NewProp_FalloffInterpType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTransformToolOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CurveEditorTools,
		nullptr,
		&NewStructOps,
		"TransformToolOptions",
		sizeof(FTransformToolOptions),
		alignof(FTransformToolOptions),
		Z_Construct_UScriptStruct_FTransformToolOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransformToolOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTransformToolOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTransformToolOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CurveEditorTools();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TransformToolOptions"), sizeof(FTransformToolOptions), Get_Z_Construct_UScriptStruct_FTransformToolOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTransformToolOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTransformToolOptions_Hash() { return 2061461158U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
