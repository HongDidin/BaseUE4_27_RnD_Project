// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTENTBROWSERASSETDATASOURCE_ContentBrowserAssetDataSource_generated_h
#error "ContentBrowserAssetDataSource.generated.h already included, missing '#pragma once' in ContentBrowserAssetDataSource.h"
#endif
#define CONTENTBROWSERASSETDATASOURCE_ContentBrowserAssetDataSource_generated_h

#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_26_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTENTBROWSERASSETDATASOURCE_API UScriptStruct* StaticStruct<struct FContentBrowserCompiledAssetDataFilter>();

#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_SPARSE_DATA
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_RPC_WRAPPERS
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUContentBrowserAssetDataSource(); \
	friend struct Z_Construct_UClass_UContentBrowserAssetDataSource_Statics; \
public: \
	DECLARE_CLASS(UContentBrowserAssetDataSource, UContentBrowserDataSource, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ContentBrowserAssetDataSource"), NO_API) \
	DECLARE_SERIALIZER(UContentBrowserAssetDataSource)


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_INCLASS \
private: \
	static void StaticRegisterNativesUContentBrowserAssetDataSource(); \
	friend struct Z_Construct_UClass_UContentBrowserAssetDataSource_Statics; \
public: \
	DECLARE_CLASS(UContentBrowserAssetDataSource, UContentBrowserDataSource, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ContentBrowserAssetDataSource"), NO_API) \
	DECLARE_SERIALIZER(UContentBrowserAssetDataSource)


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UContentBrowserAssetDataSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UContentBrowserAssetDataSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UContentBrowserAssetDataSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UContentBrowserAssetDataSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UContentBrowserAssetDataSource(UContentBrowserAssetDataSource&&); \
	NO_API UContentBrowserAssetDataSource(const UContentBrowserAssetDataSource&); \
public:


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UContentBrowserAssetDataSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UContentBrowserAssetDataSource(UContentBrowserAssetDataSource&&); \
	NO_API UContentBrowserAssetDataSource(const UContentBrowserAssetDataSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UContentBrowserAssetDataSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UContentBrowserAssetDataSource); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UContentBrowserAssetDataSource)


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_51_PROLOG
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_SPARSE_DATA \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_RPC_WRAPPERS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_INCLASS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_SPARSE_DATA \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h_54_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTENTBROWSERASSETDATASOURCE_API UClass* StaticClass<class UContentBrowserAssetDataSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_ContentBrowser_ContentBrowserAssetDataSource_Source_ContentBrowserAssetDataSource_Public_ContentBrowserAssetDataSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
