// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ContentBrowserAssetDataSource/Public/ContentBrowserAssetDataSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeContentBrowserAssetDataSource() {}
// Cross Module References
	CONTENTBROWSERASSETDATASOURCE_API UScriptStruct* Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter();
	UPackage* Z_Construct_UPackage__Script_ContentBrowserAssetDataSource();
	CONTENTBROWSERASSETDATASOURCE_API UClass* Z_Construct_UClass_UContentBrowserAssetDataSource_NoRegister();
	CONTENTBROWSERASSETDATASOURCE_API UClass* Z_Construct_UClass_UContentBrowserAssetDataSource();
	CONTENTBROWSERDATA_API UClass* Z_Construct_UClass_UContentBrowserDataSource();
// End Cross Module References
class UScriptStruct* FContentBrowserCompiledAssetDataFilter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTENTBROWSERASSETDATASOURCE_API uint32 Get_Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter, Z_Construct_UPackage__Script_ContentBrowserAssetDataSource(), TEXT("ContentBrowserCompiledAssetDataFilter"), sizeof(FContentBrowserCompiledAssetDataFilter), Get_Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Hash());
	}
	return Singleton;
}
template<> CONTENTBROWSERASSETDATASOURCE_API UScriptStruct* StaticStruct<FContentBrowserCompiledAssetDataFilter>()
{
	return FContentBrowserCompiledAssetDataFilter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FContentBrowserCompiledAssetDataFilter(FContentBrowserCompiledAssetDataFilter::StaticStruct, TEXT("/Script/ContentBrowserAssetDataSource"), TEXT("ContentBrowserCompiledAssetDataFilter"), false, nullptr, nullptr);
static struct FScriptStruct_ContentBrowserAssetDataSource_StaticRegisterNativesFContentBrowserCompiledAssetDataFilter
{
	FScriptStruct_ContentBrowserAssetDataSource_StaticRegisterNativesFContentBrowserCompiledAssetDataFilter()
	{
		UScriptStruct::DeferCppStructOps<FContentBrowserCompiledAssetDataFilter>(FName(TEXT("ContentBrowserCompiledAssetDataFilter")));
	}
} ScriptStruct_ContentBrowserAssetDataSource_StaticRegisterNativesFContentBrowserCompiledAssetDataFilter;
	struct Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ContentBrowserAssetDataSource.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FContentBrowserCompiledAssetDataFilter>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ContentBrowserAssetDataSource,
		nullptr,
		&NewStructOps,
		"ContentBrowserCompiledAssetDataFilter",
		sizeof(FContentBrowserCompiledAssetDataFilter),
		alignof(FContentBrowserCompiledAssetDataFilter),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ContentBrowserAssetDataSource();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ContentBrowserCompiledAssetDataFilter"), sizeof(FContentBrowserCompiledAssetDataFilter), Get_Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FContentBrowserCompiledAssetDataFilter_Hash() { return 1211758920U; }
	void UContentBrowserAssetDataSource::StaticRegisterNativesUContentBrowserAssetDataSource()
	{
	}
	UClass* Z_Construct_UClass_UContentBrowserAssetDataSource_NoRegister()
	{
		return UContentBrowserAssetDataSource::StaticClass();
	}
	struct Z_Construct_UClass_UContentBrowserAssetDataSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UContentBrowserAssetDataSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UContentBrowserDataSource,
		(UObject* (*)())Z_Construct_UPackage__Script_ContentBrowserAssetDataSource,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UContentBrowserAssetDataSource_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ContentBrowserAssetDataSource.h" },
		{ "ModuleRelativePath", "Public/ContentBrowserAssetDataSource.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UContentBrowserAssetDataSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UContentBrowserAssetDataSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UContentBrowserAssetDataSource_Statics::ClassParams = {
		&UContentBrowserAssetDataSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UContentBrowserAssetDataSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UContentBrowserAssetDataSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UContentBrowserAssetDataSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UContentBrowserAssetDataSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UContentBrowserAssetDataSource, 4089253888);
	template<> CONTENTBROWSERASSETDATASOURCE_API UClass* StaticClass<UContentBrowserAssetDataSource>()
	{
		return UContentBrowserAssetDataSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UContentBrowserAssetDataSource(Z_Construct_UClass_UContentBrowserAssetDataSource, &UContentBrowserAssetDataSource::StaticClass, TEXT("/Script/ContentBrowserAssetDataSource"), TEXT("UContentBrowserAssetDataSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UContentBrowserAssetDataSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
