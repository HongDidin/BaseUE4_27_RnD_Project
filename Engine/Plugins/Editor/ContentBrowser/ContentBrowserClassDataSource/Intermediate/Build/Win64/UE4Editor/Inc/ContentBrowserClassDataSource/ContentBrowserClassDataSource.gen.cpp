// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ContentBrowserClassDataSource/Public/ContentBrowserClassDataSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeContentBrowserClassDataSource() {}
// Cross Module References
	CONTENTBROWSERCLASSDATASOURCE_API UScriptStruct* Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter();
	UPackage* Z_Construct_UPackage__Script_ContentBrowserClassDataSource();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	CONTENTBROWSERCLASSDATASOURCE_API UClass* Z_Construct_UClass_UContentBrowserClassDataSource_NoRegister();
	CONTENTBROWSERCLASSDATASOURCE_API UClass* Z_Construct_UClass_UContentBrowserClassDataSource();
	CONTENTBROWSERDATA_API UClass* Z_Construct_UClass_UContentBrowserDataSource();
// End Cross Module References
class UScriptStruct* FContentBrowserCompiledClassDataFilter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTENTBROWSERCLASSDATASOURCE_API uint32 Get_Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter, Z_Construct_UPackage__Script_ContentBrowserClassDataSource(), TEXT("ContentBrowserCompiledClassDataFilter"), sizeof(FContentBrowserCompiledClassDataFilter), Get_Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Hash());
	}
	return Singleton;
}
template<> CONTENTBROWSERCLASSDATASOURCE_API UScriptStruct* StaticStruct<FContentBrowserCompiledClassDataFilter>()
{
	return FContentBrowserCompiledClassDataFilter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FContentBrowserCompiledClassDataFilter(FContentBrowserCompiledClassDataFilter::StaticStruct, TEXT("/Script/ContentBrowserClassDataSource"), TEXT("ContentBrowserCompiledClassDataFilter"), false, nullptr, nullptr);
static struct FScriptStruct_ContentBrowserClassDataSource_StaticRegisterNativesFContentBrowserCompiledClassDataFilter
{
	FScriptStruct_ContentBrowserClassDataSource_StaticRegisterNativesFContentBrowserCompiledClassDataFilter()
	{
		UScriptStruct::DeferCppStructOps<FContentBrowserCompiledClassDataFilter>(FName(TEXT("ContentBrowserCompiledClassDataFilter")));
	}
} ScriptStruct_ContentBrowserClassDataSource_StaticRegisterNativesFContentBrowserCompiledClassDataFilter;
	struct Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ValidClasses_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValidClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ValidClasses;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ValidFolders_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValidFolders_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ValidFolders;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ContentBrowserClassDataSource.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FContentBrowserCompiledClassDataFilter>();
	}
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidClasses_ElementProp = { "ValidClasses", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidClasses_MetaData[] = {
		{ "ModuleRelativePath", "Public/ContentBrowserClassDataSource.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidClasses = { "ValidClasses", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FContentBrowserCompiledClassDataFilter, ValidClasses), METADATA_PARAMS(Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidClasses_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidFolders_ElementProp = { "ValidFolders", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidFolders_MetaData[] = {
		{ "ModuleRelativePath", "Public/ContentBrowserClassDataSource.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidFolders = { "ValidFolders", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FContentBrowserCompiledClassDataFilter, ValidFolders), METADATA_PARAMS(Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidFolders_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidFolders_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidClasses_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidFolders_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::NewProp_ValidFolders,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ContentBrowserClassDataSource,
		nullptr,
		&NewStructOps,
		"ContentBrowserCompiledClassDataFilter",
		sizeof(FContentBrowserCompiledClassDataFilter),
		alignof(FContentBrowserCompiledClassDataFilter),
		Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ContentBrowserClassDataSource();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ContentBrowserCompiledClassDataFilter"), sizeof(FContentBrowserCompiledClassDataFilter), Get_Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Hash() { return 3534354351U; }
	void UContentBrowserClassDataSource::StaticRegisterNativesUContentBrowserClassDataSource()
	{
	}
	UClass* Z_Construct_UClass_UContentBrowserClassDataSource_NoRegister()
	{
		return UContentBrowserClassDataSource::StaticClass();
	}
	struct Z_Construct_UClass_UContentBrowserClassDataSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UContentBrowserClassDataSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UContentBrowserDataSource,
		(UObject* (*)())Z_Construct_UPackage__Script_ContentBrowserClassDataSource,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UContentBrowserClassDataSource_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ContentBrowserClassDataSource.h" },
		{ "ModuleRelativePath", "Public/ContentBrowserClassDataSource.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UContentBrowserClassDataSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UContentBrowserClassDataSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UContentBrowserClassDataSource_Statics::ClassParams = {
		&UContentBrowserClassDataSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UContentBrowserClassDataSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UContentBrowserClassDataSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UContentBrowserClassDataSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UContentBrowserClassDataSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UContentBrowserClassDataSource, 2368962930);
	template<> CONTENTBROWSERCLASSDATASOURCE_API UClass* StaticClass<UContentBrowserClassDataSource>()
	{
		return UContentBrowserClassDataSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UContentBrowserClassDataSource(Z_Construct_UClass_UContentBrowserClassDataSource, &UContentBrowserClassDataSource::StaticClass, TEXT("/Script/ContentBrowserClassDataSource"), TEXT("UContentBrowserClassDataSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UContentBrowserClassDataSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
