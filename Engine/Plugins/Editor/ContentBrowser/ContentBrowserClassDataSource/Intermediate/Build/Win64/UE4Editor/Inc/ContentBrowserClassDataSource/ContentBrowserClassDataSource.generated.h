// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTENTBROWSERCLASSDATASOURCE_ContentBrowserClassDataSource_generated_h
#error "ContentBrowserClassDataSource.generated.h already included, missing '#pragma once' in ContentBrowserClassDataSource.h"
#endif
#define CONTENTBROWSERCLASSDATASOURCE_ContentBrowserClassDataSource_generated_h

#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FContentBrowserCompiledClassDataFilter_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTENTBROWSERCLASSDATASOURCE_API UScriptStruct* StaticStruct<struct FContentBrowserCompiledClassDataFilter>();

#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_SPARSE_DATA
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_RPC_WRAPPERS
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUContentBrowserClassDataSource(); \
	friend struct Z_Construct_UClass_UContentBrowserClassDataSource_Statics; \
public: \
	DECLARE_CLASS(UContentBrowserClassDataSource, UContentBrowserDataSource, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ContentBrowserClassDataSource"), NO_API) \
	DECLARE_SERIALIZER(UContentBrowserClassDataSource)


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUContentBrowserClassDataSource(); \
	friend struct Z_Construct_UClass_UContentBrowserClassDataSource_Statics; \
public: \
	DECLARE_CLASS(UContentBrowserClassDataSource, UContentBrowserDataSource, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ContentBrowserClassDataSource"), NO_API) \
	DECLARE_SERIALIZER(UContentBrowserClassDataSource)


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UContentBrowserClassDataSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UContentBrowserClassDataSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UContentBrowserClassDataSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UContentBrowserClassDataSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UContentBrowserClassDataSource(UContentBrowserClassDataSource&&); \
	NO_API UContentBrowserClassDataSource(const UContentBrowserClassDataSource&); \
public:


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UContentBrowserClassDataSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UContentBrowserClassDataSource(UContentBrowserClassDataSource&&); \
	NO_API UContentBrowserClassDataSource(const UContentBrowserClassDataSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UContentBrowserClassDataSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UContentBrowserClassDataSource); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UContentBrowserClassDataSource)


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_30_PROLOG
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_SPARSE_DATA \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_RPC_WRAPPERS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_INCLASS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_SPARSE_DATA \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTENTBROWSERCLASSDATASOURCE_API UClass* StaticClass<class UContentBrowserClassDataSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_ContentBrowser_ContentBrowserClassDataSource_Source_ContentBrowserClassDataSource_Public_ContentBrowserClassDataSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
