// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ContentBrowserFileDataSource/Public/ContentBrowserFileDataSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeContentBrowserFileDataSource() {}
// Cross Module References
	CONTENTBROWSERFILEDATASOURCE_API UScriptStruct* Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter();
	UPackage* Z_Construct_UPackage__Script_ContentBrowserFileDataSource();
	CONTENTBROWSERFILEDATASOURCE_API UClass* Z_Construct_UClass_UContentBrowserFileDataSource_NoRegister();
	CONTENTBROWSERFILEDATASOURCE_API UClass* Z_Construct_UClass_UContentBrowserFileDataSource();
	CONTENTBROWSERDATA_API UClass* Z_Construct_UClass_UContentBrowserDataSource();
// End Cross Module References
class UScriptStruct* FContentBrowserCompiledFileDataFilter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONTENTBROWSERFILEDATASOURCE_API uint32 Get_Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter, Z_Construct_UPackage__Script_ContentBrowserFileDataSource(), TEXT("ContentBrowserCompiledFileDataFilter"), sizeof(FContentBrowserCompiledFileDataFilter), Get_Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Hash());
	}
	return Singleton;
}
template<> CONTENTBROWSERFILEDATASOURCE_API UScriptStruct* StaticStruct<FContentBrowserCompiledFileDataFilter>()
{
	return FContentBrowserCompiledFileDataFilter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FContentBrowserCompiledFileDataFilter(FContentBrowserCompiledFileDataFilter::StaticStruct, TEXT("/Script/ContentBrowserFileDataSource"), TEXT("ContentBrowserCompiledFileDataFilter"), false, nullptr, nullptr);
static struct FScriptStruct_ContentBrowserFileDataSource_StaticRegisterNativesFContentBrowserCompiledFileDataFilter
{
	FScriptStruct_ContentBrowserFileDataSource_StaticRegisterNativesFContentBrowserCompiledFileDataFilter()
	{
		UScriptStruct::DeferCppStructOps<FContentBrowserCompiledFileDataFilter>(FName(TEXT("ContentBrowserCompiledFileDataFilter")));
	}
} ScriptStruct_ContentBrowserFileDataSource_StaticRegisterNativesFContentBrowserCompiledFileDataFilter;
	struct Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ContentBrowserFileDataSource.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FContentBrowserCompiledFileDataFilter>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ContentBrowserFileDataSource,
		nullptr,
		&NewStructOps,
		"ContentBrowserCompiledFileDataFilter",
		sizeof(FContentBrowserCompiledFileDataFilter),
		alignof(FContentBrowserCompiledFileDataFilter),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ContentBrowserFileDataSource();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ContentBrowserCompiledFileDataFilter"), sizeof(FContentBrowserCompiledFileDataFilter), Get_Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Hash() { return 1560229856U; }
	void UContentBrowserFileDataSource::StaticRegisterNativesUContentBrowserFileDataSource()
	{
	}
	UClass* Z_Construct_UClass_UContentBrowserFileDataSource_NoRegister()
	{
		return UContentBrowserFileDataSource::StaticClass();
	}
	struct Z_Construct_UClass_UContentBrowserFileDataSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UContentBrowserFileDataSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UContentBrowserDataSource,
		(UObject* (*)())Z_Construct_UPackage__Script_ContentBrowserFileDataSource,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UContentBrowserFileDataSource_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ContentBrowserFileDataSource.h" },
		{ "ModuleRelativePath", "Public/ContentBrowserFileDataSource.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UContentBrowserFileDataSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UContentBrowserFileDataSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UContentBrowserFileDataSource_Statics::ClassParams = {
		&UContentBrowserFileDataSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UContentBrowserFileDataSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UContentBrowserFileDataSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UContentBrowserFileDataSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UContentBrowserFileDataSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UContentBrowserFileDataSource, 2423156873);
	template<> CONTENTBROWSERFILEDATASOURCE_API UClass* StaticClass<UContentBrowserFileDataSource>()
	{
		return UContentBrowserFileDataSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UContentBrowserFileDataSource(Z_Construct_UClass_UContentBrowserFileDataSource, &UContentBrowserFileDataSource::StaticClass, TEXT("/Script/ContentBrowserFileDataSource"), TEXT("UContentBrowserFileDataSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UContentBrowserFileDataSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
