// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONTENTBROWSERFILEDATASOURCE_ContentBrowserFileDataSource_generated_h
#error "ContentBrowserFileDataSource.generated.h already included, missing '#pragma once' in ContentBrowserFileDataSource.h"
#endif
#define CONTENTBROWSERFILEDATASOURCE_ContentBrowserFileDataSource_generated_h

#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FContentBrowserCompiledFileDataFilter_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONTENTBROWSERFILEDATASOURCE_API UScriptStruct* StaticStruct<struct FContentBrowserCompiledFileDataFilter>();

#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_SPARSE_DATA
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_RPC_WRAPPERS
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUContentBrowserFileDataSource(); \
	friend struct Z_Construct_UClass_UContentBrowserFileDataSource_Statics; \
public: \
	DECLARE_CLASS(UContentBrowserFileDataSource, UContentBrowserDataSource, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ContentBrowserFileDataSource"), NO_API) \
	DECLARE_SERIALIZER(UContentBrowserFileDataSource)


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUContentBrowserFileDataSource(); \
	friend struct Z_Construct_UClass_UContentBrowserFileDataSource_Statics; \
public: \
	DECLARE_CLASS(UContentBrowserFileDataSource, UContentBrowserDataSource, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ContentBrowserFileDataSource"), NO_API) \
	DECLARE_SERIALIZER(UContentBrowserFileDataSource)


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UContentBrowserFileDataSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UContentBrowserFileDataSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UContentBrowserFileDataSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UContentBrowserFileDataSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UContentBrowserFileDataSource(UContentBrowserFileDataSource&&); \
	NO_API UContentBrowserFileDataSource(const UContentBrowserFileDataSource&); \
public:


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UContentBrowserFileDataSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UContentBrowserFileDataSource(UContentBrowserFileDataSource&&); \
	NO_API UContentBrowserFileDataSource(const UContentBrowserFileDataSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UContentBrowserFileDataSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UContentBrowserFileDataSource); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UContentBrowserFileDataSource)


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_26_PROLOG
#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_SPARSE_DATA \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_RPC_WRAPPERS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_INCLASS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_SPARSE_DATA \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONTENTBROWSERFILEDATASOURCE_API UClass* StaticClass<class UContentBrowserFileDataSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_ContentBrowser_ContentBrowserFileDataSource_Source_ContentBrowserFileDataSource_Public_ContentBrowserFileDataSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
