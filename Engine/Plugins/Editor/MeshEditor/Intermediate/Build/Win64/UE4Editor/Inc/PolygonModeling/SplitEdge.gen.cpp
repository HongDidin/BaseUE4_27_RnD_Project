// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/SplitEdge.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSplitEdge() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitEdgeCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitEdgeCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorEditCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void USplitEdgeCommand::StaticRegisterNativesUSplitEdgeCommand()
	{
	}
	UClass* Z_Construct_UClass_USplitEdgeCommand_NoRegister()
	{
		return USplitEdgeCommand::StaticClass();
	}
	struct Z_Construct_UClass_USplitEdgeCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USplitEdgeCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorEditCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USplitEdgeCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Inserts a vertex along an edge and allows the user to move it around */" },
		{ "IncludePath", "SplitEdge.h" },
		{ "ModuleRelativePath", "Public/SplitEdge.h" },
		{ "ToolTip", "Inserts a vertex along an edge and allows the user to move it around" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USplitEdgeCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USplitEdgeCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USplitEdgeCommand_Statics::ClassParams = {
		&USplitEdgeCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USplitEdgeCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USplitEdgeCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USplitEdgeCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USplitEdgeCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USplitEdgeCommand, 1864787116);
	template<> POLYGONMODELING_API UClass* StaticClass<USplitEdgeCommand>()
	{
		return USplitEdgeCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USplitEdgeCommand(Z_Construct_UClass_USplitEdgeCommand, &USplitEdgeCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("USplitEdgeCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USplitEdgeCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
