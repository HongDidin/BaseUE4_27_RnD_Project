// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/MeshEditorSubdividedStaticMeshAdapter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshEditorSubdividedStaticMeshAdapter() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter();
	EDITABLEMESH_API UClass* Z_Construct_UClass_UEditableMeshAdapter();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
	MESHEDITOR_API UClass* Z_Construct_UClass_UWireframeMesh_NoRegister();
// End Cross Module References
	void UMeshEditorSubdividedStaticMeshAdapter::StaticRegisterNativesUMeshEditorSubdividedStaticMeshAdapter()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_NoRegister()
	{
		return UMeshEditorSubdividedStaticMeshAdapter::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WireframeMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WireframeMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshLODIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StaticMeshLODIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditableMeshAdapter,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshEditorSubdividedStaticMeshAdapter.h" },
		{ "ModuleRelativePath", "MeshEditorSubdividedStaticMeshAdapter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::NewProp_WireframeMesh_MetaData[] = {
		{ "Comment", "/** The wireframe mesh asset we're representing */" },
		{ "ModuleRelativePath", "MeshEditorSubdividedStaticMeshAdapter.h" },
		{ "ToolTip", "The wireframe mesh asset we're representing" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::NewProp_WireframeMesh = { "WireframeMesh", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorSubdividedStaticMeshAdapter, WireframeMesh), Z_Construct_UClass_UWireframeMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::NewProp_WireframeMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::NewProp_WireframeMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::NewProp_StaticMeshLODIndex_MetaData[] = {
		{ "ModuleRelativePath", "MeshEditorSubdividedStaticMeshAdapter.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::NewProp_StaticMeshLODIndex = { "StaticMeshLODIndex", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorSubdividedStaticMeshAdapter, StaticMeshLODIndex), METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::NewProp_StaticMeshLODIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::NewProp_StaticMeshLODIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::NewProp_WireframeMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::NewProp_StaticMeshLODIndex,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorSubdividedStaticMeshAdapter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::ClassParams = {
		&UMeshEditorSubdividedStaticMeshAdapter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorSubdividedStaticMeshAdapter, 4151141468);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorSubdividedStaticMeshAdapter>()
	{
		return UMeshEditorSubdividedStaticMeshAdapter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorSubdividedStaticMeshAdapter(Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter, &UMeshEditorSubdividedStaticMeshAdapter::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorSubdividedStaticMeshAdapter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorSubdividedStaticMeshAdapter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
