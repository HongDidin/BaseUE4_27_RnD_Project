// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/FractureToolComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolComponent() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UFractureToolComponent_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UFractureToolComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
// End Cross Module References
	void UFractureToolComponent::StaticRegisterNativesUFractureToolComponent()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolComponent_NoRegister()
	{
		return UFractureToolComponent::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n*\x09UFractureToolComponent\n*/" },
		{ "IncludePath", "FractureToolComponent.h" },
		{ "ModuleRelativePath", "FractureToolComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "UFractureToolComponent" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolComponent_Statics::ClassParams = {
		&UFractureToolComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00A000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolComponent, 1760132055);
	template<> MESHEDITOR_API UClass* StaticClass<UFractureToolComponent>()
	{
		return UFractureToolComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolComponent(Z_Construct_UClass_UFractureToolComponent, &UFractureToolComponent::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UFractureToolComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
