// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_SelectSiblingsCommand_generated_h
#error "SelectSiblingsCommand.generated.h already included, missing '#pragma once' in SelectSiblingsCommand.h"
#endif
#define POLYGONMODELING_SelectSiblingsCommand_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelectSiblingsCommand(); \
	friend struct Z_Construct_UClass_USelectSiblingsCommand_Statics; \
public: \
	DECLARE_CLASS(USelectSiblingsCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USelectSiblingsCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUSelectSiblingsCommand(); \
	friend struct Z_Construct_UClass_USelectSiblingsCommand_Statics; \
public: \
	DECLARE_CLASS(USelectSiblingsCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USelectSiblingsCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectSiblingsCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectSiblingsCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectSiblingsCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectSiblingsCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectSiblingsCommand(USelectSiblingsCommand&&); \
	NO_API USelectSiblingsCommand(const USelectSiblingsCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectSiblingsCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectSiblingsCommand(USelectSiblingsCommand&&); \
	NO_API USelectSiblingsCommand(const USelectSiblingsCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectSiblingsCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectSiblingsCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectSiblingsCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_12_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h_16_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class USelectSiblingsCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectSiblingsCommand_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
