// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/ExtrudePolygon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExtrudePolygon() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UExtrudePolygonCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UExtrudePolygonCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorEditCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UExtrudePolygonCommand::StaticRegisterNativesUExtrudePolygonCommand()
	{
	}
	UClass* Z_Construct_UClass_UExtrudePolygonCommand_NoRegister()
	{
		return UExtrudePolygonCommand::StaticClass();
	}
	struct Z_Construct_UClass_UExtrudePolygonCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UExtrudePolygonCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorEditCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExtrudePolygonCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Extrudes the polygon along an axis */" },
		{ "IncludePath", "ExtrudePolygon.h" },
		{ "ModuleRelativePath", "Public/ExtrudePolygon.h" },
		{ "ToolTip", "Extrudes the polygon along an axis" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UExtrudePolygonCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UExtrudePolygonCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UExtrudePolygonCommand_Statics::ClassParams = {
		&UExtrudePolygonCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UExtrudePolygonCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UExtrudePolygonCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UExtrudePolygonCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UExtrudePolygonCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UExtrudePolygonCommand, 2773811636);
	template<> POLYGONMODELING_API UClass* StaticClass<UExtrudePolygonCommand>()
	{
		return UExtrudePolygonCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UExtrudePolygonCommand(Z_Construct_UClass_UExtrudePolygonCommand, &UExtrudePolygonCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UExtrudePolygonCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UExtrudePolygonCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
