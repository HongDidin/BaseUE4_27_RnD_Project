// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHEDITOR_MeshEditorStaticMeshAdapter_generated_h
#error "MeshEditorStaticMeshAdapter.generated.h already included, missing '#pragma once' in MeshEditorStaticMeshAdapter.h"
#endif
#define MESHEDITOR_MeshEditorStaticMeshAdapter_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorStaticMeshAdapter(); \
	friend struct Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorStaticMeshAdapter, UEditableMeshAdapter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorStaticMeshAdapter)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorStaticMeshAdapter(); \
	friend struct Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorStaticMeshAdapter, UEditableMeshAdapter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorStaticMeshAdapter)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorStaticMeshAdapter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorStaticMeshAdapter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorStaticMeshAdapter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorStaticMeshAdapter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorStaticMeshAdapter(UMeshEditorStaticMeshAdapter&&); \
	NO_API UMeshEditorStaticMeshAdapter(const UMeshEditorStaticMeshAdapter&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorStaticMeshAdapter(UMeshEditorStaticMeshAdapter&&); \
	NO_API UMeshEditorStaticMeshAdapter(const UMeshEditorStaticMeshAdapter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorStaticMeshAdapter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorStaticMeshAdapter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshEditorStaticMeshAdapter)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__WireframeMesh() { return STRUCT_OFFSET(UMeshEditorStaticMeshAdapter, WireframeMesh); } \
	FORCEINLINE static uint32 __PPO__StaticMeshLODIndex() { return STRUCT_OFFSET(UMeshEditorStaticMeshAdapter, StaticMeshLODIndex); }


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_12_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorStaticMeshAdapter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorStaticMeshAdapter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
