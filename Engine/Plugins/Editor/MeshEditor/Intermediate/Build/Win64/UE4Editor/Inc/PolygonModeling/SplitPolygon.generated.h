// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_SplitPolygon_generated_h
#error "SplitPolygon.generated.h already included, missing '#pragma once' in SplitPolygon.h"
#endif
#define POLYGONMODELING_SplitPolygon_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSplitPolygonCommand(); \
	friend struct Z_Construct_UClass_USplitPolygonCommand_Statics; \
public: \
	DECLARE_CLASS(USplitPolygonCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USplitPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUSplitPolygonCommand(); \
	friend struct Z_Construct_UClass_USplitPolygonCommand_Statics; \
public: \
	DECLARE_CLASS(USplitPolygonCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USplitPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USplitPolygonCommand(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USplitPolygonCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USplitPolygonCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USplitPolygonCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USplitPolygonCommand(USplitPolygonCommand&&); \
	NO_API USplitPolygonCommand(const USplitPolygonCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USplitPolygonCommand(USplitPolygonCommand&&); \
	NO_API USplitPolygonCommand(const USplitPolygonCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USplitPolygonCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USplitPolygonCommand); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(USplitPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Component() { return STRUCT_OFFSET(USplitPolygonCommand, Component); } \
	FORCEINLINE static uint32 __PPO__EditableMesh() { return STRUCT_OFFSET(USplitPolygonCommand, EditableMesh); }


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_10_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class USplitPolygonCommand>();

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSplitPolygonFromVertexCommand(); \
	friend struct Z_Construct_UClass_USplitPolygonFromVertexCommand_Statics; \
public: \
	DECLARE_CLASS(USplitPolygonFromVertexCommand, USplitPolygonCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USplitPolygonFromVertexCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_INCLASS \
private: \
	static void StaticRegisterNativesUSplitPolygonFromVertexCommand(); \
	friend struct Z_Construct_UClass_USplitPolygonFromVertexCommand_Statics; \
public: \
	DECLARE_CLASS(USplitPolygonFromVertexCommand, USplitPolygonCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USplitPolygonFromVertexCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USplitPolygonFromVertexCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USplitPolygonFromVertexCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USplitPolygonFromVertexCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USplitPolygonFromVertexCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USplitPolygonFromVertexCommand(USplitPolygonFromVertexCommand&&); \
	NO_API USplitPolygonFromVertexCommand(const USplitPolygonFromVertexCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USplitPolygonFromVertexCommand() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USplitPolygonFromVertexCommand(USplitPolygonFromVertexCommand&&); \
	NO_API USplitPolygonFromVertexCommand(const USplitPolygonFromVertexCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USplitPolygonFromVertexCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USplitPolygonFromVertexCommand); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USplitPolygonFromVertexCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_56_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_59_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class USplitPolygonFromVertexCommand>();

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSplitPolygonFromEdgeCommand(); \
	friend struct Z_Construct_UClass_USplitPolygonFromEdgeCommand_Statics; \
public: \
	DECLARE_CLASS(USplitPolygonFromEdgeCommand, USplitPolygonCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USplitPolygonFromEdgeCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_INCLASS \
private: \
	static void StaticRegisterNativesUSplitPolygonFromEdgeCommand(); \
	friend struct Z_Construct_UClass_USplitPolygonFromEdgeCommand_Statics; \
public: \
	DECLARE_CLASS(USplitPolygonFromEdgeCommand, USplitPolygonCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USplitPolygonFromEdgeCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USplitPolygonFromEdgeCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USplitPolygonFromEdgeCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USplitPolygonFromEdgeCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USplitPolygonFromEdgeCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USplitPolygonFromEdgeCommand(USplitPolygonFromEdgeCommand&&); \
	NO_API USplitPolygonFromEdgeCommand(const USplitPolygonFromEdgeCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USplitPolygonFromEdgeCommand() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USplitPolygonFromEdgeCommand(USplitPolygonFromEdgeCommand&&); \
	NO_API USplitPolygonFromEdgeCommand(const USplitPolygonFromEdgeCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USplitPolygonFromEdgeCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USplitPolygonFromEdgeCommand); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USplitPolygonFromEdgeCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_74_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_77_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class USplitPolygonFromEdgeCommand>();

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSplitPolygonFromPolygonCommand(); \
	friend struct Z_Construct_UClass_USplitPolygonFromPolygonCommand_Statics; \
public: \
	DECLARE_CLASS(USplitPolygonFromPolygonCommand, USplitPolygonCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USplitPolygonFromPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_INCLASS \
private: \
	static void StaticRegisterNativesUSplitPolygonFromPolygonCommand(); \
	friend struct Z_Construct_UClass_USplitPolygonFromPolygonCommand_Statics; \
public: \
	DECLARE_CLASS(USplitPolygonFromPolygonCommand, USplitPolygonCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USplitPolygonFromPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USplitPolygonFromPolygonCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USplitPolygonFromPolygonCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USplitPolygonFromPolygonCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USplitPolygonFromPolygonCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USplitPolygonFromPolygonCommand(USplitPolygonFromPolygonCommand&&); \
	NO_API USplitPolygonFromPolygonCommand(const USplitPolygonFromPolygonCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USplitPolygonFromPolygonCommand() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USplitPolygonFromPolygonCommand(USplitPolygonFromPolygonCommand&&); \
	NO_API USplitPolygonFromPolygonCommand(const USplitPolygonFromPolygonCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USplitPolygonFromPolygonCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USplitPolygonFromPolygonCommand); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USplitPolygonFromPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_92_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h_95_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class USplitPolygonFromPolygonCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_SplitPolygon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
