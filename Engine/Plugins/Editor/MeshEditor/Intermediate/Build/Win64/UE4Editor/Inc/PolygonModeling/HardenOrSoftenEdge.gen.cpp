// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/HardenOrSoftenEdge.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHardenOrSoftenEdge() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UHardenEdgeCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UHardenEdgeCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USoftenEdgeCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USoftenEdgeCommand();
// End Cross Module References
	void UHardenEdgeCommand::StaticRegisterNativesUHardenEdgeCommand()
	{
	}
	UClass* Z_Construct_UClass_UHardenEdgeCommand_NoRegister()
	{
		return UHardenEdgeCommand::StaticClass();
	}
	struct Z_Construct_UClass_UHardenEdgeCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHardenEdgeCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHardenEdgeCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Makes an edge hard */" },
		{ "IncludePath", "HardenOrSoftenEdge.h" },
		{ "ModuleRelativePath", "Public/HardenOrSoftenEdge.h" },
		{ "ToolTip", "Makes an edge hard" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHardenEdgeCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHardenEdgeCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHardenEdgeCommand_Statics::ClassParams = {
		&UHardenEdgeCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UHardenEdgeCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHardenEdgeCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHardenEdgeCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHardenEdgeCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHardenEdgeCommand, 2724314244);
	template<> POLYGONMODELING_API UClass* StaticClass<UHardenEdgeCommand>()
	{
		return UHardenEdgeCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHardenEdgeCommand(Z_Construct_UClass_UHardenEdgeCommand, &UHardenEdgeCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UHardenEdgeCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHardenEdgeCommand);
	void USoftenEdgeCommand::StaticRegisterNativesUSoftenEdgeCommand()
	{
	}
	UClass* Z_Construct_UClass_USoftenEdgeCommand_NoRegister()
	{
		return USoftenEdgeCommand::StaticClass();
	}
	struct Z_Construct_UClass_USoftenEdgeCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoftenEdgeCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoftenEdgeCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Makes an edge soft */" },
		{ "IncludePath", "HardenOrSoftenEdge.h" },
		{ "ModuleRelativePath", "Public/HardenOrSoftenEdge.h" },
		{ "ToolTip", "Makes an edge soft" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoftenEdgeCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoftenEdgeCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoftenEdgeCommand_Statics::ClassParams = {
		&USoftenEdgeCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoftenEdgeCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoftenEdgeCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoftenEdgeCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoftenEdgeCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoftenEdgeCommand, 2954407753);
	template<> POLYGONMODELING_API UClass* StaticClass<USoftenEdgeCommand>()
	{
		return USoftenEdgeCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoftenEdgeCommand(Z_Construct_UClass_USoftenEdgeCommand, &USoftenEdgeCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("USoftenEdgeCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoftenEdgeCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
