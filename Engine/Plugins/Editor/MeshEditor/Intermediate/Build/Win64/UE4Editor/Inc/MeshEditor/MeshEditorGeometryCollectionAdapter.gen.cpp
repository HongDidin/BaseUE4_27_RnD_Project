// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/MeshEditorGeometryCollectionAdapter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshEditorGeometryCollectionAdapter() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter();
	EDITABLEMESH_API UClass* Z_Construct_UClass_UEditableMeshAdapter();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
	MESHEDITOR_API UClass* Z_Construct_UClass_UWireframeMesh_NoRegister();
// End Cross Module References
	void UMeshEditorGeometryCollectionAdapter::StaticRegisterNativesUMeshEditorGeometryCollectionAdapter()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_NoRegister()
	{
		return UMeshEditorGeometryCollectionAdapter::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WireframeMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WireframeMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditableMeshAdapter,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshEditorGeometryCollectionAdapter.h" },
		{ "ModuleRelativePath", "MeshEditorGeometryCollectionAdapter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::NewProp_WireframeMesh_MetaData[] = {
		{ "Comment", "/** The wireframe mesh asset we're representing */" },
		{ "ModuleRelativePath", "MeshEditorGeometryCollectionAdapter.h" },
		{ "ToolTip", "The wireframe mesh asset we're representing" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::NewProp_WireframeMesh = { "WireframeMesh", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorGeometryCollectionAdapter, WireframeMesh), Z_Construct_UClass_UWireframeMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::NewProp_WireframeMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::NewProp_WireframeMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::NewProp_WireframeMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorGeometryCollectionAdapter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::ClassParams = {
		&UMeshEditorGeometryCollectionAdapter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorGeometryCollectionAdapter, 824630229);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorGeometryCollectionAdapter>()
	{
		return UMeshEditorGeometryCollectionAdapter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorGeometryCollectionAdapter(Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter, &UMeshEditorGeometryCollectionAdapter::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorGeometryCollectionAdapter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorGeometryCollectionAdapter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
