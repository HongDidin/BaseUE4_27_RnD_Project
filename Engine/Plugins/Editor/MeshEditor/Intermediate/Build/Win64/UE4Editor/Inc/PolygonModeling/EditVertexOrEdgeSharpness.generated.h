// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_EditVertexOrEdgeSharpness_generated_h
#error "EditVertexOrEdgeSharpness.generated.h already included, missing '#pragma once' in EditVertexOrEdgeSharpness.h"
#endif
#define POLYGONMODELING_EditVertexOrEdgeSharpness_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditVertexCornerSharpnessCommand(); \
	friend struct Z_Construct_UClass_UEditVertexCornerSharpnessCommand_Statics; \
public: \
	DECLARE_CLASS(UEditVertexCornerSharpnessCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UEditVertexCornerSharpnessCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUEditVertexCornerSharpnessCommand(); \
	friend struct Z_Construct_UClass_UEditVertexCornerSharpnessCommand_Statics; \
public: \
	DECLARE_CLASS(UEditVertexCornerSharpnessCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UEditVertexCornerSharpnessCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditVertexCornerSharpnessCommand(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditVertexCornerSharpnessCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditVertexCornerSharpnessCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditVertexCornerSharpnessCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditVertexCornerSharpnessCommand(UEditVertexCornerSharpnessCommand&&); \
	NO_API UEditVertexCornerSharpnessCommand(const UEditVertexCornerSharpnessCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditVertexCornerSharpnessCommand(UEditVertexCornerSharpnessCommand&&); \
	NO_API UEditVertexCornerSharpnessCommand(const UEditVertexCornerSharpnessCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditVertexCornerSharpnessCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditVertexCornerSharpnessCommand); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEditVertexCornerSharpnessCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_10_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class UEditVertexCornerSharpnessCommand>();

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditEdgeCreaseSharpnessCommand(); \
	friend struct Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_Statics; \
public: \
	DECLARE_CLASS(UEditEdgeCreaseSharpnessCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UEditEdgeCreaseSharpnessCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_INCLASS \
private: \
	static void StaticRegisterNativesUEditEdgeCreaseSharpnessCommand(); \
	friend struct Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_Statics; \
public: \
	DECLARE_CLASS(UEditEdgeCreaseSharpnessCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UEditEdgeCreaseSharpnessCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditEdgeCreaseSharpnessCommand(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditEdgeCreaseSharpnessCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditEdgeCreaseSharpnessCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditEdgeCreaseSharpnessCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditEdgeCreaseSharpnessCommand(UEditEdgeCreaseSharpnessCommand&&); \
	NO_API UEditEdgeCreaseSharpnessCommand(const UEditEdgeCreaseSharpnessCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditEdgeCreaseSharpnessCommand(UEditEdgeCreaseSharpnessCommand&&); \
	NO_API UEditEdgeCreaseSharpnessCommand(const UEditEdgeCreaseSharpnessCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditEdgeCreaseSharpnessCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditEdgeCreaseSharpnessCommand); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEditEdgeCreaseSharpnessCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_36_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h_39_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class UEditEdgeCreaseSharpnessCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_EditVertexOrEdgeSharpness_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
