// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/RemoveVertex.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoveVertex() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_URemoveVertexCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_URemoveVertexCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void URemoveVertexCommand::StaticRegisterNativesURemoveVertexCommand()
	{
	}
	UClass* Z_Construct_UClass_URemoveVertexCommand_NoRegister()
	{
		return URemoveVertexCommand::StaticClass();
	}
	struct Z_Construct_UClass_URemoveVertexCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoveVertexCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveVertexCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Attempts to remove the selected vertex from the polygon it belongs to, keeping the polygon intact */" },
		{ "IncludePath", "RemoveVertex.h" },
		{ "ModuleRelativePath", "Public/RemoveVertex.h" },
		{ "ToolTip", "Attempts to remove the selected vertex from the polygon it belongs to, keeping the polygon intact" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoveVertexCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoveVertexCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoveVertexCommand_Statics::ClassParams = {
		&URemoveVertexCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoveVertexCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveVertexCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoveVertexCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoveVertexCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoveVertexCommand, 2171168283);
	template<> POLYGONMODELING_API UClass* StaticClass<URemoveVertexCommand>()
	{
		return URemoveVertexCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoveVertexCommand(Z_Construct_UClass_URemoveVertexCommand, &URemoveVertexCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("URemoveVertexCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoveVertexCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
