// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/SelectToggleGeometryCollectionCommand.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSelectToggleGeometryCollectionCommand() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_USelectToggleGeometryCollectionCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USelectToggleGeometryCollectionCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void USelectToggleGeometryCollectionCommand::StaticRegisterNativesUSelectToggleGeometryCollectionCommand()
	{
	}
	UClass* Z_Construct_UClass_USelectToggleGeometryCollectionCommand_NoRegister()
	{
		return USelectToggleGeometryCollectionCommand::StaticClass();
	}
	struct Z_Construct_UClass_USelectToggleGeometryCollectionCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectToggleGeometryCollectionCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectToggleGeometryCollectionCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Toggle Selection of chunks in mesh */" },
		{ "IncludePath", "SelectToggleGeometryCollectionCommand.h" },
		{ "ModuleRelativePath", "SelectToggleGeometryCollectionCommand.h" },
		{ "ToolTip", "Toggle Selection of chunks in mesh" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectToggleGeometryCollectionCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelectToggleGeometryCollectionCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelectToggleGeometryCollectionCommand_Statics::ClassParams = {
		&USelectToggleGeometryCollectionCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USelectToggleGeometryCollectionCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectToggleGeometryCollectionCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectToggleGeometryCollectionCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelectToggleGeometryCollectionCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelectToggleGeometryCollectionCommand, 3475555052);
	template<> POLYGONMODELING_API UClass* StaticClass<USelectToggleGeometryCollectionCommand>()
	{
		return USelectToggleGeometryCollectionCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelectToggleGeometryCollectionCommand(Z_Construct_UClass_USelectToggleGeometryCollectionCommand, &USelectToggleGeometryCollectionCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("USelectToggleGeometryCollectionCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectToggleGeometryCollectionCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
