// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/InsertEdgeLoop.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInsertEdgeLoop() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UInsertEdgeLoopCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UInsertEdgeLoopCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorEditCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UInsertEdgeLoopCommand::StaticRegisterNativesUInsertEdgeLoopCommand()
	{
	}
	UClass* Z_Construct_UClass_UInsertEdgeLoopCommand_NoRegister()
	{
		return UInsertEdgeLoopCommand::StaticClass();
	}
	struct Z_Construct_UClass_UInsertEdgeLoopCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInsertEdgeLoopCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorEditCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInsertEdgeLoopCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** With an edge selected, inserts a loop of edge perpendicular to that edge while dragging */" },
		{ "IncludePath", "InsertEdgeLoop.h" },
		{ "ModuleRelativePath", "Public/InsertEdgeLoop.h" },
		{ "ToolTip", "With an edge selected, inserts a loop of edge perpendicular to that edge while dragging" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInsertEdgeLoopCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInsertEdgeLoopCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInsertEdgeLoopCommand_Statics::ClassParams = {
		&UInsertEdgeLoopCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UInsertEdgeLoopCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInsertEdgeLoopCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInsertEdgeLoopCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInsertEdgeLoopCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInsertEdgeLoopCommand, 3065252048);
	template<> POLYGONMODELING_API UClass* StaticClass<UInsertEdgeLoopCommand>()
	{
		return UInsertEdgeLoopCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInsertEdgeLoopCommand(Z_Construct_UClass_UInsertEdgeLoopCommand, &UInsertEdgeLoopCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UInsertEdgeLoopCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInsertEdgeLoopCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
