// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHEDITOR_FractureToolComponent_generated_h
#error "FractureToolComponent.generated.h already included, missing '#pragma once' in FractureToolComponent.h"
#endif
#define MESHEDITOR_FractureToolComponent_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolComponent(); \
	friend struct Z_Construct_UClass_UFractureToolComponent_Statics; \
public: \
	DECLARE_CLASS(UFractureToolComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolComponent)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolComponent(); \
	friend struct Z_Construct_UClass_UFractureToolComponent_Statics; \
public: \
	DECLARE_CLASS(UFractureToolComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolComponent)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolComponent(UFractureToolComponent&&); \
	NO_API UFractureToolComponent(const UFractureToolComponent&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolComponent(UFractureToolComponent&&); \
	NO_API UFractureToolComponent(const UFractureToolComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolComponent)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_20_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UFractureToolComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_FractureToolComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
