// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/TessellatePolygon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTessellatePolygon() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UTessellatePolygonCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UTessellatePolygonCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UTessellatePolygonCommand::StaticRegisterNativesUTessellatePolygonCommand()
	{
	}
	UClass* Z_Construct_UClass_UTessellatePolygonCommand_NoRegister()
	{
		return UTessellatePolygonCommand::StaticClass();
	}
	struct Z_Construct_UClass_UTessellatePolygonCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTessellatePolygonCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTessellatePolygonCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Tessellates selected polygons into smaller polygons */" },
		{ "IncludePath", "TessellatePolygon.h" },
		{ "ModuleRelativePath", "Public/TessellatePolygon.h" },
		{ "ToolTip", "Tessellates selected polygons into smaller polygons" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTessellatePolygonCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTessellatePolygonCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTessellatePolygonCommand_Statics::ClassParams = {
		&UTessellatePolygonCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTessellatePolygonCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTessellatePolygonCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTessellatePolygonCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTessellatePolygonCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTessellatePolygonCommand, 2877393248);
	template<> POLYGONMODELING_API UClass* StaticClass<UTessellatePolygonCommand>()
	{
		return UTessellatePolygonCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTessellatePolygonCommand(Z_Construct_UClass_UTessellatePolygonCommand, &UTessellatePolygonCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UTessellatePolygonCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTessellatePolygonCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
