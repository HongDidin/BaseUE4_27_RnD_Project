// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/UnHideAllGeometryCollectionCommand.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnHideAllGeometryCollectionCommand() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UUnHideAllGeometryCollectionCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UUnHideAllGeometryCollectionCommand::StaticRegisterNativesUUnHideAllGeometryCollectionCommand()
	{
	}
	UClass* Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_NoRegister()
	{
		return UUnHideAllGeometryCollectionCommand::StaticClass();
	}
	struct Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Select All chunks in mesh */" },
		{ "IncludePath", "UnHideAllGeometryCollectionCommand.h" },
		{ "ModuleRelativePath", "UnHideAllGeometryCollectionCommand.h" },
		{ "ToolTip", "Select All chunks in mesh" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUnHideAllGeometryCollectionCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_Statics::ClassParams = {
		&UUnHideAllGeometryCollectionCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUnHideAllGeometryCollectionCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUnHideAllGeometryCollectionCommand, 2736279618);
	template<> POLYGONMODELING_API UClass* StaticClass<UUnHideAllGeometryCollectionCommand>()
	{
		return UUnHideAllGeometryCollectionCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUnHideAllGeometryCollectionCommand(Z_Construct_UClass_UUnHideAllGeometryCollectionCommand, &UUnHideAllGeometryCollectionCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UUnHideAllGeometryCollectionCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUnHideAllGeometryCollectionCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
