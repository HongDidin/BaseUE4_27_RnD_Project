// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/UnifyNormals.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnifyNormals() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UUnifyNormalsCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UUnifyNormalsCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UUnifyNormalsCommand::StaticRegisterNativesUUnifyNormalsCommand()
	{
	}
	UClass* Z_Construct_UClass_UUnifyNormalsCommand_NoRegister()
	{
		return UUnifyNormalsCommand::StaticClass();
	}
	struct Z_Construct_UClass_UUnifyNormalsCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUnifyNormalsCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnifyNormalsCommand_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "UnifyNormals.h" },
		{ "ModuleRelativePath", "Public/UnifyNormals.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUnifyNormalsCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUnifyNormalsCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUnifyNormalsCommand_Statics::ClassParams = {
		&UUnifyNormalsCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUnifyNormalsCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUnifyNormalsCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUnifyNormalsCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUnifyNormalsCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUnifyNormalsCommand, 942814407);
	template<> POLYGONMODELING_API UClass* StaticClass<UUnifyNormalsCommand>()
	{
		return UUnifyNormalsCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUnifyNormalsCommand(Z_Construct_UClass_UUnifyNormalsCommand, &UUnifyNormalsCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UUnifyNormalsCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUnifyNormalsCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
