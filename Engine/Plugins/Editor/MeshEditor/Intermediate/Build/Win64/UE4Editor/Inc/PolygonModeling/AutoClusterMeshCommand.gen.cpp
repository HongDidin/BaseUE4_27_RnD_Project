// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/AutoClusterMeshCommand.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAutoClusterMeshCommand() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UAutoClusterMeshCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UAutoClusterMeshCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UAutoClusterMeshCommand::StaticRegisterNativesUAutoClusterMeshCommand()
	{
	}
	UClass* Z_Construct_UClass_UAutoClusterMeshCommand_NoRegister()
	{
		return UAutoClusterMeshCommand::StaticClass();
	}
	struct Z_Construct_UClass_UAutoClusterMeshCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAutoClusterMeshCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAutoClusterMeshCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Performs clustering of the currently selected geometry collection bones */" },
		{ "IncludePath", "AutoClusterMeshCommand.h" },
		{ "ModuleRelativePath", "AutoClusterMeshCommand.h" },
		{ "ToolTip", "Performs clustering of the currently selected geometry collection bones" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAutoClusterMeshCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAutoClusterMeshCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAutoClusterMeshCommand_Statics::ClassParams = {
		&UAutoClusterMeshCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAutoClusterMeshCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAutoClusterMeshCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAutoClusterMeshCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAutoClusterMeshCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAutoClusterMeshCommand, 2735781608);
	template<> POLYGONMODELING_API UClass* StaticClass<UAutoClusterMeshCommand>()
	{
		return UAutoClusterMeshCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAutoClusterMeshCommand(Z_Construct_UClass_UAutoClusterMeshCommand, &UAutoClusterMeshCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UAutoClusterMeshCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAutoClusterMeshCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
