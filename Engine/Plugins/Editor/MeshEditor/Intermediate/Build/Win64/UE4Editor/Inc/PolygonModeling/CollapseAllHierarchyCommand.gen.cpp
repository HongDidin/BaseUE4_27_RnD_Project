// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/CollapseAllHierarchyCommand.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCollapseAllHierarchyCommand() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UCollapseAllHierarchyCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UCollapseAllHierarchyCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UCollapseAllHierarchyCommand::StaticRegisterNativesUCollapseAllHierarchyCommand()
	{
	}
	UClass* Z_Construct_UClass_UCollapseAllHierarchyCommand_NoRegister()
	{
		return UCollapseAllHierarchyCommand::StaticClass();
	}
	struct Z_Construct_UClass_UCollapseAllHierarchyCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCollapseAllHierarchyCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCollapseAllHierarchyCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Performs merging of the currently selected meshes */" },
		{ "IncludePath", "CollapseAllHierarchyCommand.h" },
		{ "ModuleRelativePath", "CollapseAllHierarchyCommand.h" },
		{ "ToolTip", "Performs merging of the currently selected meshes" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCollapseAllHierarchyCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCollapseAllHierarchyCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCollapseAllHierarchyCommand_Statics::ClassParams = {
		&UCollapseAllHierarchyCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCollapseAllHierarchyCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCollapseAllHierarchyCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCollapseAllHierarchyCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCollapseAllHierarchyCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCollapseAllHierarchyCommand, 3039264117);
	template<> POLYGONMODELING_API UClass* StaticClass<UCollapseAllHierarchyCommand>()
	{
		return UCollapseAllHierarchyCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCollapseAllHierarchyCommand(Z_Construct_UClass_UCollapseAllHierarchyCommand, &UCollapseAllHierarchyCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UCollapseAllHierarchyCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCollapseAllHierarchyCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
