// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_ExtendVertex_generated_h
#error "ExtendVertex.generated.h already included, missing '#pragma once' in ExtendVertex.h"
#endif
#define POLYGONMODELING_ExtendVertex_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUExtendVertexCommand(); \
	friend struct Z_Construct_UClass_UExtendVertexCommand_Statics; \
public: \
	DECLARE_CLASS(UExtendVertexCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UExtendVertexCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUExtendVertexCommand(); \
	friend struct Z_Construct_UClass_UExtendVertexCommand_Statics; \
public: \
	DECLARE_CLASS(UExtendVertexCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UExtendVertexCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UExtendVertexCommand(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UExtendVertexCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExtendVertexCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExtendVertexCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExtendVertexCommand(UExtendVertexCommand&&); \
	NO_API UExtendVertexCommand(const UExtendVertexCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExtendVertexCommand(UExtendVertexCommand&&); \
	NO_API UExtendVertexCommand(const UExtendVertexCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExtendVertexCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExtendVertexCommand); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UExtendVertexCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_10_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class UExtendVertexCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtendVertex_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
