// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/SplitMesh.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSplitMesh() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitMeshCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitMeshCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void USplitMeshCommand::StaticRegisterNativesUSplitMeshCommand()
	{
	}
	UClass* Z_Construct_UClass_USplitMeshCommand_NoRegister()
	{
		return USplitMeshCommand::StaticClass();
	}
	struct Z_Construct_UClass_USplitMeshCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USplitMeshCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USplitMeshCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Attempts to split the mesh into two from a selected plane */" },
		{ "IncludePath", "SplitMesh.h" },
		{ "ModuleRelativePath", "SplitMesh.h" },
		{ "ToolTip", "Attempts to split the mesh into two from a selected plane" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USplitMeshCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USplitMeshCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USplitMeshCommand_Statics::ClassParams = {
		&USplitMeshCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USplitMeshCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USplitMeshCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USplitMeshCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USplitMeshCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USplitMeshCommand, 1949283601);
	template<> POLYGONMODELING_API UClass* StaticClass<USplitMeshCommand>()
	{
		return USplitMeshCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USplitMeshCommand(Z_Construct_UClass_USplitMeshCommand, &USplitMeshCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("USplitMeshCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USplitMeshCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
