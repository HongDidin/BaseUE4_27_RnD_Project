// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_SelectAllInClusterCommand_generated_h
#error "SelectAllInClusterCommand.generated.h already included, missing '#pragma once' in SelectAllInClusterCommand.h"
#endif
#define POLYGONMODELING_SelectAllInClusterCommand_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelectAllInClusterCommand(); \
	friend struct Z_Construct_UClass_USelectAllInClusterCommand_Statics; \
public: \
	DECLARE_CLASS(USelectAllInClusterCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USelectAllInClusterCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUSelectAllInClusterCommand(); \
	friend struct Z_Construct_UClass_USelectAllInClusterCommand_Statics; \
public: \
	DECLARE_CLASS(USelectAllInClusterCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USelectAllInClusterCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectAllInClusterCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectAllInClusterCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectAllInClusterCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectAllInClusterCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectAllInClusterCommand(USelectAllInClusterCommand&&); \
	NO_API USelectAllInClusterCommand(const USelectAllInClusterCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectAllInClusterCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectAllInClusterCommand(USelectAllInClusterCommand&&); \
	NO_API USelectAllInClusterCommand(const USelectAllInClusterCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectAllInClusterCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectAllInClusterCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectAllInClusterCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_12_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h_16_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class USelectAllInClusterCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllInClusterCommand_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
