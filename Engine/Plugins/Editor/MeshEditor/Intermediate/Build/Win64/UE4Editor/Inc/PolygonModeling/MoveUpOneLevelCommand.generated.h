// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_MoveUpOneLevelCommand_generated_h
#error "MoveUpOneLevelCommand.generated.h already included, missing '#pragma once' in MoveUpOneLevelCommand.h"
#endif
#define POLYGONMODELING_MoveUpOneLevelCommand_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoveUpOneLevelCommand(); \
	friend struct Z_Construct_UClass_UMoveUpOneLevelCommand_Statics; \
public: \
	DECLARE_CLASS(UMoveUpOneLevelCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UMoveUpOneLevelCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUMoveUpOneLevelCommand(); \
	friend struct Z_Construct_UClass_UMoveUpOneLevelCommand_Statics; \
public: \
	DECLARE_CLASS(UMoveUpOneLevelCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UMoveUpOneLevelCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoveUpOneLevelCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoveUpOneLevelCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoveUpOneLevelCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoveUpOneLevelCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoveUpOneLevelCommand(UMoveUpOneLevelCommand&&); \
	NO_API UMoveUpOneLevelCommand(const UMoveUpOneLevelCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoveUpOneLevelCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoveUpOneLevelCommand(UMoveUpOneLevelCommand&&); \
	NO_API UMoveUpOneLevelCommand(const UMoveUpOneLevelCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoveUpOneLevelCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoveUpOneLevelCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoveUpOneLevelCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_13_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h_17_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class UMoveUpOneLevelCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_MoveUpOneLevelCommand_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
