// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/RemoveSelectedGeometryCollectionCommand.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoveSelectedGeometryCollectionCommand() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void URemoveSelectedGeometryCollectionCommand::StaticRegisterNativesURemoveSelectedGeometryCollectionCommand()
	{
	}
	UClass* Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand_NoRegister()
	{
		return URemoveSelectedGeometryCollectionCommand::StaticClass();
	}
	struct Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Select All chunks in mesh */" },
		{ "IncludePath", "RemoveSelectedGeometryCollectionCommand.h" },
		{ "ModuleRelativePath", "RemoveSelectedGeometryCollectionCommand.h" },
		{ "ToolTip", "Select All chunks in mesh" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoveSelectedGeometryCollectionCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand_Statics::ClassParams = {
		&URemoveSelectedGeometryCollectionCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoveSelectedGeometryCollectionCommand, 3264511054);
	template<> POLYGONMODELING_API UClass* StaticClass<URemoveSelectedGeometryCollectionCommand>()
	{
		return URemoveSelectedGeometryCollectionCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoveSelectedGeometryCollectionCommand(Z_Construct_UClass_URemoveSelectedGeometryCollectionCommand, &URemoveSelectedGeometryCollectionCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("URemoveSelectedGeometryCollectionCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoveSelectedGeometryCollectionCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
