// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/EditVertexOrEdgeSharpness.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditVertexOrEdgeSharpness() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UEditVertexCornerSharpnessCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UEditVertexCornerSharpnessCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorEditCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand();
// End Cross Module References
	void UEditVertexCornerSharpnessCommand::StaticRegisterNativesUEditVertexCornerSharpnessCommand()
	{
	}
	UClass* Z_Construct_UClass_UEditVertexCornerSharpnessCommand_NoRegister()
	{
		return UEditVertexCornerSharpnessCommand::StaticClass();
	}
	struct Z_Construct_UClass_UEditVertexCornerSharpnessCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditVertexCornerSharpnessCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorEditCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditVertexCornerSharpnessCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** For subdivision meshes, edits how sharp a vertex corner is by dragging in space */" },
		{ "IncludePath", "EditVertexOrEdgeSharpness.h" },
		{ "ModuleRelativePath", "Public/EditVertexOrEdgeSharpness.h" },
		{ "ToolTip", "For subdivision meshes, edits how sharp a vertex corner is by dragging in space" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditVertexCornerSharpnessCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditVertexCornerSharpnessCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditVertexCornerSharpnessCommand_Statics::ClassParams = {
		&UEditVertexCornerSharpnessCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEditVertexCornerSharpnessCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditVertexCornerSharpnessCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditVertexCornerSharpnessCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditVertexCornerSharpnessCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditVertexCornerSharpnessCommand, 3977452244);
	template<> POLYGONMODELING_API UClass* StaticClass<UEditVertexCornerSharpnessCommand>()
	{
		return UEditVertexCornerSharpnessCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditVertexCornerSharpnessCommand(Z_Construct_UClass_UEditVertexCornerSharpnessCommand, &UEditVertexCornerSharpnessCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UEditVertexCornerSharpnessCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditVertexCornerSharpnessCommand);
	void UEditEdgeCreaseSharpnessCommand::StaticRegisterNativesUEditEdgeCreaseSharpnessCommand()
	{
	}
	UClass* Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_NoRegister()
	{
		return UEditEdgeCreaseSharpnessCommand::StaticClass();
	}
	struct Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorEditCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** For subdivision meshes, edits how sharp an edge crease is by dragging in space */" },
		{ "IncludePath", "EditVertexOrEdgeSharpness.h" },
		{ "ModuleRelativePath", "Public/EditVertexOrEdgeSharpness.h" },
		{ "ToolTip", "For subdivision meshes, edits how sharp an edge crease is by dragging in space" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditEdgeCreaseSharpnessCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_Statics::ClassParams = {
		&UEditEdgeCreaseSharpnessCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditEdgeCreaseSharpnessCommand, 3265158505);
	template<> POLYGONMODELING_API UClass* StaticClass<UEditEdgeCreaseSharpnessCommand>()
	{
		return UEditEdgeCreaseSharpnessCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditEdgeCreaseSharpnessCommand(Z_Construct_UClass_UEditEdgeCreaseSharpnessCommand, &UEditEdgeCreaseSharpnessCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UEditEdgeCreaseSharpnessCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditEdgeCreaseSharpnessCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
