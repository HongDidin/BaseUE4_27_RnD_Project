// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/ExtendVertex.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExtendVertex() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UExtendVertexCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UExtendVertexCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorEditCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UExtendVertexCommand::StaticRegisterNativesUExtendVertexCommand()
	{
	}
	UClass* Z_Construct_UClass_UExtendVertexCommand_NoRegister()
	{
		return UExtendVertexCommand::StaticClass();
	}
	struct Z_Construct_UClass_UExtendVertexCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UExtendVertexCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorEditCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExtendVertexCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Extend a vertex by making a copy of it, creating new polygons to join the geometry together */" },
		{ "IncludePath", "ExtendVertex.h" },
		{ "ModuleRelativePath", "Public/ExtendVertex.h" },
		{ "ToolTip", "Extend a vertex by making a copy of it, creating new polygons to join the geometry together" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UExtendVertexCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UExtendVertexCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UExtendVertexCommand_Statics::ClassParams = {
		&UExtendVertexCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UExtendVertexCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UExtendVertexCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UExtendVertexCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UExtendVertexCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UExtendVertexCommand, 2658223121);
	template<> POLYGONMODELING_API UClass* StaticClass<UExtendVertexCommand>()
	{
		return UExtendVertexCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UExtendVertexCommand(Z_Construct_UClass_UExtendVertexCommand, &UExtendVertexCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UExtendVertexCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UExtendVertexCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
