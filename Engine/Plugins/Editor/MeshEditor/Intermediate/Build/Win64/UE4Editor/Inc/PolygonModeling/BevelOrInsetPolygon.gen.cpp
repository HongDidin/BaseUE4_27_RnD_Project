// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/BevelOrInsetPolygon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBevelOrInsetPolygon() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UBevelPolygonCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UBevelPolygonCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorEditCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UInsetPolygonCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UInsetPolygonCommand();
// End Cross Module References
	void UBevelPolygonCommand::StaticRegisterNativesUBevelPolygonCommand()
	{
	}
	UClass* Z_Construct_UClass_UBevelPolygonCommand_NoRegister()
	{
		return UBevelPolygonCommand::StaticClass();
	}
	struct Z_Construct_UClass_UBevelPolygonCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBevelPolygonCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorEditCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBevelPolygonCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Adds a beveled edge to an existing polygon */" },
		{ "IncludePath", "BevelOrInsetPolygon.h" },
		{ "ModuleRelativePath", "Public/BevelOrInsetPolygon.h" },
		{ "ToolTip", "Adds a beveled edge to an existing polygon" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBevelPolygonCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBevelPolygonCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBevelPolygonCommand_Statics::ClassParams = {
		&UBevelPolygonCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBevelPolygonCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBevelPolygonCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBevelPolygonCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBevelPolygonCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBevelPolygonCommand, 111009576);
	template<> POLYGONMODELING_API UClass* StaticClass<UBevelPolygonCommand>()
	{
		return UBevelPolygonCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBevelPolygonCommand(Z_Construct_UClass_UBevelPolygonCommand, &UBevelPolygonCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UBevelPolygonCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBevelPolygonCommand);
	void UInsetPolygonCommand::StaticRegisterNativesUInsetPolygonCommand()
	{
	}
	UClass* Z_Construct_UClass_UInsetPolygonCommand_NoRegister()
	{
		return UInsetPolygonCommand::StaticClass();
	}
	struct Z_Construct_UClass_UInsetPolygonCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInsetPolygonCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorEditCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInsetPolygonCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Adds a new polygon on the inside of an existing polygon, allowing the user to drag to set exactly where it should be placed. */" },
		{ "IncludePath", "BevelOrInsetPolygon.h" },
		{ "ModuleRelativePath", "Public/BevelOrInsetPolygon.h" },
		{ "ToolTip", "Adds a new polygon on the inside of an existing polygon, allowing the user to drag to set exactly where it should be placed." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInsetPolygonCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UInsetPolygonCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInsetPolygonCommand_Statics::ClassParams = {
		&UInsetPolygonCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UInsetPolygonCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInsetPolygonCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInsetPolygonCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInsetPolygonCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInsetPolygonCommand, 3754850397);
	template<> POLYGONMODELING_API UClass* StaticClass<UInsetPolygonCommand>()
	{
		return UInsetPolygonCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInsetPolygonCommand(Z_Construct_UClass_UInsetPolygonCommand, &UInsetPolygonCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UInsetPolygonCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInsetPolygonCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
