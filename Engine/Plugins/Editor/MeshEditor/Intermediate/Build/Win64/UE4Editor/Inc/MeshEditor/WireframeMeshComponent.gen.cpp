// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/Public/WireframeMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWireframeMeshComponent() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UWireframeMesh_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UWireframeMesh();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
	MESHEDITOR_API UClass* Z_Construct_UClass_UWireframeMeshComponent_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UWireframeMeshComponent();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent();
// End Cross Module References
	void UWireframeMesh::StaticRegisterNativesUWireframeMesh()
	{
	}
	UClass* Z_Construct_UClass_UWireframeMesh_NoRegister()
	{
		return UWireframeMesh::StaticClass();
	}
	struct Z_Construct_UClass_UWireframeMesh_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWireframeMesh_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWireframeMesh_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WireframeMeshComponent.h" },
		{ "ModuleRelativePath", "Public/WireframeMeshComponent.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWireframeMesh_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWireframeMesh>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWireframeMesh_Statics::ClassParams = {
		&UWireframeMesh::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWireframeMesh_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWireframeMesh_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWireframeMesh()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWireframeMesh_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWireframeMesh, 478228549);
	template<> MESHEDITOR_API UClass* StaticClass<UWireframeMesh>()
	{
		return UWireframeMesh::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWireframeMesh(Z_Construct_UClass_UWireframeMesh, &UWireframeMesh::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UWireframeMesh"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWireframeMesh);
	void UWireframeMeshComponent::StaticRegisterNativesUWireframeMeshComponent()
	{
	}
	UClass* Z_Construct_UClass_UWireframeMeshComponent_NoRegister()
	{
		return UWireframeMeshComponent::StaticClass();
	}
	struct Z_Construct_UClass_UWireframeMeshComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WireframeMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WireframeMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWireframeMeshComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWireframeMeshComponent_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Mobility Trigger" },
		{ "IncludePath", "WireframeMeshComponent.h" },
		{ "ModuleRelativePath", "Public/WireframeMeshComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWireframeMeshComponent_Statics::NewProp_WireframeMesh_MetaData[] = {
		{ "Comment", "//~ Begin USceneComponent Interface.\n" },
		{ "ModuleRelativePath", "Public/WireframeMeshComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWireframeMeshComponent_Statics::NewProp_WireframeMesh = { "WireframeMesh", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWireframeMeshComponent, WireframeMesh), Z_Construct_UClass_UWireframeMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWireframeMeshComponent_Statics::NewProp_WireframeMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWireframeMeshComponent_Statics::NewProp_WireframeMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWireframeMeshComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWireframeMeshComponent_Statics::NewProp_WireframeMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWireframeMeshComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWireframeMeshComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWireframeMeshComponent_Statics::ClassParams = {
		&UWireframeMeshComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWireframeMeshComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWireframeMeshComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UWireframeMeshComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWireframeMeshComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWireframeMeshComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWireframeMeshComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWireframeMeshComponent, 3562676466);
	template<> MESHEDITOR_API UClass* StaticClass<UWireframeMeshComponent>()
	{
		return UWireframeMeshComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWireframeMeshComponent(Z_Construct_UClass_UWireframeMeshComponent, &UWireframeMeshComponent::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UWireframeMeshComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWireframeMeshComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
