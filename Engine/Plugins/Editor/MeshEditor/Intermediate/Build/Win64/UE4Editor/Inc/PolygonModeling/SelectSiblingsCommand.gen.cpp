// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/SelectSiblingsCommand.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSelectSiblingsCommand() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_USelectSiblingsCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USelectSiblingsCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void USelectSiblingsCommand::StaticRegisterNativesUSelectSiblingsCommand()
	{
	}
	UClass* Z_Construct_UClass_USelectSiblingsCommand_NoRegister()
	{
		return USelectSiblingsCommand::StaticClass();
	}
	struct Z_Construct_UClass_USelectSiblingsCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectSiblingsCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectSiblingsCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Additionally select the siblings of the selected node */" },
		{ "IncludePath", "SelectSiblingsCommand.h" },
		{ "ModuleRelativePath", "SelectSiblingsCommand.h" },
		{ "ToolTip", "Additionally select the siblings of the selected node" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectSiblingsCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelectSiblingsCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelectSiblingsCommand_Statics::ClassParams = {
		&USelectSiblingsCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USelectSiblingsCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectSiblingsCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectSiblingsCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelectSiblingsCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelectSiblingsCommand, 2072978384);
	template<> POLYGONMODELING_API UClass* StaticClass<USelectSiblingsCommand>()
	{
		return USelectSiblingsCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelectSiblingsCommand(Z_Construct_UClass_USelectSiblingsCommand, &USelectSiblingsCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("USelectSiblingsCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectSiblingsCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
