// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/MeshEditorMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshEditorMode() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorModeProxyObject_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorModeProxyObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
// End Cross Module References
	void UMeshEditorModeProxyObject::StaticRegisterNativesUMeshEditorModeProxyObject()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorModeProxyObject_NoRegister()
	{
		return UMeshEditorModeProxyObject::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorModeProxyObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorModeProxyObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorModeProxyObject_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshEditorMode.h" },
		{ "ModuleRelativePath", "MeshEditorMode.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorModeProxyObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorModeProxyObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorModeProxyObject_Statics::ClassParams = {
		&UMeshEditorModeProxyObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorModeProxyObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorModeProxyObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorModeProxyObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorModeProxyObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorModeProxyObject, 2552066017);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorModeProxyObject>()
	{
		return UMeshEditorModeProxyObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorModeProxyObject(Z_Construct_UClass_UMeshEditorModeProxyObject, &UMeshEditorModeProxyObject::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorModeProxyObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorModeProxyObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
