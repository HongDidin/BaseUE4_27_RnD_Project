// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_RemoveVertex_generated_h
#error "RemoveVertex.generated.h already included, missing '#pragma once' in RemoveVertex.h"
#endif
#define POLYGONMODELING_RemoveVertex_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoveVertexCommand(); \
	friend struct Z_Construct_UClass_URemoveVertexCommand_Statics; \
public: \
	DECLARE_CLASS(URemoveVertexCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(URemoveVertexCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_INCLASS \
private: \
	static void StaticRegisterNativesURemoveVertexCommand(); \
	friend struct Z_Construct_UClass_URemoveVertexCommand_Statics; \
public: \
	DECLARE_CLASS(URemoveVertexCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(URemoveVertexCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoveVertexCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoveVertexCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoveVertexCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoveVertexCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoveVertexCommand(URemoveVertexCommand&&); \
	NO_API URemoveVertexCommand(const URemoveVertexCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoveVertexCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoveVertexCommand(URemoveVertexCommand&&); \
	NO_API URemoveVertexCommand(const URemoveVertexCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoveVertexCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoveVertexCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoveVertexCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_10_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class URemoveVertexCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_RemoveVertex_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
