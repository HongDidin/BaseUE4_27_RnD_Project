// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHEDITOR_MeshEditorGeometryCollectionAdapter_generated_h
#error "MeshEditorGeometryCollectionAdapter.generated.h already included, missing '#pragma once' in MeshEditorGeometryCollectionAdapter.h"
#endif
#define MESHEDITOR_MeshEditorGeometryCollectionAdapter_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorGeometryCollectionAdapter(); \
	friend struct Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorGeometryCollectionAdapter, UEditableMeshAdapter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), MESHEDITOR_API) \
	DECLARE_SERIALIZER(UMeshEditorGeometryCollectionAdapter)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorGeometryCollectionAdapter(); \
	friend struct Z_Construct_UClass_UMeshEditorGeometryCollectionAdapter_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorGeometryCollectionAdapter, UEditableMeshAdapter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), MESHEDITOR_API) \
	DECLARE_SERIALIZER(UMeshEditorGeometryCollectionAdapter)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MESHEDITOR_API UMeshEditorGeometryCollectionAdapter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorGeometryCollectionAdapter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MESHEDITOR_API, UMeshEditorGeometryCollectionAdapter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorGeometryCollectionAdapter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MESHEDITOR_API UMeshEditorGeometryCollectionAdapter(UMeshEditorGeometryCollectionAdapter&&); \
	MESHEDITOR_API UMeshEditorGeometryCollectionAdapter(const UMeshEditorGeometryCollectionAdapter&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MESHEDITOR_API UMeshEditorGeometryCollectionAdapter(UMeshEditorGeometryCollectionAdapter&&); \
	MESHEDITOR_API UMeshEditorGeometryCollectionAdapter(const UMeshEditorGeometryCollectionAdapter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MESHEDITOR_API, UMeshEditorGeometryCollectionAdapter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorGeometryCollectionAdapter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshEditorGeometryCollectionAdapter)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__WireframeMesh() { return STRUCT_OFFSET(UMeshEditorGeometryCollectionAdapter, WireframeMesh); }


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_11_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorGeometryCollectionAdapter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorGeometryCollectionAdapter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
