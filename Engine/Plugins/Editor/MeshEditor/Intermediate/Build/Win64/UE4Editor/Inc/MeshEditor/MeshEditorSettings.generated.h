// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHEDITOR_MeshEditorSettings_generated_h
#error "MeshEditorSettings.generated.h already included, missing '#pragma once' in MeshEditorSettings.h"
#endif
#define MESHEDITOR_MeshEditorSettings_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorSettings(); \
	friend struct Z_Construct_UClass_UMeshEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorSettings(); \
	friend struct Z_Construct_UClass_UMeshEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorSettings(UMeshEditorSettings&&); \
	NO_API UMeshEditorSettings(const UMeshEditorSettings&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorSettings(UMeshEditorSettings&&); \
	NO_API UMeshEditorSettings(const UMeshEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshEditorSettings)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_11_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
