// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_HardenOrSoftenEdge_generated_h
#error "HardenOrSoftenEdge.generated.h already included, missing '#pragma once' in HardenOrSoftenEdge.h"
#endif
#define POLYGONMODELING_HardenOrSoftenEdge_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHardenEdgeCommand(); \
	friend struct Z_Construct_UClass_UHardenEdgeCommand_Statics; \
public: \
	DECLARE_CLASS(UHardenEdgeCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UHardenEdgeCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUHardenEdgeCommand(); \
	friend struct Z_Construct_UClass_UHardenEdgeCommand_Statics; \
public: \
	DECLARE_CLASS(UHardenEdgeCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UHardenEdgeCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHardenEdgeCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHardenEdgeCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHardenEdgeCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHardenEdgeCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHardenEdgeCommand(UHardenEdgeCommand&&); \
	NO_API UHardenEdgeCommand(const UHardenEdgeCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHardenEdgeCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHardenEdgeCommand(UHardenEdgeCommand&&); \
	NO_API UHardenEdgeCommand(const UHardenEdgeCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHardenEdgeCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHardenEdgeCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHardenEdgeCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_10_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class UHardenEdgeCommand>();

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoftenEdgeCommand(); \
	friend struct Z_Construct_UClass_USoftenEdgeCommand_Statics; \
public: \
	DECLARE_CLASS(USoftenEdgeCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USoftenEdgeCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUSoftenEdgeCommand(); \
	friend struct Z_Construct_UClass_USoftenEdgeCommand_Statics; \
public: \
	DECLARE_CLASS(USoftenEdgeCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USoftenEdgeCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoftenEdgeCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoftenEdgeCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoftenEdgeCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoftenEdgeCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoftenEdgeCommand(USoftenEdgeCommand&&); \
	NO_API USoftenEdgeCommand(const USoftenEdgeCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoftenEdgeCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoftenEdgeCommand(USoftenEdgeCommand&&); \
	NO_API USoftenEdgeCommand(const USoftenEdgeCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoftenEdgeCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoftenEdgeCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoftenEdgeCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_29_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class USoftenEdgeCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_HardenOrSoftenEdge_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
