// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/DeleteMeshElement.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDeleteMeshElement() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UDeleteMeshElementCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UDeleteMeshElementCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UDeleteMeshElementCommand::StaticRegisterNativesUDeleteMeshElementCommand()
	{
	}
	UClass* Z_Construct_UClass_UDeleteMeshElementCommand_NoRegister()
	{
		return UDeleteMeshElementCommand::StaticClass();
	}
	struct Z_Construct_UClass_UDeleteMeshElementCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDeleteMeshElementCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDeleteMeshElementCommand_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DeleteMeshElement.h" },
		{ "ModuleRelativePath", "Public/DeleteMeshElement.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDeleteMeshElementCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDeleteMeshElementCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDeleteMeshElementCommand_Statics::ClassParams = {
		&UDeleteMeshElementCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDeleteMeshElementCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDeleteMeshElementCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDeleteMeshElementCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDeleteMeshElementCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDeleteMeshElementCommand, 2436210118);
	template<> POLYGONMODELING_API UClass* StaticClass<UDeleteMeshElementCommand>()
	{
		return UDeleteMeshElementCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDeleteMeshElementCommand(Z_Construct_UClass_UDeleteMeshElementCommand, &UDeleteMeshElementCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UDeleteMeshElementCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDeleteMeshElementCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
