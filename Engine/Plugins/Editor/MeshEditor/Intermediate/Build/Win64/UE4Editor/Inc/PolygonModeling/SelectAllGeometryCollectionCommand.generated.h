// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_SelectAllGeometryCollectionCommand_generated_h
#error "SelectAllGeometryCollectionCommand.generated.h already included, missing '#pragma once' in SelectAllGeometryCollectionCommand.h"
#endif
#define POLYGONMODELING_SelectAllGeometryCollectionCommand_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelectAllGeometryCollectionCommand(); \
	friend struct Z_Construct_UClass_USelectAllGeometryCollectionCommand_Statics; \
public: \
	DECLARE_CLASS(USelectAllGeometryCollectionCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USelectAllGeometryCollectionCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUSelectAllGeometryCollectionCommand(); \
	friend struct Z_Construct_UClass_USelectAllGeometryCollectionCommand_Statics; \
public: \
	DECLARE_CLASS(USelectAllGeometryCollectionCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USelectAllGeometryCollectionCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectAllGeometryCollectionCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectAllGeometryCollectionCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectAllGeometryCollectionCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectAllGeometryCollectionCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectAllGeometryCollectionCommand(USelectAllGeometryCollectionCommand&&); \
	NO_API USelectAllGeometryCollectionCommand(const USelectAllGeometryCollectionCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectAllGeometryCollectionCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectAllGeometryCollectionCommand(USelectAllGeometryCollectionCommand&&); \
	NO_API USelectAllGeometryCollectionCommand(const USelectAllGeometryCollectionCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectAllGeometryCollectionCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectAllGeometryCollectionCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectAllGeometryCollectionCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_15_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h_19_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class USelectAllGeometryCollectionCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectAllGeometryCollectionCommand_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
