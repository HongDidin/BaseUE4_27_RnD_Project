// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/QuadrangulateMesh.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeQuadrangulateMesh() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UQuadrangulateMeshCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UQuadrangulateMeshCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UQuadrangulateMeshCommand::StaticRegisterNativesUQuadrangulateMeshCommand()
	{
	}
	UClass* Z_Construct_UClass_UQuadrangulateMeshCommand_NoRegister()
	{
		return UQuadrangulateMeshCommand::StaticClass();
	}
	struct Z_Construct_UClass_UQuadrangulateMeshCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UQuadrangulateMeshCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UQuadrangulateMeshCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Quadrangulates the currently selected mesh */" },
		{ "IncludePath", "QuadrangulateMesh.h" },
		{ "ModuleRelativePath", "Public/QuadrangulateMesh.h" },
		{ "ToolTip", "Quadrangulates the currently selected mesh" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UQuadrangulateMeshCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UQuadrangulateMeshCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UQuadrangulateMeshCommand_Statics::ClassParams = {
		&UQuadrangulateMeshCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UQuadrangulateMeshCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UQuadrangulateMeshCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UQuadrangulateMeshCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UQuadrangulateMeshCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UQuadrangulateMeshCommand, 1316914218);
	template<> POLYGONMODELING_API UClass* StaticClass<UQuadrangulateMeshCommand>()
	{
		return UQuadrangulateMeshCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UQuadrangulateMeshCommand(Z_Construct_UClass_UQuadrangulateMeshCommand, &UQuadrangulateMeshCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UQuadrangulateMeshCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UQuadrangulateMeshCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
