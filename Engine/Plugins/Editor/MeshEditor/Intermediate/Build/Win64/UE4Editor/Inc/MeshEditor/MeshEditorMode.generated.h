// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHEDITOR_MeshEditorMode_generated_h
#error "MeshEditorMode.generated.h already included, missing '#pragma once' in MeshEditorMode.h"
#endif
#define MESHEDITOR_MeshEditorMode_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorModeProxyObject(); \
	friend struct Z_Construct_UClass_UMeshEditorModeProxyObject_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorModeProxyObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorModeProxyObject)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorModeProxyObject(); \
	friend struct Z_Construct_UClass_UMeshEditorModeProxyObject_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorModeProxyObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorModeProxyObject)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorModeProxyObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorModeProxyObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorModeProxyObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorModeProxyObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorModeProxyObject(UMeshEditorModeProxyObject&&); \
	NO_API UMeshEditorModeProxyObject(const UMeshEditorModeProxyObject&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorModeProxyObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorModeProxyObject(UMeshEditorModeProxyObject&&); \
	NO_API UMeshEditorModeProxyObject(const UMeshEditorModeProxyObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorModeProxyObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorModeProxyObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorModeProxyObject)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_28_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorModeProxyObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
