// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHEDITOR_MeshEditorCommands_generated_h
#error "MeshEditorCommands.generated.h already included, missing '#pragma once' in MeshEditorCommands.h"
#endif
#define MESHEDITOR_MeshEditorCommands_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorCommand(); \
	friend struct Z_Construct_UClass_UMeshEditorCommand_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorCommand, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorCommand(); \
	friend struct Z_Construct_UClass_UMeshEditorCommand_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorCommand, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorCommand(UMeshEditorCommand&&); \
	NO_API UMeshEditorCommand(const UMeshEditorCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorCommand(UMeshEditorCommand&&); \
	NO_API UMeshEditorCommand(const UMeshEditorCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorCommand); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_12_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorCommand>();

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorInstantCommand(); \
	friend struct Z_Construct_UClass_UMeshEditorInstantCommand_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorInstantCommand, UMeshEditorCommand, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorInstantCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorInstantCommand(); \
	friend struct Z_Construct_UClass_UMeshEditorInstantCommand_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorInstantCommand, UMeshEditorCommand, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorInstantCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorInstantCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorInstantCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorInstantCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorInstantCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorInstantCommand(UMeshEditorInstantCommand&&); \
	NO_API UMeshEditorInstantCommand(const UMeshEditorInstantCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorInstantCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorInstantCommand(UMeshEditorInstantCommand&&); \
	NO_API UMeshEditorInstantCommand(const UMeshEditorInstantCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorInstantCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorInstantCommand); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorInstantCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_53_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_56_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorInstantCommand>();

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorEditCommand(); \
	friend struct Z_Construct_UClass_UMeshEditorEditCommand_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorEditCommand, UMeshEditorCommand, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorEditCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorEditCommand(); \
	friend struct Z_Construct_UClass_UMeshEditorEditCommand_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorEditCommand, UMeshEditorCommand, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorEditCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorEditCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorEditCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorEditCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorEditCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorEditCommand(UMeshEditorEditCommand&&); \
	NO_API UMeshEditorEditCommand(const UMeshEditorEditCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorEditCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorEditCommand(UMeshEditorEditCommand&&); \
	NO_API UMeshEditorEditCommand(const UMeshEditorEditCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorEditCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorEditCommand); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorEditCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__UndoText() { return STRUCT_OFFSET(UMeshEditorEditCommand, UndoText); } \
	FORCEINLINE static uint32 __PPO__bNeedsDraggingInitiated() { return STRUCT_OFFSET(UMeshEditorEditCommand, bNeedsDraggingInitiated); } \
	FORCEINLINE static uint32 __PPO__bNeedsHoverLocation() { return STRUCT_OFFSET(UMeshEditorEditCommand, bNeedsHoverLocation); }


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_74_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_77_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorEditCommand>();

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorCommandList(); \
	friend struct Z_Construct_UClass_UMeshEditorCommandList_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorCommandList, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorCommandList)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorCommandList(); \
	friend struct Z_Construct_UClass_UMeshEditorCommandList_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorCommandList, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorCommandList)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorCommandList(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorCommandList) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorCommandList); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorCommandList); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorCommandList(UMeshEditorCommandList&&); \
	NO_API UMeshEditorCommandList(const UMeshEditorCommandList&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorCommandList(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorCommandList(UMeshEditorCommandList&&); \
	NO_API UMeshEditorCommandList(const UMeshEditorCommandList&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorCommandList); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorCommandList); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorCommandList)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_256_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h_259_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorCommandList>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorCommands_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
