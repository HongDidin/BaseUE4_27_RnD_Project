// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_AutoClusterMeshCommand_generated_h
#error "AutoClusterMeshCommand.generated.h already included, missing '#pragma once' in AutoClusterMeshCommand.h"
#endif
#define POLYGONMODELING_AutoClusterMeshCommand_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAutoClusterMeshCommand(); \
	friend struct Z_Construct_UClass_UAutoClusterMeshCommand_Statics; \
public: \
	DECLARE_CLASS(UAutoClusterMeshCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UAutoClusterMeshCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUAutoClusterMeshCommand(); \
	friend struct Z_Construct_UClass_UAutoClusterMeshCommand_Statics; \
public: \
	DECLARE_CLASS(UAutoClusterMeshCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UAutoClusterMeshCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAutoClusterMeshCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAutoClusterMeshCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAutoClusterMeshCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAutoClusterMeshCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAutoClusterMeshCommand(UAutoClusterMeshCommand&&); \
	NO_API UAutoClusterMeshCommand(const UAutoClusterMeshCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAutoClusterMeshCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAutoClusterMeshCommand(UAutoClusterMeshCommand&&); \
	NO_API UAutoClusterMeshCommand(const UAutoClusterMeshCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAutoClusterMeshCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAutoClusterMeshCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAutoClusterMeshCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_14_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h_18_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class UAutoClusterMeshCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_AutoClusterMeshCommand_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
