// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/MoveUpOneLevelCommand.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoveUpOneLevelCommand() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UMoveUpOneLevelCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UMoveUpOneLevelCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UMoveUpOneLevelCommand::StaticRegisterNativesUMoveUpOneLevelCommand()
	{
	}
	UClass* Z_Construct_UClass_UMoveUpOneLevelCommand_NoRegister()
	{
		return UMoveUpOneLevelCommand::StaticClass();
	}
	struct Z_Construct_UClass_UMoveUpOneLevelCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoveUpOneLevelCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoveUpOneLevelCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Move node up one level */" },
		{ "IncludePath", "MoveUpOneLevelCommand.h" },
		{ "ModuleRelativePath", "MoveUpOneLevelCommand.h" },
		{ "ToolTip", "Move node up one level" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoveUpOneLevelCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoveUpOneLevelCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoveUpOneLevelCommand_Statics::ClassParams = {
		&UMoveUpOneLevelCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoveUpOneLevelCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoveUpOneLevelCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoveUpOneLevelCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoveUpOneLevelCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoveUpOneLevelCommand, 3880928486);
	template<> POLYGONMODELING_API UClass* StaticClass<UMoveUpOneLevelCommand>()
	{
		return UMoveUpOneLevelCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoveUpOneLevelCommand(Z_Construct_UClass_UMoveUpOneLevelCommand, &UMoveUpOneLevelCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UMoveUpOneLevelCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoveUpOneLevelCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
