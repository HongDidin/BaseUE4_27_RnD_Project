// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_ExtrudePolygon_generated_h
#error "ExtrudePolygon.generated.h already included, missing '#pragma once' in ExtrudePolygon.h"
#endif
#define POLYGONMODELING_ExtrudePolygon_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUExtrudePolygonCommand(); \
	friend struct Z_Construct_UClass_UExtrudePolygonCommand_Statics; \
public: \
	DECLARE_CLASS(UExtrudePolygonCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UExtrudePolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUExtrudePolygonCommand(); \
	friend struct Z_Construct_UClass_UExtrudePolygonCommand_Statics; \
public: \
	DECLARE_CLASS(UExtrudePolygonCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UExtrudePolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UExtrudePolygonCommand(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UExtrudePolygonCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExtrudePolygonCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExtrudePolygonCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExtrudePolygonCommand(UExtrudePolygonCommand&&); \
	NO_API UExtrudePolygonCommand(const UExtrudePolygonCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExtrudePolygonCommand(UExtrudePolygonCommand&&); \
	NO_API UExtrudePolygonCommand(const UExtrudePolygonCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExtrudePolygonCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExtrudePolygonCommand); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UExtrudePolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_10_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class UExtrudePolygonCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_ExtrudePolygon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
