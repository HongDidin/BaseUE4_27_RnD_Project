// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/FlipPolygon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFlipPolygon() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UFlipPolygonCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UFlipPolygonCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UFlipPolygonCommand::StaticRegisterNativesUFlipPolygonCommand()
	{
	}
	UClass* Z_Construct_UClass_UFlipPolygonCommand_NoRegister()
	{
		return UFlipPolygonCommand::StaticClass();
	}
	struct Z_Construct_UClass_UFlipPolygonCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFlipPolygonCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFlipPolygonCommand_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FlipPolygon.h" },
		{ "ModuleRelativePath", "Public/FlipPolygon.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFlipPolygonCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFlipPolygonCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFlipPolygonCommand_Statics::ClassParams = {
		&UFlipPolygonCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFlipPolygonCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFlipPolygonCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFlipPolygonCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFlipPolygonCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFlipPolygonCommand, 2055799813);
	template<> POLYGONMODELING_API UClass* StaticClass<UFlipPolygonCommand>()
	{
		return UFlipPolygonCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFlipPolygonCommand(Z_Construct_UClass_UFlipPolygonCommand, &UFlipPolygonCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UFlipPolygonCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFlipPolygonCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
