// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/MeshEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshEditorSettings() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorSettings_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
// End Cross Module References
	void UMeshEditorSettings::StaticRegisterNativesUMeshEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorSettings_NoRegister()
	{
		return UMeshEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSeparateSelectionSetPerMode_MetaData[];
#endif
		static void NewProp_bSeparateSelectionSetPerMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSeparateSelectionSetPerMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlySelectVisibleMeshes_MetaData[];
#endif
		static void NewProp_bOnlySelectVisibleMeshes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlySelectVisibleMeshes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlySelectVisibleElements_MetaData[];
#endif
		static void NewProp_bOnlySelectVisibleElements_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlySelectVisibleElements;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowGrabberSphere_MetaData[];
#endif
		static void NewProp_bAllowGrabberSphere_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowGrabberSphere;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoQuadrangulate_MetaData[];
#endif
		static void NewProp_bAutoQuadrangulate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoQuadrangulate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements the Mesh Editor's settings.\n */" },
		{ "IncludePath", "MeshEditorSettings.h" },
		{ "ModuleRelativePath", "MeshEditorSettings.h" },
		{ "ToolTip", "Implements the Mesh Editor's settings." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bSeparateSelectionSetPerMode_MetaData[] = {
		{ "Category", "MeshEditor" },
		{ "Comment", "/** If set, each element selection mode remembers its own selection set. Otherwise, changing selection mode adapts the current selection as appropriate. */" },
		{ "ModuleRelativePath", "MeshEditorSettings.h" },
		{ "ToolTip", "If set, each element selection mode remembers its own selection set. Otherwise, changing selection mode adapts the current selection as appropriate." },
	};
#endif
	void Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bSeparateSelectionSetPerMode_SetBit(void* Obj)
	{
		((UMeshEditorSettings*)Obj)->bSeparateSelectionSetPerMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bSeparateSelectionSetPerMode = { "bSeparateSelectionSetPerMode", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshEditorSettings), &Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bSeparateSelectionSetPerMode_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bSeparateSelectionSetPerMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bSeparateSelectionSetPerMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleMeshes_MetaData[] = {
		{ "Category", "MeshEditor" },
		{ "Comment", "/** Whether only unoccluded meshes will be selected by marquee select, or whether all meshes within the selection box will be selected, regardless of whether they are behind another. */" },
		{ "ModuleRelativePath", "MeshEditorSettings.h" },
		{ "ToolTip", "Whether only unoccluded meshes will be selected by marquee select, or whether all meshes within the selection box will be selected, regardless of whether they are behind another." },
	};
#endif
	void Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleMeshes_SetBit(void* Obj)
	{
		((UMeshEditorSettings*)Obj)->bOnlySelectVisibleMeshes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleMeshes = { "bOnlySelectVisibleMeshes", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshEditorSettings), &Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleMeshes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleMeshes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleElements_MetaData[] = {
		{ "Category", "MeshEditor" },
		{ "Comment", "/** Whether only front-facing vertices, edges or polygons will be selected by marquee select. */" },
		{ "ModuleRelativePath", "MeshEditorSettings.h" },
		{ "ToolTip", "Whether only front-facing vertices, edges or polygons will be selected by marquee select." },
	};
#endif
	void Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleElements_SetBit(void* Obj)
	{
		((UMeshEditorSettings*)Obj)->bOnlySelectVisibleElements = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleElements = { "bOnlySelectVisibleElements", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshEditorSettings), &Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleElements_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleElements_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleElements_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAllowGrabberSphere_MetaData[] = {
		{ "Category", "MeshEditor" },
		{ "Comment", "/** When enabled, the grabber sphere will be used to select and move mesh elements near the interactor's origin */" },
		{ "ModuleRelativePath", "MeshEditorSettings.h" },
		{ "ToolTip", "When enabled, the grabber sphere will be used to select and move mesh elements near the interactor's origin" },
	};
#endif
	void Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAllowGrabberSphere_SetBit(void* Obj)
	{
		((UMeshEditorSettings*)Obj)->bAllowGrabberSphere = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAllowGrabberSphere = { "bAllowGrabberSphere", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshEditorSettings), &Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAllowGrabberSphere_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAllowGrabberSphere_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAllowGrabberSphere_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAutoQuadrangulate_MetaData[] = {
		{ "Category", "MeshEditor" },
		{ "Comment", "/** When enabled, triangulated static meshes will be auto-quadrangulated when converted to editable meshes */" },
		{ "ModuleRelativePath", "MeshEditorSettings.h" },
		{ "ToolTip", "When enabled, triangulated static meshes will be auto-quadrangulated when converted to editable meshes" },
	};
#endif
	void Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAutoQuadrangulate_SetBit(void* Obj)
	{
		((UMeshEditorSettings*)Obj)->bAutoQuadrangulate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAutoQuadrangulate = { "bAutoQuadrangulate", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshEditorSettings), &Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAutoQuadrangulate_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAutoQuadrangulate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAutoQuadrangulate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bSeparateSelectionSetPerMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleMeshes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bOnlySelectVisibleElements,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAllowGrabberSphere,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorSettings_Statics::NewProp_bAutoQuadrangulate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorSettings_Statics::ClassParams = {
		&UMeshEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorSettings, 429716537);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorSettings>()
	{
		return UMeshEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorSettings(Z_Construct_UClass_UMeshEditorSettings, &UMeshEditorSettings::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
