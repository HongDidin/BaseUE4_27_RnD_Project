// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHEDITOR_MeshEditorAssetContainer_generated_h
#error "MeshEditorAssetContainer.generated.h already included, missing '#pragma once' in MeshEditorAssetContainer.h"
#endif
#define MESHEDITOR_MeshEditorAssetContainer_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorAssetContainer(); \
	friend struct Z_Construct_UClass_UMeshEditorAssetContainer_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorAssetContainer, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorAssetContainer)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorAssetContainer(); \
	friend struct Z_Construct_UClass_UMeshEditorAssetContainer_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorAssetContainer, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorAssetContainer)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorAssetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorAssetContainer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorAssetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorAssetContainer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorAssetContainer(UMeshEditorAssetContainer&&); \
	NO_API UMeshEditorAssetContainer(const UMeshEditorAssetContainer&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorAssetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorAssetContainer(UMeshEditorAssetContainer&&); \
	NO_API UMeshEditorAssetContainer(const UMeshEditorAssetContainer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorAssetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorAssetContainer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorAssetContainer)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_11_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorAssetContainer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorAssetContainer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
