// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHEDITOR_MeshEditorSelectionModifiers_generated_h
#error "MeshEditorSelectionModifiers.generated.h already included, missing '#pragma once' in MeshEditorSelectionModifiers.h"
#endif
#define MESHEDITOR_MeshEditorSelectionModifiers_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorSelectionModifier(); \
	friend struct Z_Construct_UClass_UMeshEditorSelectionModifier_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorSelectionModifier, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorSelectionModifier)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorSelectionModifier(); \
	friend struct Z_Construct_UClass_UMeshEditorSelectionModifier_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorSelectionModifier, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorSelectionModifier)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorSelectionModifier(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorSelectionModifier) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorSelectionModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorSelectionModifier); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorSelectionModifier(UMeshEditorSelectionModifier&&); \
	NO_API UMeshEditorSelectionModifier(const UMeshEditorSelectionModifier&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorSelectionModifier(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorSelectionModifier(UMeshEditorSelectionModifier&&); \
	NO_API UMeshEditorSelectionModifier(const UMeshEditorSelectionModifier&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorSelectionModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorSelectionModifier); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorSelectionModifier)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_30_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorSelectionModifier>();

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelectSingleMeshElement(); \
	friend struct Z_Construct_UClass_USelectSingleMeshElement_Statics; \
public: \
	DECLARE_CLASS(USelectSingleMeshElement, UMeshEditorSelectionModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(USelectSingleMeshElement)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_INCLASS \
private: \
	static void StaticRegisterNativesUSelectSingleMeshElement(); \
	friend struct Z_Construct_UClass_USelectSingleMeshElement_Statics; \
public: \
	DECLARE_CLASS(USelectSingleMeshElement, UMeshEditorSelectionModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(USelectSingleMeshElement)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectSingleMeshElement(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectSingleMeshElement) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectSingleMeshElement); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectSingleMeshElement); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectSingleMeshElement(USelectSingleMeshElement&&); \
	NO_API USelectSingleMeshElement(const USelectSingleMeshElement&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectSingleMeshElement(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectSingleMeshElement(USelectSingleMeshElement&&); \
	NO_API USelectSingleMeshElement(const USelectSingleMeshElement&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectSingleMeshElement); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectSingleMeshElement); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectSingleMeshElement)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_64_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_67_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class USelectSingleMeshElement>();

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelectPolygonsByGroup(); \
	friend struct Z_Construct_UClass_USelectPolygonsByGroup_Statics; \
public: \
	DECLARE_CLASS(USelectPolygonsByGroup, UMeshEditorSelectionModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(USelectPolygonsByGroup)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_INCLASS \
private: \
	static void StaticRegisterNativesUSelectPolygonsByGroup(); \
	friend struct Z_Construct_UClass_USelectPolygonsByGroup_Statics; \
public: \
	DECLARE_CLASS(USelectPolygonsByGroup, UMeshEditorSelectionModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(USelectPolygonsByGroup)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectPolygonsByGroup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectPolygonsByGroup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectPolygonsByGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectPolygonsByGroup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectPolygonsByGroup(USelectPolygonsByGroup&&); \
	NO_API USelectPolygonsByGroup(const USelectPolygonsByGroup&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectPolygonsByGroup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectPolygonsByGroup(USelectPolygonsByGroup&&); \
	NO_API USelectPolygonsByGroup(const USelectPolygonsByGroup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectPolygonsByGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectPolygonsByGroup); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectPolygonsByGroup)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_80_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_83_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class USelectPolygonsByGroup>();

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelectPolygonsByConnectivity(); \
	friend struct Z_Construct_UClass_USelectPolygonsByConnectivity_Statics; \
public: \
	DECLARE_CLASS(USelectPolygonsByConnectivity, UMeshEditorSelectionModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(USelectPolygonsByConnectivity)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_INCLASS \
private: \
	static void StaticRegisterNativesUSelectPolygonsByConnectivity(); \
	friend struct Z_Construct_UClass_USelectPolygonsByConnectivity_Statics; \
public: \
	DECLARE_CLASS(USelectPolygonsByConnectivity, UMeshEditorSelectionModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(USelectPolygonsByConnectivity)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectPolygonsByConnectivity(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectPolygonsByConnectivity) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectPolygonsByConnectivity); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectPolygonsByConnectivity); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectPolygonsByConnectivity(USelectPolygonsByConnectivity&&); \
	NO_API USelectPolygonsByConnectivity(const USelectPolygonsByConnectivity&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectPolygonsByConnectivity(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectPolygonsByConnectivity(USelectPolygonsByConnectivity&&); \
	NO_API USelectPolygonsByConnectivity(const USelectPolygonsByConnectivity&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectPolygonsByConnectivity); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectPolygonsByConnectivity); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectPolygonsByConnectivity)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_95_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_98_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class USelectPolygonsByConnectivity>();

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelectPolygonsBySmoothingGroup(); \
	friend struct Z_Construct_UClass_USelectPolygonsBySmoothingGroup_Statics; \
public: \
	DECLARE_CLASS(USelectPolygonsBySmoothingGroup, UMeshEditorSelectionModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(USelectPolygonsBySmoothingGroup)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_INCLASS \
private: \
	static void StaticRegisterNativesUSelectPolygonsBySmoothingGroup(); \
	friend struct Z_Construct_UClass_USelectPolygonsBySmoothingGroup_Statics; \
public: \
	DECLARE_CLASS(USelectPolygonsBySmoothingGroup, UMeshEditorSelectionModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(USelectPolygonsBySmoothingGroup)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectPolygonsBySmoothingGroup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectPolygonsBySmoothingGroup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectPolygonsBySmoothingGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectPolygonsBySmoothingGroup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectPolygonsBySmoothingGroup(USelectPolygonsBySmoothingGroup&&); \
	NO_API USelectPolygonsBySmoothingGroup(const USelectPolygonsBySmoothingGroup&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectPolygonsBySmoothingGroup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectPolygonsBySmoothingGroup(USelectPolygonsBySmoothingGroup&&); \
	NO_API USelectPolygonsBySmoothingGroup(const USelectPolygonsBySmoothingGroup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectPolygonsBySmoothingGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectPolygonsBySmoothingGroup); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectPolygonsBySmoothingGroup)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_110_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_113_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class USelectPolygonsBySmoothingGroup>();

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorSelectionModifiersList(); \
	friend struct Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorSelectionModifiersList, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorSelectionModifiersList)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorSelectionModifiersList(); \
	friend struct Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorSelectionModifiersList, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UMeshEditorSelectionModifiersList)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorSelectionModifiersList(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorSelectionModifiersList) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorSelectionModifiersList); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorSelectionModifiersList); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorSelectionModifiersList(UMeshEditorSelectionModifiersList&&); \
	NO_API UMeshEditorSelectionModifiersList(const UMeshEditorSelectionModifiersList&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshEditorSelectionModifiersList(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshEditorSelectionModifiersList(UMeshEditorSelectionModifiersList&&); \
	NO_API UMeshEditorSelectionModifiersList(const UMeshEditorSelectionModifiersList&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshEditorSelectionModifiersList); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorSelectionModifiersList); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorSelectionModifiersList)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_122_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h_125_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorSelectionModifiersList>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_MeshEditorSelectionModifiers_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
