// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/SplitPolygon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSplitPolygon() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitPolygonCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitPolygonCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorEditCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	EDITABLEMESH_API UClass* Z_Construct_UClass_UEditableMesh_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitPolygonFromVertexCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitPolygonFromVertexCommand();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitPolygonFromEdgeCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitPolygonFromEdgeCommand();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitPolygonFromPolygonCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USplitPolygonFromPolygonCommand();
// End Cross Module References
	void USplitPolygonCommand::StaticRegisterNativesUSplitPolygonCommand()
	{
	}
	UClass* Z_Construct_UClass_USplitPolygonCommand_NoRegister()
	{
		return USplitPolygonCommand::StaticClass();
	}
	struct Z_Construct_UClass_USplitPolygonCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Component_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Component;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditableMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditableMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USplitPolygonCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorEditCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USplitPolygonCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Base class for polygon splitting */" },
		{ "IncludePath", "SplitPolygon.h" },
		{ "ModuleRelativePath", "Public/SplitPolygon.h" },
		{ "ToolTip", "Base class for polygon splitting" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USplitPolygonCommand_Statics::NewProp_Component_MetaData[] = {
		{ "Comment", "/** The component we're editing */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/SplitPolygon.h" },
		{ "ToolTip", "The component we're editing" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USplitPolygonCommand_Statics::NewProp_Component = { "Component", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USplitPolygonCommand, Component), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USplitPolygonCommand_Statics::NewProp_Component_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USplitPolygonCommand_Statics::NewProp_Component_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USplitPolygonCommand_Statics::NewProp_EditableMesh_MetaData[] = {
		{ "Comment", "/** The mesh we're editing */" },
		{ "ModuleRelativePath", "Public/SplitPolygon.h" },
		{ "ToolTip", "The mesh we're editing" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USplitPolygonCommand_Statics::NewProp_EditableMesh = { "EditableMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USplitPolygonCommand, EditableMesh), Z_Construct_UClass_UEditableMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USplitPolygonCommand_Statics::NewProp_EditableMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USplitPolygonCommand_Statics::NewProp_EditableMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USplitPolygonCommand_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USplitPolygonCommand_Statics::NewProp_Component,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USplitPolygonCommand_Statics::NewProp_EditableMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USplitPolygonCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USplitPolygonCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USplitPolygonCommand_Statics::ClassParams = {
		&USplitPolygonCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USplitPolygonCommand_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USplitPolygonCommand_Statics::PropPointers),
		0,
		0x009000A1u,
		METADATA_PARAMS(Z_Construct_UClass_USplitPolygonCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USplitPolygonCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USplitPolygonCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USplitPolygonCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USplitPolygonCommand, 853791683);
	template<> POLYGONMODELING_API UClass* StaticClass<USplitPolygonCommand>()
	{
		return USplitPolygonCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USplitPolygonCommand(Z_Construct_UClass_USplitPolygonCommand, &USplitPolygonCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("USplitPolygonCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USplitPolygonCommand);
	void USplitPolygonFromVertexCommand::StaticRegisterNativesUSplitPolygonFromVertexCommand()
	{
	}
	UClass* Z_Construct_UClass_USplitPolygonFromVertexCommand_NoRegister()
	{
		return USplitPolygonFromVertexCommand::StaticClass();
	}
	struct Z_Construct_UClass_USplitPolygonFromVertexCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USplitPolygonFromVertexCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USplitPolygonCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USplitPolygonFromVertexCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Splits a polygon into two, starting with a vertex on that polygon */" },
		{ "IncludePath", "SplitPolygon.h" },
		{ "ModuleRelativePath", "Public/SplitPolygon.h" },
		{ "ToolTip", "Splits a polygon into two, starting with a vertex on that polygon" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USplitPolygonFromVertexCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USplitPolygonFromVertexCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USplitPolygonFromVertexCommand_Statics::ClassParams = {
		&USplitPolygonFromVertexCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USplitPolygonFromVertexCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USplitPolygonFromVertexCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USplitPolygonFromVertexCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USplitPolygonFromVertexCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USplitPolygonFromVertexCommand, 3743279210);
	template<> POLYGONMODELING_API UClass* StaticClass<USplitPolygonFromVertexCommand>()
	{
		return USplitPolygonFromVertexCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USplitPolygonFromVertexCommand(Z_Construct_UClass_USplitPolygonFromVertexCommand, &USplitPolygonFromVertexCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("USplitPolygonFromVertexCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USplitPolygonFromVertexCommand);
	void USplitPolygonFromEdgeCommand::StaticRegisterNativesUSplitPolygonFromEdgeCommand()
	{
	}
	UClass* Z_Construct_UClass_USplitPolygonFromEdgeCommand_NoRegister()
	{
		return USplitPolygonFromEdgeCommand::StaticClass();
	}
	struct Z_Construct_UClass_USplitPolygonFromEdgeCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USplitPolygonFromEdgeCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USplitPolygonCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USplitPolygonFromEdgeCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Splits a polygon into two, starting with a point along an edge */" },
		{ "IncludePath", "SplitPolygon.h" },
		{ "ModuleRelativePath", "Public/SplitPolygon.h" },
		{ "ToolTip", "Splits a polygon into two, starting with a point along an edge" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USplitPolygonFromEdgeCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USplitPolygonFromEdgeCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USplitPolygonFromEdgeCommand_Statics::ClassParams = {
		&USplitPolygonFromEdgeCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USplitPolygonFromEdgeCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USplitPolygonFromEdgeCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USplitPolygonFromEdgeCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USplitPolygonFromEdgeCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USplitPolygonFromEdgeCommand, 1562009653);
	template<> POLYGONMODELING_API UClass* StaticClass<USplitPolygonFromEdgeCommand>()
	{
		return USplitPolygonFromEdgeCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USplitPolygonFromEdgeCommand(Z_Construct_UClass_USplitPolygonFromEdgeCommand, &USplitPolygonFromEdgeCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("USplitPolygonFromEdgeCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USplitPolygonFromEdgeCommand);
	void USplitPolygonFromPolygonCommand::StaticRegisterNativesUSplitPolygonFromPolygonCommand()
	{
	}
	UClass* Z_Construct_UClass_USplitPolygonFromPolygonCommand_NoRegister()
	{
		return USplitPolygonFromPolygonCommand::StaticClass();
	}
	struct Z_Construct_UClass_USplitPolygonFromPolygonCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USplitPolygonFromPolygonCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USplitPolygonCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USplitPolygonFromPolygonCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Splits a polygon into two, starting with an edge on a polygon */" },
		{ "IncludePath", "SplitPolygon.h" },
		{ "ModuleRelativePath", "Public/SplitPolygon.h" },
		{ "ToolTip", "Splits a polygon into two, starting with an edge on a polygon" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USplitPolygonFromPolygonCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USplitPolygonFromPolygonCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USplitPolygonFromPolygonCommand_Statics::ClassParams = {
		&USplitPolygonFromPolygonCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USplitPolygonFromPolygonCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USplitPolygonFromPolygonCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USplitPolygonFromPolygonCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USplitPolygonFromPolygonCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USplitPolygonFromPolygonCommand, 333066637);
	template<> POLYGONMODELING_API UClass* StaticClass<USplitPolygonFromPolygonCommand>()
	{
		return USplitPolygonFromPolygonCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USplitPolygonFromPolygonCommand(Z_Construct_UClass_USplitPolygonFromPolygonCommand, &USplitPolygonFromPolygonCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("USplitPolygonFromPolygonCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USplitPolygonFromPolygonCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
