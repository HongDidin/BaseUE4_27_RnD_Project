// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/Public/MeshEditorAssetContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshEditorAssetContainer() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorAssetContainer_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorAssetContainer();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USoundBase_NoRegister();
// End Cross Module References
	void UMeshEditorAssetContainer::StaticRegisterNativesUMeshEditorAssetContainer()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorAssetContainer_NoRegister()
	{
		return UMeshEditorAssetContainer::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorAssetContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoveredGeometryMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HoveredGeometryMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoveredFaceMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HoveredFaceMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WireMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WireMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubdividedMeshWireMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SubdividedMeshWireMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlayLineMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlayLineMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlayPointMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlayPointMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultSound;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorAssetContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorAssetContainer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Asset container for the mesh editor\n */" },
		{ "IncludePath", "MeshEditorAssetContainer.h" },
		{ "ModuleRelativePath", "Public/MeshEditorAssetContainer.h" },
		{ "ToolTip", "Asset container for the mesh editor" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_HoveredGeometryMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/MeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_HoveredGeometryMaterial = { "HoveredGeometryMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorAssetContainer, HoveredGeometryMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_HoveredGeometryMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_HoveredGeometryMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_HoveredFaceMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/MeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_HoveredFaceMaterial = { "HoveredFaceMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorAssetContainer, HoveredFaceMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_HoveredFaceMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_HoveredFaceMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_WireMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/MeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_WireMaterial = { "WireMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorAssetContainer, WireMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_WireMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_WireMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_SubdividedMeshWireMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/MeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_SubdividedMeshWireMaterial = { "SubdividedMeshWireMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorAssetContainer, SubdividedMeshWireMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_SubdividedMeshWireMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_SubdividedMeshWireMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_OverlayLineMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/MeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_OverlayLineMaterial = { "OverlayLineMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorAssetContainer, OverlayLineMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_OverlayLineMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_OverlayLineMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_OverlayPointMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/MeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_OverlayPointMaterial = { "OverlayPointMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorAssetContainer, OverlayPointMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_OverlayPointMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_OverlayPointMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_DefaultSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "ModuleRelativePath", "Public/MeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_DefaultSound = { "DefaultSound", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorAssetContainer, DefaultSound), Z_Construct_UClass_USoundBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_DefaultSound_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_DefaultSound_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshEditorAssetContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_HoveredGeometryMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_HoveredFaceMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_WireMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_SubdividedMeshWireMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_OverlayLineMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_OverlayPointMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorAssetContainer_Statics::NewProp_DefaultSound,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorAssetContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorAssetContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorAssetContainer_Statics::ClassParams = {
		&UMeshEditorAssetContainer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshEditorAssetContainer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorAssetContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorAssetContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorAssetContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorAssetContainer, 3355759999);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorAssetContainer>()
	{
		return UMeshEditorAssetContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorAssetContainer(Z_Construct_UClass_UMeshEditorAssetContainer, &UMeshEditorAssetContainer::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorAssetContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorAssetContainer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
