// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHEDITOR_MeshEditorSubdividedStaticMeshAdapter_generated_h
#error "MeshEditorSubdividedStaticMeshAdapter.generated.h already included, missing '#pragma once' in MeshEditorSubdividedStaticMeshAdapter.h"
#endif
#define MESHEDITOR_MeshEditorSubdividedStaticMeshAdapter_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshEditorSubdividedStaticMeshAdapter(); \
	friend struct Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorSubdividedStaticMeshAdapter, UEditableMeshAdapter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), MESHEDITOR_API) \
	DECLARE_SERIALIZER(UMeshEditorSubdividedStaticMeshAdapter)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMeshEditorSubdividedStaticMeshAdapter(); \
	friend struct Z_Construct_UClass_UMeshEditorSubdividedStaticMeshAdapter_Statics; \
public: \
	DECLARE_CLASS(UMeshEditorSubdividedStaticMeshAdapter, UEditableMeshAdapter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), MESHEDITOR_API) \
	DECLARE_SERIALIZER(UMeshEditorSubdividedStaticMeshAdapter)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MESHEDITOR_API UMeshEditorSubdividedStaticMeshAdapter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshEditorSubdividedStaticMeshAdapter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MESHEDITOR_API, UMeshEditorSubdividedStaticMeshAdapter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorSubdividedStaticMeshAdapter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MESHEDITOR_API UMeshEditorSubdividedStaticMeshAdapter(UMeshEditorSubdividedStaticMeshAdapter&&); \
	MESHEDITOR_API UMeshEditorSubdividedStaticMeshAdapter(const UMeshEditorSubdividedStaticMeshAdapter&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MESHEDITOR_API UMeshEditorSubdividedStaticMeshAdapter(UMeshEditorSubdividedStaticMeshAdapter&&); \
	MESHEDITOR_API UMeshEditorSubdividedStaticMeshAdapter(const UMeshEditorSubdividedStaticMeshAdapter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MESHEDITOR_API, UMeshEditorSubdividedStaticMeshAdapter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshEditorSubdividedStaticMeshAdapter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshEditorSubdividedStaticMeshAdapter)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__WireframeMesh() { return STRUCT_OFFSET(UMeshEditorSubdividedStaticMeshAdapter, WireframeMesh); } \
	FORCEINLINE static uint32 __PPO__StaticMeshLODIndex() { return STRUCT_OFFSET(UMeshEditorSubdividedStaticMeshAdapter, StaticMeshLODIndex); }


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_12_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UMeshEditorSubdividedStaticMeshAdapter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_MeshEditorSubdividedStaticMeshAdapter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
