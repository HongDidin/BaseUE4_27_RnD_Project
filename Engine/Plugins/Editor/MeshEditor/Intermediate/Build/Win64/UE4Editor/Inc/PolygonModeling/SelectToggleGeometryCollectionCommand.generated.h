// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_SelectToggleGeometryCollectionCommand_generated_h
#error "SelectToggleGeometryCollectionCommand.generated.h already included, missing '#pragma once' in SelectToggleGeometryCollectionCommand.h"
#endif
#define POLYGONMODELING_SelectToggleGeometryCollectionCommand_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelectToggleGeometryCollectionCommand(); \
	friend struct Z_Construct_UClass_USelectToggleGeometryCollectionCommand_Statics; \
public: \
	DECLARE_CLASS(USelectToggleGeometryCollectionCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USelectToggleGeometryCollectionCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUSelectToggleGeometryCollectionCommand(); \
	friend struct Z_Construct_UClass_USelectToggleGeometryCollectionCommand_Statics; \
public: \
	DECLARE_CLASS(USelectToggleGeometryCollectionCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(USelectToggleGeometryCollectionCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectToggleGeometryCollectionCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectToggleGeometryCollectionCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectToggleGeometryCollectionCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectToggleGeometryCollectionCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectToggleGeometryCollectionCommand(USelectToggleGeometryCollectionCommand&&); \
	NO_API USelectToggleGeometryCollectionCommand(const USelectToggleGeometryCollectionCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectToggleGeometryCollectionCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectToggleGeometryCollectionCommand(USelectToggleGeometryCollectionCommand&&); \
	NO_API USelectToggleGeometryCollectionCommand(const USelectToggleGeometryCollectionCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectToggleGeometryCollectionCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectToggleGeometryCollectionCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectToggleGeometryCollectionCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_15_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h_19_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class USelectToggleGeometryCollectionCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_SelectToggleGeometryCollectionCommand_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
