// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/Public/MeshEditorCommands.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshEditorCommands() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorCommand_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorCommand();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorEditCommand_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorEditCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorCommandList_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorCommandList();
// End Cross Module References
	void UMeshEditorCommand::StaticRegisterNativesUMeshEditorCommand()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorCommand_NoRegister()
	{
		return UMeshEditorCommand::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorCommand_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshEditorCommands.h" },
		{ "ModuleRelativePath", "Public/MeshEditorCommands.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorCommand_Statics::ClassParams = {
		&UMeshEditorCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorCommand, 5895654);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorCommand>()
	{
		return UMeshEditorCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorCommand(Z_Construct_UClass_UMeshEditorCommand, &UMeshEditorCommand::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorCommand);
	void UMeshEditorInstantCommand::StaticRegisterNativesUMeshEditorInstantCommand()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorInstantCommand_NoRegister()
	{
		return UMeshEditorInstantCommand::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorInstantCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorInstantCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorInstantCommand_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshEditorCommands.h" },
		{ "ModuleRelativePath", "Public/MeshEditorCommands.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorInstantCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorInstantCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorInstantCommand_Statics::ClassParams = {
		&UMeshEditorInstantCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorInstantCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorInstantCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorInstantCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorInstantCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorInstantCommand, 1592041966);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorInstantCommand>()
	{
		return UMeshEditorInstantCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorInstantCommand(Z_Construct_UClass_UMeshEditorInstantCommand, &UMeshEditorInstantCommand::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorInstantCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorInstantCommand);
	void UMeshEditorEditCommand::StaticRegisterNativesUMeshEditorEditCommand()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorEditCommand_NoRegister()
	{
		return UMeshEditorEditCommand::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorEditCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UndoText_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_UndoText;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNeedsDraggingInitiated_MetaData[];
#endif
		static void NewProp_bNeedsDraggingInitiated_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNeedsDraggingInitiated;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNeedsHoverLocation_MetaData[];
#endif
		static void NewProp_bNeedsHoverLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNeedsHoverLocation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorEditCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorEditCommand_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshEditorCommands.h" },
		{ "ModuleRelativePath", "Public/MeshEditorCommands.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_UndoText_MetaData[] = {
		{ "Category", "MeshEditor" },
		{ "Comment", "/** The text to send to the transaction system when creating an undo / redo event for this action */" },
		{ "ModuleRelativePath", "Public/MeshEditorCommands.h" },
		{ "ToolTip", "The text to send to the transaction system when creating an undo / redo event for this action" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_UndoText = { "UndoText", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorEditCommand, UndoText), METADATA_PARAMS(Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_UndoText_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_UndoText_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsDraggingInitiated_MetaData[] = {
		{ "Category", "MeshEditor" },
		{ "Comment", "/** Whether this command will kick off regular free translation of the selected mesh elements when dragging starts */" },
		{ "ModuleRelativePath", "Public/MeshEditorCommands.h" },
		{ "ToolTip", "Whether this command will kick off regular free translation of the selected mesh elements when dragging starts" },
	};
#endif
	void Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsDraggingInitiated_SetBit(void* Obj)
	{
		((UMeshEditorEditCommand*)Obj)->bNeedsDraggingInitiated = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsDraggingInitiated = { "bNeedsDraggingInitiated", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshEditorEditCommand), &Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsDraggingInitiated_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsDraggingInitiated_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsDraggingInitiated_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsHoverLocation_MetaData[] = {
		{ "Category", "MeshEditor" },
		{ "Comment", "/** Whether we rely on a hover location under the interactor being updated as we drag during this action */" },
		{ "ModuleRelativePath", "Public/MeshEditorCommands.h" },
		{ "ToolTip", "Whether we rely on a hover location under the interactor being updated as we drag during this action" },
	};
#endif
	void Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsHoverLocation_SetBit(void* Obj)
	{
		((UMeshEditorEditCommand*)Obj)->bNeedsHoverLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsHoverLocation = { "bNeedsHoverLocation", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshEditorEditCommand), &Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsHoverLocation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsHoverLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsHoverLocation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshEditorEditCommand_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_UndoText,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsDraggingInitiated,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorEditCommand_Statics::NewProp_bNeedsHoverLocation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorEditCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorEditCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorEditCommand_Statics::ClassParams = {
		&UMeshEditorEditCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshEditorEditCommand_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorEditCommand_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorEditCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorEditCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorEditCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorEditCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorEditCommand, 1598577291);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorEditCommand>()
	{
		return UMeshEditorEditCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorEditCommand(Z_Construct_UClass_UMeshEditorEditCommand, &UMeshEditorEditCommand::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorEditCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorEditCommand);
	void UMeshEditorCommandList::StaticRegisterNativesUMeshEditorCommandList()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorCommandList_NoRegister()
	{
		return UMeshEditorCommandList::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorCommandList_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshEditorCommands_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshEditorCommands_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MeshEditorCommands;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorCommandList_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorCommandList_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshEditorCommands.h" },
		{ "ModuleRelativePath", "Public/MeshEditorCommands.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorCommandList_Statics::NewProp_MeshEditorCommands_Inner = { "MeshEditorCommands", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshEditorCommand_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorCommandList_Statics::NewProp_MeshEditorCommands_MetaData[] = {
		{ "Comment", "/** All of the mesh editor commands that were registered at startup */" },
		{ "ModuleRelativePath", "Public/MeshEditorCommands.h" },
		{ "ToolTip", "All of the mesh editor commands that were registered at startup" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMeshEditorCommandList_Statics::NewProp_MeshEditorCommands = { "MeshEditorCommands", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorCommandList, MeshEditorCommands), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorCommandList_Statics::NewProp_MeshEditorCommands_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorCommandList_Statics::NewProp_MeshEditorCommands_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshEditorCommandList_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorCommandList_Statics::NewProp_MeshEditorCommands_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorCommandList_Statics::NewProp_MeshEditorCommands,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorCommandList_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorCommandList>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorCommandList_Statics::ClassParams = {
		&UMeshEditorCommandList::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshEditorCommandList_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorCommandList_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorCommandList_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorCommandList_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorCommandList()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorCommandList_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorCommandList, 3147927531);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorCommandList>()
	{
		return UMeshEditorCommandList::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorCommandList(Z_Construct_UClass_UMeshEditorCommandList, &UMeshEditorCommandList::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorCommandList"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorCommandList);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
