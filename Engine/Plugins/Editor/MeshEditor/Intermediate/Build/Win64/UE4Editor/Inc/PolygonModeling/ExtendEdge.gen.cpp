// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/ExtendEdge.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExtendEdge() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UExtendEdgeCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UExtendEdgeCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorEditCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UExtendEdgeCommand::StaticRegisterNativesUExtendEdgeCommand()
	{
	}
	UClass* Z_Construct_UClass_UExtendEdgeCommand_NoRegister()
	{
		return UExtendEdgeCommand::StaticClass();
	}
	struct Z_Construct_UClass_UExtendEdgeCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UExtendEdgeCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorEditCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExtendEdgeCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Extends an edge by making a copy of it and allowing you to move it around */" },
		{ "IncludePath", "ExtendEdge.h" },
		{ "ModuleRelativePath", "Public/ExtendEdge.h" },
		{ "ToolTip", "Extends an edge by making a copy of it and allowing you to move it around" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UExtendEdgeCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UExtendEdgeCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UExtendEdgeCommand_Statics::ClassParams = {
		&UExtendEdgeCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UExtendEdgeCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UExtendEdgeCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UExtendEdgeCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UExtendEdgeCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UExtendEdgeCommand, 2089734953);
	template<> POLYGONMODELING_API UClass* StaticClass<UExtendEdgeCommand>()
	{
		return UExtendEdgeCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UExtendEdgeCommand(Z_Construct_UClass_UExtendEdgeCommand, &UExtendEdgeCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UExtendEdgeCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UExtendEdgeCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
