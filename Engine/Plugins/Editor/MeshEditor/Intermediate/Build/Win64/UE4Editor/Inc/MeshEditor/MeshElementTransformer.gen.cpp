// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/MeshElementTransformer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshElementTransformer() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshElementTransformer_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshElementTransformer();
	VIEWPORTINTERACTION_API UClass* Z_Construct_UClass_UViewportTransformer();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
// End Cross Module References
	void UMeshElementTransformer::StaticRegisterNativesUMeshElementTransformer()
	{
	}
	UClass* Z_Construct_UClass_UMeshElementTransformer_NoRegister()
	{
		return UMeshElementTransformer::StaticClass();
	}
	struct Z_Construct_UClass_UMeshElementTransformer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshElementTransformer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UViewportTransformer,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshElementTransformer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshElementTransformer.h" },
		{ "ModuleRelativePath", "MeshElementTransformer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshElementTransformer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshElementTransformer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshElementTransformer_Statics::ClassParams = {
		&UMeshElementTransformer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshElementTransformer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshElementTransformer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshElementTransformer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshElementTransformer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshElementTransformer, 7425161);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshElementTransformer>()
	{
		return UMeshElementTransformer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshElementTransformer(Z_Construct_UClass_UMeshElementTransformer, &UMeshElementTransformer::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshElementTransformer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshElementTransformer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
