// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_UnHideAllGeometryCollectionCommand_generated_h
#error "UnHideAllGeometryCollectionCommand.generated.h already included, missing '#pragma once' in UnHideAllGeometryCollectionCommand.h"
#endif
#define POLYGONMODELING_UnHideAllGeometryCollectionCommand_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUnHideAllGeometryCollectionCommand(); \
	friend struct Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_Statics; \
public: \
	DECLARE_CLASS(UUnHideAllGeometryCollectionCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UUnHideAllGeometryCollectionCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUUnHideAllGeometryCollectionCommand(); \
	friend struct Z_Construct_UClass_UUnHideAllGeometryCollectionCommand_Statics; \
public: \
	DECLARE_CLASS(UUnHideAllGeometryCollectionCommand, UMeshEditorInstantCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UUnHideAllGeometryCollectionCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUnHideAllGeometryCollectionCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUnHideAllGeometryCollectionCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUnHideAllGeometryCollectionCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUnHideAllGeometryCollectionCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUnHideAllGeometryCollectionCommand(UUnHideAllGeometryCollectionCommand&&); \
	NO_API UUnHideAllGeometryCollectionCommand(const UUnHideAllGeometryCollectionCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUnHideAllGeometryCollectionCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUnHideAllGeometryCollectionCommand(UUnHideAllGeometryCollectionCommand&&); \
	NO_API UUnHideAllGeometryCollectionCommand(const UUnHideAllGeometryCollectionCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUnHideAllGeometryCollectionCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUnHideAllGeometryCollectionCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUnHideAllGeometryCollectionCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_15_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h_19_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class UUnHideAllGeometryCollectionCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_UnHideAllGeometryCollectionCommand_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
