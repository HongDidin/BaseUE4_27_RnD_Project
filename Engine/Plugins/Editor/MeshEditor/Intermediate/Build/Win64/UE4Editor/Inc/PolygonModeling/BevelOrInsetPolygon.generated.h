// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POLYGONMODELING_BevelOrInsetPolygon_generated_h
#error "BevelOrInsetPolygon.generated.h already included, missing '#pragma once' in BevelOrInsetPolygon.h"
#endif
#define POLYGONMODELING_BevelOrInsetPolygon_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBevelPolygonCommand(); \
	friend struct Z_Construct_UClass_UBevelPolygonCommand_Statics; \
public: \
	DECLARE_CLASS(UBevelPolygonCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UBevelPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUBevelPolygonCommand(); \
	friend struct Z_Construct_UClass_UBevelPolygonCommand_Statics; \
public: \
	DECLARE_CLASS(UBevelPolygonCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UBevelPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBevelPolygonCommand(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBevelPolygonCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBevelPolygonCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBevelPolygonCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBevelPolygonCommand(UBevelPolygonCommand&&); \
	NO_API UBevelPolygonCommand(const UBevelPolygonCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBevelPolygonCommand(UBevelPolygonCommand&&); \
	NO_API UBevelPolygonCommand(const UBevelPolygonCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBevelPolygonCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBevelPolygonCommand); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBevelPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_10_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class UBevelPolygonCommand>();

#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUInsetPolygonCommand(); \
	friend struct Z_Construct_UClass_UInsetPolygonCommand_Statics; \
public: \
	DECLARE_CLASS(UInsetPolygonCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UInsetPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_INCLASS \
private: \
	static void StaticRegisterNativesUInsetPolygonCommand(); \
	friend struct Z_Construct_UClass_UInsetPolygonCommand_Statics; \
public: \
	DECLARE_CLASS(UInsetPolygonCommand, UMeshEditorEditCommand, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PolygonModeling"), NO_API) \
	DECLARE_SERIALIZER(UInsetPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInsetPolygonCommand(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInsetPolygonCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInsetPolygonCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInsetPolygonCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInsetPolygonCommand(UInsetPolygonCommand&&); \
	NO_API UInsetPolygonCommand(const UInsetPolygonCommand&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UInsetPolygonCommand(UInsetPolygonCommand&&); \
	NO_API UInsetPolygonCommand(const UInsetPolygonCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInsetPolygonCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInsetPolygonCommand); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UInsetPolygonCommand)


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_38_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h_41_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POLYGONMODELING_API UClass* StaticClass<class UInsetPolygonCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_PolygonModeling_Public_BevelOrInsetPolygon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
