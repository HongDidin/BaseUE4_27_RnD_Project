// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/AssignMaterial.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAssignMaterial() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_UAssignMaterialCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_UAssignMaterialCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void UAssignMaterialCommand::StaticRegisterNativesUAssignMaterialCommand()
	{
	}
	UClass* Z_Construct_UClass_UAssignMaterialCommand_NoRegister()
	{
		return UAssignMaterialCommand::StaticClass();
	}
	struct Z_Construct_UClass_UAssignMaterialCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAssignMaterialCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAssignMaterialCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Assigns the highlighted material to the currently selected polygon(s) */" },
		{ "IncludePath", "AssignMaterial.h" },
		{ "ModuleRelativePath", "Public/AssignMaterial.h" },
		{ "ToolTip", "Assigns the highlighted material to the currently selected polygon(s)" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAssignMaterialCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAssignMaterialCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAssignMaterialCommand_Statics::ClassParams = {
		&UAssignMaterialCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAssignMaterialCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAssignMaterialCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAssignMaterialCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAssignMaterialCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAssignMaterialCommand, 3159864368);
	template<> POLYGONMODELING_API UClass* StaticClass<UAssignMaterialCommand>()
	{
		return UAssignMaterialCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAssignMaterialCommand(Z_Construct_UClass_UAssignMaterialCommand, &UAssignMaterialCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("UAssignMaterialCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAssignMaterialCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
