// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHEDITOR_WireframeMeshComponent_generated_h
#error "WireframeMeshComponent.generated.h already included, missing '#pragma once' in WireframeMeshComponent.h"
#endif
#define MESHEDITOR_WireframeMeshComponent_generated_h

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWireframeMesh(); \
	friend struct Z_Construct_UClass_UWireframeMesh_Statics; \
public: \
	DECLARE_CLASS(UWireframeMesh, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UWireframeMesh)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_INCLASS \
private: \
	static void StaticRegisterNativesUWireframeMesh(); \
	friend struct Z_Construct_UClass_UWireframeMesh_Statics; \
public: \
	DECLARE_CLASS(UWireframeMesh, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UWireframeMesh)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWireframeMesh(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWireframeMesh) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWireframeMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWireframeMesh); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWireframeMesh(UWireframeMesh&&); \
	NO_API UWireframeMesh(const UWireframeMesh&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWireframeMesh(UWireframeMesh&&); \
	NO_API UWireframeMesh(const UWireframeMesh&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWireframeMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWireframeMesh); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UWireframeMesh)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_48_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_51_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UWireframeMesh>();

#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_SPARSE_DATA
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_RPC_WRAPPERS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWireframeMeshComponent(); \
	friend struct Z_Construct_UClass_UWireframeMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UWireframeMeshComponent, UMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UWireframeMeshComponent)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_INCLASS \
private: \
	static void StaticRegisterNativesUWireframeMeshComponent(); \
	friend struct Z_Construct_UClass_UWireframeMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UWireframeMeshComponent, UMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MeshEditor"), NO_API) \
	DECLARE_SERIALIZER(UWireframeMeshComponent)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWireframeMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWireframeMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWireframeMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWireframeMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWireframeMeshComponent(UWireframeMeshComponent&&); \
	NO_API UWireframeMeshComponent(const UWireframeMeshComponent&); \
public:


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWireframeMeshComponent(UWireframeMeshComponent&&); \
	NO_API UWireframeMeshComponent(const UWireframeMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWireframeMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWireframeMeshComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWireframeMeshComponent)


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__WireframeMesh() { return STRUCT_OFFSET(UWireframeMeshComponent, WireframeMesh); }


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_100_PROLOG
#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_RPC_WRAPPERS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_INCLASS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_SPARSE_DATA \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h_103_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHEDITOR_API UClass* StaticClass<class UWireframeMeshComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_MeshEditor_Source_MeshEditor_Public_WireframeMeshComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
