// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/Public/MeshEditorSelectionModifiers.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshEditorSelectionModifiers() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorSelectionModifier_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorSelectionModifier();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
	MESHEDITOR_API UClass* Z_Construct_UClass_USelectSingleMeshElement_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_USelectSingleMeshElement();
	MESHEDITOR_API UClass* Z_Construct_UClass_USelectPolygonsByGroup_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_USelectPolygonsByGroup();
	MESHEDITOR_API UClass* Z_Construct_UClass_USelectPolygonsByConnectivity_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_USelectPolygonsByConnectivity();
	MESHEDITOR_API UClass* Z_Construct_UClass_USelectPolygonsBySmoothingGroup_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_USelectPolygonsBySmoothingGroup();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorSelectionModifiersList_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorSelectionModifiersList();
// End Cross Module References
	void UMeshEditorSelectionModifier::StaticRegisterNativesUMeshEditorSelectionModifier()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorSelectionModifier_NoRegister()
	{
		return UMeshEditorSelectionModifier::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorSelectionModifier_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorSelectionModifier_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSelectionModifier_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshEditorSelectionModifiers.h" },
		{ "ModuleRelativePath", "Public/MeshEditorSelectionModifiers.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorSelectionModifier_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorSelectionModifier>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorSelectionModifier_Statics::ClassParams = {
		&UMeshEditorSelectionModifier::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSelectionModifier_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSelectionModifier_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorSelectionModifier()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorSelectionModifier_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorSelectionModifier, 983094386);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorSelectionModifier>()
	{
		return UMeshEditorSelectionModifier::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorSelectionModifier(Z_Construct_UClass_UMeshEditorSelectionModifier, &UMeshEditorSelectionModifier::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorSelectionModifier"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorSelectionModifier);
	void USelectSingleMeshElement::StaticRegisterNativesUSelectSingleMeshElement()
	{
	}
	UClass* Z_Construct_UClass_USelectSingleMeshElement_NoRegister()
	{
		return USelectSingleMeshElement::StaticClass();
	}
	struct Z_Construct_UClass_USelectSingleMeshElement_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectSingleMeshElement_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorSelectionModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectSingleMeshElement_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *  Dummy selection modifier that doesn't actually modifies the selection.\n */" },
		{ "IncludePath", "MeshEditorSelectionModifiers.h" },
		{ "ModuleRelativePath", "Public/MeshEditorSelectionModifiers.h" },
		{ "ToolTip", "Dummy selection modifier that doesn't actually modifies the selection." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectSingleMeshElement_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelectSingleMeshElement>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelectSingleMeshElement_Statics::ClassParams = {
		&USelectSingleMeshElement::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USelectSingleMeshElement_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectSingleMeshElement_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectSingleMeshElement()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelectSingleMeshElement_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelectSingleMeshElement, 2976228236);
	template<> MESHEDITOR_API UClass* StaticClass<USelectSingleMeshElement>()
	{
		return USelectSingleMeshElement::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelectSingleMeshElement(Z_Construct_UClass_USelectSingleMeshElement, &USelectSingleMeshElement::StaticClass, TEXT("/Script/MeshEditor"), TEXT("USelectSingleMeshElement"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectSingleMeshElement);
	void USelectPolygonsByGroup::StaticRegisterNativesUSelectPolygonsByGroup()
	{
	}
	UClass* Z_Construct_UClass_USelectPolygonsByGroup_NoRegister()
	{
		return USelectPolygonsByGroup::StaticClass();
	}
	struct Z_Construct_UClass_USelectPolygonsByGroup_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectPolygonsByGroup_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorSelectionModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectPolygonsByGroup_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Selects all the polygons that are part of the selection polygons group ids.\n */" },
		{ "IncludePath", "MeshEditorSelectionModifiers.h" },
		{ "ModuleRelativePath", "Public/MeshEditorSelectionModifiers.h" },
		{ "ToolTip", "Selects all the polygons that are part of the selection polygons group ids." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectPolygonsByGroup_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelectPolygonsByGroup>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelectPolygonsByGroup_Statics::ClassParams = {
		&USelectPolygonsByGroup::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USelectPolygonsByGroup_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectPolygonsByGroup_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectPolygonsByGroup()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelectPolygonsByGroup_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelectPolygonsByGroup, 490632618);
	template<> MESHEDITOR_API UClass* StaticClass<USelectPolygonsByGroup>()
	{
		return USelectPolygonsByGroup::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelectPolygonsByGroup(Z_Construct_UClass_USelectPolygonsByGroup, &USelectPolygonsByGroup::StaticClass, TEXT("/Script/MeshEditor"), TEXT("USelectPolygonsByGroup"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectPolygonsByGroup);
	void USelectPolygonsByConnectivity::StaticRegisterNativesUSelectPolygonsByConnectivity()
	{
	}
	UClass* Z_Construct_UClass_USelectPolygonsByConnectivity_NoRegister()
	{
		return USelectPolygonsByConnectivity::StaticClass();
	}
	struct Z_Construct_UClass_USelectPolygonsByConnectivity_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectPolygonsByConnectivity_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorSelectionModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectPolygonsByConnectivity_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Selects all the polygons that are connected to the selection polygons.\n */" },
		{ "IncludePath", "MeshEditorSelectionModifiers.h" },
		{ "ModuleRelativePath", "Public/MeshEditorSelectionModifiers.h" },
		{ "ToolTip", "Selects all the polygons that are connected to the selection polygons." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectPolygonsByConnectivity_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelectPolygonsByConnectivity>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelectPolygonsByConnectivity_Statics::ClassParams = {
		&USelectPolygonsByConnectivity::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USelectPolygonsByConnectivity_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectPolygonsByConnectivity_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectPolygonsByConnectivity()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelectPolygonsByConnectivity_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelectPolygonsByConnectivity, 2873747352);
	template<> MESHEDITOR_API UClass* StaticClass<USelectPolygonsByConnectivity>()
	{
		return USelectPolygonsByConnectivity::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelectPolygonsByConnectivity(Z_Construct_UClass_USelectPolygonsByConnectivity, &USelectPolygonsByConnectivity::StaticClass, TEXT("/Script/MeshEditor"), TEXT("USelectPolygonsByConnectivity"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectPolygonsByConnectivity);
	void USelectPolygonsBySmoothingGroup::StaticRegisterNativesUSelectPolygonsBySmoothingGroup()
	{
	}
	UClass* Z_Construct_UClass_USelectPolygonsBySmoothingGroup_NoRegister()
	{
		return USelectPolygonsBySmoothingGroup::StaticClass();
	}
	struct Z_Construct_UClass_USelectPolygonsBySmoothingGroup_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectPolygonsBySmoothingGroup_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorSelectionModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectPolygonsBySmoothingGroup_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Selects all the polygons that have the same smoothing group as the selection polygons.\n */" },
		{ "IncludePath", "MeshEditorSelectionModifiers.h" },
		{ "ModuleRelativePath", "Public/MeshEditorSelectionModifiers.h" },
		{ "ToolTip", "Selects all the polygons that have the same smoothing group as the selection polygons." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectPolygonsBySmoothingGroup_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelectPolygonsBySmoothingGroup>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelectPolygonsBySmoothingGroup_Statics::ClassParams = {
		&USelectPolygonsBySmoothingGroup::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USelectPolygonsBySmoothingGroup_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectPolygonsBySmoothingGroup_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectPolygonsBySmoothingGroup()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelectPolygonsBySmoothingGroup_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelectPolygonsBySmoothingGroup, 3831918244);
	template<> MESHEDITOR_API UClass* StaticClass<USelectPolygonsBySmoothingGroup>()
	{
		return USelectPolygonsBySmoothingGroup::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelectPolygonsBySmoothingGroup(Z_Construct_UClass_USelectPolygonsBySmoothingGroup, &USelectPolygonsBySmoothingGroup::StaticClass, TEXT("/Script/MeshEditor"), TEXT("USelectPolygonsBySmoothingGroup"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectPolygonsBySmoothingGroup);
	void UMeshEditorSelectionModifiersList::StaticRegisterNativesUMeshEditorSelectionModifiersList()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorSelectionModifiersList_NoRegister()
	{
		return UMeshEditorSelectionModifiersList::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectionModifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionModifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SelectionModifiers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshEditorSelectionModifiers.h" },
		{ "ModuleRelativePath", "Public/MeshEditorSelectionModifiers.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::NewProp_SelectionModifiers_Inner = { "SelectionModifiers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshEditorSelectionModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::NewProp_SelectionModifiers_MetaData[] = {
		{ "Comment", "/** All of the selection modifiers that were registered at startup */" },
		{ "ModuleRelativePath", "Public/MeshEditorSelectionModifiers.h" },
		{ "ToolTip", "All of the selection modifiers that were registered at startup" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::NewProp_SelectionModifiers = { "SelectionModifiers", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorSelectionModifiersList, SelectionModifiers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::NewProp_SelectionModifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::NewProp_SelectionModifiers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::NewProp_SelectionModifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::NewProp_SelectionModifiers,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorSelectionModifiersList>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::ClassParams = {
		&UMeshEditorSelectionModifiersList::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorSelectionModifiersList()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorSelectionModifiersList_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorSelectionModifiersList, 1075739033);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorSelectionModifiersList>()
	{
		return UMeshEditorSelectionModifiersList::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorSelectionModifiersList(Z_Construct_UClass_UMeshEditorSelectionModifiersList, &UMeshEditorSelectionModifiersList::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorSelectionModifiersList"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorSelectionModifiersList);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
