// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/Public/MeshEditorStaticMeshAdapter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshEditorStaticMeshAdapter() {}
// Cross Module References
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorStaticMeshAdapter_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorStaticMeshAdapter();
	EDITABLEMESH_API UClass* Z_Construct_UClass_UEditableMeshAdapter();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
	MESHEDITOR_API UClass* Z_Construct_UClass_UWireframeMesh_NoRegister();
// End Cross Module References
	void UMeshEditorStaticMeshAdapter::StaticRegisterNativesUMeshEditorStaticMeshAdapter()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditorStaticMeshAdapter_NoRegister()
	{
		return UMeshEditorStaticMeshAdapter::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WireframeMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WireframeMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshLODIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StaticMeshLODIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditableMeshAdapter,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshEditorStaticMeshAdapter.h" },
		{ "ModuleRelativePath", "Public/MeshEditorStaticMeshAdapter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::NewProp_WireframeMesh_MetaData[] = {
		{ "Comment", "/** The wireframe mesh asset we're representing */" },
		{ "ModuleRelativePath", "Public/MeshEditorStaticMeshAdapter.h" },
		{ "ToolTip", "The wireframe mesh asset we're representing" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::NewProp_WireframeMesh = { "WireframeMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorStaticMeshAdapter, WireframeMesh), Z_Construct_UClass_UWireframeMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::NewProp_WireframeMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::NewProp_WireframeMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::NewProp_StaticMeshLODIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshEditorStaticMeshAdapter.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::NewProp_StaticMeshLODIndex = { "StaticMeshLODIndex", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditorStaticMeshAdapter, StaticMeshLODIndex), METADATA_PARAMS(Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::NewProp_StaticMeshLODIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::NewProp_StaticMeshLODIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::NewProp_WireframeMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::NewProp_StaticMeshLODIndex,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditorStaticMeshAdapter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::ClassParams = {
		&UMeshEditorStaticMeshAdapter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditorStaticMeshAdapter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditorStaticMeshAdapter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditorStaticMeshAdapter, 308955002);
	template<> MESHEDITOR_API UClass* StaticClass<UMeshEditorStaticMeshAdapter>()
	{
		return UMeshEditorStaticMeshAdapter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditorStaticMeshAdapter(Z_Construct_UClass_UMeshEditorStaticMeshAdapter, &UMeshEditorStaticMeshAdapter::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UMeshEditorStaticMeshAdapter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditorStaticMeshAdapter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
