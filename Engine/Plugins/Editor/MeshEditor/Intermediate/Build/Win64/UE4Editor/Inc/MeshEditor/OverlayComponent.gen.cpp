// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshEditor/Public/OverlayComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOverlayComponent() {}
// Cross Module References
	MESHEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FOverlayTriangleID();
	UPackage* Z_Construct_UPackage__Script_MeshEditor();
	MESHEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FOverlayPointID();
	MESHEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FOverlayLineID();
	MESHEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FOverlayTriangle();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	MESHEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FOverlayTriangleVertex();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	MESHEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FOverlayPoint();
	MESHEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FOverlayLine();
	MESHEDITOR_API UClass* Z_Construct_UClass_UOverlayComponent_NoRegister();
	MESHEDITOR_API UClass* Z_Construct_UClass_UOverlayComponent();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FBoxSphereBounds();
// End Cross Module References
class UScriptStruct* FOverlayTriangleID::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FOverlayTriangleID_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOverlayTriangleID, Z_Construct_UPackage__Script_MeshEditor(), TEXT("OverlayTriangleID"), sizeof(FOverlayTriangleID), Get_Z_Construct_UScriptStruct_FOverlayTriangleID_Hash());
	}
	return Singleton;
}
template<> MESHEDITOR_API UScriptStruct* StaticStruct<FOverlayTriangleID>()
{
	return FOverlayTriangleID::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOverlayTriangleID(FOverlayTriangleID::StaticStruct, TEXT("/Script/MeshEditor"), TEXT("OverlayTriangleID"), false, nullptr, nullptr);
static struct FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayTriangleID
{
	FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayTriangleID()
	{
		UScriptStruct::DeferCppStructOps<FOverlayTriangleID>(FName(TEXT("OverlayTriangleID")));
	}
} ScriptStruct_MeshEditor_StaticRegisterNativesFOverlayTriangleID;
	struct Z_Construct_UScriptStruct_FOverlayTriangleID_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOverlayTriangleID>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::NewProp_ID_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::NewProp_ID = { "ID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayTriangleID, ID), METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::NewProp_ID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::NewProp_ID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::NewProp_ID,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
		nullptr,
		&NewStructOps,
		"OverlayTriangleID",
		sizeof(FOverlayTriangleID),
		alignof(FOverlayTriangleID),
		Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOverlayTriangleID()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOverlayTriangleID_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OverlayTriangleID"), sizeof(FOverlayTriangleID), Get_Z_Construct_UScriptStruct_FOverlayTriangleID_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOverlayTriangleID_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOverlayTriangleID_Hash() { return 2867752909U; }
class UScriptStruct* FOverlayPointID::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FOverlayPointID_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOverlayPointID, Z_Construct_UPackage__Script_MeshEditor(), TEXT("OverlayPointID"), sizeof(FOverlayPointID), Get_Z_Construct_UScriptStruct_FOverlayPointID_Hash());
	}
	return Singleton;
}
template<> MESHEDITOR_API UScriptStruct* StaticStruct<FOverlayPointID>()
{
	return FOverlayPointID::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOverlayPointID(FOverlayPointID::StaticStruct, TEXT("/Script/MeshEditor"), TEXT("OverlayPointID"), false, nullptr, nullptr);
static struct FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayPointID
{
	FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayPointID()
	{
		UScriptStruct::DeferCppStructOps<FOverlayPointID>(FName(TEXT("OverlayPointID")));
	}
} ScriptStruct_MeshEditor_StaticRegisterNativesFOverlayPointID;
	struct Z_Construct_UScriptStruct_FOverlayPointID_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayPointID_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOverlayPointID_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOverlayPointID>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayPointID_Statics::NewProp_ID_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOverlayPointID_Statics::NewProp_ID = { "ID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayPointID, ID), METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayPointID_Statics::NewProp_ID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayPointID_Statics::NewProp_ID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOverlayPointID_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayPointID_Statics::NewProp_ID,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOverlayPointID_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
		nullptr,
		&NewStructOps,
		"OverlayPointID",
		sizeof(FOverlayPointID),
		alignof(FOverlayPointID),
		Z_Construct_UScriptStruct_FOverlayPointID_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayPointID_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayPointID_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayPointID_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOverlayPointID()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOverlayPointID_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OverlayPointID"), sizeof(FOverlayPointID), Get_Z_Construct_UScriptStruct_FOverlayPointID_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOverlayPointID_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOverlayPointID_Hash() { return 365552174U; }
class UScriptStruct* FOverlayLineID::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FOverlayLineID_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOverlayLineID, Z_Construct_UPackage__Script_MeshEditor(), TEXT("OverlayLineID"), sizeof(FOverlayLineID), Get_Z_Construct_UScriptStruct_FOverlayLineID_Hash());
	}
	return Singleton;
}
template<> MESHEDITOR_API UScriptStruct* StaticStruct<FOverlayLineID>()
{
	return FOverlayLineID::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOverlayLineID(FOverlayLineID::StaticStruct, TEXT("/Script/MeshEditor"), TEXT("OverlayLineID"), false, nullptr, nullptr);
static struct FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayLineID
{
	FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayLineID()
	{
		UScriptStruct::DeferCppStructOps<FOverlayLineID>(FName(TEXT("OverlayLineID")));
	}
} ScriptStruct_MeshEditor_StaticRegisterNativesFOverlayLineID;
	struct Z_Construct_UScriptStruct_FOverlayLineID_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayLineID_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOverlayLineID_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOverlayLineID>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayLineID_Statics::NewProp_ID_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOverlayLineID_Statics::NewProp_ID = { "ID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayLineID, ID), METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayLineID_Statics::NewProp_ID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayLineID_Statics::NewProp_ID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOverlayLineID_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayLineID_Statics::NewProp_ID,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOverlayLineID_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
		nullptr,
		&NewStructOps,
		"OverlayLineID",
		sizeof(FOverlayLineID),
		alignof(FOverlayLineID),
		Z_Construct_UScriptStruct_FOverlayLineID_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayLineID_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayLineID_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayLineID_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOverlayLineID()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOverlayLineID_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OverlayLineID"), sizeof(FOverlayLineID), Get_Z_Construct_UScriptStruct_FOverlayLineID_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOverlayLineID_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOverlayLineID_Hash() { return 3720795820U; }
class UScriptStruct* FOverlayTriangle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FOverlayTriangle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOverlayTriangle, Z_Construct_UPackage__Script_MeshEditor(), TEXT("OverlayTriangle"), sizeof(FOverlayTriangle), Get_Z_Construct_UScriptStruct_FOverlayTriangle_Hash());
	}
	return Singleton;
}
template<> MESHEDITOR_API UScriptStruct* StaticStruct<FOverlayTriangle>()
{
	return FOverlayTriangle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOverlayTriangle(FOverlayTriangle::StaticStruct, TEXT("/Script/MeshEditor"), TEXT("OverlayTriangle"), false, nullptr, nullptr);
static struct FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayTriangle
{
	FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayTriangle()
	{
		UScriptStruct::DeferCppStructOps<FOverlayTriangle>(FName(TEXT("OverlayTriangle")));
	}
} ScriptStruct_MeshEditor_StaticRegisterNativesFOverlayTriangle;
	struct Z_Construct_UScriptStruct_FOverlayTriangle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vertex0_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vertex0;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vertex1_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vertex1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vertex2_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vertex2;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangle_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOverlayTriangle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Material_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayTriangle, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Material_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex0_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex0 = { "Vertex0", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayTriangle, Vertex0), Z_Construct_UScriptStruct_FOverlayTriangleVertex, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex0_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex0_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex1_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex1 = { "Vertex1", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayTriangle, Vertex1), Z_Construct_UScriptStruct_FOverlayTriangleVertex, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex2_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex2 = { "Vertex2", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayTriangle, Vertex2), Z_Construct_UScriptStruct_FOverlayTriangleVertex, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex2_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex2_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOverlayTriangle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex0,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayTriangle_Statics::NewProp_Vertex2,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOverlayTriangle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
		nullptr,
		&NewStructOps,
		"OverlayTriangle",
		sizeof(FOverlayTriangle),
		alignof(FOverlayTriangle),
		Z_Construct_UScriptStruct_FOverlayTriangle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOverlayTriangle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOverlayTriangle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OverlayTriangle"), sizeof(FOverlayTriangle), Get_Z_Construct_UScriptStruct_FOverlayTriangle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOverlayTriangle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOverlayTriangle_Hash() { return 3011287387U; }
class UScriptStruct* FOverlayTriangleVertex::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FOverlayTriangleVertex_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOverlayTriangleVertex, Z_Construct_UPackage__Script_MeshEditor(), TEXT("OverlayTriangleVertex"), sizeof(FOverlayTriangleVertex), Get_Z_Construct_UScriptStruct_FOverlayTriangleVertex_Hash());
	}
	return Singleton;
}
template<> MESHEDITOR_API UScriptStruct* StaticStruct<FOverlayTriangleVertex>()
{
	return FOverlayTriangleVertex::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOverlayTriangleVertex(FOverlayTriangleVertex::StaticStruct, TEXT("/Script/MeshEditor"), TEXT("OverlayTriangleVertex"), false, nullptr, nullptr);
static struct FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayTriangleVertex
{
	FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayTriangleVertex()
	{
		UScriptStruct::DeferCppStructOps<FOverlayTriangleVertex>(FName(TEXT("OverlayTriangleVertex")));
	}
} ScriptStruct_MeshEditor_StaticRegisterNativesFOverlayTriangleVertex;
	struct Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UV_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UV;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Normal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Normal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOverlayTriangleVertex>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Position_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayTriangleVertex, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_UV_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_UV = { "UV", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayTriangleVertex, UV), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_UV_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_UV_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Normal_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Normal = { "Normal", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayTriangleVertex, Normal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Normal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Normal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Color_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayTriangleVertex, Color), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Color_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_UV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Normal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::NewProp_Color,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
		nullptr,
		&NewStructOps,
		"OverlayTriangleVertex",
		sizeof(FOverlayTriangleVertex),
		alignof(FOverlayTriangleVertex),
		Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOverlayTriangleVertex()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOverlayTriangleVertex_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OverlayTriangleVertex"), sizeof(FOverlayTriangleVertex), Get_Z_Construct_UScriptStruct_FOverlayTriangleVertex_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOverlayTriangleVertex_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOverlayTriangleVertex_Hash() { return 182612866U; }
class UScriptStruct* FOverlayPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FOverlayPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOverlayPoint, Z_Construct_UPackage__Script_MeshEditor(), TEXT("OverlayPoint"), sizeof(FOverlayPoint), Get_Z_Construct_UScriptStruct_FOverlayPoint_Hash());
	}
	return Singleton;
}
template<> MESHEDITOR_API UScriptStruct* StaticStruct<FOverlayPoint>()
{
	return FOverlayPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOverlayPoint(FOverlayPoint::StaticStruct, TEXT("/Script/MeshEditor"), TEXT("OverlayPoint"), false, nullptr, nullptr);
static struct FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayPoint
{
	FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayPoint()
	{
		UScriptStruct::DeferCppStructOps<FOverlayPoint>(FName(TEXT("OverlayPoint")));
	}
} ScriptStruct_MeshEditor_StaticRegisterNativesFOverlayPoint;
	struct Z_Construct_UScriptStruct_FOverlayPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayPoint_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOverlayPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Position_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayPoint, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Color_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayPoint, Color), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Size_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayPoint, Size), METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Size_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOverlayPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayPoint_Statics::NewProp_Size,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOverlayPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
		nullptr,
		&NewStructOps,
		"OverlayPoint",
		sizeof(FOverlayPoint),
		alignof(FOverlayPoint),
		Z_Construct_UScriptStruct_FOverlayPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOverlayPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOverlayPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OverlayPoint"), sizeof(FOverlayPoint), Get_Z_Construct_UScriptStruct_FOverlayPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOverlayPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOverlayPoint_Hash() { return 3195096294U; }
class UScriptStruct* FOverlayLine::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FOverlayLine_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOverlayLine, Z_Construct_UPackage__Script_MeshEditor(), TEXT("OverlayLine"), sizeof(FOverlayLine), Get_Z_Construct_UScriptStruct_FOverlayLine_Hash());
	}
	return Singleton;
}
template<> MESHEDITOR_API UScriptStruct* StaticStruct<FOverlayLine>()
{
	return FOverlayLine::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOverlayLine(FOverlayLine::StaticStruct, TEXT("/Script/MeshEditor"), TEXT("OverlayLine"), false, nullptr, nullptr);
static struct FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayLine
{
	FScriptStruct_MeshEditor_StaticRegisterNativesFOverlayLine()
	{
		UScriptStruct::DeferCppStructOps<FOverlayLine>(FName(TEXT("OverlayLine")));
	}
} ScriptStruct_MeshEditor_StaticRegisterNativesFOverlayLine;
	struct Z_Construct_UScriptStruct_FOverlayLine_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Start_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Start;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_End_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_End;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Thickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Thickness;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayLine_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOverlayLine_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOverlayLine>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Start_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Start = { "Start", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayLine, Start), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Start_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Start_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_End_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_End = { "End", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayLine, End), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_End_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_End_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Color_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayLine, Color), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Thickness_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Thickness = { "Thickness", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOverlayLine, Thickness), METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Thickness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Thickness_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOverlayLine_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Start,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_End,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOverlayLine_Statics::NewProp_Thickness,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOverlayLine_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
		nullptr,
		&NewStructOps,
		"OverlayLine",
		sizeof(FOverlayLine),
		alignof(FOverlayLine),
		Z_Construct_UScriptStruct_FOverlayLine_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayLine_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOverlayLine_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOverlayLine_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOverlayLine()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOverlayLine_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OverlayLine"), sizeof(FOverlayLine), Get_Z_Construct_UScriptStruct_FOverlayLine_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOverlayLine_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOverlayLine_Hash() { return 3594615065U; }
	void UOverlayComponent::StaticRegisterNativesUOverlayComponent()
	{
	}
	UClass* Z_Construct_UClass_UOverlayComponent_NoRegister()
	{
		return UOverlayComponent::StaticClass();
	}
	struct Z_Construct_UClass_UOverlayComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LineMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PointMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bounds_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Bounds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoundsDirty_MetaData[];
#endif
		static void NewProp_bBoundsDirty_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoundsDirty;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOverlayComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOverlayComponent_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Mobility Trigger" },
		{ "IncludePath", "OverlayComponent.h" },
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOverlayComponent_Statics::NewProp_LineMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOverlayComponent_Statics::NewProp_LineMaterial = { "LineMaterial", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOverlayComponent, LineMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOverlayComponent_Statics::NewProp_LineMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOverlayComponent_Statics::NewProp_LineMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOverlayComponent_Statics::NewProp_PointMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOverlayComponent_Statics::NewProp_PointMaterial = { "PointMaterial", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOverlayComponent, PointMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOverlayComponent_Statics::NewProp_PointMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOverlayComponent_Statics::NewProp_PointMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOverlayComponent_Statics::NewProp_Bounds_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOverlayComponent_Statics::NewProp_Bounds = { "Bounds", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOverlayComponent, Bounds), Z_Construct_UScriptStruct_FBoxSphereBounds, METADATA_PARAMS(Z_Construct_UClass_UOverlayComponent_Statics::NewProp_Bounds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOverlayComponent_Statics::NewProp_Bounds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOverlayComponent_Statics::NewProp_bBoundsDirty_MetaData[] = {
		{ "ModuleRelativePath", "Public/OverlayComponent.h" },
	};
#endif
	void Z_Construct_UClass_UOverlayComponent_Statics::NewProp_bBoundsDirty_SetBit(void* Obj)
	{
		((UOverlayComponent*)Obj)->bBoundsDirty = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOverlayComponent_Statics::NewProp_bBoundsDirty = { "bBoundsDirty", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOverlayComponent), &Z_Construct_UClass_UOverlayComponent_Statics::NewProp_bBoundsDirty_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOverlayComponent_Statics::NewProp_bBoundsDirty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOverlayComponent_Statics::NewProp_bBoundsDirty_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOverlayComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOverlayComponent_Statics::NewProp_LineMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOverlayComponent_Statics::NewProp_PointMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOverlayComponent_Statics::NewProp_Bounds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOverlayComponent_Statics::NewProp_bBoundsDirty,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOverlayComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOverlayComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOverlayComponent_Statics::ClassParams = {
		&UOverlayComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOverlayComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOverlayComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOverlayComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOverlayComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOverlayComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOverlayComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOverlayComponent, 355404249);
	template<> MESHEDITOR_API UClass* StaticClass<UOverlayComponent>()
	{
		return UOverlayComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOverlayComponent(Z_Construct_UClass_UOverlayComponent, &UOverlayComponent::StaticClass, TEXT("/Script/MeshEditor"), TEXT("UOverlayComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOverlayComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
