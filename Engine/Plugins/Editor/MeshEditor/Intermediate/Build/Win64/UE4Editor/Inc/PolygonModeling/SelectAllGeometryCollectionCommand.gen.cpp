// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/SelectAllGeometryCollectionCommand.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSelectAllGeometryCollectionCommand() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_USelectAllGeometryCollectionCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_USelectAllGeometryCollectionCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void USelectAllGeometryCollectionCommand::StaticRegisterNativesUSelectAllGeometryCollectionCommand()
	{
	}
	UClass* Z_Construct_UClass_USelectAllGeometryCollectionCommand_NoRegister()
	{
		return USelectAllGeometryCollectionCommand::StaticClass();
	}
	struct Z_Construct_UClass_USelectAllGeometryCollectionCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectAllGeometryCollectionCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectAllGeometryCollectionCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Select All chunks in mesh */" },
		{ "IncludePath", "SelectAllGeometryCollectionCommand.h" },
		{ "ModuleRelativePath", "SelectAllGeometryCollectionCommand.h" },
		{ "ToolTip", "Select All chunks in mesh" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectAllGeometryCollectionCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelectAllGeometryCollectionCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USelectAllGeometryCollectionCommand_Statics::ClassParams = {
		&USelectAllGeometryCollectionCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USelectAllGeometryCollectionCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectAllGeometryCollectionCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectAllGeometryCollectionCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USelectAllGeometryCollectionCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USelectAllGeometryCollectionCommand, 1981498138);
	template<> POLYGONMODELING_API UClass* StaticClass<USelectAllGeometryCollectionCommand>()
	{
		return USelectAllGeometryCollectionCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USelectAllGeometryCollectionCommand(Z_Construct_UClass_USelectAllGeometryCollectionCommand, &USelectAllGeometryCollectionCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("USelectAllGeometryCollectionCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectAllGeometryCollectionCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
