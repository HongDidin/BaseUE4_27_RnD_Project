// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PolygonModeling/Public/RemoveEdge.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoveEdge() {}
// Cross Module References
	POLYGONMODELING_API UClass* Z_Construct_UClass_URemoveEdgeCommand_NoRegister();
	POLYGONMODELING_API UClass* Z_Construct_UClass_URemoveEdgeCommand();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorInstantCommand();
	UPackage* Z_Construct_UPackage__Script_PolygonModeling();
// End Cross Module References
	void URemoveEdgeCommand::StaticRegisterNativesURemoveEdgeCommand()
	{
	}
	UClass* Z_Construct_UClass_URemoveEdgeCommand_NoRegister()
	{
		return URemoveEdgeCommand::StaticClass();
	}
	struct Z_Construct_UClass_URemoveEdgeCommand_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoveEdgeCommand_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorInstantCommand,
		(UObject* (*)())Z_Construct_UPackage__Script_PolygonModeling,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveEdgeCommand_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Attempts to remove the selected edge from the polygon, merging the adjacent polygons together */" },
		{ "IncludePath", "RemoveEdge.h" },
		{ "ModuleRelativePath", "Public/RemoveEdge.h" },
		{ "ToolTip", "Attempts to remove the selected edge from the polygon, merging the adjacent polygons together" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoveEdgeCommand_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoveEdgeCommand>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoveEdgeCommand_Statics::ClassParams = {
		&URemoveEdgeCommand::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoveEdgeCommand_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveEdgeCommand_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoveEdgeCommand()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoveEdgeCommand_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoveEdgeCommand, 805799559);
	template<> POLYGONMODELING_API UClass* StaticClass<URemoveEdgeCommand>()
	{
		return URemoveEdgeCommand::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoveEdgeCommand(Z_Construct_UClass_URemoveEdgeCommand, &URemoveEdgeCommand::StaticClass, TEXT("/Script/PolygonModeling"), TEXT("URemoveEdgeCommand"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoveEdgeCommand);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
