// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPEEDTREEIMPORTER_SpeedTreeImportFactory_generated_h
#error "SpeedTreeImportFactory.generated.h already included, missing '#pragma once' in SpeedTreeImportFactory.h"
#endif
#define SPEEDTREEIMPORTER_SpeedTreeImportFactory_generated_h

#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_SPARSE_DATA
#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_RPC_WRAPPERS
#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSpeedTreeImportFactory(); \
	friend struct Z_Construct_UClass_USpeedTreeImportFactory_Statics; \
public: \
	DECLARE_CLASS(USpeedTreeImportFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SpeedTreeImporter"), NO_API) \
	DECLARE_SERIALIZER(USpeedTreeImportFactory)


#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUSpeedTreeImportFactory(); \
	friend struct Z_Construct_UClass_USpeedTreeImportFactory_Statics; \
public: \
	DECLARE_CLASS(USpeedTreeImportFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SpeedTreeImporter"), NO_API) \
	DECLARE_SERIALIZER(USpeedTreeImportFactory)


#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpeedTreeImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpeedTreeImportFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpeedTreeImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpeedTreeImportFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpeedTreeImportFactory(USpeedTreeImportFactory&&); \
	NO_API USpeedTreeImportFactory(const USpeedTreeImportFactory&); \
public:


#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpeedTreeImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpeedTreeImportFactory(USpeedTreeImportFactory&&); \
	NO_API USpeedTreeImportFactory(const USpeedTreeImportFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpeedTreeImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpeedTreeImportFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpeedTreeImportFactory)


#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_17_PROLOG
#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_SPARSE_DATA \
	Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_RPC_WRAPPERS \
	Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_INCLASS \
	Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_SPARSE_DATA \
	Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SpeedTreeImportFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPEEDTREEIMPORTER_API UClass* StaticClass<class USpeedTreeImportFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_SpeedTreeImporter_Source_SpeedTreeImporter_Classes_SpeedTreeImportFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
