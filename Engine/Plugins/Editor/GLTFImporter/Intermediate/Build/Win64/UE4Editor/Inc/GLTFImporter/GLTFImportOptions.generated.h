// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GLTFIMPORTER_GLTFImportOptions_generated_h
#error "GLTFImportOptions.generated.h already included, missing '#pragma once' in GLTFImportOptions.h"
#endif
#define GLTFIMPORTER_GLTFImportOptions_generated_h

#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_SPARSE_DATA
#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_RPC_WRAPPERS
#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGLTFImportOptions(); \
	friend struct Z_Construct_UClass_UGLTFImportOptions_Statics; \
public: \
	DECLARE_CLASS(UGLTFImportOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GLTFImporter"), NO_API) \
	DECLARE_SERIALIZER(UGLTFImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUGLTFImportOptions(); \
	friend struct Z_Construct_UClass_UGLTFImportOptions_Statics; \
public: \
	DECLARE_CLASS(UGLTFImportOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GLTFImporter"), NO_API) \
	DECLARE_SERIALIZER(UGLTFImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGLTFImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGLTFImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGLTFImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGLTFImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGLTFImportOptions(UGLTFImportOptions&&); \
	NO_API UGLTFImportOptions(const UGLTFImportOptions&); \
public:


#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGLTFImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGLTFImportOptions(UGLTFImportOptions&&); \
	NO_API UGLTFImportOptions(const UGLTFImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGLTFImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGLTFImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGLTFImportOptions)


#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_12_PROLOG
#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_RPC_WRAPPERS \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_INCLASS \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GLTFImportOptions."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GLTFIMPORTER_API UClass* StaticClass<class UGLTFImportOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
