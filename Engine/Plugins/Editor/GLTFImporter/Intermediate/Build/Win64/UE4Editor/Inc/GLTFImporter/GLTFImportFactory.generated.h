// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GLTFIMPORTER_GLTFImportFactory_generated_h
#error "GLTFImportFactory.generated.h already included, missing '#pragma once' in GLTFImportFactory.h"
#endif
#define GLTFIMPORTER_GLTFImportFactory_generated_h

#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_SPARSE_DATA
#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_RPC_WRAPPERS
#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGLTFImportFactory(); \
	friend struct Z_Construct_UClass_UGLTFImportFactory_Statics; \
public: \
	DECLARE_CLASS(UGLTFImportFactory, UFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/GLTFImporter"), NO_API) \
	DECLARE_SERIALIZER(UGLTFImportFactory)


#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUGLTFImportFactory(); \
	friend struct Z_Construct_UClass_UGLTFImportFactory_Statics; \
public: \
	DECLARE_CLASS(UGLTFImportFactory, UFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/GLTFImporter"), NO_API) \
	DECLARE_SERIALIZER(UGLTFImportFactory)


#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGLTFImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGLTFImportFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGLTFImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGLTFImportFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGLTFImportFactory(UGLTFImportFactory&&); \
	NO_API UGLTFImportFactory(const UGLTFImportFactory&); \
public:


#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGLTFImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGLTFImportFactory(UGLTFImportFactory&&); \
	NO_API UGLTFImportFactory(const UGLTFImportFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGLTFImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGLTFImportFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGLTFImportFactory)


#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_13_PROLOG
#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_RPC_WRAPPERS \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_INCLASS \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GLTFImportFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GLTFIMPORTER_API UClass* StaticClass<class UGLTFImportFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_GLTFImporter_Source_GLTFImporter_Private_GLTFImportFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
