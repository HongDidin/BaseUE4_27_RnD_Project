// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GLTFImporter/Private/GLTFImportFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGLTFImportFactory() {}
// Cross Module References
	GLTFIMPORTER_API UClass* Z_Construct_UClass_UGLTFImportFactory_NoRegister();
	GLTFIMPORTER_API UClass* Z_Construct_UClass_UGLTFImportFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_GLTFImporter();
// End Cross Module References
	void UGLTFImportFactory::StaticRegisterNativesUGLTFImportFactory()
	{
	}
	UClass* Z_Construct_UClass_UGLTFImportFactory_NoRegister()
	{
		return UGLTFImportFactory::StaticClass();
	}
	struct Z_Construct_UClass_UGLTFImportFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGLTFImportFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_GLTFImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGLTFImportFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GLTFImportFactory.h" },
		{ "ModuleRelativePath", "Private/GLTFImportFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGLTFImportFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGLTFImportFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGLTFImportFactory_Statics::ClassParams = {
		&UGLTFImportFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGLTFImportFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGLTFImportFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGLTFImportFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGLTFImportFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGLTFImportFactory, 1769581170);
	template<> GLTFIMPORTER_API UClass* StaticClass<UGLTFImportFactory>()
	{
		return UGLTFImportFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGLTFImportFactory(Z_Construct_UClass_UGLTFImportFactory, &UGLTFImportFactory::StaticClass, TEXT("/Script/GLTFImporter"), TEXT("UGLTFImportFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGLTFImportFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
