// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PluginBrowser/Private/PluginMetadataObject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePluginMetadataObject() {}
// Cross Module References
	PLUGINBROWSER_API UScriptStruct* Z_Construct_UScriptStruct_FPluginReferenceMetadata();
	UPackage* Z_Construct_UPackage__Script_PluginBrowser();
	PLUGINBROWSER_API UClass* Z_Construct_UClass_UPluginMetadataObject_NoRegister();
	PLUGINBROWSER_API UClass* Z_Construct_UClass_UPluginMetadataObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FPluginReferenceMetadata::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PLUGINBROWSER_API uint32 Get_Z_Construct_UScriptStruct_FPluginReferenceMetadata_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPluginReferenceMetadata, Z_Construct_UPackage__Script_PluginBrowser(), TEXT("PluginReferenceMetadata"), sizeof(FPluginReferenceMetadata), Get_Z_Construct_UScriptStruct_FPluginReferenceMetadata_Hash());
	}
	return Singleton;
}
template<> PLUGINBROWSER_API UScriptStruct* StaticStruct<FPluginReferenceMetadata>()
{
	return FPluginReferenceMetadata::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPluginReferenceMetadata(FPluginReferenceMetadata::StaticStruct, TEXT("/Script/PluginBrowser"), TEXT("PluginReferenceMetadata"), false, nullptr, nullptr);
static struct FScriptStruct_PluginBrowser_StaticRegisterNativesFPluginReferenceMetadata
{
	FScriptStruct_PluginBrowser_StaticRegisterNativesFPluginReferenceMetadata()
	{
		UScriptStruct::DeferCppStructOps<FPluginReferenceMetadata>(FName(TEXT("PluginReferenceMetadata")));
	}
} ScriptStruct_PluginBrowser_StaticRegisterNativesFPluginReferenceMetadata;
	struct Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOptional_MetaData[];
#endif
		static void NewProp_bOptional_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOptional;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * We use this object to display plugin reference properties using details view.\n */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "We use this object to display plugin reference properties using details view." },
	};
#endif
	void* Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPluginReferenceMetadata>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Plugin Reference" },
		{ "Comment", "/** Name of the dependency plugin */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Name of the dependency plugin" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPluginReferenceMetadata, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bOptional_MetaData[] = {
		{ "Category", "Plugin Reference" },
		{ "Comment", "/** Whether the dependency plugin is optional meaning it will be silently ignored if not present */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Whether the dependency plugin is optional meaning it will be silently ignored if not present" },
	};
#endif
	void Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bOptional_SetBit(void* Obj)
	{
		((FPluginReferenceMetadata*)Obj)->bOptional = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bOptional = { "bOptional", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FPluginReferenceMetadata), &Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bOptional_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bOptional_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bOptional_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "Plugin Reference" },
		{ "Comment", "/** Whether the dependency plugin should be enabled by default */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Whether the dependency plugin should be enabled by default" },
	};
#endif
	void Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FPluginReferenceMetadata*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FPluginReferenceMetadata), &Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bOptional,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::NewProp_bEnabled,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_PluginBrowser,
		nullptr,
		&NewStructOps,
		"PluginReferenceMetadata",
		sizeof(FPluginReferenceMetadata),
		alignof(FPluginReferenceMetadata),
		Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPluginReferenceMetadata()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPluginReferenceMetadata_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_PluginBrowser();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PluginReferenceMetadata"), sizeof(FPluginReferenceMetadata), Get_Z_Construct_UScriptStruct_FPluginReferenceMetadata_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPluginReferenceMetadata_Hash() { return 970741719U; }
	void UPluginMetadataObject::StaticRegisterNativesUPluginMetadataObject()
	{
	}
	UClass* Z_Construct_UClass_UPluginMetadataObject_NoRegister()
	{
		return UPluginMetadataObject::StaticClass();
	}
	struct Z_Construct_UClass_UPluginMetadataObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Version_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Version;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VersionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_VersionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FriendlyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FriendlyName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Category_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Category;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CreatedBy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CreatedBy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CreatedByURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CreatedByURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DocsURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DocsURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MarketplaceURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MarketplaceURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SupportURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SupportURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCanContainContent_MetaData[];
#endif
		static void NewProp_bCanContainContent_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCanContainContent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsBetaVersion_MetaData[];
#endif
		static void NewProp_bIsBetaVersion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsBetaVersion;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Plugins_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Plugins_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Plugins;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPluginMetadataObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_PluginBrowser,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * We use this object to display plugin properties using details view.\n */" },
		{ "IncludePath", "PluginMetadataObject.h" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "We use this object to display plugin properties using details view." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Version_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** Version number for the plugin.  The version number must increase with every version of the plugin, so that the system\n\x09""can determine whether one version of a plugin is newer than another, or to enforce other requirements.  This version\n\x09number is not displayed in front-facing UI.  Use the VersionName for that. */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Version number for the plugin.  The version number must increase with every version of the plugin, so that the system\n      can determine whether one version of a plugin is newer than another, or to enforce other requirements.  This version\n      number is not displayed in front-facing UI.  Use the VersionName for that." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Version = { "Version", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, Version), METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Version_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Version_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_VersionName_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** Name of the version for this plugin.  This is the front-facing part of the version number.  It doesn't need to match\n\x09the version number numerically, but should be updated when the version number is increased accordingly. */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Name of the version for this plugin.  This is the front-facing part of the version number.  It doesn't need to match\n      the version number numerically, but should be updated when the version number is increased accordingly." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_VersionName = { "VersionName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, VersionName), METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_VersionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_VersionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_FriendlyName_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** Friendly name of the plugin */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Friendly name of the plugin" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_FriendlyName = { "FriendlyName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, FriendlyName), METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_FriendlyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_FriendlyName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** Description of the plugin */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Description of the plugin" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, Description), METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Category_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** The category that this plugin belongs to */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "The category that this plugin belongs to" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Category = { "Category", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, Category), METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Category_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Category_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_CreatedBy_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** The company or individual who created this plugin.  This is an optional field that may be displayed in the user interface. */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "The company or individual who created this plugin.  This is an optional field that may be displayed in the user interface." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_CreatedBy = { "CreatedBy", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, CreatedBy), METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_CreatedBy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_CreatedBy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_CreatedByURL_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** Hyperlink URL string for the company or individual who created this plugin.  This is optional. */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Hyperlink URL string for the company or individual who created this plugin.  This is optional." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_CreatedByURL = { "CreatedByURL", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, CreatedByURL), METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_CreatedByURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_CreatedByURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_DocsURL_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** Documentation URL string. */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Documentation URL string." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_DocsURL = { "DocsURL", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, DocsURL), METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_DocsURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_DocsURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_MarketplaceURL_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** Marketplace URL string. */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Marketplace URL string." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_MarketplaceURL = { "MarketplaceURL", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, MarketplaceURL), METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_MarketplaceURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_MarketplaceURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_SupportURL_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** Support URL/email for this plugin. Email addresses must be prefixed with 'mailto:' */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Support URL/email for this plugin. Email addresses must be prefixed with 'mailto:'" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_SupportURL = { "SupportURL", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, SupportURL), METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_SupportURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_SupportURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bCanContainContent_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** Can this plugin contain content? */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Can this plugin contain content?" },
	};
#endif
	void Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bCanContainContent_SetBit(void* Obj)
	{
		((UPluginMetadataObject*)Obj)->bCanContainContent = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bCanContainContent = { "bCanContainContent", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPluginMetadataObject), &Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bCanContainContent_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bCanContainContent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bCanContainContent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bIsBetaVersion_MetaData[] = {
		{ "Category", "Details" },
		{ "Comment", "/** Marks the plugin as beta in the UI */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Marks the plugin as beta in the UI" },
	};
#endif
	void Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bIsBetaVersion_SetBit(void* Obj)
	{
		((UPluginMetadataObject*)Obj)->bIsBetaVersion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bIsBetaVersion = { "bIsBetaVersion", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPluginMetadataObject), &Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bIsBetaVersion_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bIsBetaVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bIsBetaVersion_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Plugins_Inner = { "Plugins", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPluginReferenceMetadata, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Plugins_MetaData[] = {
		{ "Category", "Dependencies" },
		{ "Comment", "/** Plugins used by this plugin */" },
		{ "ModuleRelativePath", "Private/PluginMetadataObject.h" },
		{ "ToolTip", "Plugins used by this plugin" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Plugins = { "Plugins", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPluginMetadataObject, Plugins), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Plugins_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Plugins_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPluginMetadataObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Version,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_VersionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_FriendlyName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Category,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_CreatedBy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_CreatedByURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_DocsURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_MarketplaceURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_SupportURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bCanContainContent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_bIsBetaVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Plugins_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPluginMetadataObject_Statics::NewProp_Plugins,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPluginMetadataObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPluginMetadataObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPluginMetadataObject_Statics::ClassParams = {
		&UPluginMetadataObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPluginMetadataObject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPluginMetadataObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPluginMetadataObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPluginMetadataObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPluginMetadataObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPluginMetadataObject, 3556583290);
	template<> PLUGINBROWSER_API UClass* StaticClass<UPluginMetadataObject>()
	{
		return UPluginMetadataObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPluginMetadataObject(Z_Construct_UClass_UPluginMetadataObject, &UPluginMetadataObject::StaticClass, TEXT("/Script/PluginBrowser"), TEXT("UPluginMetadataObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPluginMetadataObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
