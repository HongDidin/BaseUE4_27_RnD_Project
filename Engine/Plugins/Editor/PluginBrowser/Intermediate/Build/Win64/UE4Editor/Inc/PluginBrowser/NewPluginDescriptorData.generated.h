// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLUGINBROWSER_NewPluginDescriptorData_generated_h
#error "NewPluginDescriptorData.generated.h already included, missing '#pragma once' in NewPluginDescriptorData.h"
#endif
#define PLUGINBROWSER_NewPluginDescriptorData_generated_h

#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_SPARSE_DATA
#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_RPC_WRAPPERS
#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNewPluginDescriptorData(); \
	friend struct Z_Construct_UClass_UNewPluginDescriptorData_Statics; \
public: \
	DECLARE_CLASS(UNewPluginDescriptorData, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PluginBrowser"), NO_API) \
	DECLARE_SERIALIZER(UNewPluginDescriptorData)


#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUNewPluginDescriptorData(); \
	friend struct Z_Construct_UClass_UNewPluginDescriptorData_Statics; \
public: \
	DECLARE_CLASS(UNewPluginDescriptorData, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PluginBrowser"), NO_API) \
	DECLARE_SERIALIZER(UNewPluginDescriptorData)


#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNewPluginDescriptorData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNewPluginDescriptorData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNewPluginDescriptorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNewPluginDescriptorData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNewPluginDescriptorData(UNewPluginDescriptorData&&); \
	NO_API UNewPluginDescriptorData(const UNewPluginDescriptorData&); \
public:


#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNewPluginDescriptorData(UNewPluginDescriptorData&&); \
	NO_API UNewPluginDescriptorData(const UNewPluginDescriptorData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNewPluginDescriptorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNewPluginDescriptorData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNewPluginDescriptorData)


#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_15_PROLOG
#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_SPARSE_DATA \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_RPC_WRAPPERS \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_INCLASS \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_SPARSE_DATA \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLUGINBROWSER_API UClass* StaticClass<class UNewPluginDescriptorData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_NewPluginDescriptorData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
