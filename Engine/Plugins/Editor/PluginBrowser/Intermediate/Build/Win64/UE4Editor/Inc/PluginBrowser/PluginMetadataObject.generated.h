// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLUGINBROWSER_PluginMetadataObject_generated_h
#error "PluginMetadataObject.generated.h already included, missing '#pragma once' in PluginMetadataObject.h"
#endif
#define PLUGINBROWSER_PluginMetadataObject_generated_h

#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_22_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPluginReferenceMetadata_Statics; \
	PLUGINBROWSER_API static class UScriptStruct* StaticStruct();


template<> PLUGINBROWSER_API UScriptStruct* StaticStruct<struct FPluginReferenceMetadata>();

#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_SPARSE_DATA
#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_RPC_WRAPPERS
#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPluginMetadataObject(); \
	friend struct Z_Construct_UClass_UPluginMetadataObject_Statics; \
public: \
	DECLARE_CLASS(UPluginMetadataObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PluginBrowser"), NO_API) \
	DECLARE_SERIALIZER(UPluginMetadataObject)


#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_INCLASS \
private: \
	static void StaticRegisterNativesUPluginMetadataObject(); \
	friend struct Z_Construct_UClass_UPluginMetadataObject_Statics; \
public: \
	DECLARE_CLASS(UPluginMetadataObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PluginBrowser"), NO_API) \
	DECLARE_SERIALIZER(UPluginMetadataObject)


#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPluginMetadataObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPluginMetadataObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPluginMetadataObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPluginMetadataObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPluginMetadataObject(UPluginMetadataObject&&); \
	NO_API UPluginMetadataObject(const UPluginMetadataObject&); \
public:


#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPluginMetadataObject(UPluginMetadataObject&&); \
	NO_API UPluginMetadataObject(const UPluginMetadataObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPluginMetadataObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPluginMetadataObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPluginMetadataObject)


#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_50_PROLOG
#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_SPARSE_DATA \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_RPC_WRAPPERS \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_INCLASS \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_SPARSE_DATA \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h_54_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLUGINBROWSER_API UClass* StaticClass<class UPluginMetadataObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_PluginBrowser_Source_PluginBrowser_Private_PluginMetadataObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
