// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AssetManagerEditor/Public/ReferenceViewer/EdGraphNode_Reference.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEdGraphNode_Reference() {}
// Cross Module References
	ASSETMANAGEREDITOR_API UClass* Z_Construct_UClass_UEdGraphNode_Reference_NoRegister();
	ASSETMANAGEREDITOR_API UClass* Z_Construct_UClass_UEdGraphNode_Reference();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphNode();
	UPackage* Z_Construct_UPackage__Script_AssetManagerEditor();
// End Cross Module References
	void UEdGraphNode_Reference::StaticRegisterNativesUEdGraphNode_Reference()
	{
	}
	UClass* Z_Construct_UClass_UEdGraphNode_Reference_NoRegister()
	{
		return UEdGraphNode_Reference::StaticClass();
	}
	struct Z_Construct_UClass_UEdGraphNode_Reference_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEdGraphNode_Reference_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_AssetManagerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdGraphNode_Reference_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ReferenceViewer/EdGraphNode_Reference.h" },
		{ "ModuleRelativePath", "Public/ReferenceViewer/EdGraphNode_Reference.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEdGraphNode_Reference_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEdGraphNode_Reference>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEdGraphNode_Reference_Statics::ClassParams = {
		&UEdGraphNode_Reference::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEdGraphNode_Reference_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEdGraphNode_Reference_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEdGraphNode_Reference()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEdGraphNode_Reference_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEdGraphNode_Reference, 3903310235);
	template<> ASSETMANAGEREDITOR_API UClass* StaticClass<UEdGraphNode_Reference>()
	{
		return UEdGraphNode_Reference::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEdGraphNode_Reference(Z_Construct_UClass_UEdGraphNode_Reference, &UEdGraphNode_Reference::StaticClass, TEXT("/Script/AssetManagerEditor"), TEXT("UEdGraphNode_Reference"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEdGraphNode_Reference);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
