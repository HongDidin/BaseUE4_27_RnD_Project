// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AssetManagerEditor/Public/ReferenceViewer/EdGraph_ReferenceViewer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEdGraph_ReferenceViewer() {}
// Cross Module References
	ASSETMANAGEREDITOR_API UClass* Z_Construct_UClass_UEdGraph_ReferenceViewer_NoRegister();
	ASSETMANAGEREDITOR_API UClass* Z_Construct_UClass_UEdGraph_ReferenceViewer();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraph();
	UPackage* Z_Construct_UPackage__Script_AssetManagerEditor();
// End Cross Module References
	void UEdGraph_ReferenceViewer::StaticRegisterNativesUEdGraph_ReferenceViewer()
	{
	}
	UClass* Z_Construct_UClass_UEdGraph_ReferenceViewer_NoRegister()
	{
		return UEdGraph_ReferenceViewer::StaticClass();
	}
	struct Z_Construct_UClass_UEdGraph_ReferenceViewer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEdGraph_ReferenceViewer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraph,
		(UObject* (*)())Z_Construct_UPackage__Script_AssetManagerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdGraph_ReferenceViewer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ReferenceViewer/EdGraph_ReferenceViewer.h" },
		{ "ModuleRelativePath", "Public/ReferenceViewer/EdGraph_ReferenceViewer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEdGraph_ReferenceViewer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEdGraph_ReferenceViewer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEdGraph_ReferenceViewer_Statics::ClassParams = {
		&UEdGraph_ReferenceViewer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEdGraph_ReferenceViewer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEdGraph_ReferenceViewer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEdGraph_ReferenceViewer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEdGraph_ReferenceViewer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEdGraph_ReferenceViewer, 3499940302);
	template<> ASSETMANAGEREDITOR_API UClass* StaticClass<UEdGraph_ReferenceViewer>()
	{
		return UEdGraph_ReferenceViewer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEdGraph_ReferenceViewer(Z_Construct_UClass_UEdGraph_ReferenceViewer, &UEdGraph_ReferenceViewer::StaticClass, TEXT("/Script/AssetManagerEditor"), TEXT("UEdGraph_ReferenceViewer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEdGraph_ReferenceViewer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
