// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSETMANAGEREDITOR_ReferenceViewerSchema_generated_h
#error "ReferenceViewerSchema.generated.h already included, missing '#pragma once' in ReferenceViewerSchema.h"
#endif
#define ASSETMANAGEREDITOR_ReferenceViewerSchema_generated_h

#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_SPARSE_DATA
#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_RPC_WRAPPERS
#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReferenceViewerSchema(); \
	friend struct Z_Construct_UClass_UReferenceViewerSchema_Statics; \
public: \
	DECLARE_CLASS(UReferenceViewerSchema, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AssetManagerEditor"), ASSETMANAGEREDITOR_API) \
	DECLARE_SERIALIZER(UReferenceViewerSchema)


#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUReferenceViewerSchema(); \
	friend struct Z_Construct_UClass_UReferenceViewerSchema_Statics; \
public: \
	DECLARE_CLASS(UReferenceViewerSchema, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AssetManagerEditor"), ASSETMANAGEREDITOR_API) \
	DECLARE_SERIALIZER(UReferenceViewerSchema)


#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ASSETMANAGEREDITOR_API UReferenceViewerSchema(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReferenceViewerSchema) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ASSETMANAGEREDITOR_API, UReferenceViewerSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReferenceViewerSchema); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ASSETMANAGEREDITOR_API UReferenceViewerSchema(UReferenceViewerSchema&&); \
	ASSETMANAGEREDITOR_API UReferenceViewerSchema(const UReferenceViewerSchema&); \
public:


#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ASSETMANAGEREDITOR_API UReferenceViewerSchema(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ASSETMANAGEREDITOR_API UReferenceViewerSchema(UReferenceViewerSchema&&); \
	ASSETMANAGEREDITOR_API UReferenceViewerSchema(const UReferenceViewerSchema&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ASSETMANAGEREDITOR_API, UReferenceViewerSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReferenceViewerSchema); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReferenceViewerSchema)


#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_14_PROLOG
#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_SPARSE_DATA \
	Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_RPC_WRAPPERS \
	Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_INCLASS \
	Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_SPARSE_DATA \
	Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ReferenceViewerSchema."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSETMANAGEREDITOR_API UClass* StaticClass<class UReferenceViewerSchema>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_AssetManagerEditor_Source_AssetManagerEditor_Public_ReferenceViewer_ReferenceViewerSchema_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
