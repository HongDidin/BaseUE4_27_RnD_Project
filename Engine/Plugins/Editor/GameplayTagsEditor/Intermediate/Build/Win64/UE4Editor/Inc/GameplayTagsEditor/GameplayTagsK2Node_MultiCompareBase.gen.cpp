// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameplayTagsEditor/Classes/GameplayTagsK2Node_MultiCompareBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameplayTagsK2Node_MultiCompareBase() {}
// Cross Module References
	GAMEPLAYTAGSEDITOR_API UClass* Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_NoRegister();
	GAMEPLAYTAGSEDITOR_API UClass* Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_GameplayTagsEditor();
// End Cross Module References
	void UGameplayTagsK2Node_MultiCompareBase::StaticRegisterNativesUGameplayTagsK2Node_MultiCompareBase()
	{
	}
	UClass* Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_NoRegister()
	{
		return UGameplayTagsK2Node_MultiCompareBase::StaticClass();
	}
	struct Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfPins_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfPins;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PinNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PinNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PinNames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node,
		(UObject* (*)())Z_Construct_UPackage__Script_GameplayTagsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GameplayTagsK2Node_MultiCompareBase.h" },
		{ "ModuleRelativePath", "Classes/GameplayTagsK2Node_MultiCompareBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_NumberOfPins_MetaData[] = {
		{ "Category", "PinOptions" },
		{ "ModuleRelativePath", "Classes/GameplayTagsK2Node_MultiCompareBase.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_NumberOfPins = { "NumberOfPins", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameplayTagsK2Node_MultiCompareBase, NumberOfPins), METADATA_PARAMS(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_NumberOfPins_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_NumberOfPins_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_PinNames_Inner = { "PinNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_PinNames_MetaData[] = {
		{ "ModuleRelativePath", "Classes/GameplayTagsK2Node_MultiCompareBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_PinNames = { "PinNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGameplayTagsK2Node_MultiCompareBase, PinNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_PinNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_PinNames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_NumberOfPins,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_PinNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::NewProp_PinNames,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameplayTagsK2Node_MultiCompareBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::ClassParams = {
		&UGameplayTagsK2Node_MultiCompareBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameplayTagsK2Node_MultiCompareBase, 3457223455);
	template<> GAMEPLAYTAGSEDITOR_API UClass* StaticClass<UGameplayTagsK2Node_MultiCompareBase>()
	{
		return UGameplayTagsK2Node_MultiCompareBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameplayTagsK2Node_MultiCompareBase(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase, &UGameplayTagsK2Node_MultiCompareBase::StaticClass, TEXT("/Script/GameplayTagsEditor"), TEXT("UGameplayTagsK2Node_MultiCompareBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameplayTagsK2Node_MultiCompareBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
