// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEPLAYTAGSEDITOR_GameplayTagSearchFilter_generated_h
#error "GameplayTagSearchFilter.generated.h already included, missing '#pragma once' in GameplayTagSearchFilter.h"
#endif
#define GAMEPLAYTAGSEDITOR_GameplayTagSearchFilter_generated_h

#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_SPARSE_DATA
#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_RPC_WRAPPERS
#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameplayTagSearchFilter(); \
	friend struct Z_Construct_UClass_UGameplayTagSearchFilter_Statics; \
public: \
	DECLARE_CLASS(UGameplayTagSearchFilter, UContentBrowserFrontEndFilterExtension, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameplayTagsEditor"), NO_API) \
	DECLARE_SERIALIZER(UGameplayTagSearchFilter)


#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUGameplayTagSearchFilter(); \
	friend struct Z_Construct_UClass_UGameplayTagSearchFilter_Statics; \
public: \
	DECLARE_CLASS(UGameplayTagSearchFilter, UContentBrowserFrontEndFilterExtension, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameplayTagsEditor"), NO_API) \
	DECLARE_SERIALIZER(UGameplayTagSearchFilter)


#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameplayTagSearchFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameplayTagSearchFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameplayTagSearchFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameplayTagSearchFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameplayTagSearchFilter(UGameplayTagSearchFilter&&); \
	NO_API UGameplayTagSearchFilter(const UGameplayTagSearchFilter&); \
public:


#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameplayTagSearchFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameplayTagSearchFilter(UGameplayTagSearchFilter&&); \
	NO_API UGameplayTagSearchFilter(const UGameplayTagSearchFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameplayTagSearchFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameplayTagSearchFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameplayTagSearchFilter)


#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_11_PROLOG
#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_RPC_WRAPPERS \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_INCLASS \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h_15_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEPLAYTAGSEDITOR_API UClass* StaticClass<class UGameplayTagSearchFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Private_GameplayTagSearchFilter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
