// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameplayTagsEditor/Classes/GameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags() {}
// Cross Module References
	GAMEPLAYTAGSEDITOR_API UClass* Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags_NoRegister();
	GAMEPLAYTAGSEDITOR_API UClass* Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags();
	GAMEPLAYTAGSEDITOR_API UClass* Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase();
	UPackage* Z_Construct_UPackage__Script_GameplayTagsEditor();
// End Cross Module References
	void UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags::StaticRegisterNativesUGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags()
	{
	}
	UClass* Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags_NoRegister()
	{
		return UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags::StaticClass();
	}
	struct Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GameplayTagsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags.h" },
		{ "ModuleRelativePath", "Classes/GameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags_Statics::ClassParams = {
		&UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags, 943511828);
	template<> GAMEPLAYTAGSEDITOR_API UClass* StaticClass<UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags>()
	{
		return UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags(Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags, &UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags::StaticClass, TEXT("/Script/GameplayTagsEditor"), TEXT("UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameplayTagsK2Node_MultiCompareGameplayTagAssetInterfaceSingleTags);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
