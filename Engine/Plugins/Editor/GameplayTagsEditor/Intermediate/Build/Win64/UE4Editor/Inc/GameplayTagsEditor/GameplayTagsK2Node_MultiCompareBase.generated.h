// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEPLAYTAGSEDITOR_GameplayTagsK2Node_MultiCompareBase_generated_h
#error "GameplayTagsK2Node_MultiCompareBase.generated.h already included, missing '#pragma once' in GameplayTagsK2Node_MultiCompareBase.h"
#endif
#define GAMEPLAYTAGSEDITOR_GameplayTagsK2Node_MultiCompareBase_generated_h

#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_SPARSE_DATA
#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_RPC_WRAPPERS
#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameplayTagsK2Node_MultiCompareBase(); \
	friend struct Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics; \
public: \
	DECLARE_CLASS(UGameplayTagsK2Node_MultiCompareBase, UK2Node, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameplayTagsEditor"), NO_API) \
	DECLARE_SERIALIZER(UGameplayTagsK2Node_MultiCompareBase)


#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUGameplayTagsK2Node_MultiCompareBase(); \
	friend struct Z_Construct_UClass_UGameplayTagsK2Node_MultiCompareBase_Statics; \
public: \
	DECLARE_CLASS(UGameplayTagsK2Node_MultiCompareBase, UK2Node, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameplayTagsEditor"), NO_API) \
	DECLARE_SERIALIZER(UGameplayTagsK2Node_MultiCompareBase)


#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameplayTagsK2Node_MultiCompareBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameplayTagsK2Node_MultiCompareBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameplayTagsK2Node_MultiCompareBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameplayTagsK2Node_MultiCompareBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameplayTagsK2Node_MultiCompareBase(UGameplayTagsK2Node_MultiCompareBase&&); \
	NO_API UGameplayTagsK2Node_MultiCompareBase(const UGameplayTagsK2Node_MultiCompareBase&); \
public:


#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameplayTagsK2Node_MultiCompareBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameplayTagsK2Node_MultiCompareBase(UGameplayTagsK2Node_MultiCompareBase&&); \
	NO_API UGameplayTagsK2Node_MultiCompareBase(const UGameplayTagsK2Node_MultiCompareBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameplayTagsK2Node_MultiCompareBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameplayTagsK2Node_MultiCompareBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameplayTagsK2Node_MultiCompareBase)


#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_11_PROLOG
#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_SPARSE_DATA \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_RPC_WRAPPERS \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_INCLASS \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_SPARSE_DATA \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GameplayTagsK2Node_MultiCompareBase."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEPLAYTAGSEDITOR_API UClass* StaticClass<class UGameplayTagsK2Node_MultiCompareBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_GameplayTagsEditor_Source_GameplayTagsEditor_Classes_GameplayTagsK2Node_MultiCompareBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
