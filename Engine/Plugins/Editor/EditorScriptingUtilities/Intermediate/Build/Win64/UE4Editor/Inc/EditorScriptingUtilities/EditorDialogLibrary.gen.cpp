// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EditorScriptingUtilities/Public/EditorDialogLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditorDialogLibrary() {}
// Cross Module References
	EDITORSCRIPTINGUTILITIES_API UClass* Z_Construct_UClass_UEditorDialogLibrary_NoRegister();
	EDITORSCRIPTINGUTILITIES_API UClass* Z_Construct_UClass_UEditorDialogLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_EditorScriptingUtilities();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EAppMsgType();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EAppReturnType();
// End Cross Module References
	DEFINE_FUNCTION(UEditorDialogLibrary::execShowMessage)
	{
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_Title);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_Message);
		P_GET_PROPERTY(FByteProperty,Z_Param_MessageType);
		P_GET_PROPERTY(FByteProperty,Z_Param_DefaultValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TEnumAsByte<EAppReturnType::Type>*)Z_Param__Result=UEditorDialogLibrary::ShowMessage(Z_Param_Out_Title,Z_Param_Out_Message,EAppMsgType::Type(Z_Param_MessageType),EAppReturnType::Type(Z_Param_DefaultValue));
		P_NATIVE_END;
	}
	void UEditorDialogLibrary::StaticRegisterNativesUEditorDialogLibrary()
	{
		UClass* Class = UEditorDialogLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ShowMessage", &UEditorDialogLibrary::execShowMessage },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics
	{
		struct EditorDialogLibrary_eventShowMessage_Parms
		{
			FText Title;
			FText Message;
			TEnumAsByte<EAppMsgType::Type> MessageType;
			TEnumAsByte<EAppReturnType::Type> DefaultValue;
			TEnumAsByte<EAppReturnType::Type> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Title_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Title;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Message_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Message;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MessageType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_Title_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_Title = { "Title", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorDialogLibrary_eventShowMessage_Parms, Title), METADATA_PARAMS(Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_Title_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_Title_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_Message_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_Message = { "Message", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorDialogLibrary_eventShowMessage_Parms, Message), METADATA_PARAMS(Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_Message_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_Message_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_MessageType = { "MessageType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorDialogLibrary_eventShowMessage_Parms, MessageType), Z_Construct_UEnum_CoreUObject_EAppMsgType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorDialogLibrary_eventShowMessage_Parms, DefaultValue), Z_Construct_UEnum_CoreUObject_EAppReturnType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorDialogLibrary_eventShowMessage_Parms, ReturnValue), Z_Construct_UEnum_CoreUObject_EAppReturnType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_Title,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_Message,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_MessageType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Message Dialog" },
		{ "Comment", "/**\n\x09 * Open a modal message box dialog with the given message. If running in \"-unattended\" mode it will immediately \n\x09 * return the value specified by DefaultValue. If not running in \"-unattended\" mode then it will block execution\n\x09 * until the user makes a decision, at which point their decision will be returned.\n\x09 * @param Title \x09\x09The title of the created dialog window\n\x09 * @param Message \x09\x09Text of the message to show\n\x09 * @param MessageType \x09Specifies which buttons the dialog should have\n\x09 * @param DefaultValue \x09If the application is Unattended, the function will log and return DefaultValue\n\x09 * @return The result of the users decision, or DefaultValue if running in unattended mode.\n\x09*/" },
		{ "CPP_Default_DefaultValue", "Type::No" },
		{ "DisplayName", "Show Message Dialog" },
		{ "ModuleRelativePath", "Public/EditorDialogLibrary.h" },
		{ "ToolTip", "Open a modal message box dialog with the given message. If running in \"-unattended\" mode it will immediately\nreturn the value specified by DefaultValue. If not running in \"-unattended\" mode then it will block execution\nuntil the user makes a decision, at which point their decision will be returned.\n@param Title                 The title of the created dialog window\n@param Message               Text of the message to show\n@param MessageType   Specifies which buttons the dialog should have\n@param DefaultValue  If the application is Unattended, the function will log and return DefaultValue\n@return The result of the users decision, or DefaultValue if running in unattended mode." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorDialogLibrary, nullptr, "ShowMessage", nullptr, nullptr, sizeof(EditorDialogLibrary_eventShowMessage_Parms), Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditorDialogLibrary_NoRegister()
	{
		return UEditorDialogLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UEditorDialogLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditorDialogLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_EditorScriptingUtilities,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditorDialogLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditorDialogLibrary_ShowMessage, "ShowMessage" }, // 3919590341
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorDialogLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Utility class to create simple pop-up dialogs to notify the user of task completion, \n * or to ask them to make simple Yes/No/Retry/Cancel type decisions.\n */" },
		{ "IncludePath", "EditorDialogLibrary.h" },
		{ "ModuleRelativePath", "Public/EditorDialogLibrary.h" },
		{ "ScriptName", "EditorDialog" },
		{ "ToolTip", "Utility class to create simple pop-up dialogs to notify the user of task completion,\nor to ask them to make simple Yes/No/Retry/Cancel type decisions." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditorDialogLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditorDialogLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditorDialogLibrary_Statics::ClassParams = {
		&UEditorDialogLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEditorDialogLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorDialogLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditorDialogLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditorDialogLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditorDialogLibrary, 2385436390);
	template<> EDITORSCRIPTINGUTILITIES_API UClass* StaticClass<UEditorDialogLibrary>()
	{
		return UEditorDialogLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditorDialogLibrary(Z_Construct_UClass_UEditorDialogLibrary, &UEditorDialogLibrary::StaticClass, TEXT("/Script/EditorScriptingUtilities"), TEXT("UEditorDialogLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditorDialogLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
