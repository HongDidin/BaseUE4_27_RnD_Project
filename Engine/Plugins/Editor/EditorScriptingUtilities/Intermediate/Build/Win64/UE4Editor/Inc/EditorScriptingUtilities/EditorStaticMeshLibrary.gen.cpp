// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EditorScriptingUtilities/Public/EditorStaticMeshLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditorStaticMeshLibrary() {}
// Cross Module References
	EDITORSCRIPTINGUTILITIES_API UEnum* Z_Construct_UEnum_EditorScriptingUtilities_EScriptingCollisionShapeType();
	UPackage* Z_Construct_UPackage__Script_EditorScriptingUtilities();
	EDITORSCRIPTINGUTILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions();
	EDITORSCRIPTINGUTILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings();
	EDITORSCRIPTINGUTILITIES_API UClass* Z_Construct_UClass_UEditorStaticMeshLibrary_NoRegister();
	EDITORSCRIPTINGUTILITIES_API UClass* Z_Construct_UClass_UEditorStaticMeshLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	PHYSICSCORE_API UEnum* Z_Construct_UEnum_PhysicsCore_ECollisionTraceFlag();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FMeshBuildSettings();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FMeshReductionSettings();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static UEnum* EScriptingCollisionShapeType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_EditorScriptingUtilities_EScriptingCollisionShapeType, Z_Construct_UPackage__Script_EditorScriptingUtilities(), TEXT("EScriptingCollisionShapeType"));
		}
		return Singleton;
	}
	template<> EDITORSCRIPTINGUTILITIES_API UEnum* StaticEnum<EScriptingCollisionShapeType>()
	{
		return EScriptingCollisionShapeType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EScriptingCollisionShapeType(EScriptingCollisionShapeType_StaticEnum, TEXT("/Script/EditorScriptingUtilities"), TEXT("EScriptingCollisionShapeType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_EditorScriptingUtilities_EScriptingCollisionShapeType_Hash() { return 2952695309U; }
	UEnum* Z_Construct_UEnum_EditorScriptingUtilities_EScriptingCollisionShapeType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_EditorScriptingUtilities();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EScriptingCollisionShapeType"), 0, Get_Z_Construct_UEnum_EditorScriptingUtilities_EScriptingCollisionShapeType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EScriptingCollisionShapeType::Box", (int64)EScriptingCollisionShapeType::Box },
				{ "EScriptingCollisionShapeType::Sphere", (int64)EScriptingCollisionShapeType::Sphere },
				{ "EScriptingCollisionShapeType::Capsule", (int64)EScriptingCollisionShapeType::Capsule },
				{ "EScriptingCollisionShapeType::NDOP10_X", (int64)EScriptingCollisionShapeType::NDOP10_X },
				{ "EScriptingCollisionShapeType::NDOP10_Y", (int64)EScriptingCollisionShapeType::NDOP10_Y },
				{ "EScriptingCollisionShapeType::NDOP10_Z", (int64)EScriptingCollisionShapeType::NDOP10_Z },
				{ "EScriptingCollisionShapeType::NDOP18", (int64)EScriptingCollisionShapeType::NDOP18 },
				{ "EScriptingCollisionShapeType::NDOP26", (int64)EScriptingCollisionShapeType::NDOP26 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Box.Name", "EScriptingCollisionShapeType::Box" },
				{ "Capsule.Name", "EScriptingCollisionShapeType::Capsule" },
				{ "Comment", "/** Types of Collision Construct that are generated **/" },
				{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
				{ "NDOP10_X.Name", "EScriptingCollisionShapeType::NDOP10_X" },
				{ "NDOP10_Y.Name", "EScriptingCollisionShapeType::NDOP10_Y" },
				{ "NDOP10_Z.Name", "EScriptingCollisionShapeType::NDOP10_Z" },
				{ "NDOP18.Name", "EScriptingCollisionShapeType::NDOP18" },
				{ "NDOP26.Name", "EScriptingCollisionShapeType::NDOP26" },
				{ "Sphere.Name", "EScriptingCollisionShapeType::Sphere" },
				{ "ToolTip", "Types of Collision Construct that are generated *" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_EditorScriptingUtilities,
				nullptr,
				"EScriptingCollisionShapeType",
				"EScriptingCollisionShapeType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FEditorScriptingMeshReductionOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern EDITORSCRIPTINGUTILITIES_API uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions, Z_Construct_UPackage__Script_EditorScriptingUtilities(), TEXT("EditorScriptingMeshReductionOptions"), sizeof(FEditorScriptingMeshReductionOptions), Get_Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Hash());
	}
	return Singleton;
}
template<> EDITORSCRIPTINGUTILITIES_API UScriptStruct* StaticStruct<FEditorScriptingMeshReductionOptions>()
{
	return FEditorScriptingMeshReductionOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEditorScriptingMeshReductionOptions(FEditorScriptingMeshReductionOptions::StaticStruct, TEXT("/Script/EditorScriptingUtilities"), TEXT("EditorScriptingMeshReductionOptions"), false, nullptr, nullptr);
static struct FScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingMeshReductionOptions
{
	FScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingMeshReductionOptions()
	{
		UScriptStruct::DeferCppStructOps<FEditorScriptingMeshReductionOptions>(FName(TEXT("EditorScriptingMeshReductionOptions")));
	}
} ScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingMeshReductionOptions;
	struct Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoComputeLODScreenSize_MetaData[];
#endif
		static void NewProp_bAutoComputeLODScreenSize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoComputeLODScreenSize;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReductionSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReductionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReductionSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEditorScriptingMeshReductionOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_bAutoComputeLODScreenSize_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// If true, the screen sizes at which LODs swap are computed automatically\n// @note that this is displayed as 'Auto Compute LOD Distances' in the UI\n" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "If true, the screen sizes at which LODs swap are computed automatically\n@note that this is displayed as 'Auto Compute LOD Distances' in the UI" },
	};
#endif
	void Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_bAutoComputeLODScreenSize_SetBit(void* Obj)
	{
		((FEditorScriptingMeshReductionOptions*)Obj)->bAutoComputeLODScreenSize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_bAutoComputeLODScreenSize = { "bAutoComputeLODScreenSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FEditorScriptingMeshReductionOptions), &Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_bAutoComputeLODScreenSize_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_bAutoComputeLODScreenSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_bAutoComputeLODScreenSize_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_ReductionSettings_Inner = { "ReductionSettings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_ReductionSettings_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// Array of reduction settings to apply to each new LOD mesh.\n" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Array of reduction settings to apply to each new LOD mesh." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_ReductionSettings = { "ReductionSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEditorScriptingMeshReductionOptions, ReductionSettings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_ReductionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_ReductionSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_bAutoComputeLODScreenSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_ReductionSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::NewProp_ReductionSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EditorScriptingUtilities,
		nullptr,
		&NewStructOps,
		"EditorScriptingMeshReductionOptions",
		sizeof(FEditorScriptingMeshReductionOptions),
		alignof(FEditorScriptingMeshReductionOptions),
		Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EditorScriptingUtilities();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EditorScriptingMeshReductionOptions"), sizeof(FEditorScriptingMeshReductionOptions), Get_Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions_Hash() { return 715815405U; }
class UScriptStruct* FEditorScriptingMeshReductionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern EDITORSCRIPTINGUTILITIES_API uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings, Z_Construct_UPackage__Script_EditorScriptingUtilities(), TEXT("EditorScriptingMeshReductionSettings"), sizeof(FEditorScriptingMeshReductionSettings), Get_Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Hash());
	}
	return Singleton;
}
template<> EDITORSCRIPTINGUTILITIES_API UScriptStruct* StaticStruct<FEditorScriptingMeshReductionSettings>()
{
	return FEditorScriptingMeshReductionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEditorScriptingMeshReductionSettings(FEditorScriptingMeshReductionSettings::StaticStruct, TEXT("/Script/EditorScriptingUtilities"), TEXT("EditorScriptingMeshReductionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingMeshReductionSettings
{
	FScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingMeshReductionSettings()
	{
		UScriptStruct::DeferCppStructOps<FEditorScriptingMeshReductionSettings>(FName(TEXT("EditorScriptingMeshReductionSettings")));
	}
} ScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingMeshReductionSettings;
	struct Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PercentTriangles_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PercentTriangles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEditorScriptingMeshReductionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewProp_PercentTriangles_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// Percentage of triangles to keep. Ranges from 0.0 to 1.0: 1.0 = no reduction, 0.0 = no triangles.\n" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Percentage of triangles to keep. Ranges from 0.0 to 1.0: 1.0 = no reduction, 0.0 = no triangles." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewProp_PercentTriangles = { "PercentTriangles", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEditorScriptingMeshReductionSettings, PercentTriangles), METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewProp_PercentTriangles_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewProp_PercentTriangles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewProp_ScreenSize_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// ScreenSize to display this LOD. Ranges from 0.0 to 1.0.\n" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "ScreenSize to display this LOD. Ranges from 0.0 to 1.0." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewProp_ScreenSize = { "ScreenSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEditorScriptingMeshReductionSettings, ScreenSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewProp_ScreenSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewProp_ScreenSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewProp_PercentTriangles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::NewProp_ScreenSize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EditorScriptingUtilities,
		nullptr,
		&NewStructOps,
		"EditorScriptingMeshReductionSettings",
		sizeof(FEditorScriptingMeshReductionSettings),
		alignof(FEditorScriptingMeshReductionSettings),
		Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EditorScriptingUtilities();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EditorScriptingMeshReductionSettings"), sizeof(FEditorScriptingMeshReductionSettings), Get_Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingMeshReductionSettings_Hash() { return 3260848887U; }
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGenerateBoxUVChannel)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_UVChannelIndex);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Position);
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_Orientation);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Size);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::GenerateBoxUVChannel(Z_Param_StaticMesh,Z_Param_LODIndex,Z_Param_UVChannelIndex,Z_Param_Out_Position,Z_Param_Out_Orientation,Z_Param_Out_Size);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGenerateCylindricalUVChannel)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_UVChannelIndex);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Position);
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_Orientation);
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_Tiling);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::GenerateCylindricalUVChannel(Z_Param_StaticMesh,Z_Param_LODIndex,Z_Param_UVChannelIndex,Z_Param_Out_Position,Z_Param_Out_Orientation,Z_Param_Out_Tiling);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGeneratePlanarUVChannel)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_UVChannelIndex);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Position);
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_Orientation);
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_Tiling);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::GeneratePlanarUVChannel(Z_Param_StaticMesh,Z_Param_LODIndex,Z_Param_UVChannelIndex,Z_Param_Out_Position,Z_Param_Out_Orientation,Z_Param_Out_Tiling);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execRemoveUVChannel)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_UVChannelIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::RemoveUVChannel(Z_Param_StaticMesh,Z_Param_LODIndex,Z_Param_UVChannelIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execInsertUVChannel)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_UVChannelIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::InsertUVChannel(Z_Param_StaticMesh,Z_Param_LODIndex,Z_Param_UVChannelIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execAddUVChannel)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::AddUVChannel(Z_Param_StaticMesh,Z_Param_LODIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetNumUVChannels)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::GetNumUVChannels(Z_Param_StaticMesh,Z_Param_LODIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execSetAllowCPUAccess)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_UBOOL(Z_Param_bAllowCPUAccess);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorStaticMeshLibrary::SetAllowCPUAccess(Z_Param_StaticMesh,Z_Param_bAllowCPUAccess);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetNumberMaterials)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::GetNumberMaterials(Z_Param_StaticMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetNumberVerts)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::GetNumberVerts(Z_Param_StaticMesh,Z_Param_LODIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execSetGenerateLightmapUVs)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_UBOOL(Z_Param_bGenerateLightmapUVs);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::SetGenerateLightmapUVs(Z_Param_StaticMesh,Z_Param_bGenerateLightmapUVs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execHasInstanceVertexColors)
	{
		P_GET_OBJECT(UStaticMeshComponent,Z_Param_StaticMeshComponent);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::HasInstanceVertexColors(Z_Param_StaticMeshComponent);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execHasVertexColors)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::HasVertexColors(Z_Param_StaticMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetLODMaterialSlot)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_SectionIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::GetLODMaterialSlot(Z_Param_StaticMesh,Z_Param_LODIndex,Z_Param_SectionIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execSetLODMaterialSlot)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_MaterialSlotIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_SectionIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorStaticMeshLibrary::SetLODMaterialSlot(Z_Param_StaticMesh,Z_Param_MaterialSlotIndex,Z_Param_LODIndex,Z_Param_SectionIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execEnableSectionCastShadow)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_UBOOL(Z_Param_bCastShadow);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_SectionIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorStaticMeshLibrary::EnableSectionCastShadow(Z_Param_StaticMesh,Z_Param_bCastShadow,Z_Param_LODIndex,Z_Param_SectionIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execIsSectionCollisionEnabled)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_SectionIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::IsSectionCollisionEnabled(Z_Param_StaticMesh,Z_Param_LODIndex,Z_Param_SectionIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execEnableSectionCollision)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_UBOOL(Z_Param_bCollisionEnabled);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_SectionIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorStaticMeshLibrary::EnableSectionCollision(Z_Param_StaticMesh,Z_Param_bCollisionEnabled,Z_Param_LODIndex,Z_Param_SectionIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execRemoveCollisions)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::RemoveCollisions(Z_Param_StaticMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execRemoveCollisionsWithNotification)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_UBOOL(Z_Param_bApplyChanges);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::RemoveCollisionsWithNotification(Z_Param_StaticMesh,Z_Param_bApplyChanges);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execBulkSetConvexDecompositionCollisions)
	{
		P_GET_TARRAY_REF(UStaticMesh*,Z_Param_Out_StaticMeshes);
		P_GET_PROPERTY(FIntProperty,Z_Param_HullCount);
		P_GET_PROPERTY(FIntProperty,Z_Param_MaxHullVerts);
		P_GET_PROPERTY(FIntProperty,Z_Param_HullPrecision);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::BulkSetConvexDecompositionCollisions(Z_Param_Out_StaticMeshes,Z_Param_HullCount,Z_Param_MaxHullVerts,Z_Param_HullPrecision);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execSetConvexDecompositionCollisions)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_HullCount);
		P_GET_PROPERTY(FIntProperty,Z_Param_MaxHullVerts);
		P_GET_PROPERTY(FIntProperty,Z_Param_HullPrecision);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::SetConvexDecompositionCollisions(Z_Param_StaticMesh,Z_Param_HullCount,Z_Param_MaxHullVerts,Z_Param_HullPrecision);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execBulkSetConvexDecompositionCollisionsWithNotification)
	{
		P_GET_TARRAY_REF(UStaticMesh*,Z_Param_Out_StaticMeshes);
		P_GET_PROPERTY(FIntProperty,Z_Param_HullCount);
		P_GET_PROPERTY(FIntProperty,Z_Param_MaxHullVerts);
		P_GET_PROPERTY(FIntProperty,Z_Param_HullPrecision);
		P_GET_UBOOL(Z_Param_bApplyChanges);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::BulkSetConvexDecompositionCollisionsWithNotification(Z_Param_Out_StaticMeshes,Z_Param_HullCount,Z_Param_MaxHullVerts,Z_Param_HullPrecision,Z_Param_bApplyChanges);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execSetConvexDecompositionCollisionsWithNotification)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_HullCount);
		P_GET_PROPERTY(FIntProperty,Z_Param_MaxHullVerts);
		P_GET_PROPERTY(FIntProperty,Z_Param_HullPrecision);
		P_GET_UBOOL(Z_Param_bApplyChanges);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::SetConvexDecompositionCollisionsWithNotification(Z_Param_StaticMesh,Z_Param_HullCount,Z_Param_MaxHullVerts,Z_Param_HullPrecision,Z_Param_bApplyChanges);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetConvexCollisionCount)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::GetConvexCollisionCount(Z_Param_StaticMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetCollisionComplexity)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TEnumAsByte<ECollisionTraceFlag>*)Z_Param__Result=UEditorStaticMeshLibrary::GetCollisionComplexity(Z_Param_StaticMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetSimpleCollisionCount)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::GetSimpleCollisionCount(Z_Param_StaticMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execAddSimpleCollisions)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_ENUM(EScriptingCollisionShapeType,Z_Param_ShapeType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::AddSimpleCollisions(Z_Param_StaticMesh,EScriptingCollisionShapeType(Z_Param_ShapeType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execAddSimpleCollisionsWithNotification)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_ENUM(EScriptingCollisionShapeType,Z_Param_ShapeType);
		P_GET_UBOOL(Z_Param_bApplyChanges);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::AddSimpleCollisionsWithNotification(Z_Param_StaticMesh,EScriptingCollisionShapeType(Z_Param_ShapeType),Z_Param_bApplyChanges);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetLodScreenSizes)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<float>*)Z_Param__Result=UEditorStaticMeshLibrary::GetLodScreenSizes(Z_Param_StaticMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execRemoveLods)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::RemoveLods(Z_Param_StaticMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetLodCount)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::GetLodCount(Z_Param_StaticMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execSetLodFromStaticMesh)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_DestinationStaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_DestinationLodIndex);
		P_GET_OBJECT(UStaticMesh,Z_Param_SourceStaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_SourceLodIndex);
		P_GET_UBOOL(Z_Param_bReuseExistingMaterialSlots);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::SetLodFromStaticMesh(Z_Param_DestinationStaticMesh,Z_Param_DestinationLodIndex,Z_Param_SourceStaticMesh,Z_Param_SourceLodIndex,Z_Param_bReuseExistingMaterialSlots);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execReimportAllCustomLODs)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorStaticMeshLibrary::ReimportAllCustomLODs(Z_Param_StaticMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execImportLOD)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_BaseStaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FStrProperty,Z_Param_SourceFilename);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::ImportLOD(Z_Param_BaseStaticMesh,Z_Param_LODIndex,Z_Param_SourceFilename);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execSetLodBuildSettings)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LodIndex);
		P_GET_STRUCT_REF(FMeshBuildSettings,Z_Param_Out_BuildOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorStaticMeshLibrary::SetLodBuildSettings(Z_Param_StaticMesh,Z_Param_LodIndex,Z_Param_Out_BuildOptions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetLodBuildSettings)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LodIndex);
		P_GET_STRUCT_REF(FMeshBuildSettings,Z_Param_Out_OutBuildOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorStaticMeshLibrary::GetLodBuildSettings(Z_Param_StaticMesh,Z_Param_LodIndex,Z_Param_Out_OutBuildOptions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execSetLodReductionSettings)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LodIndex);
		P_GET_STRUCT_REF(FMeshReductionSettings,Z_Param_Out_ReductionOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorStaticMeshLibrary::SetLodReductionSettings(Z_Param_StaticMesh,Z_Param_LodIndex,Z_Param_Out_ReductionOptions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execGetLodReductionSettings)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LodIndex);
		P_GET_STRUCT_REF(FMeshReductionSettings,Z_Param_Out_OutReductionOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorStaticMeshLibrary::GetLodReductionSettings(Z_Param_StaticMesh,Z_Param_LodIndex,Z_Param_Out_OutReductionOptions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execSetLods)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_STRUCT_REF(FEditorScriptingMeshReductionOptions,Z_Param_Out_ReductionOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::SetLods(Z_Param_StaticMesh,Z_Param_Out_ReductionOptions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorStaticMeshLibrary::execSetLodsWithNotification)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_STRUCT_REF(FEditorScriptingMeshReductionOptions,Z_Param_Out_ReductionOptions);
		P_GET_UBOOL(Z_Param_bApplyChanges);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorStaticMeshLibrary::SetLodsWithNotification(Z_Param_StaticMesh,Z_Param_Out_ReductionOptions,Z_Param_bApplyChanges);
		P_NATIVE_END;
	}
	void UEditorStaticMeshLibrary::StaticRegisterNativesUEditorStaticMeshLibrary()
	{
		UClass* Class = UEditorStaticMeshLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddSimpleCollisions", &UEditorStaticMeshLibrary::execAddSimpleCollisions },
			{ "AddSimpleCollisionsWithNotification", &UEditorStaticMeshLibrary::execAddSimpleCollisionsWithNotification },
			{ "AddUVChannel", &UEditorStaticMeshLibrary::execAddUVChannel },
			{ "BulkSetConvexDecompositionCollisions", &UEditorStaticMeshLibrary::execBulkSetConvexDecompositionCollisions },
			{ "BulkSetConvexDecompositionCollisionsWithNotification", &UEditorStaticMeshLibrary::execBulkSetConvexDecompositionCollisionsWithNotification },
			{ "EnableSectionCastShadow", &UEditorStaticMeshLibrary::execEnableSectionCastShadow },
			{ "EnableSectionCollision", &UEditorStaticMeshLibrary::execEnableSectionCollision },
			{ "GenerateBoxUVChannel", &UEditorStaticMeshLibrary::execGenerateBoxUVChannel },
			{ "GenerateCylindricalUVChannel", &UEditorStaticMeshLibrary::execGenerateCylindricalUVChannel },
			{ "GeneratePlanarUVChannel", &UEditorStaticMeshLibrary::execGeneratePlanarUVChannel },
			{ "GetCollisionComplexity", &UEditorStaticMeshLibrary::execGetCollisionComplexity },
			{ "GetConvexCollisionCount", &UEditorStaticMeshLibrary::execGetConvexCollisionCount },
			{ "GetLodBuildSettings", &UEditorStaticMeshLibrary::execGetLodBuildSettings },
			{ "GetLodCount", &UEditorStaticMeshLibrary::execGetLodCount },
			{ "GetLODMaterialSlot", &UEditorStaticMeshLibrary::execGetLODMaterialSlot },
			{ "GetLodReductionSettings", &UEditorStaticMeshLibrary::execGetLodReductionSettings },
			{ "GetLodScreenSizes", &UEditorStaticMeshLibrary::execGetLodScreenSizes },
			{ "GetNumberMaterials", &UEditorStaticMeshLibrary::execGetNumberMaterials },
			{ "GetNumberVerts", &UEditorStaticMeshLibrary::execGetNumberVerts },
			{ "GetNumUVChannels", &UEditorStaticMeshLibrary::execGetNumUVChannels },
			{ "GetSimpleCollisionCount", &UEditorStaticMeshLibrary::execGetSimpleCollisionCount },
			{ "HasInstanceVertexColors", &UEditorStaticMeshLibrary::execHasInstanceVertexColors },
			{ "HasVertexColors", &UEditorStaticMeshLibrary::execHasVertexColors },
			{ "ImportLOD", &UEditorStaticMeshLibrary::execImportLOD },
			{ "InsertUVChannel", &UEditorStaticMeshLibrary::execInsertUVChannel },
			{ "IsSectionCollisionEnabled", &UEditorStaticMeshLibrary::execIsSectionCollisionEnabled },
			{ "ReimportAllCustomLODs", &UEditorStaticMeshLibrary::execReimportAllCustomLODs },
			{ "RemoveCollisions", &UEditorStaticMeshLibrary::execRemoveCollisions },
			{ "RemoveCollisionsWithNotification", &UEditorStaticMeshLibrary::execRemoveCollisionsWithNotification },
			{ "RemoveLods", &UEditorStaticMeshLibrary::execRemoveLods },
			{ "RemoveUVChannel", &UEditorStaticMeshLibrary::execRemoveUVChannel },
			{ "SetAllowCPUAccess", &UEditorStaticMeshLibrary::execSetAllowCPUAccess },
			{ "SetConvexDecompositionCollisions", &UEditorStaticMeshLibrary::execSetConvexDecompositionCollisions },
			{ "SetConvexDecompositionCollisionsWithNotification", &UEditorStaticMeshLibrary::execSetConvexDecompositionCollisionsWithNotification },
			{ "SetGenerateLightmapUVs", &UEditorStaticMeshLibrary::execSetGenerateLightmapUVs },
			{ "SetLodBuildSettings", &UEditorStaticMeshLibrary::execSetLodBuildSettings },
			{ "SetLodFromStaticMesh", &UEditorStaticMeshLibrary::execSetLodFromStaticMesh },
			{ "SetLODMaterialSlot", &UEditorStaticMeshLibrary::execSetLODMaterialSlot },
			{ "SetLodReductionSettings", &UEditorStaticMeshLibrary::execSetLodReductionSettings },
			{ "SetLods", &UEditorStaticMeshLibrary::execSetLods },
			{ "SetLodsWithNotification", &UEditorStaticMeshLibrary::execSetLodsWithNotification },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics
	{
		struct EditorStaticMeshLibrary_eventAddSimpleCollisions_Parms
		{
			UStaticMesh* StaticMesh;
			EScriptingCollisionShapeType ShapeType;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ShapeType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShapeType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ShapeType;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventAddSimpleCollisions_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_ShapeType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_ShapeType_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_ShapeType = { "ShapeType", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventAddSimpleCollisions_Parms, ShapeType), Z_Construct_UEnum_EditorScriptingUtilities_EScriptingCollisionShapeType, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_ShapeType_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_ShapeType_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventAddSimpleCollisions_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_ShapeType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_ShapeType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Same as AddSimpleCollisionsWithNotification but changes are automatically applied.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Same as AddSimpleCollisionsWithNotification but changes are automatically applied." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "AddSimpleCollisions", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventAddSimpleCollisions_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics
	{
		struct EditorStaticMeshLibrary_eventAddSimpleCollisionsWithNotification_Parms
		{
			UStaticMesh* StaticMesh;
			EScriptingCollisionShapeType ShapeType;
			bool bApplyChanges;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ShapeType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShapeType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ShapeType;
		static void NewProp_bApplyChanges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyChanges;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventAddSimpleCollisionsWithNotification_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_ShapeType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_ShapeType_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_ShapeType = { "ShapeType", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventAddSimpleCollisionsWithNotification_Parms, ShapeType), Z_Construct_UEnum_EditorScriptingUtilities_EScriptingCollisionShapeType, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_ShapeType_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_ShapeType_MetaData)) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_bApplyChanges_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventAddSimpleCollisionsWithNotification_Parms*)Obj)->bApplyChanges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_bApplyChanges = { "bApplyChanges", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventAddSimpleCollisionsWithNotification_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_bApplyChanges_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventAddSimpleCollisionsWithNotification_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_ShapeType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_ShapeType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_bApplyChanges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Add simple collisions to a static mesh.\n\x09 * This method replicates what is done when invoking menu entries \"Collision > Add [...] Simplified Collision\" in the Mesh Editor.\n\x09 * @param\x09StaticMesh\x09\x09\x09Mesh to generate simple collision for.\n\x09 * @param\x09ShapeType\x09\x09\x09Options on which simple collision to add to the mesh.\n\x09 * @param\x09""bApplyChanges\x09\x09Indicates if changes must be apply or not.\n\x09 * @return An integer indicating the index of the collision newly created.\n\x09 * A negative value indicates the addition failed.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Add simple collisions to a static mesh.\nThis method replicates what is done when invoking menu entries \"Collision > Add [...] Simplified Collision\" in the Mesh Editor.\n@param       StaticMesh                      Mesh to generate simple collision for.\n@param       ShapeType                       Options on which simple collision to add to the mesh.\n@param       bApplyChanges           Indicates if changes must be apply or not.\n@return An integer indicating the index of the collision newly created.\nA negative value indicates the addition failed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "AddSimpleCollisionsWithNotification", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventAddSimpleCollisionsWithNotification_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics
	{
		struct EditorStaticMeshLibrary_eventAddUVChannel_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventAddUVChannel_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventAddUVChannel_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventAddUVChannel_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventAddUVChannel_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Adds an empty UV channel at the end of the existing channels on the given LOD of a StaticMesh.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh on which to add a UV channel.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @return true if a UV channel was added.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Adds an empty UV channel at the end of the existing channels on the given LOD of a StaticMesh.\n@param       StaticMesh                      Static mesh on which to add a UV channel.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@return true if a UV channel was added." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "AddUVChannel", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventAddUVChannel_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics
	{
		struct EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisions_Parms
		{
			TArray<UStaticMesh*> StaticMeshes;
			int32 HullCount;
			int32 MaxHullVerts;
			int32 HullPrecision;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StaticMeshes;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullCount;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxHullVerts;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullPrecision;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_StaticMeshes_Inner = { "StaticMeshes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_StaticMeshes_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_StaticMeshes = { "StaticMeshes", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisions_Parms, StaticMeshes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_StaticMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_StaticMeshes_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_HullCount = { "HullCount", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisions_Parms, HullCount), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_MaxHullVerts = { "MaxHullVerts", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisions_Parms, MaxHullVerts), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_HullPrecision = { "HullPrecision", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisions_Parms, HullPrecision), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisions_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisions_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_StaticMeshes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_StaticMeshes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_HullCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_MaxHullVerts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_HullPrecision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Same as SetConvexDecompositionCollisionsWithNotification but changes are automatically applied.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Same as SetConvexDecompositionCollisionsWithNotification but changes are automatically applied." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "BulkSetConvexDecompositionCollisions", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisions_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics
	{
		struct EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisionsWithNotification_Parms
		{
			TArray<UStaticMesh*> StaticMeshes;
			int32 HullCount;
			int32 MaxHullVerts;
			int32 HullPrecision;
			bool bApplyChanges;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StaticMeshes;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullCount;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxHullVerts;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullPrecision;
		static void NewProp_bApplyChanges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyChanges;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_StaticMeshes_Inner = { "StaticMeshes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_StaticMeshes_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_StaticMeshes = { "StaticMeshes", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisionsWithNotification_Parms, StaticMeshes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_StaticMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_StaticMeshes_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_HullCount = { "HullCount", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisionsWithNotification_Parms, HullCount), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_MaxHullVerts = { "MaxHullVerts", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisionsWithNotification_Parms, MaxHullVerts), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_HullPrecision = { "HullPrecision", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisionsWithNotification_Parms, HullPrecision), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_bApplyChanges_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisionsWithNotification_Parms*)Obj)->bApplyChanges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_bApplyChanges = { "bApplyChanges", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisionsWithNotification_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_bApplyChanges_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisionsWithNotification_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisionsWithNotification_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_StaticMeshes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_StaticMeshes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_HullCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_MaxHullVerts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_HullPrecision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_bApplyChanges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Compute convex collisions for a set of static meshes.\n\x09 * Any existing collisions will be removed from the static meshes.\n\x09 * This method replicates what is done when invoking menu entry \"Collision > Auto Convex Collision\" in the Mesh Editor.\n\x09 * @param\x09StaticMeshes\x09\x09\x09Set of Static mesh to add convex collision on.\n\x09 * @param\x09HullCount\x09\x09\x09\x09Maximum number of convex pieces that will be created. Must be positive.\n\x09 * @param\x09MaxHullVerts\x09\x09\x09Maximum number of vertices allowed for any generated convex hull.\n\x09 * @param\x09HullPrecision\x09\x09\x09Number of voxels to use when generating collision. Must be positive.\n\x09 * @param\x09""bApplyChanges\x09\x09\x09Indicates if changes must be apply or not.\n\x09 * @return A boolean indicating if the addition was successful or not.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Compute convex collisions for a set of static meshes.\nAny existing collisions will be removed from the static meshes.\nThis method replicates what is done when invoking menu entry \"Collision > Auto Convex Collision\" in the Mesh Editor.\n@param       StaticMeshes                    Set of Static mesh to add convex collision on.\n@param       HullCount                               Maximum number of convex pieces that will be created. Must be positive.\n@param       MaxHullVerts                    Maximum number of vertices allowed for any generated convex hull.\n@param       HullPrecision                   Number of voxels to use when generating collision. Must be positive.\n@param       bApplyChanges                   Indicates if changes must be apply or not.\n@return A boolean indicating if the addition was successful or not." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "BulkSetConvexDecompositionCollisionsWithNotification", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventBulkSetConvexDecompositionCollisionsWithNotification_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics
	{
		struct EditorStaticMeshLibrary_eventEnableSectionCastShadow_Parms
		{
			UStaticMesh* StaticMesh;
			bool bCastShadow;
			int32 LODIndex;
			int32 SectionIndex;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static void NewProp_bCastShadow_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCastShadow;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SectionIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventEnableSectionCastShadow_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::NewProp_bCastShadow_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventEnableSectionCastShadow_Parms*)Obj)->bCastShadow = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::NewProp_bCastShadow = { "bCastShadow", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventEnableSectionCastShadow_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::NewProp_bCastShadow_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventEnableSectionCastShadow_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::NewProp_SectionIndex = { "SectionIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventEnableSectionCastShadow_Parms, SectionIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::NewProp_bCastShadow,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::NewProp_SectionIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Enables/disables mesh section shadow casting for a specific LOD.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh to Enables/disables shadow casting from.\n\x09 * @param\x09""bCastShadow\x09\x09\x09If the section should cast shadow.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09SectionIndex\x09\x09Index of the StaticMesh Section.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Enables/disables mesh section shadow casting for a specific LOD.\n@param       StaticMesh                      Static mesh to Enables/disables shadow casting from.\n@param       bCastShadow                     If the section should cast shadow.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       SectionIndex            Index of the StaticMesh Section." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "EnableSectionCastShadow", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventEnableSectionCastShadow_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics
	{
		struct EditorStaticMeshLibrary_eventEnableSectionCollision_Parms
		{
			UStaticMesh* StaticMesh;
			bool bCollisionEnabled;
			int32 LODIndex;
			int32 SectionIndex;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static void NewProp_bCollisionEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCollisionEnabled;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SectionIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventEnableSectionCollision_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::NewProp_bCollisionEnabled_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventEnableSectionCollision_Parms*)Obj)->bCollisionEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::NewProp_bCollisionEnabled = { "bCollisionEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventEnableSectionCollision_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::NewProp_bCollisionEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventEnableSectionCollision_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::NewProp_SectionIndex = { "SectionIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventEnableSectionCollision_Parms, SectionIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::NewProp_bCollisionEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::NewProp_SectionIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Enables/disables mesh section collision for a specific LOD.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh to Enables/disables collisions from.\n\x09 * @param\x09""bCollisionEnabled\x09If the collision is enabled or not.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09SectionIndex\x09\x09Index of the StaticMesh Section.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Enables/disables mesh section collision for a specific LOD.\n@param       StaticMesh                      Static mesh to Enables/disables collisions from.\n@param       bCollisionEnabled       If the collision is enabled or not.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       SectionIndex            Index of the StaticMesh Section." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "EnableSectionCollision", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventEnableSectionCollision_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics
	{
		struct EditorStaticMeshLibrary_eventGenerateBoxUVChannel_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			int32 UVChannelIndex;
			FVector Position;
			FRotator Orientation;
			FVector Size;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UVChannelIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Orientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Orientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Size;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateBoxUVChannel_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateBoxUVChannel_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_UVChannelIndex = { "UVChannelIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateBoxUVChannel_Parms, UVChannelIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Position_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateBoxUVChannel_Parms, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Orientation_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Orientation = { "Orientation", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateBoxUVChannel_Parms, Orientation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Orientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Orientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Size_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateBoxUVChannel_Parms, Size), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Size_MetaData)) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventGenerateBoxUVChannel_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventGenerateBoxUVChannel_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_UVChannelIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Orientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_Size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Generates box UV mapping in the specified UV channel on the given LOD of a StaticMesh.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh on which to generate the UV mapping.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09UVChannelIndex\x09\x09""Channel where to save the UV mapping.\n\x09 * @param\x09Position\x09\x09\x09Position of the center of the projection gizmo.\n\x09 * @param\x09Orientation\x09\x09\x09Rotation to apply to the projection gizmo.\n\x09 * @param\x09Size\x09\x09\x09\x09The size of the box projection gizmo.\n\x09 * @return true if the UV mapping was generated.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Generates box UV mapping in the specified UV channel on the given LOD of a StaticMesh.\n@param       StaticMesh                      Static mesh on which to generate the UV mapping.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       UVChannelIndex          Channel where to save the UV mapping.\n@param       Position                        Position of the center of the projection gizmo.\n@param       Orientation                     Rotation to apply to the projection gizmo.\n@param       Size                            The size of the box projection gizmo.\n@return true if the UV mapping was generated." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GenerateBoxUVChannel", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGenerateBoxUVChannel_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics
	{
		struct EditorStaticMeshLibrary_eventGenerateCylindricalUVChannel_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			int32 UVChannelIndex;
			FVector Position;
			FRotator Orientation;
			FVector2D Tiling;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UVChannelIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Orientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Orientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tiling_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Tiling;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateCylindricalUVChannel_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateCylindricalUVChannel_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_UVChannelIndex = { "UVChannelIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateCylindricalUVChannel_Parms, UVChannelIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Position_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateCylindricalUVChannel_Parms, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Orientation_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Orientation = { "Orientation", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateCylindricalUVChannel_Parms, Orientation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Orientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Orientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Tiling_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Tiling = { "Tiling", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGenerateCylindricalUVChannel_Parms, Tiling), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Tiling_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Tiling_MetaData)) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventGenerateCylindricalUVChannel_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventGenerateCylindricalUVChannel_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_UVChannelIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Orientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_Tiling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Generates cylindrical UV mapping in the specified UV channel on the given LOD of a StaticMesh.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh on which to generate the UV mapping.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09UVChannelIndex\x09\x09""Channel where to save the UV mapping.\n\x09 * @param\x09Position\x09\x09\x09Position of the center of the projection gizmo.\n\x09 * @param\x09Orientation\x09\x09\x09Rotation to apply to the projection gizmo.\n\x09 * @param\x09Tiling\x09\x09\x09\x09The UV tiling to use to generate the UV mapping.\n\x09 * @return true if the UV mapping was generated.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Generates cylindrical UV mapping in the specified UV channel on the given LOD of a StaticMesh.\n@param       StaticMesh                      Static mesh on which to generate the UV mapping.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       UVChannelIndex          Channel where to save the UV mapping.\n@param       Position                        Position of the center of the projection gizmo.\n@param       Orientation                     Rotation to apply to the projection gizmo.\n@param       Tiling                          The UV tiling to use to generate the UV mapping.\n@return true if the UV mapping was generated." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GenerateCylindricalUVChannel", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGenerateCylindricalUVChannel_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics
	{
		struct EditorStaticMeshLibrary_eventGeneratePlanarUVChannel_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			int32 UVChannelIndex;
			FVector Position;
			FRotator Orientation;
			FVector2D Tiling;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UVChannelIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Orientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Orientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tiling_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Tiling;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGeneratePlanarUVChannel_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGeneratePlanarUVChannel_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_UVChannelIndex = { "UVChannelIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGeneratePlanarUVChannel_Parms, UVChannelIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Position_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGeneratePlanarUVChannel_Parms, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Orientation_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Orientation = { "Orientation", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGeneratePlanarUVChannel_Parms, Orientation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Orientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Orientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Tiling_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Tiling = { "Tiling", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGeneratePlanarUVChannel_Parms, Tiling), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Tiling_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Tiling_MetaData)) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventGeneratePlanarUVChannel_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventGeneratePlanarUVChannel_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_UVChannelIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Orientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_Tiling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Generates planar UV mapping in the specified UV channel on the given LOD of a StaticMesh.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh on which to generate the UV mapping.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09UVChannelIndex\x09\x09""Channel where to save the UV mapping.\n\x09 * @param\x09Position\x09\x09\x09Position of the center of the projection gizmo.\n\x09 * @param\x09Orientation\x09\x09\x09Rotation to apply to the projection gizmo.\n\x09 * @param\x09Tiling\x09\x09\x09\x09The UV tiling to use to generate the UV mapping.\n\x09 * @return true if the UV mapping was generated.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Generates planar UV mapping in the specified UV channel on the given LOD of a StaticMesh.\n@param       StaticMesh                      Static mesh on which to generate the UV mapping.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       UVChannelIndex          Channel where to save the UV mapping.\n@param       Position                        Position of the center of the projection gizmo.\n@param       Orientation                     Rotation to apply to the projection gizmo.\n@param       Tiling                          The UV tiling to use to generate the UV mapping.\n@return true if the UV mapping was generated." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GeneratePlanarUVChannel", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGeneratePlanarUVChannel_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics
	{
		struct EditorStaticMeshLibrary_eventGetCollisionComplexity_Parms
		{
			UStaticMesh* StaticMesh;
			TEnumAsByte<ECollisionTraceFlag> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetCollisionComplexity_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetCollisionComplexity_Parms, ReturnValue), Z_Construct_UEnum_PhysicsCore_ECollisionTraceFlag, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Get the Collision Trace behavior of a static mesh\n\x09 * @param\x09StaticMesh\x09\x09\x09\x09Mesh to query on.\n\x09 * @return the Collision Trace behavior.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Get the Collision Trace behavior of a static mesh\n@param       StaticMesh                              Mesh to query on.\n@return the Collision Trace behavior." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetCollisionComplexity", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetCollisionComplexity_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics
	{
		struct EditorStaticMeshLibrary_eventGetConvexCollisionCount_Parms
		{
			UStaticMesh* StaticMesh;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetConvexCollisionCount_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetConvexCollisionCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Get number of convex collisions present on a static mesh.\n\x09 * @param\x09StaticMesh\x09\x09\x09\x09Mesh to query on.\n\x09 * @return An integer representing the number of convex collisions on the input static mesh.\n\x09 * An negative value indicates that the command could not be executed. See log for explanation.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Get number of convex collisions present on a static mesh.\n@param       StaticMesh                              Mesh to query on.\n@return An integer representing the number of convex collisions on the input static mesh.\nAn negative value indicates that the command could not be executed. See log for explanation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetConvexCollisionCount", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetConvexCollisionCount_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics
	{
		struct EditorStaticMeshLibrary_eventGetLodBuildSettings_Parms
		{
			const UStaticMesh* StaticMesh;
			int32 LodIndex;
			FMeshBuildSettings OutBuildOptions;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LodIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LodIndex;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutBuildOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_StaticMesh_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLodBuildSettings_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_StaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_StaticMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_LodIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_LodIndex = { "LodIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLodBuildSettings_Parms, LodIndex), METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_LodIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_LodIndex_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_OutBuildOptions = { "OutBuildOptions", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLodBuildSettings_Parms, OutBuildOptions), Z_Construct_UScriptStruct_FMeshBuildSettings, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_LodIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::NewProp_OutBuildOptions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Copy the build options with the specified LOD build settings.\n\x09 * @param StaticMesh - Mesh to process.\n\x09 * @param LodIndex - The LOD we get the reduction settings.\n\x09 * @param OutBuildOptions - The build settings where we copy the build options.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Copy the build options with the specified LOD build settings.\n@param StaticMesh - Mesh to process.\n@param LodIndex - The LOD we get the reduction settings.\n@param OutBuildOptions - The build settings where we copy the build options." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetLodBuildSettings", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetLodBuildSettings_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics
	{
		struct EditorStaticMeshLibrary_eventGetLodCount_Parms
		{
			UStaticMesh* StaticMesh;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLodCount_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLodCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Get number of LODs present on a static mesh.\n\x09 * @param\x09StaticMesh\x09\x09\x09\x09Mesh to process.\n\x09 * @return the number of LODs present on the input mesh.\n\x09 * An negative value indicates that the command could not be executed. See log for explanation.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Get number of LODs present on a static mesh.\n@param       StaticMesh                              Mesh to process.\n@return the number of LODs present on the input mesh.\nAn negative value indicates that the command could not be executed. See log for explanation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetLodCount", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetLodCount_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics
	{
		struct EditorStaticMeshLibrary_eventGetLODMaterialSlot_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			int32 SectionIndex;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SectionIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLODMaterialSlot_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLODMaterialSlot_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::NewProp_SectionIndex = { "SectionIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLODMaterialSlot_Parms, SectionIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLODMaterialSlot_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::NewProp_SectionIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Gets the material slot used for a specific LOD section.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh to get the material index from.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09SectionIndex\x09\x09Index of the StaticMesh Section.\n\x09 * @return  MaterialSlotIndex\x09Index of the material slot used by the section or INDEX_NONE in case of error.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Gets the material slot used for a specific LOD section.\n@param       StaticMesh                      Static mesh to get the material index from.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       SectionIndex            Index of the StaticMesh Section.\n@return  MaterialSlotIndex   Index of the material slot used by the section or INDEX_NONE in case of error." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetLODMaterialSlot", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetLODMaterialSlot_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics
	{
		struct EditorStaticMeshLibrary_eventGetLodReductionSettings_Parms
		{
			const UStaticMesh* StaticMesh;
			int32 LodIndex;
			FMeshReductionSettings OutReductionOptions;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LodIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LodIndex;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutReductionOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_StaticMesh_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLodReductionSettings_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_StaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_StaticMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_LodIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_LodIndex = { "LodIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLodReductionSettings_Parms, LodIndex), METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_LodIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_LodIndex_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_OutReductionOptions = { "OutReductionOptions", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLodReductionSettings_Parms, OutReductionOptions), Z_Construct_UScriptStruct_FMeshReductionSettings, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_LodIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::NewProp_OutReductionOptions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Copy the reduction options with the specified LOD reduction settings.\n\x09 * @param StaticMesh - Mesh to process.\n\x09 * @param LodIndex - The LOD we get the reduction settings.\n\x09 * @param OutReductionOptions - The reduction settings where we copy the reduction options.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Copy the reduction options with the specified LOD reduction settings.\n@param StaticMesh - Mesh to process.\n@param LodIndex - The LOD we get the reduction settings.\n@param OutReductionOptions - The reduction settings where we copy the reduction options." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetLodReductionSettings", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetLodReductionSettings_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics
	{
		struct EditorStaticMeshLibrary_eventGetLodScreenSizes_Parms
		{
			UStaticMesh* StaticMesh;
			TArray<float> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLodScreenSizes_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetLodScreenSizes_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Get an array of LOD screen sizes for evaluation.\n\x09 * @param\x09StaticMesh\x09\x09\x09Mesh to process.\n\x09 * @return array of LOD screen sizes.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Get an array of LOD screen sizes for evaluation.\n@param       StaticMesh                      Mesh to process.\n@return array of LOD screen sizes." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetLodScreenSizes", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetLodScreenSizes_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics
	{
		struct EditorStaticMeshLibrary_eventGetNumberMaterials_Parms
		{
			UStaticMesh* StaticMesh;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetNumberMaterials_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetNumberMaterials_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/** Get number of StaticMesh verts for an LOD */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Get number of StaticMesh verts for an LOD" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetNumberMaterials", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetNumberMaterials_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics
	{
		struct EditorStaticMeshLibrary_eventGetNumberVerts_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetNumberVerts_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetNumberVerts_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetNumberVerts_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/** Get number of StaticMesh verts for an LOD */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Get number of StaticMesh verts for an LOD" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetNumberVerts", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetNumberVerts_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics
	{
		struct EditorStaticMeshLibrary_eventGetNumUVChannels_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetNumUVChannels_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetNumUVChannels_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetNumUVChannels_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Returns the number of UV channels for the given LOD of a StaticMesh.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh to query.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @return the number of UV channels.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Returns the number of UV channels for the given LOD of a StaticMesh.\n@param       StaticMesh                      Static mesh to query.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@return the number of UV channels." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetNumUVChannels", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetNumUVChannels_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics
	{
		struct EditorStaticMeshLibrary_eventGetSimpleCollisionCount_Parms
		{
			UStaticMesh* StaticMesh;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetSimpleCollisionCount_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventGetSimpleCollisionCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Get number of simple collisions present on a static mesh.\n\x09 * @param\x09StaticMesh\x09\x09\x09\x09Mesh to query on.\n\x09 * @return An integer representing the number of simple collisions on the input static mesh.\n\x09 * An negative value indicates that the command could not be executed. See log for explanation.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Get number of simple collisions present on a static mesh.\n@param       StaticMesh                              Mesh to query on.\n@return An integer representing the number of simple collisions on the input static mesh.\nAn negative value indicates that the command could not be executed. See log for explanation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "GetSimpleCollisionCount", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventGetSimpleCollisionCount_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics
	{
		struct EditorStaticMeshLibrary_eventHasInstanceVertexColors_Parms
		{
			UStaticMeshComponent* StaticMeshComponent;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshComponent;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::NewProp_StaticMeshComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::NewProp_StaticMeshComponent = { "StaticMeshComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventHasInstanceVertexColors_Parms, StaticMeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::NewProp_StaticMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::NewProp_StaticMeshComponent_MetaData)) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventHasInstanceVertexColors_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventHasInstanceVertexColors_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::NewProp_StaticMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/** Check whether a static mesh component has vertex colors */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Check whether a static mesh component has vertex colors" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "HasInstanceVertexColors", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventHasInstanceVertexColors_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics
	{
		struct EditorStaticMeshLibrary_eventHasVertexColors_Parms
		{
			UStaticMesh* StaticMesh;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventHasVertexColors_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventHasVertexColors_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventHasVertexColors_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/** Check whether a static mesh has vertex colors */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Check whether a static mesh has vertex colors" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "HasVertexColors", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventHasVertexColors_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics
	{
		struct EditorStaticMeshLibrary_eventImportLOD_Parms
		{
			UStaticMesh* BaseStaticMesh;
			int32 LODIndex;
			FString SourceFilename;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BaseStaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LODIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceFilename_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SourceFilename;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_BaseStaticMesh = { "BaseStaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventImportLOD_Parms, BaseStaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_LODIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventImportLOD_Parms, LODIndex), METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_LODIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_LODIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_SourceFilename_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_SourceFilename = { "SourceFilename", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventImportLOD_Parms, SourceFilename), METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_SourceFilename_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_SourceFilename_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventImportLOD_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_BaseStaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_SourceFilename,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Import or re-import a LOD into the specified base mesh. If the LOD do not exist it will import it and add it to the base static mesh. If the LOD already exist it will re-import the specified LOD.\n\x09 *\n\x09 * @param BaseStaticMesh: The static mesh we import or re-import a LOD.\n\x09 * @param LODIndex: The index of the LOD to import or re-import. Valid value should be between 0 and the base static mesh LOD number. Invalid value will return INDEX_NONE\n\x09 * @param SourceFilename: The fbx source filename. If we are re-importing an existing LOD, it can be empty in this case it will use the last import file. Otherwise it must be an existing fbx file.\n\x09 *\n\x09 * @return the index of the LOD that was imported or re-imported. Will return INDEX_NONE if anything goes bad.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Import or re-import a LOD into the specified base mesh. If the LOD do not exist it will import it and add it to the base static mesh. If the LOD already exist it will re-import the specified LOD.\n\n@param BaseStaticMesh: The static mesh we import or re-import a LOD.\n@param LODIndex: The index of the LOD to import or re-import. Valid value should be between 0 and the base static mesh LOD number. Invalid value will return INDEX_NONE\n@param SourceFilename: The fbx source filename. If we are re-importing an existing LOD, it can be empty in this case it will use the last import file. Otherwise it must be an existing fbx file.\n\n@return the index of the LOD that was imported or re-imported. Will return INDEX_NONE if anything goes bad." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "ImportLOD", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventImportLOD_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics
	{
		struct EditorStaticMeshLibrary_eventInsertUVChannel_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			int32 UVChannelIndex;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UVChannelIndex;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventInsertUVChannel_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventInsertUVChannel_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::NewProp_UVChannelIndex = { "UVChannelIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventInsertUVChannel_Parms, UVChannelIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventInsertUVChannel_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventInsertUVChannel_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::NewProp_UVChannelIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Inserts an empty UV channel at the specified channel index on the given LOD of a StaticMesh.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh on which to insert a UV channel.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09UVChannelIndex\x09\x09Index where to insert the UV channel.\n\x09 * @return true if a UV channel was added.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Inserts an empty UV channel at the specified channel index on the given LOD of a StaticMesh.\n@param       StaticMesh                      Static mesh on which to insert a UV channel.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       UVChannelIndex          Index where to insert the UV channel.\n@return true if a UV channel was added." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "InsertUVChannel", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventInsertUVChannel_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics
	{
		struct EditorStaticMeshLibrary_eventIsSectionCollisionEnabled_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			int32 SectionIndex;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SectionIndex;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventIsSectionCollisionEnabled_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventIsSectionCollisionEnabled_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::NewProp_SectionIndex = { "SectionIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventIsSectionCollisionEnabled_Parms, SectionIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventIsSectionCollisionEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventIsSectionCollisionEnabled_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::NewProp_SectionIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Checks if a specific LOD mesh section has collision.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh to remove collisions from.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09SectionIndex\x09\x09Index of the StaticMesh Section.\n\x09 * @return True is the collision is enabled for the specified LOD of the StaticMesh section.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Checks if a specific LOD mesh section has collision.\n@param       StaticMesh                      Static mesh to remove collisions from.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       SectionIndex            Index of the StaticMesh Section.\n@return True is the collision is enabled for the specified LOD of the StaticMesh section." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "IsSectionCollisionEnabled", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventIsSectionCollisionEnabled_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics
	{
		struct EditorStaticMeshLibrary_eventReimportAllCustomLODs_Parms
		{
			UStaticMesh* StaticMesh;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventReimportAllCustomLODs_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventReimportAllCustomLODs_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventReimportAllCustomLODs_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Re-import all the custom LODs present in the specified static mesh.\n\x09 *\n\x09 * @param StaticMesh: is the static mesh we import or re-import all LODs.\n\x09 *\n\x09 * @return true if re-import all LODs works, false otherwise see log for explanation.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Re-import all the custom LODs present in the specified static mesh.\n\n@param StaticMesh: is the static mesh we import or re-import all LODs.\n\n@return true if re-import all LODs works, false otherwise see log for explanation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "ReimportAllCustomLODs", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventReimportAllCustomLODs_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics
	{
		struct EditorStaticMeshLibrary_eventRemoveCollisions_Parms
		{
			UStaticMesh* StaticMesh;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventRemoveCollisions_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventRemoveCollisions_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventRemoveCollisions_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Same as RemoveCollisionsWithNotification but changes are applied.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Same as RemoveCollisionsWithNotification but changes are applied." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "RemoveCollisions", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventRemoveCollisions_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics
	{
		struct EditorStaticMeshLibrary_eventRemoveCollisionsWithNotification_Parms
		{
			UStaticMesh* StaticMesh;
			bool bApplyChanges;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static void NewProp_bApplyChanges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyChanges;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventRemoveCollisionsWithNotification_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::NewProp_bApplyChanges_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventRemoveCollisionsWithNotification_Parms*)Obj)->bApplyChanges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::NewProp_bApplyChanges = { "bApplyChanges", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventRemoveCollisionsWithNotification_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::NewProp_bApplyChanges_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventRemoveCollisionsWithNotification_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventRemoveCollisionsWithNotification_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::NewProp_bApplyChanges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Remove collisions from a static mesh.\n\x09 * This method replicates what is done when invoking menu entries \"Collision > Remove Collision\" in the Mesh Editor.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh to remove collisions from.\n\x09 * @param\x09""bApplyChanges\x09\x09Indicates if changes must be apply or not.\n\x09 * @return A boolean indicating if the removal was successful or not.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Remove collisions from a static mesh.\nThis method replicates what is done when invoking menu entries \"Collision > Remove Collision\" in the Mesh Editor.\n@param       StaticMesh                      Static mesh to remove collisions from.\n@param       bApplyChanges           Indicates if changes must be apply or not.\n@return A boolean indicating if the removal was successful or not." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "RemoveCollisionsWithNotification", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventRemoveCollisionsWithNotification_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics
	{
		struct EditorStaticMeshLibrary_eventRemoveLods_Parms
		{
			UStaticMesh* StaticMesh;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventRemoveLods_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventRemoveLods_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventRemoveLods_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Remove LODs on a static mesh except LOD 0.\n\x09 * @param\x09StaticMesh\x09\x09\x09Mesh to remove LOD from.\n\x09 * @return A boolean indicating if the removal was successful, true, or not.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Remove LODs on a static mesh except LOD 0.\n@param       StaticMesh                      Mesh to remove LOD from.\n@return A boolean indicating if the removal was successful, true, or not." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "RemoveLods", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventRemoveLods_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics
	{
		struct EditorStaticMeshLibrary_eventRemoveUVChannel_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			int32 UVChannelIndex;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UVChannelIndex;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventRemoveUVChannel_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventRemoveUVChannel_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::NewProp_UVChannelIndex = { "UVChannelIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventRemoveUVChannel_Parms, UVChannelIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventRemoveUVChannel_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventRemoveUVChannel_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::NewProp_UVChannelIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Removes the UV channel at the specified channel index on the given LOD of a StaticMesh.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh on which to remove the UV channel.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09UVChannelIndex\x09\x09Index where to remove the UV channel.\n\x09 * @return true if the UV channel was removed.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Removes the UV channel at the specified channel index on the given LOD of a StaticMesh.\n@param       StaticMesh                      Static mesh on which to remove the UV channel.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       UVChannelIndex          Index where to remove the UV channel.\n@return true if the UV channel was removed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "RemoveUVChannel", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventRemoveUVChannel_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics
	{
		struct EditorStaticMeshLibrary_eventSetAllowCPUAccess_Parms
		{
			UStaticMesh* StaticMesh;
			bool bAllowCPUAccess;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static void NewProp_bAllowCPUAccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowCPUAccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetAllowCPUAccess_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::NewProp_bAllowCPUAccess_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventSetAllowCPUAccess_Parms*)Obj)->bAllowCPUAccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::NewProp_bAllowCPUAccess = { "bAllowCPUAccess", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventSetAllowCPUAccess_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::NewProp_bAllowCPUAccess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::NewProp_bAllowCPUAccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/** Sets StaticMeshFlag bAllowCPUAccess  */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Sets StaticMeshFlag bAllowCPUAccess" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "SetAllowCPUAccess", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventSetAllowCPUAccess_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics
	{
		struct EditorStaticMeshLibrary_eventSetConvexDecompositionCollisions_Parms
		{
			UStaticMesh* StaticMesh;
			int32 HullCount;
			int32 MaxHullVerts;
			int32 HullPrecision;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullCount;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxHullVerts;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullPrecision;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisions_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_HullCount = { "HullCount", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisions_Parms, HullCount), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_MaxHullVerts = { "MaxHullVerts", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisions_Parms, MaxHullVerts), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_HullPrecision = { "HullPrecision", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisions_Parms, HullPrecision), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventSetConvexDecompositionCollisions_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisions_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_HullCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_MaxHullVerts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_HullPrecision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Same as SetConvexDecompositionCollisionsWithNotification but changes are automatically applied.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Same as SetConvexDecompositionCollisionsWithNotification but changes are automatically applied." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "SetConvexDecompositionCollisions", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisions_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics
	{
		struct EditorStaticMeshLibrary_eventSetConvexDecompositionCollisionsWithNotification_Parms
		{
			UStaticMesh* StaticMesh;
			int32 HullCount;
			int32 MaxHullVerts;
			int32 HullPrecision;
			bool bApplyChanges;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullCount;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxHullVerts;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullPrecision;
		static void NewProp_bApplyChanges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyChanges;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisionsWithNotification_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_HullCount = { "HullCount", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisionsWithNotification_Parms, HullCount), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_MaxHullVerts = { "MaxHullVerts", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisionsWithNotification_Parms, MaxHullVerts), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_HullPrecision = { "HullPrecision", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisionsWithNotification_Parms, HullPrecision), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_bApplyChanges_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventSetConvexDecompositionCollisionsWithNotification_Parms*)Obj)->bApplyChanges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_bApplyChanges = { "bApplyChanges", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisionsWithNotification_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_bApplyChanges_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventSetConvexDecompositionCollisionsWithNotification_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisionsWithNotification_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_HullCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_MaxHullVerts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_HullPrecision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_bApplyChanges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Add a convex collision to a static mesh.\n\x09 * Any existing collisions will be removed from the static mesh.\n\x09 * This method replicates what is done when invoking menu entry \"Collision > Auto Convex Collision\" in the Mesh Editor.\n\x09 * @param\x09StaticMesh\x09\x09\x09\x09Static mesh to add convex collision on.\n\x09 * @param\x09HullCount\x09\x09\x09\x09Maximum number of convex pieces that will be created. Must be positive.\n\x09 * @param\x09MaxHullVerts\x09\x09\x09Maximum number of vertices allowed for any generated convex hull.\n\x09 * @param\x09HullPrecision\x09\x09\x09Number of voxels to use when generating collision. Must be positive.\n\x09 * @param\x09""bApplyChanges\x09\x09\x09Indicates if changes must be apply or not.\n\x09 * @return A boolean indicating if the addition was successful or not.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Add a convex collision to a static mesh.\nAny existing collisions will be removed from the static mesh.\nThis method replicates what is done when invoking menu entry \"Collision > Auto Convex Collision\" in the Mesh Editor.\n@param       StaticMesh                              Static mesh to add convex collision on.\n@param       HullCount                               Maximum number of convex pieces that will be created. Must be positive.\n@param       MaxHullVerts                    Maximum number of vertices allowed for any generated convex hull.\n@param       HullPrecision                   Number of voxels to use when generating collision. Must be positive.\n@param       bApplyChanges                   Indicates if changes must be apply or not.\n@return A boolean indicating if the addition was successful or not." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "SetConvexDecompositionCollisionsWithNotification", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventSetConvexDecompositionCollisionsWithNotification_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics
	{
		struct EditorStaticMeshLibrary_eventSetGenerateLightmapUVs_Parms
		{
			UStaticMesh* StaticMesh;
			bool bGenerateLightmapUVs;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static void NewProp_bGenerateLightmapUVs_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGenerateLightmapUVs;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetGenerateLightmapUVs_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::NewProp_bGenerateLightmapUVs_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventSetGenerateLightmapUVs_Parms*)Obj)->bGenerateLightmapUVs = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::NewProp_bGenerateLightmapUVs = { "bGenerateLightmapUVs", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventSetGenerateLightmapUVs_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::NewProp_bGenerateLightmapUVs_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventSetGenerateLightmapUVs_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventSetGenerateLightmapUVs_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::NewProp_bGenerateLightmapUVs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/** Set Generate Lightmap UVs for StaticMesh */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ScriptName", "SetGenerateLightmapUv" },
		{ "ToolTip", "Set Generate Lightmap UVs for StaticMesh" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "SetGenerateLightmapUVs", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventSetGenerateLightmapUVs_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics
	{
		struct EditorStaticMeshLibrary_eventSetLodBuildSettings_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LodIndex;
			FMeshBuildSettings BuildOptions;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LodIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LodIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuildOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BuildOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodBuildSettings_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_LodIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_LodIndex = { "LodIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodBuildSettings_Parms, LodIndex), METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_LodIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_LodIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_BuildOptions_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_BuildOptions = { "BuildOptions", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodBuildSettings_Parms, BuildOptions), Z_Construct_UScriptStruct_FMeshBuildSettings, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_BuildOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_BuildOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_LodIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::NewProp_BuildOptions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Set the LOD build options for the specified LOD index.\n\x09 * @param StaticMesh - Mesh to process.\n\x09 * @param LodIndex - The LOD we will apply the build settings.\n\x09 * @param BuildOptions - The build settings we want to apply to the LOD.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Set the LOD build options for the specified LOD index.\n@param StaticMesh - Mesh to process.\n@param LodIndex - The LOD we will apply the build settings.\n@param BuildOptions - The build settings we want to apply to the LOD." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "SetLodBuildSettings", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventSetLodBuildSettings_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics
	{
		struct EditorStaticMeshLibrary_eventSetLodFromStaticMesh_Parms
		{
			UStaticMesh* DestinationStaticMesh;
			int32 DestinationLodIndex;
			UStaticMesh* SourceStaticMesh;
			int32 SourceLodIndex;
			bool bReuseExistingMaterialSlots;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DestinationStaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DestinationLodIndex;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceStaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SourceLodIndex;
		static void NewProp_bReuseExistingMaterialSlots_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReuseExistingMaterialSlots;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_DestinationStaticMesh = { "DestinationStaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodFromStaticMesh_Parms, DestinationStaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_DestinationLodIndex = { "DestinationLodIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodFromStaticMesh_Parms, DestinationLodIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_SourceStaticMesh = { "SourceStaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodFromStaticMesh_Parms, SourceStaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_SourceLodIndex = { "SourceLodIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodFromStaticMesh_Parms, SourceLodIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_bReuseExistingMaterialSlots_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventSetLodFromStaticMesh_Parms*)Obj)->bReuseExistingMaterialSlots = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_bReuseExistingMaterialSlots = { "bReuseExistingMaterialSlots", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventSetLodFromStaticMesh_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_bReuseExistingMaterialSlots_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodFromStaticMesh_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_DestinationStaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_DestinationLodIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_SourceStaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_SourceLodIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_bReuseExistingMaterialSlots,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Adds or create a LOD at DestinationLodIndex using the geometry from SourceStaticMesh SourceLodIndex\n\x09 * @param\x09""DestinationStaticMesh\x09\x09The static mesh to set the LOD in.\n\x09 * @param\x09""DestinationLodIndex\x09\x09\x09The index of the LOD to set.\n\x09 * @param\x09SourceStaticMesh\x09\x09\x09The static mesh to get the LOD from.\n\x09 * @param\x09SourceLodIndex\x09\x09\x09\x09The index of the LOD to get.\n\x09 * @param\x09""bReuseExistingMaterialSlots\x09If true, sections from SourceStaticMesh will be remapped to match the material slots of DestinationStaticMesh\n\x09\x09\x09\x09\x09\x09\x09\x09\x09\x09\x09when they have the same material assigned. If false, all material slots of SourceStaticMesh will be appended in DestinationStaticMesh.\n\x09 * @return\x09The index of the LOD that was set. It can be different than DestinationLodIndex if it wasn't a valid index.\n\x09 *\x09\x09\x09""A negative value indicates that the LOD was not set. See log for explanation.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Adds or create a LOD at DestinationLodIndex using the geometry from SourceStaticMesh SourceLodIndex\n@param       DestinationStaticMesh           The static mesh to set the LOD in.\n@param       DestinationLodIndex                     The index of the LOD to set.\n@param       SourceStaticMesh                        The static mesh to get the LOD from.\n@param       SourceLodIndex                          The index of the LOD to get.\n@param       bReuseExistingMaterialSlots     If true, sections from SourceStaticMesh will be remapped to match the material slots of DestinationStaticMesh\n                                                                                       when they have the same material assigned. If false, all material slots of SourceStaticMesh will be appended in DestinationStaticMesh.\n@return      The index of the LOD that was set. It can be different than DestinationLodIndex if it wasn't a valid index.\n                     A negative value indicates that the LOD was not set. See log for explanation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "SetLodFromStaticMesh", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventSetLodFromStaticMesh_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics
	{
		struct EditorStaticMeshLibrary_eventSetLODMaterialSlot_Parms
		{
			UStaticMesh* StaticMesh;
			int32 MaterialSlotIndex;
			int32 LODIndex;
			int32 SectionIndex;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaterialSlotIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SectionIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLODMaterialSlot_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::NewProp_MaterialSlotIndex = { "MaterialSlotIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLODMaterialSlot_Parms, MaterialSlotIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLODMaterialSlot_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::NewProp_SectionIndex = { "SectionIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLODMaterialSlot_Parms, SectionIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::NewProp_MaterialSlotIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::NewProp_SectionIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Sets the material slot for a specific LOD.\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh to Enables/disables shadow casting from.\n\x09 * @param\x09MaterialSlotIndex\x09Index of the material slot to use.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09SectionIndex\x09\x09Index of the StaticMesh Section.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Sets the material slot for a specific LOD.\n@param       StaticMesh                      Static mesh to Enables/disables shadow casting from.\n@param       MaterialSlotIndex       Index of the material slot to use.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       SectionIndex            Index of the StaticMesh Section." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "SetLODMaterialSlot", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventSetLODMaterialSlot_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics
	{
		struct EditorStaticMeshLibrary_eventSetLodReductionSettings_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LodIndex;
			FMeshReductionSettings ReductionOptions;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LodIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LodIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReductionOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReductionOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodReductionSettings_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_LodIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_LodIndex = { "LodIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodReductionSettings_Parms, LodIndex), METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_LodIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_LodIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_ReductionOptions_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_ReductionOptions = { "ReductionOptions", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodReductionSettings_Parms, ReductionOptions), Z_Construct_UScriptStruct_FMeshReductionSettings, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_ReductionOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_ReductionOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_LodIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::NewProp_ReductionOptions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Set the LOD reduction for the specified LOD index.\n\x09 * @param StaticMesh - Mesh to process.\n\x09 * @param LodIndex - The LOD we will apply the reduction settings.\n\x09 * @param ReductionOptions - The reduction settings we want to apply to the LOD.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Set the LOD reduction for the specified LOD index.\n@param StaticMesh - Mesh to process.\n@param LodIndex - The LOD we will apply the reduction settings.\n@param ReductionOptions - The reduction settings we want to apply to the LOD." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "SetLodReductionSettings", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventSetLodReductionSettings_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics
	{
		struct EditorStaticMeshLibrary_eventSetLods_Parms
		{
			UStaticMesh* StaticMesh;
			FEditorScriptingMeshReductionOptions ReductionOptions;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReductionOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReductionOptions;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLods_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::NewProp_ReductionOptions_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::NewProp_ReductionOptions = { "ReductionOptions", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLods_Parms, ReductionOptions), Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::NewProp_ReductionOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::NewProp_ReductionOptions_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLods_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::NewProp_ReductionOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Same as SetLodsWithNotification but changes are applied.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Same as SetLodsWithNotification but changes are applied." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "SetLods", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventSetLods_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics
	{
		struct EditorStaticMeshLibrary_eventSetLodsWithNotification_Parms
		{
			UStaticMesh* StaticMesh;
			FEditorScriptingMeshReductionOptions ReductionOptions;
			bool bApplyChanges;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReductionOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReductionOptions;
		static void NewProp_bApplyChanges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyChanges;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodsWithNotification_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_ReductionOptions_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_ReductionOptions = { "ReductionOptions", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodsWithNotification_Parms, ReductionOptions), Z_Construct_UScriptStruct_FEditorScriptingMeshReductionOptions, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_ReductionOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_ReductionOptions_MetaData)) };
	void Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_bApplyChanges_SetBit(void* Obj)
	{
		((EditorStaticMeshLibrary_eventSetLodsWithNotification_Parms*)Obj)->bApplyChanges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_bApplyChanges = { "bApplyChanges", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorStaticMeshLibrary_eventSetLodsWithNotification_Parms), &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_bApplyChanges_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorStaticMeshLibrary_eventSetLodsWithNotification_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_ReductionOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_bApplyChanges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Remove then add LODs on a static mesh.\n\x09 * The static mesh must have at least LOD 0.\n\x09 * The LOD 0 of the static mesh is kept after removal.\n\x09 * The build settings of LOD 0 will be applied to all subsequent LODs.\n\x09 * @param\x09StaticMesh\x09\x09\x09\x09Mesh to process.\n\x09 * @param\x09ReductionOptions\x09\x09Options on how to generate LODs on the mesh.\n\x09 * @param\x09""bApplyChanges\x09\x09\x09\x09Indicates if change must be notified.\n\x09 * @return the number of LODs generated on the input mesh.\n\x09 * An negative value indicates that the reduction could not be performed. See log for explanation.\n\x09 * No action will be performed if ReductionOptions.ReductionSettings is empty\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Remove then add LODs on a static mesh.\nThe static mesh must have at least LOD 0.\nThe LOD 0 of the static mesh is kept after removal.\nThe build settings of LOD 0 will be applied to all subsequent LODs.\n@param       StaticMesh                              Mesh to process.\n@param       ReductionOptions                Options on how to generate LODs on the mesh.\n@param       bApplyChanges                           Indicates if change must be notified.\n@return the number of LODs generated on the input mesh.\nAn negative value indicates that the reduction could not be performed. See log for explanation.\nNo action will be performed if ReductionOptions.ReductionSettings is empty" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorStaticMeshLibrary, nullptr, "SetLodsWithNotification", nullptr, nullptr, sizeof(EditorStaticMeshLibrary_eventSetLodsWithNotification_Parms), Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditorStaticMeshLibrary_NoRegister()
	{
		return UEditorStaticMeshLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UEditorStaticMeshLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditorStaticMeshLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_EditorScriptingUtilities,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditorStaticMeshLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisions, "AddSimpleCollisions" }, // 902838314
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_AddSimpleCollisionsWithNotification, "AddSimpleCollisionsWithNotification" }, // 4089011027
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_AddUVChannel, "AddUVChannel" }, // 1309735356
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisions, "BulkSetConvexDecompositionCollisions" }, // 307097168
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_BulkSetConvexDecompositionCollisionsWithNotification, "BulkSetConvexDecompositionCollisionsWithNotification" }, // 2858752779
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCastShadow, "EnableSectionCastShadow" }, // 3276049059
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_EnableSectionCollision, "EnableSectionCollision" }, // 1921120550
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateBoxUVChannel, "GenerateBoxUVChannel" }, // 4108627033
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GenerateCylindricalUVChannel, "GenerateCylindricalUVChannel" }, // 3697796059
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GeneratePlanarUVChannel, "GeneratePlanarUVChannel" }, // 1229264236
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetCollisionComplexity, "GetCollisionComplexity" }, // 1256757235
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetConvexCollisionCount, "GetConvexCollisionCount" }, // 1371817445
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodBuildSettings, "GetLodBuildSettings" }, // 4156179749
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodCount, "GetLodCount" }, // 2426835983
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLODMaterialSlot, "GetLODMaterialSlot" }, // 606659112
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodReductionSettings, "GetLodReductionSettings" }, // 3119108861
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetLodScreenSizes, "GetLodScreenSizes" }, // 3209021851
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberMaterials, "GetNumberMaterials" }, // 1669769915
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumberVerts, "GetNumberVerts" }, // 716461570
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetNumUVChannels, "GetNumUVChannels" }, // 3245351714
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_GetSimpleCollisionCount, "GetSimpleCollisionCount" }, // 3704338316
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_HasInstanceVertexColors, "HasInstanceVertexColors" }, // 789736054
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_HasVertexColors, "HasVertexColors" }, // 3322288804
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_ImportLOD, "ImportLOD" }, // 2471174888
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_InsertUVChannel, "InsertUVChannel" }, // 146270511
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_IsSectionCollisionEnabled, "IsSectionCollisionEnabled" }, // 3521426795
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_ReimportAllCustomLODs, "ReimportAllCustomLODs" }, // 2895297405
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisions, "RemoveCollisions" }, // 2543377805
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveCollisionsWithNotification, "RemoveCollisionsWithNotification" }, // 128371796
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveLods, "RemoveLods" }, // 280387240
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_RemoveUVChannel, "RemoveUVChannel" }, // 3144550654
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetAllowCPUAccess, "SetAllowCPUAccess" }, // 1578457278
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisions, "SetConvexDecompositionCollisions" }, // 2674114262
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetConvexDecompositionCollisionsWithNotification, "SetConvexDecompositionCollisionsWithNotification" }, // 3948167858
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetGenerateLightmapUVs, "SetGenerateLightmapUVs" }, // 984657108
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodBuildSettings, "SetLodBuildSettings" }, // 3703568253
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodFromStaticMesh, "SetLodFromStaticMesh" }, // 3246540543
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLODMaterialSlot, "SetLODMaterialSlot" }, // 1413250237
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodReductionSettings, "SetLodReductionSettings" }, // 67584438
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLods, "SetLods" }, // 3683892994
		{ &Z_Construct_UFunction_UEditorStaticMeshLibrary_SetLodsWithNotification, "SetLodsWithNotification" }, // 1743479702
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorStaticMeshLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Utility class to altering and analyzing a StaticMesh and use the common functionalities of the Mesh Editor.\n * The editor should not be in play in editor mode.\n */" },
		{ "IncludePath", "EditorStaticMeshLibrary.h" },
		{ "ModuleRelativePath", "Public/EditorStaticMeshLibrary.h" },
		{ "ToolTip", "Utility class to altering and analyzing a StaticMesh and use the common functionalities of the Mesh Editor.\nThe editor should not be in play in editor mode." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditorStaticMeshLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditorStaticMeshLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditorStaticMeshLibrary_Statics::ClassParams = {
		&UEditorStaticMeshLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEditorStaticMeshLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorStaticMeshLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditorStaticMeshLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditorStaticMeshLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditorStaticMeshLibrary, 3459306990);
	template<> EDITORSCRIPTINGUTILITIES_API UClass* StaticClass<UEditorStaticMeshLibrary>()
	{
		return UEditorStaticMeshLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditorStaticMeshLibrary(Z_Construct_UClass_UEditorStaticMeshLibrary, &UEditorStaticMeshLibrary::StaticClass, TEXT("/Script/EditorScriptingUtilities"), TEXT("UEditorStaticMeshLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditorStaticMeshLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
