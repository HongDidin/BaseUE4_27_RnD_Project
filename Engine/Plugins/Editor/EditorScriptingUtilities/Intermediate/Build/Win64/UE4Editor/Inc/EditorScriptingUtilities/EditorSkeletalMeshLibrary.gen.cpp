// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EditorScriptingUtilities/Public/EditorSkeletalMeshLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditorSkeletalMeshLibrary() {}
// Cross Module References
	EDITORSCRIPTINGUTILITIES_API UClass* Z_Construct_UClass_UEditorSkeletalMeshLibrary_NoRegister();
	EDITORSCRIPTINGUTILITIES_API UClass* Z_Construct_UClass_UEditorSkeletalMeshLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_EditorScriptingUtilities();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPhysicsAsset_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FSkeletalMeshBuildSettings();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execCreatePhysicsAsset)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UPhysicsAsset**)Z_Param__Result=UEditorSkeletalMeshLibrary::CreatePhysicsAsset(Z_Param_SkeletalMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execStripLODGeometry)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_OBJECT(UTexture2D,Z_Param_TextureMask);
		P_GET_PROPERTY(FFloatProperty,Z_Param_Threshold);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorSkeletalMeshLibrary::StripLODGeometry(Z_Param_SkeletalMesh,Z_Param_LODIndex,Z_Param_TextureMask,Z_Param_Threshold);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execRemoveLODs)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_GET_TARRAY(int32,Z_Param_ToRemoveLODs);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorSkeletalMeshLibrary::RemoveLODs(Z_Param_SkeletalMesh,Z_Param_ToRemoveLODs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execSetLodBuildSettings)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LodIndex);
		P_GET_STRUCT_REF(FSkeletalMeshBuildSettings,Z_Param_Out_BuildOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorSkeletalMeshLibrary::SetLodBuildSettings(Z_Param_SkeletalMesh,Z_Param_LodIndex,Z_Param_Out_BuildOptions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execGetLodBuildSettings)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LodIndex);
		P_GET_STRUCT_REF(FSkeletalMeshBuildSettings,Z_Param_Out_OutBuildOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorSkeletalMeshLibrary::GetLodBuildSettings(Z_Param_SkeletalMesh,Z_Param_LodIndex,Z_Param_Out_OutBuildOptions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execReimportAllCustomLODs)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorSkeletalMeshLibrary::ReimportAllCustomLODs(Z_Param_SkeletalMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execImportLOD)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_BaseMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FStrProperty,Z_Param_SourceFilename);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorSkeletalMeshLibrary::ImportLOD(Z_Param_BaseMesh,Z_Param_LODIndex,Z_Param_SourceFilename);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execGetLODCount)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorSkeletalMeshLibrary::GetLODCount(Z_Param_SkeletalMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execRenameSocket)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_GET_PROPERTY(FNameProperty,Z_Param_OldName);
		P_GET_PROPERTY(FNameProperty,Z_Param_NewName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorSkeletalMeshLibrary::RenameSocket(Z_Param_SkeletalMesh,Z_Param_OldName,Z_Param_NewName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execGetLODMaterialSlot)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_SectionIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorSkeletalMeshLibrary::GetLODMaterialSlot(Z_Param_SkeletalMesh,Z_Param_LODIndex,Z_Param_SectionIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execGetNumSections)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorSkeletalMeshLibrary::GetNumSections(Z_Param_SkeletalMesh,Z_Param_LODIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execGetNumVerts)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UEditorSkeletalMeshLibrary::GetNumVerts(Z_Param_SkeletalMesh,Z_Param_LODIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorSkeletalMeshLibrary::execRegenerateLOD)
	{
		P_GET_OBJECT(USkeletalMesh,Z_Param_SkeletalMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_NewLODCount);
		P_GET_UBOOL(Z_Param_bRegenerateEvenIfImported);
		P_GET_UBOOL(Z_Param_bGenerateBaseLOD);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorSkeletalMeshLibrary::RegenerateLOD(Z_Param_SkeletalMesh,Z_Param_NewLODCount,Z_Param_bRegenerateEvenIfImported,Z_Param_bGenerateBaseLOD);
		P_NATIVE_END;
	}
	void UEditorSkeletalMeshLibrary::StaticRegisterNativesUEditorSkeletalMeshLibrary()
	{
		UClass* Class = UEditorSkeletalMeshLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreatePhysicsAsset", &UEditorSkeletalMeshLibrary::execCreatePhysicsAsset },
			{ "GetLodBuildSettings", &UEditorSkeletalMeshLibrary::execGetLodBuildSettings },
			{ "GetLODCount", &UEditorSkeletalMeshLibrary::execGetLODCount },
			{ "GetLODMaterialSlot", &UEditorSkeletalMeshLibrary::execGetLODMaterialSlot },
			{ "GetNumSections", &UEditorSkeletalMeshLibrary::execGetNumSections },
			{ "GetNumVerts", &UEditorSkeletalMeshLibrary::execGetNumVerts },
			{ "ImportLOD", &UEditorSkeletalMeshLibrary::execImportLOD },
			{ "RegenerateLOD", &UEditorSkeletalMeshLibrary::execRegenerateLOD },
			{ "ReimportAllCustomLODs", &UEditorSkeletalMeshLibrary::execReimportAllCustomLODs },
			{ "RemoveLODs", &UEditorSkeletalMeshLibrary::execRemoveLODs },
			{ "RenameSocket", &UEditorSkeletalMeshLibrary::execRenameSocket },
			{ "SetLodBuildSettings", &UEditorSkeletalMeshLibrary::execSetLodBuildSettings },
			{ "StripLODGeometry", &UEditorSkeletalMeshLibrary::execStripLODGeometry },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics
	{
		struct EditorSkeletalMeshLibrary_eventCreatePhysicsAsset_Parms
		{
			USkeletalMesh* SkeletalMesh;
			UPhysicsAsset* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventCreatePhysicsAsset_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventCreatePhysicsAsset_Parms, ReturnValue), Z_Construct_UClass_UPhysicsAsset_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/**\n\x09 * This function creates a PhysicsAsset for the given SkeletalMesh with the same settings as if it were created through FBX import\n\x09 *\n\x09 * @Param SkeletalMesh: The SkeletalMesh we want to create the PhysicsAsset for\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ToolTip", "This function creates a PhysicsAsset for the given SkeletalMesh with the same settings as if it were created through FBX import\n\n@Param SkeletalMesh: The SkeletalMesh we want to create the PhysicsAsset for" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "CreatePhysicsAsset", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventCreatePhysicsAsset_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics
	{
		struct EditorSkeletalMeshLibrary_eventGetLodBuildSettings_Parms
		{
			const USkeletalMesh* SkeletalMesh;
			int32 LodIndex;
			FSkeletalMeshBuildSettings OutBuildOptions;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LodIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LodIndex;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutBuildOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_SkeletalMesh_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetLodBuildSettings_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_SkeletalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_SkeletalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_LodIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_LodIndex = { "LodIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetLodBuildSettings_Parms, LodIndex), METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_LodIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_LodIndex_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_OutBuildOptions = { "OutBuildOptions", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetLodBuildSettings_Parms, OutBuildOptions), Z_Construct_UScriptStruct_FSkeletalMeshBuildSettings, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_LodIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::NewProp_OutBuildOptions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/**\n\x09 * Copy the build options with the specified LOD build settings.\n\x09 * @param SkeletalMesh - Mesh to process.\n\x09 * @param LodIndex - The LOD we get the reduction settings.\n\x09 * @param OutBuildOptions - The build settings where we copy the build options.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ToolTip", "Copy the build options with the specified LOD build settings.\n@param SkeletalMesh - Mesh to process.\n@param LodIndex - The LOD we get the reduction settings.\n@param OutBuildOptions - The build settings where we copy the build options." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "GetLodBuildSettings", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventGetLodBuildSettings_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics
	{
		struct EditorSkeletalMeshLibrary_eventGetLODCount_Parms
		{
			USkeletalMesh* SkeletalMesh;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetLODCount_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetLODCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/**\n\x09 * Retrieve the number of LOD contain in the specified skeletal mesh.\n\x09 *\n\x09 * @param SkeletalMesh: The static mesh we import or re-import a LOD.\n\x09 *\n\x09 * @return The LOD number.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ToolTip", "Retrieve the number of LOD contain in the specified skeletal mesh.\n\n@param SkeletalMesh: The static mesh we import or re-import a LOD.\n\n@return The LOD number." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "GetLODCount", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventGetLODCount_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics
	{
		struct EditorSkeletalMeshLibrary_eventGetLODMaterialSlot_Parms
		{
			USkeletalMesh* SkeletalMesh;
			int32 LODIndex;
			int32 SectionIndex;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SectionIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetLODMaterialSlot_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetLODMaterialSlot_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::NewProp_SectionIndex = { "SectionIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetLODMaterialSlot_Parms, SectionIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetLODMaterialSlot_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::NewProp_SectionIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "Comment", "/**\n\x09 * Gets the material slot used for a specific LOD section.\n\x09 * @param\x09SkeletalMesh\x09\x09SkeletalMesh to get the material index from.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09SectionIndex\x09\x09Index of the StaticMesh Section.\n\x09 * @return  MaterialSlotIndex\x09Index of the material slot used by the section or INDEX_NONE in case of error.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ToolTip", "Gets the material slot used for a specific LOD section.\n@param       SkeletalMesh            SkeletalMesh to get the material index from.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       SectionIndex            Index of the StaticMesh Section.\n@return  MaterialSlotIndex   Index of the material slot used by the section or INDEX_NONE in case of error." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "GetLODMaterialSlot", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventGetLODMaterialSlot_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics
	{
		struct EditorSkeletalMeshLibrary_eventGetNumSections_Parms
		{
			USkeletalMesh* SkeletalMesh;
			int32 LODIndex;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetNumSections_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetNumSections_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetNumSections_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/** Get number of sections for a LOD of a Skeletal Mesh\n\x09 *\n\x09 * @param SkeletalMesh\x09\x09Mesh to get number of vertices from.\n\x09 * @param LODIndex\x09\x09\x09Index of the mesh LOD.\n\x09 * @return Number of sections. Returns INDEX_NONE if invalid mesh or LOD index.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ToolTip", "Get number of sections for a LOD of a Skeletal Mesh\n\n@param SkeletalMesh          Mesh to get number of vertices from.\n@param LODIndex                      Index of the mesh LOD.\n@return Number of sections. Returns INDEX_NONE if invalid mesh or LOD index." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "GetNumSections", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventGetNumSections_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics
	{
		struct EditorSkeletalMeshLibrary_eventGetNumVerts_Parms
		{
			USkeletalMesh* SkeletalMesh;
			int32 LODIndex;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetNumVerts_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetNumVerts_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventGetNumVerts_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/** Get number of mesh vertices for an LOD of a Skeletal Mesh\n\x09 *\n\x09 * @param SkeletalMesh\x09\x09Mesh to get number of vertices from.\n\x09 * @param LODIndex\x09\x09\x09Index of the mesh LOD.\n\x09 * @return Number of vertices. Returns 0 if invalid mesh or LOD index.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ToolTip", "Get number of mesh vertices for an LOD of a Skeletal Mesh\n\n@param SkeletalMesh          Mesh to get number of vertices from.\n@param LODIndex                      Index of the mesh LOD.\n@return Number of vertices. Returns 0 if invalid mesh or LOD index." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "GetNumVerts", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventGetNumVerts_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics
	{
		struct EditorSkeletalMeshLibrary_eventImportLOD_Parms
		{
			USkeletalMesh* BaseMesh;
			int32 LODIndex;
			FString SourceFilename;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BaseMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LODIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceFilename_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SourceFilename;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_BaseMesh = { "BaseMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventImportLOD_Parms, BaseMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_LODIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventImportLOD_Parms, LODIndex), METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_LODIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_LODIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_SourceFilename_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_SourceFilename = { "SourceFilename", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventImportLOD_Parms, SourceFilename), METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_SourceFilename_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_SourceFilename_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventImportLOD_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_BaseMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_SourceFilename,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/**\n\x09 * Import or re-import a LOD into the specified base mesh. If the LOD do not exist it will import it and add it to the base static mesh. If the LOD already exist it will re-import the specified LOD.\n\x09 *\n\x09 * @param BaseSkeletalMesh: The static mesh we import or re-import a LOD.\n\x09 * @param LODIndex: The index of the LOD to import or re-import. Valid value should be between 0 and the base skeletal mesh LOD number. Invalid value will return INDEX_NONE\n\x09 * @param SourceFilename: The fbx source filename. If we are re-importing an existing LOD, it can be empty in this case it will use the last import file. Otherwise it must be an existing fbx file.\n\x09 *\n\x09 * @return The index of the LOD that was imported or re-imported. Will return INDEX_NONE if anything goes bad.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ToolTip", "Import or re-import a LOD into the specified base mesh. If the LOD do not exist it will import it and add it to the base static mesh. If the LOD already exist it will re-import the specified LOD.\n\n@param BaseSkeletalMesh: The static mesh we import or re-import a LOD.\n@param LODIndex: The index of the LOD to import or re-import. Valid value should be between 0 and the base skeletal mesh LOD number. Invalid value will return INDEX_NONE\n@param SourceFilename: The fbx source filename. If we are re-importing an existing LOD, it can be empty in this case it will use the last import file. Otherwise it must be an existing fbx file.\n\n@return The index of the LOD that was imported or re-imported. Will return INDEX_NONE if anything goes bad." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "ImportLOD", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventImportLOD_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics
	{
		struct EditorSkeletalMeshLibrary_eventRegenerateLOD_Parms
		{
			USkeletalMesh* SkeletalMesh;
			int32 NewLODCount;
			bool bRegenerateEvenIfImported;
			bool bGenerateBaseLOD;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NewLODCount;
		static void NewProp_bRegenerateEvenIfImported_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRegenerateEvenIfImported;
		static void NewProp_bGenerateBaseLOD_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGenerateBaseLOD;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventRegenerateLOD_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_NewLODCount = { "NewLODCount", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventRegenerateLOD_Parms, NewLODCount), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_bRegenerateEvenIfImported_SetBit(void* Obj)
	{
		((EditorSkeletalMeshLibrary_eventRegenerateLOD_Parms*)Obj)->bRegenerateEvenIfImported = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_bRegenerateEvenIfImported = { "bRegenerateEvenIfImported", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorSkeletalMeshLibrary_eventRegenerateLOD_Parms), &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_bRegenerateEvenIfImported_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_bGenerateBaseLOD_SetBit(void* Obj)
	{
		((EditorSkeletalMeshLibrary_eventRegenerateLOD_Parms*)Obj)->bGenerateBaseLOD = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_bGenerateBaseLOD = { "bGenerateBaseLOD", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorSkeletalMeshLibrary_eventRegenerateLOD_Parms), &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_bGenerateBaseLOD_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorSkeletalMeshLibrary_eventRegenerateLOD_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorSkeletalMeshLibrary_eventRegenerateLOD_Parms), &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_NewLODCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_bRegenerateEvenIfImported,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_bGenerateBaseLOD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/** Regenerate LODs of the mesh\n\x09 *\n\x09 * @param SkeletalMesh\x09The mesh that will regenerate LOD\n\x09 * @param NewLODCount\x09Set valid value (>0) if you want to change LOD count.\n\x09 *\x09\x09\x09\x09\x09\x09Otherwise, it will use the current LOD and regenerate\n\x09 * @param bRegenerateEvenIfImported\x09If this is true, it only regenerate even if this LOD was imported before\n\x09 *\x09\x09\x09\x09\x09\x09\x09\x09\x09If false, it will regenerate for only previously auto generated ones\n\x09 * @param bGenerateBaseLOD If this is true and there is some reduction data, the base LOD will be reduce according to the settings\n\x09 * @return\x09true if succeed. If mesh reduction is not available this will return false.\n\x09 */" },
		{ "CPP_Default_bGenerateBaseLOD", "false" },
		{ "CPP_Default_bRegenerateEvenIfImported", "false" },
		{ "CPP_Default_NewLODCount", "0" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "Regenerate LODs of the mesh\n\n@param SkeletalMesh  The mesh that will regenerate LOD\n@param NewLODCount   Set valid value (>0) if you want to change LOD count.\n                                            Otherwise, it will use the current LOD and regenerate\n@param bRegenerateEvenIfImported     If this is true, it only regenerate even if this LOD was imported before\n                                                                    If false, it will regenerate for only previously auto generated ones\n@param bGenerateBaseLOD If this is true and there is some reduction data, the base LOD will be reduce according to the settings\n@return      true if succeed. If mesh reduction is not available this will return false." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "RegenerateLOD", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventRegenerateLOD_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics
	{
		struct EditorSkeletalMeshLibrary_eventReimportAllCustomLODs_Parms
		{
			USkeletalMesh* SkeletalMesh;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventReimportAllCustomLODs_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorSkeletalMeshLibrary_eventReimportAllCustomLODs_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorSkeletalMeshLibrary_eventReimportAllCustomLODs_Parms), &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/**\n\x09 * Re-import the specified skeletal mesh and all the custom LODs.\n\x09 *\n\x09 * @param SkeletalMesh: is the skeletal mesh we import or re-import a LOD.\n\x09 *\n\x09 * @return true if re-import works, false otherwise see log for explanation.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ToolTip", "Re-import the specified skeletal mesh and all the custom LODs.\n\n@param SkeletalMesh: is the skeletal mesh we import or re-import a LOD.\n\n@return true if re-import works, false otherwise see log for explanation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "ReimportAllCustomLODs", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventReimportAllCustomLODs_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics
	{
		struct EditorSkeletalMeshLibrary_eventRemoveLODs_Parms
		{
			USkeletalMesh* SkeletalMesh;
			TArray<int32> ToRemoveLODs;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ToRemoveLODs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ToRemoveLODs;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventRemoveLODs_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::NewProp_ToRemoveLODs_Inner = { "ToRemoveLODs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::NewProp_ToRemoveLODs = { "ToRemoveLODs", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventRemoveLODs_Parms, ToRemoveLODs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorSkeletalMeshLibrary_eventRemoveLODs_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorSkeletalMeshLibrary_eventRemoveLODs_Parms), &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::NewProp_ToRemoveLODs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::NewProp_ToRemoveLODs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/** Remove all the specified LODs. This function will remove all the valid LODs in the list.\n\x09 * Valid LOD is any LOD greater then 0 that exist in the skeletalmesh. We cannot remove the base LOD 0.\n\x09 *\n\x09 * @param SkeletalMesh\x09The mesh inside which we are renaming a socket\n\x09 * @param ToRemoveLODs\x09The LODs we need to remove\n\x09 * @return true if the successfully remove all the LODs. False otherwise, but evedn if it return false it\n\x09 * will have removed all valid LODs.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "Remove all the specified LODs. This function will remove all the valid LODs in the list.\nValid LOD is any LOD greater then 0 that exist in the skeletalmesh. We cannot remove the base LOD 0.\n\n@param SkeletalMesh  The mesh inside which we are renaming a socket\n@param ToRemoveLODs  The LODs we need to remove\n@return true if the successfully remove all the LODs. False otherwise, but evedn if it return false it\nwill have removed all valid LODs." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "RemoveLODs", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventRemoveLODs_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics
	{
		struct EditorSkeletalMeshLibrary_eventRenameSocket_Parms
		{
			USkeletalMesh* SkeletalMesh;
			FName OldName;
			FName NewName;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_OldName;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewName;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventRenameSocket_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::NewProp_OldName = { "OldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventRenameSocket_Parms, OldName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::NewProp_NewName = { "NewName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventRenameSocket_Parms, NewName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorSkeletalMeshLibrary_eventRenameSocket_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorSkeletalMeshLibrary_eventRenameSocket_Parms), &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::NewProp_OldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::NewProp_NewName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/** Rename a socket within a skeleton\n\x09 * @param SkeletalMesh\x09The mesh inside which we are renaming a socket\n\x09 * @param OldName       The old name of the socket\n\x09 * @param NewName\x09\x09The new name of the socket\n\x09 * @return true if the renaming succeeded.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "Rename a socket within a skeleton\n@param SkeletalMesh  The mesh inside which we are renaming a socket\n@param OldName       The old name of the socket\n@param NewName               The new name of the socket\n@return true if the renaming succeeded." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "RenameSocket", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventRenameSocket_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics
	{
		struct EditorSkeletalMeshLibrary_eventSetLodBuildSettings_Parms
		{
			USkeletalMesh* SkeletalMesh;
			int32 LodIndex;
			FSkeletalMeshBuildSettings BuildOptions;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LodIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LodIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuildOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BuildOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventSetLodBuildSettings_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_LodIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_LodIndex = { "LodIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventSetLodBuildSettings_Parms, LodIndex), METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_LodIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_LodIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_BuildOptions_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_BuildOptions = { "BuildOptions", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventSetLodBuildSettings_Parms, BuildOptions), Z_Construct_UScriptStruct_FSkeletalMeshBuildSettings, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_BuildOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_BuildOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_LodIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::NewProp_BuildOptions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/**\n\x09 * Set the LOD build options for the specified LOD index.\n\x09 * @param SkeletalMesh - Mesh to process.\n\x09 * @param LodIndex - The LOD we will apply the build settings.\n\x09 * @param BuildOptions - The build settings we want to apply to the LOD.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ToolTip", "Set the LOD build options for the specified LOD index.\n@param SkeletalMesh - Mesh to process.\n@param LodIndex - The LOD we will apply the build settings.\n@param BuildOptions - The build settings we want to apply to the LOD." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "SetLodBuildSettings", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventSetLodBuildSettings_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics
	{
		struct EditorSkeletalMeshLibrary_eventStripLODGeometry_Parms
		{
			USkeletalMesh* SkeletalMesh;
			int32 LODIndex;
			UTexture2D* TextureMask;
			float Threshold;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LODIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TextureMask;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Threshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Threshold;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventStripLODGeometry_Parms, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_LODIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventStripLODGeometry_Parms, LODIndex), METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_LODIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_LODIndex_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_TextureMask = { "TextureMask", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventStripLODGeometry_Parms, TextureMask), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_Threshold_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_Threshold = { "Threshold", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorSkeletalMeshLibrary_eventStripLODGeometry_Parms, Threshold), METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_Threshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_Threshold_MetaData)) };
	void Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorSkeletalMeshLibrary_eventStripLODGeometry_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorSkeletalMeshLibrary_eventStripLODGeometry_Parms), &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_TextureMask,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_Threshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | SkeletalMesh" },
		{ "Comment", "/**\n\x09 * This function will strip all triangle in the specified LOD that don't have any UV area pointing on a black pixel in the TextureMask.\n\x09 * We use the UVChannel 0 to find the pixels in the texture.\n\x09 *\n\x09 * @Param SkeletalMesh: The skeletalmesh we want to optimize\n\x09 * @Param LODIndex: The LOD we want to optimize\n\x09 * @Param TextureMask: The texture containing the stripping mask. non black pixel strip triangle, black pixel keep them.\n\x09 * @Param Threshold: The threshold we want when comparing the texture value with zero.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "This function will strip all triangle in the specified LOD that don't have any UV area pointing on a black pixel in the TextureMask.\nWe use the UVChannel 0 to find the pixels in the texture.\n\n@Param SkeletalMesh: The skeletalmesh we want to optimize\n@Param LODIndex: The LOD we want to optimize\n@Param TextureMask: The texture containing the stripping mask. non black pixel strip triangle, black pixel keep them.\n@Param Threshold: The threshold we want when comparing the texture value with zero." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorSkeletalMeshLibrary, nullptr, "StripLODGeometry", nullptr, nullptr, sizeof(EditorSkeletalMeshLibrary_eventStripLODGeometry_Parms), Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditorSkeletalMeshLibrary_NoRegister()
	{
		return UEditorSkeletalMeshLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UEditorSkeletalMeshLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditorSkeletalMeshLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_EditorScriptingUtilities,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditorSkeletalMeshLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_CreatePhysicsAsset, "CreatePhysicsAsset" }, // 2084611595
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLodBuildSettings, "GetLodBuildSettings" }, // 2434286638
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODCount, "GetLODCount" }, // 2790489921
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetLODMaterialSlot, "GetLODMaterialSlot" }, // 3096319680
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumSections, "GetNumSections" }, // 3245505676
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_GetNumVerts, "GetNumVerts" }, // 4284128714
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ImportLOD, "ImportLOD" }, // 2942517747
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RegenerateLOD, "RegenerateLOD" }, // 1825662256
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_ReimportAllCustomLODs, "ReimportAllCustomLODs" }, // 118887130
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RemoveLODs, "RemoveLODs" }, // 840185014
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_RenameSocket, "RenameSocket" }, // 1443583550
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_SetLodBuildSettings, "SetLodBuildSettings" }, // 849987662
		{ &Z_Construct_UFunction_UEditorSkeletalMeshLibrary_StripLODGeometry, "StripLODGeometry" }, // 3506074525
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorSkeletalMeshLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Utility class to altering and analyzing a SkeletalMesh and use the common functionalities of the SkeletalMesh Editor.\n* The editor should not be in play in editor mode.\n */" },
		{ "IncludePath", "EditorSkeletalMeshLibrary.h" },
		{ "ModuleRelativePath", "Public/EditorSkeletalMeshLibrary.h" },
		{ "ToolTip", "Utility class to altering and analyzing a SkeletalMesh and use the common functionalities of the SkeletalMesh Editor.\nThe editor should not be in play in editor mode." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditorSkeletalMeshLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditorSkeletalMeshLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditorSkeletalMeshLibrary_Statics::ClassParams = {
		&UEditorSkeletalMeshLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEditorSkeletalMeshLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorSkeletalMeshLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditorSkeletalMeshLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditorSkeletalMeshLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditorSkeletalMeshLibrary, 466232038);
	template<> EDITORSCRIPTINGUTILITIES_API UClass* StaticClass<UEditorSkeletalMeshLibrary>()
	{
		return UEditorSkeletalMeshLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditorSkeletalMeshLibrary(Z_Construct_UClass_UEditorSkeletalMeshLibrary, &UEditorSkeletalMeshLibrary::StaticClass, TEXT("/Script/EditorScriptingUtilities"), TEXT("UEditorSkeletalMeshLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditorSkeletalMeshLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
