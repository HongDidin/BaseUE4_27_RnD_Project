// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EditorScriptingUtilities/Public/EditorLevelLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditorLevelLibrary() {}
// Cross Module References
	EDITORSCRIPTINGUTILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions();
	UPackage* Z_Construct_UPackage__Script_EditorScriptingUtilities();
	EDITORSCRIPTINGUTILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FMeshProxySettings();
	EDITORSCRIPTINGUTILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FMeshMergingSettings();
	EDITORSCRIPTINGUTILITIES_API UClass* Z_Construct_UClass_UEditorLevelLibrary_NoRegister();
	EDITORSCRIPTINGUTILITIES_API UClass* Z_Construct_UClass_UEditorLevelLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AStaticMeshActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FEditorScriptingCreateProxyMeshActorOptions>() == std::is_polymorphic<FEditorScriptingJoinStaticMeshActorsOptions>(), "USTRUCT FEditorScriptingCreateProxyMeshActorOptions cannot be polymorphic unless super FEditorScriptingJoinStaticMeshActorsOptions is polymorphic");

class UScriptStruct* FEditorScriptingCreateProxyMeshActorOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern EDITORSCRIPTINGUTILITIES_API uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions, Z_Construct_UPackage__Script_EditorScriptingUtilities(), TEXT("EditorScriptingCreateProxyMeshActorOptions"), sizeof(FEditorScriptingCreateProxyMeshActorOptions), Get_Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Hash());
	}
	return Singleton;
}
template<> EDITORSCRIPTINGUTILITIES_API UScriptStruct* StaticStruct<FEditorScriptingCreateProxyMeshActorOptions>()
{
	return FEditorScriptingCreateProxyMeshActorOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions(FEditorScriptingCreateProxyMeshActorOptions::StaticStruct, TEXT("/Script/EditorScriptingUtilities"), TEXT("EditorScriptingCreateProxyMeshActorOptions"), false, nullptr, nullptr);
static struct FScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingCreateProxyMeshActorOptions
{
	FScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingCreateProxyMeshActorOptions()
	{
		UScriptStruct::DeferCppStructOps<FEditorScriptingCreateProxyMeshActorOptions>(FName(TEXT("EditorScriptingCreateProxyMeshActorOptions")));
	}
} ScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingCreateProxyMeshActorOptions;
	struct Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSpawnMergedActor_MetaData[];
#endif
		static void NewProp_bSpawnMergedActor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSpawnMergedActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasePackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BasePackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshProxySettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeshProxySettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEditorScriptingCreateProxyMeshActorOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_bSpawnMergedActor_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// Spawn the new merged actors\n" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Spawn the new merged actors" },
	};
#endif
	void Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_bSpawnMergedActor_SetBit(void* Obj)
	{
		((FEditorScriptingCreateProxyMeshActorOptions*)Obj)->bSpawnMergedActor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_bSpawnMergedActor = { "bSpawnMergedActor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FEditorScriptingCreateProxyMeshActorOptions), &Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_bSpawnMergedActor_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_bSpawnMergedActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_bSpawnMergedActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_BasePackageName_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// The package path you want to save to. ie: /Game/MyFolder\n" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "The package path you want to save to. ie: /Game/MyFolder" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_BasePackageName = { "BasePackageName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEditorScriptingCreateProxyMeshActorOptions, BasePackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_BasePackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_BasePackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_MeshProxySettings_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_MeshProxySettings = { "MeshProxySettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEditorScriptingCreateProxyMeshActorOptions, MeshProxySettings), Z_Construct_UScriptStruct_FMeshProxySettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_MeshProxySettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_MeshProxySettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_bSpawnMergedActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_BasePackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::NewProp_MeshProxySettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EditorScriptingUtilities,
		Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions,
		&NewStructOps,
		"EditorScriptingCreateProxyMeshActorOptions",
		sizeof(FEditorScriptingCreateProxyMeshActorOptions),
		alignof(FEditorScriptingCreateProxyMeshActorOptions),
		Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EditorScriptingUtilities();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EditorScriptingCreateProxyMeshActorOptions"), sizeof(FEditorScriptingCreateProxyMeshActorOptions), Get_Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions_Hash() { return 3681401646U; }

static_assert(std::is_polymorphic<FEditorScriptingMergeStaticMeshActorsOptions>() == std::is_polymorphic<FEditorScriptingJoinStaticMeshActorsOptions>(), "USTRUCT FEditorScriptingMergeStaticMeshActorsOptions cannot be polymorphic unless super FEditorScriptingJoinStaticMeshActorsOptions is polymorphic");

class UScriptStruct* FEditorScriptingMergeStaticMeshActorsOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern EDITORSCRIPTINGUTILITIES_API uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions, Z_Construct_UPackage__Script_EditorScriptingUtilities(), TEXT("EditorScriptingMergeStaticMeshActorsOptions"), sizeof(FEditorScriptingMergeStaticMeshActorsOptions), Get_Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Hash());
	}
	return Singleton;
}
template<> EDITORSCRIPTINGUTILITIES_API UScriptStruct* StaticStruct<FEditorScriptingMergeStaticMeshActorsOptions>()
{
	return FEditorScriptingMergeStaticMeshActorsOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions(FEditorScriptingMergeStaticMeshActorsOptions::StaticStruct, TEXT("/Script/EditorScriptingUtilities"), TEXT("EditorScriptingMergeStaticMeshActorsOptions"), false, nullptr, nullptr);
static struct FScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingMergeStaticMeshActorsOptions
{
	FScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingMergeStaticMeshActorsOptions()
	{
		UScriptStruct::DeferCppStructOps<FEditorScriptingMergeStaticMeshActorsOptions>(FName(TEXT("EditorScriptingMergeStaticMeshActorsOptions")));
	}
} ScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingMergeStaticMeshActorsOptions;
	struct Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSpawnMergedActor_MetaData[];
#endif
		static void NewProp_bSpawnMergedActor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSpawnMergedActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasePackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BasePackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshMergingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeshMergingSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEditorScriptingMergeStaticMeshActorsOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_bSpawnMergedActor_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// Spawn the new merged actors\n" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Spawn the new merged actors" },
	};
#endif
	void Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_bSpawnMergedActor_SetBit(void* Obj)
	{
		((FEditorScriptingMergeStaticMeshActorsOptions*)Obj)->bSpawnMergedActor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_bSpawnMergedActor = { "bSpawnMergedActor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FEditorScriptingMergeStaticMeshActorsOptions), &Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_bSpawnMergedActor_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_bSpawnMergedActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_bSpawnMergedActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_BasePackageName_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// The package path you want to save to. ie: /Game/MyFolder\n" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "The package path you want to save to. ie: /Game/MyFolder" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_BasePackageName = { "BasePackageName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEditorScriptingMergeStaticMeshActorsOptions, BasePackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_BasePackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_BasePackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_MeshMergingSettings_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_MeshMergingSettings = { "MeshMergingSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEditorScriptingMergeStaticMeshActorsOptions, MeshMergingSettings), Z_Construct_UScriptStruct_FMeshMergingSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_MeshMergingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_MeshMergingSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_bSpawnMergedActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_BasePackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::NewProp_MeshMergingSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EditorScriptingUtilities,
		Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions,
		&NewStructOps,
		"EditorScriptingMergeStaticMeshActorsOptions",
		sizeof(FEditorScriptingMergeStaticMeshActorsOptions),
		alignof(FEditorScriptingMergeStaticMeshActorsOptions),
		Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EditorScriptingUtilities();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EditorScriptingMergeStaticMeshActorsOptions"), sizeof(FEditorScriptingMergeStaticMeshActorsOptions), Get_Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions_Hash() { return 454652537U; }
class UScriptStruct* FEditorScriptingJoinStaticMeshActorsOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern EDITORSCRIPTINGUTILITIES_API uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions, Z_Construct_UPackage__Script_EditorScriptingUtilities(), TEXT("EditorScriptingJoinStaticMeshActorsOptions"), sizeof(FEditorScriptingJoinStaticMeshActorsOptions), Get_Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Hash());
	}
	return Singleton;
}
template<> EDITORSCRIPTINGUTILITIES_API UScriptStruct* StaticStruct<FEditorScriptingJoinStaticMeshActorsOptions>()
{
	return FEditorScriptingJoinStaticMeshActorsOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions(FEditorScriptingJoinStaticMeshActorsOptions::StaticStruct, TEXT("/Script/EditorScriptingUtilities"), TEXT("EditorScriptingJoinStaticMeshActorsOptions"), false, nullptr, nullptr);
static struct FScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingJoinStaticMeshActorsOptions
{
	FScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingJoinStaticMeshActorsOptions()
	{
		UScriptStruct::DeferCppStructOps<FEditorScriptingJoinStaticMeshActorsOptions>(FName(TEXT("EditorScriptingJoinStaticMeshActorsOptions")));
	}
} ScriptStruct_EditorScriptingUtilities_StaticRegisterNativesFEditorScriptingJoinStaticMeshActorsOptions;
	struct Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDestroySourceActors_MetaData[];
#endif
		static void NewProp_bDestroySourceActors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDestroySourceActors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewActorLabel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewActorLabel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRenameComponentsFromSource_MetaData[];
#endif
		static void NewProp_bRenameComponentsFromSource_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRenameComponentsFromSource;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEditorScriptingJoinStaticMeshActorsOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bDestroySourceActors_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// Destroy the provided Actors after the operation.\n" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Destroy the provided Actors after the operation." },
	};
#endif
	void Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bDestroySourceActors_SetBit(void* Obj)
	{
		((FEditorScriptingJoinStaticMeshActorsOptions*)Obj)->bDestroySourceActors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bDestroySourceActors = { "bDestroySourceActors", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FEditorScriptingJoinStaticMeshActorsOptions), &Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bDestroySourceActors_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bDestroySourceActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bDestroySourceActors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_NewActorLabel_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// Name of the new spawned Actor to replace the provided Actors.\n" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Name of the new spawned Actor to replace the provided Actors." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_NewActorLabel = { "NewActorLabel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEditorScriptingJoinStaticMeshActorsOptions, NewActorLabel), METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_NewActorLabel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_NewActorLabel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bRenameComponentsFromSource_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "// Rename StaticMeshComponents based on source Actor's name.\n" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Rename StaticMeshComponents based on source Actor's name." },
	};
#endif
	void Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bRenameComponentsFromSource_SetBit(void* Obj)
	{
		((FEditorScriptingJoinStaticMeshActorsOptions*)Obj)->bRenameComponentsFromSource = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bRenameComponentsFromSource = { "bRenameComponentsFromSource", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FEditorScriptingJoinStaticMeshActorsOptions), &Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bRenameComponentsFromSource_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bRenameComponentsFromSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bRenameComponentsFromSource_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bDestroySourceActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_NewActorLabel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::NewProp_bRenameComponentsFromSource,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EditorScriptingUtilities,
		nullptr,
		&NewStructOps,
		"EditorScriptingJoinStaticMeshActorsOptions",
		sizeof(FEditorScriptingJoinStaticMeshActorsOptions),
		alignof(FEditorScriptingJoinStaticMeshActorsOptions),
		Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EditorScriptingUtilities();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EditorScriptingJoinStaticMeshActorsOptions"), sizeof(FEditorScriptingJoinStaticMeshActorsOptions), Get_Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions_Hash() { return 3590726803U; }
	DEFINE_FUNCTION(UEditorLevelLibrary::execCreateProxyMeshActor)
	{
		P_GET_TARRAY_REF(AStaticMeshActor*,Z_Param_Out_ActorsToMerge);
		P_GET_STRUCT_REF(FEditorScriptingCreateProxyMeshActorOptions,Z_Param_Out_MergeOptions);
		P_GET_OBJECT_REF(AStaticMeshActor,Z_Param_Out_OutMergedActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorLevelLibrary::CreateProxyMeshActor(Z_Param_Out_ActorsToMerge,Z_Param_Out_MergeOptions,Z_Param_Out_OutMergedActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execMergeStaticMeshActors)
	{
		P_GET_TARRAY_REF(AStaticMeshActor*,Z_Param_Out_ActorsToMerge);
		P_GET_STRUCT_REF(FEditorScriptingMergeStaticMeshActorsOptions,Z_Param_Out_MergeOptions);
		P_GET_OBJECT_REF(AStaticMeshActor,Z_Param_Out_OutMergedActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorLevelLibrary::MergeStaticMeshActors(Z_Param_Out_ActorsToMerge,Z_Param_Out_MergeOptions,Z_Param_Out_OutMergedActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execJoinStaticMeshActors)
	{
		P_GET_TARRAY_REF(AStaticMeshActor*,Z_Param_Out_ActorsToJoin);
		P_GET_STRUCT_REF(FEditorScriptingJoinStaticMeshActorsOptions,Z_Param_Out_JoinOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=UEditorLevelLibrary::JoinStaticMeshActors(Z_Param_Out_ActorsToJoin,Z_Param_Out_JoinOptions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execConvertActors)
	{
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_Actors);
		P_GET_OBJECT(UClass,Z_Param_ActorClass);
		P_GET_PROPERTY(FStrProperty,Z_Param_StaticMeshPackagePath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<AActor*>*)Z_Param__Result=UEditorLevelLibrary::ConvertActors(Z_Param_Out_Actors,Z_Param_ActorClass,Z_Param_StaticMeshPackagePath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execReplaceMeshComponentsMeshesOnActors)
	{
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_Actors);
		P_GET_OBJECT(UStaticMesh,Z_Param_MeshToBeReplaced);
		P_GET_OBJECT(UStaticMesh,Z_Param_NewMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::ReplaceMeshComponentsMeshesOnActors(Z_Param_Out_Actors,Z_Param_MeshToBeReplaced,Z_Param_NewMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execReplaceMeshComponentsMeshes)
	{
		P_GET_TARRAY_REF(UStaticMeshComponent*,Z_Param_Out_MeshComponents);
		P_GET_OBJECT(UStaticMesh,Z_Param_MeshToBeReplaced);
		P_GET_OBJECT(UStaticMesh,Z_Param_NewMesh);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::ReplaceMeshComponentsMeshes(Z_Param_Out_MeshComponents,Z_Param_MeshToBeReplaced,Z_Param_NewMesh);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execReplaceMeshComponentsMaterialsOnActors)
	{
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_Actors);
		P_GET_OBJECT(UMaterialInterface,Z_Param_MaterialToBeReplaced);
		P_GET_OBJECT(UMaterialInterface,Z_Param_NewMaterial);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::ReplaceMeshComponentsMaterialsOnActors(Z_Param_Out_Actors,Z_Param_MaterialToBeReplaced,Z_Param_NewMaterial);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execReplaceMeshComponentsMaterials)
	{
		P_GET_TARRAY_REF(UMeshComponent*,Z_Param_Out_MeshComponents);
		P_GET_OBJECT(UMaterialInterface,Z_Param_MaterialToBeReplaced);
		P_GET_OBJECT(UMaterialInterface,Z_Param_NewMaterial);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::ReplaceMeshComponentsMaterials(Z_Param_Out_MeshComponents,Z_Param_MaterialToBeReplaced,Z_Param_NewMaterial);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execDestroyActor)
	{
		P_GET_OBJECT(AActor,Z_Param_ActorToDestroy);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorLevelLibrary::DestroyActor(Z_Param_ActorToDestroy);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execSpawnActorFromClass)
	{
		P_GET_OBJECT(UClass,Z_Param_ActorClass);
		P_GET_STRUCT(FVector,Z_Param_Location);
		P_GET_STRUCT(FRotator,Z_Param_Rotation);
		P_GET_UBOOL(Z_Param_bTransient);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=UEditorLevelLibrary::SpawnActorFromClass(Z_Param_ActorClass,Z_Param_Location,Z_Param_Rotation,Z_Param_bTransient);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execSpawnActorFromObject)
	{
		P_GET_OBJECT(UObject,Z_Param_ObjectToUse);
		P_GET_STRUCT(FVector,Z_Param_Location);
		P_GET_STRUCT(FRotator,Z_Param_Rotation);
		P_GET_UBOOL(Z_Param_bTransient);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=UEditorLevelLibrary::SpawnActorFromObject(Z_Param_ObjectToUse,Z_Param_Location,Z_Param_Rotation,Z_Param_bTransient);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execEditorSetGameView)
	{
		P_GET_UBOOL(Z_Param_bGameView);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::EditorSetGameView(Z_Param_bGameView);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execGetActorReference)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_PathToActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=UEditorLevelLibrary::GetActorReference(Z_Param_PathToActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execSetActorSelectionState)
	{
		P_GET_OBJECT(AActor,Z_Param_Actor);
		P_GET_UBOOL(Z_Param_bShouldBeSelected);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::SetActorSelectionState(Z_Param_Actor,Z_Param_bShouldBeSelected);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execSelectNothing)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::SelectNothing();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execClearActorSelectionSet)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::ClearActorSelectionSet();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execSetLevelViewportCameraInfo)
	{
		P_GET_STRUCT(FVector,Z_Param_CameraLocation);
		P_GET_STRUCT(FRotator,Z_Param_CameraRotation);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::SetLevelViewportCameraInfo(Z_Param_CameraLocation,Z_Param_CameraRotation);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execGetLevelViewportCameraInfo)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_CameraLocation);
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_CameraRotation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorLevelLibrary::GetLevelViewportCameraInfo(Z_Param_Out_CameraLocation,Z_Param_Out_CameraRotation);
		P_NATIVE_END;
	}
#if WITH_EDITOR
	DEFINE_FUNCTION(UEditorLevelLibrary::execSetCurrentLevelByName)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_LevelName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorLevelLibrary::SetCurrentLevelByName(Z_Param_LevelName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execSaveAllDirtyLevels)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorLevelLibrary::SaveAllDirtyLevels();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execSaveCurrentLevel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorLevelLibrary::SaveCurrentLevel();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execLoadLevel)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AssetPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorLevelLibrary::LoadLevel(Z_Param_AssetPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execNewLevelFromTemplate)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AssetPath);
		P_GET_PROPERTY(FStrProperty,Z_Param_TemplateAssetPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorLevelLibrary::NewLevelFromTemplate(Z_Param_AssetPath,Z_Param_TemplateAssetPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execNewLevel)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AssetPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UEditorLevelLibrary::NewLevel(Z_Param_AssetPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execGetPIEWorlds)
	{
		P_GET_UBOOL(Z_Param_bIncludeDedicatedServer);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UWorld*>*)Z_Param__Result=UEditorLevelLibrary::GetPIEWorlds(Z_Param_bIncludeDedicatedServer);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execGetGameWorld)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UWorld**)Z_Param__Result=UEditorLevelLibrary::GetGameWorld();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execGetEditorWorld)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UWorld**)Z_Param__Result=UEditorLevelLibrary::GetEditorWorld();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execReplaceSelectedActors)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InAssetPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::ReplaceSelectedActors(Z_Param_InAssetPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execEditorInvalidateViewports)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::EditorInvalidateViewports();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execEditorEndPlay)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::EditorEndPlay();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execEditorPlaySimulate)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::EditorPlaySimulate();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execEjectPilotLevelActor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::EjectPilotLevelActor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execPilotLevelActor)
	{
		P_GET_OBJECT(AActor,Z_Param_ActorToPilot);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::PilotLevelActor(Z_Param_ActorToPilot);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execSetSelectedLevelActors)
	{
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_ActorsToSelect);
		P_FINISH;
		P_NATIVE_BEGIN;
		UEditorLevelLibrary::SetSelectedLevelActors(Z_Param_Out_ActorsToSelect);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execGetSelectedLevelActors)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<AActor*>*)Z_Param__Result=UEditorLevelLibrary::GetSelectedLevelActors();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execGetAllLevelActorsComponents)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UActorComponent*>*)Z_Param__Result=UEditorLevelLibrary::GetAllLevelActorsComponents();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEditorLevelLibrary::execGetAllLevelActors)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<AActor*>*)Z_Param__Result=UEditorLevelLibrary::GetAllLevelActors();
		P_NATIVE_END;
	}
#endif //WITH_EDITOR
	void UEditorLevelLibrary::StaticRegisterNativesUEditorLevelLibrary()
	{
		UClass* Class = UEditorLevelLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ClearActorSelectionSet", &UEditorLevelLibrary::execClearActorSelectionSet },
			{ "ConvertActors", &UEditorLevelLibrary::execConvertActors },
			{ "CreateProxyMeshActor", &UEditorLevelLibrary::execCreateProxyMeshActor },
			{ "DestroyActor", &UEditorLevelLibrary::execDestroyActor },
#if WITH_EDITOR
			{ "EditorEndPlay", &UEditorLevelLibrary::execEditorEndPlay },
			{ "EditorInvalidateViewports", &UEditorLevelLibrary::execEditorInvalidateViewports },
			{ "EditorPlaySimulate", &UEditorLevelLibrary::execEditorPlaySimulate },
#endif // WITH_EDITOR
			{ "EditorSetGameView", &UEditorLevelLibrary::execEditorSetGameView },
#if WITH_EDITOR
			{ "EjectPilotLevelActor", &UEditorLevelLibrary::execEjectPilotLevelActor },
#endif // WITH_EDITOR
			{ "GetActorReference", &UEditorLevelLibrary::execGetActorReference },
#if WITH_EDITOR
			{ "GetAllLevelActors", &UEditorLevelLibrary::execGetAllLevelActors },
			{ "GetAllLevelActorsComponents", &UEditorLevelLibrary::execGetAllLevelActorsComponents },
			{ "GetEditorWorld", &UEditorLevelLibrary::execGetEditorWorld },
			{ "GetGameWorld", &UEditorLevelLibrary::execGetGameWorld },
#endif // WITH_EDITOR
			{ "GetLevelViewportCameraInfo", &UEditorLevelLibrary::execGetLevelViewportCameraInfo },
#if WITH_EDITOR
			{ "GetPIEWorlds", &UEditorLevelLibrary::execGetPIEWorlds },
			{ "GetSelectedLevelActors", &UEditorLevelLibrary::execGetSelectedLevelActors },
#endif // WITH_EDITOR
			{ "JoinStaticMeshActors", &UEditorLevelLibrary::execJoinStaticMeshActors },
#if WITH_EDITOR
			{ "LoadLevel", &UEditorLevelLibrary::execLoadLevel },
#endif // WITH_EDITOR
			{ "MergeStaticMeshActors", &UEditorLevelLibrary::execMergeStaticMeshActors },
#if WITH_EDITOR
			{ "NewLevel", &UEditorLevelLibrary::execNewLevel },
			{ "NewLevelFromTemplate", &UEditorLevelLibrary::execNewLevelFromTemplate },
			{ "PilotLevelActor", &UEditorLevelLibrary::execPilotLevelActor },
#endif // WITH_EDITOR
			{ "ReplaceMeshComponentsMaterials", &UEditorLevelLibrary::execReplaceMeshComponentsMaterials },
			{ "ReplaceMeshComponentsMaterialsOnActors", &UEditorLevelLibrary::execReplaceMeshComponentsMaterialsOnActors },
			{ "ReplaceMeshComponentsMeshes", &UEditorLevelLibrary::execReplaceMeshComponentsMeshes },
			{ "ReplaceMeshComponentsMeshesOnActors", &UEditorLevelLibrary::execReplaceMeshComponentsMeshesOnActors },
#if WITH_EDITOR
			{ "ReplaceSelectedActors", &UEditorLevelLibrary::execReplaceSelectedActors },
			{ "SaveAllDirtyLevels", &UEditorLevelLibrary::execSaveAllDirtyLevels },
			{ "SaveCurrentLevel", &UEditorLevelLibrary::execSaveCurrentLevel },
#endif // WITH_EDITOR
			{ "SelectNothing", &UEditorLevelLibrary::execSelectNothing },
			{ "SetActorSelectionState", &UEditorLevelLibrary::execSetActorSelectionState },
#if WITH_EDITOR
			{ "SetCurrentLevelByName", &UEditorLevelLibrary::execSetCurrentLevelByName },
#endif // WITH_EDITOR
			{ "SetLevelViewportCameraInfo", &UEditorLevelLibrary::execSetLevelViewportCameraInfo },
#if WITH_EDITOR
			{ "SetSelectedLevelActors", &UEditorLevelLibrary::execSetSelectedLevelActors },
#endif // WITH_EDITOR
			{ "SpawnActorFromClass", &UEditorLevelLibrary::execSpawnActorFromClass },
			{ "SpawnActorFromObject", &UEditorLevelLibrary::execSpawnActorFromObject },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEditorLevelLibrary_ClearActorSelectionSet_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ClearActorSelectionSet_Statics::Function_MetaDataParams[] = {
		{ "Category", "Development|Editor" },
		{ "Comment", "// Remove all actors from the selection set\n" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Remove all actors from the selection set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_ClearActorSelectionSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "ClearActorSelectionSet", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ClearActorSelectionSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ClearActorSelectionSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_ClearActorSelectionSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_ClearActorSelectionSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics
	{
		struct EditorLevelLibrary_eventConvertActors_Parms
		{
			TArray<AActor*> Actors;
			TSubclassOf<AActor>  ActorClass;
			FString StaticMeshPackagePath;
			TArray<AActor*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actors;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshPackagePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StaticMeshPackagePath;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_Actors_Inner = { "Actors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_Actors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_Actors = { "Actors", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventConvertActors_Parms, Actors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_Actors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_Actors_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_ActorClass = { "ActorClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventConvertActors_Parms, ActorClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_StaticMeshPackagePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_StaticMeshPackagePath = { "StaticMeshPackagePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventConvertActors_Parms, StaticMeshPackagePath), METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_StaticMeshPackagePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_StaticMeshPackagePath_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventConvertActors_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_Actors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_Actors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_ActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_StaticMeshPackagePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Dataprep" },
		{ "Comment", "/**\n\x09 * Replace in the level all Actors provided with a new actor of type ActorClass. Destroy all Actors provided.\n\x09 * @param\x09""Actors\x09\x09\x09\x09\x09List of Actors to replace.\n\x09 * @param\x09""ActorClass\x09\x09\x09\x09""Class/Blueprint of the new actor that will be spawn.\n\x09 * @param\x09StaticMeshPackagePath\x09If the list contains Brushes and it is requested to change them to StaticMesh, StaticMeshPackagePath is the package path to where the StaticMesh will be created. ie. /Game/MyFolder/\n\x09 */" },
		{ "DeterminesOutputType", "ActorClass" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Replace in the level all Actors provided with a new actor of type ActorClass. Destroy all Actors provided.\n@param       Actors                                  List of Actors to replace.\n@param       ActorClass                              Class/Blueprint of the new actor that will be spawn.\n@param       StaticMeshPackagePath   If the list contains Brushes and it is requested to change them to StaticMesh, StaticMeshPackagePath is the package path to where the StaticMesh will be created. ie. /Game/MyFolder/" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "ConvertActors", nullptr, nullptr, sizeof(EditorLevelLibrary_eventConvertActors_Parms), Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics
	{
		struct EditorLevelLibrary_eventCreateProxyMeshActor_Parms
		{
			TArray<AStaticMeshActor*> ActorsToMerge;
			FEditorScriptingCreateProxyMeshActorOptions MergeOptions;
			AStaticMeshActor* OutMergedActor;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorsToMerge_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorsToMerge_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActorsToMerge;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MergeOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MergeOptions;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutMergedActor;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ActorsToMerge_Inner = { "ActorsToMerge", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AStaticMeshActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ActorsToMerge_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ActorsToMerge = { "ActorsToMerge", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventCreateProxyMeshActor_Parms, ActorsToMerge), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ActorsToMerge_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ActorsToMerge_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_MergeOptions_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_MergeOptions = { "MergeOptions", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventCreateProxyMeshActor_Parms, MergeOptions), Z_Construct_UScriptStruct_FEditorScriptingCreateProxyMeshActorOptions, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_MergeOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_MergeOptions_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_OutMergedActor = { "OutMergedActor", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventCreateProxyMeshActor_Parms, OutMergedActor), Z_Construct_UClass_AStaticMeshActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventCreateProxyMeshActor_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventCreateProxyMeshActor_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ActorsToMerge_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ActorsToMerge,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_MergeOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_OutMergedActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Dataprep" },
		{ "Comment", "/**\n\x09 * Build a proxy mesh actor that can replace a set of mesh actors.\n\x09 * @param   ActorsToMerge  List of actors to build a proxy for.\n\x09 * @param   MergeOptions\n\x09 * @param   OutMergedActor generated actor if requested\n\x09 * @return  Success of the proxy creation\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Build a proxy mesh actor that can replace a set of mesh actors.\n@param   ActorsToMerge  List of actors to build a proxy for.\n@param   MergeOptions\n@param   OutMergedActor generated actor if requested\n@return  Success of the proxy creation" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "CreateProxyMeshActor", nullptr, nullptr, sizeof(EditorLevelLibrary_eventCreateProxyMeshActor_Parms), Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics
	{
		struct EditorLevelLibrary_eventDestroyActor_Parms
		{
			AActor* ActorToDestroy;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorToDestroy;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::NewProp_ActorToDestroy = { "ActorToDestroy", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventDestroyActor_Parms, ActorToDestroy), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventDestroyActor_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventDestroyActor_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::NewProp_ActorToDestroy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Destroy the actor from the world editor. Notify the Editor that the actor got destroyed.\n\x09 * @param\x09ToDestroyActor\x09""Actor to destroy.\n\x09 * @return\x09True if the operation succeeds.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Destroy the actor from the world editor. Notify the Editor that the actor got destroyed.\n@param       ToDestroyActor  Actor to destroy.\n@return      True if the operation succeeds." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "DestroyActor", nullptr, nullptr, sizeof(EditorLevelLibrary_eventDestroyActor_Parms), Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_EditorEndPlay_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_EditorEndPlay_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_EditorEndPlay_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "EditorEndPlay", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_EditorEndPlay_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_EditorEndPlay_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_EditorEndPlay()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_EditorEndPlay_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_EditorInvalidateViewports_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_EditorInvalidateViewports_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_EditorInvalidateViewports_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "EditorInvalidateViewports", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_EditorInvalidateViewports_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_EditorInvalidateViewports_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_EditorInvalidateViewports()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_EditorInvalidateViewports_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_EditorPlaySimulate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_EditorPlaySimulate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_EditorPlaySimulate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "EditorPlaySimulate", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_EditorPlaySimulate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_EditorPlaySimulate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_EditorPlaySimulate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_EditorPlaySimulate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics
	{
		struct EditorLevelLibrary_eventEditorSetGameView_Parms
		{
			bool bGameView;
		};
		static void NewProp_bGameView_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGameView;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::NewProp_bGameView_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventEditorSetGameView_Parms*)Obj)->bGameView = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::NewProp_bGameView = { "bGameView", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventEditorSetGameView_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::NewProp_bGameView_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::NewProp_bGameView,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "DevelopmentOnly", "" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "EditorSetGameView", nullptr, nullptr, sizeof(EditorLevelLibrary_eventEditorSetGameView_Parms), Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_EjectPilotLevelActor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_EjectPilotLevelActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_EjectPilotLevelActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "EjectPilotLevelActor", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_EjectPilotLevelActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_EjectPilotLevelActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_EjectPilotLevelActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_EjectPilotLevelActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics
	{
		struct EditorLevelLibrary_eventGetActorReference_Parms
		{
			FString PathToActor;
			AActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PathToActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::NewProp_PathToActor = { "PathToActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventGetActorReference_Parms, PathToActor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventGetActorReference_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::NewProp_PathToActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::Function_MetaDataParams[] = {
		{ "Category", "Development|Editor" },
		{ "Comment", "/**\n\x09* Attempts to find the actor specified by PathToActor in the current editor world\n\x09* @param\x09PathToActor\x09The path to the actor (e.g. PersistentLevel.PlayerStart)\n\x09* @return\x09""A reference to the actor, or none if it wasn't found\n\x09*/" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Attempts to find the actor specified by PathToActor in the current editor world\n@param        PathToActor     The path to the actor (e.g. PersistentLevel.PlayerStart)\n@return       A reference to the actor, or none if it wasn't found" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "GetActorReference", nullptr, nullptr, sizeof(EditorLevelLibrary_eventGetActorReference_Parms), Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics
	{
		struct EditorLevelLibrary_eventGetAllLevelActors_Parms
		{
			TArray<AActor*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventGetAllLevelActors_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Find all loaded Actors in the world editor. Exclude actor that are pending kill, in PIE, PreviewEditor, ...\n\x09 * @return\x09List of found Actors\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Find all loaded Actors in the world editor. Exclude actor that are pending kill, in PIE, PreviewEditor, ...\n@return      List of found Actors" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "GetAllLevelActors", nullptr, nullptr, sizeof(EditorLevelLibrary_eventGetAllLevelActors_Parms), Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics
	{
		struct EditorLevelLibrary_eventGetAllLevelActorsComponents_Parms
		{
			TArray<UActorComponent*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UActorComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000588, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventGetAllLevelActorsComponents_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Find all loaded ActorComponent own by an actor in the world editor. Exclude actor that are pending kill, in PIE, PreviewEditor, ...\n\x09 * @return\x09List of found ActorComponent\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Find all loaded ActorComponent own by an actor in the world editor. Exclude actor that are pending kill, in PIE, PreviewEditor, ...\n@return      List of found ActorComponent" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "GetAllLevelActorsComponents", nullptr, nullptr, sizeof(EditorLevelLibrary_eventGetAllLevelActorsComponents_Parms), Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics
	{
		struct EditorLevelLibrary_eventGetEditorWorld_Parms
		{
			UWorld* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventGetEditorWorld_Parms, ReturnValue), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Find the World in the world editor. It can then be used as WorldContext by other libraries like GameplayStatics.\n\x09 * @return\x09The World used by the world editor.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Find the World in the world editor. It can then be used as WorldContext by other libraries like GameplayStatics.\n@return      The World used by the world editor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "GetEditorWorld", nullptr, nullptr, sizeof(EditorLevelLibrary_eventGetEditorWorld_Parms), Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics
	{
		struct EditorLevelLibrary_eventGetGameWorld_Parms
		{
			UWorld* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventGetGameWorld_Parms, ReturnValue), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "GetGameWorld", nullptr, nullptr, sizeof(EditorLevelLibrary_eventGetGameWorld_Parms), Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics
	{
		struct EditorLevelLibrary_eventGetLevelViewportCameraInfo_Parms
		{
			FVector CameraLocation;
			FRotator CameraRotation;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraLocation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraRotation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::NewProp_CameraLocation = { "CameraLocation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventGetLevelViewportCameraInfo_Parms, CameraLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::NewProp_CameraRotation = { "CameraRotation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventGetLevelViewportCameraInfo_Parms, CameraRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventGetLevelViewportCameraInfo_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventGetLevelViewportCameraInfo_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::NewProp_CameraLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::NewProp_CameraRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::Function_MetaDataParams[] = {
		{ "Category", "Development|Editor" },
		{ "Comment", "/**\n\x09 * Gets information about the camera position for the primary level editor viewport.  In non-editor builds, these will be zeroed\n\x09 *\n\x09 * @param\x09""CameraLocation\x09(out) Current location of the level editing viewport camera, or zero if none found\n\x09 * @param\x09""CameraRotation\x09(out) Current rotation of the level editing viewport camera, or zero if none found\n\x09 * @return\x09Whether or not we were able to get a camera for a level editing viewport\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Gets information about the camera position for the primary level editor viewport.  In non-editor builds, these will be zeroed\n\n@param       CameraLocation  (out) Current location of the level editing viewport camera, or zero if none found\n@param       CameraRotation  (out) Current rotation of the level editing viewport camera, or zero if none found\n@return      Whether or not we were able to get a camera for a level editing viewport" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "GetLevelViewportCameraInfo", nullptr, nullptr, sizeof(EditorLevelLibrary_eventGetLevelViewportCameraInfo_Parms), Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics
	{
		struct EditorLevelLibrary_eventGetPIEWorlds_Parms
		{
			bool bIncludeDedicatedServer;
			TArray<UWorld*> ReturnValue;
		};
		static void NewProp_bIncludeDedicatedServer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeDedicatedServer;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::NewProp_bIncludeDedicatedServer_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventGetPIEWorlds_Parms*)Obj)->bIncludeDedicatedServer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::NewProp_bIncludeDedicatedServer = { "bIncludeDedicatedServer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventGetPIEWorlds_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::NewProp_bIncludeDedicatedServer_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventGetPIEWorlds_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::NewProp_bIncludeDedicatedServer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "GetPIEWorlds", nullptr, nullptr, sizeof(EditorLevelLibrary_eventGetPIEWorlds_Parms), Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics
	{
		struct EditorLevelLibrary_eventGetSelectedLevelActors_Parms
		{
			TArray<AActor*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventGetSelectedLevelActors_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Find all loaded Actors that are selected in the world editor. Exclude actor that are pending kill, in PIE, PreviewEditor, ...\n\x09 * @param\x09""ActorClass\x09""Actor Class to find.\n\x09 * @return\x09List of found Actors\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Find all loaded Actors that are selected in the world editor. Exclude actor that are pending kill, in PIE, PreviewEditor, ...\n@param       ActorClass      Actor Class to find.\n@return      List of found Actors" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "GetSelectedLevelActors", nullptr, nullptr, sizeof(EditorLevelLibrary_eventGetSelectedLevelActors_Parms), Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics
	{
		struct EditorLevelLibrary_eventJoinStaticMeshActors_Parms
		{
			TArray<AStaticMeshActor*> ActorsToJoin;
			FEditorScriptingJoinStaticMeshActorsOptions JoinOptions;
			AActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorsToJoin_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorsToJoin_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActorsToJoin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JoinOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_JoinOptions;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_ActorsToJoin_Inner = { "ActorsToJoin", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AStaticMeshActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_ActorsToJoin_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_ActorsToJoin = { "ActorsToJoin", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventJoinStaticMeshActors_Parms, ActorsToJoin), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_ActorsToJoin_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_ActorsToJoin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_JoinOptions_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_JoinOptions = { "JoinOptions", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventJoinStaticMeshActors_Parms, JoinOptions), Z_Construct_UScriptStruct_FEditorScriptingJoinStaticMeshActorsOptions, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_JoinOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_JoinOptions_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventJoinStaticMeshActors_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_ActorsToJoin_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_ActorsToJoin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_JoinOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Dataprep" },
		{ "Comment", "/**\n\x09 * Create a new Actor in the level that contains a duplicate of all the Actors Static Meshes Component.\n\x09 * The ActorsToJoin need to be in the same Level.\n\x09 * This will have a low impact on performance but may help the edition by grouping the meshes under a single Actor.\n\x09 * @param\x09""ActorsToJoin\x09\x09\x09List of Actors to join.\n\x09 * @param\x09JoinOptions\x09\x09\x09\x09Options on how to join the actors.\n\x09 * @return The new created actor.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Create a new Actor in the level that contains a duplicate of all the Actors Static Meshes Component.\nThe ActorsToJoin need to be in the same Level.\nThis will have a low impact on performance but may help the edition by grouping the meshes under a single Actor.\n@param       ActorsToJoin                    List of Actors to join.\n@param       JoinOptions                             Options on how to join the actors.\n@return The new created actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "JoinStaticMeshActors", nullptr, nullptr, sizeof(EditorLevelLibrary_eventJoinStaticMeshActors_Parms), Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics
	{
		struct EditorLevelLibrary_eventLoadLevel_Parms
		{
			FString AssetPath;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetPath;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::NewProp_AssetPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::NewProp_AssetPath = { "AssetPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventLoadLevel_Parms, AssetPath), METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::NewProp_AssetPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::NewProp_AssetPath_MetaData)) };
	void Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventLoadLevel_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventLoadLevel_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::NewProp_AssetPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Close the current Persistent Level (without saving it). Loads the specified level.\n\x09 * @param\x09""AssetPath\x09\x09\x09\x09""Asset Path of the level to be loaded.\n\x09 *\x09\x09ie. /Game/MyFolder/MyAsset\n\x09 * @return\x09True if the operation succeeds.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Close the current Persistent Level (without saving it). Loads the specified level.\n@param       AssetPath                               Asset Path of the level to be loaded.\n             ie. /Game/MyFolder/MyAsset\n@return      True if the operation succeeds." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "LoadLevel", nullptr, nullptr, sizeof(EditorLevelLibrary_eventLoadLevel_Parms), Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics
	{
		struct EditorLevelLibrary_eventMergeStaticMeshActors_Parms
		{
			TArray<AStaticMeshActor*> ActorsToMerge;
			FEditorScriptingMergeStaticMeshActorsOptions MergeOptions;
			AStaticMeshActor* OutMergedActor;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorsToMerge_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorsToMerge_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActorsToMerge;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MergeOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MergeOptions;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutMergedActor;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ActorsToMerge_Inner = { "ActorsToMerge", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AStaticMeshActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ActorsToMerge_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ActorsToMerge = { "ActorsToMerge", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventMergeStaticMeshActors_Parms, ActorsToMerge), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ActorsToMerge_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ActorsToMerge_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_MergeOptions_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_MergeOptions = { "MergeOptions", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventMergeStaticMeshActors_Parms, MergeOptions), Z_Construct_UScriptStruct_FEditorScriptingMergeStaticMeshActorsOptions, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_MergeOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_MergeOptions_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_OutMergedActor = { "OutMergedActor", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventMergeStaticMeshActors_Parms, OutMergedActor), Z_Construct_UClass_AStaticMeshActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventMergeStaticMeshActors_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventMergeStaticMeshActors_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ActorsToMerge_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ActorsToMerge,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_MergeOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_OutMergedActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Dataprep" },
		{ "Comment", "/**\n\x09 * Merge the meshes into a unique mesh with the provided StaticMeshActors. There are multiple options on how to merge the meshes and their materials.\n\x09 * The ActorsToMerge need to be in the same Level.\n\x09 * This may have a high impact on performance depending of the MeshMergingSettings options.\n\x09 * @param\x09""ActorsToMerge\x09\x09\x09List of Actors to merge.\n\x09 * @param\x09MergeOptions\x09\x09\x09Options on how to merge the actors.\n\x09 * @param\x09OutMergedActor\x09\x09\x09The new created actor, if requested.\n\x09 * @return\x09if the operation is successful.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Merge the meshes into a unique mesh with the provided StaticMeshActors. There are multiple options on how to merge the meshes and their materials.\nThe ActorsToMerge need to be in the same Level.\nThis may have a high impact on performance depending of the MeshMergingSettings options.\n@param       ActorsToMerge                   List of Actors to merge.\n@param       MergeOptions                    Options on how to merge the actors.\n@param       OutMergedActor                  The new created actor, if requested.\n@return      if the operation is successful." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "MergeStaticMeshActors", nullptr, nullptr, sizeof(EditorLevelLibrary_eventMergeStaticMeshActors_Parms), Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics
	{
		struct EditorLevelLibrary_eventNewLevel_Parms
		{
			FString AssetPath;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetPath;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::NewProp_AssetPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::NewProp_AssetPath = { "AssetPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventNewLevel_Parms, AssetPath), METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::NewProp_AssetPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::NewProp_AssetPath_MetaData)) };
	void Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventNewLevel_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventNewLevel_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::NewProp_AssetPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Close the current Persistent Level (without saving it). Create a new blank Level and save it. Load the new created level.\n\x09 * @param\x09""AssetPath\x09\x09""Asset Path of where the level will be saved.\n\x09 *\x09\x09ie. /Game/MyFolder/MyAsset\n\x09 * @return\x09True if the operation succeeds.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Close the current Persistent Level (without saving it). Create a new blank Level and save it. Load the new created level.\n@param       AssetPath               Asset Path of where the level will be saved.\n             ie. /Game/MyFolder/MyAsset\n@return      True if the operation succeeds." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "NewLevel", nullptr, nullptr, sizeof(EditorLevelLibrary_eventNewLevel_Parms), Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_NewLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_NewLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics
	{
		struct EditorLevelLibrary_eventNewLevelFromTemplate_Parms
		{
			FString AssetPath;
			FString TemplateAssetPath;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TemplateAssetPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TemplateAssetPath;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_AssetPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_AssetPath = { "AssetPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventNewLevelFromTemplate_Parms, AssetPath), METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_AssetPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_AssetPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_TemplateAssetPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_TemplateAssetPath = { "TemplateAssetPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventNewLevelFromTemplate_Parms, TemplateAssetPath), METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_TemplateAssetPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_TemplateAssetPath_MetaData)) };
	void Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventNewLevelFromTemplate_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventNewLevelFromTemplate_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_AssetPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_TemplateAssetPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Close the current Persistent Level (without saving it). Create a new Level base on another level and save it. Load the new created level.\n\x09 * @param\x09""AssetPath\x09\x09\x09\x09""Asset Path of where the level will be saved.\n\x09 *\x09\x09ie. /Game/MyFolder/MyAsset\n\x09 * @param\x09TemplateAssetPath\x09\x09Level to be used as Template.\n\x09 *\x09\x09ie. /Game/MyFolder/MyAsset\n\x09 * @return\x09True if the operation succeeds.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Close the current Persistent Level (without saving it). Create a new Level base on another level and save it. Load the new created level.\n@param       AssetPath                               Asset Path of where the level will be saved.\n             ie. /Game/MyFolder/MyAsset\n@param       TemplateAssetPath               Level to be used as Template.\n             ie. /Game/MyFolder/MyAsset\n@return      True if the operation succeeds." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "NewLevelFromTemplate", nullptr, nullptr, sizeof(EditorLevelLibrary_eventNewLevelFromTemplate_Parms), Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics
	{
		struct EditorLevelLibrary_eventPilotLevelActor_Parms
		{
			AActor* ActorToPilot;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorToPilot;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics::NewProp_ActorToPilot = { "ActorToPilot", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventPilotLevelActor_Parms, ActorToPilot), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics::NewProp_ActorToPilot,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "PilotLevelActor", nullptr, nullptr, sizeof(EditorLevelLibrary_eventPilotLevelActor_Parms), Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics
	{
		struct EditorLevelLibrary_eventReplaceMeshComponentsMaterials_Parms
		{
			TArray<UMeshComponent*> MeshComponents;
			UMaterialInterface* MaterialToBeReplaced;
			UMaterialInterface* NewMaterial;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MeshComponents;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialToBeReplaced;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_MeshComponents_Inner = { "MeshComponents", nullptr, (EPropertyFlags)0x0000000000080000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_MeshComponents_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_MeshComponents = { "MeshComponents", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMaterials_Parms, MeshComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_MeshComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_MeshComponents_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_MaterialToBeReplaced = { "MaterialToBeReplaced", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMaterials_Parms, MaterialToBeReplaced), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_NewMaterial = { "NewMaterial", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMaterials_Parms, NewMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_MeshComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_MeshComponents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_MaterialToBeReplaced,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::NewProp_NewMaterial,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Dataprep" },
		{ "Comment", "/**\n\x09 * Find the references of the material MaterialToReplaced on all the MeshComponents provided and replace it by NewMaterial.\n\x09 * @param\x09MeshComponents\x09\x09\x09List of MeshComponent to search from.\n\x09 * @param\x09MaterialToBeReplaced\x09Material we want to replace.\n\x09 * @param\x09NewMaterial\x09\x09\x09\x09Material to replace MaterialToBeReplaced by.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Find the references of the material MaterialToReplaced on all the MeshComponents provided and replace it by NewMaterial.\n@param       MeshComponents                  List of MeshComponent to search from.\n@param       MaterialToBeReplaced    Material we want to replace.\n@param       NewMaterial                             Material to replace MaterialToBeReplaced by." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "ReplaceMeshComponentsMaterials", nullptr, nullptr, sizeof(EditorLevelLibrary_eventReplaceMeshComponentsMaterials_Parms), Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics
	{
		struct EditorLevelLibrary_eventReplaceMeshComponentsMaterialsOnActors_Parms
		{
			TArray<AActor*> Actors;
			UMaterialInterface* MaterialToBeReplaced;
			UMaterialInterface* NewMaterial;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialToBeReplaced;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_Actors_Inner = { "Actors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_Actors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_Actors = { "Actors", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMaterialsOnActors_Parms, Actors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_Actors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_Actors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_MaterialToBeReplaced = { "MaterialToBeReplaced", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMaterialsOnActors_Parms, MaterialToBeReplaced), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_NewMaterial = { "NewMaterial", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMaterialsOnActors_Parms, NewMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_Actors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_Actors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_MaterialToBeReplaced,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::NewProp_NewMaterial,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Dataprep" },
		{ "Comment", "/**\n\x09 * Find the references of the material MaterialToReplaced on all the MeshComponents of all the Actors provided and replace it by NewMaterial.\n\x09 * @param\x09""Actors\x09\x09\x09\x09\x09List of Actors to search from.\n\x09 * @param\x09MaterialToBeReplaced\x09Material we want to replace.\n\x09 * @param\x09NewMaterial\x09\x09\x09\x09Material to replace MaterialToBeReplaced by.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Find the references of the material MaterialToReplaced on all the MeshComponents of all the Actors provided and replace it by NewMaterial.\n@param       Actors                                  List of Actors to search from.\n@param       MaterialToBeReplaced    Material we want to replace.\n@param       NewMaterial                             Material to replace MaterialToBeReplaced by." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "ReplaceMeshComponentsMaterialsOnActors", nullptr, nullptr, sizeof(EditorLevelLibrary_eventReplaceMeshComponentsMaterialsOnActors_Parms), Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics
	{
		struct EditorLevelLibrary_eventReplaceMeshComponentsMeshes_Parms
		{
			TArray<UStaticMeshComponent*> MeshComponents;
			UStaticMesh* MeshToBeReplaced;
			UStaticMesh* NewMesh;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MeshComponents;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshToBeReplaced;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_MeshComponents_Inner = { "MeshComponents", nullptr, (EPropertyFlags)0x0000000000080000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_MeshComponents_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_MeshComponents = { "MeshComponents", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMeshes_Parms, MeshComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_MeshComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_MeshComponents_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_MeshToBeReplaced = { "MeshToBeReplaced", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMeshes_Parms, MeshToBeReplaced), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_NewMesh = { "NewMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMeshes_Parms, NewMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_MeshComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_MeshComponents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_MeshToBeReplaced,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::NewProp_NewMesh,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Dataprep" },
		{ "Comment", "/**\n\x09 * Find the references of the mesh MeshToBeReplaced on all the MeshComponents provided and replace it by NewMesh.\n\x09 * The editor should not be in play in editor mode.\n\x09 * @param\x09MeshComponents\x09\x09\x09List of MeshComponent to search from.\n\x09 * @param\x09MeshToBeReplaced\x09\x09Mesh we want to replace.\n\x09 * @param\x09NewMesh\x09\x09\x09\x09\x09Mesh to replace MeshToBeReplaced by.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Find the references of the mesh MeshToBeReplaced on all the MeshComponents provided and replace it by NewMesh.\nThe editor should not be in play in editor mode.\n@param       MeshComponents                  List of MeshComponent to search from.\n@param       MeshToBeReplaced                Mesh we want to replace.\n@param       NewMesh                                 Mesh to replace MeshToBeReplaced by." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "ReplaceMeshComponentsMeshes", nullptr, nullptr, sizeof(EditorLevelLibrary_eventReplaceMeshComponentsMeshes_Parms), Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics
	{
		struct EditorLevelLibrary_eventReplaceMeshComponentsMeshesOnActors_Parms
		{
			TArray<AActor*> Actors;
			UStaticMesh* MeshToBeReplaced;
			UStaticMesh* NewMesh;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshToBeReplaced;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_Actors_Inner = { "Actors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_Actors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_Actors = { "Actors", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMeshesOnActors_Parms, Actors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_Actors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_Actors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_MeshToBeReplaced = { "MeshToBeReplaced", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMeshesOnActors_Parms, MeshToBeReplaced), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_NewMesh = { "NewMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceMeshComponentsMeshesOnActors_Parms, NewMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_Actors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_Actors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_MeshToBeReplaced,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::NewProp_NewMesh,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Dataprep" },
		{ "Comment", "/**\n\x09 * Find the references of the mesh MeshToBeReplaced on all the MeshComponents of all the Actors provided and replace it by NewMesh.\n\x09 * @param\x09""Actors\x09\x09\x09\x09\x09List of Actors to search from.\n\x09 * @param\x09MeshToBeReplaced\x09\x09Mesh we want to replace.\n\x09 * @param\x09NewMesh\x09\x09\x09\x09\x09Mesh to replace MeshToBeReplaced by.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Find the references of the mesh MeshToBeReplaced on all the MeshComponents of all the Actors provided and replace it by NewMesh.\n@param       Actors                                  List of Actors to search from.\n@param       MeshToBeReplaced                Mesh we want to replace.\n@param       NewMesh                                 Mesh to replace MeshToBeReplaced by." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "ReplaceMeshComponentsMeshesOnActors", nullptr, nullptr, sizeof(EditorLevelLibrary_eventReplaceMeshComponentsMeshesOnActors_Parms), Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics
	{
		struct EditorLevelLibrary_eventReplaceSelectedActors_Parms
		{
			FString InAssetPath;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InAssetPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InAssetPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::NewProp_InAssetPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::NewProp_InAssetPath = { "InAssetPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventReplaceSelectedActors_Parms, InAssetPath), METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::NewProp_InAssetPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::NewProp_InAssetPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::NewProp_InAssetPath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Replaces the selected Actors with the same number of a different kind of Actor using the specified factory to spawn the new Actors\n\x09 * note that only Location, Rotation, Drawscale, Drawscale3D, Tag, and Group are copied from the old Actors\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Replaces the selected Actors with the same number of a different kind of Actor using the specified factory to spawn the new Actors\nnote that only Location, Rotation, Drawscale, Drawscale3D, Tag, and Group are copied from the old Actors" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "ReplaceSelectedActors", nullptr, nullptr, sizeof(EditorLevelLibrary_eventReplaceSelectedActors_Parms), Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics
	{
		struct EditorLevelLibrary_eventSaveAllDirtyLevels_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventSaveAllDirtyLevels_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventSaveAllDirtyLevels_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Saves all Level currently loaded by the World Editor.\n\x09 * @return\x09True if the operation succeeds.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Saves all Level currently loaded by the World Editor.\n@return      True if the operation succeeds." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "SaveAllDirtyLevels", nullptr, nullptr, sizeof(EditorLevelLibrary_eventSaveAllDirtyLevels_Parms), Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics
	{
		struct EditorLevelLibrary_eventSaveCurrentLevel_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventSaveCurrentLevel_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventSaveCurrentLevel_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Saves the specified Level. Must already be saved at lease once to have a valid path.\n\x09 * @return\x09True if the operation succeeds.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Saves the specified Level. Must already be saved at lease once to have a valid path.\n@return      True if the operation succeeds." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "SaveCurrentLevel", nullptr, nullptr, sizeof(EditorLevelLibrary_eventSaveCurrentLevel_Parms), Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_SelectNothing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_SelectNothing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Development|Editor" },
		{ "Comment", "// Selects nothing in the editor (another way to clear the selection)\n" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Selects nothing in the editor (another way to clear the selection)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_SelectNothing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "SelectNothing", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_SelectNothing_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SelectNothing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_SelectNothing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_SelectNothing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics
	{
		struct EditorLevelLibrary_eventSetActorSelectionState_Parms
		{
			AActor* Actor;
			bool bShouldBeSelected;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static void NewProp_bShouldBeSelected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldBeSelected;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSetActorSelectionState_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::NewProp_bShouldBeSelected_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventSetActorSelectionState_Parms*)Obj)->bShouldBeSelected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::NewProp_bShouldBeSelected = { "bShouldBeSelected", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventSetActorSelectionState_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::NewProp_bShouldBeSelected_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::NewProp_Actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::NewProp_bShouldBeSelected,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Development|Editor" },
		{ "Comment", "// Set the selection state for the selected actor\n" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Set the selection state for the selected actor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "SetActorSelectionState", nullptr, nullptr, sizeof(EditorLevelLibrary_eventSetActorSelectionState_Parms), Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics
	{
		struct EditorLevelLibrary_eventSetCurrentLevelByName_Parms
		{
			FName LevelName;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LevelName;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::NewProp_LevelName = { "LevelName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSetCurrentLevelByName_Parms, LevelName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventSetCurrentLevelByName_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventSetCurrentLevelByName_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::NewProp_LevelName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Set the current level used by the world editor.\n\x09 * If more than one level shares the same name, the first one encounter of that level name will be used.\n\x09 * @param\x09LevelName\x09The name of the Level the actor belongs to (same name as in the ContentBrowser).\n\x09 * @return\x09True if the operation succeeds.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Set the current level used by the world editor.\nIf more than one level shares the same name, the first one encounter of that level name will be used.\n@param       LevelName       The name of the Level the actor belongs to (same name as in the ContentBrowser).\n@return      True if the operation succeeds." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "SetCurrentLevelByName", nullptr, nullptr, sizeof(EditorLevelLibrary_eventSetCurrentLevelByName_Parms), Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics
	{
		struct EditorLevelLibrary_eventSetLevelViewportCameraInfo_Parms
		{
			FVector CameraLocation;
			FRotator CameraRotation;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraLocation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraRotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::NewProp_CameraLocation = { "CameraLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSetLevelViewportCameraInfo_Parms, CameraLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::NewProp_CameraRotation = { "CameraRotation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSetLevelViewportCameraInfo_Parms, CameraRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::NewProp_CameraLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::NewProp_CameraRotation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::Function_MetaDataParams[] = {
		{ "Category", "Development|Editor" },
		{ "Comment", "/**\n\x09* Sets information about the camera position for the primary level editor viewport.\n\x09*\n\x09* @param\x09""CameraLocation\x09Location the camera will be moved to.\n\x09* @param\x09""CameraRotation\x09Rotation the camera will be set to.\n\x09*/" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Sets information about the camera position for the primary level editor viewport.\n\n@param        CameraLocation  Location the camera will be moved to.\n@param        CameraRotation  Rotation the camera will be set to." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "SetLevelViewportCameraInfo", nullptr, nullptr, sizeof(EditorLevelLibrary_eventSetLevelViewportCameraInfo_Parms), Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics
	{
		struct EditorLevelLibrary_eventSetSelectedLevelActors_Parms
		{
			TArray<AActor*> ActorsToSelect;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorsToSelect_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorsToSelect_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActorsToSelect;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::NewProp_ActorsToSelect_Inner = { "ActorsToSelect", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::NewProp_ActorsToSelect_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::NewProp_ActorsToSelect = { "ActorsToSelect", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSetSelectedLevelActors_Parms, ActorsToSelect), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::NewProp_ActorsToSelect_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::NewProp_ActorsToSelect_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::NewProp_ActorsToSelect_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::NewProp_ActorsToSelect,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Clear the current world editor selection and select the provided actors. Exclude actor that are pending kill, in PIE, PreviewEditor, ...\n\x09 * @param\x09""ActorsToSelect\x09""Actor that should be selected in the world editor.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Clear the current world editor selection and select the provided actors. Exclude actor that are pending kill, in PIE, PreviewEditor, ...\n@param       ActorsToSelect  Actor that should be selected in the world editor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "SetSelectedLevelActors", nullptr, nullptr, sizeof(EditorLevelLibrary_eventSetSelectedLevelActors_Parms), Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	struct Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics
	{
		struct EditorLevelLibrary_eventSpawnActorFromClass_Parms
		{
			TSubclassOf<AActor>  ActorClass;
			FVector Location;
			FRotator Rotation;
			bool bTransient;
			AActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorClass;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static void NewProp_bTransient_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTransient;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_ActorClass = { "ActorClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSpawnActorFromClass_Parms, ActorClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSpawnActorFromClass_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSpawnActorFromClass_Parms, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_bTransient_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventSpawnActorFromClass_Parms*)Obj)->bTransient = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_bTransient = { "bTransient", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventSpawnActorFromClass_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_bTransient_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSpawnActorFromClass_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_ActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_bTransient,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Create an actor and place it in the world editor. Can be created from a Blueprint or a Class.\n\x09 * The actor will be created in the current level and will be selected.\n\x09 * @param\x09""ActorClass\x09\x09""Asset to attempt to use for an actor to place.\n\x09 * @param\x09Location\x09\x09Location of the new actor.\n\x09 * @return\x09The created actor.\n\x09 */" },
		{ "CPP_Default_bTransient", "false" },
		{ "CPP_Default_Rotation", "" },
		{ "DeterminesOutputType", "ActorClass" },
		{ "KeyWords", "Transient" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Create an actor and place it in the world editor. Can be created from a Blueprint or a Class.\nThe actor will be created in the current level and will be selected.\n@param       ActorClass              Asset to attempt to use for an actor to place.\n@param       Location                Location of the new actor.\n@return      The created actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "SpawnActorFromClass", nullptr, nullptr, sizeof(EditorLevelLibrary_eventSpawnActorFromClass_Parms), Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics
	{
		struct EditorLevelLibrary_eventSpawnActorFromObject_Parms
		{
			UObject* ObjectToUse;
			FVector Location;
			FRotator Rotation;
			bool bTransient;
			AActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectToUse;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static void NewProp_bTransient_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTransient;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_ObjectToUse = { "ObjectToUse", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSpawnActorFromObject_Parms, ObjectToUse), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSpawnActorFromObject_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSpawnActorFromObject_Parms, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_bTransient_SetBit(void* Obj)
	{
		((EditorLevelLibrary_eventSpawnActorFromObject_Parms*)Obj)->bTransient = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_bTransient = { "bTransient", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EditorLevelLibrary_eventSpawnActorFromObject_Parms), &Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_bTransient_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EditorLevelLibrary_eventSpawnActorFromObject_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_ObjectToUse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_bTransient,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Level Utility" },
		{ "Comment", "/**\n\x09 * Create an actor and place it in the world editor. The Actor can be created from a Factory, Archetype, Blueprint, Class or an Asset.\n\x09 * The actor will be created in the current level and will be selected.\n\x09 * @param\x09ObjectToUse\x09\x09""Asset to attempt to use for an actor to place.\n\x09 * @param\x09Location\x09\x09Location of the new actor.\n\x09 * @return\x09The created actor.\n\x09 */" },
		{ "CPP_Default_bTransient", "false" },
		{ "CPP_Default_Rotation", "" },
		{ "KeyWords", "Transient" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Create an actor and place it in the world editor. The Actor can be created from a Factory, Archetype, Blueprint, Class or an Asset.\nThe actor will be created in the current level and will be selected.\n@param       ObjectToUse             Asset to attempt to use for an actor to place.\n@param       Location                Location of the new actor.\n@return      The created actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEditorLevelLibrary, nullptr, "SpawnActorFromObject", nullptr, nullptr, sizeof(EditorLevelLibrary_eventSpawnActorFromObject_Parms), Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEditorLevelLibrary_NoRegister()
	{
		return UEditorLevelLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UEditorLevelLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditorLevelLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_EditorScriptingUtilities,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEditorLevelLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEditorLevelLibrary_ClearActorSelectionSet, "ClearActorSelectionSet" }, // 2136613247
		{ &Z_Construct_UFunction_UEditorLevelLibrary_ConvertActors, "ConvertActors" }, // 1151202797
		{ &Z_Construct_UFunction_UEditorLevelLibrary_CreateProxyMeshActor, "CreateProxyMeshActor" }, // 981327735
		{ &Z_Construct_UFunction_UEditorLevelLibrary_DestroyActor, "DestroyActor" }, // 1107728212
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_EditorEndPlay, "EditorEndPlay" }, // 4015473167
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_EditorInvalidateViewports, "EditorInvalidateViewports" }, // 2955190766
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_EditorPlaySimulate, "EditorPlaySimulate" }, // 1013487734
#endif //WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_EditorSetGameView, "EditorSetGameView" }, // 745750200
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_EjectPilotLevelActor, "EjectPilotLevelActor" }, // 2868096849
#endif //WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_GetActorReference, "GetActorReference" }, // 2064849135
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActors, "GetAllLevelActors" }, // 1902337910
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_GetAllLevelActorsComponents, "GetAllLevelActorsComponents" }, // 1559733741
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_GetEditorWorld, "GetEditorWorld" }, // 1548849536
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_GetGameWorld, "GetGameWorld" }, // 1734133836
#endif //WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_GetLevelViewportCameraInfo, "GetLevelViewportCameraInfo" }, // 478895612
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_GetPIEWorlds, "GetPIEWorlds" }, // 2496536026
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_GetSelectedLevelActors, "GetSelectedLevelActors" }, // 2827796063
#endif //WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_JoinStaticMeshActors, "JoinStaticMeshActors" }, // 732061407
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_LoadLevel, "LoadLevel" }, // 2964574031
#endif //WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_MergeStaticMeshActors, "MergeStaticMeshActors" }, // 2599104256
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_NewLevel, "NewLevel" }, // 1221421792
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_NewLevelFromTemplate, "NewLevelFromTemplate" }, // 756129407
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_PilotLevelActor, "PilotLevelActor" }, // 3034313452
#endif //WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterials, "ReplaceMeshComponentsMaterials" }, // 903277996
		{ &Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMaterialsOnActors, "ReplaceMeshComponentsMaterialsOnActors" }, // 3263443545
		{ &Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshes, "ReplaceMeshComponentsMeshes" }, // 1156816345
		{ &Z_Construct_UFunction_UEditorLevelLibrary_ReplaceMeshComponentsMeshesOnActors, "ReplaceMeshComponentsMeshesOnActors" }, // 820922892
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_ReplaceSelectedActors, "ReplaceSelectedActors" }, // 3471169913
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_SaveAllDirtyLevels, "SaveAllDirtyLevels" }, // 2961119305
#endif //WITH_EDITOR
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_SaveCurrentLevel, "SaveCurrentLevel" }, // 381387465
#endif //WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_SelectNothing, "SelectNothing" }, // 4150011206
		{ &Z_Construct_UFunction_UEditorLevelLibrary_SetActorSelectionState, "SetActorSelectionState" }, // 576187190
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_SetCurrentLevelByName, "SetCurrentLevelByName" }, // 2533261564
#endif //WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_SetLevelViewportCameraInfo, "SetLevelViewportCameraInfo" }, // 3839755494
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_SetSelectedLevelActors, "SetSelectedLevelActors" }, // 2369664264
#endif //WITH_EDITOR
		{ &Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromClass, "SpawnActorFromClass" }, // 1227531417
		{ &Z_Construct_UFunction_UEditorLevelLibrary_SpawnActorFromObject, "SpawnActorFromObject" }, // 1917131107
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorLevelLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Utility class to do most of the common functionalities in the World Editor.\n * The editor should not be in play in editor mode.\n */" },
		{ "IncludePath", "EditorLevelLibrary.h" },
		{ "ModuleRelativePath", "Public/EditorLevelLibrary.h" },
		{ "ToolTip", "Utility class to do most of the common functionalities in the World Editor.\nThe editor should not be in play in editor mode." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditorLevelLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditorLevelLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditorLevelLibrary_Statics::ClassParams = {
		&UEditorLevelLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEditorLevelLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorLevelLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditorLevelLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditorLevelLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditorLevelLibrary, 3238231884);
	template<> EDITORSCRIPTINGUTILITIES_API UClass* StaticClass<UEditorLevelLibrary>()
	{
		return UEditorLevelLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditorLevelLibrary(Z_Construct_UClass_UEditorLevelLibrary, &UEditorLevelLibrary::StaticClass, TEXT("/Script/EditorScriptingUtilities"), TEXT("UEditorLevelLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditorLevelLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
