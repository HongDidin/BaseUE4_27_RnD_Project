// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef EDITORSCRIPTINGUTILITIES_EditorDialogLibrary_generated_h
#error "EditorDialogLibrary.generated.h already included, missing '#pragma once' in EditorDialogLibrary.h"
#endif
#define EDITORSCRIPTINGUTILITIES_EditorDialogLibrary_generated_h

#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_SPARSE_DATA
#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execShowMessage);


#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execShowMessage);


#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditorDialogLibrary(); \
	friend struct Z_Construct_UClass_UEditorDialogLibrary_Statics; \
public: \
	DECLARE_CLASS(UEditorDialogLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EditorScriptingUtilities"), NO_API) \
	DECLARE_SERIALIZER(UEditorDialogLibrary)


#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUEditorDialogLibrary(); \
	friend struct Z_Construct_UClass_UEditorDialogLibrary_Statics; \
public: \
	DECLARE_CLASS(UEditorDialogLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EditorScriptingUtilities"), NO_API) \
	DECLARE_SERIALIZER(UEditorDialogLibrary)


#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditorDialogLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditorDialogLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditorDialogLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditorDialogLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditorDialogLibrary(UEditorDialogLibrary&&); \
	NO_API UEditorDialogLibrary(const UEditorDialogLibrary&); \
public:


#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditorDialogLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditorDialogLibrary(UEditorDialogLibrary&&); \
	NO_API UEditorDialogLibrary(const UEditorDialogLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditorDialogLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditorDialogLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditorDialogLibrary)


#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_12_PROLOG
#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_RPC_WRAPPERS \
	Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_INCLASS \
	Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> EDITORSCRIPTINGUTILITIES_API UClass* StaticClass<class UEditorDialogLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_EditorScriptingUtilities_Source_EditorScriptingUtilities_Public_EditorDialogLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
