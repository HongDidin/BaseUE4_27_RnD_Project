// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FACIALANIMATIONEDITOR_FacialAnimationBulkImporterSettings_generated_h
#error "FacialAnimationBulkImporterSettings.generated.h already included, missing '#pragma once' in FacialAnimationBulkImporterSettings.h"
#endif
#define FACIALANIMATIONEDITOR_FacialAnimationBulkImporterSettings_generated_h

#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_SPARSE_DATA
#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_RPC_WRAPPERS
#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFacialAnimationBulkImporterSettings(); \
	friend struct Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics; \
public: \
	DECLARE_CLASS(UFacialAnimationBulkImporterSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FacialAnimationEditor"), FACIALANIMATIONEDITOR_API) \
	DECLARE_SERIALIZER(UFacialAnimationBulkImporterSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUFacialAnimationBulkImporterSettings(); \
	friend struct Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics; \
public: \
	DECLARE_CLASS(UFacialAnimationBulkImporterSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FacialAnimationEditor"), FACIALANIMATIONEDITOR_API) \
	DECLARE_SERIALIZER(UFacialAnimationBulkImporterSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FACIALANIMATIONEDITOR_API UFacialAnimationBulkImporterSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFacialAnimationBulkImporterSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FACIALANIMATIONEDITOR_API, UFacialAnimationBulkImporterSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFacialAnimationBulkImporterSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FACIALANIMATIONEDITOR_API UFacialAnimationBulkImporterSettings(UFacialAnimationBulkImporterSettings&&); \
	FACIALANIMATIONEDITOR_API UFacialAnimationBulkImporterSettings(const UFacialAnimationBulkImporterSettings&); \
public:


#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FACIALANIMATIONEDITOR_API UFacialAnimationBulkImporterSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FACIALANIMATIONEDITOR_API UFacialAnimationBulkImporterSettings(UFacialAnimationBulkImporterSettings&&); \
	FACIALANIMATIONEDITOR_API UFacialAnimationBulkImporterSettings(const UFacialAnimationBulkImporterSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FACIALANIMATIONEDITOR_API, UFacialAnimationBulkImporterSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFacialAnimationBulkImporterSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFacialAnimationBulkImporterSettings)


#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_11_PROLOG
#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_RPC_WRAPPERS \
	Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_INCLASS \
	Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_SPARSE_DATA \
	Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h_15_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FACIALANIMATIONEDITOR_API UClass* StaticClass<class UFacialAnimationBulkImporterSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Editor_FacialAnimation_Source_FacialAnimationEditor_Private_FacialAnimationBulkImporterSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
