// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FacialAnimationEditor/Private/FacialAnimationBulkImporterSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFacialAnimationBulkImporterSettings() {}
// Cross Module References
	FACIALANIMATIONEDITOR_API UClass* Z_Construct_UClass_UFacialAnimationBulkImporterSettings_NoRegister();
	FACIALANIMATIONEDITOR_API UClass* Z_Construct_UClass_UFacialAnimationBulkImporterSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_FacialAnimationEditor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
	void UFacialAnimationBulkImporterSettings::StaticRegisterNativesUFacialAnimationBulkImporterSettings()
	{
	}
	UClass* Z_Construct_UClass_UFacialAnimationBulkImporterSettings_NoRegister()
	{
		return UFacialAnimationBulkImporterSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceImportPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SourceImportPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetImportPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetImportPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveNodeName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CurveNodeName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_FacialAnimationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::Class_MetaDataParams[] = {
		{ "DevelopmentStatus", "Experimental" },
		{ "IncludePath", "FacialAnimationBulkImporterSettings.h" },
		{ "ModuleRelativePath", "Private/FacialAnimationBulkImporterSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_SourceImportPath_MetaData[] = {
		{ "Category", "Directories" },
		{ "Comment", "/** The path to import files from */" },
		{ "ModuleRelativePath", "Private/FacialAnimationBulkImporterSettings.h" },
		{ "ToolTip", "The path to import files from" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_SourceImportPath = { "SourceImportPath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFacialAnimationBulkImporterSettings, SourceImportPath), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_SourceImportPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_SourceImportPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_TargetImportPath_MetaData[] = {
		{ "Category", "Directories" },
		{ "Comment", "/** The path to import files to */" },
		{ "ContentDir", "" },
		{ "ModuleRelativePath", "Private/FacialAnimationBulkImporterSettings.h" },
		{ "ToolTip", "The path to import files to" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_TargetImportPath = { "TargetImportPath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFacialAnimationBulkImporterSettings, TargetImportPath), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_TargetImportPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_TargetImportPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_CurveNodeName_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Node in the FBX scene that contains the curves we are interested in */" },
		{ "ModuleRelativePath", "Private/FacialAnimationBulkImporterSettings.h" },
		{ "ToolTip", "Node in the FBX scene that contains the curves we are interested in" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_CurveNodeName = { "CurveNodeName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFacialAnimationBulkImporterSettings, CurveNodeName), METADATA_PARAMS(Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_CurveNodeName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_CurveNodeName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_SourceImportPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_TargetImportPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::NewProp_CurveNodeName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFacialAnimationBulkImporterSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::ClassParams = {
		&UFacialAnimationBulkImporterSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFacialAnimationBulkImporterSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFacialAnimationBulkImporterSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFacialAnimationBulkImporterSettings, 2151805030);
	template<> FACIALANIMATIONEDITOR_API UClass* StaticClass<UFacialAnimationBulkImporterSettings>()
	{
		return UFacialAnimationBulkImporterSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFacialAnimationBulkImporterSettings(Z_Construct_UClass_UFacialAnimationBulkImporterSettings, &UFacialAnimationBulkImporterSettings::StaticClass, TEXT("/Script/FacialAnimationEditor"), TEXT("UFacialAnimationBulkImporterSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFacialAnimationBulkImporterSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
