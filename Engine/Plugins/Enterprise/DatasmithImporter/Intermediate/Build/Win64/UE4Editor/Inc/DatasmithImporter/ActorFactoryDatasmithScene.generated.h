// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHIMPORTER_ActorFactoryDatasmithScene_generated_h
#error "ActorFactoryDatasmithScene.generated.h already included, missing '#pragma once' in ActorFactoryDatasmithScene.h"
#endif
#define DATASMITHIMPORTER_ActorFactoryDatasmithScene_generated_h

#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorFactoryDatasmithScene(); \
	friend struct Z_Construct_UClass_UActorFactoryDatasmithScene_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryDatasmithScene, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), DATASMITHIMPORTER_API) \
	DECLARE_SERIALIZER(UActorFactoryDatasmithScene)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUActorFactoryDatasmithScene(); \
	friend struct Z_Construct_UClass_UActorFactoryDatasmithScene_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryDatasmithScene, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), DATASMITHIMPORTER_API) \
	DECLARE_SERIALIZER(UActorFactoryDatasmithScene)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DATASMITHIMPORTER_API UActorFactoryDatasmithScene(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryDatasmithScene) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATASMITHIMPORTER_API, UActorFactoryDatasmithScene); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryDatasmithScene); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATASMITHIMPORTER_API UActorFactoryDatasmithScene(UActorFactoryDatasmithScene&&); \
	DATASMITHIMPORTER_API UActorFactoryDatasmithScene(const UActorFactoryDatasmithScene&); \
public:


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DATASMITHIMPORTER_API UActorFactoryDatasmithScene(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATASMITHIMPORTER_API UActorFactoryDatasmithScene(UActorFactoryDatasmithScene&&); \
	DATASMITHIMPORTER_API UActorFactoryDatasmithScene(const UActorFactoryDatasmithScene&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATASMITHIMPORTER_API, UActorFactoryDatasmithScene); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryDatasmithScene); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryDatasmithScene)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_11_PROLOG
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_INCLASS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ActorFactoryDatasmithScene."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHIMPORTER_API UClass* StaticClass<class UActorFactoryDatasmithScene>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_ActorFactoryDatasmithScene_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
