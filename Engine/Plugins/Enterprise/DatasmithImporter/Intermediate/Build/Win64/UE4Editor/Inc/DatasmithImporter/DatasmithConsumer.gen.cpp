// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithImporter/Private/DatasmithConsumer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithConsumer() {}
// Cross Module References
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDatasmithConsumer_NoRegister();
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDatasmithConsumer();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepContentConsumer();
	UPackage* Z_Construct_UPackage__Script_DatasmithImporter();
	DATASMITHCONTENT_API UClass* Z_Construct_UClass_UDatasmithScene_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
// End Cross Module References
	DEFINE_FUNCTION(UDatasmithConsumer::execGetDatasmithScene)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDatasmithScene**)Z_Param__Result=P_THIS->GetDatasmithScene();
		P_NATIVE_END;
	}
	void UDatasmithConsumer::StaticRegisterNativesUDatasmithConsumer()
	{
		UClass* Class = UDatasmithConsumer::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDatasmithScene", &UDatasmithConsumer::execGetDatasmithScene },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics
	{
		struct DatasmithConsumer_eventGetDatasmithScene_Parms
		{
			UDatasmithScene* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DatasmithConsumer_eventGetDatasmithScene_Parms, ReturnValue), Z_Construct_UClass_UDatasmithScene_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics::Function_MetaDataParams[] = {
		{ "Category", "DatasmithConsumerInternal" },
		{ "ModuleRelativePath", "Private/DatasmithConsumer.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDatasmithConsumer, nullptr, "GetDatasmithScene", nullptr, nullptr, sizeof(DatasmithConsumer_eventGetDatasmithScene_Parms), Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDatasmithConsumer_NoRegister()
	{
		return UDatasmithConsumer::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithConsumer_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DatasmithScene_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DatasmithScene;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniqueID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UniqueID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DatasmithSceneObjectPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DatasmithSceneObjectPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputLevelObjectPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutputLevelObjectPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputLevelSoftObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputLevelSoftObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DatasmithSceneWeakPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_DatasmithSceneWeakPtr;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithConsumer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepContentConsumer,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithImporter,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDatasmithConsumer_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDatasmithConsumer_GetDatasmithScene, "GetDatasmithScene" }, // 426045209
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithConsumer_Statics::Class_MetaDataParams[] = {
		{ "DevelopmentStatus", "Experimental" },
		{ "HideCategories", "DatasmithConsumerInternal" },
		{ "IncludePath", "DatasmithConsumer.h" },
		{ "ModuleRelativePath", "Private/DatasmithConsumer.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithScene_MetaData[] = {
		{ "Comment", "/** DEPRECATED: Removing use of TSoftObjectPtr */" },
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Use GetDatasmithScene method to obtain the associated UDatasmithScene." },
		{ "ModuleRelativePath", "Private/DatasmithConsumer.h" },
		{ "ToolTip", "DEPRECATED: Removing use of TSoftObjectPtr" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithScene = { "DatasmithScene", nullptr, (EPropertyFlags)0x0014000020000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithConsumer, DatasmithScene_DEPRECATED), Z_Construct_UClass_UDatasmithScene_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithScene_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithScene_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_UniqueID_MetaData[] = {
		{ "Category", "DatasmithConsumerInternal" },
		{ "Comment", "/** Stores the level used on the last call to UDatasmithConsumer::Run */" },
		{ "ModuleRelativePath", "Private/DatasmithConsumer.h" },
		{ "ToolTip", "Stores the level used on the last call to UDatasmithConsumer::Run" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_UniqueID = { "UniqueID", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithConsumer, UniqueID), METADATA_PARAMS(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_UniqueID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_UniqueID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithSceneObjectPath_MetaData[] = {
		{ "Category", "DatasmithConsumerInternal" },
		{ "Comment", "/** Path to UDatasmithScene potentially linked to */" },
		{ "ModuleRelativePath", "Private/DatasmithConsumer.h" },
		{ "ToolTip", "Path to UDatasmithScene potentially linked to" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithSceneObjectPath = { "DatasmithSceneObjectPath", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithConsumer, DatasmithSceneObjectPath), METADATA_PARAMS(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithSceneObjectPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithSceneObjectPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_OutputLevelObjectPath_MetaData[] = {
		{ "Category", "DatasmithConsumerInternal" },
		{ "Comment", "/** Path to ULevel potentially linked to */" },
		{ "ModuleRelativePath", "Private/DatasmithConsumer.h" },
		{ "ToolTip", "Path to ULevel potentially linked to" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_OutputLevelObjectPath = { "OutputLevelObjectPath", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithConsumer, OutputLevelObjectPath), METADATA_PARAMS(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_OutputLevelObjectPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_OutputLevelObjectPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_OutputLevelSoftObject_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "" },
		{ "ModuleRelativePath", "Private/DatasmithConsumer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_OutputLevelSoftObject = { "OutputLevelSoftObject", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithConsumer, OutputLevelSoftObject_DEPRECATED), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_OutputLevelSoftObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_OutputLevelSoftObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithSceneWeakPtr_MetaData[] = {
		{ "ModuleRelativePath", "Private/DatasmithConsumer.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithSceneWeakPtr = { "DatasmithSceneWeakPtr", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithConsumer, DatasmithSceneWeakPtr), Z_Construct_UClass_UDatasmithScene_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithSceneWeakPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithSceneWeakPtr_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDatasmithConsumer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithScene,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_UniqueID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithSceneObjectPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_OutputLevelObjectPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_OutputLevelSoftObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithConsumer_Statics::NewProp_DatasmithSceneWeakPtr,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithConsumer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithConsumer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithConsumer_Statics::ClassParams = {
		&UDatasmithConsumer::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDatasmithConsumer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithConsumer_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithConsumer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithConsumer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithConsumer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithConsumer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithConsumer, 4020985741);
	template<> DATASMITHIMPORTER_API UClass* StaticClass<UDatasmithConsumer>()
	{
		return UDatasmithConsumer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithConsumer(Z_Construct_UClass_UDatasmithConsumer, &UDatasmithConsumer::StaticClass, TEXT("/Script/DatasmithImporter"), TEXT("UDatasmithConsumer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithConsumer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
