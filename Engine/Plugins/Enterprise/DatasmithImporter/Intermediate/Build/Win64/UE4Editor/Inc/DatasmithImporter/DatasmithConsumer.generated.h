// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDatasmithScene;
#ifdef DATASMITHIMPORTER_DatasmithConsumer_generated_h
#error "DatasmithConsumer.generated.h already included, missing '#pragma once' in DatasmithConsumer.h"
#endif
#define DATASMITHIMPORTER_DatasmithConsumer_generated_h

#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDatasmithScene);


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDatasmithScene);


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithConsumer(); \
	friend struct Z_Construct_UClass_UDatasmithConsumer_Statics; \
public: \
	DECLARE_CLASS(UDatasmithConsumer, UDataprepContentConsumer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithConsumer)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithConsumer(); \
	friend struct Z_Construct_UClass_UDatasmithConsumer_Statics; \
public: \
	DECLARE_CLASS(UDatasmithConsumer, UDataprepContentConsumer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithConsumer)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithConsumer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithConsumer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithConsumer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithConsumer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithConsumer(UDatasmithConsumer&&); \
	NO_API UDatasmithConsumer(const UDatasmithConsumer&); \
public:


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithConsumer(UDatasmithConsumer&&); \
	NO_API UDatasmithConsumer(const UDatasmithConsumer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithConsumer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithConsumer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDatasmithConsumer)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DatasmithSceneObjectPath() { return STRUCT_OFFSET(UDatasmithConsumer, DatasmithSceneObjectPath); } \
	FORCEINLINE static uint32 __PPO__OutputLevelObjectPath() { return STRUCT_OFFSET(UDatasmithConsumer, OutputLevelObjectPath); } \
	FORCEINLINE static uint32 __PPO__OutputLevelSoftObject_DEPRECATED() { return STRUCT_OFFSET(UDatasmithConsumer, OutputLevelSoftObject_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__DatasmithSceneWeakPtr() { return STRUCT_OFFSET(UDatasmithConsumer, DatasmithSceneWeakPtr); }


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_26_PROLOG
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_INCLASS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHIMPORTER_API UClass* StaticClass<class UDatasmithConsumer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithConsumer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
