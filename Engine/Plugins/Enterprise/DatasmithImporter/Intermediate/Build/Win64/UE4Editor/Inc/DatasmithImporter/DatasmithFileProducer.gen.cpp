// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithImporter/Private/DatasmithFileProducer.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithFileProducer() {}
// Cross Module References
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDatasmithFileProducer_NoRegister();
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDatasmithFileProducer();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepContentProducer();
	UPackage* Z_Construct_UPackage__Script_DatasmithImporter();
	DATASMITHCONTENT_API UClass* Z_Construct_UClass_UDatasmithScene_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UPackage();
	DATASMITHCONTENT_API UClass* Z_Construct_UClass_UDatasmithOptionsBase_NoRegister();
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDatasmithDirProducer_NoRegister();
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDatasmithDirProducer();
	DATASMITHCONTENT_API UClass* Z_Construct_UClass_UDatasmithCommonTessellationOptions_NoRegister();
// End Cross Module References
	void UDatasmithFileProducer::StaticRegisterNativesUDatasmithFileProducer()
	{
	}
	UClass* Z_Construct_UClass_UDatasmithFileProducer_NoRegister()
	{
		return UDatasmithFileProducer::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithFileProducer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DatasmithScene_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DatasmithScene;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransientPackage_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransientPackage;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TranslatorImportOptions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TranslatorImportOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TranslatorImportOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTranslatorImportOptionsInitialized_MetaData[];
#endif
		static void NewProp_bTranslatorImportOptionsInitialized_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTranslatorImportOptionsInitialized;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithFileProducer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepContentProducer,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithFileProducer_Statics::Class_MetaDataParams[] = {
		{ "DevelopmentStatus", "Experimental" },
		{ "HideCategories", "DatasmithProducer_Internal" },
		{ "IncludePath", "DatasmithFileProducer.h" },
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_FilePath_MetaData[] = {
		{ "Category", "DatasmithProducer" },
		{ "Comment", "// End UDataprepContentProducer overrides\n" },
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
		{ "ToolTip", "End UDataprepContentProducer overrides" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithFileProducer, FilePath), METADATA_PARAMS(Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_FilePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_DatasmithScene_MetaData[] = {
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_DatasmithScene = { "DatasmithScene", nullptr, (EPropertyFlags)0x0040000000202000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithFileProducer, DatasmithScene), Z_Construct_UClass_UDatasmithScene_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_DatasmithScene_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_DatasmithScene_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TransientPackage_MetaData[] = {
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TransientPackage = { "TransientPackage", nullptr, (EPropertyFlags)0x0040000000202000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithFileProducer, TransientPackage), Z_Construct_UClass_UPackage, METADATA_PARAMS(Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TransientPackage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TransientPackage_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TranslatorImportOptions_Inner = { "TranslatorImportOptions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDatasmithOptionsBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TranslatorImportOptions_MetaData[] = {
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TranslatorImportOptions = { "TranslatorImportOptions", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithFileProducer, TranslatorImportOptions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TranslatorImportOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TranslatorImportOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_bTranslatorImportOptionsInitialized_MetaData[] = {
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
	};
#endif
	void Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_bTranslatorImportOptionsInitialized_SetBit(void* Obj)
	{
		((UDatasmithFileProducer*)Obj)->bTranslatorImportOptionsInitialized = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_bTranslatorImportOptionsInitialized = { "bTranslatorImportOptionsInitialized", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDatasmithFileProducer), &Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_bTranslatorImportOptionsInitialized_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_bTranslatorImportOptionsInitialized_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_bTranslatorImportOptionsInitialized_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDatasmithFileProducer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_FilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_DatasmithScene,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TransientPackage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TranslatorImportOptions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_TranslatorImportOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithFileProducer_Statics::NewProp_bTranslatorImportOptionsInitialized,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithFileProducer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithFileProducer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithFileProducer_Statics::ClassParams = {
		&UDatasmithFileProducer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDatasmithFileProducer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithFileProducer_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithFileProducer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithFileProducer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithFileProducer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithFileProducer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithFileProducer, 3761981483);
	template<> DATASMITHIMPORTER_API UClass* StaticClass<UDatasmithFileProducer>()
	{
		return UDatasmithFileProducer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithFileProducer(Z_Construct_UClass_UDatasmithFileProducer, &UDatasmithFileProducer::StaticClass, TEXT("/Script/DatasmithImporter"), TEXT("UDatasmithFileProducer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithFileProducer);
	void UDatasmithDirProducer::StaticRegisterNativesUDatasmithDirProducer()
	{
	}
	UClass* Z_Construct_UClass_UDatasmithDirProducer_NoRegister()
	{
		return UDatasmithDirProducer::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithDirProducer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FolderPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FolderPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtensionString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ExtensionString;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecursive_MetaData[];
#endif
		static void NewProp_bRecursive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecursive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileProducer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FileProducer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TessellationOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TessellationOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithDirProducer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepContentProducer,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDirProducer_Statics::Class_MetaDataParams[] = {
		{ "DevelopmentStatus", "Experimental" },
		{ "HideCategories", "DatasmithDirProducer_Internal" },
		{ "IncludePath", "DatasmithFileProducer.h" },
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_FolderPath_MetaData[] = {
		{ "Category", "DatasmithDirProducer_Internal" },
		{ "Comment", "// The folder were datasmith will look for files to import\n" },
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
		{ "ToolTip", "The folder were datasmith will look for files to import" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_FolderPath = { "FolderPath", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithDirProducer, FolderPath), METADATA_PARAMS(Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_FolderPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_FolderPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_ExtensionString_MetaData[] = {
		{ "Category", "DatasmithDirProducer" },
		{ "Comment", "// Semi-column separated string containing the extensions to consider. By default, set to * to get all extensions.\n" },
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
		{ "ToolTip", "Semi-column separated string containing the extensions to consider. By default, set to * to get all extensions." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_ExtensionString = { "ExtensionString", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithDirProducer, ExtensionString), METADATA_PARAMS(Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_ExtensionString_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_ExtensionString_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_bRecursive_MetaData[] = {
		{ "Category", "DatasmithDirProducer" },
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
		{ "ToolTip", "If checked, sub-directories will be traversed" },
	};
#endif
	void Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_bRecursive_SetBit(void* Obj)
	{
		((UDatasmithDirProducer*)Obj)->bRecursive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_bRecursive = { "bRecursive", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDatasmithDirProducer), &Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_bRecursive_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_bRecursive_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_bRecursive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_FileProducer_MetaData[] = {
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_FileProducer = { "FileProducer", nullptr, (EPropertyFlags)0x0040000000202000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithDirProducer, FileProducer), Z_Construct_UClass_UDatasmithFileProducer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_FileProducer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_FileProducer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_TessellationOptions_MetaData[] = {
		{ "ModuleRelativePath", "Private/DatasmithFileProducer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_TessellationOptions = { "TessellationOptions", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithDirProducer, TessellationOptions), Z_Construct_UClass_UDatasmithCommonTessellationOptions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_TessellationOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_TessellationOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDatasmithDirProducer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_FolderPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_ExtensionString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_bRecursive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_FileProducer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDirProducer_Statics::NewProp_TessellationOptions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithDirProducer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithDirProducer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithDirProducer_Statics::ClassParams = {
		&UDatasmithDirProducer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDatasmithDirProducer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDirProducer_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithDirProducer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDirProducer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithDirProducer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithDirProducer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithDirProducer, 1754226281);
	template<> DATASMITHIMPORTER_API UClass* StaticClass<UDatasmithDirProducer>()
	{
		return UDatasmithDirProducer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithDirProducer(Z_Construct_UClass_UDatasmithDirProducer, &UDatasmithDirProducer::StaticClass, TEXT("/Script/DatasmithImporter"), TEXT("UDatasmithDirProducer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithDirProducer);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDatasmithDirProducer)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
