// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHIMPORTER_DatasmithDataprepOperation_generated_h
#error "DatasmithDataprepOperation.generated.h already included, missing '#pragma once' in DatasmithDataprepOperation.h"
#endif
#define DATASMITHIMPORTER_DatasmithDataprepOperation_generated_h

#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepSetupStaticLightingOperation(); \
	friend struct Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics; \
public: \
	DECLARE_CLASS(UDataprepSetupStaticLightingOperation, UDataprepOperation, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDataprepSetupStaticLightingOperation)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepSetupStaticLightingOperation(); \
	friend struct Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics; \
public: \
	DECLARE_CLASS(UDataprepSetupStaticLightingOperation, UDataprepOperation, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDataprepSetupStaticLightingOperation)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepSetupStaticLightingOperation(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepSetupStaticLightingOperation) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepSetupStaticLightingOperation); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepSetupStaticLightingOperation); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepSetupStaticLightingOperation(UDataprepSetupStaticLightingOperation&&); \
	NO_API UDataprepSetupStaticLightingOperation(const UDataprepSetupStaticLightingOperation&); \
public:


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepSetupStaticLightingOperation(UDataprepSetupStaticLightingOperation&&); \
	NO_API UDataprepSetupStaticLightingOperation(const UDataprepSetupStaticLightingOperation&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepSetupStaticLightingOperation); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepSetupStaticLightingOperation); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepSetupStaticLightingOperation)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_11_PROLOG
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_INCLASS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHIMPORTER_API UClass* StaticClass<class UDataprepSetupStaticLightingOperation>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepOperation_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
