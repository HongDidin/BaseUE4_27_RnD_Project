// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHIMPORTER_DatasmithImportFactory_generated_h
#error "DatasmithImportFactory.generated.h already included, missing '#pragma once' in DatasmithImportFactory.h"
#endif
#define DATASMITHIMPORTER_DatasmithImportFactory_generated_h

#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithImportFactory(); \
	friend struct Z_Construct_UClass_UDatasmithImportFactory_Statics; \
public: \
	DECLARE_CLASS(UDatasmithImportFactory, USceneImportFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithImportFactory)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithImportFactory(); \
	friend struct Z_Construct_UClass_UDatasmithImportFactory_Statics; \
public: \
	DECLARE_CLASS(UDatasmithImportFactory, USceneImportFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithImportFactory)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithImportFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithImportFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithImportFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithImportFactory(UDatasmithImportFactory&&); \
	NO_API UDatasmithImportFactory(const UDatasmithImportFactory&); \
public:


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithImportFactory(UDatasmithImportFactory&&); \
	NO_API UDatasmithImportFactory(const UDatasmithImportFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithImportFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDatasmithImportFactory)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_30_PROLOG
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_INCLASS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHIMPORTER_API UClass* StaticClass<class UDatasmithImportFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Public_DatasmithImportFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
