// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHIMPORTER_DatasmithFileProducer_generated_h
#error "DatasmithFileProducer.generated.h already included, missing '#pragma once' in DatasmithFileProducer.h"
#endif
#define DATASMITHIMPORTER_DatasmithFileProducer_generated_h

#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithFileProducer(); \
	friend struct Z_Construct_UClass_UDatasmithFileProducer_Statics; \
public: \
	DECLARE_CLASS(UDatasmithFileProducer, UDataprepContentProducer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithFileProducer)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithFileProducer(); \
	friend struct Z_Construct_UClass_UDatasmithFileProducer_Statics; \
public: \
	DECLARE_CLASS(UDatasmithFileProducer, UDataprepContentProducer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithFileProducer)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithFileProducer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithFileProducer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithFileProducer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithFileProducer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithFileProducer(UDatasmithFileProducer&&); \
	NO_API UDatasmithFileProducer(const UDatasmithFileProducer&); \
public:


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithFileProducer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithFileProducer(UDatasmithFileProducer&&); \
	NO_API UDatasmithFileProducer(const UDatasmithFileProducer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithFileProducer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithFileProducer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithFileProducer)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FilePath() { return STRUCT_OFFSET(UDatasmithFileProducer, FilePath); } \
	FORCEINLINE static uint32 __PPO__DatasmithScene() { return STRUCT_OFFSET(UDatasmithFileProducer, DatasmithScene); } \
	FORCEINLINE static uint32 __PPO__TransientPackage() { return STRUCT_OFFSET(UDatasmithFileProducer, TransientPackage); } \
	FORCEINLINE static uint32 __PPO__TranslatorImportOptions() { return STRUCT_OFFSET(UDatasmithFileProducer, TranslatorImportOptions); } \
	FORCEINLINE static uint32 __PPO__bTranslatorImportOptionsInitialized() { return STRUCT_OFFSET(UDatasmithFileProducer, bTranslatorImportOptionsInitialized); }


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_22_PROLOG
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_INCLASS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHIMPORTER_API UClass* StaticClass<class UDatasmithFileProducer>();

#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UDatasmithDirProducer, NO_API)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithDirProducer(); \
	friend struct Z_Construct_UClass_UDatasmithDirProducer_Statics; \
public: \
	DECLARE_CLASS(UDatasmithDirProducer, UDataprepContentProducer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithDirProducer) \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_ARCHIVESERIALIZER


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithDirProducer(); \
	friend struct Z_Construct_UClass_UDatasmithDirProducer_Statics; \
public: \
	DECLARE_CLASS(UDatasmithDirProducer, UDataprepContentProducer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithDirProducer) \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_ARCHIVESERIALIZER


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithDirProducer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithDirProducer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithDirProducer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithDirProducer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithDirProducer(UDatasmithDirProducer&&); \
	NO_API UDatasmithDirProducer(const UDatasmithDirProducer&); \
public:


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithDirProducer(UDatasmithDirProducer&&); \
	NO_API UDatasmithDirProducer(const UDatasmithDirProducer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithDirProducer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithDirProducer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDatasmithDirProducer)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FolderPath() { return STRUCT_OFFSET(UDatasmithDirProducer, FolderPath); } \
	FORCEINLINE static uint32 __PPO__ExtensionString() { return STRUCT_OFFSET(UDatasmithDirProducer, ExtensionString); } \
	FORCEINLINE static uint32 __PPO__bRecursive() { return STRUCT_OFFSET(UDatasmithDirProducer, bRecursive); } \
	FORCEINLINE static uint32 __PPO__FileProducer() { return STRUCT_OFFSET(UDatasmithDirProducer, FileProducer); } \
	FORCEINLINE static uint32 __PPO__TessellationOptions() { return STRUCT_OFFSET(UDatasmithDirProducer, TessellationOptions); }


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_140_PROLOG
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_INCLASS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h_143_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHIMPORTER_API UClass* StaticClass<class UDatasmithDirProducer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithFileProducer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
