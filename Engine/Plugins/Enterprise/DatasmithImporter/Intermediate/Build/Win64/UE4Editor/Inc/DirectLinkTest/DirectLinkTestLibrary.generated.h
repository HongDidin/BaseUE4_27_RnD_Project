// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DIRECTLINKTEST_DirectLinkTestLibrary_generated_h
#error "DirectLinkTestLibrary.generated.h already included, missing '#pragma once' in DirectLinkTestLibrary.h"
#endif
#define DIRECTLINKTEST_DirectLinkTestLibrary_generated_h

#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDeleteAllEndpoint); \
	DECLARE_FUNCTION(execAddPublicDestination); \
	DECLARE_FUNCTION(execAddPublicSource); \
	DECLARE_FUNCTION(execDeleteEndpoint); \
	DECLARE_FUNCTION(execMakeEndpoint); \
	DECLARE_FUNCTION(execDumpReceivedScene); \
	DECLARE_FUNCTION(execSendScene); \
	DECLARE_FUNCTION(execStopSender); \
	DECLARE_FUNCTION(execSetupSender); \
	DECLARE_FUNCTION(execStartSender); \
	DECLARE_FUNCTION(execStopReceiver); \
	DECLARE_FUNCTION(execSetupReceiver); \
	DECLARE_FUNCTION(execStartReceiver); \
	DECLARE_FUNCTION(execTestParameters);


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDeleteAllEndpoint); \
	DECLARE_FUNCTION(execAddPublicDestination); \
	DECLARE_FUNCTION(execAddPublicSource); \
	DECLARE_FUNCTION(execDeleteEndpoint); \
	DECLARE_FUNCTION(execMakeEndpoint); \
	DECLARE_FUNCTION(execDumpReceivedScene); \
	DECLARE_FUNCTION(execSendScene); \
	DECLARE_FUNCTION(execStopSender); \
	DECLARE_FUNCTION(execSetupSender); \
	DECLARE_FUNCTION(execStartSender); \
	DECLARE_FUNCTION(execStopReceiver); \
	DECLARE_FUNCTION(execSetupReceiver); \
	DECLARE_FUNCTION(execStartReceiver); \
	DECLARE_FUNCTION(execTestParameters);


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDirectLinkTestLibrary(); \
	friend struct Z_Construct_UClass_UDirectLinkTestLibrary_Statics; \
public: \
	DECLARE_CLASS(UDirectLinkTestLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DirectLinkTest"), NO_API) \
	DECLARE_SERIALIZER(UDirectLinkTestLibrary)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUDirectLinkTestLibrary(); \
	friend struct Z_Construct_UClass_UDirectLinkTestLibrary_Statics; \
public: \
	DECLARE_CLASS(UDirectLinkTestLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DirectLinkTest"), NO_API) \
	DECLARE_SERIALIZER(UDirectLinkTestLibrary)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDirectLinkTestLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDirectLinkTestLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDirectLinkTestLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDirectLinkTestLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDirectLinkTestLibrary(UDirectLinkTestLibrary&&); \
	NO_API UDirectLinkTestLibrary(const UDirectLinkTestLibrary&); \
public:


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDirectLinkTestLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDirectLinkTestLibrary(UDirectLinkTestLibrary&&); \
	NO_API UDirectLinkTestLibrary(const UDirectLinkTestLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDirectLinkTestLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDirectLinkTestLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDirectLinkTestLibrary)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_12_PROLOG
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_INCLASS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DIRECTLINKTEST_API UClass* StaticClass<class UDirectLinkTestLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithImporter_Source_DirectLinkTest_Public_DirectLinkTestLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
