// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithImporter/Public/DatasmithImporterEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithImporterEditorSettings() {}
// Cross Module References
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDatasmithImporterEditorSettings_NoRegister();
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDatasmithImporterEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DatasmithImporter();
// End Cross Module References
	void UDatasmithImporterEditorSettings::StaticRegisterNativesUDatasmithImporterEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UDatasmithImporterEditorSettings_NoRegister()
	{
		return UDatasmithImporterEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOfflineImporter_MetaData[];
#endif
		static void NewProp_bOfflineImporter_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOfflineImporter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DatasmithImporterEditorSettings.h" },
		{ "ModuleRelativePath", "Public/DatasmithImporterEditorSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::NewProp_bOfflineImporter_MetaData[] = {
		{ "ModuleRelativePath", "Public/DatasmithImporterEditorSettings.h" },
	};
#endif
	void Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::NewProp_bOfflineImporter_SetBit(void* Obj)
	{
		((UDatasmithImporterEditorSettings*)Obj)->bOfflineImporter = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::NewProp_bOfflineImporter = { "bOfflineImporter", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDatasmithImporterEditorSettings), &Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::NewProp_bOfflineImporter_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::NewProp_bOfflineImporter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::NewProp_bOfflineImporter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::NewProp_bOfflineImporter,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithImporterEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::ClassParams = {
		&UDatasmithImporterEditorSettings::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithImporterEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithImporterEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithImporterEditorSettings, 2463518014);
	template<> DATASMITHIMPORTER_API UClass* StaticClass<UDatasmithImporterEditorSettings>()
	{
		return UDatasmithImporterEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithImporterEditorSettings(Z_Construct_UClass_UDatasmithImporterEditorSettings, &UDatasmithImporterEditorSettings::StaticClass, TEXT("/Script/DatasmithImporter"), TEXT("UDatasmithImporterEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithImporterEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
