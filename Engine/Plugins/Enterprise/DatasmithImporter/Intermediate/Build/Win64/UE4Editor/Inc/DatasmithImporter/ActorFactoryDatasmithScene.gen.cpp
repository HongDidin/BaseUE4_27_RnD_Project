// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithImporter/Public/ActorFactoryDatasmithScene.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorFactoryDatasmithScene() {}
// Cross Module References
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UActorFactoryDatasmithScene_NoRegister();
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UActorFactoryDatasmithScene();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_DatasmithImporter();
// End Cross Module References
	void UActorFactoryDatasmithScene::StaticRegisterNativesUActorFactoryDatasmithScene()
	{
	}
	UClass* Z_Construct_UClass_UActorFactoryDatasmithScene_NoRegister()
	{
		return UActorFactoryDatasmithScene::StaticClass();
	}
	struct Z_Construct_UClass_UActorFactoryDatasmithScene_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorFactoryDatasmithScene_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorFactoryDatasmithScene_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "ActorFactoryDatasmithScene.h" },
		{ "ModuleRelativePath", "Public/ActorFactoryDatasmithScene.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorFactoryDatasmithScene_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorFactoryDatasmithScene>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorFactoryDatasmithScene_Statics::ClassParams = {
		&UActorFactoryDatasmithScene::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UActorFactoryDatasmithScene_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorFactoryDatasmithScene_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorFactoryDatasmithScene()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorFactoryDatasmithScene_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorFactoryDatasmithScene, 2584550800);
	template<> DATASMITHIMPORTER_API UClass* StaticClass<UActorFactoryDatasmithScene>()
	{
		return UActorFactoryDatasmithScene::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorFactoryDatasmithScene(Z_Construct_UClass_UActorFactoryDatasmithScene, &UActorFactoryDatasmithScene::StaticClass, TEXT("/Script/DatasmithImporter"), TEXT("UActorFactoryDatasmithScene"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorFactoryDatasmithScene);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
