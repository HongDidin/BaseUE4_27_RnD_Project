// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHIMPORTER_DatasmithDataprepFetcher_generated_h
#error "DatasmithDataprepFetcher.generated.h already included, missing '#pragma once' in DatasmithDataprepFetcher.h"
#endif
#define DATASMITHIMPORTER_DatasmithDataprepFetcher_generated_h

#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithStringMetadataValueFetcher(); \
	friend struct Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics; \
public: \
	DECLARE_CLASS(UDatasmithStringMetadataValueFetcher, UDataprepStringFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithStringMetadataValueFetcher)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithStringMetadataValueFetcher(); \
	friend struct Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics; \
public: \
	DECLARE_CLASS(UDatasmithStringMetadataValueFetcher, UDataprepStringFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithImporter"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithStringMetadataValueFetcher)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithStringMetadataValueFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithStringMetadataValueFetcher) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithStringMetadataValueFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithStringMetadataValueFetcher); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithStringMetadataValueFetcher(UDatasmithStringMetadataValueFetcher&&); \
	NO_API UDatasmithStringMetadataValueFetcher(const UDatasmithStringMetadataValueFetcher&); \
public:


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithStringMetadataValueFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithStringMetadataValueFetcher(UDatasmithStringMetadataValueFetcher&&); \
	NO_API UDatasmithStringMetadataValueFetcher(const UDatasmithStringMetadataValueFetcher&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithStringMetadataValueFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithStringMetadataValueFetcher); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithStringMetadataValueFetcher)


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_11_PROLOG
#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_INCLASS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHIMPORTER_API UClass* StaticClass<class UDatasmithStringMetadataValueFetcher>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithImporter_Source_DatasmithImporter_Private_DatasmithDataprepFetcher_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
