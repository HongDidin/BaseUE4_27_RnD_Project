// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithImporter/Private/DatasmithDataprepOperation.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithDataprepOperation() {}
// Cross Module References
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDataprepSetupStaticLightingOperation_NoRegister();
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDataprepSetupStaticLightingOperation();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepOperation();
	UPackage* Z_Construct_UPackage__Script_DatasmithImporter();
// End Cross Module References
	void UDataprepSetupStaticLightingOperation::StaticRegisterNativesUDataprepSetupStaticLightingOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetupStaticLightingOperation_NoRegister()
	{
		return UDataprepSetupStaticLightingOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableLightmapUVGeneration_MetaData[];
#endif
		static void NewProp_bEnableLightmapUVGeneration_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableLightmapUVGeneration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LightmapResolutionIdealRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LightmapResolutionIdealRatio;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "LightmapOptions" },
		{ "DevelopmentStatus", "Experimental" },
		{ "DisplayName", "Setup Static Lighting" },
		{ "IncludePath", "DatasmithDataprepOperation.h" },
		{ "ModuleRelativePath", "Private/DatasmithDataprepOperation.h" },
		{ "ToolTip", "For each static mesh to process, setup the settings to enable lightmap UVs generation and compute the lightmap resolution." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_bEnableLightmapUVGeneration_MetaData[] = {
		{ "Category", "LightmapOptions" },
		{ "DisplayName", "Enable Lightmap UV Generation" },
		{ "ModuleRelativePath", "Private/DatasmithDataprepOperation.h" },
		{ "ToolTip", "Enable the lightmap UV generation." },
	};
#endif
	void Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_bEnableLightmapUVGeneration_SetBit(void* Obj)
	{
		((UDataprepSetupStaticLightingOperation*)Obj)->bEnableLightmapUVGeneration = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_bEnableLightmapUVGeneration = { "bEnableLightmapUVGeneration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepSetupStaticLightingOperation), &Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_bEnableLightmapUVGeneration_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_bEnableLightmapUVGeneration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_bEnableLightmapUVGeneration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_LightmapResolutionIdealRatio_MetaData[] = {
		{ "Category", "LightmapOptions" },
		{ "DisplayName", "Resolution Ideal Ratio" },
		{ "ModuleRelativePath", "Private/DatasmithDataprepOperation.h" },
		{ "ToolTip", "The ratio used to compute the resolution of the lightmap." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_LightmapResolutionIdealRatio = { "LightmapResolutionIdealRatio", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetupStaticLightingOperation, LightmapResolutionIdealRatio), METADATA_PARAMS(Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_LightmapResolutionIdealRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_LightmapResolutionIdealRatio_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_bEnableLightmapUVGeneration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::NewProp_LightmapResolutionIdealRatio,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetupStaticLightingOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::ClassParams = {
		&UDataprepSetupStaticLightingOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetupStaticLightingOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetupStaticLightingOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetupStaticLightingOperation, 2069255606);
	template<> DATASMITHIMPORTER_API UClass* StaticClass<UDataprepSetupStaticLightingOperation>()
	{
		return UDataprepSetupStaticLightingOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetupStaticLightingOperation(Z_Construct_UClass_UDataprepSetupStaticLightingOperation, &UDataprepSetupStaticLightingOperation::StaticClass, TEXT("/Script/DatasmithImporter"), TEXT("UDataprepSetupStaticLightingOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetupStaticLightingOperation);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
