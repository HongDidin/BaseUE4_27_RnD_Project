// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithImporter/Private/DatasmithDataprepFetcher.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithDataprepFetcher() {}
// Cross Module References
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_NoRegister();
	DATASMITHIMPORTER_API UClass* Z_Construct_UClass_UDatasmithStringMetadataValueFetcher();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepStringFetcher();
	UPackage* Z_Construct_UPackage__Script_DatasmithImporter();
// End Cross Module References
	void UDatasmithStringMetadataValueFetcher::StaticRegisterNativesUDatasmithStringMetadataValueFetcher()
	{
	}
	UClass* Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_NoRegister()
	{
		return UDatasmithStringMetadataValueFetcher::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepStringFetcher,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Metadata Value" },
		{ "IncludePath", "DatasmithDataprepFetcher.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Private/DatasmithDataprepFetcher.h" },
		{ "ToolTip", "Filter objects based on the key value of their metadata." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::NewProp_Key_MetaData[] = {
		{ "Category", "Key" },
		{ "Comment", "// The key for the for the string\n" },
		{ "ModuleRelativePath", "Private/DatasmithDataprepFetcher.h" },
		{ "ToolTip", "The key for the for the string" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithStringMetadataValueFetcher, Key), METADATA_PARAMS(Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::NewProp_Key,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithStringMetadataValueFetcher>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::ClassParams = {
		&UDatasmithStringMetadataValueFetcher::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithStringMetadataValueFetcher()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithStringMetadataValueFetcher_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithStringMetadataValueFetcher, 267056728);
	template<> DATASMITHIMPORTER_API UClass* StaticClass<UDatasmithStringMetadataValueFetcher>()
	{
		return UDatasmithStringMetadataValueFetcher::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithStringMetadataValueFetcher(Z_Construct_UClass_UDatasmithStringMetadataValueFetcher, &UDatasmithStringMetadataValueFetcher::StaticClass, TEXT("/Script/DatasmithImporter"), TEXT("UDatasmithStringMetadataValueFetcher"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithStringMetadataValueFetcher);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
