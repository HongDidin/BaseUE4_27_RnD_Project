// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHVREDTRANSLATOR_DatasmithVREDImportData_generated_h
#error "DatasmithVREDImportData.generated.h already included, missing '#pragma once' in DatasmithVREDImportData.h"
#endif
#define DATASMITHVREDTRANSLATOR_DatasmithVREDImportData_generated_h

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_163_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariant_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariant>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_151_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantLight>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_139_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantTransform>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_127_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantMaterial>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_106_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantSet>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_94_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantGeometry>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_85_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantCamera>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_76_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantLightOption>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_64_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantTransformOption>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_55_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantMaterialOption>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_40_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantCameraOption>();

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h_25_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics; \
	DATASMITHVREDTRANSLATOR_API static class UScriptStruct* StaticStruct();


template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<struct FVREDCppVariantGeometryOption>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportData_h


#define FOREACH_ENUM_EVREDCPPVARIANTTYPE(op) \
	op(EVREDCppVariantType::Unsupported) \
	op(EVREDCppVariantType::Camera) \
	op(EVREDCppVariantType::Geometry) \
	op(EVREDCppVariantType::VariantSet) \
	op(EVREDCppVariantType::Material) \
	op(EVREDCppVariantType::Transform) \
	op(EVREDCppVariantType::Light) 

enum class EVREDCppVariantType : uint8;
template<> DATASMITHVREDTRANSLATOR_API UEnum* StaticEnum<EVREDCppVariantType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
