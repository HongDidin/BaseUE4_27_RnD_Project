// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHDELTAGENTRANSLATOR_DatasmithDeltaGenImportOptions_generated_h
#error "DatasmithDeltaGenImportOptions.generated.h already included, missing '#pragma once' in DatasmithDeltaGenImportOptions.h"
#endif
#define DATASMITHDELTAGENTRANSLATOR_DatasmithDeltaGenImportOptions_generated_h

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithDeltaGenImportOptions(); \
	friend struct Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDatasmithDeltaGenImportOptions, UDatasmithFBXImportOptions, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithDeltaGenTranslator"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithDeltaGenImportOptions)


#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithDeltaGenImportOptions(); \
	friend struct Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDatasmithDeltaGenImportOptions, UDatasmithFBXImportOptions, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithDeltaGenTranslator"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithDeltaGenImportOptions)


#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithDeltaGenImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithDeltaGenImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithDeltaGenImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithDeltaGenImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithDeltaGenImportOptions(UDatasmithDeltaGenImportOptions&&); \
	NO_API UDatasmithDeltaGenImportOptions(const UDatasmithDeltaGenImportOptions&); \
public:


#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithDeltaGenImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithDeltaGenImportOptions(UDatasmithDeltaGenImportOptions&&); \
	NO_API UDatasmithDeltaGenImportOptions(const UDatasmithDeltaGenImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithDeltaGenImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithDeltaGenImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithDeltaGenImportOptions)


#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_25_PROLOG
#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_INCLASS \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h_28_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class DatasmithDeltaGenImportOptions."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHDELTAGENTRANSLATOR_API UClass* StaticClass<class UDatasmithDeltaGenImportOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithDeltaGenTranslator_Private_DatasmithDeltaGenImportOptions_h


#define FOREACH_ENUM_ESHADOWTEXTUREMODE(op) \
	op(EShadowTextureMode::Ignore) \
	op(EShadowTextureMode::AmbientOcclusion) \
	op(EShadowTextureMode::Multiplier) \
	op(EShadowTextureMode::AmbientOcclusionAndMultiplier) 

enum class EShadowTextureMode : uint8;
template<> DATASMITHDELTAGENTRANSLATOR_API UEnum* StaticEnum<EShadowTextureMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
