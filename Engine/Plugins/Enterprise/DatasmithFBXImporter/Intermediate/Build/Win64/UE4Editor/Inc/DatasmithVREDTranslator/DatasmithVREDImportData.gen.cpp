// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithVREDTranslator/Private/DatasmithVREDImportData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithVREDImportData() {}
// Cross Module References
	DATASMITHVREDTRANSLATOR_API UEnum* Z_Construct_UEnum_DatasmithVREDTranslator_EVREDCppVariantType();
	UPackage* Z_Construct_UPackage__Script_DatasmithVREDTranslator();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariant();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantCamera();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantGeometry();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantMaterial();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantSet();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantTransform();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantLight();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantLightOption();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantTransformOption();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption();
	DATASMITHVREDTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantCameraOption();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
// End Cross Module References
	static UEnum* EVREDCppVariantType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DatasmithVREDTranslator_EVREDCppVariantType, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("EVREDCppVariantType"));
		}
		return Singleton;
	}
	template<> DATASMITHVREDTRANSLATOR_API UEnum* StaticEnum<EVREDCppVariantType>()
	{
		return EVREDCppVariantType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVREDCppVariantType(EVREDCppVariantType_StaticEnum, TEXT("/Script/DatasmithVREDTranslator"), TEXT("EVREDCppVariantType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DatasmithVREDTranslator_EVREDCppVariantType_Hash() { return 3299197292U; }
	UEnum* Z_Construct_UEnum_DatasmithVREDTranslator_EVREDCppVariantType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVREDCppVariantType"), 0, Get_Z_Construct_UEnum_DatasmithVREDTranslator_EVREDCppVariantType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVREDCppVariantType::Unsupported", (int64)EVREDCppVariantType::Unsupported },
				{ "EVREDCppVariantType::Camera", (int64)EVREDCppVariantType::Camera },
				{ "EVREDCppVariantType::Geometry", (int64)EVREDCppVariantType::Geometry },
				{ "EVREDCppVariantType::VariantSet", (int64)EVREDCppVariantType::VariantSet },
				{ "EVREDCppVariantType::Material", (int64)EVREDCppVariantType::Material },
				{ "EVREDCppVariantType::Transform", (int64)EVREDCppVariantType::Transform },
				{ "EVREDCppVariantType::Light", (int64)EVREDCppVariantType::Light },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Camera.Name", "EVREDCppVariantType::Camera" },
				{ "Geometry.Name", "EVREDCppVariantType::Geometry" },
				{ "Light.Name", "EVREDCppVariantType::Light" },
				{ "Material.Name", "EVREDCppVariantType::Material" },
				{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
				{ "Transform.Name", "EVREDCppVariantType::Transform" },
				{ "Unsupported.Name", "EVREDCppVariantType::Unsupported" },
				{ "VariantSet.Name", "EVREDCppVariantType::VariantSet" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
				nullptr,
				"EVREDCppVariantType",
				"EVREDCppVariantType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FVREDCppVariant>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FVREDCppVariant cannot be polymorphic unless super FTableRowBase is polymorphic");

class UScriptStruct* FVREDCppVariant::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariant_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariant, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariant"), sizeof(FVREDCppVariant), Get_Z_Construct_UScriptStruct_FVREDCppVariant_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariant>()
{
	return FVREDCppVariant::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariant(FVREDCppVariant::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariant"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariant
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariant()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariant>(FName(TEXT("VREDCppVariant")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariant;
	struct Z_Construct_UScriptStruct_FVREDCppVariant_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Camera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Geometry_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Geometry;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Material;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VariantSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VariantSet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Light_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Light;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariant_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariant>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariant, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariant, Type), Z_Construct_UEnum_DatasmithVREDTranslator_EVREDCppVariantType, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Camera_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Camera = { "Camera", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariant, Camera), Z_Construct_UScriptStruct_FVREDCppVariantCamera, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Camera_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Camera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Geometry_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Geometry = { "Geometry", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariant, Geometry), Z_Construct_UScriptStruct_FVREDCppVariantGeometry, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Geometry_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Geometry_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Material_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariant, Material), Z_Construct_UScriptStruct_FVREDCppVariantMaterial, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Material_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_VariantSet_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_VariantSet = { "VariantSet", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariant, VariantSet), Z_Construct_UScriptStruct_FVREDCppVariantSet, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_VariantSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_VariantSet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariant, Transform), Z_Construct_UScriptStruct_FVREDCppVariantTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Light_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Light = { "Light", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariant, Light), Z_Construct_UScriptStruct_FVREDCppVariantLight, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Light_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Light_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariant_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Camera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Geometry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_VariantSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariant_Statics::NewProp_Light,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariant_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"VREDCppVariant",
		sizeof(FVREDCppVariant),
		alignof(FVREDCppVariant),
		Z_Construct_UScriptStruct_FVREDCppVariant_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariant_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariant()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariant_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariant"), sizeof(FVREDCppVariant), Get_Z_Construct_UScriptStruct_FVREDCppVariant_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariant_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariant_Hash() { return 3504792303U; }
class UScriptStruct* FVREDCppVariantLight::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantLight_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantLight, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantLight"), sizeof(FVREDCppVariantLight), Get_Z_Construct_UScriptStruct_FVREDCppVariantLight_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantLight>()
{
	return FVREDCppVariantLight::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantLight(FVREDCppVariantLight::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantLight"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantLight
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantLight()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantLight>(FName(TEXT("VREDCppVariantLight")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantLight;
	struct Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TargetNodes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetNodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetNodes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Options_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Options_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantLight>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_TargetNodes_Inner = { "TargetNodes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_TargetNodes_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_TargetNodes = { "TargetNodes", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantLight, TargetNodes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_TargetNodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_TargetNodes_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_Options_Inner = { "Options", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVREDCppVariantLightOption, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_Options_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantLight, Options), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_Options_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_Options_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_TargetNodes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_TargetNodes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_Options_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::NewProp_Options,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantLight",
		sizeof(FVREDCppVariantLight),
		alignof(FVREDCppVariantLight),
		Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantLight()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantLight_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantLight"), sizeof(FVREDCppVariantLight), Get_Z_Construct_UScriptStruct_FVREDCppVariantLight_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantLight_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantLight_Hash() { return 4291679382U; }
class UScriptStruct* FVREDCppVariantTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantTransform, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantTransform"), sizeof(FVREDCppVariantTransform), Get_Z_Construct_UScriptStruct_FVREDCppVariantTransform_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantTransform>()
{
	return FVREDCppVariantTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantTransform(FVREDCppVariantTransform::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantTransform"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantTransform
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantTransform()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantTransform>(FName(TEXT("VREDCppVariantTransform")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantTransform;
	struct Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TargetNodes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetNodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetNodes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Options_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Options_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantTransform>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_TargetNodes_Inner = { "TargetNodes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_TargetNodes_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_TargetNodes = { "TargetNodes", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantTransform, TargetNodes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_TargetNodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_TargetNodes_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_Options_Inner = { "Options", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVREDCppVariantTransformOption, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_Options_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantTransform, Options), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_Options_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_Options_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_TargetNodes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_TargetNodes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_Options_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::NewProp_Options,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantTransform",
		sizeof(FVREDCppVariantTransform),
		alignof(FVREDCppVariantTransform),
		Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantTransform"), sizeof(FVREDCppVariantTransform), Get_Z_Construct_UScriptStruct_FVREDCppVariantTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantTransform_Hash() { return 1834241206U; }
class UScriptStruct* FVREDCppVariantMaterial::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantMaterial, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantMaterial"), sizeof(FVREDCppVariantMaterial), Get_Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantMaterial>()
{
	return FVREDCppVariantMaterial::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantMaterial(FVREDCppVariantMaterial::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantMaterial"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantMaterial
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantMaterial()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantMaterial>(FName(TEXT("VREDCppVariantMaterial")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantMaterial;
	struct Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TargetNodes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetNodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetNodes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Options_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Options_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantMaterial>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_TargetNodes_Inner = { "TargetNodes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_TargetNodes_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_TargetNodes = { "TargetNodes", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantMaterial, TargetNodes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_TargetNodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_TargetNodes_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_Options_Inner = { "Options", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_Options_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantMaterial, Options), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_Options_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_Options_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_TargetNodes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_TargetNodes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_Options_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::NewProp_Options,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantMaterial",
		sizeof(FVREDCppVariantMaterial),
		alignof(FVREDCppVariantMaterial),
		Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantMaterial()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantMaterial"), sizeof(FVREDCppVariantMaterial), Get_Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantMaterial_Hash() { return 449442505U; }
class UScriptStruct* FVREDCppVariantSet::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantSet_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantSet, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantSet"), sizeof(FVREDCppVariantSet), Get_Z_Construct_UScriptStruct_FVREDCppVariantSet_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantSet>()
{
	return FVREDCppVariantSet::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantSet(FVREDCppVariantSet::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantSet"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantSet
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantSet()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantSet>(FName(TEXT("VREDCppVariantSet")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantSet;
	struct Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TargetVariantNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetVariantNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetVariantNames;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ChosenOptions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChosenOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ChosenOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VariantSetGroupName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_VariantSetGroupName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AnimClips_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AnimClips_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AnimClips;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSequentialAnimation_MetaData[];
#endif
		static void NewProp_bSequentialAnimation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSequentialAnimation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantSet>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_TargetVariantNames_Inner = { "TargetVariantNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_TargetVariantNames_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_TargetVariantNames = { "TargetVariantNames", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantSet, TargetVariantNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_TargetVariantNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_TargetVariantNames_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_ChosenOptions_Inner = { "ChosenOptions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_ChosenOptions_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_ChosenOptions = { "ChosenOptions", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantSet, ChosenOptions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_ChosenOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_ChosenOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_VariantSetGroupName_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_VariantSetGroupName = { "VariantSetGroupName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantSet, VariantSetGroupName), METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_VariantSetGroupName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_VariantSetGroupName_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_AnimClips_Inner = { "AnimClips", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_AnimClips_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_AnimClips = { "AnimClips", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantSet, AnimClips), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_AnimClips_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_AnimClips_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_bSequentialAnimation_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_bSequentialAnimation_SetBit(void* Obj)
	{
		((FVREDCppVariantSet*)Obj)->bSequentialAnimation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_bSequentialAnimation = { "bSequentialAnimation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FVREDCppVariantSet), &Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_bSequentialAnimation_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_bSequentialAnimation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_bSequentialAnimation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_TargetVariantNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_TargetVariantNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_ChosenOptions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_ChosenOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_VariantSetGroupName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_AnimClips_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_AnimClips,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::NewProp_bSequentialAnimation,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantSet",
		sizeof(FVREDCppVariantSet),
		alignof(FVREDCppVariantSet),
		Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantSet()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantSet_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantSet"), sizeof(FVREDCppVariantSet), Get_Z_Construct_UScriptStruct_FVREDCppVariantSet_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantSet_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantSet_Hash() { return 2384810120U; }
class UScriptStruct* FVREDCppVariantGeometry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantGeometry, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantGeometry"), sizeof(FVREDCppVariantGeometry), Get_Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantGeometry>()
{
	return FVREDCppVariantGeometry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantGeometry(FVREDCppVariantGeometry::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantGeometry"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantGeometry
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantGeometry()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantGeometry>(FName(TEXT("VREDCppVariantGeometry")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantGeometry;
	struct Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TargetNodes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetNodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetNodes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Options_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Options_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantGeometry>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_TargetNodes_Inner = { "TargetNodes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_TargetNodes_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_TargetNodes = { "TargetNodes", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantGeometry, TargetNodes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_TargetNodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_TargetNodes_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_Options_Inner = { "Options", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_Options_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantGeometry, Options), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_Options_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_Options_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_TargetNodes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_TargetNodes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_Options_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::NewProp_Options,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantGeometry",
		sizeof(FVREDCppVariantGeometry),
		alignof(FVREDCppVariantGeometry),
		Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantGeometry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantGeometry"), sizeof(FVREDCppVariantGeometry), Get_Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantGeometry_Hash() { return 1943571166U; }
class UScriptStruct* FVREDCppVariantCamera::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantCamera_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantCamera, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantCamera"), sizeof(FVREDCppVariantCamera), Get_Z_Construct_UScriptStruct_FVREDCppVariantCamera_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantCamera>()
{
	return FVREDCppVariantCamera::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantCamera(FVREDCppVariantCamera::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantCamera"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantCamera
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantCamera()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantCamera>(FName(TEXT("VREDCppVariantCamera")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantCamera;
	struct Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Options_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Options_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantCamera>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::NewProp_Options_Inner = { "Options", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVREDCppVariantCameraOption, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::NewProp_Options_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantCamera, Options), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::NewProp_Options_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::NewProp_Options_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::NewProp_Options_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::NewProp_Options,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantCamera",
		sizeof(FVREDCppVariantCamera),
		alignof(FVREDCppVariantCamera),
		Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantCamera()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantCamera_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantCamera"), sizeof(FVREDCppVariantCamera), Get_Z_Construct_UScriptStruct_FVREDCppVariantCamera_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantCamera_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantCamera_Hash() { return 1169451865U; }
class UScriptStruct* FVREDCppVariantLightOption::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantLightOption, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantLightOption"), sizeof(FVREDCppVariantLightOption), Get_Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantLightOption>()
{
	return FVREDCppVariantLightOption::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantLightOption(FVREDCppVariantLightOption::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantLightOption"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantLightOption
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantLightOption()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantLightOption>(FName(TEXT("VREDCppVariantLightOption")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantLightOption;
	struct Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantLightOption>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantLightOption, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantLightOption",
		sizeof(FVREDCppVariantLightOption),
		alignof(FVREDCppVariantLightOption),
		Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantLightOption()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantLightOption"), sizeof(FVREDCppVariantLightOption), Get_Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantLightOption_Hash() { return 432115178U; }
class UScriptStruct* FVREDCppVariantTransformOption::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantTransformOption, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantTransformOption"), sizeof(FVREDCppVariantTransformOption), Get_Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantTransformOption>()
{
	return FVREDCppVariantTransformOption::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantTransformOption(FVREDCppVariantTransformOption::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantTransformOption"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantTransformOption
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantTransformOption()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantTransformOption>(FName(TEXT("VREDCppVariantTransformOption")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantTransformOption;
	struct Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantTransformOption>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantTransformOption, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantTransformOption, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::NewProp_Transform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantTransformOption",
		sizeof(FVREDCppVariantTransformOption),
		alignof(FVREDCppVariantTransformOption),
		Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantTransformOption()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantTransformOption"), sizeof(FVREDCppVariantTransformOption), Get_Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantTransformOption_Hash() { return 677872869U; }
class UScriptStruct* FVREDCppVariantMaterialOption::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantMaterialOption"), sizeof(FVREDCppVariantMaterialOption), Get_Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantMaterialOption>()
{
	return FVREDCppVariantMaterialOption::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantMaterialOption(FVREDCppVariantMaterialOption::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantMaterialOption"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantMaterialOption
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantMaterialOption()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantMaterialOption>(FName(TEXT("VREDCppVariantMaterialOption")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantMaterialOption;
	struct Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantMaterialOption>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantMaterialOption, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantMaterialOption",
		sizeof(FVREDCppVariantMaterialOption),
		alignof(FVREDCppVariantMaterialOption),
		Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantMaterialOption"), sizeof(FVREDCppVariantMaterialOption), Get_Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantMaterialOption_Hash() { return 2787933716U; }
class UScriptStruct* FVREDCppVariantCameraOption::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantCameraOption, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantCameraOption"), sizeof(FVREDCppVariantCameraOption), Get_Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantCameraOption>()
{
	return FVREDCppVariantCameraOption::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantCameraOption(FVREDCppVariantCameraOption::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantCameraOption"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantCameraOption
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantCameraOption()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantCameraOption>(FName(TEXT("VREDCppVariantCameraOption")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantCameraOption;
	struct Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Location_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantCameraOption>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantCameraOption, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Location_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantCameraOption, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Location_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Location_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Rotation_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantCameraOption, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Rotation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::NewProp_Rotation,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantCameraOption",
		sizeof(FVREDCppVariantCameraOption),
		alignof(FVREDCppVariantCameraOption),
		Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantCameraOption()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantCameraOption"), sizeof(FVREDCppVariantCameraOption), Get_Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantCameraOption_Hash() { return 3872338000U; }
class UScriptStruct* FVREDCppVariantGeometryOption::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHVREDTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption, Z_Construct_UPackage__Script_DatasmithVREDTranslator(), TEXT("VREDCppVariantGeometryOption"), sizeof(FVREDCppVariantGeometryOption), Get_Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Hash());
	}
	return Singleton;
}
template<> DATASMITHVREDTRANSLATOR_API UScriptStruct* StaticStruct<FVREDCppVariantGeometryOption>()
{
	return FVREDCppVariantGeometryOption::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVREDCppVariantGeometryOption(FVREDCppVariantGeometryOption::StaticStruct, TEXT("/Script/DatasmithVREDTranslator"), TEXT("VREDCppVariantGeometryOption"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantGeometryOption
{
	FScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantGeometryOption()
	{
		UScriptStruct::DeferCppStructOps<FVREDCppVariantGeometryOption>(FName(TEXT("VREDCppVariantGeometryOption")));
	}
} ScriptStruct_DatasmithVREDTranslator_StaticRegisterNativesFVREDCppVariantGeometryOption;
	struct Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_VisibleMeshes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisibleMeshes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_VisibleMeshes;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_HiddenMeshes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HiddenMeshes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HiddenMeshes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVREDCppVariantGeometryOption>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantGeometryOption, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_VisibleMeshes_Inner = { "VisibleMeshes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_VisibleMeshes_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_VisibleMeshes = { "VisibleMeshes", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantGeometryOption, VisibleMeshes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_VisibleMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_VisibleMeshes_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_HiddenMeshes_Inner = { "HiddenMeshes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_HiddenMeshes_MetaData[] = {
		{ "Category", "VRED" },
		{ "ModuleRelativePath", "Private/DatasmithVREDImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_HiddenMeshes = { "HiddenMeshes", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVREDCppVariantGeometryOption, HiddenMeshes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_HiddenMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_HiddenMeshes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_VisibleMeshes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_VisibleMeshes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_HiddenMeshes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::NewProp_HiddenMeshes,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithVREDTranslator,
		nullptr,
		&NewStructOps,
		"VREDCppVariantGeometryOption",
		sizeof(FVREDCppVariantGeometryOption),
		alignof(FVREDCppVariantGeometryOption),
		Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithVREDTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VREDCppVariantGeometryOption"), sizeof(FVREDCppVariantGeometryOption), Get_Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVREDCppVariantGeometryOption_Hash() { return 1128146315U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
