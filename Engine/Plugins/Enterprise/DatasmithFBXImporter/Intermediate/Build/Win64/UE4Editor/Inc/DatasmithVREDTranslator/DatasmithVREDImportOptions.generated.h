// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHVREDTRANSLATOR_DatasmithVREDImportOptions_generated_h
#error "DatasmithVREDImportOptions.generated.h already included, missing '#pragma once' in DatasmithVREDImportOptions.h"
#endif
#define DATASMITHVREDTRANSLATOR_DatasmithVREDImportOptions_generated_h

#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithVREDImportOptions(); \
	friend struct Z_Construct_UClass_UDatasmithVREDImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDatasmithVREDImportOptions, UDatasmithFBXImportOptions, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithVREDTranslator"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithVREDImportOptions)


#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithVREDImportOptions(); \
	friend struct Z_Construct_UClass_UDatasmithVREDImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDatasmithVREDImportOptions, UDatasmithFBXImportOptions, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithVREDTranslator"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithVREDImportOptions)


#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithVREDImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithVREDImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithVREDImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithVREDImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithVREDImportOptions(UDatasmithVREDImportOptions&&); \
	NO_API UDatasmithVREDImportOptions(const UDatasmithVREDImportOptions&); \
public:


#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithVREDImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithVREDImportOptions(UDatasmithVREDImportOptions&&); \
	NO_API UDatasmithVREDImportOptions(const UDatasmithVREDImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithVREDImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithVREDImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithVREDImportOptions)


#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_16_PROLOG
#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_INCLASS \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h_19_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class DatasmithVREDImportOptions."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHVREDTRANSLATOR_API UClass* StaticClass<class UDatasmithVREDImportOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithFBXImporter_Source_DatasmithVREDTranslator_Private_DatasmithVREDImportOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
