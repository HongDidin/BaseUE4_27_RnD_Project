// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithFBXTranslator/Public/DatasmithFBXImportOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithFBXImportOptions() {}
// Cross Module References
	DATASMITHFBXTRANSLATOR_API UClass* Z_Construct_UClass_UDatasmithFBXImportOptions_NoRegister();
	DATASMITHFBXTRANSLATOR_API UClass* Z_Construct_UClass_UDatasmithFBXImportOptions();
	DATASMITHCONTENT_API UClass* Z_Construct_UClass_UDatasmithOptionsBase();
	UPackage* Z_Construct_UPackage__Script_DatasmithFBXTranslator();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
	void UDatasmithFBXImportOptions::StaticRegisterNativesUDatasmithFBXImportOptions()
	{
	}
	UClass* Z_Construct_UClass_UDatasmithFBXImportOptions_NoRegister()
	{
		return UDatasmithFBXImportOptions::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithFBXImportOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextureDirs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureDirs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TextureDirs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDatasmithOptionsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithFBXTranslator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "DebugProperty" },
		{ "IncludePath", "DatasmithFBXImportOptions.h" },
		{ "ModuleRelativePath", "Public/DatasmithFBXImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::NewProp_TextureDirs_Inner = { "TextureDirs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::NewProp_TextureDirs_MetaData[] = {
		{ "Category", "AssetImporting" },
		{ "DisplayName", "Texture folders" },
		{ "ModuleRelativePath", "Public/DatasmithFBXImportOptions.h" },
		{ "ToolTip", "Where to look for textures" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::NewProp_TextureDirs = { "TextureDirs", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithFBXImportOptions, TextureDirs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::NewProp_TextureDirs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::NewProp_TextureDirs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::NewProp_TextureDirs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::NewProp_TextureDirs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithFBXImportOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::ClassParams = {
		&UDatasmithFBXImportOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithFBXImportOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithFBXImportOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithFBXImportOptions, 4036227731);
	template<> DATASMITHFBXTRANSLATOR_API UClass* StaticClass<UDatasmithFBXImportOptions>()
	{
		return UDatasmithFBXImportOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithFBXImportOptions(Z_Construct_UClass_UDatasmithFBXImportOptions, &UDatasmithFBXImportOptions::StaticClass, TEXT("/Script/DatasmithFBXTranslator"), TEXT("UDatasmithFBXImportOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithFBXImportOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
