// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithDeltaGenTranslator/Private/DatasmithDeltaGenImportOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithDeltaGenImportOptions() {}
// Cross Module References
	DATASMITHDELTAGENTRANSLATOR_API UEnum* Z_Construct_UEnum_DatasmithDeltaGenTranslator_EShadowTextureMode();
	UPackage* Z_Construct_UPackage__Script_DatasmithDeltaGenTranslator();
	DATASMITHDELTAGENTRANSLATOR_API UClass* Z_Construct_UClass_UDatasmithDeltaGenImportOptions_NoRegister();
	DATASMITHDELTAGENTRANSLATOR_API UClass* Z_Construct_UClass_UDatasmithDeltaGenImportOptions();
	DATASMITHFBXTRANSLATOR_API UClass* Z_Construct_UClass_UDatasmithFBXImportOptions();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FFilePath();
// End Cross Module References
	static UEnum* EShadowTextureMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DatasmithDeltaGenTranslator_EShadowTextureMode, Z_Construct_UPackage__Script_DatasmithDeltaGenTranslator(), TEXT("EShadowTextureMode"));
		}
		return Singleton;
	}
	template<> DATASMITHDELTAGENTRANSLATOR_API UEnum* StaticEnum<EShadowTextureMode>()
	{
		return EShadowTextureMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EShadowTextureMode(EShadowTextureMode_StaticEnum, TEXT("/Script/DatasmithDeltaGenTranslator"), TEXT("EShadowTextureMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DatasmithDeltaGenTranslator_EShadowTextureMode_Hash() { return 1665536236U; }
	UEnum* Z_Construct_UEnum_DatasmithDeltaGenTranslator_EShadowTextureMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithDeltaGenTranslator();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EShadowTextureMode"), 0, Get_Z_Construct_UEnum_DatasmithDeltaGenTranslator_EShadowTextureMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EShadowTextureMode::Ignore", (int64)EShadowTextureMode::Ignore },
				{ "EShadowTextureMode::AmbientOcclusion", (int64)EShadowTextureMode::AmbientOcclusion },
				{ "EShadowTextureMode::Multiplier", (int64)EShadowTextureMode::Multiplier },
				{ "EShadowTextureMode::AmbientOcclusionAndMultiplier", (int64)EShadowTextureMode::AmbientOcclusionAndMultiplier },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AmbientOcclusion.Name", "EShadowTextureMode::AmbientOcclusion" },
				{ "AmbientOcclusion.Tooltip", "Use shadow textures as ambient occlusion maps" },
				{ "AmbientOcclusionAndMultiplier.Name", "EShadowTextureMode::AmbientOcclusionAndMultiplier" },
				{ "AmbientOcclusionAndMultiplier.Tooltip", "Use shadow textures as ambient occlusion maps as well as multipliers for base color and specular" },
				{ "Ignore.Name", "EShadowTextureMode::Ignore" },
				{ "Ignore.Tooltip", "Ignore exported shadow textures" },
				{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
				{ "Multiplier.Name", "EShadowTextureMode::Multiplier" },
				{ "Multiplier.Tooltip", "Use shadow textures as multipliers for base color and specular" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DatasmithDeltaGenTranslator,
				nullptr,
				"EShadowTextureMode",
				"EShadowTextureMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDatasmithDeltaGenImportOptions::StaticRegisterNativesUDatasmithDeltaGenImportOptions()
	{
	}
	UClass* Z_Construct_UClass_UDatasmithDeltaGenImportOptions_NoRegister()
	{
		return UDatasmithDeltaGenImportOptions::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveInvisibleNodes_MetaData[];
#endif
		static void NewProp_bRemoveInvisibleNodes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveInvisibleNodes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSimplifyNodeHierarchy_MetaData[];
#endif
		static void NewProp_bSimplifyNodeHierarchy_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSimplifyNodeHierarchy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportVar_MetaData[];
#endif
		static void NewProp_bImportVar_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportVar;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VarPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VarPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportPos_MetaData[];
#endif
		static void NewProp_bImportPos_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportPos;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PosPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PosPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportTml_MetaData[];
#endif
		static void NewProp_bImportTml_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportTml;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TmlPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TmlPath;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ShadowTextureMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShadowTextureMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ShadowTextureMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDatasmithFBXImportOptions,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithDeltaGenTranslator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Debug DebugProperty" },
		{ "IncludePath", "DatasmithDeltaGenImportOptions.h" },
		{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bRemoveInvisibleNodes_MetaData[] = {
		{ "Category", "Processing" },
		{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
		{ "ToolTip", "Don't keep nodes that marked invisible in FBX(an din the original scene), except switch variants" },
	};
#endif
	void Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bRemoveInvisibleNodes_SetBit(void* Obj)
	{
		((UDatasmithDeltaGenImportOptions*)Obj)->bRemoveInvisibleNodes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bRemoveInvisibleNodes = { "bRemoveInvisibleNodes", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDatasmithDeltaGenImportOptions), &Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bRemoveInvisibleNodes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bRemoveInvisibleNodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bRemoveInvisibleNodes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bSimplifyNodeHierarchy_MetaData[] = {
		{ "Category", "Processing" },
		{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
		{ "ToolTip", "Collapse nodes that have identity transform, have no mesh and not used in animation/variants/switches" },
	};
#endif
	void Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bSimplifyNodeHierarchy_SetBit(void* Obj)
	{
		((UDatasmithDeltaGenImportOptions*)Obj)->bSimplifyNodeHierarchy = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bSimplifyNodeHierarchy = { "bSimplifyNodeHierarchy", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDatasmithDeltaGenImportOptions), &Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bSimplifyNodeHierarchy_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bSimplifyNodeHierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bSimplifyNodeHierarchy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportVar_MetaData[] = {
		{ "Category", "AssetImporting" },
		{ "DisplayName", "Import Variants" },
		{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
		{ "ToolTip", "import VAR files" },
	};
#endif
	void Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportVar_SetBit(void* Obj)
	{
		((UDatasmithDeltaGenImportOptions*)Obj)->bImportVar = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportVar = { "bImportVar", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDatasmithDeltaGenImportOptions), &Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportVar_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportVar_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportVar_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_VarPath_MetaData[] = {
		{ "Category", "AssetImporting" },
		{ "DisplayName", "Variants file path" },
		{ "EditCondition", "bImportVar" },
		{ "FilePathFilter", "var" },
		{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
		{ "ToolTip", "Path to the *.var file. By default it will search for a *.var file in the same folder as the FBX file, with the same base filename as it" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_VarPath = { "VarPath", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithDeltaGenImportOptions, VarPath), Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_VarPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_VarPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportPos_MetaData[] = {
		{ "Category", "AssetImporting" },
		{ "DisplayName", "Import POS States" },
		{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
		{ "ToolTip", "import POS files" },
	};
#endif
	void Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportPos_SetBit(void* Obj)
	{
		((UDatasmithDeltaGenImportOptions*)Obj)->bImportPos = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportPos = { "bImportPos", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDatasmithDeltaGenImportOptions), &Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportPos_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportPos_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportPos_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_PosPath_MetaData[] = {
		{ "Category", "AssetImporting" },
		{ "DisplayName", "POS file path" },
		{ "EditCondition", "bImportPos" },
		{ "FilePathFilter", "pos" },
		{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
		{ "ToolTip", "Path to the *.pos file. By default it will search for a *.pos file in the same folder as the FBX file, with the same base filename as it" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_PosPath = { "PosPath", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithDeltaGenImportOptions, PosPath), Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_PosPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_PosPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportTml_MetaData[] = {
		{ "Category", "AssetImporting" },
		{ "DisplayName", "Import TML Animations" },
		{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
		{ "ToolTip", "import TML files" },
	};
#endif
	void Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportTml_SetBit(void* Obj)
	{
		((UDatasmithDeltaGenImportOptions*)Obj)->bImportTml = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportTml = { "bImportTml", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDatasmithDeltaGenImportOptions), &Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportTml_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportTml_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportTml_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_TmlPath_MetaData[] = {
		{ "Category", "AssetImporting" },
		{ "DisplayName", "TML file path" },
		{ "EditCondition", "bImportTml" },
		{ "FilePathFilter", "tml" },
		{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
		{ "ToolTip", "Path to the *.tml file. By default it will search for a *.tml file in the same folder as the FBX file, with the same base filename as it" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_TmlPath = { "TmlPath", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithDeltaGenImportOptions, TmlPath), Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_TmlPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_TmlPath_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_ShadowTextureMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_ShadowTextureMode_MetaData[] = {
		{ "Category", "AssetImporting" },
		{ "DisplayName", "Shadow Textures" },
		{ "ModuleRelativePath", "Private/DatasmithDeltaGenImportOptions.h" },
		{ "ToolTip", "How to handle shadow textures" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_ShadowTextureMode = { "ShadowTextureMode", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithDeltaGenImportOptions, ShadowTextureMode), Z_Construct_UEnum_DatasmithDeltaGenTranslator_EShadowTextureMode, METADATA_PARAMS(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_ShadowTextureMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_ShadowTextureMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bRemoveInvisibleNodes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bSimplifyNodeHierarchy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportVar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_VarPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportPos,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_PosPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_bImportTml,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_TmlPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_ShadowTextureMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::NewProp_ShadowTextureMode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithDeltaGenImportOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::ClassParams = {
		&UDatasmithDeltaGenImportOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithDeltaGenImportOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithDeltaGenImportOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithDeltaGenImportOptions, 3480437066);
	template<> DATASMITHDELTAGENTRANSLATOR_API UClass* StaticClass<UDatasmithDeltaGenImportOptions>()
	{
		return UDatasmithDeltaGenImportOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithDeltaGenImportOptions(Z_Construct_UClass_UDatasmithDeltaGenImportOptions, &UDatasmithDeltaGenImportOptions::StaticClass, TEXT("/Script/DatasmithDeltaGenTranslator"), TEXT("UDatasmithDeltaGenImportOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithDeltaGenImportOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
