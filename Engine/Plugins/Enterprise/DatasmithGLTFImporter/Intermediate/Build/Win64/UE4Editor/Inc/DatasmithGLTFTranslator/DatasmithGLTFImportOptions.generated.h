// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHGLTFTRANSLATOR_DatasmithGLTFImportOptions_generated_h
#error "DatasmithGLTFImportOptions.generated.h already included, missing '#pragma once' in DatasmithGLTFImportOptions.h"
#endif
#define DATASMITHGLTFTRANSLATOR_DatasmithGLTFImportOptions_generated_h

#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithGLTFImportOptions(); \
	friend struct Z_Construct_UClass_UDatasmithGLTFImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDatasmithGLTFImportOptions, UDatasmithOptionsBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithGLTFTranslator"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithGLTFImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithGLTFImportOptions(); \
	friend struct Z_Construct_UClass_UDatasmithGLTFImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDatasmithGLTFImportOptions, UDatasmithOptionsBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithGLTFTranslator"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithGLTFImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithGLTFImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithGLTFImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithGLTFImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithGLTFImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithGLTFImportOptions(UDatasmithGLTFImportOptions&&); \
	NO_API UDatasmithGLTFImportOptions(const UDatasmithGLTFImportOptions&); \
public:


#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithGLTFImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithGLTFImportOptions(UDatasmithGLTFImportOptions&&); \
	NO_API UDatasmithGLTFImportOptions(const UDatasmithGLTFImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithGLTFImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithGLTFImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithGLTFImportOptions)


#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_14_PROLOG
#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_INCLASS \
	Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHGLTFTRANSLATOR_API UClass* StaticClass<class UDatasmithGLTFImportOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithGLTFImporter_Source_DatasmithGLTFTranslator_Private_DatasmithGLTFImportOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
