/******************************************************************************
 * Copyright 1986, 2017 NVIDIA Corporation. All rights reserved.
 *****************************************************************************/

mdl 1.4;

import df::*;
import base::*;
import tex::*;
import anno::*;
import state::*;


export color saturated_color(color c) uniform
{
    return base::blend_color_layers(
        layers: base::color_layer[](
            base::color_layer(
                layer_color: c,
                mode: base::color_layer_color
                )
            ),
        base: color(1.0, 1.0, 1.0)
        ).tint;
}


export enum brdf_type
[[
    anno::description("BRDF types supported for AxF SVBRDFs"),
    anno::hidden()
]]
{
    brdf_ward_geislermoroder,
    brdf_cooktorrance,
    brdf_count
};


export enum fresnel_type
[[
    anno::description("Fresnel types supported for AxF SVBRDFs"),
    anno::hidden()
]]
{
    fresnel_none,
    fresnel_schlick,
    fresnel_fresnel,
    fresnel_count
};


export material svbrdf(
    uniform texture_2d diffuse_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform texture_2d specular_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform texture_2d normal_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform texture_2d height_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform texture_2d alpha_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform brdf_type brdf_type = brdf_ward_geislermoroder
    [[
        anno::hidden()
    ]],
    uniform bool isotropic = true
    [[
        anno::hidden()
    ]],
    uniform texture_2d specular_brdf_u_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform texture_2d specular_brdf_v_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform texture_2d specular_brdf_rotation_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform texture_2d specular_brdf_f0_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform fresnel_type fresnel_type = fresnel_none
    [[
        anno::hidden()
    ]],
    uniform texture_2d specular_brdf_fresnel_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform bool has_clearcoat = false
    [[
        anno::hidden()
    ]],
    uniform texture_2d clearcoat_color_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform texture_2d clearcoat_ior_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform texture_2d clearcoat_normal_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    base::texture_coordinate_info coordinate_override = base::texture_coordinate_info()
    [[
        anno::hidden(),
        anno::description("By default, texturing is using object uv coordinates. Procedural coordinate"
                        " generators like base::coordinate_projection can be attached here to override the default behavior")
    ]],
    uniform bool use_sample_size = true
    [[
        anno::display_name("Use sample size"),
        anno::description("Set to true if the real world measurements of the measured sample are known"),
        anno::in_group("Real world size")
    ]],
    uniform float sample_size_u = state::meters_per_scene_unit()
    [[
        anno::display_name("Sample size U"),
        anno::description("The size of the measured sample in m, measured in the U direction"),
        anno::soft_range(0.01, 1000),
        anno::in_group("Real world size")
    ]],
    uniform float sample_size_v = state::meters_per_scene_unit()
    [[
        anno::display_name("Sample size V"),
        anno::description("The size of the measured sample in m, measured in the V direction"),
        anno::soft_range(0.01, 1000),
        anno::in_group("Real world size")
    ]],
    uniform float scene_to_meter = state::meters_per_scene_unit()
    [[
        anno::display_name("Scene to m conversion factor"),
        anno::description("Conversion factor from modeling scale used in the scene and m. Only used if \"Use real world size\" is true and for displacement"),
        anno::soft_range(0.01, 1000),
        anno::in_group("Real world size")
    ]],
    uniform float scale_u = 1.0
    [[
        anno::display_name("Scale V"),
        anno::description("Scale factor in V direction"),
        anno::soft_range(0.01, 100),
        anno::in_group("Material placement")
    ]],
    uniform float scale_v = 1.0
    [[
        anno::display_name("Scale U"),
        anno::description("Scale factor in U direction"),
        anno::soft_range(0.01, 100),
        anno::in_group("Material placement")
    ]],
    uniform float rotate_uv = 0.0
    [[
        anno::display_name("Rotation angle"),
        anno::description("Changes the orientation of the textures on the object"),
        anno::soft_range(0.0, 6.284),
        anno::in_group("Material placement")
    ]],
    uniform tex::wrap_mode wrap = tex::wrap_repeat
    [[
        anno::display_name("Texture wrapping behavior"),
        anno::description("Controls the wrapping behavior of the texture if it fits multiple times on an object"),
        anno::in_group("Material placement")
    ]],
    uniform float normal_texture_strength = 1.0
    [[
        anno::display_name("Normal texture strength"),
        anno::description("Can be used to tweak the bumpiness of the material. At 1.0 it will match the measured bumpiness"),
        anno::soft_range(0.01, 100.0),
        anno::in_group("Creative control")
    ]],
    float3 geometry_normal = state::normal()
    [[
        anno::display_name("Geometry normal"),
        anno::description("Allows the use of an additional bump mapping effect"),
        anno::in_group("Creative control")
    ]],
    uniform float scale_height = 1.0
    [[
        anno::display_name("Scale height texture"),
        anno::description("Scale factor for the displacement mapping effect"),
        anno::in_group("Creative control")
    ]]
)
[[
    anno::display_name("AxF SVBRDF material"),
    anno::description("MDL implementation of the SVBRDF model used in AxF files"),
    anno::author("NVIDIA Corporation"),
    anno::copyright_notice("Copyright 1986, 2017 NVIDIA Corporation. All rights reserved."),
    anno::key_words(string[]("axf","svbrdf"))
]]
 = let {

    uniform float3 scale = use_sample_size ? float3(scene_to_meter / sample_size_u, scene_to_meter / sample_size_v, 1.0) : float3(scale_u, scale_v, 1.0);
    base::texture_coordinate_info coordinate = base::transform_coordinate(
        base::rotation_translation_scale(
            rotation: float3(0.0, 0.0, rotate_uv),
            translation: float3(0.0),
            scaling: scale
            ),
            coordinate: coordinate_override
        );

    base::texture_return diffuse = base::file_texture(
        texture: diffuse_texture,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        mono_source: base::mono_maximum
        );
    color diffuse_sat = saturated_color(diffuse.tint);

    base::texture_return specular = base::file_texture(
        texture: specular_texture,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        mono_source: base::mono_maximum
        );
    color specular_sat = saturated_color(specular.tint);

    float specular_brdf_u = base::file_texture(
        texture: specular_brdf_u_texture,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        mono_source: base::mono_maximum
        ).mono;
    float specular_brdf_v = isotropic ? specular_brdf_u : base::file_texture(
        texture: specular_brdf_v_texture,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        mono_source: base::mono_maximum
        ).mono;

    float3 tangent_u = (isotropic) 
        ? coordinate_override.tangent_u 
        : base::anisotropy_conversion(
            tangent_u: coordinate_override.tangent_u, 
            anisotropy_rotation:  base::file_texture(
                texture: specular_brdf_rotation_texture,
                uvw: coordinate,
                wrap_u: wrap,
                wrap_v: wrap,
                mono_source: base::mono_maximum
                ).mono).tangent_u;

    float3 normal = base::tangent_space_normal_texture(
        texture: normal_texture,
        factor: normal_texture_strength,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        flip_tangent_u : false,
        flip_tangent_v : false,
        offset: 0.0,
        scale: 1.0
        );

    bsdf glossy_component0 =
              brdf_type == brdf_ward_geislermoroder ?
                    df::ward_geisler_moroder_bsdf(
                        roughness_u: specular_brdf_u,
                        roughness_v: specular_brdf_v,
                        tint: specular_sat,
                        tangent_u: tangent_u)
            : // brdf_cooktorrance
                    df::custom_curve_layer(
                        normal_reflectivity: base::file_texture(
                            texture: specular_brdf_f0_texture,
                            uvw: coordinate,
                            wrap_u: wrap,
                            wrap_v: wrap,
                            mono_source: base::mono_maximum
                            ).mono,
                        layer: df::microfacet_beckmann_vcavities_bsdf(
                            tint: specular_sat,
                            roughness_u: specular_brdf_u,
                            roughness_v: specular_brdf_v,
                            tangent_u: tangent_u))
            ;

    base::texture_return fresnel_tex = (fresnel_type == fresnel_schlick || fresnel_type == fresnel_fresnel) ?
        base::file_texture(
            texture: specular_brdf_fresnel_texture,
            uvw: coordinate,
            wrap_u: wrap,
            wrap_v: wrap,
            mono_source: base::mono_maximum) : base::texture_return();

    bsdf glossy_component = (fresnel_type == fresnel_schlick) ? 
        df::custom_curve_layer(normal_reflectivity: fresnel_tex.mono, layer: glossy_component0) :
        ((fresnel_type == fresnel_fresnel) ? df::color_fresnel_layer(ior: fresnel_tex.tint, layer: glossy_component0) : glossy_component0);

    bsdf base = df::normalized_mix(df::bsdf_component[2](
            df::bsdf_component(specular.mono, glossy_component),
            df::bsdf_component(diffuse.mono, df::diffuse_reflection_bsdf( tint: diffuse_sat))
            ));

    color clearcoat_ior = has_clearcoat ? base::file_texture(
        texture:  clearcoat_ior_texture,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        mono_source: base::mono_maximum).tint : color(0.0);
    color clearcoat_color = (has_clearcoat && tex::texture_isvalid(clearcoat_color_texture)) ? base::file_texture(
        texture:  clearcoat_color_texture,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        mono_source: base::mono_maximum).tint  : color(1.0);
    float3 clearcoat_normal = (has_clearcoat && tex::texture_isvalid(clearcoat_normal_texture)) ? base::tangent_space_normal_texture(
        texture: clearcoat_normal_texture,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        flip_tangent_u : false,
        flip_tangent_v : false,
        offset: 0.0,
        scale: 1.0
        ) : state::normal();
    
    bsdf coated = has_clearcoat ? 
        df::color_fresnel_layer(ior: clearcoat_ior, weight: color(1.0), layer: df::specular_bsdf(tint: clearcoat_color), base: base, normal: clearcoat_normal) : 
        base;

    bsdf surface = df::weighted_layer(
        weight: 1.0,
        normal: normal,
        layer: coated
        );

    // height texture is in millimeters, respect that in the scale
    uniform float displacement_scale = scale_height * scene_to_meter * 0.001;
    
    float3 displacement_value = tex::texture_isvalid(height_texture) && (displacement_scale > 0.0) ? base::file_texture(
        texture: height_texture,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        color_scale: color(displacement_scale),
        mono_source: base::mono_maximum).mono * state::normal() : float3(0.0);

    float cutout_value = tex::texture_isvalid(alpha_texture) ? base::file_texture(
        texture: alpha_texture,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        mono_source: base::mono_maximum).mono : 1.0f;

} in material(
    surface: material_surface(
        scattering: surface
        ),
    geometry: material_geometry(
        normal: geometry_normal,
        displacement: displacement_value,
        cutout_opacity: cutout_value
        ),
    thin_walled: true
    );

export material carpaint(
    uniform color[<N>] brdf_colors = color[](color(1.0))
    [[
        anno::hidden()
    ]],
    uniform texture_2d clearcoat_normal_texture = texture_2d()
    [[
        anno::hidden()
    ]],
    uniform color base_tint = color(0.0)
    [[
        anno::hidden()
    ]],
    uniform float clearcoat_scale = 1.0
    [[
        anno::hidden()
    ]],
    uniform float brdf_scale = 1.0
    [[
        anno::hidden()
    ]],
    uniform float ior = 1.0
    [[
        anno::hidden()
    ]],
    uniform float ct_diffuse = 0.0
    [[
        anno::hidden()
    ]],
    uniform float3 ct_coeffs = float3(0.0, 0.0, 0.0)
    [[
        anno::hidden()
    ]],
    uniform float3 ct_f0s = float3(0.0, 0.0, 0.0)
    [[
        anno::hidden()
    ]],
    uniform float3 ct_spreads = float3(0.0, 0.0, 0.0)
    [[
        anno::hidden()
    ]],
    base::texture_coordinate_info coordinate_override = base::texture_coordinate_info()
    [[
        anno::hidden(),
        anno::description("By default, texturing is using object uv coordinates. Procedural coordinate"
                        " generators like base::coordinate_projection can be attached here to override the default behavior")
    ]],
    uniform bool use_sample_size = true
    [[
        anno::display_name("Use sample size"),
        anno::description("Set to true if the real world measurements of the measured sample are known"),
        anno::in_group("Real world size")
    ]],
    uniform float sample_size_u = state::meters_per_scene_unit()
    [[
        anno::display_name("Sample size U"),
        anno::description("The size of the measured sample in m, measured in the U direction"),
        anno::soft_range(0.01, 1000),
        anno::in_group("Real world size")
    ]],
    uniform float sample_size_v = state::meters_per_scene_unit()
    [[
        anno::display_name("Sample size V"),
        anno::description("The size of the measured sample in m, measured in the V direction"),
        anno::soft_range(0.01, 1000),
        anno::in_group("Real world size")
    ]],
    uniform float scene_to_meter = state::meters_per_scene_unit()
    [[
        anno::display_name("Scene to m conversion factor"),
        anno::description("Conversion factor from modeling scale used in the scene and m. Only used if \"Use real world size\" is true"),
        anno::soft_range(0.01, 1000),
        anno::in_group("Real world size")
    ]],
    uniform float scale_u = 1.0
    [[
        anno::display_name("Scale V"),
        anno::description("Scale factor in V direction"),
        anno::soft_range(0.01, 100),
        anno::in_group("Material placement")
    ]],
    uniform float scale_v = 1.0
    [[
        anno::display_name("Scale U"),
        anno::description("Scale factor in U direction"),
        anno::soft_range(0.01, 100),
        anno::in_group("Material placement")
    ]],
    uniform float rotate_uv = 0.0
    [[
        anno::display_name("Rotation angle"),
        anno::description("Changes the orientation of the textures on the object"),
        anno::soft_range(0.0, 6.284),
        anno::in_group("Material placement")
    ]],
    uniform tex::wrap_mode wrap = tex::wrap_repeat
    [[
        anno::display_name("Texture wrapping behavior"),
        anno::description("Controls the wrapping behavior of the texture if it fits multiple times on an object"),
        anno::in_group("Material placement")
    ]],
    float3 geometry_normal = state::normal()
    [[
        anno::display_name("Geometry normal"),
        anno::description("Allows the use of an additional bump mapping effect"),
        anno::in_group("Creative control")
    ]]
)
[[
    anno::display_name("AxF carpaint material"),
    anno::description("MDL approximation of the AxF carpaint material"),
    anno::author("NVIDIA Corporation"),
    anno::copyright_notice("Copyright 1986, 2017 NVIDIA Corporation. All rights reserved."),
    anno::key_words(string[]("axf","carpaint"))
]]
 = let {

    uniform float3 scale = use_sample_size ? float3(scene_to_meter / sample_size_u, scene_to_meter / sample_size_v, 1.0) : float3(scale_u, scale_v, 1.0);
    base::texture_coordinate_info coordinate = base::transform_coordinate(
        base::rotation_translation_scale(
            rotation: float3(0.0, 0.0, rotate_uv),
            translation: float3(0.0),
            scaling: scale
            ),
            coordinate: coordinate_override
        );

    bsdf clearcoat_component = df::specular_bsdf(tint: color(clearcoat_scale));

    bsdf ct0 = df::custom_curve_layer(
        normal_reflectivity: ct_f0s.x,
        layer: df::microfacet_beckmann_vcavities_bsdf(roughness_u: ct_spreads.x)
        );
    bsdf ct1 = df::custom_curve_layer(
        normal_reflectivity: ct_f0s.y,
        layer: df::microfacet_beckmann_vcavities_bsdf(roughness_u: ct_spreads.y)
        );
    bsdf ct2 = df::custom_curve_layer(
        normal_reflectivity: ct_f0s.z,
        layer: df::microfacet_beckmann_vcavities_bsdf(roughness_u: ct_spreads.z)
        );
           
    bsdf base_component = df::normalized_mix(
        df::bsdf_component[4](
            df::bsdf_component(weight: ct_diffuse * brdf_scale, component: df::diffuse_reflection_bsdf(tint : base_tint)),
            df::bsdf_component(weight: ct_coeffs.x * brdf_scale, component: ct0),
            df::bsdf_component(weight: ct_coeffs.y * brdf_scale, component: ct1),
            df::bsdf_component(weight: ct_coeffs.z * brdf_scale, component: ct2)
            )
        );

    float3 clearcoat_normal = tex::texture_isvalid(clearcoat_normal_texture) ? base::tangent_space_normal_texture(
        texture: clearcoat_normal_texture,
        uvw: coordinate,
        wrap_u: wrap,
        wrap_v: wrap,
        flip_tangent_u : false,
        flip_tangent_v : false,
        offset: 0.0,
        scale: 1.0
        ) : state::normal();

    bsdf surface = df::color_fresnel_layer(
        ior: color(ior),
        layer: clearcoat_component,
        base: df::measured_curve_factor(
            curve_values: brdf_colors,
            base: base_component),
        normal: clearcoat_normal
        );

} in material(
    surface: material_surface(
        scattering: surface
        ),
    geometry: material_geometry(
        normal: geometry_normal
        )
    );
