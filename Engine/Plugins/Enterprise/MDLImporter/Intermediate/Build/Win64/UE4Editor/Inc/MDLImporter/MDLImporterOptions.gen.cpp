// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MDLImporter/Public/MDLImporterOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMDLImporterOptions() {}
// Cross Module References
	MDLIMPORTER_API UClass* Z_Construct_UClass_UMDLImporterOptions_NoRegister();
	MDLIMPORTER_API UClass* Z_Construct_UClass_UMDLImporterOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MDLImporter();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
	void UMDLImporterOptions::StaticRegisterNativesUMDLImporterOptions()
	{
	}
	UClass* Z_Construct_UClass_UMDLImporterOptions_NoRegister()
	{
		return UMDLImporterOptions::StaticClass();
	}
	struct Z_Construct_UClass_UMDLImporterOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BakingResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_BakingResolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BakingSamples_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_BakingSamples;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResourcesDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ResourcesDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModulesDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModulesDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MetersPerSceneUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MetersPerSceneUnit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bForceBaking_MetaData[];
#endif
		static void NewProp_bForceBaking_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bForceBaking;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMDLImporterOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MDLImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMDLImporterOptions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MDLImporterOptions.h" },
		{ "ModuleRelativePath", "Public/MDLImporterOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_BakingResolution_MetaData[] = {
		{ "Category", "Bake options" },
		{ "ClampMax", "16384" },
		{ "ClampMin", "128" },
		{ "DisplayName", "Baking resolution" },
		{ "FixedIncrement", "2" },
		{ "ModuleRelativePath", "Public/MDLImporterOptions.h" },
		{ "ToolTip", "Resolution for baking procedural textures." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_BakingResolution = { "BakingResolution", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMDLImporterOptions, BakingResolution), METADATA_PARAMS(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_BakingResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_BakingResolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_BakingSamples_MetaData[] = {
		{ "Category", "Bake options" },
		{ "ClampMax", "16" },
		{ "ClampMin", "1" },
		{ "DisplayName", "Baking samples" },
		{ "FixedIncrement", "2" },
		{ "ModuleRelativePath", "Public/MDLImporterOptions.h" },
		{ "ToolTip", "Samples used for baking procedural textures." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_BakingSamples = { "BakingSamples", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMDLImporterOptions, BakingSamples), METADATA_PARAMS(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_BakingSamples_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_BakingSamples_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_ResourcesDir_MetaData[] = {
		{ "Category", "Advanced options" },
		{ "DisplayName", "Resource folder" },
		{ "ModuleRelativePath", "Public/MDLImporterOptions.h" },
		{ "ToolTip", "Path to look for resources: textures, light profiles and other." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_ResourcesDir = { "ResourcesDir", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMDLImporterOptions, ResourcesDir), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_ResourcesDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_ResourcesDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_ModulesDir_MetaData[] = {
		{ "Category", "Advanced options" },
		{ "DisplayName", "Modules folder" },
		{ "ModuleRelativePath", "Public/MDLImporterOptions.h" },
		{ "ToolTip", "Path to look for extra MDL modules." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_ModulesDir = { "ModulesDir", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMDLImporterOptions, ModulesDir), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_ModulesDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_ModulesDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_MetersPerSceneUnit_MetaData[] = {
		{ "ClampMax", "1000" },
		{ "ClampMin", "0.010000" },
		{ "DisplayName", "Meters per scene unit" },
		{ "ModuleRelativePath", "Public/MDLImporterOptions.h" },
		{ "ToolTip", "The conversion ratio between meters and scene units for materials." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_MetersPerSceneUnit = { "MetersPerSceneUnit", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMDLImporterOptions, MetersPerSceneUnit), METADATA_PARAMS(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_MetersPerSceneUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_MetersPerSceneUnit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_bForceBaking_MetaData[] = {
		{ "DisplayName", "Force baking of all maps" },
		{ "ModuleRelativePath", "Public/MDLImporterOptions.h" },
		{ "ToolTip", "Always bakes the maps to textures instead of using material nodes." },
	};
#endif
	void Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_bForceBaking_SetBit(void* Obj)
	{
		((UMDLImporterOptions*)Obj)->bForceBaking = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_bForceBaking = { "bForceBaking", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMDLImporterOptions), &Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_bForceBaking_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_bForceBaking_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_bForceBaking_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMDLImporterOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_BakingResolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_BakingSamples,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_ResourcesDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_ModulesDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_MetersPerSceneUnit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMDLImporterOptions_Statics::NewProp_bForceBaking,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMDLImporterOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMDLImporterOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMDLImporterOptions_Statics::ClassParams = {
		&UMDLImporterOptions::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMDLImporterOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMDLImporterOptions_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UMDLImporterOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMDLImporterOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMDLImporterOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMDLImporterOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMDLImporterOptions, 339443877);
	template<> MDLIMPORTER_API UClass* StaticClass<UMDLImporterOptions>()
	{
		return UMDLImporterOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMDLImporterOptions(Z_Construct_UClass_UMDLImporterOptions, &UMDLImporterOptions::StaticClass, TEXT("/Script/MDLImporter"), TEXT("UMDLImporterOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMDLImporterOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
