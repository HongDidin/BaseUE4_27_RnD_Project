// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MDLImporter/Private/MDLImporterFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMDLImporterFactory() {}
// Cross Module References
	MDLIMPORTER_API UClass* Z_Construct_UClass_UMDLImporterFactory_NoRegister();
	MDLIMPORTER_API UClass* Z_Construct_UClass_UMDLImporterFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_MDLImporter();
// End Cross Module References
	void UMDLImporterFactory::StaticRegisterNativesUMDLImporterFactory()
	{
	}
	UClass* Z_Construct_UClass_UMDLImporterFactory_NoRegister()
	{
		return UMDLImporterFactory::StaticClass();
	}
	struct Z_Construct_UClass_UMDLImporterFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMDLImporterFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_MDLImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMDLImporterFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "MDLImporterFactory.h" },
		{ "ModuleRelativePath", "Private/MDLImporterFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMDLImporterFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMDLImporterFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMDLImporterFactory_Statics::ClassParams = {
		&UMDLImporterFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000020A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMDLImporterFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMDLImporterFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMDLImporterFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMDLImporterFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMDLImporterFactory, 2642473464);
	template<> MDLIMPORTER_API UClass* StaticClass<UMDLImporterFactory>()
	{
		return UMDLImporterFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMDLImporterFactory(Z_Construct_UClass_UMDLImporterFactory, &UMDLImporterFactory::StaticClass, TEXT("/Script/MDLImporter"), TEXT("UMDLImporterFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMDLImporterFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
