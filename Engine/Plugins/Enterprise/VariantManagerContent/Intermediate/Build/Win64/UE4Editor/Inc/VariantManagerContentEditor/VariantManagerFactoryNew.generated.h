// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VARIANTMANAGERCONTENTEDITOR_VariantManagerFactoryNew_generated_h
#error "VariantManagerFactoryNew.generated.h already included, missing '#pragma once' in VariantManagerFactoryNew.h"
#endif
#define VARIANTMANAGERCONTENTEDITOR_VariantManagerFactoryNew_generated_h

#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_SPARSE_DATA
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVariantManagerFactoryNew(); \
	friend struct Z_Construct_UClass_UVariantManagerFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UVariantManagerFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VariantManagerContentEditor"), NO_API) \
	DECLARE_SERIALIZER(UVariantManagerFactoryNew)


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUVariantManagerFactoryNew(); \
	friend struct Z_Construct_UClass_UVariantManagerFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UVariantManagerFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VariantManagerContentEditor"), NO_API) \
	DECLARE_SERIALIZER(UVariantManagerFactoryNew)


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVariantManagerFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVariantManagerFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVariantManagerFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVariantManagerFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVariantManagerFactoryNew(UVariantManagerFactoryNew&&); \
	NO_API UVariantManagerFactoryNew(const UVariantManagerFactoryNew&); \
public:


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVariantManagerFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVariantManagerFactoryNew(UVariantManagerFactoryNew&&); \
	NO_API UVariantManagerFactoryNew(const UVariantManagerFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVariantManagerFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVariantManagerFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVariantManagerFactoryNew)


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_11_PROLOG
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_INCLASS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class VariantManagerFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VARIANTMANAGERCONTENTEDITOR_API UClass* StaticClass<class UVariantManagerFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_VariantManagerFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
