// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VARIANTMANAGERCONTENTEDITOR_LevelVariantSetsActorFactory_generated_h
#error "LevelVariantSetsActorFactory.generated.h already included, missing '#pragma once' in LevelVariantSetsActorFactory.h"
#endif
#define VARIANTMANAGERCONTENTEDITOR_LevelVariantSetsActorFactory_generated_h

#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_SPARSE_DATA
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelVariantSetsActorFactory(); \
	friend struct Z_Construct_UClass_ULevelVariantSetsActorFactory_Statics; \
public: \
	DECLARE_CLASS(ULevelVariantSetsActorFactory, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/VariantManagerContentEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelVariantSetsActorFactory)


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_INCLASS \
private: \
	static void StaticRegisterNativesULevelVariantSetsActorFactory(); \
	friend struct Z_Construct_UClass_ULevelVariantSetsActorFactory_Statics; \
public: \
	DECLARE_CLASS(ULevelVariantSetsActorFactory, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/VariantManagerContentEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelVariantSetsActorFactory)


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelVariantSetsActorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelVariantSetsActorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelVariantSetsActorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelVariantSetsActorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelVariantSetsActorFactory(ULevelVariantSetsActorFactory&&); \
	NO_API ULevelVariantSetsActorFactory(const ULevelVariantSetsActorFactory&); \
public:


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelVariantSetsActorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelVariantSetsActorFactory(ULevelVariantSetsActorFactory&&); \
	NO_API ULevelVariantSetsActorFactory(const ULevelVariantSetsActorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelVariantSetsActorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelVariantSetsActorFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelVariantSetsActorFactory)


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_16_PROLOG
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_SPARSE_DATA \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_INCLASS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_SPARSE_DATA \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h_19_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LevelVariantSetsActorFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VARIANTMANAGERCONTENTEDITOR_API UClass* StaticClass<class ULevelVariantSetsActorFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_LevelVariantSetsActorFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
