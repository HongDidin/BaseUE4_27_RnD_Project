// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VariantManagerContentEditor/Public/SwitchActorFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSwitchActorFactory() {}
// Cross Module References
	VARIANTMANAGERCONTENTEDITOR_API UClass* Z_Construct_UClass_USwitchActorFactory_NoRegister();
	VARIANTMANAGERCONTENTEDITOR_API UClass* Z_Construct_UClass_USwitchActorFactory();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_VariantManagerContentEditor();
// End Cross Module References
	void USwitchActorFactory::StaticRegisterNativesUSwitchActorFactory()
	{
	}
	UClass* Z_Construct_UClass_USwitchActorFactory_NoRegister()
	{
		return USwitchActorFactory::StaticClass();
	}
	struct Z_Construct_UClass_USwitchActorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USwitchActorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_VariantManagerContentEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USwitchActorFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// The only purpose of this class is to trigger a slightly different code path within\n// FLevelEditorViewportClient::TryPlacingActorFromObject when dragging and dropping a SwitchActor into the\n// viewport, so that the SwitchActor labels get sanitized by FActorLabelUtilities::SetActorLabelUnique\n// and don't repeatedly increment\n" },
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "SwitchActorFactory.h" },
		{ "ModuleRelativePath", "Public/SwitchActorFactory.h" },
		{ "ToolTip", "The only purpose of this class is to trigger a slightly different code path within\nFLevelEditorViewportClient::TryPlacingActorFromObject when dragging and dropping a SwitchActor into the\nviewport, so that the SwitchActor labels get sanitized by FActorLabelUtilities::SetActorLabelUnique\nand don't repeatedly increment" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USwitchActorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USwitchActorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USwitchActorFactory_Statics::ClassParams = {
		&USwitchActorFactory::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_USwitchActorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USwitchActorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USwitchActorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USwitchActorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USwitchActorFactory, 2070553137);
	template<> VARIANTMANAGERCONTENTEDITOR_API UClass* StaticClass<USwitchActorFactory>()
	{
		return USwitchActorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USwitchActorFactory(Z_Construct_UClass_USwitchActorFactory, &USwitchActorFactory::StaticClass, TEXT("/Script/VariantManagerContentEditor"), TEXT("USwitchActorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USwitchActorFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
