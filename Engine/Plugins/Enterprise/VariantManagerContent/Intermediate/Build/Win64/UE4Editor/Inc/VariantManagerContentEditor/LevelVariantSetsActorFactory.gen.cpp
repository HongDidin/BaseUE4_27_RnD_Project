// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VariantManagerContentEditor/Public/LevelVariantSetsActorFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelVariantSetsActorFactory() {}
// Cross Module References
	VARIANTMANAGERCONTENTEDITOR_API UClass* Z_Construct_UClass_ULevelVariantSetsActorFactory_NoRegister();
	VARIANTMANAGERCONTENTEDITOR_API UClass* Z_Construct_UClass_ULevelVariantSetsActorFactory();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_VariantManagerContentEditor();
// End Cross Module References
	void ULevelVariantSetsActorFactory::StaticRegisterNativesULevelVariantSetsActorFactory()
	{
	}
	UClass* Z_Construct_UClass_ULevelVariantSetsActorFactory_NoRegister()
	{
		return ULevelVariantSetsActorFactory::StaticClass();
	}
	struct Z_Construct_UClass_ULevelVariantSetsActorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelVariantSetsActorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_VariantManagerContentEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelVariantSetsActorFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "LevelVariantSetsActorFactory.h" },
		{ "ModuleRelativePath", "Public/LevelVariantSetsActorFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelVariantSetsActorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelVariantSetsActorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelVariantSetsActorFactory_Statics::ClassParams = {
		&ULevelVariantSetsActorFactory::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000030ACu,
		METADATA_PARAMS(Z_Construct_UClass_ULevelVariantSetsActorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelVariantSetsActorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelVariantSetsActorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelVariantSetsActorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelVariantSetsActorFactory, 3955115915);
	template<> VARIANTMANAGERCONTENTEDITOR_API UClass* StaticClass<ULevelVariantSetsActorFactory>()
	{
		return ULevelVariantSetsActorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelVariantSetsActorFactory(Z_Construct_UClass_ULevelVariantSetsActorFactory, &ULevelVariantSetsActorFactory::StaticClass, TEXT("/Script/VariantManagerContentEditor"), TEXT("ULevelVariantSetsActorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelVariantSetsActorFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
