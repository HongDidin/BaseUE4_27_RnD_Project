// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VariantManagerContentEditor/Public/VariantManagerTestActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVariantManagerTestActor() {}
// Cross Module References
	VARIANTMANAGERCONTENTEDITOR_API UEnum* Z_Construct_UEnum_VariantManagerContentEditor_EVariantManagerTestEnum();
	UPackage* Z_Construct_UPackage__Script_VariantManagerContentEditor();
	VARIANTMANAGERCONTENTEDITOR_API UClass* Z_Construct_UClass_AVariantManagerTestActor_NoRegister();
	VARIANTMANAGERCONTENTEDITOR_API UClass* Z_Construct_UClass_AVariantManagerTestActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector4();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
// End Cross Module References
	static UEnum* EVariantManagerTestEnum_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VariantManagerContentEditor_EVariantManagerTestEnum, Z_Construct_UPackage__Script_VariantManagerContentEditor(), TEXT("EVariantManagerTestEnum"));
		}
		return Singleton;
	}
	template<> VARIANTMANAGERCONTENTEDITOR_API UEnum* StaticEnum<EVariantManagerTestEnum>()
	{
		return EVariantManagerTestEnum_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVariantManagerTestEnum(EVariantManagerTestEnum_StaticEnum, TEXT("/Script/VariantManagerContentEditor"), TEXT("EVariantManagerTestEnum"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VariantManagerContentEditor_EVariantManagerTestEnum_Hash() { return 737445213U; }
	UEnum* Z_Construct_UEnum_VariantManagerContentEditor_EVariantManagerTestEnum()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VariantManagerContentEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVariantManagerTestEnum"), 0, Get_Z_Construct_UEnum_VariantManagerContentEditor_EVariantManagerTestEnum_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVariantManagerTestEnum::None", (int64)EVariantManagerTestEnum::None },
				{ "EVariantManagerTestEnum::FirstOption", (int64)EVariantManagerTestEnum::FirstOption },
				{ "EVariantManagerTestEnum::SecondOption", (int64)EVariantManagerTestEnum::SecondOption },
				{ "EVariantManagerTestEnum::ThirdOption", (int64)EVariantManagerTestEnum::ThirdOption },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "FirstOption.Name", "EVariantManagerTestEnum::FirstOption" },
				{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
				{ "None.Hidden", "" },
				{ "None.Name", "EVariantManagerTestEnum::None" },
				{ "SecondOption.Name", "EVariantManagerTestEnum::SecondOption" },
				{ "ThirdOption.Name", "EVariantManagerTestEnum::ThirdOption" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VariantManagerContentEditor,
				nullptr,
				"EVariantManagerTestEnum",
				"EVariantManagerTestEnum",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void AVariantManagerTestActor::StaticRegisterNativesAVariantManagerTestActor()
	{
	}
	UClass* Z_Construct_UClass_AVariantManagerTestActor_NoRegister()
	{
		return AVariantManagerTestActor::StaticClass();
	}
	struct Z_Construct_UClass_AVariantManagerTestActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EnumWithNoDefault_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumWithNoDefault_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EnumWithNoDefault;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EnumWithSecondDefault_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumWithSecondDefault_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EnumWithSecondDefault;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedByteProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CapturedByteProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedIntProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CapturedIntProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedFloatProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CapturedFloatProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCapturedBoolProperty_MetaData[];
#endif
		static void NewProp_bCapturedBoolProperty_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCapturedBoolProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedObjectProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CapturedObjectProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedInterfaceProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_CapturedInterfaceProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedNameProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CapturedNameProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedStrProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CapturedStrProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedTextProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_CapturedTextProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedRotatorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedRotatorProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedColorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedColorProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedLinearColorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedLinearColorProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedVectorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedVectorProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedQuatProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedQuatProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedVector4Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedVector4Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedVector2DProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedVector2DProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedIntPointProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedIntPointProperty;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CapturedUObjectArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedUObjectArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CapturedUObjectArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedVectorArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedVectorArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CapturedVectorArrayProperty;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVariantManagerTestActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_VariantManagerContentEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Physics LOD Activation Input Actor Cooking" },
		{ "IncludePath", "VariantManagerTestActor.h" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithNoDefault_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithNoDefault_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured byte property" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithNoDefault = { "EnumWithNoDefault", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, EnumWithNoDefault), Z_Construct_UEnum_VariantManagerContentEditor_EVariantManagerTestEnum, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithNoDefault_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithNoDefault_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithSecondDefault_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithSecondDefault_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured byte property" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithSecondDefault = { "EnumWithSecondDefault", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, EnumWithSecondDefault), Z_Construct_UEnum_VariantManagerContentEditor_EVariantManagerTestEnum, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithSecondDefault_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithSecondDefault_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedByteProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured byte property" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedByteProperty = { "CapturedByteProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedByteProperty), nullptr, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedByteProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedByteProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedIntProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured int32 property" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedIntProperty = { "CapturedIntProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedIntProperty), METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedIntProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedIntProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedFloatProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured float property" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedFloatProperty = { "CapturedFloatProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedFloatProperty), METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedFloatProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedFloatProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_bCapturedBoolProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured boolean property" },
	};
#endif
	void Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_bCapturedBoolProperty_SetBit(void* Obj)
	{
		((AVariantManagerTestActor*)Obj)->bCapturedBoolProperty = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_bCapturedBoolProperty = { "bCapturedBoolProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AVariantManagerTestActor), &Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_bCapturedBoolProperty_SetBit, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_bCapturedBoolProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_bCapturedBoolProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedObjectProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured UObject property" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedObjectProperty = { "CapturedObjectProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedObjectProperty), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedObjectProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedObjectProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedInterfaceProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured UInterface property" },
	};
#endif
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedInterfaceProperty = { "CapturedInterfaceProperty", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedInterfaceProperty), Z_Construct_UClass_UInterface, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedInterfaceProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedInterfaceProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedNameProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FName property" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedNameProperty = { "CapturedNameProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedNameProperty), METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedNameProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedNameProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedStrProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FString property" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedStrProperty = { "CapturedStrProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedStrProperty), METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedStrProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedStrProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedTextProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FText property" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedTextProperty = { "CapturedTextProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedTextProperty), METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedTextProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedTextProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedRotatorProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FRotator property" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedRotatorProperty = { "CapturedRotatorProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedRotatorProperty), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedRotatorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedRotatorProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedColorProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FColor property" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedColorProperty = { "CapturedColorProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedColorProperty), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedColorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedColorProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedLinearColorProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FLinearColor property" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedLinearColorProperty = { "CapturedLinearColorProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedLinearColorProperty), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedLinearColorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedLinearColorProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FVector property" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorProperty = { "CapturedVectorProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedVectorProperty), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedQuatProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FQuat property" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedQuatProperty = { "CapturedQuatProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedQuatProperty), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedQuatProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedQuatProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVector4Property_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FVector4 property" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVector4Property = { "CapturedVector4Property", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedVector4Property), Z_Construct_UScriptStruct_FVector4, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVector4Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVector4Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVector2DProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FVector2D property" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVector2DProperty = { "CapturedVector2DProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedVector2DProperty), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVector2DProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVector2DProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedIntPointProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FIntPoint property" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedIntPointProperty = { "CapturedIntPointProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedIntPointProperty), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedIntPointProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedIntPointProperty_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedUObjectArrayProperty_Inner = { "CapturedUObjectArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedUObjectArrayProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured UObject array property" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedUObjectArrayProperty = { "CapturedUObjectArrayProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedUObjectArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedUObjectArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedUObjectArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorArrayProperty_Inner = { "CapturedVectorArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorArrayProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Public/VariantManagerTestActor.h" },
		{ "ToolTip", "Captured FVector array property" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorArrayProperty = { "CapturedVectorArrayProperty", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVariantManagerTestActor, CapturedVectorArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AVariantManagerTestActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithNoDefault_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithNoDefault,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithSecondDefault_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_EnumWithSecondDefault,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedByteProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedIntProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedFloatProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_bCapturedBoolProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedObjectProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedInterfaceProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedNameProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedStrProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedTextProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedRotatorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedColorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedLinearColorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedQuatProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVector4Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVector2DProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedIntPointProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedUObjectArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedUObjectArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVariantManagerTestActor_Statics::NewProp_CapturedVectorArrayProperty,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVariantManagerTestActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVariantManagerTestActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AVariantManagerTestActor_Statics::ClassParams = {
		&AVariantManagerTestActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AVariantManagerTestActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AVariantManagerTestActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AVariantManagerTestActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVariantManagerTestActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AVariantManagerTestActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AVariantManagerTestActor, 3858621736);
	template<> VARIANTMANAGERCONTENTEDITOR_API UClass* StaticClass<AVariantManagerTestActor>()
	{
		return AVariantManagerTestActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AVariantManagerTestActor(Z_Construct_UClass_AVariantManagerTestActor, &AVariantManagerTestActor::StaticClass, TEXT("/Script/VariantManagerContentEditor"), TEXT("AVariantManagerTestActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AVariantManagerTestActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
