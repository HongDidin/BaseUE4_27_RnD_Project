// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VARIANTMANAGERCONTENTEDITOR_SwitchActorFactory_generated_h
#error "SwitchActorFactory.generated.h already included, missing '#pragma once' in SwitchActorFactory.h"
#endif
#define VARIANTMANAGERCONTENTEDITOR_SwitchActorFactory_generated_h

#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_SPARSE_DATA
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSwitchActorFactory(); \
	friend struct Z_Construct_UClass_USwitchActorFactory_Statics; \
public: \
	DECLARE_CLASS(USwitchActorFactory, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/VariantManagerContentEditor"), VARIANTMANAGERCONTENTEDITOR_API) \
	DECLARE_SERIALIZER(USwitchActorFactory)


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUSwitchActorFactory(); \
	friend struct Z_Construct_UClass_USwitchActorFactory_Statics; \
public: \
	DECLARE_CLASS(USwitchActorFactory, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/VariantManagerContentEditor"), VARIANTMANAGERCONTENTEDITOR_API) \
	DECLARE_SERIALIZER(USwitchActorFactory)


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	VARIANTMANAGERCONTENTEDITOR_API USwitchActorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USwitchActorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VARIANTMANAGERCONTENTEDITOR_API, USwitchActorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USwitchActorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VARIANTMANAGERCONTENTEDITOR_API USwitchActorFactory(USwitchActorFactory&&); \
	VARIANTMANAGERCONTENTEDITOR_API USwitchActorFactory(const USwitchActorFactory&); \
public:


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	VARIANTMANAGERCONTENTEDITOR_API USwitchActorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VARIANTMANAGERCONTENTEDITOR_API USwitchActorFactory(USwitchActorFactory&&); \
	VARIANTMANAGERCONTENTEDITOR_API USwitchActorFactory(const USwitchActorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VARIANTMANAGERCONTENTEDITOR_API, USwitchActorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USwitchActorFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USwitchActorFactory)


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_14_PROLOG
#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_SPARSE_DATA \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_INCLASS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_SPARSE_DATA \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SwitchActorFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VARIANTMANAGERCONTENTEDITOR_API UClass* StaticClass<class USwitchActorFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_VariantManagerContent_Source_VariantManagerContentEditor_Public_SwitchActorFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
