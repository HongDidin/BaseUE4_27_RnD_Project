// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VariantManagerContentEditor/Public/VariantManagerFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVariantManagerFactoryNew() {}
// Cross Module References
	VARIANTMANAGERCONTENTEDITOR_API UClass* Z_Construct_UClass_UVariantManagerFactoryNew_NoRegister();
	VARIANTMANAGERCONTENTEDITOR_API UClass* Z_Construct_UClass_UVariantManagerFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_VariantManagerContentEditor();
// End Cross Module References
	void UVariantManagerFactoryNew::StaticRegisterNativesUVariantManagerFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UVariantManagerFactoryNew_NoRegister()
	{
		return UVariantManagerFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UVariantManagerFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVariantManagerFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_VariantManagerContentEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVariantManagerFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "VariantManagerFactoryNew.h" },
		{ "ModuleRelativePath", "Public/VariantManagerFactoryNew.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVariantManagerFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVariantManagerFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVariantManagerFactoryNew_Statics::ClassParams = {
		&UVariantManagerFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVariantManagerFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVariantManagerFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVariantManagerFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVariantManagerFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVariantManagerFactoryNew, 698420392);
	template<> VARIANTMANAGERCONTENTEDITOR_API UClass* StaticClass<UVariantManagerFactoryNew>()
	{
		return UVariantManagerFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVariantManagerFactoryNew(Z_Construct_UClass_UVariantManagerFactoryNew, &UVariantManagerFactoryNew::StaticClass, TEXT("/Script/VariantManagerContentEditor"), TEXT("UVariantManagerFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVariantManagerFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
