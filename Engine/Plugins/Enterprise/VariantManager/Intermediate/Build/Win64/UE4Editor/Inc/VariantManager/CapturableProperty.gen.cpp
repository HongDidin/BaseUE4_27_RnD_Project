// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VariantManager/Public/CapturableProperty.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCapturableProperty() {}
// Cross Module References
	VARIANTMANAGER_API UScriptStruct* Z_Construct_UScriptStruct_FCapturableProperty();
	UPackage* Z_Construct_UPackage__Script_VariantManager();
// End Cross Module References
class UScriptStruct* FCapturableProperty::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VARIANTMANAGER_API uint32 Get_Z_Construct_UScriptStruct_FCapturableProperty_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCapturableProperty, Z_Construct_UPackage__Script_VariantManager(), TEXT("CapturableProperty"), sizeof(FCapturableProperty), Get_Z_Construct_UScriptStruct_FCapturableProperty_Hash());
	}
	return Singleton;
}
template<> VARIANTMANAGER_API UScriptStruct* StaticStruct<FCapturableProperty>()
{
	return FCapturableProperty::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCapturableProperty(FCapturableProperty::StaticStruct, TEXT("/Script/VariantManager"), TEXT("CapturableProperty"), false, nullptr, nullptr);
static struct FScriptStruct_VariantManager_StaticRegisterNativesFCapturableProperty
{
	FScriptStruct_VariantManager_StaticRegisterNativesFCapturableProperty()
	{
		UScriptStruct::DeferCppStructOps<FCapturableProperty>(FName(TEXT("CapturableProperty")));
	}
} ScriptStruct_VariantManager_StaticRegisterNativesFCapturableProperty;
	struct Z_Construct_UScriptStruct_FCapturableProperty_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturableProperty_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Describes a property path that can be captured. It just exposes a display name but\n// uses internal data in order to be able to capture exception properties, like materials\n" },
		{ "ModuleRelativePath", "Public/CapturableProperty.h" },
		{ "ToolTip", "Describes a property path that can be captured. It just exposes a display name but\nuses internal data in order to be able to capture exception properties, like materials" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCapturableProperty_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCapturableProperty>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturableProperty_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "VariantManager" },
		{ "ModuleRelativePath", "Public/CapturableProperty.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCapturableProperty_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturableProperty, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturableProperty_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturableProperty_Statics::NewProp_DisplayName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCapturableProperty_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturableProperty_Statics::NewProp_DisplayName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCapturableProperty_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VariantManager,
		nullptr,
		&NewStructOps,
		"CapturableProperty",
		sizeof(FCapturableProperty),
		alignof(FCapturableProperty),
		Z_Construct_UScriptStruct_FCapturableProperty_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturableProperty_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturableProperty_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturableProperty_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCapturableProperty()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCapturableProperty_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VariantManager();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CapturableProperty"), sizeof(FCapturableProperty), Get_Z_Construct_UScriptStruct_FCapturableProperty_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCapturableProperty_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCapturableProperty_Hash() { return 413264085U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
