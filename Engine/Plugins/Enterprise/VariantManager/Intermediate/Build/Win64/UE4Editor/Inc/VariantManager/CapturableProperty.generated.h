// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VARIANTMANAGER_CapturableProperty_generated_h
#error "CapturableProperty.generated.h already included, missing '#pragma once' in CapturableProperty.h"
#endif
#define VARIANTMANAGER_CapturableProperty_generated_h

#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Public_CapturableProperty_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCapturableProperty_Statics; \
	VARIANTMANAGER_API static class UScriptStruct* StaticStruct();


template<> VARIANTMANAGER_API UScriptStruct* StaticStruct<struct FCapturableProperty>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Public_CapturableProperty_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
