// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VariantManager/Private/PropertyTemplateObject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePropertyTemplateObject() {}
// Cross Module References
	VARIANTMANAGER_API UClass* Z_Construct_UClass_UPropertyTemplateObject_NoRegister();
	VARIANTMANAGER_API UClass* Z_Construct_UClass_UPropertyTemplateObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_VariantManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void UPropertyTemplateObject::StaticRegisterNativesUPropertyTemplateObject()
	{
	}
	UClass* Z_Construct_UClass_UPropertyTemplateObject_NoRegister()
	{
		return UPropertyTemplateObject::StaticClass();
	}
	struct Z_Construct_UClass_UPropertyTemplateObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedByteProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CapturedByteProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedUInt16Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_CapturedUInt16Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedUInt32Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_CapturedUInt32Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedUInt64Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_CapturedUInt64Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedInt8Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_CapturedInt8Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedInt16Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_CapturedInt16Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedIntProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CapturedIntProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedInt64Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_CapturedInt64Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedFloatProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CapturedFloatProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedDoubleProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_CapturedDoubleProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCapturedBoolProperty_MetaData[];
#endif
		static void NewProp_bCapturedBoolProperty_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCapturedBoolProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedObjectProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CapturedObjectProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedSoftObjectProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_CapturedSoftObjectProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedInterfaceProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_CapturedInterfaceProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedNameProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CapturedNameProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedStrProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CapturedStrProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedTextProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_CapturedTextProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedVectorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedVectorProperty;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPropertyTemplateObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_VariantManager,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\nIn order to use PropertyEditorModule.CreateSingleProperty we have to give it an object instance\nand the name of the target property to edit. It will then iterate the object for a property with that\nname and create a property editor widget.\n\nThis is very limiting when editing a single entry within an FArrayProperty, as the inner and the\narray prop will have the same name, leading it to create an array editor. Also, since we have to\ngive it an instance, modifying the widget will automatically modify the object, which we may not\nwant, we may just want a property editor of a particular type.\n\nThis class is a hack around all that: It has an instance of most property types,\nso that you can instantiate one of these and just pass along the name of the property type you want.\nThey are all be named Captured<propertyType> (e.g. CapturedFloatProperty, CapturedObjectProperty,\nbCapturedBoolProperty) but you can use the helper function to get the name of the property you want.\n*/// TODO: Convert this into a static dictionary that maps to a small separate class for each property type\n// Maybe even template it for array/map/set property types\n" },
		{ "IncludePath", "PropertyTemplateObject.h" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "In order to use PropertyEditorModule.CreateSingleProperty we have to give it an object instance\nand the name of the target property to edit. It will then iterate the object for a property with that\nname and create a property editor widget.\n\nThis is very limiting when editing a single entry within an FArrayProperty, as the inner and the\narray prop will have the same name, leading it to create an array editor. Also, since we have to\ngive it an instance, modifying the widget will automatically modify the object, which we may not\nwant, we may just want a property editor of a particular type.\n\nThis class is a hack around all that: It has an instance of most property types,\nso that you can instantiate one of these and just pass along the name of the property type you want.\nThey are all be named Captured<propertyType> (e.g. CapturedFloatProperty, CapturedObjectProperty,\nbCapturedBoolProperty) but you can use the helper function to get the name of the property you want.\n// TODO: Convert this into a static dictionary that maps to a small separate class for each property type\n// Maybe even template it for array/map/set property types" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedByteProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured byte property" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedByteProperty = { "CapturedByteProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedByteProperty), nullptr, METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedByteProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedByteProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt16Property_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured uint16 property" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt16Property = { "CapturedUInt16Property", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedUInt16Property), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt16Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt16Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt32Property_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured uint32 property" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt32Property = { "CapturedUInt32Property", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedUInt32Property), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt32Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt32Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt64Property_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured uint16 property" },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt64Property = { "CapturedUInt64Property", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedUInt64Property), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt64Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt64Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt8Property_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured int8 property" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt8Property = { "CapturedInt8Property", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedInt8Property), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt8Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt8Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt16Property_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured int16 property" },
	};
#endif
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt16Property = { "CapturedInt16Property", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedInt16Property), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt16Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt16Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedIntProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured int32 property" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedIntProperty = { "CapturedIntProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedIntProperty), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedIntProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedIntProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt64Property_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured int64 property" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt64Property = { "CapturedInt64Property", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedInt64Property), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt64Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt64Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedFloatProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured float property" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedFloatProperty = { "CapturedFloatProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedFloatProperty), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedFloatProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedFloatProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedDoubleProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured double property" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedDoubleProperty = { "CapturedDoubleProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedDoubleProperty), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedDoubleProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedDoubleProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_bCapturedBoolProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured boolean property" },
	};
#endif
	void Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_bCapturedBoolProperty_SetBit(void* Obj)
	{
		((UPropertyTemplateObject*)Obj)->bCapturedBoolProperty = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_bCapturedBoolProperty = { "bCapturedBoolProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPropertyTemplateObject), &Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_bCapturedBoolProperty_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_bCapturedBoolProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_bCapturedBoolProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedObjectProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured UObject property" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedObjectProperty = { "CapturedObjectProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedObjectProperty), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedObjectProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedObjectProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedSoftObjectProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured Soft UObject property" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedSoftObjectProperty = { "CapturedSoftObjectProperty", nullptr, (EPropertyFlags)0x0014000000002001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedSoftObjectProperty), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedSoftObjectProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedSoftObjectProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInterfaceProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured UInterface property" },
	};
#endif
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInterfaceProperty = { "CapturedInterfaceProperty", nullptr, (EPropertyFlags)0x0014000000002001, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedInterfaceProperty), Z_Construct_UClass_UInterface, METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInterfaceProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInterfaceProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedNameProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured FName property" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedNameProperty = { "CapturedNameProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedNameProperty), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedNameProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedNameProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedStrProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured FString property" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedStrProperty = { "CapturedStrProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedStrProperty), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedStrProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedStrProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedTextProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured FText property" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedTextProperty = { "CapturedTextProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedTextProperty), METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedTextProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedTextProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedVectorProperty_MetaData[] = {
		{ "Category", "Template" },
		{ "ModuleRelativePath", "Private/PropertyTemplateObject.h" },
		{ "ToolTip", "Captured FVector property" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedVectorProperty = { "CapturedVectorProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyTemplateObject, CapturedVectorProperty), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedVectorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedVectorProperty_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPropertyTemplateObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedByteProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt16Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt32Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedUInt64Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt8Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt16Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedIntProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInt64Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedFloatProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedDoubleProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_bCapturedBoolProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedObjectProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedSoftObjectProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedInterfaceProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedNameProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedStrProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedTextProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyTemplateObject_Statics::NewProp_CapturedVectorProperty,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPropertyTemplateObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPropertyTemplateObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPropertyTemplateObject_Statics::ClassParams = {
		&UPropertyTemplateObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPropertyTemplateObject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::PropPointers),
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPropertyTemplateObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyTemplateObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPropertyTemplateObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPropertyTemplateObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPropertyTemplateObject, 1272373636);
	template<> VARIANTMANAGER_API UClass* StaticClass<UPropertyTemplateObject>()
	{
		return UPropertyTemplateObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPropertyTemplateObject(Z_Construct_UClass_UPropertyTemplateObject, &UPropertyTemplateObject::StaticClass, TEXT("/Script/VariantManager"), TEXT("UPropertyTemplateObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPropertyTemplateObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
