// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VARIANTMANAGER_PropertyTemplateObject_generated_h
#error "PropertyTemplateObject.generated.h already included, missing '#pragma once' in PropertyTemplateObject.h"
#endif
#define VARIANTMANAGER_PropertyTemplateObject_generated_h

#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_SPARSE_DATA
#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPropertyTemplateObject(); \
	friend struct Z_Construct_UClass_UPropertyTemplateObject_Statics; \
public: \
	DECLARE_CLASS(UPropertyTemplateObject, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/VariantManager"), NO_API) \
	DECLARE_SERIALIZER(UPropertyTemplateObject)


#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUPropertyTemplateObject(); \
	friend struct Z_Construct_UClass_UPropertyTemplateObject_Statics; \
public: \
	DECLARE_CLASS(UPropertyTemplateObject, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/VariantManager"), NO_API) \
	DECLARE_SERIALIZER(UPropertyTemplateObject)


#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPropertyTemplateObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPropertyTemplateObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPropertyTemplateObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPropertyTemplateObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPropertyTemplateObject(UPropertyTemplateObject&&); \
	NO_API UPropertyTemplateObject(const UPropertyTemplateObject&); \
public:


#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPropertyTemplateObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPropertyTemplateObject(UPropertyTemplateObject&&); \
	NO_API UPropertyTemplateObject(const UPropertyTemplateObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPropertyTemplateObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPropertyTemplateObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPropertyTemplateObject)


#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_29_PROLOG
#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_SPARSE_DATA \
	Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_INCLASS \
	Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_SPARSE_DATA \
	Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h_32_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PropertyTemplateObject."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VARIANTMANAGER_API UClass* StaticClass<class UPropertyTemplateObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_VariantManager_Source_VariantManager_Private_PropertyTemplateObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
