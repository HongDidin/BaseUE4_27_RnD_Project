// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VariantManager/Private/DisplayNodes/VariantManagerDisplayNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVariantManagerDisplayNode() {}
// Cross Module References
	VARIANTMANAGER_API UEnum* Z_Construct_UEnum_VariantManager_EVariantManagerNodeType();
	UPackage* Z_Construct_UPackage__Script_VariantManager();
// End Cross Module References
	static UEnum* EVariantManagerNodeType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VariantManager_EVariantManagerNodeType, Z_Construct_UPackage__Script_VariantManager(), TEXT("EVariantManagerNodeType"));
		}
		return Singleton;
	}
	template<> VARIANTMANAGER_API UEnum* StaticEnum<EVariantManagerNodeType>()
	{
		return EVariantManagerNodeType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVariantManagerNodeType(EVariantManagerNodeType_StaticEnum, TEXT("/Script/VariantManager"), TEXT("EVariantManagerNodeType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VariantManager_EVariantManagerNodeType_Hash() { return 4032019619U; }
	UEnum* Z_Construct_UEnum_VariantManager_EVariantManagerNodeType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VariantManager();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVariantManagerNodeType"), 0, Get_Z_Construct_UEnum_VariantManager_EVariantManagerNodeType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVariantManagerNodeType::VariantSet", (int64)EVariantManagerNodeType::VariantSet },
				{ "EVariantManagerNodeType::Variant", (int64)EVariantManagerNodeType::Variant },
				{ "EVariantManagerNodeType::Actor", (int64)EVariantManagerNodeType::Actor },
				{ "EVariantManagerNodeType::Property", (int64)EVariantManagerNodeType::Property },
				{ "EVariantManagerNodeType::Function", (int64)EVariantManagerNodeType::Function },
				{ "EVariantManagerNodeType::Spacer", (int64)EVariantManagerNodeType::Spacer },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Actor.Name", "EVariantManagerNodeType::Actor" },
				{ "Function.Name", "EVariantManagerNodeType::Function" },
				{ "ModuleRelativePath", "Private/DisplayNodes/VariantManagerDisplayNode.h" },
				{ "Property.Name", "EVariantManagerNodeType::Property" },
				{ "Spacer.Name", "EVariantManagerNodeType::Spacer" },
				{ "Variant.Name", "EVariantManagerNodeType::Variant" },
				{ "VariantSet.Name", "EVariantManagerNodeType::VariantSet" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VariantManager,
				nullptr,
				"EVariantManagerNodeType",
				"EVariantManagerNodeType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
