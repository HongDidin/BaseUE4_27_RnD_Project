// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithIFCTranslator/Private/DatasmithIFCImportOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithIFCImportOptions() {}
// Cross Module References
	DATASMITHIFCTRANSLATOR_API UClass* Z_Construct_UClass_UDatasmithIFCImportOptions_NoRegister();
	DATASMITHIFCTRANSLATOR_API UClass* Z_Construct_UClass_UDatasmithIFCImportOptions();
	DATASMITHCONTENT_API UClass* Z_Construct_UClass_UDatasmithOptionsBase();
	UPackage* Z_Construct_UPackage__Script_DatasmithIFCTranslator();
// End Cross Module References
	void UDatasmithIFCImportOptions::StaticRegisterNativesUDatasmithIFCImportOptions()
	{
	}
	UClass* Z_Construct_UClass_UDatasmithIFCImportOptions_NoRegister()
	{
		return UDatasmithIFCImportOptions::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithIFCImportOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithIFCImportOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDatasmithOptionsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithIFCTranslator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithIFCImportOptions_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "DebugProperty" },
		{ "IncludePath", "DatasmithIFCImportOptions.h" },
		{ "ModuleRelativePath", "Private/DatasmithIFCImportOptions.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithIFCImportOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithIFCImportOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithIFCImportOptions_Statics::ClassParams = {
		&UDatasmithIFCImportOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithIFCImportOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithIFCImportOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithIFCImportOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithIFCImportOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithIFCImportOptions, 506755565);
	template<> DATASMITHIFCTRANSLATOR_API UClass* StaticClass<UDatasmithIFCImportOptions>()
	{
		return UDatasmithIFCImportOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithIFCImportOptions(Z_Construct_UClass_UDatasmithIFCImportOptions, &UDatasmithIFCImportOptions::StaticClass, TEXT("/Script/DatasmithIFCTranslator"), TEXT("UDatasmithIFCImportOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithIFCImportOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
