// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHIFCTRANSLATOR_DatasmithIFCImportOptions_generated_h
#error "DatasmithIFCImportOptions.generated.h already included, missing '#pragma once' in DatasmithIFCImportOptions.h"
#endif
#define DATASMITHIFCTRANSLATOR_DatasmithIFCImportOptions_generated_h

#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithIFCImportOptions(); \
	friend struct Z_Construct_UClass_UDatasmithIFCImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDatasmithIFCImportOptions, UDatasmithOptionsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithIFCTranslator"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithIFCImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithIFCImportOptions(); \
	friend struct Z_Construct_UClass_UDatasmithIFCImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDatasmithIFCImportOptions, UDatasmithOptionsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithIFCTranslator"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithIFCImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithIFCImportOptions(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithIFCImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithIFCImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithIFCImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithIFCImportOptions(UDatasmithIFCImportOptions&&); \
	NO_API UDatasmithIFCImportOptions(const UDatasmithIFCImportOptions&); \
public:


#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithIFCImportOptions(UDatasmithIFCImportOptions&&); \
	NO_API UDatasmithIFCImportOptions(const UDatasmithIFCImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithIFCImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithIFCImportOptions); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDatasmithIFCImportOptions)


#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_14_PROLOG
#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_INCLASS \
	Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHIFCTRANSLATOR_API UClass* StaticClass<class UDatasmithIFCImportOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithIFCImporter_Source_DatasmithIFCTranslator_Private_DatasmithIFCImportOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
