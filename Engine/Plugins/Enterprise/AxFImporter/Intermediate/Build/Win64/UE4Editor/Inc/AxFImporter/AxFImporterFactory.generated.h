// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AXFIMPORTER_AxFImporterFactory_generated_h
#error "AxFImporterFactory.generated.h already included, missing '#pragma once' in AxFImporterFactory.h"
#endif
#define AXFIMPORTER_AxFImporterFactory_generated_h

#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_SPARSE_DATA
#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAxFImporterFactory(); \
	friend struct Z_Construct_UClass_UAxFImporterFactory_Statics; \
public: \
	DECLARE_CLASS(UAxFImporterFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AxFImporter"), NO_API) \
	DECLARE_SERIALIZER(UAxFImporterFactory)


#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUAxFImporterFactory(); \
	friend struct Z_Construct_UClass_UAxFImporterFactory_Statics; \
public: \
	DECLARE_CLASS(UAxFImporterFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AxFImporter"), NO_API) \
	DECLARE_SERIALIZER(UAxFImporterFactory)


#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAxFImporterFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAxFImporterFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAxFImporterFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAxFImporterFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAxFImporterFactory(UAxFImporterFactory&&); \
	NO_API UAxFImporterFactory(const UAxFImporterFactory&); \
public:


#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAxFImporterFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAxFImporterFactory(UAxFImporterFactory&&); \
	NO_API UAxFImporterFactory(const UAxFImporterFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAxFImporterFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAxFImporterFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAxFImporterFactory)


#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_16_PROLOG
#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_SPARSE_DATA \
	Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_INCLASS \
	Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_SPARSE_DATA \
	Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h_19_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AxFImporterFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AXFIMPORTER_API UClass* StaticClass<class UAxFImporterFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_AxFImporter_Source_AxFImporter_Private_AxFImporterFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
