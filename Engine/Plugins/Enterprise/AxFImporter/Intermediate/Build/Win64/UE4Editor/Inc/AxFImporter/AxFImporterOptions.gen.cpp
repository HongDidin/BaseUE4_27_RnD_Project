// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AxFImporter/Private/AxFImporterOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAxFImporterOptions() {}
// Cross Module References
	AXFIMPORTER_API UClass* Z_Construct_UClass_UAxFImporterOptions_NoRegister();
	AXFIMPORTER_API UClass* Z_Construct_UClass_UAxFImporterOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_AxFImporter();
// End Cross Module References
	void UAxFImporterOptions::StaticRegisterNativesUAxFImporterOptions()
	{
	}
	UClass* Z_Construct_UClass_UAxFImporterOptions_NoRegister()
	{
		return UAxFImporterOptions::StaticClass();
	}
	struct Z_Construct_UClass_UAxFImporterOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MetersPerSceneUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MetersPerSceneUnit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAxFImporterOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_AxFImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAxFImporterOptions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AxFImporterOptions.h" },
		{ "ModuleRelativePath", "Private/AxFImporterOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAxFImporterOptions_Statics::NewProp_MetersPerSceneUnit_MetaData[] = {
		{ "ClampMax", "1000" },
		{ "ClampMin", "0.010000" },
		{ "DisplayName", "Meters per scene unit" },
		{ "ModuleRelativePath", "Private/AxFImporterOptions.h" },
		{ "ToolTip", "The conversion ratio between meters and scene units for materials." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UAxFImporterOptions_Statics::NewProp_MetersPerSceneUnit = { "MetersPerSceneUnit", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAxFImporterOptions, MetersPerSceneUnit), METADATA_PARAMS(Z_Construct_UClass_UAxFImporterOptions_Statics::NewProp_MetersPerSceneUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAxFImporterOptions_Statics::NewProp_MetersPerSceneUnit_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAxFImporterOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAxFImporterOptions_Statics::NewProp_MetersPerSceneUnit,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAxFImporterOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAxFImporterOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAxFImporterOptions_Statics::ClassParams = {
		&UAxFImporterOptions::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAxFImporterOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAxFImporterOptions_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UAxFImporterOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAxFImporterOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAxFImporterOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAxFImporterOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAxFImporterOptions, 3033095595);
	template<> AXFIMPORTER_API UClass* StaticClass<UAxFImporterOptions>()
	{
		return UAxFImporterOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAxFImporterOptions(Z_Construct_UClass_UAxFImporterOptions, &UAxFImporterOptions::StaticClass, TEXT("/Script/AxFImporter"), TEXT("UAxFImporterOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAxFImporterOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
