// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AxFImporter/Private/AxFImporterFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAxFImporterFactory() {}
// Cross Module References
	AXFIMPORTER_API UClass* Z_Construct_UClass_UAxFImporterFactory_NoRegister();
	AXFIMPORTER_API UClass* Z_Construct_UClass_UAxFImporterFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_AxFImporter();
// End Cross Module References
	void UAxFImporterFactory::StaticRegisterNativesUAxFImporterFactory()
	{
	}
	UClass* Z_Construct_UClass_UAxFImporterFactory_NoRegister()
	{
		return UAxFImporterFactory::StaticClass();
	}
	struct Z_Construct_UClass_UAxFImporterFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAxFImporterFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_AxFImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAxFImporterFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "AxFImporterFactory.h" },
		{ "ModuleRelativePath", "Private/AxFImporterFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAxFImporterFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAxFImporterFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAxFImporterFactory_Statics::ClassParams = {
		&UAxFImporterFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000020A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAxFImporterFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAxFImporterFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAxFImporterFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAxFImporterFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAxFImporterFactory, 3722455072);
	template<> AXFIMPORTER_API UClass* StaticClass<UAxFImporterFactory>()
	{
		return UAxFImporterFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAxFImporterFactory(Z_Construct_UClass_UAxFImporterFactory, &UAxFImporterFactory::StaticClass, TEXT("/Script/AxFImporter"), TEXT("UAxFImporterFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAxFImporterFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
