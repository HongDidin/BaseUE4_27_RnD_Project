// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHOPENNURBSTRANSLATOR_DatasmithOpenNurbsImportOptions_generated_h
#error "DatasmithOpenNurbsImportOptions.generated.h already included, missing '#pragma once' in DatasmithOpenNurbsImportOptions.h"
#endif
#define DATASMITHOPENNURBSTRANSLATOR_DatasmithOpenNurbsImportOptions_generated_h

#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_30_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics; \
	DATASMITHOPENNURBSTRANSLATOR_API static class UScriptStruct* StaticStruct(); \
	typedef FDatasmithTessellationOptions Super;


template<> DATASMITHOPENNURBSTRANSLATOR_API UScriptStruct* StaticStruct<struct FDatasmithOpenNurbsOptions>();

#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithOpenNurbsImportOptions(); \
	friend struct Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDatasmithOpenNurbsImportOptions, UDatasmithOptionsBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithOpenNurbsTranslator"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithOpenNurbsImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithOpenNurbsImportOptions(); \
	friend struct Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDatasmithOpenNurbsImportOptions, UDatasmithOptionsBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithOpenNurbsTranslator"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithOpenNurbsImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithOpenNurbsImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithOpenNurbsImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithOpenNurbsImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithOpenNurbsImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithOpenNurbsImportOptions(UDatasmithOpenNurbsImportOptions&&); \
	NO_API UDatasmithOpenNurbsImportOptions(const UDatasmithOpenNurbsImportOptions&); \
public:


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithOpenNurbsImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithOpenNurbsImportOptions(UDatasmithOpenNurbsImportOptions&&); \
	NO_API UDatasmithOpenNurbsImportOptions(const UDatasmithOpenNurbsImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithOpenNurbsImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithOpenNurbsImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithOpenNurbsImportOptions)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_42_PROLOG
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_INCLASS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h_45_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHOPENNURBSTRANSLATOR_API UClass* StaticClass<class UDatasmithOpenNurbsImportOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithOpenNurbsTranslator_Public_DatasmithOpenNurbsImportOptions_h


#define FOREACH_ENUM_EDATASMITHOPENNURBSBREPTESSELLATEDSOURCE(op) \
	op(EDatasmithOpenNurbsBrepTessellatedSource::UseUnrealNurbsTessellation) \
	op(EDatasmithOpenNurbsBrepTessellatedSource::UseRenderMeshes) 

enum class EDatasmithOpenNurbsBrepTessellatedSource : uint8;
template<> DATASMITHOPENNURBSTRANSLATOR_API UEnum* StaticEnum<EDatasmithOpenNurbsBrepTessellatedSource>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
