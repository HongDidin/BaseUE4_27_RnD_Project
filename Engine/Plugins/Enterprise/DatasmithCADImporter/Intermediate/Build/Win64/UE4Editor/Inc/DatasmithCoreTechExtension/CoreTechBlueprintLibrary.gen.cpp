// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithCoreTechExtension/Public/CoreTechBlueprintLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCoreTechBlueprintLibrary() {}
// Cross Module References
	DATASMITHCORETECHEXTENSION_API UClass* Z_Construct_UClass_UCoreTechBlueprintLibrary_NoRegister();
	DATASMITHCORETECHEXTENSION_API UClass* Z_Construct_UClass_UCoreTechBlueprintLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_DatasmithCoreTechExtension();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	DATASMITHCONTENT_API UScriptStruct* Z_Construct_UScriptStruct_FDatasmithRetessellationOptions();
// End Cross Module References
	DEFINE_FUNCTION(UCoreTechBlueprintLibrary::execRetessellateStaticMeshWithNotification)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_STRUCT_REF(FDatasmithRetessellationOptions,Z_Param_Out_TessellationSettings);
		P_GET_UBOOL(Z_Param_bApplyChanges);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_FailureReason);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UCoreTechBlueprintLibrary::RetessellateStaticMeshWithNotification(Z_Param_StaticMesh,Z_Param_Out_TessellationSettings,Z_Param_bApplyChanges,Z_Param_Out_FailureReason);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCoreTechBlueprintLibrary::execRetessellateStaticMesh)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_STRUCT_REF(FDatasmithRetessellationOptions,Z_Param_Out_TessellationSettings);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_FailureReason);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UCoreTechBlueprintLibrary::RetessellateStaticMesh(Z_Param_StaticMesh,Z_Param_Out_TessellationSettings,Z_Param_Out_FailureReason);
		P_NATIVE_END;
	}
	void UCoreTechBlueprintLibrary::StaticRegisterNativesUCoreTechBlueprintLibrary()
	{
		UClass* Class = UCoreTechBlueprintLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "RetessellateStaticMesh", &UCoreTechBlueprintLibrary::execRetessellateStaticMesh },
			{ "RetessellateStaticMeshWithNotification", &UCoreTechBlueprintLibrary::execRetessellateStaticMeshWithNotification },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics
	{
		struct CoreTechBlueprintLibrary_eventRetessellateStaticMesh_Parms
		{
			UStaticMesh* StaticMesh;
			FDatasmithRetessellationOptions TessellationSettings;
			FText FailureReason;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TessellationSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TessellationSettings;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_FailureReason;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CoreTechBlueprintLibrary_eventRetessellateStaticMesh_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_TessellationSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_TessellationSettings = { "TessellationSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CoreTechBlueprintLibrary_eventRetessellateStaticMesh_Parms, TessellationSettings), Z_Construct_UScriptStruct_FDatasmithRetessellationOptions, METADATA_PARAMS(Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_TessellationSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_TessellationSettings_MetaData)) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_FailureReason = { "FailureReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CoreTechBlueprintLibrary_eventRetessellateStaticMesh_Parms, FailureReason), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CoreTechBlueprintLibrary_eventRetessellateStaticMesh_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CoreTechBlueprintLibrary_eventRetessellateStaticMesh_Parms), &Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_TessellationSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_FailureReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::Function_MetaDataParams[] = {
		{ "Category", "Datasmith | Surface Operations" },
		{ "Comment", "/**\n\x09 * Re-tessellate LOD 0 of a static mesh if it contains parametric surface data.\n\x09 * @param\x09StaticMesh\x09\x09\x09\x09Static mesh to re-tessellate.\n\x09 * @param\x09TessellationSettings\x09Tessellation settings to use.\n\x09 * @param\x09""FailureReason\x09\x09\x09Text describing the reason of failure.\n\x09 * @return True if successful, false otherwise\n\x09 */" },
		{ "ModuleRelativePath", "Public/CoreTechBlueprintLibrary.h" },
		{ "ToolTip", "Re-tessellate LOD 0 of a static mesh if it contains parametric surface data.\n@param       StaticMesh                              Static mesh to re-tessellate.\n@param       TessellationSettings    Tessellation settings to use.\n@param       FailureReason                   Text describing the reason of failure.\n@return True if successful, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCoreTechBlueprintLibrary, nullptr, "RetessellateStaticMesh", nullptr, nullptr, sizeof(CoreTechBlueprintLibrary_eventRetessellateStaticMesh_Parms), Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics
	{
		struct CoreTechBlueprintLibrary_eventRetessellateStaticMeshWithNotification_Parms
		{
			UStaticMesh* StaticMesh;
			FDatasmithRetessellationOptions TessellationSettings;
			bool bApplyChanges;
			FText FailureReason;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TessellationSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TessellationSettings;
		static void NewProp_bApplyChanges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyChanges;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_FailureReason;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CoreTechBlueprintLibrary_eventRetessellateStaticMeshWithNotification_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_TessellationSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_TessellationSettings = { "TessellationSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CoreTechBlueprintLibrary_eventRetessellateStaticMeshWithNotification_Parms, TessellationSettings), Z_Construct_UScriptStruct_FDatasmithRetessellationOptions, METADATA_PARAMS(Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_TessellationSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_TessellationSettings_MetaData)) };
	void Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_bApplyChanges_SetBit(void* Obj)
	{
		((CoreTechBlueprintLibrary_eventRetessellateStaticMeshWithNotification_Parms*)Obj)->bApplyChanges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_bApplyChanges = { "bApplyChanges", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CoreTechBlueprintLibrary_eventRetessellateStaticMeshWithNotification_Parms), &Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_bApplyChanges_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_FailureReason = { "FailureReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CoreTechBlueprintLibrary_eventRetessellateStaticMeshWithNotification_Parms, FailureReason), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CoreTechBlueprintLibrary_eventRetessellateStaticMeshWithNotification_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CoreTechBlueprintLibrary_eventRetessellateStaticMeshWithNotification_Parms), &Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_TessellationSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_bApplyChanges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_FailureReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::Function_MetaDataParams[] = {
		{ "Category", "Datasmith | Surface Operations" },
		{ "Comment", "/**\n\x09 * Re-tessellate LOD 0 of a static mesh if it contains parametric surface data.\n\x09 * This implementations allows to skip post edit operations (bApplyChanges=false),\n\x09 * the caller is then responsible to handle those operations.\n\x09 * @param\x09StaticMesh\x09\x09\x09\x09Static mesh to re-tessellate.\n\x09 * @param\x09TessellationSettings\x09Tessellation settings to use.\n\x09 * @param\x09""bApplyChanges\x09\x09\x09Indicates if change must be notified.\n\x09 * @param\x09""FailureReason\x09\x09\x09Text describing the reason of failure, or a warning if the operation was successful.\n\x09 * @return True if successful, false otherwise\n\x09 */" },
		{ "ModuleRelativePath", "Public/CoreTechBlueprintLibrary.h" },
		{ "ToolTip", "Re-tessellate LOD 0 of a static mesh if it contains parametric surface data.\nThis implementations allows to skip post edit operations (bApplyChanges=false),\nthe caller is then responsible to handle those operations.\n@param       StaticMesh                              Static mesh to re-tessellate.\n@param       TessellationSettings    Tessellation settings to use.\n@param       bApplyChanges                   Indicates if change must be notified.\n@param       FailureReason                   Text describing the reason of failure, or a warning if the operation was successful.\n@return True if successful, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCoreTechBlueprintLibrary, nullptr, "RetessellateStaticMeshWithNotification", nullptr, nullptr, sizeof(CoreTechBlueprintLibrary_eventRetessellateStaticMeshWithNotification_Parms), Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCoreTechBlueprintLibrary_NoRegister()
	{
		return UCoreTechBlueprintLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithCoreTechExtension,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMesh, "RetessellateStaticMesh" }, // 782622513
		{ &Z_Construct_UFunction_UCoreTechBlueprintLibrary_RetessellateStaticMeshWithNotification, "RetessellateStaticMeshWithNotification" }, // 2204527195
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "CAD Surface Operations Library" },
		{ "IncludePath", "CoreTechBlueprintLibrary.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CoreTechBlueprintLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCoreTechBlueprintLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics::ClassParams = {
		&UCoreTechBlueprintLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCoreTechBlueprintLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCoreTechBlueprintLibrary, 3956324732);
	template<> DATASMITHCORETECHEXTENSION_API UClass* StaticClass<UCoreTechBlueprintLibrary>()
	{
		return UCoreTechBlueprintLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCoreTechBlueprintLibrary(Z_Construct_UClass_UCoreTechBlueprintLibrary, &UCoreTechBlueprintLibrary::StaticClass, TEXT("/Script/DatasmithCoreTechExtension"), TEXT("UCoreTechBlueprintLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCoreTechBlueprintLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
