// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithCoreTechParametricSurfaceData/Public/CoreTechParametricSurfaceExtension.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCoreTechParametricSurfaceExtension() {}
// Cross Module References
	DATASMITHCORETECHPARAMETRICSURFACEDATA_API UScriptStruct* Z_Construct_UScriptStruct_FCoreTechMeshParameters();
	UPackage* Z_Construct_UPackage__Script_DatasmithCoreTechParametricSurfaceData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	DATASMITHCORETECHPARAMETRICSURFACEDATA_API UScriptStruct* Z_Construct_UScriptStruct_FCoreTechSceneParameters();
	DATASMITHCORETECHPARAMETRICSURFACEDATA_API UClass* Z_Construct_UClass_UCoreTechParametricSurfaceData_NoRegister();
	DATASMITHCORETECHPARAMETRICSURFACEDATA_API UClass* Z_Construct_UClass_UCoreTechParametricSurfaceData();
	DATASMITHCONTENT_API UClass* Z_Construct_UClass_UDatasmithAdditionalData();
	DATASMITHCONTENT_API UScriptStruct* Z_Construct_UScriptStruct_FDatasmithTessellationOptions();
// End Cross Module References
class UScriptStruct* FCoreTechMeshParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHCORETECHPARAMETRICSURFACEDATA_API uint32 Get_Z_Construct_UScriptStruct_FCoreTechMeshParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCoreTechMeshParameters, Z_Construct_UPackage__Script_DatasmithCoreTechParametricSurfaceData(), TEXT("CoreTechMeshParameters"), sizeof(FCoreTechMeshParameters), Get_Z_Construct_UScriptStruct_FCoreTechMeshParameters_Hash());
	}
	return Singleton;
}
template<> DATASMITHCORETECHPARAMETRICSURFACEDATA_API UScriptStruct* StaticStruct<FCoreTechMeshParameters>()
{
	return FCoreTechMeshParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCoreTechMeshParameters(FCoreTechMeshParameters::StaticStruct, TEXT("/Script/DatasmithCoreTechParametricSurfaceData"), TEXT("CoreTechMeshParameters"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithCoreTechParametricSurfaceData_StaticRegisterNativesFCoreTechMeshParameters
{
	FScriptStruct_DatasmithCoreTechParametricSurfaceData_StaticRegisterNativesFCoreTechMeshParameters()
	{
		UScriptStruct::DeferCppStructOps<FCoreTechMeshParameters>(FName(TEXT("CoreTechMeshParameters")));
	}
} ScriptStruct_DatasmithCoreTechParametricSurfaceData_StaticRegisterNativesFCoreTechMeshParameters;
	struct Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNeedSwapOrientation_MetaData[];
#endif
		static void NewProp_bNeedSwapOrientation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNeedSwapOrientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSymmetric_MetaData[];
#endif
		static void NewProp_bIsSymmetric_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSymmetric;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SymmetricOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SymmetricOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SymmetricNormal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SymmetricNormal;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCoreTechMeshParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bNeedSwapOrientation_MetaData[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bNeedSwapOrientation_SetBit(void* Obj)
	{
		((FCoreTechMeshParameters*)Obj)->bNeedSwapOrientation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bNeedSwapOrientation = { "bNeedSwapOrientation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCoreTechMeshParameters), &Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bNeedSwapOrientation_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bNeedSwapOrientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bNeedSwapOrientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bIsSymmetric_MetaData[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bIsSymmetric_SetBit(void* Obj)
	{
		((FCoreTechMeshParameters*)Obj)->bIsSymmetric = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bIsSymmetric = { "bIsSymmetric", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCoreTechMeshParameters), &Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bIsSymmetric_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bIsSymmetric_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bIsSymmetric_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_SymmetricOrigin_MetaData[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_SymmetricOrigin = { "SymmetricOrigin", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCoreTechMeshParameters, SymmetricOrigin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_SymmetricOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_SymmetricOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_SymmetricNormal_MetaData[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_SymmetricNormal = { "SymmetricNormal", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCoreTechMeshParameters, SymmetricNormal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_SymmetricNormal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_SymmetricNormal_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bNeedSwapOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_bIsSymmetric,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_SymmetricOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::NewProp_SymmetricNormal,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithCoreTechParametricSurfaceData,
		nullptr,
		&NewStructOps,
		"CoreTechMeshParameters",
		sizeof(FCoreTechMeshParameters),
		alignof(FCoreTechMeshParameters),
		Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCoreTechMeshParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCoreTechMeshParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithCoreTechParametricSurfaceData();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CoreTechMeshParameters"), sizeof(FCoreTechMeshParameters), Get_Z_Construct_UScriptStruct_FCoreTechMeshParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCoreTechMeshParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCoreTechMeshParameters_Hash() { return 3389040747U; }
class UScriptStruct* FCoreTechSceneParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHCORETECHPARAMETRICSURFACEDATA_API uint32 Get_Z_Construct_UScriptStruct_FCoreTechSceneParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCoreTechSceneParameters, Z_Construct_UPackage__Script_DatasmithCoreTechParametricSurfaceData(), TEXT("CoreTechSceneParameters"), sizeof(FCoreTechSceneParameters), Get_Z_Construct_UScriptStruct_FCoreTechSceneParameters_Hash());
	}
	return Singleton;
}
template<> DATASMITHCORETECHPARAMETRICSURFACEDATA_API UScriptStruct* StaticStruct<FCoreTechSceneParameters>()
{
	return FCoreTechSceneParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCoreTechSceneParameters(FCoreTechSceneParameters::StaticStruct, TEXT("/Script/DatasmithCoreTechParametricSurfaceData"), TEXT("CoreTechSceneParameters"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithCoreTechParametricSurfaceData_StaticRegisterNativesFCoreTechSceneParameters
{
	FScriptStruct_DatasmithCoreTechParametricSurfaceData_StaticRegisterNativesFCoreTechSceneParameters()
	{
		UScriptStruct::DeferCppStructOps<FCoreTechSceneParameters>(FName(TEXT("CoreTechSceneParameters")));
	}
} ScriptStruct_DatasmithCoreTechParametricSurfaceData_StaticRegisterNativesFCoreTechSceneParameters;
	struct Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModelCoordSys_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ModelCoordSys;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MetricUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MetricUnit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScaleFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScaleFactor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCoreTechSceneParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_ModelCoordSys_MetaData[] = {
		{ "Comment", "// value from FDatasmithUtils::EModelCoordSystem\n" },
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
		{ "ToolTip", "value from FDatasmithUtils::EModelCoordSystem" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_ModelCoordSys = { "ModelCoordSys", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCoreTechSceneParameters, ModelCoordSys), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_ModelCoordSys_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_ModelCoordSys_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_MetricUnit_MetaData[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_MetricUnit = { "MetricUnit", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCoreTechSceneParameters, MetricUnit), METADATA_PARAMS(Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_MetricUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_MetricUnit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_ScaleFactor_MetaData[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_ScaleFactor = { "ScaleFactor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCoreTechSceneParameters, ScaleFactor), METADATA_PARAMS(Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_ScaleFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_ScaleFactor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_ModelCoordSys,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_MetricUnit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::NewProp_ScaleFactor,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithCoreTechParametricSurfaceData,
		nullptr,
		&NewStructOps,
		"CoreTechSceneParameters",
		sizeof(FCoreTechSceneParameters),
		alignof(FCoreTechSceneParameters),
		Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCoreTechSceneParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCoreTechSceneParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithCoreTechParametricSurfaceData();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CoreTechSceneParameters"), sizeof(FCoreTechSceneParameters), Get_Z_Construct_UScriptStruct_FCoreTechSceneParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCoreTechSceneParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCoreTechSceneParameters_Hash() { return 7600154U; }
	void UCoreTechParametricSurfaceData::StaticRegisterNativesUCoreTechParametricSurfaceData()
	{
	}
	UClass* Z_Construct_UClass_UCoreTechParametricSurfaceData_NoRegister()
	{
		return UCoreTechParametricSurfaceData::StaticClass();
	}
	struct Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SourceFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SceneParameters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeshParameters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastTessellationOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastTessellationOptions;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RawData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RawData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RawData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDatasmithAdditionalData,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithCoreTechParametricSurfaceData,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Parametric Surface Data" },
		{ "IncludePath", "CoreTechParametricSurfaceExtension.h" },
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_SourceFile_MetaData[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_SourceFile = { "SourceFile", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoreTechParametricSurfaceData, SourceFile), METADATA_PARAMS(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_SourceFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_SourceFile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_SceneParameters_MetaData[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_SceneParameters = { "SceneParameters", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoreTechParametricSurfaceData, SceneParameters), Z_Construct_UScriptStruct_FCoreTechSceneParameters, METADATA_PARAMS(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_SceneParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_SceneParameters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_MeshParameters_MetaData[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_MeshParameters = { "MeshParameters", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoreTechParametricSurfaceData, MeshParameters), Z_Construct_UScriptStruct_FCoreTechMeshParameters, METADATA_PARAMS(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_MeshParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_MeshParameters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_LastTessellationOptions_MetaData[] = {
		{ "Category", "NURBS" },
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_LastTessellationOptions = { "LastTessellationOptions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoreTechParametricSurfaceData, LastTessellationOptions), Z_Construct_UScriptStruct_FDatasmithTessellationOptions, METADATA_PARAMS(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_LastTessellationOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_LastTessellationOptions_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_RawData_Inner = { "RawData", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_RawData_MetaData[] = {
		{ "ModuleRelativePath", "Public/CoreTechParametricSurfaceExtension.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_RawData = { "RawData", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoreTechParametricSurfaceData, RawData_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_RawData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_RawData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_SourceFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_SceneParameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_MeshParameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_LastTessellationOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_RawData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::NewProp_RawData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCoreTechParametricSurfaceData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::ClassParams = {
		&UCoreTechParametricSurfaceData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCoreTechParametricSurfaceData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCoreTechParametricSurfaceData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCoreTechParametricSurfaceData, 1344744778);
	template<> DATASMITHCORETECHPARAMETRICSURFACEDATA_API UClass* StaticClass<UCoreTechParametricSurfaceData>()
	{
		return UCoreTechParametricSurfaceData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCoreTechParametricSurfaceData(Z_Construct_UClass_UCoreTechParametricSurfaceData, &UCoreTechParametricSurfaceData::StaticClass, TEXT("/Script/DatasmithCoreTechParametricSurfaceData"), TEXT("UCoreTechParametricSurfaceData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCoreTechParametricSurfaceData);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UCoreTechParametricSurfaceData)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
