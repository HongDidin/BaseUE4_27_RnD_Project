// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UStaticMesh;
struct FDatasmithRetessellationOptions;
#ifdef DATASMITHCORETECHEXTENSION_CoreTechBlueprintLibrary_generated_h
#error "CoreTechBlueprintLibrary.generated.h already included, missing '#pragma once' in CoreTechBlueprintLibrary.h"
#endif
#define DATASMITHCORETECHEXTENSION_CoreTechBlueprintLibrary_generated_h

#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRetessellateStaticMeshWithNotification); \
	DECLARE_FUNCTION(execRetessellateStaticMesh);


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRetessellateStaticMeshWithNotification); \
	DECLARE_FUNCTION(execRetessellateStaticMesh);


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCoreTechBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UCoreTechBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithCoreTechExtension"), NO_API) \
	DECLARE_SERIALIZER(UCoreTechBlueprintLibrary)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUCoreTechBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UCoreTechBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UCoreTechBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithCoreTechExtension"), NO_API) \
	DECLARE_SERIALIZER(UCoreTechBlueprintLibrary)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCoreTechBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCoreTechBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCoreTechBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCoreTechBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCoreTechBlueprintLibrary(UCoreTechBlueprintLibrary&&); \
	NO_API UCoreTechBlueprintLibrary(const UCoreTechBlueprintLibrary&); \
public:


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCoreTechBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCoreTechBlueprintLibrary(UCoreTechBlueprintLibrary&&); \
	NO_API UCoreTechBlueprintLibrary(const UCoreTechBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCoreTechBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCoreTechBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCoreTechBlueprintLibrary)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_16_PROLOG
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_INCLASS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHCORETECHEXTENSION_API UClass* StaticClass<class UCoreTechBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
