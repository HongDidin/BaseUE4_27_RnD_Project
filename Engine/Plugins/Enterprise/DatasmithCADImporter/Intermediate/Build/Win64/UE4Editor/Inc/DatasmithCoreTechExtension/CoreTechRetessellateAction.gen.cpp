// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithCoreTechExtension/Public/CoreTechRetessellateAction.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCoreTechRetessellateAction() {}
// Cross Module References
	DATASMITHCORETECHEXTENSION_API UClass* Z_Construct_UClass_UCoreTechRetessellateAction_NoRegister();
	DATASMITHCORETECHEXTENSION_API UClass* Z_Construct_UClass_UCoreTechRetessellateAction();
	DATASMITHCONTENT_API UClass* Z_Construct_UClass_UDatasmithCustomActionBase();
	UPackage* Z_Construct_UPackage__Script_DatasmithCoreTechExtension();
	DATASMITHCORETECHEXTENSION_API UClass* Z_Construct_UClass_UCoreTechRetessellateActionOptions_NoRegister();
	DATASMITHCORETECHEXTENSION_API UClass* Z_Construct_UClass_UCoreTechRetessellateActionOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	DATASMITHCONTENT_API UScriptStruct* Z_Construct_UScriptStruct_FDatasmithRetessellationOptions();
// End Cross Module References
	void UCoreTechRetessellateAction::StaticRegisterNativesUCoreTechRetessellateAction()
	{
	}
	UClass* Z_Construct_UClass_UCoreTechRetessellateAction_NoRegister()
	{
		return UCoreTechRetessellateAction::StaticClass();
	}
	struct Z_Construct_UClass_UCoreTechRetessellateAction_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCoreTechRetessellateAction_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDatasmithCustomActionBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithCoreTechExtension,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoreTechRetessellateAction_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CoreTechRetessellateAction.h" },
		{ "ModuleRelativePath", "Public/CoreTechRetessellateAction.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCoreTechRetessellateAction_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCoreTechRetessellateAction>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCoreTechRetessellateAction_Statics::ClassParams = {
		&UCoreTechRetessellateAction::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCoreTechRetessellateAction_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechRetessellateAction_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCoreTechRetessellateAction()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCoreTechRetessellateAction_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCoreTechRetessellateAction, 166047838);
	template<> DATASMITHCORETECHEXTENSION_API UClass* StaticClass<UCoreTechRetessellateAction>()
	{
		return UCoreTechRetessellateAction::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCoreTechRetessellateAction(Z_Construct_UClass_UCoreTechRetessellateAction, &UCoreTechRetessellateAction::StaticClass, TEXT("/Script/DatasmithCoreTechExtension"), TEXT("UCoreTechRetessellateAction"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCoreTechRetessellateAction);
	void UCoreTechRetessellateActionOptions::StaticRegisterNativesUCoreTechRetessellateActionOptions()
	{
	}
	UClass* Z_Construct_UClass_UCoreTechRetessellateActionOptions_NoRegister()
	{
		return UCoreTechRetessellateActionOptions::StaticClass();
	}
	struct Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Options_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithCoreTechExtension,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CoreTechRetessellateAction.h" },
		{ "ModuleRelativePath", "Public/CoreTechRetessellateAction.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::NewProp_Options_MetaData[] = {
		{ "Category", "NotVisible" },
		{ "ModuleRelativePath", "Public/CoreTechRetessellateAction.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoreTechRetessellateActionOptions, Options), Z_Construct_UScriptStruct_FDatasmithRetessellationOptions, METADATA_PARAMS(Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::NewProp_Options_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::NewProp_Options_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::NewProp_Options,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCoreTechRetessellateActionOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::ClassParams = {
		&UCoreTechRetessellateActionOptions::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::PropPointers),
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCoreTechRetessellateActionOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCoreTechRetessellateActionOptions, 493023903);
	template<> DATASMITHCORETECHEXTENSION_API UClass* StaticClass<UCoreTechRetessellateActionOptions>()
	{
		return UCoreTechRetessellateActionOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCoreTechRetessellateActionOptions(Z_Construct_UClass_UCoreTechRetessellateActionOptions, &UCoreTechRetessellateActionOptions::StaticClass, TEXT("/Script/DatasmithCoreTechExtension"), TEXT("UCoreTechRetessellateActionOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCoreTechRetessellateActionOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
