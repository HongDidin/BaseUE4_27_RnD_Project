// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHCORETECHEXTENSION_CoreTechRetessellateAction_generated_h
#error "CoreTechRetessellateAction.generated.h already included, missing '#pragma once' in CoreTechRetessellateAction.h"
#endif
#define DATASMITHCORETECHEXTENSION_CoreTechRetessellateAction_generated_h

#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCoreTechRetessellateAction(); \
	friend struct Z_Construct_UClass_UCoreTechRetessellateAction_Statics; \
public: \
	DECLARE_CLASS(UCoreTechRetessellateAction, UDatasmithCustomActionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithCoreTechExtension"), NO_API) \
	DECLARE_SERIALIZER(UCoreTechRetessellateAction)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUCoreTechRetessellateAction(); \
	friend struct Z_Construct_UClass_UCoreTechRetessellateAction_Statics; \
public: \
	DECLARE_CLASS(UCoreTechRetessellateAction, UDatasmithCustomActionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithCoreTechExtension"), NO_API) \
	DECLARE_SERIALIZER(UCoreTechRetessellateAction)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCoreTechRetessellateAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCoreTechRetessellateAction) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCoreTechRetessellateAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCoreTechRetessellateAction); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCoreTechRetessellateAction(UCoreTechRetessellateAction&&); \
	NO_API UCoreTechRetessellateAction(const UCoreTechRetessellateAction&); \
public:


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCoreTechRetessellateAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCoreTechRetessellateAction(UCoreTechRetessellateAction&&); \
	NO_API UCoreTechRetessellateAction(const UCoreTechRetessellateAction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCoreTechRetessellateAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCoreTechRetessellateAction); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCoreTechRetessellateAction)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_26_PROLOG
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_INCLASS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHCORETECHEXTENSION_API UClass* StaticClass<class UCoreTechRetessellateAction>();

#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCoreTechRetessellateActionOptions(); \
	friend struct Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics; \
public: \
	DECLARE_CLASS(UCoreTechRetessellateActionOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithCoreTechExtension"), NO_API) \
	DECLARE_SERIALIZER(UCoreTechRetessellateActionOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_INCLASS \
private: \
	static void StaticRegisterNativesUCoreTechRetessellateActionOptions(); \
	friend struct Z_Construct_UClass_UCoreTechRetessellateActionOptions_Statics; \
public: \
	DECLARE_CLASS(UCoreTechRetessellateActionOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithCoreTechExtension"), NO_API) \
	DECLARE_SERIALIZER(UCoreTechRetessellateActionOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCoreTechRetessellateActionOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCoreTechRetessellateActionOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCoreTechRetessellateActionOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCoreTechRetessellateActionOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCoreTechRetessellateActionOptions(UCoreTechRetessellateActionOptions&&); \
	NO_API UCoreTechRetessellateActionOptions(const UCoreTechRetessellateActionOptions&); \
public:


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCoreTechRetessellateActionOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCoreTechRetessellateActionOptions(UCoreTechRetessellateActionOptions&&); \
	NO_API UCoreTechRetessellateActionOptions(const UCoreTechRetessellateActionOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCoreTechRetessellateActionOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCoreTechRetessellateActionOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCoreTechRetessellateActionOptions)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_47_PROLOG
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_INCLASS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h_50_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHCORETECHEXTENSION_API UClass* StaticClass<class UCoreTechRetessellateActionOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Public_CoreTechRetessellateAction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
