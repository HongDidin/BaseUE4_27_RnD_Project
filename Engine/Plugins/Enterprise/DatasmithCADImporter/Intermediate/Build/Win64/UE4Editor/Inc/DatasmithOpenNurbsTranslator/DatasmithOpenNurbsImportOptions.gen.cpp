// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DatasmithOpenNurbsTranslator/Public/DatasmithOpenNurbsImportOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithOpenNurbsImportOptions() {}
// Cross Module References
	DATASMITHOPENNURBSTRANSLATOR_API UEnum* Z_Construct_UEnum_DatasmithOpenNurbsTranslator_EDatasmithOpenNurbsBrepTessellatedSource();
	UPackage* Z_Construct_UPackage__Script_DatasmithOpenNurbsTranslator();
	DATASMITHOPENNURBSTRANSLATOR_API UScriptStruct* Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions();
	DATASMITHCONTENT_API UScriptStruct* Z_Construct_UScriptStruct_FDatasmithTessellationOptions();
	DATASMITHOPENNURBSTRANSLATOR_API UClass* Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_NoRegister();
	DATASMITHOPENNURBSTRANSLATOR_API UClass* Z_Construct_UClass_UDatasmithOpenNurbsImportOptions();
	DATASMITHCONTENT_API UClass* Z_Construct_UClass_UDatasmithOptionsBase();
// End Cross Module References
	static UEnum* EDatasmithOpenNurbsBrepTessellatedSource_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DatasmithOpenNurbsTranslator_EDatasmithOpenNurbsBrepTessellatedSource, Z_Construct_UPackage__Script_DatasmithOpenNurbsTranslator(), TEXT("EDatasmithOpenNurbsBrepTessellatedSource"));
		}
		return Singleton;
	}
	template<> DATASMITHOPENNURBSTRANSLATOR_API UEnum* StaticEnum<EDatasmithOpenNurbsBrepTessellatedSource>()
	{
		return EDatasmithOpenNurbsBrepTessellatedSource_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDatasmithOpenNurbsBrepTessellatedSource(EDatasmithOpenNurbsBrepTessellatedSource_StaticEnum, TEXT("/Script/DatasmithOpenNurbsTranslator"), TEXT("EDatasmithOpenNurbsBrepTessellatedSource"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DatasmithOpenNurbsTranslator_EDatasmithOpenNurbsBrepTessellatedSource_Hash() { return 3541025501U; }
	UEnum* Z_Construct_UEnum_DatasmithOpenNurbsTranslator_EDatasmithOpenNurbsBrepTessellatedSource()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithOpenNurbsTranslator();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDatasmithOpenNurbsBrepTessellatedSource"), 0, Get_Z_Construct_UEnum_DatasmithOpenNurbsTranslator_EDatasmithOpenNurbsBrepTessellatedSource_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDatasmithOpenNurbsBrepTessellatedSource::UseUnrealNurbsTessellation", (int64)EDatasmithOpenNurbsBrepTessellatedSource::UseUnrealNurbsTessellation },
				{ "EDatasmithOpenNurbsBrepTessellatedSource::UseRenderMeshes", (int64)EDatasmithOpenNurbsBrepTessellatedSource::UseRenderMeshes },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/DatasmithOpenNurbsImportOptions.h" },
				{ "UseRenderMeshes.Comment", "/** Use render meshes stored in the scene file */" },
				{ "UseRenderMeshes.DisplayName", "Import Rhino Meshes and UVs" },
				{ "UseRenderMeshes.Name", "EDatasmithOpenNurbsBrepTessellatedSource::UseRenderMeshes" },
				{ "UseRenderMeshes.ToolTip", "Use render meshes stored in the scene file" },
				{ "UseUnrealNurbsTessellation.Comment", "/** Tessellate all Breps on import */" },
				{ "UseUnrealNurbsTessellation.DisplayName", "Import as NURBS, Tessellate in Unreal" },
				{ "UseUnrealNurbsTessellation.Name", "EDatasmithOpenNurbsBrepTessellatedSource::UseUnrealNurbsTessellation" },
				{ "UseUnrealNurbsTessellation.ToolTip", "Tessellate all Breps on import" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DatasmithOpenNurbsTranslator,
				nullptr,
				"EDatasmithOpenNurbsBrepTessellatedSource",
				"EDatasmithOpenNurbsBrepTessellatedSource",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FDatasmithOpenNurbsOptions>() == std::is_polymorphic<FDatasmithTessellationOptions>(), "USTRUCT FDatasmithOpenNurbsOptions cannot be polymorphic unless super FDatasmithTessellationOptions is polymorphic");

class UScriptStruct* FDatasmithOpenNurbsOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATASMITHOPENNURBSTRANSLATOR_API uint32 Get_Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions, Z_Construct_UPackage__Script_DatasmithOpenNurbsTranslator(), TEXT("DatasmithOpenNurbsOptions"), sizeof(FDatasmithOpenNurbsOptions), Get_Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Hash());
	}
	return Singleton;
}
template<> DATASMITHOPENNURBSTRANSLATOR_API UScriptStruct* StaticStruct<FDatasmithOpenNurbsOptions>()
{
	return FDatasmithOpenNurbsOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDatasmithOpenNurbsOptions(FDatasmithOpenNurbsOptions::StaticStruct, TEXT("/Script/DatasmithOpenNurbsTranslator"), TEXT("DatasmithOpenNurbsOptions"), false, nullptr, nullptr);
static struct FScriptStruct_DatasmithOpenNurbsTranslator_StaticRegisterNativesFDatasmithOpenNurbsOptions
{
	FScriptStruct_DatasmithOpenNurbsTranslator_StaticRegisterNativesFDatasmithOpenNurbsOptions()
	{
		UScriptStruct::DeferCppStructOps<FDatasmithOpenNurbsOptions>(FName(TEXT("DatasmithOpenNurbsOptions")));
	}
} ScriptStruct_DatasmithOpenNurbsTranslator_StaticRegisterNativesFDatasmithOpenNurbsOptions;
	struct Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Geometry_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Geometry_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Geometry;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DatasmithOpenNurbsImportOptions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDatasmithOpenNurbsOptions>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::NewProp_Geometry_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::NewProp_Geometry_MetaData[] = {
		{ "Category", "Geometry & Tessellation Options" },
		{ "ModuleRelativePath", "Public/DatasmithOpenNurbsImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::NewProp_Geometry = { "Geometry", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDatasmithOpenNurbsOptions, Geometry), Z_Construct_UEnum_DatasmithOpenNurbsTranslator_EDatasmithOpenNurbsBrepTessellatedSource, METADATA_PARAMS(Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::NewProp_Geometry_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::NewProp_Geometry_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::NewProp_Geometry_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::NewProp_Geometry,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithOpenNurbsTranslator,
		Z_Construct_UScriptStruct_FDatasmithTessellationOptions,
		&NewStructOps,
		"DatasmithOpenNurbsOptions",
		sizeof(FDatasmithOpenNurbsOptions),
		alignof(FDatasmithOpenNurbsOptions),
		Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DatasmithOpenNurbsTranslator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DatasmithOpenNurbsOptions"), sizeof(FDatasmithOpenNurbsOptions), Get_Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions_Hash() { return 1945779728U; }
	void UDatasmithOpenNurbsImportOptions::StaticRegisterNativesUDatasmithOpenNurbsImportOptions()
	{
	}
	UClass* Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_NoRegister()
	{
		return UDatasmithOpenNurbsImportOptions::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Options_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDatasmithOptionsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithOpenNurbsTranslator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "DatasmithOpenNurbsImportOptions.h" },
		{ "ModuleRelativePath", "Public/DatasmithOpenNurbsImportOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::NewProp_Options_MetaData[] = {
		{ "Category", "Geometry & Tessellation Options" },
		{ "ModuleRelativePath", "Public/DatasmithOpenNurbsImportOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithOpenNurbsImportOptions, Options), Z_Construct_UScriptStruct_FDatasmithOpenNurbsOptions, METADATA_PARAMS(Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::NewProp_Options_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::NewProp_Options_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::NewProp_Options,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithOpenNurbsImportOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::ClassParams = {
		&UDatasmithOpenNurbsImportOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithOpenNurbsImportOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithOpenNurbsImportOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithOpenNurbsImportOptions, 3170810422);
	template<> DATASMITHOPENNURBSTRANSLATOR_API UClass* StaticClass<UDatasmithOpenNurbsImportOptions>()
	{
		return UDatasmithOpenNurbsImportOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithOpenNurbsImportOptions(Z_Construct_UClass_UDatasmithOpenNurbsImportOptions, &UDatasmithOpenNurbsImportOptions::StaticClass, TEXT("/Script/DatasmithOpenNurbsTranslator"), TEXT("UDatasmithOpenNurbsImportOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithOpenNurbsImportOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
