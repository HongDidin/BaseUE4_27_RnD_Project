// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHCORETECHEXTENSION_DataprepTessellationOperation_generated_h
#error "DataprepTessellationOperation.generated.h already included, missing '#pragma once' in DataprepTessellationOperation.h"
#endif
#define DATASMITHCORETECHEXTENSION_DataprepTessellationOperation_generated_h

#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_SPARSE_DATA
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepTessellationOperation(); \
	friend struct Z_Construct_UClass_UDataprepTessellationOperation_Statics; \
public: \
	DECLARE_CLASS(UDataprepTessellationOperation, UDataprepOperation, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithCoreTechExtension"), NO_API) \
	DECLARE_SERIALIZER(UDataprepTessellationOperation)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepTessellationOperation(); \
	friend struct Z_Construct_UClass_UDataprepTessellationOperation_Statics; \
public: \
	DECLARE_CLASS(UDataprepTessellationOperation, UDataprepOperation, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithCoreTechExtension"), NO_API) \
	DECLARE_SERIALIZER(UDataprepTessellationOperation)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepTessellationOperation(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepTessellationOperation) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepTessellationOperation); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepTessellationOperation); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepTessellationOperation(UDataprepTessellationOperation&&); \
	NO_API UDataprepTessellationOperation(const UDataprepTessellationOperation&); \
public:


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepTessellationOperation(UDataprepTessellationOperation&&); \
	NO_API UDataprepTessellationOperation(const UDataprepTessellationOperation&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepTessellationOperation); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepTessellationOperation); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepTessellationOperation)


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_21_PROLOG
#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_INCLASS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_SPARSE_DATA \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHCORETECHEXTENSION_API UClass* StaticClass<class UDataprepTessellationOperation>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DatasmithCADImporter_Source_DatasmithCoreTechExtension_Private_DataprepTessellationOperation_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
