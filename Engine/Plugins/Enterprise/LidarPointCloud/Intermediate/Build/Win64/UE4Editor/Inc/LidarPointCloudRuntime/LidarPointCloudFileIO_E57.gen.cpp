// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LidarPointCloudRuntime/Public/IO/LidarPointCloudFileIO_E57.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLidarPointCloudFileIO_E57() {}
// Cross Module References
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloudFileIO_E57_NoRegister();
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloudFileIO_E57();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LidarPointCloudRuntime();
// End Cross Module References
	void ULidarPointCloudFileIO_E57::StaticRegisterNativesULidarPointCloudFileIO_E57()
	{
	}
	UClass* Z_Construct_UClass_ULidarPointCloudFileIO_E57_NoRegister()
	{
		return ULidarPointCloudFileIO_E57::StaticClass();
	}
	struct Z_Construct_UClass_ULidarPointCloudFileIO_E57_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULidarPointCloudFileIO_E57_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULidarPointCloudFileIO_E57_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "IO/LidarPointCloudFileIO_E57.h" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_E57.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULidarPointCloudFileIO_E57_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULidarPointCloudFileIO_E57>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULidarPointCloudFileIO_E57_Statics::ClassParams = {
		&ULidarPointCloudFileIO_E57::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULidarPointCloudFileIO_E57_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULidarPointCloudFileIO_E57_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULidarPointCloudFileIO_E57()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULidarPointCloudFileIO_E57_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULidarPointCloudFileIO_E57, 3300737776);
	template<> LIDARPOINTCLOUDRUNTIME_API UClass* StaticClass<ULidarPointCloudFileIO_E57>()
	{
		return ULidarPointCloudFileIO_E57::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULidarPointCloudFileIO_E57(Z_Construct_UClass_ULidarPointCloudFileIO_E57, &ULidarPointCloudFileIO_E57::StaticClass, TEXT("/Script/LidarPointCloudRuntime"), TEXT("ULidarPointCloudFileIO_E57"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULidarPointCloudFileIO_E57);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
