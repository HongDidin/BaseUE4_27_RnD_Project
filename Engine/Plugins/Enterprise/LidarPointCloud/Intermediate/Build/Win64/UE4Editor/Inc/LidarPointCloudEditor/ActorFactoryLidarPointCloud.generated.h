// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIDARPOINTCLOUDEDITOR_ActorFactoryLidarPointCloud_generated_h
#error "ActorFactoryLidarPointCloud.generated.h already included, missing '#pragma once' in ActorFactoryLidarPointCloud.h"
#endif
#define LIDARPOINTCLOUDEDITOR_ActorFactoryLidarPointCloud_generated_h

#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_SPARSE_DATA
#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorFactoryLidarPointCloud(); \
	friend struct Z_Construct_UClass_UActorFactoryLidarPointCloud_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryLidarPointCloud, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/LidarPointCloudEditor"), LIDARPOINTCLOUDEDITOR_API) \
	DECLARE_SERIALIZER(UActorFactoryLidarPointCloud)


#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUActorFactoryLidarPointCloud(); \
	friend struct Z_Construct_UClass_UActorFactoryLidarPointCloud_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryLidarPointCloud, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/LidarPointCloudEditor"), LIDARPOINTCLOUDEDITOR_API) \
	DECLARE_SERIALIZER(UActorFactoryLidarPointCloud)


#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	LIDARPOINTCLOUDEDITOR_API UActorFactoryLidarPointCloud(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryLidarPointCloud) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(LIDARPOINTCLOUDEDITOR_API, UActorFactoryLidarPointCloud); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryLidarPointCloud); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	LIDARPOINTCLOUDEDITOR_API UActorFactoryLidarPointCloud(UActorFactoryLidarPointCloud&&); \
	LIDARPOINTCLOUDEDITOR_API UActorFactoryLidarPointCloud(const UActorFactoryLidarPointCloud&); \
public:


#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	LIDARPOINTCLOUDEDITOR_API UActorFactoryLidarPointCloud(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	LIDARPOINTCLOUDEDITOR_API UActorFactoryLidarPointCloud(UActorFactoryLidarPointCloud&&); \
	LIDARPOINTCLOUDEDITOR_API UActorFactoryLidarPointCloud(const UActorFactoryLidarPointCloud&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(LIDARPOINTCLOUDEDITOR_API, UActorFactoryLidarPointCloud); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryLidarPointCloud); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryLidarPointCloud)


#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_13_PROLOG
#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_SPARSE_DATA \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_INCLASS \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_SPARSE_DATA \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ActorFactoryLidarPointCloud."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIDARPOINTCLOUDEDITOR_API UClass* StaticClass<class UActorFactoryLidarPointCloud>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_ActorFactoryLidarPointCloud_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
