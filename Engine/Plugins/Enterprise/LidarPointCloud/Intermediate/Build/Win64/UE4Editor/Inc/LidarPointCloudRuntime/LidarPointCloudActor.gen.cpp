// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LidarPointCloudRuntime/Public/LidarPointCloudActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLidarPointCloudActor() {}
// Cross Module References
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ALidarPointCloudActor_NoRegister();
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ALidarPointCloudActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_LidarPointCloudRuntime();
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloud_NoRegister();
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloudComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ALidarPointCloudActor::execSetPointCloud)
	{
		P_GET_OBJECT(ULidarPointCloud,Z_Param_InPointCloud);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPointCloud(Z_Param_InPointCloud);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALidarPointCloudActor::execGetPointCloud)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULidarPointCloud**)Z_Param__Result=P_THIS->GetPointCloud();
		P_NATIVE_END;
	}
	void ALidarPointCloudActor::StaticRegisterNativesALidarPointCloudActor()
	{
		UClass* Class = ALidarPointCloudActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetPointCloud", &ALidarPointCloudActor::execGetPointCloud },
			{ "SetPointCloud", &ALidarPointCloudActor::execSetPointCloud },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics
	{
		struct LidarPointCloudActor_eventGetPointCloud_Parms
		{
			ULidarPointCloud* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LidarPointCloudActor_eventGetPointCloud_Parms, ReturnValue), Z_Construct_UClass_ULidarPointCloud_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics::Function_MetaDataParams[] = {
		{ "Category", "Components|LidarPointCloud" },
		{ "ModuleRelativePath", "Public/LidarPointCloudActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALidarPointCloudActor, nullptr, "GetPointCloud", nullptr, nullptr, sizeof(LidarPointCloudActor_eventGetPointCloud_Parms), Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics
	{
		struct LidarPointCloudActor_eventSetPointCloud_Parms
		{
			ULidarPointCloud* InPointCloud;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPointCloud;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics::NewProp_InPointCloud = { "InPointCloud", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LidarPointCloudActor_eventSetPointCloud_Parms, InPointCloud), Z_Construct_UClass_ULidarPointCloud_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics::NewProp_InPointCloud,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics::Function_MetaDataParams[] = {
		{ "Category", "Components|LidarPointCloud" },
		{ "ModuleRelativePath", "Public/LidarPointCloudActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALidarPointCloudActor, nullptr, "SetPointCloud", nullptr, nullptr, sizeof(LidarPointCloudActor_eventSetPointCloud_Parms), Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ALidarPointCloudActor_NoRegister()
	{
		return ALidarPointCloudActor::StaticClass();
	}
	struct Z_Construct_UClass_ALidarPointCloudActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointCloudComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PointCloudComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALidarPointCloudActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ALidarPointCloudActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ALidarPointCloudActor_GetPointCloud, "GetPointCloud" }, // 2670642715
		{ &Z_Construct_UFunction_ALidarPointCloudActor_SetPointCloud, "SetPointCloud" }, // 600878595
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALidarPointCloudActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Materials" },
		{ "IncludePath", "LidarPointCloudActor.h" },
		{ "ModuleRelativePath", "Public/LidarPointCloudActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALidarPointCloudActor_Statics::NewProp_PointCloudComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "PointCloudActor" },
		{ "EditInline", "true" },
		{ "ExposeFunctionCategories", "Rendering,Components|LidarPointCloud" },
		{ "ModuleRelativePath", "Public/LidarPointCloudActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ALidarPointCloudActor_Statics::NewProp_PointCloudComponent = { "PointCloudComponent", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALidarPointCloudActor, PointCloudComponent), Z_Construct_UClass_ULidarPointCloudComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ALidarPointCloudActor_Statics::NewProp_PointCloudComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALidarPointCloudActor_Statics::NewProp_PointCloudComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ALidarPointCloudActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALidarPointCloudActor_Statics::NewProp_PointCloudComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALidarPointCloudActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALidarPointCloudActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALidarPointCloudActor_Statics::ClassParams = {
		&ALidarPointCloudActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ALidarPointCloudActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ALidarPointCloudActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ALidarPointCloudActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALidarPointCloudActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALidarPointCloudActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALidarPointCloudActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALidarPointCloudActor, 852873349);
	template<> LIDARPOINTCLOUDRUNTIME_API UClass* StaticClass<ALidarPointCloudActor>()
	{
		return ALidarPointCloudActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALidarPointCloudActor(Z_Construct_UClass_ALidarPointCloudActor, &ALidarPointCloudActor::StaticClass, TEXT("/Script/LidarPointCloudRuntime"), TEXT("ALidarPointCloudActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALidarPointCloudActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
