// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LidarPointCloudEditor/Private/ActorFactoryLidarPointCloud.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorFactoryLidarPointCloud() {}
// Cross Module References
	LIDARPOINTCLOUDEDITOR_API UClass* Z_Construct_UClass_UActorFactoryLidarPointCloud_NoRegister();
	LIDARPOINTCLOUDEDITOR_API UClass* Z_Construct_UClass_UActorFactoryLidarPointCloud();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_LidarPointCloudEditor();
// End Cross Module References
	void UActorFactoryLidarPointCloud::StaticRegisterNativesUActorFactoryLidarPointCloud()
	{
	}
	UClass* Z_Construct_UClass_UActorFactoryLidarPointCloud_NoRegister()
	{
		return UActorFactoryLidarPointCloud::StaticClass();
	}
	struct Z_Construct_UClass_UActorFactoryLidarPointCloud_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorFactoryLidarPointCloud_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorFactoryLidarPointCloud_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "ActorFactoryLidarPointCloud.h" },
		{ "ModuleRelativePath", "Private/ActorFactoryLidarPointCloud.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorFactoryLidarPointCloud_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorFactoryLidarPointCloud>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorFactoryLidarPointCloud_Statics::ClassParams = {
		&UActorFactoryLidarPointCloud::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UActorFactoryLidarPointCloud_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorFactoryLidarPointCloud_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorFactoryLidarPointCloud()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorFactoryLidarPointCloud_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorFactoryLidarPointCloud, 1624857224);
	template<> LIDARPOINTCLOUDEDITOR_API UClass* StaticClass<UActorFactoryLidarPointCloud>()
	{
		return UActorFactoryLidarPointCloud::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorFactoryLidarPointCloud(Z_Construct_UClass_UActorFactoryLidarPointCloud, &UActorFactoryLidarPointCloud::StaticClass, TEXT("/Script/LidarPointCloudEditor"), TEXT("UActorFactoryLidarPointCloud"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorFactoryLidarPointCloud);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
