// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LidarPointCloudRuntime/Public/LidarPointCloudShared.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLidarPointCloudShared() {}
// Cross Module References
	LIDARPOINTCLOUDRUNTIME_API UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudSpriteShape();
	UPackage* Z_Construct_UPackage__Script_LidarPointCloudRuntime();
	LIDARPOINTCLOUDRUNTIME_API UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudColorationMode();
	LIDARPOINTCLOUDRUNTIME_API UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarClippingVolumeMode();
	LIDARPOINTCLOUDRUNTIME_API UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudScalingMethod();
	LIDARPOINTCLOUDRUNTIME_API UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudAsyncMode();
	LIDARPOINTCLOUDRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FLidarPointCloudPoint();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	LIDARPOINTCLOUDRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FLidarPointCloudNormal();
	LIDARPOINTCLOUDRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDoubleVector();
// End Cross Module References
	static UEnum* ELidarPointCloudSpriteShape_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudSpriteShape, Z_Construct_UPackage__Script_LidarPointCloudRuntime(), TEXT("ELidarPointCloudSpriteShape"));
		}
		return Singleton;
	}
	template<> LIDARPOINTCLOUDRUNTIME_API UEnum* StaticEnum<ELidarPointCloudSpriteShape>()
	{
		return ELidarPointCloudSpriteShape_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ELidarPointCloudSpriteShape(ELidarPointCloudSpriteShape_StaticEnum, TEXT("/Script/LidarPointCloudRuntime"), TEXT("ELidarPointCloudSpriteShape"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudSpriteShape_Hash() { return 1850420296U; }
	UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudSpriteShape()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LidarPointCloudRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ELidarPointCloudSpriteShape"), 0, Get_Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudSpriteShape_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ELidarPointCloudSpriteShape::Square", (int64)ELidarPointCloudSpriteShape::Square },
				{ "ELidarPointCloudSpriteShape::Circle", (int64)ELidarPointCloudSpriteShape::Circle },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Circle.Name", "ELidarPointCloudSpriteShape::Circle" },
				{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
				{ "Square.Name", "ELidarPointCloudSpriteShape::Square" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
				nullptr,
				"ELidarPointCloudSpriteShape",
				"ELidarPointCloudSpriteShape",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ELidarPointCloudColorationMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudColorationMode, Z_Construct_UPackage__Script_LidarPointCloudRuntime(), TEXT("ELidarPointCloudColorationMode"));
		}
		return Singleton;
	}
	template<> LIDARPOINTCLOUDRUNTIME_API UEnum* StaticEnum<ELidarPointCloudColorationMode>()
	{
		return ELidarPointCloudColorationMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ELidarPointCloudColorationMode(ELidarPointCloudColorationMode_StaticEnum, TEXT("/Script/LidarPointCloudRuntime"), TEXT("ELidarPointCloudColorationMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudColorationMode_Hash() { return 10035064U; }
	UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudColorationMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LidarPointCloudRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ELidarPointCloudColorationMode"), 0, Get_Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudColorationMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ELidarPointCloudColorationMode::None", (int64)ELidarPointCloudColorationMode::None },
				{ "ELidarPointCloudColorationMode::Data", (int64)ELidarPointCloudColorationMode::Data },
				{ "ELidarPointCloudColorationMode::Elevation", (int64)ELidarPointCloudColorationMode::Elevation },
				{ "ELidarPointCloudColorationMode::Position", (int64)ELidarPointCloudColorationMode::Position },
				{ "ELidarPointCloudColorationMode::Classification", (int64)ELidarPointCloudColorationMode::Classification },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Classification.Comment", "/** Uses Classification ID of the point along with the component's Classification Colors property to sample the color */" },
				{ "Classification.Name", "ELidarPointCloudColorationMode::Classification" },
				{ "Classification.ToolTip", "Uses Classification ID of the point along with the component's Classification Colors property to sample the color" },
				{ "Data.Comment", "/** Uses imported RGB / Intensity data */" },
				{ "Data.Name", "ELidarPointCloudColorationMode::Data" },
				{ "Data.ToolTip", "Uses imported RGB / Intensity data" },
				{ "Elevation.Comment", "/** The cloud's color will be overridden with elevation-based color */" },
				{ "Elevation.Name", "ELidarPointCloudColorationMode::Elevation" },
				{ "Elevation.ToolTip", "The cloud's color will be overridden with elevation-based color" },
				{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
				{ "None.Comment", "/** Uses color tint only */" },
				{ "None.Name", "ELidarPointCloudColorationMode::None" },
				{ "None.ToolTip", "Uses color tint only" },
				{ "Position.Comment", "/** The cloud's color will be overridden with relative position-based color */" },
				{ "Position.Name", "ELidarPointCloudColorationMode::Position" },
				{ "Position.ToolTip", "The cloud's color will be overridden with relative position-based color" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
				nullptr,
				"ELidarPointCloudColorationMode",
				"ELidarPointCloudColorationMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ELidarClippingVolumeMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LidarPointCloudRuntime_ELidarClippingVolumeMode, Z_Construct_UPackage__Script_LidarPointCloudRuntime(), TEXT("ELidarClippingVolumeMode"));
		}
		return Singleton;
	}
	template<> LIDARPOINTCLOUDRUNTIME_API UEnum* StaticEnum<ELidarClippingVolumeMode>()
	{
		return ELidarClippingVolumeMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ELidarClippingVolumeMode(ELidarClippingVolumeMode_StaticEnum, TEXT("/Script/LidarPointCloudRuntime"), TEXT("ELidarClippingVolumeMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LidarPointCloudRuntime_ELidarClippingVolumeMode_Hash() { return 942034586U; }
	UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarClippingVolumeMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LidarPointCloudRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ELidarClippingVolumeMode"), 0, Get_Z_Construct_UEnum_LidarPointCloudRuntime_ELidarClippingVolumeMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ELidarClippingVolumeMode::ClipInside", (int64)ELidarClippingVolumeMode::ClipInside },
				{ "ELidarClippingVolumeMode::ClipOutside", (int64)ELidarClippingVolumeMode::ClipOutside },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ClipInside.Comment", "/** This will clip all points inside the volume */" },
				{ "ClipInside.Name", "ELidarClippingVolumeMode::ClipInside" },
				{ "ClipInside.ToolTip", "This will clip all points inside the volume" },
				{ "ClipOutside.Comment", "/** This will clip all points outside of the volume */" },
				{ "ClipOutside.Name", "ELidarClippingVolumeMode::ClipOutside" },
				{ "ClipOutside.ToolTip", "This will clip all points outside of the volume" },
				{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
				nullptr,
				"ELidarClippingVolumeMode",
				"ELidarClippingVolumeMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ELidarPointCloudScalingMethod_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudScalingMethod, Z_Construct_UPackage__Script_LidarPointCloudRuntime(), TEXT("ELidarPointCloudScalingMethod"));
		}
		return Singleton;
	}
	template<> LIDARPOINTCLOUDRUNTIME_API UEnum* StaticEnum<ELidarPointCloudScalingMethod>()
	{
		return ELidarPointCloudScalingMethod_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ELidarPointCloudScalingMethod(ELidarPointCloudScalingMethod_StaticEnum, TEXT("/Script/LidarPointCloudRuntime"), TEXT("ELidarPointCloudScalingMethod"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudScalingMethod_Hash() { return 783602494U; }
	UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudScalingMethod()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LidarPointCloudRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ELidarPointCloudScalingMethod"), 0, Get_Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudScalingMethod_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ELidarPointCloudScalingMethod::PerNode", (int64)ELidarPointCloudScalingMethod::PerNode },
				{ "ELidarPointCloudScalingMethod::PerNodeAdaptive", (int64)ELidarPointCloudScalingMethod::PerNodeAdaptive },
				{ "ELidarPointCloudScalingMethod::PerPoint", (int64)ELidarPointCloudScalingMethod::PerPoint },
				{ "ELidarPointCloudScalingMethod::FixedScreenSize", (int64)ELidarPointCloudScalingMethod::FixedScreenSize },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "FixedScreenSize.Comment", "/**\n\x09 * Sprites will be rendered using screen-space scaling method.\n\x09 * In that mode, Point Size property will work as Screen Percentage.\n\x09 */" },
				{ "FixedScreenSize.Name", "ELidarPointCloudScalingMethod::FixedScreenSize" },
				{ "FixedScreenSize.ToolTip", "Sprites will be rendered using screen-space scaling method.\nIn that mode, Point Size property will work as Screen Percentage." },
				{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
				{ "PerNode.Comment", "/**\n\x09 * Points are scaled based on the estimated density of their containing node.\n\x09 * Recommended for assets with high variance of point densities, but may produce less fine detail overall.\n\x09 * Default method in 4.25 and 4.26\n\x09 */" },
				{ "PerNode.Name", "ELidarPointCloudScalingMethod::PerNode" },
				{ "PerNode.ToolTip", "Points are scaled based on the estimated density of their containing node.\nRecommended for assets with high variance of point densities, but may produce less fine detail overall.\nDefault method in 4.25 and 4.26" },
				{ "PerNodeAdaptive.Comment", "/**\n\x09 * Similar to PerNode, but the density is calculated adaptively based on the current view.\n\x09 * Produces good amount of fine detail while being generally resistant to density variance.\n\x09 */" },
				{ "PerNodeAdaptive.Name", "ELidarPointCloudScalingMethod::PerNodeAdaptive" },
				{ "PerNodeAdaptive.ToolTip", "Similar to PerNode, but the density is calculated adaptively based on the current view.\nProduces good amount of fine detail while being generally resistant to density variance." },
				{ "PerPoint.Comment", "/**\n\x09 * Points are scaled based on their individual calculated depth.\n\x09 * Capable of resolving the highest amount of fine detail, but is the most susceptible to \n\x09 * density changes across the dataset, and may result in patches of varying point sizes.\n\x09 */" },
				{ "PerPoint.Name", "ELidarPointCloudScalingMethod::PerPoint" },
				{ "PerPoint.ToolTip", "Points are scaled based on their individual calculated depth.\nCapable of resolving the highest amount of fine detail, but is the most susceptible to\ndensity changes across the dataset, and may result in patches of varying point sizes." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
				nullptr,
				"ELidarPointCloudScalingMethod",
				"ELidarPointCloudScalingMethod",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ELidarPointCloudAsyncMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudAsyncMode, Z_Construct_UPackage__Script_LidarPointCloudRuntime(), TEXT("ELidarPointCloudAsyncMode"));
		}
		return Singleton;
	}
	template<> LIDARPOINTCLOUDRUNTIME_API UEnum* StaticEnum<ELidarPointCloudAsyncMode>()
	{
		return ELidarPointCloudAsyncMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ELidarPointCloudAsyncMode(ELidarPointCloudAsyncMode_StaticEnum, TEXT("/Script/LidarPointCloudRuntime"), TEXT("ELidarPointCloudAsyncMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudAsyncMode_Hash() { return 3892501092U; }
	UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudAsyncMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LidarPointCloudRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ELidarPointCloudAsyncMode"), 0, Get_Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudAsyncMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ELidarPointCloudAsyncMode::Success", (int64)ELidarPointCloudAsyncMode::Success },
				{ "ELidarPointCloudAsyncMode::Failure", (int64)ELidarPointCloudAsyncMode::Failure },
				{ "ELidarPointCloudAsyncMode::Progress", (int64)ELidarPointCloudAsyncMode::Progress },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Used in blueprint latent function execution */" },
				{ "Failure.Name", "ELidarPointCloudAsyncMode::Failure" },
				{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
				{ "Progress.Name", "ELidarPointCloudAsyncMode::Progress" },
				{ "Success.Name", "ELidarPointCloudAsyncMode::Success" },
				{ "ToolTip", "Used in blueprint latent function execution" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
				nullptr,
				"ELidarPointCloudAsyncMode",
				"ELidarPointCloudAsyncMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FLidarPointCloudPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIDARPOINTCLOUDRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FLidarPointCloudPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLidarPointCloudPoint, Z_Construct_UPackage__Script_LidarPointCloudRuntime(), TEXT("LidarPointCloudPoint"), sizeof(FLidarPointCloudPoint), Get_Z_Construct_UScriptStruct_FLidarPointCloudPoint_Hash());
	}
	return Singleton;
}
template<> LIDARPOINTCLOUDRUNTIME_API UScriptStruct* StaticStruct<FLidarPointCloudPoint>()
{
	return FLidarPointCloudPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLidarPointCloudPoint(FLidarPointCloudPoint::StaticStruct, TEXT("/Script/LidarPointCloudRuntime"), TEXT("LidarPointCloudPoint"), false, nullptr, nullptr);
static struct FScriptStruct_LidarPointCloudRuntime_StaticRegisterNativesFLidarPointCloudPoint
{
	FScriptStruct_LidarPointCloudRuntime_StaticRegisterNativesFLidarPointCloudPoint()
	{
		UScriptStruct::DeferCppStructOps<FLidarPointCloudPoint>(FName(TEXT("LidarPointCloudPoint")));
	}
} ScriptStruct_LidarPointCloudRuntime_StaticRegisterNativesFLidarPointCloudPoint;
	struct Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Location_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Normal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Normal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bVisible_MetaData[];
#endif
		static void NewProp_bVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bVisible;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLidarPointCloudPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Location_MetaData[] = {
		{ "Category", "Lidar Point Cloud Point" },
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudPoint, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Location_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Location_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "Lidar Point Cloud Point" },
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudPoint, Color), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Normal_MetaData[] = {
		{ "Category", "Lidar Point Cloud Point" },
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Normal = { "Normal", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudPoint, Normal), Z_Construct_UScriptStruct_FLidarPointCloudNormal, METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Normal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Normal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_bVisible_MetaData[] = {
		{ "Category", "Lidar Point Cloud Point" },
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_bVisible_SetBit(void* Obj)
	{
		((FLidarPointCloudPoint*)Obj)->bVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_bVisible = { "bVisible", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FLidarPointCloudPoint), &Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_bVisible_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_bVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_bVisible_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_Normal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::NewProp_bVisible,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
		nullptr,
		&NewStructOps,
		"LidarPointCloudPoint",
		sizeof(FLidarPointCloudPoint),
		alignof(FLidarPointCloudPoint),
		Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLidarPointCloudPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLidarPointCloudPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LidarPointCloudRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LidarPointCloudPoint"), sizeof(FLidarPointCloudPoint), Get_Z_Construct_UScriptStruct_FLidarPointCloudPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLidarPointCloudPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLidarPointCloudPoint_Hash() { return 1628860088U; }
class UScriptStruct* FLidarPointCloudNormal::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIDARPOINTCLOUDRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FLidarPointCloudNormal_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLidarPointCloudNormal, Z_Construct_UPackage__Script_LidarPointCloudRuntime(), TEXT("LidarPointCloudNormal"), sizeof(FLidarPointCloudNormal), Get_Z_Construct_UScriptStruct_FLidarPointCloudNormal_Hash());
	}
	return Singleton;
}
template<> LIDARPOINTCLOUDRUNTIME_API UScriptStruct* StaticStruct<FLidarPointCloudNormal>()
{
	return FLidarPointCloudNormal::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLidarPointCloudNormal(FLidarPointCloudNormal::StaticStruct, TEXT("/Script/LidarPointCloudRuntime"), TEXT("LidarPointCloudNormal"), false, nullptr, nullptr);
static struct FScriptStruct_LidarPointCloudRuntime_StaticRegisterNativesFLidarPointCloudNormal
{
	FScriptStruct_LidarPointCloudRuntime_StaticRegisterNativesFLidarPointCloudNormal()
	{
		UScriptStruct::DeferCppStructOps<FLidarPointCloudNormal>(FName(TEXT("LidarPointCloudNormal")));
	}
} ScriptStruct_LidarPointCloudRuntime_StaticRegisterNativesFLidarPointCloudNormal;
	struct Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_X_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_X;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Y_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Y;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Z_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Z;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** 3D vector represented using only a single byte per component */" },
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
		{ "ToolTip", "3D vector represented using only a single byte per component" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLidarPointCloudNormal>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_X_MetaData[] = {
		{ "Category", "Lidar Point Normal" },
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_X = { "X", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudNormal, X), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_X_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_X_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_Y_MetaData[] = {
		{ "Category", "Lidar Point Normal" },
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_Y = { "Y", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudNormal, Y), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_Y_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_Y_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_Z_MetaData[] = {
		{ "Category", "Lidar Point Normal" },
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_Z = { "Z", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudNormal, Z), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_Z_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_Z_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_X,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_Y,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::NewProp_Z,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
		nullptr,
		&NewStructOps,
		"LidarPointCloudNormal",
		sizeof(FLidarPointCloudNormal),
		alignof(FLidarPointCloudNormal),
		Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLidarPointCloudNormal()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLidarPointCloudNormal_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LidarPointCloudRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LidarPointCloudNormal"), sizeof(FLidarPointCloudNormal), Get_Z_Construct_UScriptStruct_FLidarPointCloudNormal_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLidarPointCloudNormal_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLidarPointCloudNormal_Hash() { return 603846443U; }
	struct Z_Construct_UScriptStruct_FDoubleVector_Statics
	{
		struct FDoubleVector
		{
			double X;
			double Y;
			double Z;
		};

#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_X_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_X;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Y_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_Y;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Z_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_Z;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDoubleVector_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_X_MetaData[] = {
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_X = { "X", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDoubleVector, X), METADATA_PARAMS(Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_X_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_X_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_Y_MetaData[] = {
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_Y = { "Y", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDoubleVector, Y), METADATA_PARAMS(Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_Y_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_Y_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_Z_MetaData[] = {
		{ "ModuleRelativePath", "Public/LidarPointCloudShared.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_Z = { "Z", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDoubleVector, Z), METADATA_PARAMS(Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_Z_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_Z_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDoubleVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_X,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_Y,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDoubleVector_Statics::NewProp_Z,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDoubleVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
		nullptr,
		nullptr,
		"DoubleVector",
		sizeof(FDoubleVector),
		alignof(FDoubleVector),
		Z_Construct_UScriptStruct_FDoubleVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDoubleVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000008),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDoubleVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDoubleVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDoubleVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDoubleVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LidarPointCloudRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DoubleVector"), sizeof(Z_Construct_UScriptStruct_FDoubleVector_Statics::FDoubleVector), Get_Z_Construct_UScriptStruct_FDoubleVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDoubleVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDoubleVector_Hash() { return 1896035319U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
