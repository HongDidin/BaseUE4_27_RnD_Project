// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIDARPOINTCLOUDEDITOR_LidarPointCloudFactory_generated_h
#error "LidarPointCloudFactory.generated.h already included, missing '#pragma once' in LidarPointCloudFactory.h"
#endif
#define LIDARPOINTCLOUDEDITOR_LidarPointCloudFactory_generated_h

#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_SPARSE_DATA
#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULidarPointCloudFactory(); \
	friend struct Z_Construct_UClass_ULidarPointCloudFactory_Statics; \
public: \
	DECLARE_CLASS(ULidarPointCloudFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LidarPointCloudEditor"), NO_API) \
	DECLARE_SERIALIZER(ULidarPointCloudFactory)


#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_INCLASS \
private: \
	static void StaticRegisterNativesULidarPointCloudFactory(); \
	friend struct Z_Construct_UClass_ULidarPointCloudFactory_Statics; \
public: \
	DECLARE_CLASS(ULidarPointCloudFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LidarPointCloudEditor"), NO_API) \
	DECLARE_SERIALIZER(ULidarPointCloudFactory)


#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULidarPointCloudFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULidarPointCloudFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULidarPointCloudFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULidarPointCloudFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULidarPointCloudFactory(ULidarPointCloudFactory&&); \
	NO_API ULidarPointCloudFactory(const ULidarPointCloudFactory&); \
public:


#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULidarPointCloudFactory(ULidarPointCloudFactory&&); \
	NO_API ULidarPointCloudFactory(const ULidarPointCloudFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULidarPointCloudFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULidarPointCloudFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULidarPointCloudFactory)


#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_34_PROLOG
#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_SPARSE_DATA \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_INCLASS \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_SPARSE_DATA \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIDARPOINTCLOUDEDITOR_API UClass* StaticClass<class ULidarPointCloudFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_LidarPointCloud_Source_LidarPointCloudEditor_Private_LidarPointCloudFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
