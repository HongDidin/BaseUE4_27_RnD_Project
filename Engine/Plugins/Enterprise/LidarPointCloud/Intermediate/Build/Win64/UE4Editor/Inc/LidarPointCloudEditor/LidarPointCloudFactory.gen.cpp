// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LidarPointCloudEditor/Private/LidarPointCloudFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLidarPointCloudFactory() {}
// Cross Module References
	LIDARPOINTCLOUDEDITOR_API UClass* Z_Construct_UClass_ULidarPointCloudFactory_NoRegister();
	LIDARPOINTCLOUDEDITOR_API UClass* Z_Construct_UClass_ULidarPointCloudFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_LidarPointCloudEditor();
// End Cross Module References
	void ULidarPointCloudFactory::StaticRegisterNativesULidarPointCloudFactory()
	{
	}
	UClass* Z_Construct_UClass_ULidarPointCloudFactory_NoRegister()
	{
		return ULidarPointCloudFactory::StaticClass();
	}
	struct Z_Construct_UClass_ULidarPointCloudFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULidarPointCloudFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULidarPointCloudFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LidarPointCloudFactory.h" },
		{ "ModuleRelativePath", "Private/LidarPointCloudFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULidarPointCloudFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULidarPointCloudFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULidarPointCloudFactory_Statics::ClassParams = {
		&ULidarPointCloudFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULidarPointCloudFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULidarPointCloudFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULidarPointCloudFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULidarPointCloudFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULidarPointCloudFactory, 471301493);
	template<> LIDARPOINTCLOUDEDITOR_API UClass* StaticClass<ULidarPointCloudFactory>()
	{
		return ULidarPointCloudFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULidarPointCloudFactory(Z_Construct_UClass_ULidarPointCloudFactory, &ULidarPointCloudFactory::StaticClass, TEXT("/Script/LidarPointCloudEditor"), TEXT("ULidarPointCloudFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULidarPointCloudFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
