// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LidarPointCloudRuntime/Public/IO/LidarPointCloudFileIO_ASCII.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLidarPointCloudFileIO_ASCII() {}
// Cross Module References
	LIDARPOINTCLOUDRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns();
	UPackage* Z_Construct_UPackage__Script_LidarPointCloudRuntime();
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_NoRegister();
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloudFileIO_ASCII();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FLatentActionInfo();
	LIDARPOINTCLOUDRUNTIME_API UEnum* Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudAsyncMode();
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloud_NoRegister();
// End Cross Module References
class UScriptStruct* FLidarPointCloudImportSettings_ASCII_Columns::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIDARPOINTCLOUDRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns, Z_Construct_UPackage__Script_LidarPointCloudRuntime(), TEXT("LidarPointCloudImportSettings_ASCII_Columns"), sizeof(FLidarPointCloudImportSettings_ASCII_Columns), Get_Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Hash());
	}
	return Singleton;
}
template<> LIDARPOINTCLOUDRUNTIME_API UScriptStruct* StaticStruct<FLidarPointCloudImportSettings_ASCII_Columns>()
{
	return FLidarPointCloudImportSettings_ASCII_Columns::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns(FLidarPointCloudImportSettings_ASCII_Columns::StaticStruct, TEXT("/Script/LidarPointCloudRuntime"), TEXT("LidarPointCloudImportSettings_ASCII_Columns"), false, nullptr, nullptr);
static struct FScriptStruct_LidarPointCloudRuntime_StaticRegisterNativesFLidarPointCloudImportSettings_ASCII_Columns
{
	FScriptStruct_LidarPointCloudRuntime_StaticRegisterNativesFLidarPointCloudImportSettings_ASCII_Columns()
	{
		UScriptStruct::DeferCppStructOps<FLidarPointCloudImportSettings_ASCII_Columns>(FName(TEXT("LidarPointCloudImportSettings_ASCII_Columns")));
	}
} ScriptStruct_LidarPointCloudRuntime_StaticRegisterNativesFLidarPointCloudImportSettings_ASCII_Columns;
	struct Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationX_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LocationX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationY_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LocationY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationZ_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LocationZ;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Red_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Red;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Green_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Green;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Blue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Intensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Intensity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalX_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NormalX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalY_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NormalY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalZ_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NormalZ;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Used to help expose the import settings to Blueprints */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Used to help expose the import settings to Blueprints" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLidarPointCloudImportSettings_ASCII_Columns>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationX_MetaData[] = {
		{ "Category", "Import Settings" },
		{ "Comment", "/** Index of a column containing Location X data */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Index of a column containing Location X data" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationX = { "LocationX", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudImportSettings_ASCII_Columns, LocationX), METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationX_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationY_MetaData[] = {
		{ "Category", "Import Settings" },
		{ "Comment", "/** Index of a column containing Location Y data */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Index of a column containing Location Y data" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationY = { "LocationY", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudImportSettings_ASCII_Columns, LocationY), METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationY_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationZ_MetaData[] = {
		{ "Category", "Import Settings" },
		{ "Comment", "/** Index of a column containing Location Z data */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Index of a column containing Location Z data" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationZ = { "LocationZ", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudImportSettings_ASCII_Columns, LocationZ), METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationZ_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationZ_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Red_MetaData[] = {
		{ "Category", "Import Settings" },
		{ "Comment", "/** Index of a column containing Red channel. Set to -1 if not available */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Index of a column containing Red channel. Set to -1 if not available" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Red = { "Red", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudImportSettings_ASCII_Columns, Red), METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Red_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Red_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Green_MetaData[] = {
		{ "Category", "Import Settings" },
		{ "Comment", "/** Index of a column containing Green channel. Set to -1 if not available */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Index of a column containing Green channel. Set to -1 if not available" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Green = { "Green", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudImportSettings_ASCII_Columns, Green), METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Green_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Green_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Blue_MetaData[] = {
		{ "Category", "Import Settings" },
		{ "Comment", "/** Index of a column containing Blue channel. Set to -1 if not available */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Index of a column containing Blue channel. Set to -1 if not available" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Blue = { "Blue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudImportSettings_ASCII_Columns, Blue), METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Blue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Blue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Intensity_MetaData[] = {
		{ "Category", "Import Settings" },
		{ "Comment", "/** Index of a column containing Intensity channel. Set to -1 if not available */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Index of a column containing Intensity channel. Set to -1 if not available" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Intensity = { "Intensity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudImportSettings_ASCII_Columns, Intensity), METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Intensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Intensity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalX_MetaData[] = {
		{ "Category", "Import Settings" },
		{ "Comment", "/** Index of a column containing Normal X data. Set to -1 if not available */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Index of a column containing Normal X data. Set to -1 if not available" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalX = { "NormalX", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudImportSettings_ASCII_Columns, NormalX), METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalX_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalY_MetaData[] = {
		{ "Category", "Import Settings" },
		{ "Comment", "/** Index of a column containing Normal Y data. Set to -1 if not available */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Index of a column containing Normal Y data. Set to -1 if not available" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalY = { "NormalY", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudImportSettings_ASCII_Columns, NormalY), METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalY_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalZ_MetaData[] = {
		{ "Category", "Import Settings" },
		{ "Comment", "/** Index of a column containing Normal Z data. Set to -1 if not available */" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Index of a column containing Normal Z data. Set to -1 if not available" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalZ = { "NormalZ", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLidarPointCloudImportSettings_ASCII_Columns, NormalZ), METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalZ_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalZ_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_LocationZ,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Red,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Green,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Blue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_Intensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::NewProp_NormalZ,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
		nullptr,
		&NewStructOps,
		"LidarPointCloudImportSettings_ASCII_Columns",
		sizeof(FLidarPointCloudImportSettings_ASCII_Columns),
		alignof(FLidarPointCloudImportSettings_ASCII_Columns),
		Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LidarPointCloudRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LidarPointCloudImportSettings_ASCII_Columns"), sizeof(FLidarPointCloudImportSettings_ASCII_Columns), Get_Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns_Hash() { return 2419692839U; }
	DEFINE_FUNCTION(ULidarPointCloudFileIO_ASCII::execCreatePointCloudFromFile)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_PROPERTY(FStrProperty,Z_Param_Filename);
		P_GET_UBOOL(Z_Param_bUseAsync);
		P_GET_STRUCT(FVector2D,Z_Param_RGBRange);
		P_GET_STRUCT(FLidarPointCloudImportSettings_ASCII_Columns,Z_Param_Columns);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_ENUM_REF(ELidarPointCloudAsyncMode,Z_Param_Out_AsyncMode);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_Progress);
		P_GET_OBJECT_REF(ULidarPointCloud,Z_Param_Out_PointCloud);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULidarPointCloudFileIO_ASCII::CreatePointCloudFromFile(Z_Param_WorldContextObject,Z_Param_Filename,Z_Param_bUseAsync,Z_Param_RGBRange,Z_Param_Columns,Z_Param_LatentInfo,(ELidarPointCloudAsyncMode&)(Z_Param_Out_AsyncMode),Z_Param_Out_Progress,Z_Param_Out_PointCloud);
		P_NATIVE_END;
	}
	void ULidarPointCloudFileIO_ASCII::StaticRegisterNativesULidarPointCloudFileIO_ASCII()
	{
		UClass* Class = ULidarPointCloudFileIO_ASCII::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreatePointCloudFromFile", &ULidarPointCloudFileIO_ASCII::execCreatePointCloudFromFile },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics
	{
		struct LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms
		{
			UObject* WorldContextObject;
			FString Filename;
			bool bUseAsync;
			FVector2D RGBRange;
			FLidarPointCloudImportSettings_ASCII_Columns Columns;
			FLatentActionInfo LatentInfo;
			ELidarPointCloudAsyncMode AsyncMode;
			float Progress;
			ULidarPointCloud* PointCloud;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Filename_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Filename;
		static void NewProp_bUseAsync_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseAsync;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RGBRange;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Columns;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AsyncMode_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AsyncMode;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Progress;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PointCloud;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_Filename_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_Filename = { "Filename", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms, Filename), METADATA_PARAMS(Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_Filename_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_Filename_MetaData)) };
	void Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_bUseAsync_SetBit(void* Obj)
	{
		((LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms*)Obj)->bUseAsync = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_bUseAsync = { "bUseAsync", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms), &Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_bUseAsync_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_RGBRange = { "RGBRange", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms, RGBRange), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_Columns = { "Columns", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms, Columns), Z_Construct_UScriptStruct_FLidarPointCloudImportSettings_ASCII_Columns, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_AsyncMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_AsyncMode = { "AsyncMode", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms, AsyncMode), Z_Construct_UEnum_LidarPointCloudRuntime_ELidarPointCloudAsyncMode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_Progress = { "Progress", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms, Progress), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_PointCloud = { "PointCloud", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms, PointCloud), Z_Construct_UClass_ULidarPointCloud_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_Filename,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_bUseAsync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_RGBRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_Columns,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_LatentInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_AsyncMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_AsyncMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_Progress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::NewProp_PointCloud,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lidar Point Cloud" },
		{ "DisplayName", "Create Lidar Point Cloud From File (ASCII)" },
		{ "ExpandEnumAsExecs", "AsyncMode" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULidarPointCloudFileIO_ASCII, nullptr, "CreatePointCloudFromFile", nullptr, nullptr, sizeof(LidarPointCloudFileIO_ASCII_eventCreatePointCloudFromFile_Parms), Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_NoRegister()
	{
		return ULidarPointCloudFileIO_ASCII::StaticClass();
	}
	struct Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULidarPointCloudFileIO_ASCII_CreatePointCloudFromFile, "CreatePointCloudFromFile" }, // 96983
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Inherits from UBlueprintFunctionLibrary to allow exposure to Blueprint Library in the same class.\n */" },
		{ "IncludePath", "IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_ASCII.h" },
		{ "ToolTip", "Inherits from UBlueprintFunctionLibrary to allow exposure to Blueprint Library in the same class." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULidarPointCloudFileIO_ASCII>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_Statics::ClassParams = {
		&ULidarPointCloudFileIO_ASCII::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULidarPointCloudFileIO_ASCII()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULidarPointCloudFileIO_ASCII_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULidarPointCloudFileIO_ASCII, 553263525);
	template<> LIDARPOINTCLOUDRUNTIME_API UClass* StaticClass<ULidarPointCloudFileIO_ASCII>()
	{
		return ULidarPointCloudFileIO_ASCII::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULidarPointCloudFileIO_ASCII(Z_Construct_UClass_ULidarPointCloudFileIO_ASCII, &ULidarPointCloudFileIO_ASCII::StaticClass, TEXT("/Script/LidarPointCloudRuntime"), TEXT("ULidarPointCloudFileIO_ASCII"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULidarPointCloudFileIO_ASCII);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
