// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LidarPointCloudRuntime/Public/IO/LidarPointCloudFileIO_LAS.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLidarPointCloudFileIO_LAS() {}
// Cross Module References
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloudFileIO_LAS_NoRegister();
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloudFileIO_LAS();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LidarPointCloudRuntime();
// End Cross Module References
	void ULidarPointCloudFileIO_LAS::StaticRegisterNativesULidarPointCloudFileIO_LAS()
	{
	}
	UClass* Z_Construct_UClass_ULidarPointCloudFileIO_LAS_NoRegister()
	{
		return ULidarPointCloudFileIO_LAS::StaticClass();
	}
	struct Z_Construct_UClass_ULidarPointCloudFileIO_LAS_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULidarPointCloudFileIO_LAS_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULidarPointCloudFileIO_LAS_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "IO/LidarPointCloudFileIO_LAS.h" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO_LAS.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULidarPointCloudFileIO_LAS_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULidarPointCloudFileIO_LAS>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULidarPointCloudFileIO_LAS_Statics::ClassParams = {
		&ULidarPointCloudFileIO_LAS::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULidarPointCloudFileIO_LAS_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULidarPointCloudFileIO_LAS_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULidarPointCloudFileIO_LAS()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULidarPointCloudFileIO_LAS_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULidarPointCloudFileIO_LAS, 901644581);
	template<> LIDARPOINTCLOUDRUNTIME_API UClass* StaticClass<ULidarPointCloudFileIO_LAS>()
	{
		return ULidarPointCloudFileIO_LAS::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULidarPointCloudFileIO_LAS(Z_Construct_UClass_ULidarPointCloudFileIO_LAS, &ULidarPointCloudFileIO_LAS::StaticClass, TEXT("/Script/LidarPointCloudRuntime"), TEXT("ULidarPointCloudFileIO_LAS"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULidarPointCloudFileIO_LAS);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
