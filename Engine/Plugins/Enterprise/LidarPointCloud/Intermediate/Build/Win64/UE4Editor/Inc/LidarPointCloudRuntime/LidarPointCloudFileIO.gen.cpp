// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LidarPointCloudRuntime/Public/IO/LidarPointCloudFileIO.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLidarPointCloudFileIO() {}
// Cross Module References
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloudFileIO_NoRegister();
	LIDARPOINTCLOUDRUNTIME_API UClass* Z_Construct_UClass_ULidarPointCloudFileIO();
	ENGINE_API UClass* Z_Construct_UClass_UExporter();
	UPackage* Z_Construct_UPackage__Script_LidarPointCloudRuntime();
// End Cross Module References
	void ULidarPointCloudFileIO::StaticRegisterNativesULidarPointCloudFileIO()
	{
	}
	UClass* Z_Construct_UClass_ULidarPointCloudFileIO_NoRegister()
	{
		return ULidarPointCloudFileIO::StaticClass();
	}
	struct Z_Construct_UClass_ULidarPointCloudFileIO_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULidarPointCloudFileIO_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UExporter,
		(UObject* (*)())Z_Construct_UPackage__Script_LidarPointCloudRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULidarPointCloudFileIO_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds information about all registered file handlers\n */" },
		{ "IncludePath", "IO/LidarPointCloudFileIO.h" },
		{ "ModuleRelativePath", "Public/IO/LidarPointCloudFileIO.h" },
		{ "ToolTip", "Holds information about all registered file handlers" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULidarPointCloudFileIO_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULidarPointCloudFileIO>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULidarPointCloudFileIO_Statics::ClassParams = {
		&ULidarPointCloudFileIO::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_ULidarPointCloudFileIO_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULidarPointCloudFileIO_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULidarPointCloudFileIO()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULidarPointCloudFileIO_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULidarPointCloudFileIO, 1608645108);
	template<> LIDARPOINTCLOUDRUNTIME_API UClass* StaticClass<ULidarPointCloudFileIO>()
	{
		return ULidarPointCloudFileIO::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULidarPointCloudFileIO(Z_Construct_UClass_ULidarPointCloudFileIO, &ULidarPointCloudFileIO::StaticClass, TEXT("/Script/LidarPointCloudRuntime"), TEXT("ULidarPointCloudFileIO"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULidarPointCloudFileIO);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
