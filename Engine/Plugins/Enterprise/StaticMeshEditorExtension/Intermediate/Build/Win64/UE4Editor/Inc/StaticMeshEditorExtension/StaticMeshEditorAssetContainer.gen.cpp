// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StaticMeshEditorExtension/Private/StaticMeshEditorAssetContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStaticMeshEditorAssetContainer() {}
// Cross Module References
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UStaticMeshEditorAssetContainer_NoRegister();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UStaticMeshEditorAssetContainer();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
	UPackage* Z_Construct_UPackage__Script_StaticMeshEditorExtension();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void UStaticMeshEditorAssetContainer::StaticRegisterNativesUStaticMeshEditorAssetContainer()
	{
	}
	UClass* Z_Construct_UClass_UStaticMeshEditorAssetContainer_NoRegister()
	{
		return UStaticMeshEditorAssetContainer::StaticClass();
	}
	struct Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoveredGeometryMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HoveredGeometryMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoveredFaceMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HoveredFaceMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WireMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WireMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlayLineMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlayLineMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlayPointMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlayPointMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Asset container for the static mesh editor extension\n */" },
		{ "IncludePath", "StaticMeshEditorAssetContainer.h" },
		{ "ModuleRelativePath", "Private/StaticMeshEditorAssetContainer.h" },
		{ "ToolTip", "Asset container for the static mesh editor extension" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_HoveredGeometryMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Private/StaticMeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_HoveredGeometryMaterial = { "HoveredGeometryMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStaticMeshEditorAssetContainer, HoveredGeometryMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_HoveredGeometryMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_HoveredGeometryMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_HoveredFaceMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Private/StaticMeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_HoveredFaceMaterial = { "HoveredFaceMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStaticMeshEditorAssetContainer, HoveredFaceMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_HoveredFaceMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_HoveredFaceMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_WireMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Private/StaticMeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_WireMaterial = { "WireMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStaticMeshEditorAssetContainer, WireMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_WireMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_WireMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_OverlayLineMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Private/StaticMeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_OverlayLineMaterial = { "OverlayLineMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStaticMeshEditorAssetContainer, OverlayLineMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_OverlayLineMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_OverlayLineMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_OverlayPointMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Private/StaticMeshEditorAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_OverlayPointMaterial = { "OverlayPointMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStaticMeshEditorAssetContainer, OverlayPointMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_OverlayPointMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_OverlayPointMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_HoveredGeometryMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_HoveredFaceMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_WireMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_OverlayLineMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::NewProp_OverlayPointMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UStaticMeshEditorAssetContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::ClassParams = {
		&UStaticMeshEditorAssetContainer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UStaticMeshEditorAssetContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UStaticMeshEditorAssetContainer, 2146169461);
	template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<UStaticMeshEditorAssetContainer>()
	{
		return UStaticMeshEditorAssetContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UStaticMeshEditorAssetContainer(Z_Construct_UClass_UStaticMeshEditorAssetContainer, &UStaticMeshEditorAssetContainer::StaticClass, TEXT("/Script/StaticMeshEditorExtension"), TEXT("UStaticMeshEditorAssetContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UStaticMeshEditorAssetContainer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
