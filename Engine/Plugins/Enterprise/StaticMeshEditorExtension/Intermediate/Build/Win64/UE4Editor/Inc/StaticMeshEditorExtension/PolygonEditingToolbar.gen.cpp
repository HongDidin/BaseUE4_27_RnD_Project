// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StaticMeshEditorExtension/Private/PolygonEditingToolbar.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePolygonEditingToolbar() {}
// Cross Module References
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UPolygonToolbarProxyObject_NoRegister();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UPolygonToolbarProxyObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_StaticMeshEditorExtension();
// End Cross Module References
	void UPolygonToolbarProxyObject::StaticRegisterNativesUPolygonToolbarProxyObject()
	{
	}
	UClass* Z_Construct_UClass_UPolygonToolbarProxyObject_NoRegister()
	{
		return UPolygonToolbarProxyObject::StaticClass();
	}
	struct Z_Construct_UClass_UPolygonToolbarProxyObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolygonToolbarProxyObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonToolbarProxyObject_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PolygonEditingToolbar.h" },
		{ "ModuleRelativePath", "Private/PolygonEditingToolbar.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolygonToolbarProxyObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolygonToolbarProxyObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolygonToolbarProxyObject_Statics::ClassParams = {
		&UPolygonToolbarProxyObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPolygonToolbarProxyObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonToolbarProxyObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolygonToolbarProxyObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolygonToolbarProxyObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolygonToolbarProxyObject, 2916710146);
	template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<UPolygonToolbarProxyObject>()
	{
		return UPolygonToolbarProxyObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolygonToolbarProxyObject(Z_Construct_UClass_UPolygonToolbarProxyObject, &UPolygonToolbarProxyObject::StaticClass, TEXT("/Script/StaticMeshEditorExtension"), TEXT("UPolygonToolbarProxyObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolygonToolbarProxyObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
