// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STATICMESHEDITOREXTENSION_SUVGenerationTool_generated_h
#error "SUVGenerationTool.generated.h already included, missing '#pragma once' in SUVGenerationTool.h"
#endif
#define STATICMESHEDITOREXTENSION_SUVGenerationTool_generated_h

#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_SPARSE_DATA
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGenerateUVSettingsUIHolder(); \
	friend struct Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics; \
public: \
	DECLARE_CLASS(UGenerateUVSettingsUIHolder, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StaticMeshEditorExtension"), NO_API) \
	DECLARE_SERIALIZER(UGenerateUVSettingsUIHolder)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUGenerateUVSettingsUIHolder(); \
	friend struct Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics; \
public: \
	DECLARE_CLASS(UGenerateUVSettingsUIHolder, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StaticMeshEditorExtension"), NO_API) \
	DECLARE_SERIALIZER(UGenerateUVSettingsUIHolder)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGenerateUVSettingsUIHolder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGenerateUVSettingsUIHolder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGenerateUVSettingsUIHolder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGenerateUVSettingsUIHolder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGenerateUVSettingsUIHolder(UGenerateUVSettingsUIHolder&&); \
	NO_API UGenerateUVSettingsUIHolder(const UGenerateUVSettingsUIHolder&); \
public:


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGenerateUVSettingsUIHolder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGenerateUVSettingsUIHolder(UGenerateUVSettingsUIHolder&&); \
	NO_API UGenerateUVSettingsUIHolder(const UGenerateUVSettingsUIHolder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGenerateUVSettingsUIHolder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGenerateUVSettingsUIHolder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGenerateUVSettingsUIHolder)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_24_PROLOG
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_SPARSE_DATA \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_INCLASS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_SPARSE_DATA \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<class UGenerateUVSettingsUIHolder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_SUVGenerationTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
