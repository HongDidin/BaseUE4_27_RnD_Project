// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STATICMESHEDITOREXTENSION_StaticMeshEditorAssetContainer_generated_h
#error "StaticMeshEditorAssetContainer.generated.h already included, missing '#pragma once' in StaticMeshEditorAssetContainer.h"
#endif
#define STATICMESHEDITOREXTENSION_StaticMeshEditorAssetContainer_generated_h

#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_SPARSE_DATA
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUStaticMeshEditorAssetContainer(); \
	friend struct Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics; \
public: \
	DECLARE_CLASS(UStaticMeshEditorAssetContainer, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StaticMeshEditorExtension"), NO_API) \
	DECLARE_SERIALIZER(UStaticMeshEditorAssetContainer)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUStaticMeshEditorAssetContainer(); \
	friend struct Z_Construct_UClass_UStaticMeshEditorAssetContainer_Statics; \
public: \
	DECLARE_CLASS(UStaticMeshEditorAssetContainer, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StaticMeshEditorExtension"), NO_API) \
	DECLARE_SERIALIZER(UStaticMeshEditorAssetContainer)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStaticMeshEditorAssetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStaticMeshEditorAssetContainer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStaticMeshEditorAssetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStaticMeshEditorAssetContainer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStaticMeshEditorAssetContainer(UStaticMeshEditorAssetContainer&&); \
	NO_API UStaticMeshEditorAssetContainer(const UStaticMeshEditorAssetContainer&); \
public:


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStaticMeshEditorAssetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStaticMeshEditorAssetContainer(UStaticMeshEditorAssetContainer&&); \
	NO_API UStaticMeshEditorAssetContainer(const UStaticMeshEditorAssetContainer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStaticMeshEditorAssetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStaticMeshEditorAssetContainer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStaticMeshEditorAssetContainer)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_11_PROLOG
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_INCLASS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<class UStaticMeshEditorAssetContainer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_StaticMeshEditorAssetContainer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
