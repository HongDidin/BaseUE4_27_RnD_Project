// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StaticMeshEditorExtension/Private/StaticMeshAdapter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStaticMeshAdapter() {}
// Cross Module References
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter_NoRegister();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter();
	MESHEDITOR_API UClass* Z_Construct_UClass_UMeshEditorStaticMeshAdapter();
	UPackage* Z_Construct_UPackage__Script_StaticMeshEditorExtension();
// End Cross Module References
	void UStaticMeshEditorStaticMeshAdapter::StaticRegisterNativesUStaticMeshEditorStaticMeshAdapter()
	{
	}
	UClass* Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter_NoRegister()
	{
		return UStaticMeshEditorStaticMeshAdapter::StaticClass();
	}
	struct Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshEditorStaticMeshAdapter,
		(UObject* (*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "StaticMeshAdapter.h" },
		{ "ModuleRelativePath", "Private/StaticMeshAdapter.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UStaticMeshEditorStaticMeshAdapter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter_Statics::ClassParams = {
		&UStaticMeshEditorStaticMeshAdapter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UStaticMeshEditorStaticMeshAdapter, 1891517157);
	template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<UStaticMeshEditorStaticMeshAdapter>()
	{
		return UStaticMeshEditorStaticMeshAdapter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UStaticMeshEditorStaticMeshAdapter(Z_Construct_UClass_UStaticMeshEditorStaticMeshAdapter, &UStaticMeshEditorStaticMeshAdapter::StaticClass, TEXT("/Script/StaticMeshEditorExtension"), TEXT("UStaticMeshEditorStaticMeshAdapter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UStaticMeshEditorStaticMeshAdapter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
