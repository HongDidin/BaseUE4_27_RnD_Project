// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StaticMeshEditorExtension/Public/UVTools/UVGenerationFlattenMapping.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUVGenerationFlattenMapping() {}
// Cross Module References
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UUVGenerationFlattenMapping_NoRegister();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UUVGenerationFlattenMapping();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_StaticMeshEditorExtension();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UUVGenerationFlattenMapping::execGenerateFlattenMappingUVs)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_InStaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_UVChannel);
		P_GET_PROPERTY(FFloatProperty,Z_Param_AngleThreshold);
		P_FINISH;
		P_NATIVE_BEGIN;
		UUVGenerationFlattenMapping::GenerateFlattenMappingUVs(Z_Param_InStaticMesh,Z_Param_UVChannel,Z_Param_AngleThreshold);
		P_NATIVE_END;
	}
	void UUVGenerationFlattenMapping::StaticRegisterNativesUUVGenerationFlattenMapping()
	{
		UClass* Class = UUVGenerationFlattenMapping::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GenerateFlattenMappingUVs", &UUVGenerationFlattenMapping::execGenerateFlattenMappingUVs },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics
	{
		struct UVGenerationFlattenMapping_eventGenerateFlattenMappingUVs_Parms
		{
			UStaticMesh* InStaticMesh;
			int32 UVChannel;
			float AngleThreshold;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InStaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UVChannel;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngleThreshold;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::NewProp_InStaticMesh = { "InStaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVGenerationFlattenMapping_eventGenerateFlattenMappingUVs_Parms, InStaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::NewProp_UVChannel = { "UVChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVGenerationFlattenMapping_eventGenerateFlattenMappingUVs_Parms, UVChannel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::NewProp_AngleThreshold = { "AngleThreshold", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVGenerationFlattenMapping_eventGenerateFlattenMappingUVs_Parms, AngleThreshold), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::NewProp_InStaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::NewProp_UVChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::NewProp_AngleThreshold,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | StaticMesh" },
		{ "CPP_Default_AngleThreshold", "66.000000" },
		{ "DisplayName", "Generate Unwrapped UVs" },
		{ "ModuleRelativePath", "Public/UVTools/UVGenerationFlattenMapping.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUVGenerationFlattenMapping, nullptr, "GenerateFlattenMappingUVs", nullptr, nullptr, sizeof(UVGenerationFlattenMapping_eventGenerateFlattenMappingUVs_Parms), Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UUVGenerationFlattenMapping_NoRegister()
	{
		return UUVGenerationFlattenMapping::StaticClass();
	}
	struct Z_Construct_UClass_UUVGenerationFlattenMapping_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVGenerationFlattenMapping_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UUVGenerationFlattenMapping_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UUVGenerationFlattenMapping_GenerateFlattenMappingUVs, "GenerateFlattenMappingUVs" }, // 11881626
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVGenerationFlattenMapping_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "UVTools/UVGenerationFlattenMapping.h" },
		{ "ModuleRelativePath", "Public/UVTools/UVGenerationFlattenMapping.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVGenerationFlattenMapping_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVGenerationFlattenMapping>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVGenerationFlattenMapping_Statics::ClassParams = {
		&UUVGenerationFlattenMapping::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUVGenerationFlattenMapping_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVGenerationFlattenMapping_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVGenerationFlattenMapping()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVGenerationFlattenMapping_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVGenerationFlattenMapping, 1984640989);
	template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<UUVGenerationFlattenMapping>()
	{
		return UUVGenerationFlattenMapping::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVGenerationFlattenMapping(Z_Construct_UClass_UUVGenerationFlattenMapping, &UUVGenerationFlattenMapping::StaticClass, TEXT("/Script/StaticMeshEditorExtension"), TEXT("UUVGenerationFlattenMapping"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVGenerationFlattenMapping);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
