// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshProcessingLibrary/Public/MeshProcessingLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshProcessingLibrary() {}
// Cross Module References
	MESHPROCESSINGLIBRARY_API UEnum* Z_Construct_UEnum_MeshProcessingLibrary_EJacketingTarget();
	UPackage* Z_Construct_UPackage__Script_MeshProcessingLibrary();
	MESHPROCESSINGLIBRARY_API UClass* Z_Construct_UClass_UJacketingOptions_NoRegister();
	MESHPROCESSINGLIBRARY_API UClass* Z_Construct_UClass_UJacketingOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHPROCESSINGLIBRARY_API UClass* Z_Construct_UClass_UMeshDefeaturingParameterObject_NoRegister();
	MESHPROCESSINGLIBRARY_API UClass* Z_Construct_UClass_UMeshDefeaturingParameterObject();
	MESHPROCESSINGLIBRARY_API UClass* Z_Construct_UClass_UMeshProcessingEnterpriseSettings_NoRegister();
	MESHPROCESSINGLIBRARY_API UClass* Z_Construct_UClass_UMeshProcessingEnterpriseSettings();
	MESHPROCESSINGLIBRARY_API UClass* Z_Construct_UClass_UMeshProcessingLibrary_NoRegister();
	MESHPROCESSINGLIBRARY_API UClass* Z_Construct_UClass_UMeshProcessingLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
// End Cross Module References
	static UEnum* EJacketingTarget_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshProcessingLibrary_EJacketingTarget, Z_Construct_UPackage__Script_MeshProcessingLibrary(), TEXT("EJacketingTarget"));
		}
		return Singleton;
	}
	template<> MESHPROCESSINGLIBRARY_API UEnum* StaticEnum<EJacketingTarget>()
	{
		return EJacketingTarget_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EJacketingTarget(EJacketingTarget_StaticEnum, TEXT("/Script/MeshProcessingLibrary"), TEXT("EJacketingTarget"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshProcessingLibrary_EJacketingTarget_Hash() { return 943113505U; }
	UEnum* Z_Construct_UEnum_MeshProcessingLibrary_EJacketingTarget()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshProcessingLibrary();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EJacketingTarget"), 0, Get_Z_Construct_UEnum_MeshProcessingLibrary_EJacketingTarget_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EJacketingTarget::Level", (int64)EJacketingTarget::Level },
				{ "EJacketingTarget::Mesh", (int64)EJacketingTarget::Mesh },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Level.Comment", "/** Apply jacketing on the level, will hide/tag/destroy actors and static mesh components. */" },
				{ "Level.Name", "EJacketingTarget::Level" },
				{ "Level.ToolTip", "Apply jacketing on the level, will hide/tag/destroy actors and static mesh components." },
				{ "Mesh.Comment", "/** Apply jacketing on the mesh, will remove triangles/vertices. */" },
				{ "Mesh.Name", "EJacketingTarget::Mesh" },
				{ "Mesh.ToolTip", "Apply jacketing on the mesh, will remove triangles/vertices." },
				{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshProcessingLibrary,
				nullptr,
				"EJacketingTarget",
				"EJacketingTarget",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UJacketingOptions::StaticRegisterNativesUJacketingOptions()
	{
	}
	UClass* Z_Construct_UClass_UJacketingOptions_NoRegister()
	{
		return UJacketingOptions::StaticClass();
	}
	struct Z_Construct_UClass_UJacketingOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Accuracy_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Accuracy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MergeDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MergeDistance;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Target_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Target;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UJacketingOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshProcessingLibrary,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJacketingOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MeshProcessingLibrary.h" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Accuracy_MetaData[] = {
		{ "Category", "Jacketing" },
		{ "DisplayName", "Voxel Precision" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Set the minimum size of voxels" },
		{ "Units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Accuracy = { "Accuracy", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UJacketingOptions, Accuracy), METADATA_PARAMS(Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Accuracy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Accuracy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJacketingOptions_Statics::NewProp_MergeDistance_MetaData[] = {
		{ "Category", "Jacketing" },
		{ "DisplayName", "Gap Max Diameter" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Maximum diameter of gaps to fill" },
		{ "Units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UJacketingOptions_Statics::NewProp_MergeDistance = { "MergeDistance", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UJacketingOptions, MergeDistance), METADATA_PARAMS(Z_Construct_UClass_UJacketingOptions_Statics::NewProp_MergeDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UJacketingOptions_Statics::NewProp_MergeDistance_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Target_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Target_MetaData[] = {
		{ "Category", "Jacketing" },
		{ "DisplayName", "Action Level" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Action to be applied on actors (Level) or triangles (Mesh)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UJacketingOptions, Target), Z_Construct_UEnum_MeshProcessingLibrary_EJacketingTarget, METADATA_PARAMS(Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Target_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UJacketingOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Accuracy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJacketingOptions_Statics::NewProp_MergeDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Target_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJacketingOptions_Statics::NewProp_Target,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UJacketingOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UJacketingOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UJacketingOptions_Statics::ClassParams = {
		&UJacketingOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UJacketingOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UJacketingOptions_Statics::PropPointers),
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UJacketingOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UJacketingOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UJacketingOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UJacketingOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UJacketingOptions, 3605082580);
	template<> MESHPROCESSINGLIBRARY_API UClass* StaticClass<UJacketingOptions>()
	{
		return UJacketingOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UJacketingOptions(Z_Construct_UClass_UJacketingOptions, &UJacketingOptions::StaticClass, TEXT("/Script/MeshProcessingLibrary"), TEXT("UJacketingOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UJacketingOptions);
	void UMeshDefeaturingParameterObject::StaticRegisterNativesUMeshDefeaturingParameterObject()
	{
	}
	UClass* Z_Construct_UClass_UMeshDefeaturingParameterObject_NoRegister()
	{
		return UMeshDefeaturingParameterObject::StaticClass();
	}
	struct Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFillThroughHoles_MetaData[];
#endif
		static void NewProp_bFillThroughHoles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFillThroughHoles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThroughHoleMaxDiameter_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ThroughHoleMaxDiameter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFillBlindHoles_MetaData[];
#endif
		static void NewProp_bFillBlindHoles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFillBlindHoles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilledHoleMaxDiameter_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FilledHoleMaxDiameter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilledHoleMaxDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FilledHoleMaxDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveProtrusions_MetaData[];
#endif
		static void NewProp_bRemoveProtrusions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveProtrusions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProtrusionMaxDiameter_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ProtrusionMaxDiameter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProtrusionMaxHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ProtrusionMaxHeight;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshProcessingLibrary,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MeshProcessingLibrary.h" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillThroughHoles_MetaData[] = {
		{ "Category", "ThroughHoles" },
		{ "DisplayName", "Fill through holes" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Enable filling of through holes with diameter smaller than a given maximum" },
	};
#endif
	void Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillThroughHoles_SetBit(void* Obj)
	{
		((UMeshDefeaturingParameterObject*)Obj)->bFillThroughHoles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillThroughHoles = { "bFillThroughHoles", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshDefeaturingParameterObject), &Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillThroughHoles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillThroughHoles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillThroughHoles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ThroughHoleMaxDiameter_MetaData[] = {
		{ "Category", "ThroughHoles" },
		{ "DisplayName", "Filled holes max diameter" },
		{ "EditCondition", "bFillThroughHoles" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Maximum diameter of through holes to fill" },
		{ "Units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ThroughHoleMaxDiameter = { "ThroughHoleMaxDiameter", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshDefeaturingParameterObject, ThroughHoleMaxDiameter), METADATA_PARAMS(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ThroughHoleMaxDiameter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ThroughHoleMaxDiameter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillBlindHoles_MetaData[] = {
		{ "Category", "BlindHoles" },
		{ "DisplayName", "Fill blind holes" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Enable filling of non emerging (blind) holes with diameter smaller than a given maximum" },
	};
#endif
	void Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillBlindHoles_SetBit(void* Obj)
	{
		((UMeshDefeaturingParameterObject*)Obj)->bFillBlindHoles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillBlindHoles = { "bFillBlindHoles", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshDefeaturingParameterObject), &Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillBlindHoles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillBlindHoles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillBlindHoles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_FilledHoleMaxDiameter_MetaData[] = {
		{ "Category", "BlindHoles" },
		{ "DisplayName", "Filled hole max diameter" },
		{ "EditCondition", "bFillBlindHoles" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Maximum diameter of blind holes to fill" },
		{ "Units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_FilledHoleMaxDiameter = { "FilledHoleMaxDiameter", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshDefeaturingParameterObject, FilledHoleMaxDiameter), METADATA_PARAMS(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_FilledHoleMaxDiameter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_FilledHoleMaxDiameter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_FilledHoleMaxDepth_MetaData[] = {
		{ "Category", "BlindHoles" },
		{ "DisplayName", "Filled hole max depth" },
		{ "EditCondition", "bFillBlindHoles" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Maximum depth of blind holes to fill" },
		{ "Units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_FilledHoleMaxDepth = { "FilledHoleMaxDepth", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshDefeaturingParameterObject, FilledHoleMaxDepth), METADATA_PARAMS(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_FilledHoleMaxDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_FilledHoleMaxDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bRemoveProtrusions_MetaData[] = {
		{ "Category", "Protrusion" },
		{ "DisplayName", "Remove protrusions" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Remove bumps under a maximal height" },
	};
#endif
	void Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bRemoveProtrusions_SetBit(void* Obj)
	{
		((UMeshDefeaturingParameterObject*)Obj)->bRemoveProtrusions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bRemoveProtrusions = { "bRemoveProtrusions", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshDefeaturingParameterObject), &Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bRemoveProtrusions_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bRemoveProtrusions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bRemoveProtrusions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ProtrusionMaxDiameter_MetaData[] = {
		{ "Category", "Protrusion" },
		{ "DisplayName", "Protrusion max diameter" },
		{ "EditCondition", "bRemoveProtrusions" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Maximum diameter of protrusions to remove" },
		{ "Units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ProtrusionMaxDiameter = { "ProtrusionMaxDiameter", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshDefeaturingParameterObject, ProtrusionMaxDiameter), METADATA_PARAMS(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ProtrusionMaxDiameter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ProtrusionMaxDiameter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ProtrusionMaxHeight_MetaData[] = {
		{ "Category", "Protrusion" },
		{ "DisplayName", "Protrusion max height" },
		{ "EditCondition", "bRemoveProtrusions" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Maximum height of protrusions to remove" },
		{ "Units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ProtrusionMaxHeight = { "ProtrusionMaxHeight", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshDefeaturingParameterObject, ProtrusionMaxHeight), METADATA_PARAMS(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ProtrusionMaxHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ProtrusionMaxHeight_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillThroughHoles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ThroughHoleMaxDiameter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bFillBlindHoles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_FilledHoleMaxDiameter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_FilledHoleMaxDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_bRemoveProtrusions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ProtrusionMaxDiameter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::NewProp_ProtrusionMaxHeight,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshDefeaturingParameterObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::ClassParams = {
		&UMeshDefeaturingParameterObject::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::PropPointers),
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshDefeaturingParameterObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshDefeaturingParameterObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshDefeaturingParameterObject, 486903182);
	template<> MESHPROCESSINGLIBRARY_API UClass* StaticClass<UMeshDefeaturingParameterObject>()
	{
		return UMeshDefeaturingParameterObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshDefeaturingParameterObject(Z_Construct_UClass_UMeshDefeaturingParameterObject, &UMeshDefeaturingParameterObject::StaticClass, TEXT("/Script/MeshProcessingLibrary"), TEXT("UMeshDefeaturingParameterObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshDefeaturingParameterObject);
	void UMeshProcessingEnterpriseSettings::StaticRegisterNativesUMeshProcessingEnterpriseSettings()
	{
	}
	UClass* Z_Construct_UClass_UMeshProcessingEnterpriseSettings_NoRegister()
	{
		return UMeshProcessingEnterpriseSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideUndoBufferSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OverrideUndoBufferSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshProcessingLibrary,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshProcessingLibrary.h" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::NewProp_OverrideUndoBufferSize_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "Units", "MB" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::NewProp_OverrideUndoBufferSize = { "OverrideUndoBufferSize", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshProcessingEnterpriseSettings, OverrideUndoBufferSize), METADATA_PARAMS(Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::NewProp_OverrideUndoBufferSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::NewProp_OverrideUndoBufferSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::NewProp_OverrideUndoBufferSize,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshProcessingEnterpriseSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::ClassParams = {
		&UMeshProcessingEnterpriseSettings::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshProcessingEnterpriseSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshProcessingEnterpriseSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshProcessingEnterpriseSettings, 982284115);
	template<> MESHPROCESSINGLIBRARY_API UClass* StaticClass<UMeshProcessingEnterpriseSettings>()
	{
		return UMeshProcessingEnterpriseSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshProcessingEnterpriseSettings(Z_Construct_UClass_UMeshProcessingEnterpriseSettings, &UMeshProcessingEnterpriseSettings::StaticClass, TEXT("/Script/MeshProcessingLibrary"), TEXT("UMeshProcessingEnterpriseSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshProcessingEnterpriseSettings);
	DEFINE_FUNCTION(UMeshProcessingLibrary::execApplyJacketingOnMeshActors)
	{
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_Actors);
		P_GET_OBJECT(UJacketingOptions,Z_Param_Options);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_OccludedActorArray);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMeshProcessingLibrary::ApplyJacketingOnMeshActors(Z_Param_Out_Actors,Z_Param_Options,Z_Param_Out_OccludedActorArray);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMeshProcessingLibrary::execDefeatureMesh)
	{
		P_GET_OBJECT(UStaticMesh,Z_Param_StaticMesh);
		P_GET_PROPERTY(FIntProperty,Z_Param_LODIndex);
		P_GET_OBJECT(UMeshDefeaturingParameterObject,Z_Param_Parameters);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMeshProcessingLibrary::DefeatureMesh(Z_Param_StaticMesh,Z_Param_LODIndex,Z_Param_Parameters);
		P_NATIVE_END;
	}
	void UMeshProcessingLibrary::StaticRegisterNativesUMeshProcessingLibrary()
	{
		UClass* Class = UMeshProcessingLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ApplyJacketingOnMeshActors", &UMeshProcessingLibrary::execApplyJacketingOnMeshActors },
			{ "DefeatureMesh", &UMeshProcessingLibrary::execDefeatureMesh },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics
	{
		struct MeshProcessingLibrary_eventApplyJacketingOnMeshActors_Parms
		{
			TArray<AActor*> Actors;
			const UJacketingOptions* Options;
			TArray<AActor*> OccludedActorArray;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Options_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OccludedActorArray_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OccludedActorArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Actors_Inner = { "Actors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Actors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Actors = { "Actors", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MeshProcessingLibrary_eventApplyJacketingOnMeshActors_Parms, Actors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Actors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Actors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Options_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MeshProcessingLibrary_eventApplyJacketingOnMeshActors_Parms, Options), Z_Construct_UClass_UJacketingOptions_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Options_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Options_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_OccludedActorArray_Inner = { "OccludedActorArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_OccludedActorArray = { "OccludedActorArray", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MeshProcessingLibrary_eventApplyJacketingOnMeshActors_Parms, OccludedActorArray), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Actors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Actors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_Options,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_OccludedActorArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::NewProp_OccludedActorArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mesh Processing | MeshActor" },
		{ "Comment", "/**\n\x09 * Detect partially or totally occluded objects in a list of actors. Truncate partially occluded meshes.\n\x09 * @param\x09""Actors\x09\x09\x09\x09List of actors to process.\n\x09 * @param\x09Options\x09\x09\x09\x09Parameter values to use for the jacketing.\n\x09 * @param\x09OccludedActorArray\x09""Array of actors which are fully occluded. Only filled if target is EJacketingTarget::Level.\n\x09 */" },
		{ "DisplayName", "Simplify Assembly" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Detect partially or totally occluded objects in a list of actors. Truncate partially occluded meshes.\n@param       Actors                          List of actors to process.\n@param       Options                         Parameter values to use for the jacketing.\n@param       OccludedActorArray      Array of actors which are fully occluded. Only filled if target is EJacketingTarget::Level." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshProcessingLibrary, nullptr, "ApplyJacketingOnMeshActors", nullptr, nullptr, sizeof(MeshProcessingLibrary_eventApplyJacketingOnMeshActors_Parms), Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics
	{
		struct MeshProcessingLibrary_eventDefeatureMesh_Parms
		{
			UStaticMesh* StaticMesh;
			int32 LODIndex;
			const UMeshDefeaturingParameterObject* Parameters;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Parameters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MeshProcessingLibrary_eventDefeatureMesh_Parms, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MeshProcessingLibrary_eventDefeatureMesh_Parms, LODIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::NewProp_Parameters_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::NewProp_Parameters = { "Parameters", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MeshProcessingLibrary_eventDefeatureMesh_Parms, Parameters), Z_Construct_UClass_UMeshDefeaturingParameterObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::NewProp_Parameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::NewProp_Parameters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::NewProp_Parameters,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mesh Processing | StaticMesh" },
		{ "Comment", "/**\n\x09 * Remove holes, emerging and/or non-emerging, and bumps (features).\n\x09 * @param\x09StaticMesh\x09\x09\x09Static mesh to remove features from.\n\x09 * @param\x09LODIndex\x09\x09\x09Index of the StaticMesh LOD.\n\x09 * @param\x09Parameters\x09\x09\x09Parameter values to use for the defeaturing.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
		{ "ToolTip", "Remove holes, emerging and/or non-emerging, and bumps (features).\n@param       StaticMesh                      Static mesh to remove features from.\n@param       LODIndex                        Index of the StaticMesh LOD.\n@param       Parameters                      Parameter values to use for the defeaturing." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMeshProcessingLibrary, nullptr, "DefeatureMesh", nullptr, nullptr, sizeof(MeshProcessingLibrary_eventDefeatureMesh_Parms), Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMeshProcessingLibrary_NoRegister()
	{
		return UMeshProcessingLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMeshProcessingLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshProcessingLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshProcessingLibrary,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMeshProcessingLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMeshProcessingLibrary_ApplyJacketingOnMeshActors, "ApplyJacketingOnMeshActors" }, // 3616474581
		{ &Z_Construct_UFunction_UMeshProcessingLibrary_DefeatureMesh, "DefeatureMesh" }, // 4006285236
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshProcessingLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshProcessingLibrary.h" },
		{ "ModuleRelativePath", "Public/MeshProcessingLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshProcessingLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshProcessingLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshProcessingLibrary_Statics::ClassParams = {
		&UMeshProcessingLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshProcessingLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshProcessingLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshProcessingLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshProcessingLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshProcessingLibrary, 91997062);
	template<> MESHPROCESSINGLIBRARY_API UClass* StaticClass<UMeshProcessingLibrary>()
	{
		return UMeshProcessingLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshProcessingLibrary(Z_Construct_UClass_UMeshProcessingLibrary, &UMeshProcessingLibrary::StaticClass, TEXT("/Script/MeshProcessingLibrary"), TEXT("UMeshProcessingLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshProcessingLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
