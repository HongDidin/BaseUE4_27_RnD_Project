// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StaticMeshEditorExtension/Private/UVTools/UVGenerationDataprepOperation.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUVGenerationDataprepOperation() {}
// Cross Module References
	STATICMESHEDITOREXTENSION_API UEnum* Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVDatasmithOperationChannelSelection();
	UPackage* Z_Construct_UPackage__Script_StaticMeshEditorExtension();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UUVGenerationFlattenMappingOperation_NoRegister();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UUVGenerationFlattenMappingOperation();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepOperation();
// End Cross Module References
	static UEnum* EUnwrappedUVDatasmithOperationChannelSelection_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVDatasmithOperationChannelSelection, Z_Construct_UPackage__Script_StaticMeshEditorExtension(), TEXT("EUnwrappedUVDatasmithOperationChannelSelection"));
		}
		return Singleton;
	}
	template<> STATICMESHEDITOREXTENSION_API UEnum* StaticEnum<EUnwrappedUVDatasmithOperationChannelSelection>()
	{
		return EUnwrappedUVDatasmithOperationChannelSelection_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUnwrappedUVDatasmithOperationChannelSelection(EUnwrappedUVDatasmithOperationChannelSelection_StaticEnum, TEXT("/Script/StaticMeshEditorExtension"), TEXT("EUnwrappedUVDatasmithOperationChannelSelection"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVDatasmithOperationChannelSelection_Hash() { return 662754058U; }
	UEnum* Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVDatasmithOperationChannelSelection()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_StaticMeshEditorExtension();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUnwrappedUVDatasmithOperationChannelSelection"), 0, Get_Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVDatasmithOperationChannelSelection_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUnwrappedUVDatasmithOperationChannelSelection::FirstEmptyChannel", (int64)EUnwrappedUVDatasmithOperationChannelSelection::FirstEmptyChannel },
				{ "EUnwrappedUVDatasmithOperationChannelSelection::SpecifyChannel", (int64)EUnwrappedUVDatasmithOperationChannelSelection::SpecifyChannel },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "FirstEmptyChannel.Name", "EUnwrappedUVDatasmithOperationChannelSelection::FirstEmptyChannel" },
				{ "FirstEmptyChannel.Tooltip", "Generate the unwrapped UV in the first UV channel that is empty." },
				{ "ModuleRelativePath", "Private/UVTools/UVGenerationDataprepOperation.h" },
				{ "SpecifyChannel.Name", "EUnwrappedUVDatasmithOperationChannelSelection::SpecifyChannel" },
				{ "SpecifyChannel.Tooltip", "Manually select the target UV channel for the unwrapped UV generation." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
				nullptr,
				"EUnwrappedUVDatasmithOperationChannelSelection",
				"EUnwrappedUVDatasmithOperationChannelSelection",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UUVGenerationFlattenMappingOperation::StaticRegisterNativesUUVGenerationFlattenMappingOperation()
	{
	}
	UClass* Z_Construct_UClass_UUVGenerationFlattenMappingOperation_NoRegister()
	{
		return UUVGenerationFlattenMappingOperation::StaticClass();
	}
	struct Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ChannelSelection_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelSelection_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ChannelSelection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_UVChannel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngleThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngleThreshold;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "MeshOperation" },
		{ "DevelopmentStatus", "Experimental" },
		{ "DisplayName", "Generate Unwrapped UVs" },
		{ "IncludePath", "UVTools/UVGenerationDataprepOperation.h" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationDataprepOperation.h" },
		{ "ToolTip", "For each static mesh to process, generate an unwrapped UV map in the specified channel" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_ChannelSelection_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_ChannelSelection_MetaData[] = {
		{ "Category", "UV Generation Settings" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationDataprepOperation.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_ChannelSelection = { "ChannelSelection", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVGenerationFlattenMappingOperation, ChannelSelection), Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVDatasmithOperationChannelSelection, METADATA_PARAMS(Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_ChannelSelection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_ChannelSelection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_UVChannel_MetaData[] = {
		{ "Category", "UV Generation Settings" },
		{ "ClampMax", "7" },
		{ "ClampMin", "0" },
		{ "DisplayName", "UV Channel" },
		{ "EditCondition", "ChannelSelection == EUnwrappedUVDatasmithOperationChannelSelection::SpecifyChannel" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationDataprepOperation.h" },
		{ "ToolTip", "The UV channel where to generate the flatten mapping" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_UVChannel = { "UVChannel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVGenerationFlattenMappingOperation, UVChannel), METADATA_PARAMS(Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_UVChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_UVChannel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_AngleThreshold_MetaData[] = {
		{ "Category", "UV Generation Settings" },
		{ "ClampMax", "90" },
		{ "ClampMin", "1" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationDataprepOperation.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_AngleThreshold = { "AngleThreshold", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVGenerationFlattenMappingOperation, AngleThreshold), METADATA_PARAMS(Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_AngleThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_AngleThreshold_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_ChannelSelection_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_ChannelSelection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_UVChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::NewProp_AngleThreshold,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVGenerationFlattenMappingOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::ClassParams = {
		&UUVGenerationFlattenMappingOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVGenerationFlattenMappingOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVGenerationFlattenMappingOperation, 2421673402);
	template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<UUVGenerationFlattenMappingOperation>()
	{
		return UUVGenerationFlattenMappingOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVGenerationFlattenMappingOperation(Z_Construct_UClass_UUVGenerationFlattenMappingOperation, &UUVGenerationFlattenMappingOperation::StaticClass, TEXT("/Script/StaticMeshEditorExtension"), TEXT("UUVGenerationFlattenMappingOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVGenerationFlattenMappingOperation);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
