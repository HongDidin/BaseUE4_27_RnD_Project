// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StaticMeshEditorExtension/Private/UVTools/UVGenerationFlattenMappingTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUVGenerationFlattenMappingTool() {}
// Cross Module References
	STATICMESHEDITOREXTENSION_API UEnum* Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVChannelSelection();
	UPackage* Z_Construct_UPackage__Script_StaticMeshEditorExtension();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UUVUnwrapSettings_NoRegister();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UUVUnwrapSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject_NoRegister();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject();
// End Cross Module References
	static UEnum* EUnwrappedUVChannelSelection_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVChannelSelection, Z_Construct_UPackage__Script_StaticMeshEditorExtension(), TEXT("EUnwrappedUVChannelSelection"));
		}
		return Singleton;
	}
	template<> STATICMESHEDITOREXTENSION_API UEnum* StaticEnum<EUnwrappedUVChannelSelection>()
	{
		return EUnwrappedUVChannelSelection_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUnwrappedUVChannelSelection(EUnwrappedUVChannelSelection_StaticEnum, TEXT("/Script/StaticMeshEditorExtension"), TEXT("EUnwrappedUVChannelSelection"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVChannelSelection_Hash() { return 3102658021U; }
	UEnum* Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVChannelSelection()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_StaticMeshEditorExtension();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUnwrappedUVChannelSelection"), 0, Get_Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVChannelSelection_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUnwrappedUVChannelSelection::AutomaticLightmapSetup", (int64)EUnwrappedUVChannelSelection::AutomaticLightmapSetup },
				{ "EUnwrappedUVChannelSelection::FirstEmptyChannel", (int64)EUnwrappedUVChannelSelection::FirstEmptyChannel },
				{ "EUnwrappedUVChannelSelection::SpecifyChannel", (int64)EUnwrappedUVChannelSelection::SpecifyChannel },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AutomaticLightmapSetup.Name", "EUnwrappedUVChannelSelection::AutomaticLightmapSetup" },
				{ "AutomaticLightmapSetup.Tooltip", "Enable lightmap generation and use the generated unwrapped UV as the lightmap source." },
				{ "FirstEmptyChannel.Name", "EUnwrappedUVChannelSelection::FirstEmptyChannel" },
				{ "FirstEmptyChannel.Tooltip", "Generate the unwrapped UV in the first UV channel that is empty." },
				{ "ModuleRelativePath", "Private/UVTools/UVGenerationFlattenMappingTool.h" },
				{ "SpecifyChannel.Name", "EUnwrappedUVChannelSelection::SpecifyChannel" },
				{ "SpecifyChannel.Tooltip", "Manually select the target UV channel for the unwrapped UV generation." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
				nullptr,
				"EUnwrappedUVChannelSelection",
				"EUnwrappedUVChannelSelection",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UUVUnwrapSettings::StaticRegisterNativesUUVUnwrapSettings()
	{
	}
	UClass* Z_Construct_UClass_UUVUnwrapSettings_NoRegister()
	{
		return UUVUnwrapSettings::StaticClass();
	}
	struct Z_Construct_UClass_UUVUnwrapSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ChannelSelection_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelSelection_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ChannelSelection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UVChannel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngleThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngleThreshold;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVUnwrapSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVUnwrapSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "UVTools/UVGenerationFlattenMappingTool.h" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationFlattenMappingTool.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_ChannelSelection_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_ChannelSelection_MetaData[] = {
		{ "Category", "Flatten Mapping" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationFlattenMappingTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_ChannelSelection = { "ChannelSelection", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVUnwrapSettings, ChannelSelection), Z_Construct_UEnum_StaticMeshEditorExtension_EUnwrappedUVChannelSelection, METADATA_PARAMS(Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_ChannelSelection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_ChannelSelection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_UVChannel_MetaData[] = {
		{ "Category", "Flatten Mapping" },
		{ "ClampMax", "7" },
		{ "ClampMin", "0" },
		{ "EditCondition", "ChannelSelection == EUnwrappedUVChannelSelection::SpecifyChannel" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationFlattenMappingTool.h" },
		{ "ToolTip", "The UV channel where to generate the flatten mapping" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_UVChannel = { "UVChannel", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVUnwrapSettings, UVChannel), METADATA_PARAMS(Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_UVChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_UVChannel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_AngleThreshold_MetaData[] = {
		{ "Category", "Flatten Mapping" },
		{ "ClampMax", "90" },
		{ "ClampMin", "1" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationFlattenMappingTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_AngleThreshold = { "AngleThreshold", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVUnwrapSettings, AngleThreshold), METADATA_PARAMS(Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_AngleThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_AngleThreshold_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUVUnwrapSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_ChannelSelection_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_ChannelSelection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_UVChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVUnwrapSettings_Statics::NewProp_AngleThreshold,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVUnwrapSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVUnwrapSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVUnwrapSettings_Statics::ClassParams = {
		&UUVUnwrapSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUVUnwrapSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUVUnwrapSettings_Statics::PropPointers),
		0,
		0x000000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UUVUnwrapSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVUnwrapSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVUnwrapSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVUnwrapSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVUnwrapSettings, 2353280303);
	template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<UUVUnwrapSettings>()
	{
		return UUVUnwrapSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVUnwrapSettings(Z_Construct_UClass_UUVUnwrapSettings, &UUVUnwrapSettings::StaticClass, TEXT("/Script/StaticMeshEditorExtension"), TEXT("UUVUnwrapSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVUnwrapSettings);
	void UUVGenerationFlattenMappingToolbarProxyObject::StaticRegisterNativesUUVGenerationFlattenMappingToolbarProxyObject()
	{
	}
	UClass* Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject_NoRegister()
	{
		return UUVGenerationFlattenMappingToolbarProxyObject::StaticClass();
	}
	struct Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "UVTools/UVGenerationFlattenMappingTool.h" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationFlattenMappingTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVGenerationFlattenMappingToolbarProxyObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject_Statics::ClassParams = {
		&UUVGenerationFlattenMappingToolbarProxyObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVGenerationFlattenMappingToolbarProxyObject, 1095561546);
	template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<UUVGenerationFlattenMappingToolbarProxyObject>()
	{
		return UUVGenerationFlattenMappingToolbarProxyObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVGenerationFlattenMappingToolbarProxyObject(Z_Construct_UClass_UUVGenerationFlattenMappingToolbarProxyObject, &UUVGenerationFlattenMappingToolbarProxyObject::StaticClass, TEXT("/Script/StaticMeshEditorExtension"), TEXT("UUVGenerationFlattenMappingToolbarProxyObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVGenerationFlattenMappingToolbarProxyObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
