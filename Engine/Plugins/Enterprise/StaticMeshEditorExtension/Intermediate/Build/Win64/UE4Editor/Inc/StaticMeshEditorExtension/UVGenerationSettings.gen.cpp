// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StaticMeshEditorExtension/Private/UVTools/UVGenerationSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUVGenerationSettings() {}
// Cross Module References
	STATICMESHEDITOREXTENSION_API UEnum* Z_Construct_UEnum_StaticMeshEditorExtension_EGenerateUVProjectionType();
	UPackage* Z_Construct_UPackage__Script_StaticMeshEditorExtension();
	STATICMESHEDITOREXTENSION_API UScriptStruct* Z_Construct_UScriptStruct_FUVGenerationSettings();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	static UEnum* EGenerateUVProjectionType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_StaticMeshEditorExtension_EGenerateUVProjectionType, Z_Construct_UPackage__Script_StaticMeshEditorExtension(), TEXT("EGenerateUVProjectionType"));
		}
		return Singleton;
	}
	template<> STATICMESHEDITOREXTENSION_API UEnum* StaticEnum<EGenerateUVProjectionType>()
	{
		return EGenerateUVProjectionType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGenerateUVProjectionType(EGenerateUVProjectionType_StaticEnum, TEXT("/Script/StaticMeshEditorExtension"), TEXT("EGenerateUVProjectionType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_StaticMeshEditorExtension_EGenerateUVProjectionType_Hash() { return 2890133634U; }
	UEnum* Z_Construct_UEnum_StaticMeshEditorExtension_EGenerateUVProjectionType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_StaticMeshEditorExtension();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGenerateUVProjectionType"), 0, Get_Z_Construct_UEnum_StaticMeshEditorExtension_EGenerateUVProjectionType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGenerateUVProjectionType::Box", (int64)EGenerateUVProjectionType::Box },
				{ "EGenerateUVProjectionType::Cylindrical", (int64)EGenerateUVProjectionType::Cylindrical },
				{ "EGenerateUVProjectionType::Planar", (int64)EGenerateUVProjectionType::Planar },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Box.Name", "EGenerateUVProjectionType::Box" },
				{ "Cylindrical.Name", "EGenerateUVProjectionType::Cylindrical" },
				{ "ModuleRelativePath", "Private/UVTools/UVGenerationSettings.h" },
				{ "Planar.Name", "EGenerateUVProjectionType::Planar" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
				nullptr,
				"EGenerateUVProjectionType",
				"EGenerateUVProjectionType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FUVGenerationSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STATICMESHEDITOREXTENSION_API uint32 Get_Z_Construct_UScriptStruct_FUVGenerationSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUVGenerationSettings, Z_Construct_UPackage__Script_StaticMeshEditorExtension(), TEXT("UVGenerationSettings"), sizeof(FUVGenerationSettings), Get_Z_Construct_UScriptStruct_FUVGenerationSettings_Hash());
	}
	return Singleton;
}
template<> STATICMESHEDITOREXTENSION_API UScriptStruct* StaticStruct<FUVGenerationSettings>()
{
	return FUVGenerationSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUVGenerationSettings(FUVGenerationSettings::StaticStruct, TEXT("/Script/StaticMeshEditorExtension"), TEXT("UVGenerationSettings"), false, nullptr, nullptr);
static struct FScriptStruct_StaticMeshEditorExtension_StaticRegisterNativesFUVGenerationSettings
{
	FScriptStruct_StaticMeshEditorExtension_StaticRegisterNativesFUVGenerationSettings()
	{
		UScriptStruct::DeferCppStructOps<FUVGenerationSettings>(FName(TEXT("UVGenerationSettings")));
	}
} ScriptStruct_StaticMeshEditorExtension_StaticRegisterNativesFUVGenerationSettings;
	struct Z_Construct_UScriptStruct_FUVGenerationSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ProjectionType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ProjectionType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Size;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVTilingScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UVTilingScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UVOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TargetChannel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUVGenerationSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_ProjectionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_ProjectionType_MetaData[] = {
		{ "Category", "Projection Settings" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_ProjectionType = { "ProjectionType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUVGenerationSettings, ProjectionType), Z_Construct_UEnum_StaticMeshEditorExtension_EGenerateUVProjectionType, METADATA_PARAMS(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_ProjectionType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_ProjectionType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Position_MetaData[] = {
		{ "Category", "Projection Settings" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUVGenerationSettings, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Rotation_MetaData[] = {
		{ "Category", "Projection Settings" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUVGenerationSettings, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Size_MetaData[] = {
		{ "Category", "Projection Settings" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUVGenerationSettings, Size), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Size_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_UVTilingScale_MetaData[] = {
		{ "Category", "Projection Settings" },
		{ "DisplayName", "UV Tiling Scale" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_UVTilingScale = { "UVTilingScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUVGenerationSettings, UVTilingScale), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_UVTilingScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_UVTilingScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_UVOffset_MetaData[] = {
		{ "Category", "Projection Settings" },
		{ "DisplayName", "UV Offset" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_UVOffset = { "UVOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUVGenerationSettings, UVOffset), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_UVOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_UVOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_TargetChannel_MetaData[] = {
		{ "Category", "Projection Settings" },
		{ "ClampMax", "7" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Private/UVTools/UVGenerationSettings.h" },
		{ "Tooltip", "The UV channel the projection will be applied to." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_TargetChannel = { "TargetChannel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUVGenerationSettings, TargetChannel), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_TargetChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_TargetChannel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_ProjectionType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_ProjectionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_Size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_UVTilingScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_UVOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::NewProp_TargetChannel,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
		nullptr,
		&NewStructOps,
		"UVGenerationSettings",
		sizeof(FUVGenerationSettings),
		alignof(FUVGenerationSettings),
		Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUVGenerationSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUVGenerationSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StaticMeshEditorExtension();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UVGenerationSettings"), sizeof(FUVGenerationSettings), Get_Z_Construct_UScriptStruct_FUVGenerationSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUVGenerationSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUVGenerationSettings_Hash() { return 2680664710U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
