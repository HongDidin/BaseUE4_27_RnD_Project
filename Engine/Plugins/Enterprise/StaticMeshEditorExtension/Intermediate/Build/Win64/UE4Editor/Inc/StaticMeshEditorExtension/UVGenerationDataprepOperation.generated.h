// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STATICMESHEDITOREXTENSION_UVGenerationDataprepOperation_generated_h
#error "UVGenerationDataprepOperation.generated.h already included, missing '#pragma once' in UVGenerationDataprepOperation.h"
#endif
#define STATICMESHEDITOREXTENSION_UVGenerationDataprepOperation_generated_h

#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_SPARSE_DATA
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUVGenerationFlattenMappingOperation(); \
	friend struct Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics; \
public: \
	DECLARE_CLASS(UUVGenerationFlattenMappingOperation, UDataprepOperation, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StaticMeshEditorExtension"), NO_API) \
	DECLARE_SERIALIZER(UUVGenerationFlattenMappingOperation)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUUVGenerationFlattenMappingOperation(); \
	friend struct Z_Construct_UClass_UUVGenerationFlattenMappingOperation_Statics; \
public: \
	DECLARE_CLASS(UUVGenerationFlattenMappingOperation, UDataprepOperation, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StaticMeshEditorExtension"), NO_API) \
	DECLARE_SERIALIZER(UUVGenerationFlattenMappingOperation)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUVGenerationFlattenMappingOperation(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUVGenerationFlattenMappingOperation) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUVGenerationFlattenMappingOperation); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUVGenerationFlattenMappingOperation); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUVGenerationFlattenMappingOperation(UUVGenerationFlattenMappingOperation&&); \
	NO_API UUVGenerationFlattenMappingOperation(const UUVGenerationFlattenMappingOperation&); \
public:


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUVGenerationFlattenMappingOperation(UUVGenerationFlattenMappingOperation&&); \
	NO_API UUVGenerationFlattenMappingOperation(const UUVGenerationFlattenMappingOperation&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUVGenerationFlattenMappingOperation); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUVGenerationFlattenMappingOperation); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UUVGenerationFlattenMappingOperation)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_17_PROLOG
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_SPARSE_DATA \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_INCLASS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_SPARSE_DATA \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<class UUVGenerationFlattenMappingOperation>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationDataprepOperation_h


#define FOREACH_ENUM_EUNWRAPPEDUVDATASMITHOPERATIONCHANNELSELECTION(op) \
	op(EUnwrappedUVDatasmithOperationChannelSelection::FirstEmptyChannel) \
	op(EUnwrappedUVDatasmithOperationChannelSelection::SpecifyChannel) 

enum class EUnwrappedUVDatasmithOperationChannelSelection : uint8;
template<> STATICMESHEDITOREXTENSION_API UEnum* StaticEnum<EUnwrappedUVDatasmithOperationChannelSelection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
