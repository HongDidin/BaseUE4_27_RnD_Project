// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UStaticMesh;
#ifdef STATICMESHEDITOREXTENSION_UVGenerationFlattenMapping_generated_h
#error "UVGenerationFlattenMapping.generated.h already included, missing '#pragma once' in UVGenerationFlattenMapping.h"
#endif
#define STATICMESHEDITOREXTENSION_UVGenerationFlattenMapping_generated_h

#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_SPARSE_DATA
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGenerateFlattenMappingUVs);


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGenerateFlattenMappingUVs);


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUVGenerationFlattenMapping(); \
	friend struct Z_Construct_UClass_UUVGenerationFlattenMapping_Statics; \
public: \
	DECLARE_CLASS(UUVGenerationFlattenMapping, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StaticMeshEditorExtension"), NO_API) \
	DECLARE_SERIALIZER(UUVGenerationFlattenMapping)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUUVGenerationFlattenMapping(); \
	friend struct Z_Construct_UClass_UUVGenerationFlattenMapping_Statics; \
public: \
	DECLARE_CLASS(UUVGenerationFlattenMapping, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StaticMeshEditorExtension"), NO_API) \
	DECLARE_SERIALIZER(UUVGenerationFlattenMapping)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUVGenerationFlattenMapping(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUVGenerationFlattenMapping) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUVGenerationFlattenMapping); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUVGenerationFlattenMapping); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUVGenerationFlattenMapping(UUVGenerationFlattenMapping&&); \
	NO_API UUVGenerationFlattenMapping(const UUVGenerationFlattenMapping&); \
public:


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUVGenerationFlattenMapping(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUVGenerationFlattenMapping(UUVGenerationFlattenMapping&&); \
	NO_API UUVGenerationFlattenMapping(const UUVGenerationFlattenMapping&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUVGenerationFlattenMapping); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUVGenerationFlattenMapping); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUVGenerationFlattenMapping)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_13_PROLOG
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_SPARSE_DATA \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_INCLASS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_SPARSE_DATA \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<class UUVGenerationFlattenMapping>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Public_UVTools_UVGenerationFlattenMapping_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
