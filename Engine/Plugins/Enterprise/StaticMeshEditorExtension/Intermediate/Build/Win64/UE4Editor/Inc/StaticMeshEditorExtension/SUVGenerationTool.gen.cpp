// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StaticMeshEditorExtension/Private/UVTools/SUVGenerationTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSUVGenerationTool() {}
// Cross Module References
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UGenerateUVSettingsUIHolder_NoRegister();
	STATICMESHEDITOREXTENSION_API UClass* Z_Construct_UClass_UGenerateUVSettingsUIHolder();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_StaticMeshEditorExtension();
	STATICMESHEDITOREXTENSION_API UScriptStruct* Z_Construct_UScriptStruct_FUVGenerationSettings();
// End Cross Module References
	void UGenerateUVSettingsUIHolder::StaticRegisterNativesUGenerateUVSettingsUIHolder()
	{
	}
	UClass* Z_Construct_UClass_UGenerateUVSettingsUIHolder_NoRegister()
	{
		return UGenerateUVSettingsUIHolder::StaticClass();
	}
	struct Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenerateUVSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GenerateUVSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_StaticMeshEditorExtension,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Dummy object class needed to use the FUVGenerationSettings custom UI */" },
		{ "IncludePath", "UVTools/SUVGenerationTool.h" },
		{ "ModuleRelativePath", "Private/UVTools/SUVGenerationTool.h" },
		{ "ToolTip", "Dummy object class needed to use the FUVGenerationSettings custom UI" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::NewProp_GenerateUVSettings_MetaData[] = {
		{ "Category", "Projection Settings" },
		{ "ModuleRelativePath", "Private/UVTools/SUVGenerationTool.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::NewProp_GenerateUVSettings = { "GenerateUVSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGenerateUVSettingsUIHolder, GenerateUVSettings), Z_Construct_UScriptStruct_FUVGenerationSettings, METADATA_PARAMS(Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::NewProp_GenerateUVSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::NewProp_GenerateUVSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::NewProp_GenerateUVSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGenerateUVSettingsUIHolder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::ClassParams = {
		&UGenerateUVSettingsUIHolder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGenerateUVSettingsUIHolder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGenerateUVSettingsUIHolder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGenerateUVSettingsUIHolder, 561455215);
	template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<UGenerateUVSettingsUIHolder>()
	{
		return UGenerateUVSettingsUIHolder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGenerateUVSettingsUIHolder(Z_Construct_UClass_UGenerateUVSettingsUIHolder, &UGenerateUVSettingsUIHolder::StaticClass, TEXT("/Script/StaticMeshEditorExtension"), TEXT("UGenerateUVSettingsUIHolder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGenerateUVSettingsUIHolder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
