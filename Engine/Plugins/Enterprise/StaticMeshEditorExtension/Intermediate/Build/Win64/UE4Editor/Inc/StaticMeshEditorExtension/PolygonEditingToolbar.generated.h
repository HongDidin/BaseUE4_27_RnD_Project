// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STATICMESHEDITOREXTENSION_PolygonEditingToolbar_generated_h
#error "PolygonEditingToolbar.generated.h already included, missing '#pragma once' in PolygonEditingToolbar.h"
#endif
#define STATICMESHEDITOREXTENSION_PolygonEditingToolbar_generated_h

#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_SPARSE_DATA
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPolygonToolbarProxyObject(); \
	friend struct Z_Construct_UClass_UPolygonToolbarProxyObject_Statics; \
public: \
	DECLARE_CLASS(UPolygonToolbarProxyObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StaticMeshEditorExtension"), NO_API) \
	DECLARE_SERIALIZER(UPolygonToolbarProxyObject)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_INCLASS \
private: \
	static void StaticRegisterNativesUPolygonToolbarProxyObject(); \
	friend struct Z_Construct_UClass_UPolygonToolbarProxyObject_Statics; \
public: \
	DECLARE_CLASS(UPolygonToolbarProxyObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StaticMeshEditorExtension"), NO_API) \
	DECLARE_SERIALIZER(UPolygonToolbarProxyObject)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonToolbarProxyObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonToolbarProxyObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonToolbarProxyObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonToolbarProxyObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonToolbarProxyObject(UPolygonToolbarProxyObject&&); \
	NO_API UPolygonToolbarProxyObject(const UPolygonToolbarProxyObject&); \
public:


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonToolbarProxyObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonToolbarProxyObject(UPolygonToolbarProxyObject&&); \
	NO_API UPolygonToolbarProxyObject(const UPolygonToolbarProxyObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonToolbarProxyObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonToolbarProxyObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonToolbarProxyObject)


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_69_PROLOG
#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_SPARSE_DATA \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_INCLASS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_SPARSE_DATA \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h_72_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STATICMESHEDITOREXTENSION_API UClass* StaticClass<class UPolygonToolbarProxyObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_PolygonEditingToolbar_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
