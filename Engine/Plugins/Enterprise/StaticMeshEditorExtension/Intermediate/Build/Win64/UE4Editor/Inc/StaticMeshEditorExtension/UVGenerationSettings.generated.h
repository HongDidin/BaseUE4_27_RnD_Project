// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STATICMESHEDITOREXTENSION_UVGenerationSettings_generated_h
#error "UVGenerationSettings.generated.h already included, missing '#pragma once' in UVGenerationSettings.h"
#endif
#define STATICMESHEDITOREXTENSION_UVGenerationSettings_generated_h

#define Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationSettings_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FUVGenerationSettings_Statics; \
	STATICMESHEDITOREXTENSION_API static class UScriptStruct* StaticStruct();


template<> STATICMESHEDITOREXTENSION_API UScriptStruct* StaticStruct<struct FUVGenerationSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_StaticMeshEditorExtension_Source_StaticMeshEditorExtension_Private_UVTools_UVGenerationSettings_h


#define FOREACH_ENUM_EGENERATEUVPROJECTIONTYPE(op) \
	op(EGenerateUVProjectionType::Box) \
	op(EGenerateUVProjectionType::Cylindrical) \
	op(EGenerateUVProjectionType::Planar) 

enum class EGenerateUVProjectionType : uint8;
template<> STATICMESHEDITOREXTENSION_API UEnum* StaticEnum<EGenerateUVProjectionType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
