// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
#ifdef DATAPREPCORE_DataprepActionAsset_generated_h
#error "DataprepActionAsset.generated.h already included, missing '#pragma once' in DataprepActionAsset.h"
#endif
#define DATAPREPCORE_DataprepActionAsset_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepActionStep(); \
	friend struct Z_Construct_UClass_UDataprepActionStep_Statics; \
public: \
	DECLARE_CLASS(UDataprepActionStep, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepActionStep)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepActionStep(); \
	friend struct Z_Construct_UClass_UDataprepActionStep_Statics; \
public: \
	DECLARE_CLASS(UDataprepActionStep, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepActionStep)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepActionStep(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepActionStep) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepActionStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepActionStep); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepActionStep(UDataprepActionStep&&); \
	NO_API UDataprepActionStep(const UDataprepActionStep&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepActionStep(UDataprepActionStep&&); \
	NO_API UDataprepActionStep(const UDataprepActionStep&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepActionStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepActionStep); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepActionStep)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StepObject() { return STRUCT_OFFSET(UDataprepActionStep, StepObject); } \
	FORCEINLINE static uint32 __PPO__PathOfStepObjectClass() { return STRUCT_OFFSET(UDataprepActionStep, PathOfStepObjectClass); } \
	FORCEINLINE static uint32 __PPO__Operation_DEPRECATED() { return STRUCT_OFFSET(UDataprepActionStep, Operation_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__Filter_DEPRECATED() { return STRUCT_OFFSET(UDataprepActionStep, Filter_DEPRECATED); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_50_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_53_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepActionStep>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepActionAppearance(); \
	friend struct Z_Construct_UClass_UDataprepActionAppearance_Statics; \
public: \
	DECLARE_CLASS(UDataprepActionAppearance, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepActionAppearance)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepActionAppearance(); \
	friend struct Z_Construct_UClass_UDataprepActionAppearance_Statics; \
public: \
	DECLARE_CLASS(UDataprepActionAppearance, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepActionAppearance)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepActionAppearance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepActionAppearance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepActionAppearance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepActionAppearance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepActionAppearance(UDataprepActionAppearance&&); \
	NO_API UDataprepActionAppearance(const UDataprepActionAppearance&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepActionAppearance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepActionAppearance(UDataprepActionAppearance&&); \
	NO_API UDataprepActionAppearance(const UDataprepActionAppearance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepActionAppearance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepActionAppearance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepActionAppearance)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_177_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_180_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepActionAppearance>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execExecute);


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execExecute);


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepActionAsset(); \
	friend struct Z_Construct_UClass_UDataprepActionAsset_Statics; \
public: \
	DECLARE_CLASS(UDataprepActionAsset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepActionAsset)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepActionAsset(); \
	friend struct Z_Construct_UClass_UDataprepActionAsset_Statics; \
public: \
	DECLARE_CLASS(UDataprepActionAsset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepActionAsset)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepActionAsset(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepActionAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepActionAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepActionAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepActionAsset(UDataprepActionAsset&&); \
	NO_API UDataprepActionAsset(const UDataprepActionAsset&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepActionAsset(UDataprepActionAsset&&); \
	NO_API UDataprepActionAsset(const UDataprepActionAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepActionAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepActionAsset); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepActionAsset)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Steps() { return STRUCT_OFFSET(UDataprepActionAsset, Steps); } \
	FORCEINLINE static uint32 __PPO__Appearance() { return STRUCT_OFFSET(UDataprepActionAsset, Appearance); } \
	FORCEINLINE static uint32 __PPO__Label() { return STRUCT_OFFSET(UDataprepActionAsset, Label); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_201_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h_204_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepActionAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepActionAsset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
