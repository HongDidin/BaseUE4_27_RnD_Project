// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPEDITOR_DataprepEditorMenu_generated_h
#error "DataprepEditorMenu.generated.h already included, missing '#pragma once' in DataprepEditorMenu.h"
#endif
#define DATAPREPEDITOR_DataprepEditorMenu_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepEditorContextMenuContext(); \
	friend struct Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics; \
public: \
	DECLARE_CLASS(UDataprepEditorContextMenuContext, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepEditor"), NO_API) \
	DECLARE_SERIALIZER(UDataprepEditorContextMenuContext)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepEditorContextMenuContext(); \
	friend struct Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics; \
public: \
	DECLARE_CLASS(UDataprepEditorContextMenuContext, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepEditor"), NO_API) \
	DECLARE_SERIALIZER(UDataprepEditorContextMenuContext)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepEditorContextMenuContext(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepEditorContextMenuContext) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepEditorContextMenuContext); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepEditorContextMenuContext); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepEditorContextMenuContext(UDataprepEditorContextMenuContext&&); \
	NO_API UDataprepEditorContextMenuContext(const UDataprepEditorContextMenuContext&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepEditorContextMenuContext(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepEditorContextMenuContext(UDataprepEditorContextMenuContext&&); \
	NO_API UDataprepEditorContextMenuContext(const UDataprepEditorContextMenuContext&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepEditorContextMenuContext); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepEditorContextMenuContext); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepEditorContextMenuContext)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_9_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPEDITOR_API UClass* StaticClass<class UDataprepEditorContextMenuContext>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Public_DataprepEditorMenu_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
