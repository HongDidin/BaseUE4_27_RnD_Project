// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepEditor/Private/Widgets/SDataprepEditorViewport.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSDataprepEditorViewport() {}
// Cross Module References
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UCustomStaticMeshComponent_NoRegister();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UCustomStaticMeshComponent();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent();
	UPackage* Z_Construct_UPackage__Script_DataprepEditor();
// End Cross Module References
	void UCustomStaticMeshComponent::StaticRegisterNativesUCustomStaticMeshComponent()
	{
	}
	UClass* Z_Construct_UClass_UCustomStaticMeshComponent_NoRegister()
	{
		return UCustomStaticMeshComponent::StaticClass();
	}
	struct Z_Construct_UClass_UCustomStaticMeshComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCustomStaticMeshComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UStaticMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCustomStaticMeshComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Custom class deriving from UStaticMeshComponent to allow the display of individual meshes in wireframe\n * @note: This technique was inspired from USkinnedMeshComponent\n */" },
		{ "HideCategories", "Object Activation Components|Activation Trigger" },
		{ "IncludePath", "Widgets/SDataprepEditorViewport.h" },
		{ "ModuleRelativePath", "Private/Widgets/SDataprepEditorViewport.h" },
		{ "ToolTip", "Custom class deriving from UStaticMeshComponent to allow the display of individual meshes in wireframe\n@note: This technique was inspired from USkinnedMeshComponent" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCustomStaticMeshComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCustomStaticMeshComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCustomStaticMeshComponent_Statics::ClassParams = {
		&UCustomStaticMeshComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UCustomStaticMeshComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCustomStaticMeshComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCustomStaticMeshComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCustomStaticMeshComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCustomStaticMeshComponent, 366820356);
	template<> DATAPREPEDITOR_API UClass* StaticClass<UCustomStaticMeshComponent>()
	{
		return UCustomStaticMeshComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCustomStaticMeshComponent(Z_Construct_UClass_UCustomStaticMeshComponent, &UCustomStaticMeshComponent::StaticClass, TEXT("/Script/DataprepEditor"), TEXT("UCustomStaticMeshComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCustomStaticMeshComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
