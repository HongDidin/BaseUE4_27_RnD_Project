// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepFloatFilter_generated_h
#error "DataprepFloatFilter.generated.h already included, missing '#pragma once' in DataprepFloatFilter.h"
#endif
#define DATAPREPCORE_DataprepFloatFilter_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepFloatFilter(); \
	friend struct Z_Construct_UClass_UDataprepFloatFilter_Statics; \
public: \
	DECLARE_CLASS(UDataprepFloatFilter, UDataprepFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepFloatFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepFloatFilter(); \
	friend struct Z_Construct_UClass_UDataprepFloatFilter_Statics; \
public: \
	DECLARE_CLASS(UDataprepFloatFilter, UDataprepFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepFloatFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepFloatFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepFloatFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepFloatFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepFloatFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepFloatFilter(UDataprepFloatFilter&&); \
	NO_API UDataprepFloatFilter(const UDataprepFloatFilter&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepFloatFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepFloatFilter(UDataprepFloatFilter&&); \
	NO_API UDataprepFloatFilter(const UDataprepFloatFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepFloatFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepFloatFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepFloatFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FloatFetcher() { return STRUCT_OFFSET(UDataprepFloatFilter, FloatFetcher); } \
	FORCEINLINE static uint32 __PPO__FloatMatchingCriteria() { return STRUCT_OFFSET(UDataprepFloatFilter, FloatMatchingCriteria); } \
	FORCEINLINE static uint32 __PPO__EqualValue() { return STRUCT_OFFSET(UDataprepFloatFilter, EqualValue); } \
	FORCEINLINE static uint32 __PPO__Tolerance() { return STRUCT_OFFSET(UDataprepFloatFilter, Tolerance); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_23_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepFloatFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepFloatFilter_h


#define FOREACH_ENUM_EDATAPREPFLOATMATCHTYPE(op) \
	op(EDataprepFloatMatchType::LessThan) \
	op(EDataprepFloatMatchType::GreatherThan) \
	op(EDataprepFloatMatchType::IsNearlyEqual) 

enum class EDataprepFloatMatchType : uint8;
template<> DATAPREPCORE_API UEnum* StaticEnum<EDataprepFloatMatchType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
