// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPLIBRARIES_DataprepIntegerFetcherLibrary_generated_h
#error "DataprepIntegerFetcherLibrary.generated.h already included, missing '#pragma once' in DataprepIntegerFetcherLibrary.h"
#endif
#define DATAPREPLIBRARIES_DataprepIntegerFetcherLibrary_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepTriangleCountFetcher(); \
	friend struct Z_Construct_UClass_UDataprepTriangleCountFetcher_Statics; \
public: \
	DECLARE_CLASS(UDataprepTriangleCountFetcher, UDataprepIntegerFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepTriangleCountFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepTriangleCountFetcher(); \
	friend struct Z_Construct_UClass_UDataprepTriangleCountFetcher_Statics; \
public: \
	DECLARE_CLASS(UDataprepTriangleCountFetcher, UDataprepIntegerFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepTriangleCountFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepTriangleCountFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepTriangleCountFetcher) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepTriangleCountFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepTriangleCountFetcher); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepTriangleCountFetcher(UDataprepTriangleCountFetcher&&); \
	NO_API UDataprepTriangleCountFetcher(const UDataprepTriangleCountFetcher&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepTriangleCountFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepTriangleCountFetcher(UDataprepTriangleCountFetcher&&); \
	NO_API UDataprepTriangleCountFetcher(const UDataprepTriangleCountFetcher&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepTriangleCountFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepTriangleCountFetcher); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepTriangleCountFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_11_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPLIBRARIES_API UClass* StaticClass<class UDataprepTriangleCountFetcher>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepVertexCountFetcher(); \
	friend struct Z_Construct_UClass_UDataprepVertexCountFetcher_Statics; \
public: \
	DECLARE_CLASS(UDataprepVertexCountFetcher, UDataprepIntegerFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepVertexCountFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepVertexCountFetcher(); \
	friend struct Z_Construct_UClass_UDataprepVertexCountFetcher_Statics; \
public: \
	DECLARE_CLASS(UDataprepVertexCountFetcher, UDataprepIntegerFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepVertexCountFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepVertexCountFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepVertexCountFetcher) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepVertexCountFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepVertexCountFetcher); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepVertexCountFetcher(UDataprepVertexCountFetcher&&); \
	NO_API UDataprepVertexCountFetcher(const UDataprepVertexCountFetcher&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepVertexCountFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepVertexCountFetcher(UDataprepVertexCountFetcher&&); \
	NO_API UDataprepVertexCountFetcher(const UDataprepVertexCountFetcher&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepVertexCountFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepVertexCountFetcher); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepVertexCountFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_26_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPLIBRARIES_API UClass* StaticClass<class UDataprepVertexCountFetcher>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepIntegerFetcherLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
