// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/SelectionSystem/DataprepFloatFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepFloatFilter() {}
// Cross Module References
	DATAPREPCORE_API UEnum* Z_Construct_UEnum_DataprepCore_EDataprepFloatMatchType();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepFloatFilter_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepFloatFilter();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepFilter();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepFloatFetcher_NoRegister();
// End Cross Module References
	static UEnum* EDataprepFloatMatchType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DataprepCore_EDataprepFloatMatchType, Z_Construct_UPackage__Script_DataprepCore(), TEXT("EDataprepFloatMatchType"));
		}
		return Singleton;
	}
	template<> DATAPREPCORE_API UEnum* StaticEnum<EDataprepFloatMatchType>()
	{
		return EDataprepFloatMatchType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDataprepFloatMatchType(EDataprepFloatMatchType_StaticEnum, TEXT("/Script/DataprepCore"), TEXT("EDataprepFloatMatchType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DataprepCore_EDataprepFloatMatchType_Hash() { return 2081513766U; }
	UEnum* Z_Construct_UEnum_DataprepCore_EDataprepFloatMatchType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDataprepFloatMatchType"), 0, Get_Z_Construct_UEnum_DataprepCore_EDataprepFloatMatchType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDataprepFloatMatchType::LessThan", (int64)EDataprepFloatMatchType::LessThan },
				{ "EDataprepFloatMatchType::GreatherThan", (int64)EDataprepFloatMatchType::GreatherThan },
				{ "EDataprepFloatMatchType::IsNearlyEqual", (int64)EDataprepFloatMatchType::IsNearlyEqual },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "GreatherThan.Name", "EDataprepFloatMatchType::GreatherThan" },
				{ "IsNearlyEqual.Name", "EDataprepFloatMatchType::IsNearlyEqual" },
				{ "LessThan.Name", "EDataprepFloatMatchType::LessThan" },
				{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFloatFilter.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DataprepCore,
				nullptr,
				"EDataprepFloatMatchType",
				"EDataprepFloatMatchType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDataprepFloatFilter::StaticRegisterNativesUDataprepFloatFilter()
	{
	}
	UClass* Z_Construct_UClass_UDataprepFloatFilter_NoRegister()
	{
		return UDataprepFloatFilter::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepFloatFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatFetcher_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FloatFetcher;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FloatMatchingCriteria_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatMatchingCriteria_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FloatMatchingCriteria;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EqualValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EqualValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Tolerance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepFloatFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepFloatFilter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SelectionSystem/DataprepFloatFilter.h" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFloatFilter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatFetcher_MetaData[] = {
		{ "Comment", "// The source of float selected by the user\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFloatFilter.h" },
		{ "ToolTip", "The source of float selected by the user" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatFetcher = { "FloatFetcher", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepFloatFilter, FloatFetcher), Z_Construct_UClass_UDataprepFloatFetcher_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatFetcher_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatFetcher_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatMatchingCriteria_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatMatchingCriteria_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The matching criteria used when checking if a fetched value can pass the filter\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFloatFilter.h" },
		{ "ToolTip", "The matching criteria used when checking if a fetched value can pass the filter" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatMatchingCriteria = { "FloatMatchingCriteria", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepFloatFilter, FloatMatchingCriteria), Z_Construct_UEnum_DataprepCore_EDataprepFloatMatchType, METADATA_PARAMS(Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatMatchingCriteria_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatMatchingCriteria_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_EqualValue_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The value to use for the comparison against the fetched value\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFloatFilter.h" },
		{ "ToolTip", "The value to use for the comparison against the fetched value" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_EqualValue = { "EqualValue", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepFloatFilter, EqualValue), METADATA_PARAMS(Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_EqualValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_EqualValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_Tolerance_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The value used for the tolerance when doing a nearly equal\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFloatFilter.h" },
		{ "ToolTip", "The value used for the tolerance when doing a nearly equal" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_Tolerance = { "Tolerance", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepFloatFilter, Tolerance), METADATA_PARAMS(Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_Tolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_Tolerance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepFloatFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatFetcher,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatMatchingCriteria_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_FloatMatchingCriteria,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_EqualValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepFloatFilter_Statics::NewProp_Tolerance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepFloatFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepFloatFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepFloatFilter_Statics::ClassParams = {
		&UDataprepFloatFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepFloatFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepFloatFilter_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepFloatFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepFloatFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepFloatFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepFloatFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepFloatFilter, 3115089317);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepFloatFilter>()
	{
		return UDataprepFloatFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepFloatFilter(Z_Construct_UClass_UDataprepFloatFilter, &UDataprepFloatFilter::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepFloatFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepFloatFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
