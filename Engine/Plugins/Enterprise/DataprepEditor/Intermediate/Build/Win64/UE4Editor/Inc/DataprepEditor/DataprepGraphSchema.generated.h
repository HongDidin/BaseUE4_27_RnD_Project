// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPEDITOR_DataprepGraphSchema_generated_h
#error "DataprepGraphSchema.generated.h already included, missing '#pragma once' in DataprepGraphSchema.h"
#endif
#define DATAPREPEDITOR_DataprepGraphSchema_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepGraphSchema(); \
	friend struct Z_Construct_UClass_UDataprepGraphSchema_Statics; \
public: \
	DECLARE_CLASS(UDataprepGraphSchema, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepEditor"), NO_API) \
	DECLARE_SERIALIZER(UDataprepGraphSchema)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepGraphSchema(); \
	friend struct Z_Construct_UClass_UDataprepGraphSchema_Statics; \
public: \
	DECLARE_CLASS(UDataprepGraphSchema, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepEditor"), NO_API) \
	DECLARE_SERIALIZER(UDataprepGraphSchema)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepGraphSchema(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepGraphSchema) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepGraphSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepGraphSchema); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepGraphSchema(UDataprepGraphSchema&&); \
	NO_API UDataprepGraphSchema(const UDataprepGraphSchema&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepGraphSchema(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepGraphSchema(UDataprepGraphSchema&&); \
	NO_API UDataprepGraphSchema(const UDataprepGraphSchema&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepGraphSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepGraphSchema); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepGraphSchema)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_12_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPEDITOR_API UClass* StaticClass<class UDataprepGraphSchema>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraphSchema_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
