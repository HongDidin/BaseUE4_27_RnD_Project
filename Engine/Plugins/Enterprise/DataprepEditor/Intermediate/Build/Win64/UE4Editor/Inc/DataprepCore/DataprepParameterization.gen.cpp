// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Private/Parameterization/DataprepParameterization.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepParameterization() {}
// Cross Module References
	DATAPREPCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDataprepParameterizationBinding();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizableObject_NoRegister();
	DATAPREPCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDataprepPropertyLink();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizationBindings_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizationBindings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterization_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterization();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizationInstance_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizationInstance();
// End Cross Module References
class UScriptStruct* FDataprepParameterizationBinding::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAPREPCORE_API uint32 Get_Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataprepParameterizationBinding, Z_Construct_UPackage__Script_DataprepCore(), TEXT("DataprepParameterizationBinding"), sizeof(FDataprepParameterizationBinding), Get_Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Hash());
	}
	return Singleton;
}
template<> DATAPREPCORE_API UScriptStruct* StaticStruct<FDataprepParameterizationBinding>()
{
	return FDataprepParameterizationBinding::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataprepParameterizationBinding(FDataprepParameterizationBinding::StaticStruct, TEXT("/Script/DataprepCore"), TEXT("DataprepParameterizationBinding"), false, nullptr, nullptr);
static struct FScriptStruct_DataprepCore_StaticRegisterNativesFDataprepParameterizationBinding
{
	FScriptStruct_DataprepCore_StaticRegisterNativesFDataprepParameterizationBinding()
	{
		UScriptStruct::DeferCppStructOps<FDataprepParameterizationBinding>(FName(TEXT("DataprepParameterizationBinding")));
	}
} ScriptStruct_DataprepCore_StaticRegisterNativesFDataprepParameterizationBinding;
	struct Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectBinded_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectBinded;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PropertyChain_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyChain_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PropertyChain;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * The parameterization binding is a struct that hold an object and the property path to the parameterized property\n * It also hold a array to validate the that value type of the parameterized property didn't change since it's creation\n */" },
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
		{ "ToolTip", "The parameterization binding is a struct that hold an object and the property path to the parameterized property\nIt also hold a array to validate the that value type of the parameterized property didn't change since it's creation" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataprepParameterizationBinding>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_ObjectBinded_MetaData[] = {
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_ObjectBinded = { "ObjectBinded", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataprepParameterizationBinding, ObjectBinded), Z_Construct_UClass_UDataprepParameterizableObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_ObjectBinded_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_ObjectBinded_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_PropertyChain_Inner = { "PropertyChain", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDataprepPropertyLink, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_PropertyChain_MetaData[] = {
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_PropertyChain = { "PropertyChain", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataprepParameterizationBinding, PropertyChain), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_PropertyChain_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_PropertyChain_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_ObjectBinded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_PropertyChain_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::NewProp_PropertyChain,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
		nullptr,
		&NewStructOps,
		"DataprepParameterizationBinding",
		sizeof(FDataprepParameterizationBinding),
		alignof(FDataprepParameterizationBinding),
		Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataprepParameterizationBinding()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataprepParameterizationBinding"), sizeof(FDataprepParameterizationBinding), Get_Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataprepParameterizationBinding_Hash() { return 1790890444U; }
	void UDataprepParameterizationBindings::StaticRegisterNativesUDataprepParameterizationBindings()
	{
	}
	UClass* Z_Construct_UClass_UDataprepParameterizationBindings_NoRegister()
	{
		return UDataprepParameterizationBindings::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepParameterizationBindings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepParameterizationBindings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterizationBindings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Encapsulate the unidirectionality necessary for a constant cost of access to the data related to the bindings\n */" },
		{ "IncludePath", "Parameterization/DataprepParameterization.h" },
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
		{ "ToolTip", "Encapsulate the unidirectionality necessary for a constant cost of access to the data related to the bindings" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepParameterizationBindings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepParameterizationBindings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepParameterizationBindings_Statics::ClassParams = {
		&UDataprepParameterizationBindings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterizationBindings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterizationBindings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepParameterizationBindings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepParameterizationBindings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepParameterizationBindings, 198705949);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepParameterizationBindings>()
	{
		return UDataprepParameterizationBindings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepParameterizationBindings(Z_Construct_UClass_UDataprepParameterizationBindings, &UDataprepParameterizationBindings::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepParameterizationBindings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepParameterizationBindings);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDataprepParameterizationBindings)
	void UDataprepParameterization::StaticRegisterNativesUDataprepParameterization()
	{
	}
	UClass* Z_Construct_UClass_UDataprepParameterization_NoRegister()
	{
		return UDataprepParameterization::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepParameterization_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BindingsContainer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BindingsContainer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomContainerClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CustomContainerClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultParameterisation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultParameterisation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ParameterizationStorage_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterizationStorage_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ParameterizationStorage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepParameterization_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterization_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * The DataprepParameterization contains the data for the parameterization of a pipeline\n */" },
		{ "IncludePath", "Parameterization/DataprepParameterization.h" },
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
		{ "ToolTip", "The DataprepParameterization contains the data for the parameterization of a pipeline" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_BindingsContainer_MetaData[] = {
		{ "Comment", "// The containers for the bindings\n" },
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
		{ "ToolTip", "The containers for the bindings" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_BindingsContainer = { "BindingsContainer", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepParameterization, BindingsContainer), Z_Construct_UClass_UDataprepParameterizationBindings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_BindingsContainer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_BindingsContainer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_CustomContainerClass_MetaData[] = {
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_CustomContainerClass = { "CustomContainerClass", nullptr, (EPropertyFlags)0x0040000400002000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepParameterization, CustomContainerClass), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_CustomContainerClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_CustomContainerClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_DefaultParameterisation_MetaData[] = {
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_DefaultParameterisation = { "DefaultParameterisation", nullptr, (EPropertyFlags)0x0040000400002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepParameterization, DefaultParameterisation), Z_Construct_UClass_UDataprepParameterizableObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_DefaultParameterisation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_DefaultParameterisation_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_ParameterizationStorage_Inner = { "ParameterizationStorage", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_ParameterizationStorage_MetaData[] = {
		{ "Comment", "/** \n\x09 * This is used only to store a serialization of the values of the parameterization since we can't save our custom container class\n\x09 */" },
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
		{ "ToolTip", "This is used only to store a serialization of the values of the parameterization since we can't save our custom container class" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_ParameterizationStorage = { "ParameterizationStorage", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepParameterization, ParameterizationStorage), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_ParameterizationStorage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_ParameterizationStorage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepParameterization_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_BindingsContainer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_CustomContainerClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_DefaultParameterisation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_ParameterizationStorage_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepParameterization_Statics::NewProp_ParameterizationStorage,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepParameterization_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepParameterization>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepParameterization_Statics::ClassParams = {
		&UDataprepParameterization::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepParameterization_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterization_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterization_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterization_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepParameterization()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepParameterization_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepParameterization, 2844768377);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepParameterization>()
	{
		return UDataprepParameterization::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepParameterization(Z_Construct_UClass_UDataprepParameterization, &UDataprepParameterization::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepParameterization"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepParameterization);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDataprepParameterization)
	void UDataprepParameterizationInstance::StaticRegisterNativesUDataprepParameterizationInstance()
	{
	}
	UClass* Z_Construct_UClass_UDataprepParameterizationInstance_NoRegister()
	{
		return UDataprepParameterizationInstance::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepParameterizationInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceParameterization_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceParameterization;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterizationInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParameterizationInstance;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ParameterizationInstanceStorage_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterizationInstanceStorage_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ParameterizationInstanceStorage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepParameterizationInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterizationInstance_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Parameterization/DataprepParameterization.h" },
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_SourceParameterization_MetaData[] = {
		{ "Comment", "// The parameterization from which this instance was constructed\n" },
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
		{ "ToolTip", "The parameterization from which this instance was constructed" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_SourceParameterization = { "SourceParameterization", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepParameterizationInstance, SourceParameterization), Z_Construct_UClass_UDataprepParameterization_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_SourceParameterization_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_SourceParameterization_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstance_MetaData[] = {
		{ "Comment", "// The actual object on which the parameterization data is stored\n" },
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
		{ "ToolTip", "The actual object on which the parameterization data is stored" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstance = { "ParameterizationInstance", nullptr, (EPropertyFlags)0x0040000400002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepParameterizationInstance, ParameterizationInstance), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstance_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstanceStorage_Inner = { "ParameterizationInstanceStorage", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstanceStorage_MetaData[] = {
		{ "Comment", "// This is used only to store a serialization of the values of the parameterization since we can't save the custom class\n" },
		{ "ModuleRelativePath", "Private/Parameterization/DataprepParameterization.h" },
		{ "ToolTip", "This is used only to store a serialization of the values of the parameterization since we can't save the custom class" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstanceStorage = { "ParameterizationInstanceStorage", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepParameterizationInstance, ParameterizationInstanceStorage), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstanceStorage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstanceStorage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepParameterizationInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_SourceParameterization,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstanceStorage_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepParameterizationInstance_Statics::NewProp_ParameterizationInstanceStorage,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepParameterizationInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepParameterizationInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepParameterizationInstance_Statics::ClassParams = {
		&UDataprepParameterizationInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepParameterizationInstance_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterizationInstance_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterizationInstance_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterizationInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepParameterizationInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepParameterizationInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepParameterizationInstance, 3401506472);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepParameterizationInstance>()
	{
		return UDataprepParameterizationInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepParameterizationInstance(Z_Construct_UClass_UDataprepParameterizationInstance, &UDataprepParameterizationInstance::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepParameterizationInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepParameterizationInstance);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDataprepParameterizationInstance)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
