// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepParameterizableObject_generated_h
#error "DataprepParameterizableObject.generated.h already included, missing '#pragma once' in DataprepParameterizableObject.h"
#endif
#define DATAPREPCORE_DataprepParameterizableObject_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepParameterizableObject(); \
	friend struct Z_Construct_UClass_UDataprepParameterizableObject_Statics; \
public: \
	DECLARE_CLASS(UDataprepParameterizableObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepParameterizableObject)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepParameterizableObject(); \
	friend struct Z_Construct_UClass_UDataprepParameterizableObject_Statics; \
public: \
	DECLARE_CLASS(UDataprepParameterizableObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepParameterizableObject)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepParameterizableObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepParameterizableObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepParameterizableObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepParameterizableObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepParameterizableObject(UDataprepParameterizableObject&&); \
	NO_API UDataprepParameterizableObject(const UDataprepParameterizableObject&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepParameterizableObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepParameterizableObject(UDataprepParameterizableObject&&); \
	NO_API UDataprepParameterizableObject(const UDataprepParameterizableObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepParameterizableObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepParameterizableObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepParameterizableObject)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_18_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h_22_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepParameterizableObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepParameterizableObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
