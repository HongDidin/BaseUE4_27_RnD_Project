// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/DataprepAssetUserData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepAssetUserData() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetUserData_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetUserData();
	ENGINE_API UClass* Z_Construct_UClass_UAssetUserData();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepEditingOperation_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInterface_NoRegister();
// End Cross Module References
	void UDataprepAssetUserData::StaticRegisterNativesUDataprepAssetUserData()
	{
	}
	UClass* Z_Construct_UClass_UDataprepAssetUserData_NoRegister()
	{
		return UDataprepAssetUserData::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepAssetUserData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataprepOperationPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DataprepOperationPtr;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataprepAssetPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DataprepAssetPtr;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepAssetUserData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetUserData,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetUserData_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** A DataprepAssetUserData is used to mark assets or actors created through a Dataprep pipeline  */" },
		{ "DisplayName", "Dataprep User Data" },
		{ "IncludePath", "DataprepAssetUserData.h" },
		{ "ModuleRelativePath", "Public/DataprepAssetUserData.h" },
		{ "ScriptName", "DataprepUserData" },
		{ "ToolTip", "A DataprepAssetUserData is used to mark assets or actors created through a Dataprep pipeline" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetUserData_Statics::NewProp_DataprepOperationPtr_MetaData[] = {
		{ "Comment", "/** Dataprep operation which was used to generate the hosting object, if applicable */" },
		{ "ModuleRelativePath", "Public/DataprepAssetUserData.h" },
		{ "ToolTip", "Dataprep operation which was used to generate the hosting object, if applicable" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UDataprepAssetUserData_Statics::NewProp_DataprepOperationPtr = { "DataprepOperationPtr", nullptr, (EPropertyFlags)0x0014000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetUserData, DataprepOperationPtr), Z_Construct_UClass_UDataprepEditingOperation_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetUserData_Statics::NewProp_DataprepOperationPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetUserData_Statics::NewProp_DataprepOperationPtr_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetUserData_Statics::NewProp_DataprepAssetPtr_MetaData[] = {
		{ "Comment", "/** Dataprep asset which was used to generate the hosting object */" },
		{ "ModuleRelativePath", "Public/DataprepAssetUserData.h" },
		{ "ToolTip", "Dataprep asset which was used to generate the hosting object" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UDataprepAssetUserData_Statics::NewProp_DataprepAssetPtr = { "DataprepAssetPtr", nullptr, (EPropertyFlags)0x0014000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetUserData, DataprepAssetPtr), Z_Construct_UClass_UDataprepAssetInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetUserData_Statics::NewProp_DataprepAssetPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetUserData_Statics::NewProp_DataprepAssetPtr_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepAssetUserData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetUserData_Statics::NewProp_DataprepOperationPtr,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetUserData_Statics::NewProp_DataprepAssetPtr,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepAssetUserData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepAssetUserData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepAssetUserData_Statics::ClassParams = {
		&UDataprepAssetUserData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepAssetUserData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetUserData_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetUserData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetUserData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepAssetUserData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepAssetUserData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepAssetUserData, 2883095895);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepAssetUserData>()
	{
		return UDataprepAssetUserData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepAssetUserData(Z_Construct_UClass_UDataprepAssetUserData, &UDataprepAssetUserData::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepAssetUserData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepAssetUserData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
