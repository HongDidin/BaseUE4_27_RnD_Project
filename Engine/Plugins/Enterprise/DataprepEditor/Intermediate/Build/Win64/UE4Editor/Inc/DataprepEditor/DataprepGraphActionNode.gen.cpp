// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepEditor/Private/DataprepGraph/DataprepGraphActionNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepGraphActionNode() {}
// Cross Module References
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepGraphActionStepNode_NoRegister();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepGraphActionStepNode();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphNode();
	UPackage* Z_Construct_UPackage__Script_DataprepEditor();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepActionAsset_NoRegister();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepGraphActionNode_NoRegister();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepGraphActionNode();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAsset_NoRegister();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepGraphActionGroupNode_NoRegister();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepGraphActionGroupNode();
// End Cross Module References
	void UDataprepGraphActionStepNode::StaticRegisterNativesUDataprepGraphActionStepNode()
	{
	}
	UClass* Z_Construct_UClass_UDataprepGraphActionStepNode_NoRegister()
	{
		return UDataprepGraphActionStepNode::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepGraphActionStepNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataprepActionAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DataprepActionAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StepIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StepIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * The UDataprepGraphActionStepNode class is used as the UEdGraphNode associated\n * to an SGraphNode in order to display the action's steps in a SDataprepGraphEditor.\n */" },
		{ "IncludePath", "DataprepGraph/DataprepGraphActionNode.h" },
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
		{ "ToolTip", "The UDataprepGraphActionStepNode class is used as the UEdGraphNode associated\nto an SGraphNode in order to display the action's steps in a SDataprepGraphEditor." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::NewProp_DataprepActionAsset_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::NewProp_DataprepActionAsset = { "DataprepActionAsset", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepGraphActionStepNode, DataprepActionAsset), Z_Construct_UClass_UDataprepActionAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::NewProp_DataprepActionAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::NewProp_DataprepActionAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::NewProp_StepIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::NewProp_StepIndex = { "StepIndex", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepGraphActionStepNode, StepIndex), METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::NewProp_StepIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::NewProp_StepIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::NewProp_DataprepActionAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::NewProp_StepIndex,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepGraphActionStepNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::ClassParams = {
		&UDataprepGraphActionStepNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepGraphActionStepNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepGraphActionStepNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepGraphActionStepNode, 2306054632);
	template<> DATAPREPEDITOR_API UClass* StaticClass<UDataprepGraphActionStepNode>()
	{
		return UDataprepGraphActionStepNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepGraphActionStepNode(Z_Construct_UClass_UDataprepGraphActionStepNode, &UDataprepGraphActionStepNode::StaticClass, TEXT("/Script/DataprepEditor"), TEXT("UDataprepGraphActionStepNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepGraphActionStepNode);
	void UDataprepGraphActionNode::StaticRegisterNativesUDataprepGraphActionNode()
	{
	}
	UClass* Z_Construct_UClass_UDataprepGraphActionNode_NoRegister()
	{
		return UDataprepGraphActionNode::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepGraphActionNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActionTitle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ActionTitle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataprepActionAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DataprepActionAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataprepAssetPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_DataprepAssetPtr;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExecutionOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ExecutionOrder;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepGraphActionNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionNode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * The UDataprepGraphActionNode class is used as the UEdGraphNode associated\n * to an SGraphNode in order to display actions in a SDataprepGraphEditor.\n */" },
		{ "IncludePath", "DataprepGraph/DataprepGraphActionNode.h" },
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
		{ "ToolTip", "The UDataprepGraphActionNode class is used as the UEdGraphNode associated\nto an SGraphNode in order to display actions in a SDataprepGraphEditor." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_ActionTitle_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_ActionTitle = { "ActionTitle", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepGraphActionNode, ActionTitle), METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_ActionTitle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_ActionTitle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_DataprepActionAsset_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_DataprepActionAsset = { "DataprepActionAsset", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepGraphActionNode, DataprepActionAsset), Z_Construct_UClass_UDataprepActionAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_DataprepActionAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_DataprepActionAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_DataprepAssetPtr_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_DataprepAssetPtr = { "DataprepAssetPtr", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepGraphActionNode, DataprepAssetPtr), Z_Construct_UClass_UDataprepAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_DataprepAssetPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_DataprepAssetPtr_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_ExecutionOrder_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_ExecutionOrder = { "ExecutionOrder", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepGraphActionNode, ExecutionOrder), METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_ExecutionOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_ExecutionOrder_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepGraphActionNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_ActionTitle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_DataprepActionAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_DataprepAssetPtr,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionNode_Statics::NewProp_ExecutionOrder,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepGraphActionNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepGraphActionNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepGraphActionNode_Statics::ClassParams = {
		&UDataprepGraphActionNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepGraphActionNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionNode_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepGraphActionNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepGraphActionNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepGraphActionNode, 677577960);
	template<> DATAPREPEDITOR_API UClass* StaticClass<UDataprepGraphActionNode>()
	{
		return UDataprepGraphActionNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepGraphActionNode(Z_Construct_UClass_UDataprepGraphActionNode, &UDataprepGraphActionNode::StaticClass, TEXT("/Script/DataprepEditor"), TEXT("UDataprepGraphActionNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepGraphActionNode);
	void UDataprepGraphActionGroupNode::StaticRegisterNativesUDataprepGraphActionGroupNode()
	{
	}
	UClass* Z_Construct_UClass_UDataprepGraphActionGroupNode_NoRegister()
	{
		return UDataprepGraphActionGroupNode::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExecutionOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ExecutionOrder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeTitle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NodeTitle;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataprepAssetPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_DataprepAssetPtr;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DataprepGraph/DataprepGraphActionNode.h" },
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_ExecutionOrder_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_ExecutionOrder = { "ExecutionOrder", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepGraphActionGroupNode, ExecutionOrder), METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_ExecutionOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_ExecutionOrder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_NodeTitle_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_NodeTitle = { "NodeTitle", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepGraphActionGroupNode, NodeTitle), METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_NodeTitle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_NodeTitle_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_Actions_Inner = { "Actions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDataprepActionAsset_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_Actions_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_Actions = { "Actions", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepGraphActionGroupNode, Actions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_Actions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_Actions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_DataprepAssetPtr_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraphActionNode.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_DataprepAssetPtr = { "DataprepAssetPtr", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepGraphActionGroupNode, DataprepAssetPtr), Z_Construct_UClass_UDataprepAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_DataprepAssetPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_DataprepAssetPtr_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_ExecutionOrder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_NodeTitle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_Actions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_Actions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::NewProp_DataprepAssetPtr,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepGraphActionGroupNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::ClassParams = {
		&UDataprepGraphActionGroupNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepGraphActionGroupNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepGraphActionGroupNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepGraphActionGroupNode, 2676344215);
	template<> DATAPREPEDITOR_API UClass* StaticClass<UDataprepGraphActionGroupNode>()
	{
		return UDataprepGraphActionGroupNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepGraphActionGroupNode(Z_Construct_UClass_UDataprepGraphActionGroupNode, &UDataprepGraphActionGroupNode::StaticClass, TEXT("/Script/DataprepEditor"), TEXT("UDataprepGraphActionGroupNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepGraphActionGroupNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
