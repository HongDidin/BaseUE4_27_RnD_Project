// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/DataprepRecipe.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepRecipe() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDEPRECATED_DataprepRecipe_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDEPRECATED_DataprepRecipe();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UDEPRECATED_DataprepRecipe::execGetAssets)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UObject*>*)Z_Param__Result=P_THIS->GetAssets();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDEPRECATED_DataprepRecipe::execGetActors)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<AActor*>*)Z_Param__Result=P_THIS->GetActors();
		P_NATIVE_END;
	}
	static FName NAME_UDEPRECATED_DataprepRecipe_TriggerPipelineTraversal = FName(TEXT("TriggerPipelineTraversal"));
	void UDEPRECATED_DataprepRecipe::TriggerPipelineTraversal()
	{
		ProcessEvent(FindFunctionChecked(NAME_UDEPRECATED_DataprepRecipe_TriggerPipelineTraversal),NULL);
	}
	void UDEPRECATED_DataprepRecipe::StaticRegisterNativesUDEPRECATED_DataprepRecipe()
	{
		UClass* Class = UDEPRECATED_DataprepRecipe::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetActors", &UDEPRECATED_DataprepRecipe::execGetActors },
			{ "GetAssets", &UDEPRECATED_DataprepRecipe::execGetAssets },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics
	{
		struct DataprepRecipe_eventGetActors_Parms
		{
			TArray<AActor*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepRecipe_eventGetActors_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Dataprep | Query" },
		{ "Comment", "/**\n\x09 * DEPRECATED\n\x09 * Returns all actors contained in its attached world\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "No use of Blueprint with Dataprep." },
		{ "HideSelfPin", "true" },
		{ "ModuleRelativePath", "Public/DataprepRecipe.h" },
		{ "ToolTip", "DEPRECATED\nReturns all actors contained in its attached world" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDEPRECATED_DataprepRecipe, nullptr, "GetActors", nullptr, nullptr, sizeof(DataprepRecipe_eventGetActors_Parms), Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics
	{
		struct DataprepRecipe_eventGetAssets_Parms
		{
			TArray<UObject*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepRecipe_eventGetAssets_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::Function_MetaDataParams[] = {
		{ "Category", "Dataprep | Query" },
		{ "Comment", "/**\n\x09 * DEPRECATED\n\x09 * Returns all valid assets contained in attached world\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "No use of Blueprint with Dataprep." },
		{ "HideSelfPin", "true" },
		{ "ModuleRelativePath", "Public/DataprepRecipe.h" },
		{ "ToolTip", "DEPRECATED\nReturns all valid assets contained in attached world" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDEPRECATED_DataprepRecipe, nullptr, "GetAssets", nullptr, nullptr, sizeof(DataprepRecipe_eventGetAssets_Parms), Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_TriggerPipelineTraversal_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_TriggerPipelineTraversal_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * DEPRECATED\n\x09 * Function used to trigger the execution of the pipeline\n\x09 * An event node associated to this function must be in the pipeline graph to run it.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "No use of Blueprint with Dataprep." },
		{ "ModuleRelativePath", "Public/DataprepRecipe.h" },
		{ "ToolTip", "DEPRECATED\nFunction used to trigger the execution of the pipeline\nAn event node associated to this function must be in the pipeline graph to run it." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_TriggerPipelineTraversal_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDEPRECATED_DataprepRecipe, nullptr, "TriggerPipelineTraversal", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_TriggerPipelineTraversal_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_TriggerPipelineTraversal_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_TriggerPipelineTraversal()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_TriggerPipelineTraversal_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDEPRECATED_DataprepRecipe_NoRegister()
	{
		return UDEPRECATED_DataprepRecipe::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetActors, "GetActors" }, // 135074606
		{ &Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_GetAssets, "GetAssets" }, // 799238266
		{ &Z_Construct_UFunction_UDEPRECATED_DataprepRecipe_TriggerPipelineTraversal, "TriggerPipelineTraversal" }, // 6672732
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DeprecationMessage", "No use of Blueprint with Dataprep." },
		{ "DisplayName", "DEPRECATED Data Preparation Recipe" },
		{ "IncludePath", "DataprepRecipe.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DataprepRecipe.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_DataprepRecipe>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics::ClassParams = {
		&UDEPRECATED_DataprepRecipe::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x021002A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_DataprepRecipe()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_DataprepRecipe, 1938215138);
	template<> DATAPREPCORE_API UClass* StaticClass<UDEPRECATED_DataprepRecipe>()
	{
		return UDEPRECATED_DataprepRecipe::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_DataprepRecipe(Z_Construct_UClass_UDEPRECATED_DataprepRecipe, &UDEPRECATED_DataprepRecipe::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDEPRECATED_DataprepRecipe"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_DataprepRecipe);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
