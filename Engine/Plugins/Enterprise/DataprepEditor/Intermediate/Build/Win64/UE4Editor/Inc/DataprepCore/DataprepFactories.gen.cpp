// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/DataprepFactories.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepFactories() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetFactory_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInstanceFactory_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInstanceFactory();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAsset_NoRegister();
// End Cross Module References
	void UDataprepAssetFactory::StaticRegisterNativesUDataprepAssetFactory()
	{
	}
	UClass* Z_Construct_UClass_UDataprepAssetFactory_NoRegister()
	{
		return UDataprepAssetFactory::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepAssetFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepAssetFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetFactory_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "DataprepFactories.h" },
		{ "ModuleRelativePath", "Public/DataprepFactories.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepAssetFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepAssetFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepAssetFactory_Statics::ClassParams = {
		&UDataprepAssetFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepAssetFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepAssetFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepAssetFactory, 2230461952);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepAssetFactory>()
	{
		return UDataprepAssetFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepAssetFactory(Z_Construct_UClass_UDataprepAssetFactory, &UDataprepAssetFactory::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepAssetFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepAssetFactory);
	void UDataprepAssetInstanceFactory::StaticRegisterNativesUDataprepAssetInstanceFactory()
	{
	}
	UClass* Z_Construct_UClass_UDataprepAssetInstanceFactory_NoRegister()
	{
		return UDataprepAssetInstanceFactory::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Parent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "DataprepFactories.h" },
		{ "ModuleRelativePath", "Public/DataprepFactories.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::NewProp_Parent_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/**\n\x09 * The parent of the of the instance to create\n\x09 */" },
		{ "ModuleRelativePath", "Public/DataprepFactories.h" },
		{ "ToolTip", "The parent of the of the instance to create" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::NewProp_Parent = { "Parent", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetInstanceFactory, Parent), Z_Construct_UClass_UDataprepAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::NewProp_Parent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::NewProp_Parent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::NewProp_Parent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepAssetInstanceFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::ClassParams = {
		&UDataprepAssetInstanceFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepAssetInstanceFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepAssetInstanceFactory, 1919652938);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepAssetInstanceFactory>()
	{
		return UDataprepAssetInstanceFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepAssetInstanceFactory(Z_Construct_UClass_UDataprepAssetInstanceFactory, &UDataprepAssetInstanceFactory::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepAssetInstanceFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepAssetInstanceFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
