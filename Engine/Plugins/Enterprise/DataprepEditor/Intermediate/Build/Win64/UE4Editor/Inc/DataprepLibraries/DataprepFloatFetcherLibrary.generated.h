// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPLIBRARIES_DataprepFloatFetcherLibrary_generated_h
#error "DataprepFloatFetcherLibrary.generated.h already included, missing '#pragma once' in DataprepFloatFetcherLibrary.h"
#endif
#define DATAPREPLIBRARIES_DataprepFloatFetcherLibrary_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepFloatBoundingVolumeFetcher(); \
	friend struct Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_Statics; \
public: \
	DECLARE_CLASS(UDataprepFloatBoundingVolumeFetcher, UDataprepFloatFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepFloatBoundingVolumeFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepFloatBoundingVolumeFetcher(); \
	friend struct Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_Statics; \
public: \
	DECLARE_CLASS(UDataprepFloatBoundingVolumeFetcher, UDataprepFloatFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepFloatBoundingVolumeFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepFloatBoundingVolumeFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepFloatBoundingVolumeFetcher) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepFloatBoundingVolumeFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepFloatBoundingVolumeFetcher); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepFloatBoundingVolumeFetcher(UDataprepFloatBoundingVolumeFetcher&&); \
	NO_API UDataprepFloatBoundingVolumeFetcher(const UDataprepFloatBoundingVolumeFetcher&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepFloatBoundingVolumeFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepFloatBoundingVolumeFetcher(UDataprepFloatBoundingVolumeFetcher&&); \
	NO_API UDataprepFloatBoundingVolumeFetcher(const UDataprepFloatBoundingVolumeFetcher&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepFloatBoundingVolumeFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepFloatBoundingVolumeFetcher); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepFloatBoundingVolumeFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_11_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPLIBRARIES_API UClass* StaticClass<class UDataprepFloatBoundingVolumeFetcher>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepFloatFetcherLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
