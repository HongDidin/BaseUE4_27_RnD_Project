// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepEditor/Private/DataprepGraph/DataprepGraph.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepGraph() {}
// Cross Module References
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepGraph_NoRegister();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepGraph();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraph();
	UPackage* Z_Construct_UPackage__Script_DataprepEditor();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepGraphRecipeNode_NoRegister();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepGraphRecipeNode();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphNode();
// End Cross Module References
	void UDataprepGraph::StaticRegisterNativesUDataprepGraph()
	{
	}
	UClass* Z_Construct_UClass_UDataprepGraph_NoRegister()
	{
		return UDataprepGraph::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepGraph_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepGraph_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraph,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraph_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * The UDataprepGraph class is used to display the pipeline of a Dataprep asset\n * in a SDataprepGraphEditor.\n */" },
		{ "IncludePath", "DataprepGraph/DataprepGraph.h" },
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraph.h" },
		{ "ToolTip", "The UDataprepGraph class is used to display the pipeline of a Dataprep asset\nin a SDataprepGraphEditor." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepGraph_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepGraph>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepGraph_Statics::ClassParams = {
		&UDataprepGraph::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepGraph_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraph_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepGraph()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepGraph_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepGraph, 531926787);
	template<> DATAPREPEDITOR_API UClass* StaticClass<UDataprepGraph>()
	{
		return UDataprepGraph::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepGraph(Z_Construct_UClass_UDataprepGraph, &UDataprepGraph::StaticClass, TEXT("/Script/DataprepEditor"), TEXT("UDataprepGraph"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepGraph);
	void UDataprepGraphRecipeNode::StaticRegisterNativesUDataprepGraphRecipeNode()
	{
	}
	UClass* Z_Construct_UClass_UDataprepGraphRecipeNode_NoRegister()
	{
		return UDataprepGraphRecipeNode::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepGraphRecipeNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepGraphRecipeNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepGraphRecipeNode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * The UDataprepGraphRecipeNode is the root graph node from which the associated widget,\n * SDataprepGraphTrackNode, will add all the action nodes and their content\n */" },
		{ "IncludePath", "DataprepGraph/DataprepGraph.h" },
		{ "ModuleRelativePath", "Private/DataprepGraph/DataprepGraph.h" },
		{ "ToolTip", "The UDataprepGraphRecipeNode is the root graph node from which the associated widget,\nSDataprepGraphTrackNode, will add all the action nodes and their content" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepGraphRecipeNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepGraphRecipeNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepGraphRecipeNode_Statics::ClassParams = {
		&UDataprepGraphRecipeNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepGraphRecipeNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepGraphRecipeNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepGraphRecipeNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepGraphRecipeNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepGraphRecipeNode, 2865328983);
	template<> DATAPREPEDITOR_API UClass* StaticClass<UDataprepGraphRecipeNode>()
	{
		return UDataprepGraphRecipeNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepGraphRecipeNode(Z_Construct_UClass_UDataprepGraphRecipeNode, &UDataprepGraphRecipeNode::StaticClass, TEXT("/Script/DataprepEditor"), TEXT("UDataprepGraphRecipeNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepGraphRecipeNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
