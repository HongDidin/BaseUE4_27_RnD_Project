// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepAssetInterface_generated_h
#error "DataprepAssetInterface.generated.h already included, missing '#pragma once' in DataprepAssetInterface.h"
#endif
#define DATAPREPCORE_DataprepAssetInterface_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepRecipeInterface(); \
	friend struct Z_Construct_UClass_UDataprepRecipeInterface_Statics; \
public: \
	DECLARE_CLASS(UDataprepRecipeInterface, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepRecipeInterface)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepRecipeInterface(); \
	friend struct Z_Construct_UClass_UDataprepRecipeInterface_Statics; \
public: \
	DECLARE_CLASS(UDataprepRecipeInterface, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepRecipeInterface)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepRecipeInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepRecipeInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepRecipeInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepRecipeInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepRecipeInterface(UDataprepRecipeInterface&&); \
	NO_API UDataprepRecipeInterface(const UDataprepRecipeInterface&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepRecipeInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepRecipeInterface(UDataprepRecipeInterface&&); \
	NO_API UDataprepRecipeInterface(const UDataprepRecipeInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepRecipeInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepRecipeInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepRecipeInterface)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_39_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_42_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepRecipeInterface>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepAssetInterface(); \
	friend struct Z_Construct_UClass_UDataprepAssetInterface_Statics; \
public: \
	DECLARE_CLASS(UDataprepAssetInterface, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepAssetInterface)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepAssetInterface(); \
	friend struct Z_Construct_UClass_UDataprepAssetInterface_Statics; \
public: \
	DECLARE_CLASS(UDataprepAssetInterface, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepAssetInterface)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepAssetInterface(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepAssetInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepAssetInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAssetInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepAssetInterface(UDataprepAssetInterface&&); \
	NO_API UDataprepAssetInterface(const UDataprepAssetInterface&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepAssetInterface(UDataprepAssetInterface&&); \
	NO_API UDataprepAssetInterface(const UDataprepAssetInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepAssetInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAssetInterface); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UDataprepAssetInterface)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_57_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h_60_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepAssetInterface>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInterface_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
