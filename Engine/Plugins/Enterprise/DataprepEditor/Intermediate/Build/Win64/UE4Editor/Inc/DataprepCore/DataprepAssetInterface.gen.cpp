// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/DataprepAssetInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepAssetInterface() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepRecipeInterface_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepRecipeInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInterface_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInterface();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetProducers_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepContentConsumer_NoRegister();
// End Cross Module References
	void UDataprepRecipeInterface::StaticRegisterNativesUDataprepRecipeInterface()
	{
	}
	UClass* Z_Construct_UClass_UDataprepRecipeInterface_NoRegister()
	{
		return UDataprepRecipeInterface::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepRecipeInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepRecipeInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRecipeInterface_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A DataprepAssetInterface is composed of a set of producers, inputs, a consumer, output,\n * and a recipe, set of actions. The producers generate assets and populate a given world.\n * The pipeline modifies the assets and the actors of the given world. Finally, the consumer\n * converts the assets and the given world. An FBX exporter is a possible consumer.\n * This class is an abstract modeling the data preparation pipeline.\n */" },
		{ "IncludePath", "DataprepAssetInterface.h" },
		{ "ModuleRelativePath", "Public/DataprepAssetInterface.h" },
		{ "ToolTip", "A DataprepAssetInterface is composed of a set of producers, inputs, a consumer, output,\nand a recipe, set of actions. The producers generate assets and populate a given world.\nThe pipeline modifies the assets and the actors of the given world. Finally, the consumer\nconverts the assets and the given world. An FBX exporter is a possible consumer.\nThis class is an abstract modeling the data preparation pipeline." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepRecipeInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepRecipeInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepRecipeInterface_Statics::ClassParams = {
		&UDataprepRecipeInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepRecipeInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRecipeInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepRecipeInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepRecipeInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepRecipeInterface, 467500499);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepRecipeInterface>()
	{
		return UDataprepRecipeInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepRecipeInterface(Z_Construct_UClass_UDataprepRecipeInterface, &UDataprepRecipeInterface::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepRecipeInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepRecipeInterface);
	void UDataprepAssetInterface::StaticRegisterNativesUDataprepAssetInterface()
	{
	}
	UClass* Z_Construct_UClass_UDataprepAssetInterface_NoRegister()
	{
		return UDataprepAssetInterface::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepAssetInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Recipe_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Recipe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Inputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Inputs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepAssetInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetInterface_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "DataprepAssetInterface.h" },
		{ "ModuleRelativePath", "Public/DataprepAssetInterface.h" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Recipe_MetaData[] = {
		{ "Comment", "/** Recipe associated to the Dataprep asset */" },
		{ "ModuleRelativePath", "Public/DataprepAssetInterface.h" },
		{ "ToolTip", "Recipe associated to the Dataprep asset" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Recipe = { "Recipe", nullptr, (EPropertyFlags)0x0020080800000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetInterface, Recipe), Z_Construct_UClass_UDataprepRecipeInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Recipe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Recipe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Inputs_MetaData[] = {
		{ "Comment", "/** Producers associated to the Dataprep asset */" },
		{ "ModuleRelativePath", "Public/DataprepAssetInterface.h" },
		{ "ToolTip", "Producers associated to the Dataprep asset" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Inputs = { "Inputs", nullptr, (EPropertyFlags)0x0020080800000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetInterface, Inputs), Z_Construct_UClass_UDataprepAssetProducers_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Inputs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Inputs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Output_MetaData[] = {
		{ "Comment", "/** Consumer associated to the Dataprep asset */" },
		{ "ModuleRelativePath", "Public/DataprepAssetInterface.h" },
		{ "ToolTip", "Consumer associated to the Dataprep asset" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0020080800000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetInterface, Output), Z_Construct_UClass_UDataprepContentConsumer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepAssetInterface_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Recipe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Inputs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetInterface_Statics::NewProp_Output,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepAssetInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepAssetInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepAssetInterface_Statics::ClassParams = {
		&UDataprepAssetInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UDataprepAssetInterface_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInterface_Statics::PropPointers), 0),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepAssetInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepAssetInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepAssetInterface, 651314442);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepAssetInterface>()
	{
		return UDataprepAssetInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepAssetInterface(Z_Construct_UClass_UDataprepAssetInterface, &UDataprepAssetInterface::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepAssetInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepAssetInterface);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
