// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepAssetInstance_generated_h
#error "DataprepAssetInstance.generated.h already included, missing '#pragma once' in DataprepAssetInstance.h"
#endif
#define DATAPREPCORE_DataprepAssetInstance_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepAssetInstance(); \
	friend struct Z_Construct_UClass_UDataprepAssetInstance_Statics; \
public: \
	DECLARE_CLASS(UDataprepAssetInstance, UDataprepAssetInterface, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepAssetInstance)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepAssetInstance(); \
	friend struct Z_Construct_UClass_UDataprepAssetInstance_Statics; \
public: \
	DECLARE_CLASS(UDataprepAssetInstance, UDataprepAssetInterface, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepAssetInstance)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepAssetInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepAssetInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepAssetInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAssetInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepAssetInstance(UDataprepAssetInstance&&); \
	NO_API UDataprepAssetInstance(const UDataprepAssetInstance&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepAssetInstance(UDataprepAssetInstance&&); \
	NO_API UDataprepAssetInstance(const UDataprepAssetInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepAssetInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAssetInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepAssetInstance)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Parent() { return STRUCT_OFFSET(UDataprepAssetInstance, Parent); } \
	FORCEINLINE static uint32 __PPO__Parameterization() { return STRUCT_OFFSET(UDataprepAssetInstance, Parameterization); } \
	FORCEINLINE static uint32 __PPO__ActionsFromDataprepAsset() { return STRUCT_OFFSET(UDataprepAssetInstance, ActionsFromDataprepAsset); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_21_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepAssetInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
