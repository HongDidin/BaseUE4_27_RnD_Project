// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepEditor/Private/BlueprintNodes/K2Node_DataprepAction.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_DataprepAction() {}
// Cross Module References
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_NoRegister();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UK2Node_DataprepActionCore();
	UPackage* Z_Construct_UPackage__Script_DataprepEditor();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepActionAsset_NoRegister();
// End Cross Module References
	void UDEPRECATED_K2Node_DataprepAction::StaticRegisterNativesUDEPRECATED_K2Node_DataprepAction()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_NoRegister()
	{
		return UDEPRECATED_K2Node_DataprepAction::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActionTitle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ActionTitle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataprepAction_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DataprepAction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_DataprepActionCore,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::Class_MetaDataParams[] = {
		{ "DeprecationMessage", "No use of Blueprint with Dataprep." },
		{ "IncludePath", "BlueprintNodes/K2Node_DataprepAction.h" },
		{ "ModuleRelativePath", "Private/BlueprintNodes/K2Node_DataprepAction.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::NewProp_ActionTitle_MetaData[] = {
		{ "ModuleRelativePath", "Private/BlueprintNodes/K2Node_DataprepAction.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::NewProp_ActionTitle = { "ActionTitle", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_K2Node_DataprepAction, ActionTitle), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::NewProp_ActionTitle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::NewProp_ActionTitle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::NewProp_DataprepAction_MetaData[] = {
		{ "ModuleRelativePath", "Private/BlueprintNodes/K2Node_DataprepAction.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::NewProp_DataprepAction = { "DataprepAction", nullptr, (EPropertyFlags)0x0020080020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_K2Node_DataprepAction, DataprepAction_DEPRECATED), Z_Construct_UClass_UDataprepActionAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::NewProp_DataprepAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::NewProp_DataprepAction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::NewProp_ActionTitle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::NewProp_DataprepAction,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_K2Node_DataprepAction>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::ClassParams = {
		&UDEPRECATED_K2Node_DataprepAction::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::PropPointers),
		0,
		0x020802A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_K2Node_DataprepAction, 2288884024);
	template<> DATAPREPEDITOR_API UClass* StaticClass<UDEPRECATED_K2Node_DataprepAction>()
	{
		return UDEPRECATED_K2Node_DataprepAction::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_K2Node_DataprepAction(Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction, &UDEPRECATED_K2Node_DataprepAction::StaticClass, TEXT("/Script/DataprepEditor"), TEXT("UDEPRECATED_K2Node_DataprepAction"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_K2Node_DataprepAction);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
