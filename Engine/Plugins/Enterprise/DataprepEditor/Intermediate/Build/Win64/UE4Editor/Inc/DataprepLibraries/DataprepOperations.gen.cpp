// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepLibraries/Private/DataprepOperations.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepOperations() {}
// Cross Module References
	DATAPREPLIBRARIES_API UScriptStruct* Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings();
	UPackage* Z_Construct_UPackage__Script_DataprepLibraries();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetLODsOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetLODsOperation();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetLODGroupOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetLODGroupOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetSimpleCollisionOperation();
	EDITORSCRIPTINGUTILITIES_API UEnum* Z_Construct_UEnum_EditorScriptingUtilities_EScriptingCollisionShapeType();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetMobilityOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetMobilityOperation();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_EComponentMobility();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetMaterialOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetMaterialOperation();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSubstituteMaterialOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSubstituteMaterialOperation();
	EDITORSCRIPTINGUTILITIES_API UEnum* Z_Construct_UEnum_EditorScriptingUtilities_EEditorScriptingStringMatchType();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetMeshOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetMeshOperation();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepAddTagsOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepAddTagsOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetMetadataOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetMetadataOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepConsolidateObjectsOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepConsolidateObjectsOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepRandomizeTransformOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepRandomizeTransformOperation();
	DATAPREPLIBRARIES_API UEnum* Z_Construct_UEnum_DataprepLibraries_ERandomizeTransformType();
	DATAPREPLIBRARIES_API UEnum* Z_Construct_UEnum_DataprepLibraries_ERandomizeTransformReferenceFrame();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepFlipFacesOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepFlipFacesOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetOutputFolder_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetOutputFolder();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepAddToLayerOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepAddToLayerOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetCollisionComplexityOperation();
	PHYSICSCORE_API UEnum* Z_Construct_UEnum_PhysicsCore_ECollisionTraceFlag();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation();
// End Cross Module References
class UScriptStruct* FDataprepSetLODsReductionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAPREPLIBRARIES_API uint32 Get_Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings, Z_Construct_UPackage__Script_DataprepLibraries(), TEXT("DataprepSetLODsReductionSettings"), sizeof(FDataprepSetLODsReductionSettings), Get_Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Hash());
	}
	return Singleton;
}
template<> DATAPREPLIBRARIES_API UScriptStruct* StaticStruct<FDataprepSetLODsReductionSettings>()
{
	return FDataprepSetLODsReductionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataprepSetLODsReductionSettings(FDataprepSetLODsReductionSettings::StaticStruct, TEXT("/Script/DataprepLibraries"), TEXT("DataprepSetLODsReductionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DataprepLibraries_StaticRegisterNativesFDataprepSetLODsReductionSettings
{
	FScriptStruct_DataprepLibraries_StaticRegisterNativesFDataprepSetLODsReductionSettings()
	{
		UScriptStruct::DeferCppStructOps<FDataprepSetLODsReductionSettings>(FName(TEXT("DataprepSetLODsReductionSettings")));
	}
} ScriptStruct_DataprepLibraries_StaticRegisterNativesFDataprepSetLODsReductionSettings;
	struct Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PercentTriangles_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PercentTriangles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Local struct used by UDataprepSetLODsOperation to better control UX */" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Local struct used by UDataprepSetLODsOperation to better control UX" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataprepSetLODsReductionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewProp_PercentTriangles_MetaData[] = {
		{ "Category", "SetLODsReductionSettings" },
		{ "Comment", "// Percentage of triangles to keep. Ranges from 0.0 to 1.0: 1.0 = no reduction, 0.0 = no triangles.\n" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Percentage of triangles to keep. Ranges from 0.0 to 1.0: 1.0 = no reduction, 0.0 = no triangles." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewProp_PercentTriangles = { "PercentTriangles", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataprepSetLODsReductionSettings, PercentTriangles), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewProp_PercentTriangles_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewProp_PercentTriangles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewProp_ScreenSize_MetaData[] = {
		{ "Category", "SetLODsReductionSettings" },
		{ "Comment", "// ScreenSize to display this LOD. Ranges from 0.0 to 1.0.\n" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "ScreenSize to display this LOD. Ranges from 0.0 to 1.0." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewProp_ScreenSize = { "ScreenSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataprepSetLODsReductionSettings, ScreenSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewProp_ScreenSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewProp_ScreenSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewProp_PercentTriangles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::NewProp_ScreenSize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
		nullptr,
		&NewStructOps,
		"DataprepSetLODsReductionSettings",
		sizeof(FDataprepSetLODsReductionSettings),
		alignof(FDataprepSetLODsReductionSettings),
		Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepLibraries();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataprepSetLODsReductionSettings"), sizeof(FDataprepSetLODsReductionSettings), Get_Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings_Hash() { return 925255762U; }
	void UDataprepSetLODsOperation::StaticRegisterNativesUDataprepSetLODsOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetLODsOperation_NoRegister()
	{
		return UDataprepSetLODsOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetLODsOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoComputeLODScreenSize_MetaData[];
#endif
		static void NewProp_bAutoComputeLODScreenSize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoComputeLODScreenSize;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReductionSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReductionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReductionSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetLODsOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetLODsOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "MeshOperation" },
		{ "DisplayName", "Set LODs" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "For each static mesh to process, replace the existing static mesh's LODs with new ones based on the set of reduction settings" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_bAutoComputeLODScreenSize_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "DisplayName", "Auto Screen Size" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "If true, the screen sizes at which LODs swap are automatically computed" },
	};
#endif
	void Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_bAutoComputeLODScreenSize_SetBit(void* Obj)
	{
		((UDataprepSetLODsOperation*)Obj)->bAutoComputeLODScreenSize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_bAutoComputeLODScreenSize = { "bAutoComputeLODScreenSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepSetLODsOperation), &Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_bAutoComputeLODScreenSize_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_bAutoComputeLODScreenSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_bAutoComputeLODScreenSize_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_ReductionSettings_Inner = { "ReductionSettings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDataprepSetLODsReductionSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_ReductionSettings_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Array of LOD reduction settings" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_ReductionSettings = { "ReductionSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetLODsOperation, ReductionSettings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_ReductionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_ReductionSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetLODsOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_bAutoComputeLODScreenSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_ReductionSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetLODsOperation_Statics::NewProp_ReductionSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetLODsOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetLODsOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetLODsOperation_Statics::ClassParams = {
		&UDataprepSetLODsOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetLODsOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetLODsOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetLODsOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetLODsOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetLODsOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetLODsOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetLODsOperation, 3694615598);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetLODsOperation>()
	{
		return UDataprepSetLODsOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetLODsOperation(Z_Construct_UClass_UDataprepSetLODsOperation, &UDataprepSetLODsOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetLODsOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetLODsOperation);
	void UDataprepSetLODGroupOperation::StaticRegisterNativesUDataprepSetLODGroupOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetLODGroupOperation_NoRegister()
	{
		return UDataprepSetLODGroupOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_GroupName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "MeshOperation" },
		{ "DisplayName", "Set LOD Group" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "For each static mesh to process, replace the existing static mesh's LODs with new ones based on selected group" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::NewProp_GroupName_MetaData[] = {
		{ "Category", "SetLOGGroup_Internal" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::NewProp_GroupName = { "GroupName", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetLODGroupOperation, GroupName), METADATA_PARAMS(Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::NewProp_GroupName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::NewProp_GroupName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::NewProp_GroupName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetLODGroupOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::ClassParams = {
		&UDataprepSetLODGroupOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetLODGroupOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetLODGroupOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetLODGroupOperation, 2239272047);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetLODGroupOperation>()
	{
		return UDataprepSetLODGroupOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetLODGroupOperation(Z_Construct_UClass_UDataprepSetLODGroupOperation, &UDataprepSetLODGroupOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetLODGroupOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetLODGroupOperation);
	void UDataprepSetSimpleCollisionOperation::StaticRegisterNativesUDataprepSetSimpleCollisionOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_NoRegister()
	{
		return UDataprepSetSimpleCollisionOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ShapeType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShapeType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ShapeType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "MeshOperation" },
		{ "DisplayName", "Set Simple Collision" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "For each static mesh to process, replace the existing static mesh's collision setup with a simple one based on selected shape" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::NewProp_ShapeType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::NewProp_ShapeType_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Shape's of the collision geometry encompassing the static mesh" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::NewProp_ShapeType = { "ShapeType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetSimpleCollisionOperation, ShapeType), Z_Construct_UEnum_EditorScriptingUtilities_EScriptingCollisionShapeType, METADATA_PARAMS(Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::NewProp_ShapeType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::NewProp_ShapeType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::NewProp_ShapeType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::NewProp_ShapeType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetSimpleCollisionOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::ClassParams = {
		&UDataprepSetSimpleCollisionOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetSimpleCollisionOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetSimpleCollisionOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetSimpleCollisionOperation, 787577121);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetSimpleCollisionOperation>()
	{
		return UDataprepSetSimpleCollisionOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetSimpleCollisionOperation(Z_Construct_UClass_UDataprepSetSimpleCollisionOperation, &UDataprepSetSimpleCollisionOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetSimpleCollisionOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetSimpleCollisionOperation);
	void UDataprepSetConvexDecompositionCollisionOperation::StaticRegisterNativesUDataprepSetConvexDecompositionCollisionOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_NoRegister()
	{
		return UDataprepSetConvexDecompositionCollisionOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HullCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxHullVerts_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxHullVerts;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HullPrecision_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HullPrecision;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "MeshOperation" },
		{ "DisplayName", "Set Convex Collision" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "For each static mesh to process, replace the existing static mesh's collision setup with a convex decomposition one computed using the Hull settings" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_HullCount_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Maximum number of convex pieces that will be created" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_HullCount = { "HullCount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetConvexDecompositionCollisionOperation, HullCount), METADATA_PARAMS(Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_HullCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_HullCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_MaxHullVerts_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Maximum number of vertices allowed for any generated convex hulls" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_MaxHullVerts = { "MaxHullVerts", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetConvexDecompositionCollisionOperation, MaxHullVerts), METADATA_PARAMS(Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_MaxHullVerts_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_MaxHullVerts_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_HullPrecision_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Number of voxels to use when generating collision" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_HullPrecision = { "HullPrecision", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetConvexDecompositionCollisionOperation, HullPrecision), METADATA_PARAMS(Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_HullPrecision_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_HullPrecision_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_HullCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_MaxHullVerts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::NewProp_HullPrecision,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetConvexDecompositionCollisionOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::ClassParams = {
		&UDataprepSetConvexDecompositionCollisionOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetConvexDecompositionCollisionOperation, 4283643194);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetConvexDecompositionCollisionOperation>()
	{
		return UDataprepSetConvexDecompositionCollisionOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetConvexDecompositionCollisionOperation(Z_Construct_UClass_UDataprepSetConvexDecompositionCollisionOperation, &UDataprepSetConvexDecompositionCollisionOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetConvexDecompositionCollisionOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetConvexDecompositionCollisionOperation);
	void UDataprepSetMobilityOperation::StaticRegisterNativesUDataprepSetMobilityOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetMobilityOperation_NoRegister()
	{
		return UDataprepSetMobilityOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetMobilityOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MobilityType_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MobilityType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ActorOperation" },
		{ "DisplayName", "Set Mobility" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "For each actor to process, update its mobilty with the selected value" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::NewProp_MobilityType_MetaData[] = {
		{ "Category", "ActorOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Type of mobility to set on actors" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::NewProp_MobilityType = { "MobilityType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetMobilityOperation, MobilityType), Z_Construct_UEnum_Engine_EComponentMobility, METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::NewProp_MobilityType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::NewProp_MobilityType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::NewProp_MobilityType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetMobilityOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::ClassParams = {
		&UDataprepSetMobilityOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetMobilityOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetMobilityOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetMobilityOperation, 1555463736);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetMobilityOperation>()
	{
		return UDataprepSetMobilityOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetMobilityOperation(Z_Construct_UClass_UDataprepSetMobilityOperation, &UDataprepSetMobilityOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetMobilityOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetMobilityOperation);
	void UDataprepSetMaterialOperation::StaticRegisterNativesUDataprepSetMaterialOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetMaterialOperation_NoRegister()
	{
		return UDataprepSetMaterialOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetMaterialOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "MeshOperation" },
		{ "DisplayName", "Set Material" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "On each static mesh or actor to process, replace any materials used with the specified one" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::NewProp_Material_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Material to use as a substitute" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetMaterialOperation, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::NewProp_Material_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::NewProp_Material,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetMaterialOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::ClassParams = {
		&UDataprepSetMaterialOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetMaterialOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetMaterialOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetMaterialOperation, 520171997);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetMaterialOperation>()
	{
		return UDataprepSetMaterialOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetMaterialOperation(Z_Construct_UClass_UDataprepSetMaterialOperation, &UDataprepSetMaterialOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetMaterialOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetMaterialOperation);
	void UDataprepSubstituteMaterialOperation::StaticRegisterNativesUDataprepSubstituteMaterialOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSubstituteMaterialOperation_NoRegister()
	{
		return UDataprepSubstituteMaterialOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSearch_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MaterialSearch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_StringMatch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringMatch_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_StringMatch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSubstitute_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialSubstitute;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "MeshOperation" },
		{ "DisplayName", "Substitute Material" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "On each static mesh or actor to process, replace the material matching the criteria with the specified one" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_MaterialSearch_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Name of the material(s) to search for. Wildcard is supported" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_MaterialSearch = { "MaterialSearch", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSubstituteMaterialOperation, MaterialSearch), METADATA_PARAMS(Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_MaterialSearch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_MaterialSearch_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_StringMatch_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_StringMatch_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Type of matching to perform with MaterialSearch string" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_StringMatch = { "StringMatch", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSubstituteMaterialOperation, StringMatch), Z_Construct_UEnum_EditorScriptingUtilities_EEditorScriptingStringMatchType, METADATA_PARAMS(Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_StringMatch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_StringMatch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_MaterialSubstitute_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Material to use as a substitute" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_MaterialSubstitute = { "MaterialSubstitute", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSubstituteMaterialOperation, MaterialSubstitute), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_MaterialSubstitute_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_MaterialSubstitute_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_MaterialSearch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_StringMatch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_StringMatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::NewProp_MaterialSubstitute,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSubstituteMaterialOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::ClassParams = {
		&UDataprepSubstituteMaterialOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSubstituteMaterialOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSubstituteMaterialOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSubstituteMaterialOperation, 3026487377);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSubstituteMaterialOperation>()
	{
		return UDataprepSubstituteMaterialOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSubstituteMaterialOperation(Z_Construct_UClass_UDataprepSubstituteMaterialOperation, &UDataprepSubstituteMaterialOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSubstituteMaterialOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSubstituteMaterialOperation);
	void UDataprepSubstituteMaterialByTableOperation::StaticRegisterNativesUDataprepSubstituteMaterialByTableOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_NoRegister()
	{
		return UDataprepSubstituteMaterialByTableOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialDataTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialDataTable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "MeshOperation" },
		{ "DisplayName", "Substitute Material By Table" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "On each static mesh or actor to process, replace the material found in the first column of the table with the one from the second column in the same row" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::NewProp_MaterialDataTable_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Data table to use for the substitution" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::NewProp_MaterialDataTable = { "MaterialDataTable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSubstituteMaterialByTableOperation, MaterialDataTable), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::NewProp_MaterialDataTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::NewProp_MaterialDataTable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::NewProp_MaterialDataTable,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSubstituteMaterialByTableOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::ClassParams = {
		&UDataprepSubstituteMaterialByTableOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSubstituteMaterialByTableOperation, 19864395);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSubstituteMaterialByTableOperation>()
	{
		return UDataprepSubstituteMaterialByTableOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSubstituteMaterialByTableOperation(Z_Construct_UClass_UDataprepSubstituteMaterialByTableOperation, &UDataprepSubstituteMaterialByTableOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSubstituteMaterialByTableOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSubstituteMaterialByTableOperation);
	void UDataprepSetMeshOperation::StaticRegisterNativesUDataprepSetMeshOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetMeshOperation_NoRegister()
	{
		return UDataprepSetMeshOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetMeshOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetMeshOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMeshOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ActorOperation" },
		{ "DisplayName", "Set Mesh" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "On each actor to process, replace any meshes used with the specified one" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMeshOperation_Statics::NewProp_StaticMesh_MetaData[] = {
		{ "Category", "ActorOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Mesh to use as a substitute" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepSetMeshOperation_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetMeshOperation, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMeshOperation_Statics::NewProp_StaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMeshOperation_Statics::NewProp_StaticMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetMeshOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetMeshOperation_Statics::NewProp_StaticMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetMeshOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetMeshOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetMeshOperation_Statics::ClassParams = {
		&UDataprepSetMeshOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetMeshOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMeshOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMeshOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMeshOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetMeshOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetMeshOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetMeshOperation, 1564249891);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetMeshOperation>()
	{
		return UDataprepSetMeshOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetMeshOperation(Z_Construct_UClass_UDataprepSetMeshOperation, &UDataprepSetMeshOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetMeshOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetMeshOperation);
	void UDataprepAddTagsOperation::StaticRegisterNativesUDataprepAddTagsOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepAddTagsOperation_NoRegister()
	{
		return UDataprepAddTagsOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepAddTagsOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Tags_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tags_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Tags;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepAddTagsOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAddTagsOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ActorOperation" },
		{ "DisplayName", "Add Tags" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "On each actor to process, add specified tags" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDataprepAddTagsOperation_Statics::NewProp_Tags_Inner = { "Tags", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAddTagsOperation_Statics::NewProp_Tags_MetaData[] = {
		{ "Category", "ActorOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Array of tags to add" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepAddTagsOperation_Statics::NewProp_Tags = { "Tags", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAddTagsOperation, Tags), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepAddTagsOperation_Statics::NewProp_Tags_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAddTagsOperation_Statics::NewProp_Tags_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepAddTagsOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAddTagsOperation_Statics::NewProp_Tags_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAddTagsOperation_Statics::NewProp_Tags,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepAddTagsOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepAddTagsOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepAddTagsOperation_Statics::ClassParams = {
		&UDataprepAddTagsOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepAddTagsOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAddTagsOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepAddTagsOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAddTagsOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepAddTagsOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepAddTagsOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepAddTagsOperation, 444320066);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepAddTagsOperation>()
	{
		return UDataprepAddTagsOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepAddTagsOperation(Z_Construct_UClass_UDataprepAddTagsOperation, &UDataprepAddTagsOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepAddTagsOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepAddTagsOperation);
	void UDataprepSetMetadataOperation::StaticRegisterNativesUDataprepSetMetadataOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetMetadataOperation_NoRegister()
	{
		return UDataprepSetMetadataOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetMetadataOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Metadata_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Metadata_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Metadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Metadata;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ActorOperation" },
		{ "DisplayName", "Set Metadata" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "On each actor to process set metadata value" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::NewProp_Metadata_ValueProp = { "Metadata", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::NewProp_Metadata_Key_KeyProp = { "Metadata_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::NewProp_Metadata_MetaData[] = {
		{ "Category", "ActorOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Array of metadata values" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::NewProp_Metadata = { "Metadata", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetMetadataOperation, Metadata), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::NewProp_Metadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::NewProp_Metadata_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::NewProp_Metadata_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::NewProp_Metadata_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::NewProp_Metadata,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetMetadataOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::ClassParams = {
		&UDataprepSetMetadataOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetMetadataOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetMetadataOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetMetadataOperation, 1316494595);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetMetadataOperation>()
	{
		return UDataprepSetMetadataOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetMetadataOperation(Z_Construct_UClass_UDataprepSetMetadataOperation, &UDataprepSetMetadataOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetMetadataOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetMetadataOperation);
	void UDataprepConsolidateObjectsOperation::StaticRegisterNativesUDataprepConsolidateObjectsOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepConsolidateObjectsOperation_NoRegister()
	{
		return UDataprepConsolidateObjectsOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepConsolidateObjectsOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepConsolidateObjectsOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepConsolidateObjectsOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "AssetOperation" },
		{ "DisplayName", "Replace Asset References" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Replace references to each asset with the first asset in the list" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepConsolidateObjectsOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepConsolidateObjectsOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepConsolidateObjectsOperation_Statics::ClassParams = {
		&UDataprepConsolidateObjectsOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepConsolidateObjectsOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepConsolidateObjectsOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepConsolidateObjectsOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepConsolidateObjectsOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepConsolidateObjectsOperation, 2167800904);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepConsolidateObjectsOperation>()
	{
		return UDataprepConsolidateObjectsOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepConsolidateObjectsOperation(Z_Construct_UClass_UDataprepConsolidateObjectsOperation, &UDataprepConsolidateObjectsOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepConsolidateObjectsOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepConsolidateObjectsOperation);
	void UDataprepRandomizeTransformOperation::StaticRegisterNativesUDataprepRandomizeTransformOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepRandomizeTransformOperation_NoRegister()
	{
		return UDataprepRandomizeTransformOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TransformType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransformType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReferenceFrame_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReferenceFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReferenceFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Max;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ActorOperation" },
		{ "DisplayName", "Random Offset Transform" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "For each actor in the input set, offset its position/rotation/scale with random vector generated from X/Y/Z Min-Max." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_TransformType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_TransformType_MetaData[] = {
		{ "Category", "ActorOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Transform component to randomize" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_TransformType = { "TransformType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRandomizeTransformOperation, TransformType), Z_Construct_UEnum_DataprepLibraries_ERandomizeTransformType, METADATA_PARAMS(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_TransformType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_TransformType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_ReferenceFrame_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_ReferenceFrame_MetaData[] = {
		{ "Category", "ActorOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Reference frame to use (relative/world)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_ReferenceFrame = { "ReferenceFrame", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRandomizeTransformOperation, ReferenceFrame), Z_Construct_UEnum_DataprepLibraries_ERandomizeTransformReferenceFrame, METADATA_PARAMS(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_ReferenceFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_ReferenceFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "ActorOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Min random value" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRandomizeTransformOperation, Min), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "ActorOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Max random value" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRandomizeTransformOperation, Max), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_Max_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_TransformType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_TransformType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_ReferenceFrame_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_ReferenceFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::NewProp_Max,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepRandomizeTransformOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::ClassParams = {
		&UDataprepRandomizeTransformOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepRandomizeTransformOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepRandomizeTransformOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepRandomizeTransformOperation, 1057830249);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepRandomizeTransformOperation>()
	{
		return UDataprepRandomizeTransformOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepRandomizeTransformOperation(Z_Construct_UClass_UDataprepRandomizeTransformOperation, &UDataprepRandomizeTransformOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepRandomizeTransformOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepRandomizeTransformOperation);
	void UDataprepFlipFacesOperation::StaticRegisterNativesUDataprepFlipFacesOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepFlipFacesOperation_NoRegister()
	{
		return UDataprepFlipFacesOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepFlipFacesOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepFlipFacesOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepFlipFacesOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ActorOperation" },
		{ "DisplayName", "Flip Faces" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "On each actor to process, flip faces of each mesh" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepFlipFacesOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepFlipFacesOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepFlipFacesOperation_Statics::ClassParams = {
		&UDataprepFlipFacesOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepFlipFacesOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepFlipFacesOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepFlipFacesOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepFlipFacesOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepFlipFacesOperation, 2509621898);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepFlipFacesOperation>()
	{
		return UDataprepFlipFacesOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepFlipFacesOperation(Z_Construct_UClass_UDataprepFlipFacesOperation, &UDataprepFlipFacesOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepFlipFacesOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepFlipFacesOperation);
	void UDataprepSetOutputFolder::StaticRegisterNativesUDataprepSetOutputFolder()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetOutputFolder_NoRegister()
	{
		return UDataprepSetOutputFolder::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetOutputFolder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FolderName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FolderName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetOutputFolder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetOutputFolder_Statics::Class_MetaDataParams[] = {
		{ "Category", "AssetOperation" },
		{ "DisplayName", "Output to Folder" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "For each asset to process, set the sub-folder to save it to.\nThe sub-folder is relative to the folder specified to the Dataprep consumer." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetOutputFolder_Statics::NewProp_FolderName_MetaData[] = {
		{ "Category", "AssetOperation" },
		{ "DisplayName", "Folder Name" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Name of the sub folder the assets to be saved to" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDataprepSetOutputFolder_Statics::NewProp_FolderName = { "FolderName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetOutputFolder, FolderName), METADATA_PARAMS(Z_Construct_UClass_UDataprepSetOutputFolder_Statics::NewProp_FolderName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetOutputFolder_Statics::NewProp_FolderName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetOutputFolder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetOutputFolder_Statics::NewProp_FolderName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetOutputFolder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetOutputFolder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetOutputFolder_Statics::ClassParams = {
		&UDataprepSetOutputFolder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetOutputFolder_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetOutputFolder_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetOutputFolder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetOutputFolder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetOutputFolder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetOutputFolder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetOutputFolder, 251526440);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetOutputFolder>()
	{
		return UDataprepSetOutputFolder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetOutputFolder(Z_Construct_UClass_UDataprepSetOutputFolder, &UDataprepSetOutputFolder::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetOutputFolder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetOutputFolder);
	void UDataprepAddToLayerOperation::StaticRegisterNativesUDataprepAddToLayerOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepAddToLayerOperation_NoRegister()
	{
		return UDataprepAddToLayerOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepAddToLayerOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LayerName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LayerName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ActorOperation" },
		{ "DisplayName", "Add To Layer" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "On each actor to process, add the actor to the layer" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::NewProp_LayerName_MetaData[] = {
		{ "Category", "ActorOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Name of the layer to add the actors to" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::NewProp_LayerName = { "LayerName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAddToLayerOperation, LayerName), METADATA_PARAMS(Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::NewProp_LayerName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::NewProp_LayerName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::NewProp_LayerName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepAddToLayerOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::ClassParams = {
		&UDataprepAddToLayerOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepAddToLayerOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepAddToLayerOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepAddToLayerOperation, 1964000921);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepAddToLayerOperation>()
	{
		return UDataprepAddToLayerOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepAddToLayerOperation(Z_Construct_UClass_UDataprepAddToLayerOperation, &UDataprepAddToLayerOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepAddToLayerOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepAddToLayerOperation);
	void UDataprepSetCollisionComplexityOperation::StaticRegisterNativesUDataprepSetCollisionComplexityOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_NoRegister()
	{
		return UDataprepSetCollisionComplexityOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionTraceFlag_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CollisionTraceFlag;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "MeshOperation" },
		{ "DisplayName", "Set Collision Complexity" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "For each static mesh to process, set its collision complexity" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::NewProp_CollisionTraceFlag_MetaData[] = {
		{ "Category", "MeshOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Collision complexity" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::NewProp_CollisionTraceFlag = { "CollisionTraceFlag", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetCollisionComplexityOperation, CollisionTraceFlag), Z_Construct_UEnum_PhysicsCore_ECollisionTraceFlag, METADATA_PARAMS(Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::NewProp_CollisionTraceFlag_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::NewProp_CollisionTraceFlag_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::NewProp_CollisionTraceFlag,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetCollisionComplexityOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::ClassParams = {
		&UDataprepSetCollisionComplexityOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetCollisionComplexityOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetCollisionComplexityOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetCollisionComplexityOperation, 2534261659);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetCollisionComplexityOperation>()
	{
		return UDataprepSetCollisionComplexityOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetCollisionComplexityOperation(Z_Construct_UClass_UDataprepSetCollisionComplexityOperation, &UDataprepSetCollisionComplexityOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetCollisionComplexityOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetCollisionComplexityOperation);
	void UDataprepSetMaxTextureSizeOperation::StaticRegisterNativesUDataprepSetMaxTextureSizeOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_NoRegister()
	{
		return UDataprepSetMaxTextureSizeOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxTextureSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxTextureSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowPadding_MetaData[];
#endif
		static void NewProp_bAllowPadding_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowPadding;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "AssetOperation" },
		{ "DisplayName", "Set Max Texture Size" },
		{ "IncludePath", "DataprepOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "Set max size (width or height) each input texture" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_MaxTextureSize_MetaData[] = {
		{ "Category", "AssetOperation" },
		{ "ClampMin", "1" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_MaxTextureSize = { "MaxTextureSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSetMaxTextureSizeOperation, MaxTextureSize), METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_MaxTextureSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_MaxTextureSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_bAllowPadding_MetaData[] = {
		{ "Category", "AssetOperation" },
		{ "ModuleRelativePath", "Private/DataprepOperations.h" },
		{ "ToolTip", "If true, original texture size will be enforced to power of two before resizing (if it's a non-power of two size), else only POT textures will be affected." },
	};
#endif
	void Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_bAllowPadding_SetBit(void* Obj)
	{
		((UDataprepSetMaxTextureSizeOperation*)Obj)->bAllowPadding = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_bAllowPadding = { "bAllowPadding", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepSetMaxTextureSizeOperation), &Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_bAllowPadding_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_bAllowPadding_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_bAllowPadding_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_MaxTextureSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::NewProp_bAllowPadding,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSetMaxTextureSizeOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::ClassParams = {
		&UDataprepSetMaxTextureSizeOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSetMaxTextureSizeOperation, 3092814390);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSetMaxTextureSizeOperation>()
	{
		return UDataprepSetMaxTextureSizeOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSetMaxTextureSizeOperation(Z_Construct_UClass_UDataprepSetMaxTextureSizeOperation, &UDataprepSetMaxTextureSizeOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSetMaxTextureSizeOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSetMaxTextureSizeOperation);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
