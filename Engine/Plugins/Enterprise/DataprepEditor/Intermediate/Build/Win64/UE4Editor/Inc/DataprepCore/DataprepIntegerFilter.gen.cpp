// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/SelectionSystem/DataprepIntegerFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepIntegerFilter() {}
// Cross Module References
	DATAPREPCORE_API UEnum* Z_Construct_UEnum_DataprepCore_EDataprepIntegerMatchType();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepIntegerFilter_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepIntegerFilter();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepFilter();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepIntegerFetcher_NoRegister();
// End Cross Module References
	static UEnum* EDataprepIntegerMatchType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DataprepCore_EDataprepIntegerMatchType, Z_Construct_UPackage__Script_DataprepCore(), TEXT("EDataprepIntegerMatchType"));
		}
		return Singleton;
	}
	template<> DATAPREPCORE_API UEnum* StaticEnum<EDataprepIntegerMatchType>()
	{
		return EDataprepIntegerMatchType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDataprepIntegerMatchType(EDataprepIntegerMatchType_StaticEnum, TEXT("/Script/DataprepCore"), TEXT("EDataprepIntegerMatchType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DataprepCore_EDataprepIntegerMatchType_Hash() { return 619998402U; }
	UEnum* Z_Construct_UEnum_DataprepCore_EDataprepIntegerMatchType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDataprepIntegerMatchType"), 0, Get_Z_Construct_UEnum_DataprepCore_EDataprepIntegerMatchType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDataprepIntegerMatchType::LessThan", (int64)EDataprepIntegerMatchType::LessThan },
				{ "EDataprepIntegerMatchType::GreatherThan", (int64)EDataprepIntegerMatchType::GreatherThan },
				{ "EDataprepIntegerMatchType::IsEqual", (int64)EDataprepIntegerMatchType::IsEqual },
				{ "EDataprepIntegerMatchType::InBetween", (int64)EDataprepIntegerMatchType::InBetween },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "GreatherThan.Name", "EDataprepIntegerMatchType::GreatherThan" },
				{ "InBetween.Name", "EDataprepIntegerMatchType::InBetween" },
				{ "IsEqual.Name", "EDataprepIntegerMatchType::IsEqual" },
				{ "LessThan.Name", "EDataprepIntegerMatchType::LessThan" },
				{ "ModuleRelativePath", "Public/SelectionSystem/DataprepIntegerFilter.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DataprepCore,
				nullptr,
				"EDataprepIntegerMatchType",
				"EDataprepIntegerMatchType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDataprepIntegerFilter::StaticRegisterNativesUDataprepIntegerFilter()
	{
	}
	UClass* Z_Construct_UClass_UDataprepIntegerFilter_NoRegister()
	{
		return UDataprepIntegerFilter::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepIntegerFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntFetcher_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_IntFetcher;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_IntegerMatchingCriteria_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntegerMatchingCriteria_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_IntegerMatchingCriteria;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EqualValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_EqualValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FromValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_FromValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ToValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ToValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepIntegerFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepIntegerFilter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SelectionSystem/DataprepIntegerFilter.h" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepIntegerFilter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntFetcher_MetaData[] = {
		{ "Comment", "// The source of int selected by the user\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepIntegerFilter.h" },
		{ "ToolTip", "The source of int selected by the user" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntFetcher = { "IntFetcher", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepIntegerFilter, IntFetcher), Z_Construct_UClass_UDataprepIntegerFetcher_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntFetcher_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntFetcher_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntegerMatchingCriteria_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntegerMatchingCriteria_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The matching criteria used when checking if a fetched value can pass the filter\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepIntegerFilter.h" },
		{ "ToolTip", "The matching criteria used when checking if a fetched value can pass the filter" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntegerMatchingCriteria = { "IntegerMatchingCriteria", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepIntegerFilter, IntegerMatchingCriteria), Z_Construct_UEnum_DataprepCore_EDataprepIntegerMatchType, METADATA_PARAMS(Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntegerMatchingCriteria_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntegerMatchingCriteria_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_EqualValue_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The value to use when doing the comparison against the fetched value\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepIntegerFilter.h" },
		{ "ToolTip", "The value to use when doing the comparison against the fetched value" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_EqualValue = { "EqualValue", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepIntegerFilter, EqualValue), METADATA_PARAMS(Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_EqualValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_EqualValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_FromValue_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// A value used for the in-between check\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepIntegerFilter.h" },
		{ "ToolTip", "A value used for the in-between check" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_FromValue = { "FromValue", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepIntegerFilter, FromValue), METADATA_PARAMS(Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_FromValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_FromValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_ToValue_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// A value used for the in-between check\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepIntegerFilter.h" },
		{ "ToolTip", "A value used for the in-between check" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_ToValue = { "ToValue", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepIntegerFilter, ToValue), METADATA_PARAMS(Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_ToValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_ToValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepIntegerFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntFetcher,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntegerMatchingCriteria_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_IntegerMatchingCriteria,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_EqualValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_FromValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepIntegerFilter_Statics::NewProp_ToValue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepIntegerFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepIntegerFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepIntegerFilter_Statics::ClassParams = {
		&UDataprepIntegerFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepIntegerFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIntegerFilter_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepIntegerFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIntegerFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepIntegerFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepIntegerFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepIntegerFilter, 1282273454);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepIntegerFilter>()
	{
		return UDataprepIntegerFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepIntegerFilter(Z_Construct_UClass_UDataprepIntegerFilter, &UDataprepIntegerFilter::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepIntegerFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepIntegerFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
