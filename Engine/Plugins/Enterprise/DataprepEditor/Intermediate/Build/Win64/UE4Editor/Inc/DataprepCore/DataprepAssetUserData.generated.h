// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepAssetUserData_generated_h
#error "DataprepAssetUserData.generated.h already included, missing '#pragma once' in DataprepAssetUserData.h"
#endif
#define DATAPREPCORE_DataprepAssetUserData_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepAssetUserData(); \
	friend struct Z_Construct_UClass_UDataprepAssetUserData_Statics; \
public: \
	DECLARE_CLASS(UDataprepAssetUserData, UAssetUserData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepAssetUserData)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepAssetUserData(); \
	friend struct Z_Construct_UClass_UDataprepAssetUserData_Statics; \
public: \
	DECLARE_CLASS(UDataprepAssetUserData, UAssetUserData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepAssetUserData)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepAssetUserData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepAssetUserData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepAssetUserData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAssetUserData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepAssetUserData(UDataprepAssetUserData&&); \
	NO_API UDataprepAssetUserData(const UDataprepAssetUserData&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepAssetUserData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepAssetUserData(UDataprepAssetUserData&&); \
	NO_API UDataprepAssetUserData(const UDataprepAssetUserData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepAssetUserData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAssetUserData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepAssetUserData)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_14_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepAssetUserData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAssetUserData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
