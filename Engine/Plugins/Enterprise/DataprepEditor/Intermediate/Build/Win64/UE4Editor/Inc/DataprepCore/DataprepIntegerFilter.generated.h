// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepIntegerFilter_generated_h
#error "DataprepIntegerFilter.generated.h already included, missing '#pragma once' in DataprepIntegerFilter.h"
#endif
#define DATAPREPCORE_DataprepIntegerFilter_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepIntegerFilter(); \
	friend struct Z_Construct_UClass_UDataprepIntegerFilter_Statics; \
public: \
	DECLARE_CLASS(UDataprepIntegerFilter, UDataprepFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepIntegerFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepIntegerFilter(); \
	friend struct Z_Construct_UClass_UDataprepIntegerFilter_Statics; \
public: \
	DECLARE_CLASS(UDataprepIntegerFilter, UDataprepFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepIntegerFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepIntegerFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepIntegerFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepIntegerFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepIntegerFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepIntegerFilter(UDataprepIntegerFilter&&); \
	NO_API UDataprepIntegerFilter(const UDataprepIntegerFilter&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepIntegerFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepIntegerFilter(UDataprepIntegerFilter&&); \
	NO_API UDataprepIntegerFilter(const UDataprepIntegerFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepIntegerFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepIntegerFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepIntegerFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__IntFetcher() { return STRUCT_OFFSET(UDataprepIntegerFilter, IntFetcher); } \
	FORCEINLINE static uint32 __PPO__IntegerMatchingCriteria() { return STRUCT_OFFSET(UDataprepIntegerFilter, IntegerMatchingCriteria); } \
	FORCEINLINE static uint32 __PPO__EqualValue() { return STRUCT_OFFSET(UDataprepIntegerFilter, EqualValue); } \
	FORCEINLINE static uint32 __PPO__FromValue() { return STRUCT_OFFSET(UDataprepIntegerFilter, FromValue); } \
	FORCEINLINE static uint32 __PPO__ToValue() { return STRUCT_OFFSET(UDataprepIntegerFilter, ToValue); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_24_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepIntegerFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepIntegerFilter_h


#define FOREACH_ENUM_EDATAPREPINTEGERMATCHTYPE(op) \
	op(EDataprepIntegerMatchType::LessThan) \
	op(EDataprepIntegerMatchType::GreatherThan) \
	op(EDataprepIntegerMatchType::IsEqual) \
	op(EDataprepIntegerMatchType::InBetween) 

enum class EDataprepIntegerMatchType : uint8;
template<> DATAPREPCORE_API UEnum* StaticEnum<EDataprepIntegerMatchType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
