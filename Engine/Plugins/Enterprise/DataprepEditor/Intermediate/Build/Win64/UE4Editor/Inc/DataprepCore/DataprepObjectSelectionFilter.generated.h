// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepObjectSelectionFilter_generated_h
#error "DataprepObjectSelectionFilter.generated.h already included, missing '#pragma once' in DataprepObjectSelectionFilter.h"
#endif
#define DATAPREPCORE_DataprepObjectSelectionFilter_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepObjectSelectionFilter(); \
	friend struct Z_Construct_UClass_UDataprepObjectSelectionFilter_Statics; \
public: \
	DECLARE_CLASS(UDataprepObjectSelectionFilter, UDataprepFilterNoFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepObjectSelectionFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepObjectSelectionFilter(); \
	friend struct Z_Construct_UClass_UDataprepObjectSelectionFilter_Statics; \
public: \
	DECLARE_CLASS(UDataprepObjectSelectionFilter, UDataprepFilterNoFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepObjectSelectionFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepObjectSelectionFilter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepObjectSelectionFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepObjectSelectionFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepObjectSelectionFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepObjectSelectionFilter(UDataprepObjectSelectionFilter&&); \
	NO_API UDataprepObjectSelectionFilter(const UDataprepObjectSelectionFilter&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepObjectSelectionFilter(UDataprepObjectSelectionFilter&&); \
	NO_API UDataprepObjectSelectionFilter(const UDataprepObjectSelectionFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepObjectSelectionFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepObjectSelectionFilter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepObjectSelectionFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SelectedObjectPaths() { return STRUCT_OFFSET(UDataprepObjectSelectionFilter, SelectedObjectPaths); } \
	FORCEINLINE static uint32 __PPO__NumAssets() { return STRUCT_OFFSET(UDataprepObjectSelectionFilter, NumAssets); } \
	FORCEINLINE static uint32 __PPO__NumActors() { return STRUCT_OFFSET(UDataprepObjectSelectionFilter, NumActors); } \
	FORCEINLINE static uint32 __PPO__CachedNames() { return STRUCT_OFFSET(UDataprepObjectSelectionFilter, CachedNames); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_13_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepObjectSelectionFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepObjectSelectionFilter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
