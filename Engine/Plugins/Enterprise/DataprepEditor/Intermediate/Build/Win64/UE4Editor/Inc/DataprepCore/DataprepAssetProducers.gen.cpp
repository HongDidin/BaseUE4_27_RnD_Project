// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/DataprepAssetProducers.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepAssetProducers() {}
// Cross Module References
	DATAPREPCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDataprepAssetProducer();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepContentProducer_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetProducers_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetProducers();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FDataprepAssetProducer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAPREPCORE_API uint32 Get_Z_Construct_UScriptStruct_FDataprepAssetProducer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataprepAssetProducer, Z_Construct_UPackage__Script_DataprepCore(), TEXT("DataprepAssetProducer"), sizeof(FDataprepAssetProducer), Get_Z_Construct_UScriptStruct_FDataprepAssetProducer_Hash());
	}
	return Singleton;
}
template<> DATAPREPCORE_API UScriptStruct* StaticStruct<FDataprepAssetProducer>()
{
	return FDataprepAssetProducer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataprepAssetProducer(FDataprepAssetProducer::StaticStruct, TEXT("/Script/DataprepCore"), TEXT("DataprepAssetProducer"), false, nullptr, nullptr);
static struct FScriptStruct_DataprepCore_StaticRegisterNativesFDataprepAssetProducer
{
	FScriptStruct_DataprepCore_StaticRegisterNativesFDataprepAssetProducer()
	{
		UScriptStruct::DeferCppStructOps<FDataprepAssetProducer>(FName(TEXT("DataprepAssetProducer")));
	}
} ScriptStruct_DataprepCore_StaticRegisterNativesFDataprepAssetProducer;
	struct Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Producer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Producer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SupersededBy_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SupersededBy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Structure to hold on a producer and its configuration */" },
		{ "ModuleRelativePath", "Public/DataprepAssetProducers.h" },
		{ "ToolTip", "Structure to hold on a producer and its configuration" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataprepAssetProducer>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_Producer_MetaData[] = {
		{ "ModuleRelativePath", "Public/DataprepAssetProducers.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_Producer = { "Producer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataprepAssetProducer, Producer), Z_Construct_UClass_UDataprepContentProducer_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_Producer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_Producer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "ModuleRelativePath", "Public/DataprepAssetProducers.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((FDataprepAssetProducer*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDataprepAssetProducer), &Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_SupersededBy_MetaData[] = {
		{ "ModuleRelativePath", "Public/DataprepAssetProducers.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_SupersededBy = { "SupersededBy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataprepAssetProducer, SupersededBy), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_SupersededBy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_SupersededBy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_Producer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::NewProp_SupersededBy,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
		nullptr,
		&NewStructOps,
		"DataprepAssetProducer",
		sizeof(FDataprepAssetProducer),
		alignof(FDataprepAssetProducer),
		Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataprepAssetProducer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataprepAssetProducer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataprepAssetProducer"), sizeof(FDataprepAssetProducer), Get_Z_Construct_UScriptStruct_FDataprepAssetProducer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataprepAssetProducer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataprepAssetProducer_Hash() { return 4208413550U; }
	void UDataprepAssetProducers::StaticRegisterNativesUDataprepAssetProducers()
	{
	}
	UClass* Z_Construct_UClass_UDataprepAssetProducers_NoRegister()
	{
		return UDataprepAssetProducers::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepAssetProducers_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AssetProducers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetProducers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AssetProducers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Padding_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Padding;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepAssetProducers_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetProducers_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A UDataprepAssetProducers is a utility class to manage the set of producers constituting\n * the inputs of a DataprepAssetInterface. It provides a set of methods to edit the set of\n * producers and their respective configuration.\n */" },
		{ "IncludePath", "DataprepAssetProducers.h" },
		{ "ModuleRelativePath", "Public/DataprepAssetProducers.h" },
		{ "ToolTip", "A UDataprepAssetProducers is a utility class to manage the set of producers constituting\nthe inputs of a DataprepAssetInterface. It provides a set of methods to edit the set of\nproducers and their respective configuration." },
	};
#endif
#if WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_AssetProducers_Inner = { "AssetProducers", nullptr, (EPropertyFlags)0x0000000800000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDataprepAssetProducer, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_AssetProducers_MetaData[] = {
		{ "Comment", "/** List of producers referenced by the asset */" },
		{ "ModuleRelativePath", "Public/DataprepAssetProducers.h" },
		{ "ToolTip", "List of producers referenced by the asset" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_AssetProducers = { "AssetProducers", nullptr, (EPropertyFlags)0x0020080800000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetProducers, AssetProducers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_AssetProducers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_AssetProducers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_Padding_MetaData[] = {
		{ "Comment", "/** List of producers referenced by the asset */" },
		{ "ModuleRelativePath", "Public/DataprepAssetProducers.h" },
		{ "ToolTip", "List of producers referenced by the asset" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_Padding = { "Padding", nullptr, (EPropertyFlags)0x0020080800000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetProducers, Padding), METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_Padding_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_Padding_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepAssetProducers_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_AssetProducers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_AssetProducers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetProducers_Statics::NewProp_Padding,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepAssetProducers_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepAssetProducers>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepAssetProducers_Statics::ClassParams = {
		&UDataprepAssetProducers::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UDataprepAssetProducers_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetProducers_Statics::PropPointers), 0),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetProducers_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetProducers_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepAssetProducers()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepAssetProducers_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepAssetProducers, 3351125233);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepAssetProducers>()
	{
		return UDataprepAssetProducers::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepAssetProducers(Z_Construct_UClass_UDataprepAssetProducers, &UDataprepAssetProducers::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepAssetProducers"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepAssetProducers);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDataprepAssetProducers)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
