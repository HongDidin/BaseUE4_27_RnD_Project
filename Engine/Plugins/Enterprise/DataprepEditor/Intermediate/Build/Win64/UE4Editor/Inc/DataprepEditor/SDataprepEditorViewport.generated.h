// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPEDITOR_SDataprepEditorViewport_generated_h
#error "SDataprepEditorViewport.generated.h already included, missing '#pragma once' in SDataprepEditorViewport.h"
#endif
#define DATAPREPEDITOR_SDataprepEditorViewport_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCustomStaticMeshComponent(); \
	friend struct Z_Construct_UClass_UCustomStaticMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UCustomStaticMeshComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DataprepEditor"), NO_API) \
	DECLARE_SERIALIZER(UCustomStaticMeshComponent)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_INCLASS \
private: \
	static void StaticRegisterNativesUCustomStaticMeshComponent(); \
	friend struct Z_Construct_UClass_UCustomStaticMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UCustomStaticMeshComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DataprepEditor"), NO_API) \
	DECLARE_SERIALIZER(UCustomStaticMeshComponent)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCustomStaticMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCustomStaticMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCustomStaticMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCustomStaticMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCustomStaticMeshComponent(UCustomStaticMeshComponent&&); \
	NO_API UCustomStaticMeshComponent(const UCustomStaticMeshComponent&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCustomStaticMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCustomStaticMeshComponent(UCustomStaticMeshComponent&&); \
	NO_API UCustomStaticMeshComponent(const UCustomStaticMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCustomStaticMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCustomStaticMeshComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCustomStaticMeshComponent)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_39_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h_42_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPEDITOR_API UClass* StaticClass<class UCustomStaticMeshComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_Widgets_SDataprepEditorViewport_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
