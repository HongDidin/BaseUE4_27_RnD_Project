// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/SelectionSystem/DataprepFetcher.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepFetcher() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepFetcher_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepFetcher();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizableObject();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
// End Cross Module References
	DEFINE_FUNCTION(UDataprepFetcher::execGetAdditionalKeyword)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=P_THIS->GetAdditionalKeyword_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepFetcher::execGetTooltipText)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=P_THIS->GetTooltipText_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepFetcher::execGetNodeDisplayFetcherName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=P_THIS->GetNodeDisplayFetcherName_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepFetcher::execGetDisplayFetcherName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=P_THIS->GetDisplayFetcherName_Implementation();
		P_NATIVE_END;
	}
	static FName NAME_UDataprepFetcher_GetAdditionalKeyword = FName(TEXT("GetAdditionalKeyword"));
	FText UDataprepFetcher::GetAdditionalKeyword() const
	{
		DataprepFetcher_eventGetAdditionalKeyword_Parms Parms;
		const_cast<UDataprepFetcher*>(this)->ProcessEvent(FindFunctionChecked(NAME_UDataprepFetcher_GetAdditionalKeyword),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UDataprepFetcher_GetDisplayFetcherName = FName(TEXT("GetDisplayFetcherName"));
	FText UDataprepFetcher::GetDisplayFetcherName() const
	{
		DataprepFetcher_eventGetDisplayFetcherName_Parms Parms;
		const_cast<UDataprepFetcher*>(this)->ProcessEvent(FindFunctionChecked(NAME_UDataprepFetcher_GetDisplayFetcherName),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UDataprepFetcher_GetNodeDisplayFetcherName = FName(TEXT("GetNodeDisplayFetcherName"));
	FText UDataprepFetcher::GetNodeDisplayFetcherName() const
	{
		DataprepFetcher_eventGetNodeDisplayFetcherName_Parms Parms;
		const_cast<UDataprepFetcher*>(this)->ProcessEvent(FindFunctionChecked(NAME_UDataprepFetcher_GetNodeDisplayFetcherName),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UDataprepFetcher_GetTooltipText = FName(TEXT("GetTooltipText"));
	FText UDataprepFetcher::GetTooltipText() const
	{
		DataprepFetcher_eventGetTooltipText_Parms Parms;
		const_cast<UDataprepFetcher*>(this)->ProcessEvent(FindFunctionChecked(NAME_UDataprepFetcher_GetTooltipText),&Parms);
		return Parms.ReturnValue;
	}
	void UDataprepFetcher::StaticRegisterNativesUDataprepFetcher()
	{
		UClass* Class = UDataprepFetcher::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetAdditionalKeyword", &UDataprepFetcher::execGetAdditionalKeyword },
			{ "GetDisplayFetcherName", &UDataprepFetcher::execGetDisplayFetcherName },
			{ "GetNodeDisplayFetcherName", &UDataprepFetcher::execGetNodeDisplayFetcherName },
			{ "GetTooltipText", &UDataprepFetcher::execGetTooltipText },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics
	{
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFetcher_eventGetAdditionalKeyword_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics::Function_MetaDataParams[] = {
		{ "Category", "Display|Search" },
		{ "Comment", "/**\n\x09 * Allows to add more keywords for when a user is searching for the fetcher in the ui.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFetcher.h" },
		{ "ToolTip", "Allows to add more keywords for when a user is searching for the fetcher in the ui." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepFetcher, nullptr, "GetAdditionalKeyword", nullptr, nullptr, sizeof(DataprepFetcher_eventGetAdditionalKeyword_Parms), Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics
	{
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFetcher_eventGetDisplayFetcherName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Display" },
		{ "Comment", "/** \n\x09 * Allows to change the name of the fetcher for the ui if needed.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFetcher.h" },
		{ "ToolTip", "Allows to change the name of the fetcher for the ui if needed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepFetcher, nullptr, "GetDisplayFetcherName", nullptr, nullptr, sizeof(DataprepFetcher_eventGetDisplayFetcherName_Parms), Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics
	{
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFetcher_eventGetNodeDisplayFetcherName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Display" },
		{ "Comment", "/**\n\x09 * The name displayed on node title.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFetcher.h" },
		{ "ToolTip", "The name displayed on node title." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepFetcher, nullptr, "GetNodeDisplayFetcherName", nullptr, nullptr, sizeof(DataprepFetcher_eventGetNodeDisplayFetcherName_Parms), Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics
	{
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFetcher_eventGetTooltipText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics::Function_MetaDataParams[] = {
		{ "Category", "Display" },
		{ "Comment", "/**\n\x09 * Allows to change the tooltip of the fetcher for the ui if needed.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFetcher.h" },
		{ "ToolTip", "Allows to change the tooltip of the fetcher for the ui if needed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepFetcher, nullptr, "GetTooltipText", nullptr, nullptr, sizeof(DataprepFetcher_eventGetTooltipText_Parms), Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepFetcher_GetTooltipText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepFetcher_GetTooltipText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDataprepFetcher_NoRegister()
	{
		return UDataprepFetcher::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepFetcher_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepFetcher_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepParameterizableObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDataprepFetcher_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDataprepFetcher_GetAdditionalKeyword, "GetAdditionalKeyword" }, // 3221292218
		{ &Z_Construct_UFunction_UDataprepFetcher_GetDisplayFetcherName, "GetDisplayFetcherName" }, // 2538360130
		{ &Z_Construct_UFunction_UDataprepFetcher_GetNodeDisplayFetcherName, "GetNodeDisplayFetcherName" }, // 3774295190
		{ &Z_Construct_UFunction_UDataprepFetcher_GetTooltipText, "GetTooltipText" }, // 2453704940
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepFetcher_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * The Dataprep fetcher is a base class for Dataprep Selection system.\n * The responsibility of fetcher is return a certain type of data for a object. (Look at DataprepStringFetcher for a example)\n * This abstract base class exist currently for the discovery process, some compile time validation and to propagate UDataprepParameterizableObject\n */" },
		{ "IncludePath", "SelectionSystem/DataprepFetcher.h" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepFetcher.h" },
		{ "ToolTip", "The Dataprep fetcher is a base class for Dataprep Selection system.\nThe responsibility of fetcher is return a certain type of data for a object. (Look at DataprepStringFetcher for a example)\nThis abstract base class exist currently for the discovery process, some compile time validation and to propagate UDataprepParameterizableObject" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepFetcher_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepFetcher>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepFetcher_Statics::ClassParams = {
		&UDataprepFetcher::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepFetcher_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepFetcher_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepFetcher()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepFetcher_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepFetcher, 98577071);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepFetcher>()
	{
		return UDataprepFetcher::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepFetcher(Z_Construct_UClass_UDataprepFetcher, &UDataprepFetcher::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepFetcher"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepFetcher);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
