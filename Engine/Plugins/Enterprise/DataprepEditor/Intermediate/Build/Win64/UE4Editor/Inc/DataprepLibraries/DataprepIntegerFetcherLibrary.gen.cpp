// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepLibraries/Public/Fetchers/DataprepIntegerFetcherLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepIntegerFetcherLibrary() {}
// Cross Module References
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepTriangleCountFetcher_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepTriangleCountFetcher();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepIntegerFetcher();
	UPackage* Z_Construct_UPackage__Script_DataprepLibraries();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepVertexCountFetcher_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepVertexCountFetcher();
// End Cross Module References
	void UDataprepTriangleCountFetcher::StaticRegisterNativesUDataprepTriangleCountFetcher()
	{
	}
	UClass* Z_Construct_UClass_UDataprepTriangleCountFetcher_NoRegister()
	{
		return UDataprepTriangleCountFetcher::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepTriangleCountFetcher_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepTriangleCountFetcher_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepIntegerFetcher,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepTriangleCountFetcher_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Triangle Count" },
		{ "IncludePath", "Fetchers/DataprepIntegerFetcherLibrary.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/Fetchers/DataprepIntegerFetcherLibrary.h" },
		{ "ToolTip", "Filter objects based on their triangle count." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepTriangleCountFetcher_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepTriangleCountFetcher>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepTriangleCountFetcher_Statics::ClassParams = {
		&UDataprepTriangleCountFetcher::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepTriangleCountFetcher_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepTriangleCountFetcher_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepTriangleCountFetcher()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepTriangleCountFetcher_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepTriangleCountFetcher, 2035338483);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepTriangleCountFetcher>()
	{
		return UDataprepTriangleCountFetcher::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepTriangleCountFetcher(Z_Construct_UClass_UDataprepTriangleCountFetcher, &UDataprepTriangleCountFetcher::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepTriangleCountFetcher"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepTriangleCountFetcher);
	void UDataprepVertexCountFetcher::StaticRegisterNativesUDataprepVertexCountFetcher()
	{
	}
	UClass* Z_Construct_UClass_UDataprepVertexCountFetcher_NoRegister()
	{
		return UDataprepVertexCountFetcher::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepVertexCountFetcher_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepVertexCountFetcher_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepIntegerFetcher,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepVertexCountFetcher_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Vertex Count" },
		{ "IncludePath", "Fetchers/DataprepIntegerFetcherLibrary.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/Fetchers/DataprepIntegerFetcherLibrary.h" },
		{ "ToolTip", "Filter objects based on their vertex count." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepVertexCountFetcher_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepVertexCountFetcher>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepVertexCountFetcher_Statics::ClassParams = {
		&UDataprepVertexCountFetcher::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepVertexCountFetcher_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepVertexCountFetcher_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepVertexCountFetcher()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepVertexCountFetcher_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepVertexCountFetcher, 3113842710);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepVertexCountFetcher>()
	{
		return UDataprepVertexCountFetcher::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepVertexCountFetcher(Z_Construct_UClass_UDataprepVertexCountFetcher, &UDataprepVertexCountFetcher::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepVertexCountFetcher"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepVertexCountFetcher);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
