// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/DataprepParameterizableObject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepParameterizableObject() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizableObject_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizableObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
// End Cross Module References
	void UDataprepParameterizableObject::StaticRegisterNativesUDataprepParameterizableObject()
	{
	}
	UClass* Z_Construct_UClass_UDataprepParameterizableObject_NoRegister()
	{
		return UDataprepParameterizableObject::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepParameterizableObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepParameterizableObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepParameterizableObject_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * The base class of all the object that can interact with the dataprep parameterization\n * This include all the parameterizable object and the parameterization object itself\n * Also all the object that can be place in a dataprep action derive from it\n */" },
		{ "IncludePath", "DataprepParameterizableObject.h" },
		{ "ModuleRelativePath", "Public/DataprepParameterizableObject.h" },
		{ "ToolTip", "The base class of all the object that can interact with the dataprep parameterization\nThis include all the parameterizable object and the parameterization object itself\nAlso all the object that can be place in a dataprep action derive from it" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepParameterizableObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepParameterizableObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepParameterizableObject_Statics::ClassParams = {
		&UDataprepParameterizableObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepParameterizableObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepParameterizableObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepParameterizableObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepParameterizableObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepParameterizableObject, 342260833);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepParameterizableObject>()
	{
		return UDataprepParameterizableObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepParameterizableObject(Z_Construct_UClass_UDataprepParameterizableObject, &UDataprepParameterizableObject::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepParameterizableObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepParameterizableObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
