// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/Parameterization/DataprepParameterizationUtils.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepParameterizationUtils() {}
// Cross Module References
	DATAPREPCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDataprepPropertyLink();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
// End Cross Module References
class UScriptStruct* FDataprepPropertyLink::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAPREPCORE_API uint32 Get_Z_Construct_UScriptStruct_FDataprepPropertyLink_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataprepPropertyLink, Z_Construct_UPackage__Script_DataprepCore(), TEXT("DataprepPropertyLink"), sizeof(FDataprepPropertyLink), Get_Z_Construct_UScriptStruct_FDataprepPropertyLink_Hash());
	}
	return Singleton;
}
template<> DATAPREPCORE_API UScriptStruct* StaticStruct<FDataprepPropertyLink>()
{
	return FDataprepPropertyLink::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataprepPropertyLink(FDataprepPropertyLink::StaticStruct, TEXT("/Script/DataprepCore"), TEXT("DataprepPropertyLink"), false, nullptr, nullptr);
static struct FScriptStruct_DataprepCore_StaticRegisterNativesFDataprepPropertyLink
{
	FScriptStruct_DataprepCore_StaticRegisterNativesFDataprepPropertyLink()
	{
		UScriptStruct::DeferCppStructOps<FDataprepPropertyLink>(FName(TEXT("DataprepPropertyLink")));
	}
} ScriptStruct_DataprepCore_StaticRegisterNativesFDataprepPropertyLink;
	struct Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FFieldPathPropertyParams NewProp_CachedProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PropertyName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ContainerIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ContainerIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Parameterization/DataprepParameterizationUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataprepPropertyLink>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_CachedProperty_MetaData[] = {
		{ "ModuleRelativePath", "Public/Parameterization/DataprepParameterizationUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFieldPathPropertyParams Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_CachedProperty = { "CachedProperty", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::FieldPath, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataprepPropertyLink, CachedProperty), &FProperty::StaticClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_CachedProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_CachedProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_PropertyName_MetaData[] = {
		{ "ModuleRelativePath", "Public/Parameterization/DataprepParameterizationUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_PropertyName = { "PropertyName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataprepPropertyLink, PropertyName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_PropertyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_PropertyName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_ContainerIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/Parameterization/DataprepParameterizationUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_ContainerIndex = { "ContainerIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataprepPropertyLink, ContainerIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_ContainerIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_ContainerIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_CachedProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_PropertyName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::NewProp_ContainerIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
		nullptr,
		&NewStructOps,
		"DataprepPropertyLink",
		sizeof(FDataprepPropertyLink),
		alignof(FDataprepPropertyLink),
		Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataprepPropertyLink()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataprepPropertyLink_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataprepPropertyLink"), sizeof(FDataprepPropertyLink), Get_Z_Construct_UScriptStruct_FDataprepPropertyLink_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataprepPropertyLink_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataprepPropertyLink_Hash() { return 1455741171U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
