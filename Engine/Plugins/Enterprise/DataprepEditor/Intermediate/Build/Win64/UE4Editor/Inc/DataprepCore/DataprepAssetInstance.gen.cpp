// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/DataprepAssetInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepAssetInstance() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInstance_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInstance();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInterface();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInterface_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizationInstance_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepActionAsset_NoRegister();
// End Cross Module References
	void UDataprepAssetInstance::StaticRegisterNativesUDataprepAssetInstance()
	{
	}
	UClass* Z_Construct_UClass_UDataprepAssetInstance_NoRegister()
	{
		return UDataprepAssetInstance::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepAssetInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Parent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameterization_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Parameterization;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActionsFromDataprepAsset_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActionsFromDataprepAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActionsFromDataprepAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepAssetInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepAssetInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetInstance_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A DataprepAssetInstance is an implementation of the DataprepAssetInterface sharing\n * its recipe pipeline with an existing Dataprep asset or another DataprepAssetInstance.\n * It has its own inputs and output. It has overrides of the externalized parameters\n * of the pipeline.\n */" },
		{ "IncludePath", "DataprepAssetInstance.h" },
		{ "ModuleRelativePath", "Public/DataprepAssetInstance.h" },
		{ "ToolTip", "A DataprepAssetInstance is an implementation of the DataprepAssetInterface sharing\nits recipe pipeline with an existing Dataprep asset or another DataprepAssetInstance.\nIt has its own inputs and output. It has overrides of the externalized parameters\nof the pipeline." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_Parent_MetaData[] = {
		{ "Comment", "/** Parent Dataprep asset's interface */" },
		{ "ModuleRelativePath", "Public/DataprepAssetInstance.h" },
		{ "ToolTip", "Parent Dataprep asset's interface" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_Parent = { "Parent", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetInstance, Parent), Z_Construct_UClass_UDataprepAssetInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_Parent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_Parent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_Parameterization_MetaData[] = {
		{ "ModuleRelativePath", "Public/DataprepAssetInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_Parameterization = { "Parameterization", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetInstance, Parameterization), Z_Construct_UClass_UDataprepParameterizationInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_Parameterization_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_Parameterization_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_ActionsFromDataprepAsset_Inner = { "ActionsFromDataprepAsset", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDataprepActionAsset_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_ActionsFromDataprepAsset_MetaData[] = {
		{ "ModuleRelativePath", "Public/DataprepAssetInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_ActionsFromDataprepAsset = { "ActionsFromDataprepAsset", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAssetInstance, ActionsFromDataprepAsset), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_ActionsFromDataprepAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_ActionsFromDataprepAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepAssetInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_Parent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_Parameterization,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_ActionsFromDataprepAsset_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAssetInstance_Statics::NewProp_ActionsFromDataprepAsset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepAssetInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepAssetInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepAssetInstance_Statics::ClassParams = {
		&UDataprepAssetInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepAssetInstance_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInstance_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepAssetInstance_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAssetInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepAssetInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepAssetInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepAssetInstance, 1614467615);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepAssetInstance>()
	{
		return UDataprepAssetInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepAssetInstance(Z_Construct_UClass_UDataprepAssetInstance, &UDataprepAssetInstance::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepAssetInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepAssetInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
