// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Private/Blueprint/K2Node_DataprepProducer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_DataprepProducer() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UK2Node_DataprepProducer_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UK2Node_DataprepProducer();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
// End Cross Module References
	void UK2Node_DataprepProducer::StaticRegisterNativesUK2Node_DataprepProducer()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_DataprepProducer_NoRegister()
	{
		return UK2Node_DataprepProducer::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_DataprepProducer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_DataprepProducer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_DataprepProducer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Blueprint/K2Node_DataprepProducer.h" },
		{ "ModuleRelativePath", "Private/Blueprint/K2Node_DataprepProducer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_DataprepProducer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_DataprepProducer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_DataprepProducer_Statics::ClassParams = {
		&UK2Node_DataprepProducer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_DataprepProducer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_DataprepProducer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_DataprepProducer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_DataprepProducer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_DataprepProducer, 2751567971);
	template<> DATAPREPCORE_API UClass* StaticClass<UK2Node_DataprepProducer>()
	{
		return UK2Node_DataprepProducer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_DataprepProducer(Z_Construct_UClass_UK2Node_DataprepProducer, &UK2Node_DataprepProducer::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UK2Node_DataprepProducer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_DataprepProducer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
