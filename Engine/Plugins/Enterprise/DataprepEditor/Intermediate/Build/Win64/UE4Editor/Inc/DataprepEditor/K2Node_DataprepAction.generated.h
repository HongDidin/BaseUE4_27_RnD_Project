// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPEDITOR_K2Node_DataprepAction_generated_h
#error "K2Node_DataprepAction.generated.h already included, missing '#pragma once' in K2Node_DataprepAction.h"
#endif
#define DATAPREPEDITOR_K2Node_DataprepAction_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_K2Node_DataprepAction(); \
	friend struct Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_K2Node_DataprepAction, UK2Node_DataprepActionCore, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/DataprepEditor"), DATAPREPEDITOR_API) \
	DECLARE_SERIALIZER(UDEPRECATED_K2Node_DataprepAction)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_K2Node_DataprepAction(); \
	friend struct Z_Construct_UClass_UDEPRECATED_K2Node_DataprepAction_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_K2Node_DataprepAction, UK2Node_DataprepActionCore, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/DataprepEditor"), DATAPREPEDITOR_API) \
	DECLARE_SERIALIZER(UDEPRECATED_K2Node_DataprepAction)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DATAPREPEDITOR_API UDEPRECATED_K2Node_DataprepAction(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_K2Node_DataprepAction) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATAPREPEDITOR_API, UDEPRECATED_K2Node_DataprepAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_K2Node_DataprepAction); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATAPREPEDITOR_API UDEPRECATED_K2Node_DataprepAction(UDEPRECATED_K2Node_DataprepAction&&); \
	DATAPREPEDITOR_API UDEPRECATED_K2Node_DataprepAction(const UDEPRECATED_K2Node_DataprepAction&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATAPREPEDITOR_API UDEPRECATED_K2Node_DataprepAction(UDEPRECATED_K2Node_DataprepAction&&); \
	DATAPREPEDITOR_API UDEPRECATED_K2Node_DataprepAction(const UDEPRECATED_K2Node_DataprepAction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATAPREPEDITOR_API, UDEPRECATED_K2Node_DataprepAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_K2Node_DataprepAction); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDEPRECATED_K2Node_DataprepAction)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ActionTitle() { return STRUCT_OFFSET(UDEPRECATED_K2Node_DataprepAction, ActionTitle); } \
	FORCEINLINE static uint32 __PPO__DataprepAction_DEPRECATED() { return STRUCT_OFFSET(UDEPRECATED_K2Node_DataprepAction, DataprepAction_DEPRECATED); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_18_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPEDITOR_API UClass* StaticClass<class UDEPRECATED_K2Node_DataprepAction>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_BlueprintNodes_K2Node_DataprepAction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
