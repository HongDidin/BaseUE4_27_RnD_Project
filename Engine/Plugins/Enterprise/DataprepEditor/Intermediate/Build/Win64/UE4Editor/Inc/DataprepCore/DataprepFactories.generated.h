// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepFactories_generated_h
#error "DataprepFactories.generated.h already included, missing '#pragma once' in DataprepFactories.h"
#endif
#define DATAPREPCORE_DataprepFactories_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepAssetFactory(); \
	friend struct Z_Construct_UClass_UDataprepAssetFactory_Statics; \
public: \
	DECLARE_CLASS(UDataprepAssetFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), DATAPREPCORE_API) \
	DECLARE_SERIALIZER(UDataprepAssetFactory)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepAssetFactory(); \
	friend struct Z_Construct_UClass_UDataprepAssetFactory_Statics; \
public: \
	DECLARE_CLASS(UDataprepAssetFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), DATAPREPCORE_API) \
	DECLARE_SERIALIZER(UDataprepAssetFactory)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DATAPREPCORE_API UDataprepAssetFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepAssetFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATAPREPCORE_API, UDataprepAssetFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAssetFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATAPREPCORE_API UDataprepAssetFactory(UDataprepAssetFactory&&); \
	DATAPREPCORE_API UDataprepAssetFactory(const UDataprepAssetFactory&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATAPREPCORE_API UDataprepAssetFactory(UDataprepAssetFactory&&); \
	DATAPREPCORE_API UDataprepAssetFactory(const UDataprepAssetFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATAPREPCORE_API, UDataprepAssetFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAssetFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepAssetFactory)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_15_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepAssetFactory>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepAssetInstanceFactory(); \
	friend struct Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics; \
public: \
	DECLARE_CLASS(UDataprepAssetInstanceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), DATAPREPCORE_API) \
	DECLARE_SERIALIZER(UDataprepAssetInstanceFactory)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepAssetInstanceFactory(); \
	friend struct Z_Construct_UClass_UDataprepAssetInstanceFactory_Statics; \
public: \
	DECLARE_CLASS(UDataprepAssetInstanceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), DATAPREPCORE_API) \
	DECLARE_SERIALIZER(UDataprepAssetInstanceFactory)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DATAPREPCORE_API UDataprepAssetInstanceFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepAssetInstanceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATAPREPCORE_API, UDataprepAssetInstanceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAssetInstanceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATAPREPCORE_API UDataprepAssetInstanceFactory(UDataprepAssetInstanceFactory&&); \
	DATAPREPCORE_API UDataprepAssetInstanceFactory(const UDataprepAssetInstanceFactory&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATAPREPCORE_API UDataprepAssetInstanceFactory(UDataprepAssetInstanceFactory&&); \
	DATAPREPCORE_API UDataprepAssetInstanceFactory(const UDataprepAssetInstanceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATAPREPCORE_API, UDataprepAssetInstanceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAssetInstanceFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepAssetInstanceFactory)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_29_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepAssetInstanceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepFactories_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
