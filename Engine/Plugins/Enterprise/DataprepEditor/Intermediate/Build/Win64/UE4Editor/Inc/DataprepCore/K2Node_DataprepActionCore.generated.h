// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_K2Node_DataprepActionCore_generated_h
#error "K2Node_DataprepActionCore.generated.h already included, missing '#pragma once' in K2Node_DataprepActionCore.h"
#endif
#define DATAPREPCORE_K2Node_DataprepActionCore_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_DataprepActionCore(); \
	friend struct Z_Construct_UClass_UK2Node_DataprepActionCore_Statics; \
public: \
	DECLARE_CLASS(UK2Node_DataprepActionCore, UK2Node, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_DataprepActionCore)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_DataprepActionCore(); \
	friend struct Z_Construct_UClass_UK2Node_DataprepActionCore_Statics; \
public: \
	DECLARE_CLASS(UK2Node_DataprepActionCore, UK2Node, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_DataprepActionCore)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_DataprepActionCore(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_DataprepActionCore) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_DataprepActionCore); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_DataprepActionCore); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_DataprepActionCore(UK2Node_DataprepActionCore&&); \
	NO_API UK2Node_DataprepActionCore(const UK2Node_DataprepActionCore&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_DataprepActionCore(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_DataprepActionCore(UK2Node_DataprepActionCore&&); \
	NO_API UK2Node_DataprepActionCore(const UK2Node_DataprepActionCore&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_DataprepActionCore); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_DataprepActionCore); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_DataprepActionCore)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DataprepActionAsset() { return STRUCT_OFFSET(UK2Node_DataprepActionCore, DataprepActionAsset); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_15_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UK2Node_DataprepActionCore>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_Blueprint_K2Node_DataprepActionCore_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
