// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/SelectionSystem/DataprepStringFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepStringFilter() {}
// Cross Module References
	DATAPREPCORE_API UEnum* Z_Construct_UEnum_DataprepCore_EDataprepStringMatchType();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepStringFilterMatchingArray_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepStringFilterMatchingArray();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizableObject();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepStringFilter_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepStringFilter();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepFilter();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepStringFetcher_NoRegister();
// End Cross Module References
	static UEnum* EDataprepStringMatchType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DataprepCore_EDataprepStringMatchType, Z_Construct_UPackage__Script_DataprepCore(), TEXT("EDataprepStringMatchType"));
		}
		return Singleton;
	}
	template<> DATAPREPCORE_API UEnum* StaticEnum<EDataprepStringMatchType>()
	{
		return EDataprepStringMatchType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDataprepStringMatchType(EDataprepStringMatchType_StaticEnum, TEXT("/Script/DataprepCore"), TEXT("EDataprepStringMatchType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DataprepCore_EDataprepStringMatchType_Hash() { return 2617023887U; }
	UEnum* Z_Construct_UEnum_DataprepCore_EDataprepStringMatchType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDataprepStringMatchType"), 0, Get_Z_Construct_UEnum_DataprepCore_EDataprepStringMatchType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDataprepStringMatchType::Contains", (int64)EDataprepStringMatchType::Contains },
				{ "EDataprepStringMatchType::MatchesWildcard", (int64)EDataprepStringMatchType::MatchesWildcard },
				{ "EDataprepStringMatchType::ExactMatch", (int64)EDataprepStringMatchType::ExactMatch },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Contains.Name", "EDataprepStringMatchType::Contains" },
				{ "ExactMatch.Name", "EDataprepStringMatchType::ExactMatch" },
				{ "MatchesWildcard.Name", "EDataprepStringMatchType::MatchesWildcard" },
				{ "ModuleRelativePath", "Public/SelectionSystem/DataprepStringFilter.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DataprepCore,
				nullptr,
				"EDataprepStringMatchType",
				"EDataprepStringMatchType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDataprepStringFilterMatchingArray::StaticRegisterNativesUDataprepStringFilterMatchingArray()
	{
	}
	UClass* Z_Construct_UClass_UDataprepStringFilterMatchingArray_NoRegister()
	{
		return UDataprepStringFilterMatchingArray::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Strings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Strings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExpanded_MetaData[];
#endif
		static void NewProp_bExpanded_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExpanded;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepParameterizableObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SelectionSystem/DataprepStringFilter.h" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepStringFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_Strings_Inner = { "Strings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_Strings_MetaData[] = {
		{ "Category", "Filter" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepStringFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_Strings = { "Strings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepStringFilterMatchingArray, Strings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_Strings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_Strings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_bExpanded_MetaData[] = {
		{ "Comment", "// UI state that needs to be serialized along with string values\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepStringFilter.h" },
		{ "ToolTip", "UI state that needs to be serialized along with string values" },
	};
#endif
	void Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_bExpanded_SetBit(void* Obj)
	{
		((UDataprepStringFilterMatchingArray*)Obj)->bExpanded = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_bExpanded = { "bExpanded", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepStringFilterMatchingArray), &Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_bExpanded_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_bExpanded_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_bExpanded_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_Strings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_Strings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::NewProp_bExpanded,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepStringFilterMatchingArray>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::ClassParams = {
		&UDataprepStringFilterMatchingArray::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepStringFilterMatchingArray()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepStringFilterMatchingArray_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepStringFilterMatchingArray, 2274649933);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepStringFilterMatchingArray>()
	{
		return UDataprepStringFilterMatchingArray::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepStringFilterMatchingArray(Z_Construct_UClass_UDataprepStringFilterMatchingArray, &UDataprepStringFilterMatchingArray::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepStringFilterMatchingArray"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepStringFilterMatchingArray);
	void UDataprepStringFilter::StaticRegisterNativesUDataprepStringFilter()
	{
	}
	UClass* Z_Construct_UClass_UDataprepStringFilter_NoRegister()
	{
		return UDataprepStringFilter::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepStringFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_StringMatchingCriteria_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringMatchingCriteria_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_StringMatchingCriteria;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UserString;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserStringArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UserStringArray;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMatchInArray_MetaData[];
#endif
		static void NewProp_bMatchInArray_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMatchInArray;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringFetcher_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StringFetcher;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepStringFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepStringFilter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SelectionSystem/DataprepStringFilter.h" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepStringFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringMatchingCriteria_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringMatchingCriteria_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The matching criteria used when checking if a fetched value can pass the filter\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepStringFilter.h" },
		{ "ToolTip", "The matching criteria used when checking if a fetched value can pass the filter" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringMatchingCriteria = { "StringMatchingCriteria", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepStringFilter, StringMatchingCriteria), Z_Construct_UEnum_DataprepCore_EDataprepStringMatchType, METADATA_PARAMS(Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringMatchingCriteria_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringMatchingCriteria_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_UserString_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The string used when doing the comparison\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepStringFilter.h" },
		{ "ToolTip", "The string used when doing the comparison" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_UserString = { "UserString", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepStringFilter, UserString), METADATA_PARAMS(Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_UserString_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_UserString_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_UserStringArray_MetaData[] = {
		{ "Category", "Filter" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepStringFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_UserStringArray = { "UserStringArray", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepStringFilter, UserStringArray), Z_Construct_UClass_UDataprepStringFilterMatchingArray_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_UserStringArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_UserStringArray_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_bMatchInArray_MetaData[] = {
		{ "Category", "Filter" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepStringFilter.h" },
	};
#endif
	void Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_bMatchInArray_SetBit(void* Obj)
	{
		((UDataprepStringFilter*)Obj)->bMatchInArray = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_bMatchInArray = { "bMatchInArray", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepStringFilter), &Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_bMatchInArray_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_bMatchInArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_bMatchInArray_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringFetcher_MetaData[] = {
		{ "Comment", "// The source of string selected by the user\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepStringFilter.h" },
		{ "ToolTip", "The source of string selected by the user" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringFetcher = { "StringFetcher", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepStringFilter, StringFetcher), Z_Construct_UClass_UDataprepStringFetcher_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringFetcher_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringFetcher_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepStringFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringMatchingCriteria_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringMatchingCriteria,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_UserString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_UserStringArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_bMatchInArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepStringFilter_Statics::NewProp_StringFetcher,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepStringFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepStringFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepStringFilter_Statics::ClassParams = {
		&UDataprepStringFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepStringFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilter_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepStringFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepStringFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepStringFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepStringFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepStringFilter, 694623692);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepStringFilter>()
	{
		return UDataprepStringFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepStringFilter(Z_Construct_UClass_UDataprepStringFilter, &UDataprepStringFilter::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepStringFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepStringFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
