// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_K2Node_DataprepProducer_generated_h
#error "K2Node_DataprepProducer.generated.h already included, missing '#pragma once' in K2Node_DataprepProducer.h"
#endif
#define DATAPREPCORE_K2Node_DataprepProducer_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_DataprepProducer(); \
	friend struct Z_Construct_UClass_UK2Node_DataprepProducer_Statics; \
public: \
	DECLARE_CLASS(UK2Node_DataprepProducer, UK2Node, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_DataprepProducer)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_DataprepProducer(); \
	friend struct Z_Construct_UClass_UK2Node_DataprepProducer_Statics; \
public: \
	DECLARE_CLASS(UK2Node_DataprepProducer, UK2Node, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_DataprepProducer)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_DataprepProducer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_DataprepProducer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_DataprepProducer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_DataprepProducer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_DataprepProducer(UK2Node_DataprepProducer&&); \
	NO_API UK2Node_DataprepProducer(const UK2Node_DataprepProducer&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_DataprepProducer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_DataprepProducer(UK2Node_DataprepProducer&&); \
	NO_API UK2Node_DataprepProducer(const UK2Node_DataprepProducer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_DataprepProducer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_DataprepProducer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_DataprepProducer)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_13_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h_17_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UK2Node_DataprepProducer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Private_Blueprint_K2Node_DataprepProducer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
