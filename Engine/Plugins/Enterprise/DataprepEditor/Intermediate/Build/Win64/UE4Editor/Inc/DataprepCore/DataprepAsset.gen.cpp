// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/DataprepAsset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepAsset() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAsset_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAsset();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInterface();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprint_NoRegister();
	DATAPREPCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDataprepAssetProducer();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepContentConsumer_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphNode_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterization_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepActionAsset_NoRegister();
// End Cross Module References
	void UDataprepAsset::StaticRegisterNativesUDataprepAsset()
	{
	}
	UClass* Z_Construct_UClass_UDataprepAsset_NoRegister()
	{
		return UDataprepAsset::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataprepRecipeBP_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DataprepRecipeBP;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Producers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Producers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Producers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Consumer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Consumer;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StartNode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameterization_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Parameterization;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActionAssets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActionAssets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActionAssets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepAssetInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAsset_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A DataprepAsset is an implementation of the DataprepAssetInterface using\n * a Blueprint as the recipe pipeline. The Blueprint is composed of DataprepAction\n * nodes linearly connected.\n */" },
		{ "IncludePath", "DataprepAsset.h" },
		{ "ModuleRelativePath", "Public/DataprepAsset.h" },
		{ "ToolTip", "A DataprepAsset is an implementation of the DataprepAssetInterface using\na Blueprint as the recipe pipeline. The Blueprint is composed of DataprepAction\nnodes linearly connected." },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAsset_Statics::NewProp_DataprepRecipeBP_MetaData[] = {
		{ "Comment", "/** DEPRECATED: Pointer to data preparation pipeline blueprint previously used to process input data */" },
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Using directly ActionAssets property instead of Blueprint to manage actions." },
		{ "ModuleRelativePath", "Public/DataprepAsset.h" },
		{ "ToolTip", "DEPRECATED: Pointer to data preparation pipeline blueprint previously used to process input data" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAsset_Statics::NewProp_DataprepRecipeBP = { "DataprepRecipeBP", nullptr, (EPropertyFlags)0x0020080820000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAsset, DataprepRecipeBP_DEPRECATED), Z_Construct_UClass_UBlueprint_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_DataprepRecipeBP_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_DataprepRecipeBP_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Producers_Inner = { "Producers", nullptr, (EPropertyFlags)0x0000000820000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDataprepAssetProducer, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Producers_MetaData[] = {
		{ "Comment", "/** DEPRECATED: List of producers referenced by the asset */" },
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Property moved to UDataprepAssetInterface as Inputs." },
		{ "ModuleRelativePath", "Public/DataprepAsset.h" },
		{ "ToolTip", "DEPRECATED: List of producers referenced by the asset" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Producers = { "Producers", nullptr, (EPropertyFlags)0x0020080820000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAsset, Producers_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Producers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Producers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Consumer_MetaData[] = {
		{ "Comment", "/** DEPRECATED: COnsumer referenced by the asset */" },
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Property moved to UDataprepAssetInterface as Output." },
		{ "ModuleRelativePath", "Public/DataprepAsset.h" },
		{ "ToolTip", "DEPRECATED: COnsumer referenced by the asset" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Consumer = { "Consumer", nullptr, (EPropertyFlags)0x0020080820000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAsset, Consumer_DEPRECATED), Z_Construct_UClass_UDataprepContentConsumer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Consumer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Consumer_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAsset_Statics::NewProp_StartNode_MetaData[] = {
		{ "Comment", "/** DEPRECATED: Pointer to the entry node of the pipeline blueprint previously used to process input data */" },
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Using directly ActionAssets property instead of Blueprint to manage actions." },
		{ "ModuleRelativePath", "Public/DataprepAsset.h" },
		{ "ToolTip", "DEPRECATED: Pointer to the entry node of the pipeline blueprint previously used to process input data" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAsset_Statics::NewProp_StartNode = { "StartNode", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAsset, StartNode_DEPRECATED), Z_Construct_UClass_UEdGraphNode_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_StartNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_StartNode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Parameterization_MetaData[] = {
		{ "ModuleRelativePath", "Public/DataprepAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Parameterization = { "Parameterization", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAsset, Parameterization), Z_Construct_UClass_UDataprepParameterization_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Parameterization_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Parameterization_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepAsset_Statics::NewProp_ActionAssets_Inner = { "ActionAssets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDataprepActionAsset_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepAsset_Statics::NewProp_ActionAssets_MetaData[] = {
		{ "ModuleRelativePath", "Public/DataprepAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepAsset_Statics::NewProp_ActionAssets = { "ActionAssets", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepAsset, ActionAssets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_ActionAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAsset_Statics::NewProp_ActionAssets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepAsset_Statics::PropPointers[] = {
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAsset_Statics::NewProp_DataprepRecipeBP,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Producers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Producers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Consumer,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAsset_Statics::NewProp_StartNode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAsset_Statics::NewProp_Parameterization,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAsset_Statics::NewProp_ActionAssets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepAsset_Statics::NewProp_ActionAssets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepAsset_Statics::ClassParams = {
		&UDataprepAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepAsset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAsset_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepAsset, 3620366666);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepAsset>()
	{
		return UDataprepAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepAsset(Z_Construct_UClass_UDataprepAsset, &UDataprepAsset::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepAsset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
