// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepLibraries/Private/DataprepSelectionTransforms.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepSelectionTransforms() {}
// Cross Module References
	DATAPREPLIBRARIES_API UEnum* Z_Construct_UEnum_DataprepLibraries_EDataprepHierarchySelectionPolicy();
	UPackage* Z_Construct_UPackage__Script_DataprepLibraries();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepReferenceSelectionTransform_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepReferenceSelectionTransform();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepSelectionTransform();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepReferencedSelectionTransform_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepReferencedSelectionTransform();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepHierarchySelectionTransform_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepHierarchySelectionTransform();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepActorComponentsSelectionTransform();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepOwningActorSelectionTransform_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepOwningActorSelectionTransform();
// End Cross Module References
	static UEnum* EDataprepHierarchySelectionPolicy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DataprepLibraries_EDataprepHierarchySelectionPolicy, Z_Construct_UPackage__Script_DataprepLibraries(), TEXT("EDataprepHierarchySelectionPolicy"));
		}
		return Singleton;
	}
	template<> DATAPREPLIBRARIES_API UEnum* StaticEnum<EDataprepHierarchySelectionPolicy>()
	{
		return EDataprepHierarchySelectionPolicy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDataprepHierarchySelectionPolicy(EDataprepHierarchySelectionPolicy_StaticEnum, TEXT("/Script/DataprepLibraries"), TEXT("EDataprepHierarchySelectionPolicy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DataprepLibraries_EDataprepHierarchySelectionPolicy_Hash() { return 3784199031U; }
	UEnum* Z_Construct_UEnum_DataprepLibraries_EDataprepHierarchySelectionPolicy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepLibraries();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDataprepHierarchySelectionPolicy"), 0, Get_Z_Construct_UEnum_DataprepLibraries_EDataprepHierarchySelectionPolicy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDataprepHierarchySelectionPolicy::ImmediateChildren", (int64)EDataprepHierarchySelectionPolicy::ImmediateChildren },
				{ "EDataprepHierarchySelectionPolicy::AllDescendants", (int64)EDataprepHierarchySelectionPolicy::AllDescendants },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AllDescendants.Comment", "/** Select all descendants of the selected objects */" },
				{ "AllDescendants.Name", "EDataprepHierarchySelectionPolicy::AllDescendants" },
				{ "AllDescendants.ToolTip", "Select all descendants of the selected objects" },
				{ "ImmediateChildren.Comment", "/** Select immediate children of the selected objects */" },
				{ "ImmediateChildren.Name", "EDataprepHierarchySelectionPolicy::ImmediateChildren" },
				{ "ImmediateChildren.ToolTip", "Select immediate children of the selected objects" },
				{ "ModuleRelativePath", "Private/DataprepSelectionTransforms.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DataprepLibraries,
				nullptr,
				"EDataprepHierarchySelectionPolicy",
				"EDataprepHierarchySelectionPolicy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDataprepReferenceSelectionTransform::StaticRegisterNativesUDataprepReferenceSelectionTransform()
	{
	}
	UClass* Z_Construct_UClass_UDataprepReferenceSelectionTransform_NoRegister()
	{
		return UDataprepReferenceSelectionTransform::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowIndirectReferences_MetaData[];
#endif
		static void NewProp_bAllowIndirectReferences_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowIndirectReferences;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepSelectionTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::Class_MetaDataParams[] = {
		{ "Category", "SelectionTransform" },
		{ "DisplayName", "Select Referenced" },
		{ "IncludePath", "DataprepSelectionTransforms.h" },
		{ "ModuleRelativePath", "Private/DataprepSelectionTransforms.h" },
		{ "ToolTip", "Return assets directly used/referenced by the selected objects." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::NewProp_bAllowIndirectReferences_MetaData[] = {
		{ "Category", "HierarchySelectionOptions" },
		{ "ModuleRelativePath", "Private/DataprepSelectionTransforms.h" },
		{ "ToolTip", "Include assets referenced/used by assets directly referenced/used by selected objects" },
	};
#endif
	void Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::NewProp_bAllowIndirectReferences_SetBit(void* Obj)
	{
		((UDataprepReferenceSelectionTransform*)Obj)->bAllowIndirectReferences = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::NewProp_bAllowIndirectReferences = { "bAllowIndirectReferences", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepReferenceSelectionTransform), &Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::NewProp_bAllowIndirectReferences_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::NewProp_bAllowIndirectReferences_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::NewProp_bAllowIndirectReferences_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::NewProp_bAllowIndirectReferences,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepReferenceSelectionTransform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::ClassParams = {
		&UDataprepReferenceSelectionTransform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepReferenceSelectionTransform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepReferenceSelectionTransform, 2833793258);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepReferenceSelectionTransform>()
	{
		return UDataprepReferenceSelectionTransform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepReferenceSelectionTransform(Z_Construct_UClass_UDataprepReferenceSelectionTransform, &UDataprepReferenceSelectionTransform::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepReferenceSelectionTransform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepReferenceSelectionTransform);
	void UDataprepReferencedSelectionTransform::StaticRegisterNativesUDataprepReferencedSelectionTransform()
	{
	}
	UClass* Z_Construct_UClass_UDataprepReferencedSelectionTransform_NoRegister()
	{
		return UDataprepReferencedSelectionTransform::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepReferencedSelectionTransform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepReferencedSelectionTransform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepSelectionTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepReferencedSelectionTransform_Statics::Class_MetaDataParams[] = {
		{ "Category", "SelectionTransform" },
		{ "DisplayName", "Select Referencers" },
		{ "IncludePath", "DataprepSelectionTransforms.h" },
		{ "ModuleRelativePath", "Private/DataprepSelectionTransforms.h" },
		{ "ToolTip", "Return assets directly using/referencing the objects from previous filtering" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepReferencedSelectionTransform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepReferencedSelectionTransform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepReferencedSelectionTransform_Statics::ClassParams = {
		&UDataprepReferencedSelectionTransform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepReferencedSelectionTransform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepReferencedSelectionTransform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepReferencedSelectionTransform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepReferencedSelectionTransform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepReferencedSelectionTransform, 1121517962);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepReferencedSelectionTransform>()
	{
		return UDataprepReferencedSelectionTransform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepReferencedSelectionTransform(Z_Construct_UClass_UDataprepReferencedSelectionTransform, &UDataprepReferencedSelectionTransform::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepReferencedSelectionTransform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepReferencedSelectionTransform);
	void UDataprepHierarchySelectionTransform::StaticRegisterNativesUDataprepHierarchySelectionTransform()
	{
	}
	UClass* Z_Construct_UClass_UDataprepHierarchySelectionTransform_NoRegister()
	{
		return UDataprepHierarchySelectionTransform::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SelectionPolicy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SelectionPolicy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepSelectionTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::Class_MetaDataParams[] = {
		{ "Category", "SelectionTransform" },
		{ "DisplayName", "Select Hierarchy" },
		{ "IncludePath", "DataprepSelectionTransforms.h" },
		{ "ModuleRelativePath", "Private/DataprepSelectionTransforms.h" },
		{ "ToolTip", "Return immediate children or all the descendants of the selected objects" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::NewProp_SelectionPolicy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::NewProp_SelectionPolicy_MetaData[] = {
		{ "Category", "HierarchySelectionOptions" },
		{ "DisplayName", "Select" },
		{ "ModuleRelativePath", "Private/DataprepSelectionTransforms.h" },
		{ "ToolTip", "Specify policy of hierarchical parsing of selected objects" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::NewProp_SelectionPolicy = { "SelectionPolicy", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepHierarchySelectionTransform, SelectionPolicy), Z_Construct_UEnum_DataprepLibraries_EDataprepHierarchySelectionPolicy, METADATA_PARAMS(Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::NewProp_SelectionPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::NewProp_SelectionPolicy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::NewProp_SelectionPolicy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::NewProp_SelectionPolicy,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepHierarchySelectionTransform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::ClassParams = {
		&UDataprepHierarchySelectionTransform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepHierarchySelectionTransform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepHierarchySelectionTransform, 3937426567);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepHierarchySelectionTransform>()
	{
		return UDataprepHierarchySelectionTransform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepHierarchySelectionTransform(Z_Construct_UClass_UDataprepHierarchySelectionTransform, &UDataprepHierarchySelectionTransform::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepHierarchySelectionTransform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepHierarchySelectionTransform);
	void UDataprepActorComponentsSelectionTransform::StaticRegisterNativesUDataprepActorComponentsSelectionTransform()
	{
	}
	UClass* Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_NoRegister()
	{
		return UDataprepActorComponentsSelectionTransform::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepSelectionTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_Statics::Class_MetaDataParams[] = {
		{ "Category", "SelectionTransform" },
		{ "DisplayName", "Select Actor Components" },
		{ "IncludePath", "DataprepSelectionTransforms.h" },
		{ "ModuleRelativePath", "Private/DataprepSelectionTransforms.h" },
		{ "ToolTip", "Return components of the selected actors" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepActorComponentsSelectionTransform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_Statics::ClassParams = {
		&UDataprepActorComponentsSelectionTransform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepActorComponentsSelectionTransform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepActorComponentsSelectionTransform, 55205787);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepActorComponentsSelectionTransform>()
	{
		return UDataprepActorComponentsSelectionTransform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepActorComponentsSelectionTransform(Z_Construct_UClass_UDataprepActorComponentsSelectionTransform, &UDataprepActorComponentsSelectionTransform::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepActorComponentsSelectionTransform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepActorComponentsSelectionTransform);
	void UDataprepOwningActorSelectionTransform::StaticRegisterNativesUDataprepOwningActorSelectionTransform()
	{
	}
	UClass* Z_Construct_UClass_UDataprepOwningActorSelectionTransform_NoRegister()
	{
		return UDataprepOwningActorSelectionTransform::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepOwningActorSelectionTransform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepOwningActorSelectionTransform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepSelectionTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepOwningActorSelectionTransform_Statics::Class_MetaDataParams[] = {
		{ "Category", "SelectionTransform" },
		{ "DisplayName", "Select Owning Actor" },
		{ "IncludePath", "DataprepSelectionTransforms.h" },
		{ "ModuleRelativePath", "Private/DataprepSelectionTransforms.h" },
		{ "ToolTip", "Return the owning actors of selected components" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepOwningActorSelectionTransform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepOwningActorSelectionTransform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepOwningActorSelectionTransform_Statics::ClassParams = {
		&UDataprepOwningActorSelectionTransform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepOwningActorSelectionTransform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepOwningActorSelectionTransform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepOwningActorSelectionTransform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepOwningActorSelectionTransform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepOwningActorSelectionTransform, 2578706140);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepOwningActorSelectionTransform>()
	{
		return UDataprepOwningActorSelectionTransform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepOwningActorSelectionTransform(Z_Construct_UClass_UDataprepOwningActorSelectionTransform, &UDataprepOwningActorSelectionTransform::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepOwningActorSelectionTransform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepOwningActorSelectionTransform);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
