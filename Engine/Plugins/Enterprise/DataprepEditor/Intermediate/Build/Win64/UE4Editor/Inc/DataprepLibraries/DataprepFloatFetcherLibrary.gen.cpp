// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepLibraries/Public/Fetchers/DataprepFloatFetcherLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepFloatFetcherLibrary() {}
// Cross Module References
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepFloatFetcher();
	UPackage* Z_Construct_UPackage__Script_DataprepLibraries();
// End Cross Module References
	void UDataprepFloatBoundingVolumeFetcher::StaticRegisterNativesUDataprepFloatBoundingVolumeFetcher()
	{
	}
	UClass* Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_NoRegister()
	{
		return UDataprepFloatBoundingVolumeFetcher::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepFloatFetcher,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Bounding Volume" },
		{ "IncludePath", "Fetchers/DataprepFloatFetcherLibrary.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/Fetchers/DataprepFloatFetcherLibrary.h" },
		{ "ToolTip", "Filter objects based on their bounding box volume.\n For actor's bounding box only the components with a collision enabled will be used." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepFloatBoundingVolumeFetcher>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_Statics::ClassParams = {
		&UDataprepFloatBoundingVolumeFetcher::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepFloatBoundingVolumeFetcher, 527258209);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepFloatBoundingVolumeFetcher>()
	{
		return UDataprepFloatBoundingVolumeFetcher::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepFloatBoundingVolumeFetcher(Z_Construct_UClass_UDataprepFloatBoundingVolumeFetcher, &UDataprepFloatBoundingVolumeFetcher::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepFloatBoundingVolumeFetcher"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepFloatBoundingVolumeFetcher);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
