// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
class AActor;
#ifdef DATAPREPCORE_DataprepRecipe_generated_h
#error "DataprepRecipe.generated.h already included, missing '#pragma once' in DataprepRecipe.h"
#endif
#define DATAPREPCORE_DataprepRecipe_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAssets); \
	DECLARE_FUNCTION(execGetActors);


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAssets); \
	DECLARE_FUNCTION(execGetActors);


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_EVENT_PARMS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_CALLBACK_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_DataprepRecipe(); \
	friend struct Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_DataprepRecipe, UObject, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_DataprepRecipe)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_DataprepRecipe(); \
	friend struct Z_Construct_UClass_UDEPRECATED_DataprepRecipe_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_DataprepRecipe, UObject, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_DataprepRecipe)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_DataprepRecipe(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_DataprepRecipe) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_DataprepRecipe); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_DataprepRecipe); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_DataprepRecipe(UDEPRECATED_DataprepRecipe&&); \
	NO_API UDEPRECATED_DataprepRecipe(const UDEPRECATED_DataprepRecipe&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_DataprepRecipe(UDEPRECATED_DataprepRecipe&&); \
	NO_API UDEPRECATED_DataprepRecipe(const UDEPRECATED_DataprepRecipe&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_DataprepRecipe); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_DataprepRecipe); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDEPRECATED_DataprepRecipe)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_13_PROLOG \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_EVENT_PARMS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_CALLBACK_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_CALLBACK_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDEPRECATED_DataprepRecipe>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepRecipe_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
