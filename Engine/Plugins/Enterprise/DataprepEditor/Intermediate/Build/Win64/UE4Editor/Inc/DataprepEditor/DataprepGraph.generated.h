// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPEDITOR_DataprepGraph_generated_h
#error "DataprepGraph.generated.h already included, missing '#pragma once' in DataprepGraph.h"
#endif
#define DATAPREPEDITOR_DataprepGraph_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepGraph(); \
	friend struct Z_Construct_UClass_UDataprepGraph_Statics; \
public: \
	DECLARE_CLASS(UDataprepGraph, UEdGraph, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepEditor"), NO_API) \
	DECLARE_SERIALIZER(UDataprepGraph)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepGraph(); \
	friend struct Z_Construct_UClass_UDataprepGraph_Statics; \
public: \
	DECLARE_CLASS(UDataprepGraph, UEdGraph, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepEditor"), NO_API) \
	DECLARE_SERIALIZER(UDataprepGraph)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepGraph(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepGraph) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepGraph); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepGraph(UDataprepGraph&&); \
	NO_API UDataprepGraph(const UDataprepGraph&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepGraph(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepGraph(UDataprepGraph&&); \
	NO_API UDataprepGraph(const UDataprepGraph&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepGraph); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepGraph)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_26_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPEDITOR_API UClass* StaticClass<class UDataprepGraph>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepGraphRecipeNode(); \
	friend struct Z_Construct_UClass_UDataprepGraphRecipeNode_Statics; \
public: \
	DECLARE_CLASS(UDataprepGraphRecipeNode, UEdGraphNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepEditor"), NO_API) \
	DECLARE_SERIALIZER(UDataprepGraphRecipeNode)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepGraphRecipeNode(); \
	friend struct Z_Construct_UClass_UDataprepGraphRecipeNode_Statics; \
public: \
	DECLARE_CLASS(UDataprepGraphRecipeNode, UEdGraphNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepEditor"), NO_API) \
	DECLARE_SERIALIZER(UDataprepGraphRecipeNode)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepGraphRecipeNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepGraphRecipeNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepGraphRecipeNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepGraphRecipeNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepGraphRecipeNode(UDataprepGraphRecipeNode&&); \
	NO_API UDataprepGraphRecipeNode(const UDataprepGraphRecipeNode&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepGraphRecipeNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepGraphRecipeNode(UDataprepGraphRecipeNode&&); \
	NO_API UDataprepGraphRecipeNode(const UDataprepGraphRecipeNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepGraphRecipeNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepGraphRecipeNode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepGraphRecipeNode)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_47_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h_50_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPEDITOR_API UClass* StaticClass<class UDataprepGraphRecipeNode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_DataprepGraph_DataprepGraph_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
