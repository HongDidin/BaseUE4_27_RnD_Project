// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPLIBRARIES_DataprepSelectionTransforms_generated_h
#error "DataprepSelectionTransforms.generated.h already included, missing '#pragma once' in DataprepSelectionTransforms.h"
#endif
#define DATAPREPLIBRARIES_DataprepSelectionTransforms_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepReferenceSelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepReferenceSelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepReferenceSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepReferenceSelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepReferenceSelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepReferenceSelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepReferenceSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepReferenceSelectionTransform(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepReferenceSelectionTransform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepReferenceSelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepReferenceSelectionTransform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepReferenceSelectionTransform(UDataprepReferenceSelectionTransform&&); \
	NO_API UDataprepReferenceSelectionTransform(const UDataprepReferenceSelectionTransform&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepReferenceSelectionTransform(UDataprepReferenceSelectionTransform&&); \
	NO_API UDataprepReferenceSelectionTransform(const UDataprepReferenceSelectionTransform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepReferenceSelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepReferenceSelectionTransform); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepReferenceSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bAllowIndirectReferences() { return STRUCT_OFFSET(UDataprepReferenceSelectionTransform, bAllowIndirectReferences); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_25_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPLIBRARIES_API UClass* StaticClass<class UDataprepReferenceSelectionTransform>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepReferencedSelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepReferencedSelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepReferencedSelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepReferencedSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepReferencedSelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepReferencedSelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepReferencedSelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepReferencedSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepReferencedSelectionTransform(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepReferencedSelectionTransform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepReferencedSelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepReferencedSelectionTransform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepReferencedSelectionTransform(UDataprepReferencedSelectionTransform&&); \
	NO_API UDataprepReferencedSelectionTransform(const UDataprepReferencedSelectionTransform&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepReferencedSelectionTransform(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepReferencedSelectionTransform(UDataprepReferencedSelectionTransform&&); \
	NO_API UDataprepReferencedSelectionTransform(const UDataprepReferencedSelectionTransform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepReferencedSelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepReferencedSelectionTransform); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepReferencedSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_41_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_44_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPLIBRARIES_API UClass* StaticClass<class UDataprepReferencedSelectionTransform>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepHierarchySelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepHierarchySelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepHierarchySelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepHierarchySelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepHierarchySelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepHierarchySelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepHierarchySelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepHierarchySelectionTransform(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepHierarchySelectionTransform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepHierarchySelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepHierarchySelectionTransform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepHierarchySelectionTransform(UDataprepHierarchySelectionTransform&&); \
	NO_API UDataprepHierarchySelectionTransform(const UDataprepHierarchySelectionTransform&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepHierarchySelectionTransform(UDataprepHierarchySelectionTransform&&); \
	NO_API UDataprepHierarchySelectionTransform(const UDataprepHierarchySelectionTransform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepHierarchySelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepHierarchySelectionTransform); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepHierarchySelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SelectionPolicy() { return STRUCT_OFFSET(UDataprepHierarchySelectionTransform, SelectionPolicy); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_50_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_53_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPLIBRARIES_API UClass* StaticClass<class UDataprepHierarchySelectionTransform>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepActorComponentsSelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepActorComponentsSelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepActorComponentsSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepActorComponentsSelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepActorComponentsSelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepActorComponentsSelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepActorComponentsSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepActorComponentsSelectionTransform(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepActorComponentsSelectionTransform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepActorComponentsSelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepActorComponentsSelectionTransform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepActorComponentsSelectionTransform(UDataprepActorComponentsSelectionTransform&&); \
	NO_API UDataprepActorComponentsSelectionTransform(const UDataprepActorComponentsSelectionTransform&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepActorComponentsSelectionTransform(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepActorComponentsSelectionTransform(UDataprepActorComponentsSelectionTransform&&); \
	NO_API UDataprepActorComponentsSelectionTransform(const UDataprepActorComponentsSelectionTransform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepActorComponentsSelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepActorComponentsSelectionTransform); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepActorComponentsSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_66_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_69_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPLIBRARIES_API UClass* StaticClass<class UDataprepActorComponentsSelectionTransform>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepOwningActorSelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepOwningActorSelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepOwningActorSelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepOwningActorSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepOwningActorSelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepOwningActorSelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepOwningActorSelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepOwningActorSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepOwningActorSelectionTransform(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepOwningActorSelectionTransform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepOwningActorSelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepOwningActorSelectionTransform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepOwningActorSelectionTransform(UDataprepOwningActorSelectionTransform&&); \
	NO_API UDataprepOwningActorSelectionTransform(const UDataprepOwningActorSelectionTransform&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepOwningActorSelectionTransform(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepOwningActorSelectionTransform(UDataprepOwningActorSelectionTransform&&); \
	NO_API UDataprepOwningActorSelectionTransform(const UDataprepOwningActorSelectionTransform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepOwningActorSelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepOwningActorSelectionTransform); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepOwningActorSelectionTransform)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_75_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h_78_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPLIBRARIES_API UClass* StaticClass<class UDataprepOwningActorSelectionTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Private_DataprepSelectionTransforms_h


#define FOREACH_ENUM_EDATAPREPHIERARCHYSELECTIONPOLICY(op) \
	op(EDataprepHierarchySelectionPolicy::ImmediateChildren) \
	op(EDataprepHierarchySelectionPolicy::AllDescendants) 

enum class EDataprepHierarchySelectionPolicy : uint8;
template<> DATAPREPLIBRARIES_API UEnum* StaticEnum<EDataprepHierarchySelectionPolicy>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
