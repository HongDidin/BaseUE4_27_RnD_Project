// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepAsset_generated_h
#error "DataprepAsset.generated.h already included, missing '#pragma once' in DataprepAsset.h"
#endif
#define DATAPREPCORE_DataprepAsset_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepAsset(); \
	friend struct Z_Construct_UClass_UDataprepAsset_Statics; \
public: \
	DECLARE_CLASS(UDataprepAsset, UDataprepAssetInterface, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepAsset)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepAsset(); \
	friend struct Z_Construct_UClass_UDataprepAsset_Statics; \
public: \
	DECLARE_CLASS(UDataprepAsset, UDataprepAssetInterface, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepAsset)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepAsset(UDataprepAsset&&); \
	NO_API UDataprepAsset(const UDataprepAsset&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepAsset() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepAsset(UDataprepAsset&&); \
	NO_API UDataprepAsset(const UDataprepAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepAsset); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataprepAsset)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StartNode_DEPRECATED() { return STRUCT_OFFSET(UDataprepAsset, StartNode_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__Parameterization() { return STRUCT_OFFSET(UDataprepAsset, Parameterization); } \
	FORCEINLINE static uint32 __PPO__ActionAssets() { return STRUCT_OFFSET(UDataprepAsset, ActionAssets); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_29_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepAsset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
