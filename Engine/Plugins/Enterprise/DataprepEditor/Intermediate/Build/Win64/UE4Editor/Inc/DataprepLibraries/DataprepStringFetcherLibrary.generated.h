// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPLIBRARIES_DataprepStringFetcherLibrary_generated_h
#error "DataprepStringFetcherLibrary.generated.h already included, missing '#pragma once' in DataprepStringFetcherLibrary.h"
#endif
#define DATAPREPLIBRARIES_DataprepStringFetcherLibrary_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepStringObjectNameFetcher(); \
	friend struct Z_Construct_UClass_UDataprepStringObjectNameFetcher_Statics; \
public: \
	DECLARE_CLASS(UDataprepStringObjectNameFetcher, UDataprepStringFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepStringObjectNameFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepStringObjectNameFetcher(); \
	friend struct Z_Construct_UClass_UDataprepStringObjectNameFetcher_Statics; \
public: \
	DECLARE_CLASS(UDataprepStringObjectNameFetcher, UDataprepStringFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepStringObjectNameFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepStringObjectNameFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepStringObjectNameFetcher) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepStringObjectNameFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepStringObjectNameFetcher); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepStringObjectNameFetcher(UDataprepStringObjectNameFetcher&&); \
	NO_API UDataprepStringObjectNameFetcher(const UDataprepStringObjectNameFetcher&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepStringObjectNameFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepStringObjectNameFetcher(UDataprepStringObjectNameFetcher&&); \
	NO_API UDataprepStringObjectNameFetcher(const UDataprepStringObjectNameFetcher&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepStringObjectNameFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepStringObjectNameFetcher); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepStringObjectNameFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_11_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPLIBRARIES_API UClass* StaticClass<class UDataprepStringObjectNameFetcher>();

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepStringActorLabelFetcher(); \
	friend struct Z_Construct_UClass_UDataprepStringActorLabelFetcher_Statics; \
public: \
	DECLARE_CLASS(UDataprepStringActorLabelFetcher, UDataprepStringFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepStringActorLabelFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepStringActorLabelFetcher(); \
	friend struct Z_Construct_UClass_UDataprepStringActorLabelFetcher_Statics; \
public: \
	DECLARE_CLASS(UDataprepStringActorLabelFetcher, UDataprepStringFetcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepLibraries"), NO_API) \
	DECLARE_SERIALIZER(UDataprepStringActorLabelFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepStringActorLabelFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepStringActorLabelFetcher) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepStringActorLabelFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepStringActorLabelFetcher); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepStringActorLabelFetcher(UDataprepStringActorLabelFetcher&&); \
	NO_API UDataprepStringActorLabelFetcher(const UDataprepStringActorLabelFetcher&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepStringActorLabelFetcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepStringActorLabelFetcher(UDataprepStringActorLabelFetcher&&); \
	NO_API UDataprepStringActorLabelFetcher(const UDataprepStringActorLabelFetcher&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepStringActorLabelFetcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepStringActorLabelFetcher); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepStringActorLabelFetcher)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_26_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPLIBRARIES_API UClass* StaticClass<class UDataprepStringActorLabelFetcher>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepLibraries_Public_Fetchers_DataprepStringFetcherLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
