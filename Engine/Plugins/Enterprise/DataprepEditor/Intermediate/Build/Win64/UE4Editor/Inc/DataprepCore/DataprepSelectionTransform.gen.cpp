// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/SelectionSystem/DataprepSelectionTransform.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepSelectionTransform() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepSelectionTransform_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepSelectionTransform();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepParameterizableObject();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepRecursiveSelectionTransform_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepRecursiveSelectionTransform();
// End Cross Module References
	DEFINE_FUNCTION(UDataprepSelectionTransform::execGetAdditionalKeyword)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=P_THIS->GetAdditionalKeyword_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepSelectionTransform::execGetCategory)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=P_THIS->GetCategory_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepSelectionTransform::execGetTooltip)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=P_THIS->GetTooltip_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepSelectionTransform::execGetDisplayTransformName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=P_THIS->GetDisplayTransformName_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepSelectionTransform::execOnExecution)
	{
		P_GET_TARRAY_REF(UObject*,Z_Param_Out_InObjects);
		P_GET_TARRAY_REF(UObject*,Z_Param_Out_OutObjects);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnExecution_Implementation(Z_Param_Out_InObjects,Z_Param_Out_OutObjects);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepSelectionTransform::execExecute)
	{
		P_GET_TARRAY_REF(UObject*,Z_Param_Out_InObjects);
		P_GET_TARRAY_REF(UObject*,Z_Param_Out_OutObjects);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Execute(Z_Param_Out_InObjects,Z_Param_Out_OutObjects);
		P_NATIVE_END;
	}
	static FName NAME_UDataprepSelectionTransform_GetAdditionalKeyword = FName(TEXT("GetAdditionalKeyword"));
	FText UDataprepSelectionTransform::GetAdditionalKeyword() const
	{
		DataprepSelectionTransform_eventGetAdditionalKeyword_Parms Parms;
		const_cast<UDataprepSelectionTransform*>(this)->ProcessEvent(FindFunctionChecked(NAME_UDataprepSelectionTransform_GetAdditionalKeyword),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UDataprepSelectionTransform_GetCategory = FName(TEXT("GetCategory"));
	FText UDataprepSelectionTransform::GetCategory() const
	{
		DataprepSelectionTransform_eventGetCategory_Parms Parms;
		const_cast<UDataprepSelectionTransform*>(this)->ProcessEvent(FindFunctionChecked(NAME_UDataprepSelectionTransform_GetCategory),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UDataprepSelectionTransform_GetDisplayTransformName = FName(TEXT("GetDisplayTransformName"));
	FText UDataprepSelectionTransform::GetDisplayTransformName() const
	{
		DataprepSelectionTransform_eventGetDisplayTransformName_Parms Parms;
		const_cast<UDataprepSelectionTransform*>(this)->ProcessEvent(FindFunctionChecked(NAME_UDataprepSelectionTransform_GetDisplayTransformName),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UDataprepSelectionTransform_GetTooltip = FName(TEXT("GetTooltip"));
	FText UDataprepSelectionTransform::GetTooltip() const
	{
		DataprepSelectionTransform_eventGetTooltip_Parms Parms;
		const_cast<UDataprepSelectionTransform*>(this)->ProcessEvent(FindFunctionChecked(NAME_UDataprepSelectionTransform_GetTooltip),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UDataprepSelectionTransform_OnExecution = FName(TEXT("OnExecution"));
	void UDataprepSelectionTransform::OnExecution(TArray<UObject*> const& InObjects, TArray<UObject*>& OutObjects)
	{
		DataprepSelectionTransform_eventOnExecution_Parms Parms;
		Parms.InObjects=InObjects;
		Parms.OutObjects=OutObjects;
		ProcessEvent(FindFunctionChecked(NAME_UDataprepSelectionTransform_OnExecution),&Parms);
		OutObjects=Parms.OutObjects;
	}
	void UDataprepSelectionTransform::StaticRegisterNativesUDataprepSelectionTransform()
	{
		UClass* Class = UDataprepSelectionTransform::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Execute", &UDataprepSelectionTransform::execExecute },
			{ "GetAdditionalKeyword", &UDataprepSelectionTransform::execGetAdditionalKeyword },
			{ "GetCategory", &UDataprepSelectionTransform::execGetCategory },
			{ "GetDisplayTransformName", &UDataprepSelectionTransform::execGetDisplayTransformName },
			{ "GetTooltip", &UDataprepSelectionTransform::execGetTooltip },
			{ "OnExecution", &UDataprepSelectionTransform::execOnExecution },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics
	{
		struct DataprepSelectionTransform_eventExecute_Parms
		{
			TArray<UObject*> InObjects;
			TArray<UObject*> OutObjects;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InObjects;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutObjects_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutObjects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_InObjects_Inner = { "InObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_InObjects_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_InObjects = { "InObjects", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepSelectionTransform_eventExecute_Parms, InObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_InObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_InObjects_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_OutObjects_Inner = { "OutObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_OutObjects = { "OutObjects", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepSelectionTransform_eventExecute_Parms, OutObjects), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_InObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_InObjects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_OutObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::NewProp_OutObjects,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::Function_MetaDataParams[] = {
		{ "Category", "Execution" },
		{ "Comment", "/**\n\x09 * Execute the transform\n\x09 * @param InObjects Input objects the transform will operate on\n\x09 * @param OutObjects Resulting objects after the transform was executed\n\x09 */" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepSelectionTransform.h" },
		{ "ToolTip", "Execute the transform\n@param InObjects Input objects the transform will operate on\n@param OutObjects Resulting objects after the transform was executed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepSelectionTransform, nullptr, "Execute", nullptr, nullptr, sizeof(DataprepSelectionTransform_eventExecute_Parms), Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepSelectionTransform_Execute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepSelectionTransform_Execute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics
	{
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepSelectionTransform_eventGetAdditionalKeyword_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics::Function_MetaDataParams[] = {
		{ "Category", "Display|Search" },
		{ "Comment", "/**\n\x09 * Allows to add more keywords for when a user is searching for the fetcher in the ui.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepSelectionTransform.h" },
		{ "ToolTip", "Allows to add more keywords for when a user is searching for the fetcher in the ui." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepSelectionTransform, nullptr, "GetAdditionalKeyword", nullptr, nullptr, sizeof(DataprepSelectionTransform_eventGetAdditionalKeyword_Parms), Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics
	{
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepSelectionTransform_eventGetCategory_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics::Function_MetaDataParams[] = {
		{ "Category", "Display" },
		{ "Comment", "/**\n\x09 * Allows to change the category of the transform for the ui if needed.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepSelectionTransform.h" },
		{ "ToolTip", "Allows to change the category of the transform for the ui if needed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepSelectionTransform, nullptr, "GetCategory", nullptr, nullptr, sizeof(DataprepSelectionTransform_eventGetCategory_Parms), Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics
	{
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepSelectionTransform_eventGetDisplayTransformName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Display" },
		{ "Comment", "/** \n\x09 * Allows to change the name of the transform for the ui if needed.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepSelectionTransform.h" },
		{ "ToolTip", "Allows to change the name of the transform for the ui if needed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepSelectionTransform, nullptr, "GetDisplayTransformName", nullptr, nullptr, sizeof(DataprepSelectionTransform_eventGetDisplayTransformName_Parms), Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics
	{
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepSelectionTransform_eventGetTooltip_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics::Function_MetaDataParams[] = {
		{ "Category", "Display" },
		{ "Comment", "/**\n\x09 * Allows to change the tooltip of the transform for the ui if needed.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepSelectionTransform.h" },
		{ "ToolTip", "Allows to change the tooltip of the transform for the ui if needed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepSelectionTransform, nullptr, "GetTooltip", nullptr, nullptr, sizeof(DataprepSelectionTransform_eventGetTooltip_Parms), Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InObjects;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutObjects_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutObjects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_InObjects_Inner = { "InObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_InObjects_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_InObjects = { "InObjects", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepSelectionTransform_eventOnExecution_Parms, InObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_InObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_InObjects_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_OutObjects_Inner = { "OutObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_OutObjects = { "OutObjects", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepSelectionTransform_eventOnExecution_Parms, OutObjects), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_InObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_InObjects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_OutObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::NewProp_OutObjects,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * This function is called when the transform is executed.\n\x09 * If your defining your transform in Blueprint or Python this is the function to override.\n\x09 * @param InObjects Input objects the transform will operate on\n\x09 * @param OutObjects Resulting objects after the transform was executed\n\x09 */" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepSelectionTransform.h" },
		{ "ToolTip", "This function is called when the transform is executed.\nIf your defining your transform in Blueprint or Python this is the function to override.\n@param InObjects Input objects the transform will operate on\n@param OutObjects Resulting objects after the transform was executed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepSelectionTransform, nullptr, "OnExecution", nullptr, nullptr, sizeof(DataprepSelectionTransform_eventOnExecution_Parms), Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08480C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDataprepSelectionTransform_NoRegister()
	{
		return UDataprepSelectionTransform::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSelectionTransform_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOutputCanIncludeInput_MetaData[];
#endif
		static void NewProp_bOutputCanIncludeInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOutputCanIncludeInput;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSelectionTransform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepParameterizableObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDataprepSelectionTransform_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDataprepSelectionTransform_Execute, "Execute" }, // 2846269027
		{ &Z_Construct_UFunction_UDataprepSelectionTransform_GetAdditionalKeyword, "GetAdditionalKeyword" }, // 4100887475
		{ &Z_Construct_UFunction_UDataprepSelectionTransform_GetCategory, "GetCategory" }, // 2095856737
		{ &Z_Construct_UFunction_UDataprepSelectionTransform_GetDisplayTransformName, "GetDisplayTransformName" }, // 3168629735
		{ &Z_Construct_UFunction_UDataprepSelectionTransform_GetTooltip, "GetTooltip" }, // 3211847881
		{ &Z_Construct_UFunction_UDataprepSelectionTransform_OnExecution, "OnExecution" }, // 1033965139
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSelectionTransform_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "SelectionSystem/DataprepSelectionTransform.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepSelectionTransform.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSelectionTransform_Statics::NewProp_bOutputCanIncludeInput_MetaData[] = {
		{ "Category", "SelectionTransform" },
		{ "Comment", "// Specifies if input objects that have matching type can be added to the result\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepSelectionTransform.h" },
		{ "ToolTip", "Specifies if input objects that have matching type can be added to the result" },
	};
#endif
	void Z_Construct_UClass_UDataprepSelectionTransform_Statics::NewProp_bOutputCanIncludeInput_SetBit(void* Obj)
	{
		((UDataprepSelectionTransform*)Obj)->bOutputCanIncludeInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepSelectionTransform_Statics::NewProp_bOutputCanIncludeInput = { "bOutputCanIncludeInput", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepSelectionTransform), &Z_Construct_UClass_UDataprepSelectionTransform_Statics::NewProp_bOutputCanIncludeInput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepSelectionTransform_Statics::NewProp_bOutputCanIncludeInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSelectionTransform_Statics::NewProp_bOutputCanIncludeInput_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSelectionTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSelectionTransform_Statics::NewProp_bOutputCanIncludeInput,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSelectionTransform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSelectionTransform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSelectionTransform_Statics::ClassParams = {
		&UDataprepSelectionTransform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDataprepSelectionTransform_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSelectionTransform_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSelectionTransform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSelectionTransform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSelectionTransform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSelectionTransform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSelectionTransform, 3567306322);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepSelectionTransform>()
	{
		return UDataprepSelectionTransform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSelectionTransform(Z_Construct_UClass_UDataprepSelectionTransform, &UDataprepSelectionTransform::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepSelectionTransform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSelectionTransform);
	void UDataprepRecursiveSelectionTransform::StaticRegisterNativesUDataprepRecursiveSelectionTransform()
	{
	}
	UClass* Z_Construct_UClass_UDataprepRecursiveSelectionTransform_NoRegister()
	{
		return UDataprepRecursiveSelectionTransform::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllowRecursionLevels_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AllowRecursionLevels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepSelectionTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "SelectionSystem/DataprepSelectionTransform.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepSelectionTransform.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::NewProp_AllowRecursionLevels_MetaData[] = {
		{ "Category", "SelectionTransform" },
		{ "Comment", "// How many times is it allowed to apply another transform on the result\n" },
		{ "ModuleRelativePath", "Public/SelectionSystem/DataprepSelectionTransform.h" },
		{ "ToolTip", "How many times is it allowed to apply another transform on the result" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::NewProp_AllowRecursionLevels = { "AllowRecursionLevels", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRecursiveSelectionTransform, AllowRecursionLevels), METADATA_PARAMS(Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::NewProp_AllowRecursionLevels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::NewProp_AllowRecursionLevels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::NewProp_AllowRecursionLevels,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepRecursiveSelectionTransform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::ClassParams = {
		&UDataprepRecursiveSelectionTransform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepRecursiveSelectionTransform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepRecursiveSelectionTransform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepRecursiveSelectionTransform, 3176237364);
	template<> DATAPREPCORE_API UClass* StaticClass<UDataprepRecursiveSelectionTransform>()
	{
		return UDataprepRecursiveSelectionTransform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepRecursiveSelectionTransform(Z_Construct_UClass_UDataprepRecursiveSelectionTransform, &UDataprepRecursiveSelectionTransform::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UDataprepRecursiveSelectionTransform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepRecursiveSelectionTransform);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
