// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepContentProducer_generated_h
#error "DataprepContentProducer.generated.h already included, missing '#pragma once' in DataprepContentProducer.h"
#endif
#define DATAPREPCORE_DataprepContentProducer_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepContentProducer(); \
	friend struct Z_Construct_UClass_UDataprepContentProducer_Statics; \
public: \
	DECLARE_CLASS(UDataprepContentProducer, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepContentProducer)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepContentProducer(); \
	friend struct Z_Construct_UClass_UDataprepContentProducer_Statics; \
public: \
	DECLARE_CLASS(UDataprepContentProducer, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepContentProducer)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepContentProducer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepContentProducer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepContentProducer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepContentProducer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepContentProducer(UDataprepContentProducer&&); \
	NO_API UDataprepContentProducer(const UDataprepContentProducer&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepContentProducer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepContentProducer(UDataprepContentProducer&&); \
	NO_API UDataprepContentProducer(const UDataprepContentProducer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepContentProducer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepContentProducer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepContentProducer)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_65_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h_68_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepContentProducer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_DataprepContentProducer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
