// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepLibraries/Private/DataprepEditingOperations.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepEditingOperations() {}
// Cross Module References
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepDeleteObjectsOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepDeleteObjectsOperation();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepEditingOperation();
	UPackage* Z_Construct_UPackage__Script_DataprepLibraries();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepMergeActorsOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepMergeActorsOperation();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FMeshMergingSettings();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepCreateProxyMeshOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepCreateProxyMeshOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepCompactSceneGraphOperation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepCompactSceneGraphOperation();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSpawnActorsAtLocation_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepSpawnActorsAtLocation();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UDataprepDeleteObjectsOperation::StaticRegisterNativesUDataprepDeleteObjectsOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepDeleteObjectsOperation_NoRegister()
	{
		return UDataprepDeleteObjectsOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepDeleteObjectsOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepDeleteObjectsOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepDeleteObjectsOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ObjectOperation" },
		{ "DisplayName", "Delete Objects" },
		{ "IncludePath", "DataprepEditingOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "Delete any asset or actor to process" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepDeleteObjectsOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepDeleteObjectsOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepDeleteObjectsOperation_Statics::ClassParams = {
		&UDataprepDeleteObjectsOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepDeleteObjectsOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepDeleteObjectsOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepDeleteObjectsOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepDeleteObjectsOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepDeleteObjectsOperation, 625606141);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepDeleteObjectsOperation>()
	{
		return UDataprepDeleteObjectsOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepDeleteObjectsOperation(Z_Construct_UClass_UDataprepDeleteObjectsOperation, &UDataprepDeleteObjectsOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepDeleteObjectsOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepDeleteObjectsOperation);
	void UDataprepMergeActorsOperation::StaticRegisterNativesUDataprepMergeActorsOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepMergeActorsOperation_NoRegister()
	{
		return UDataprepMergeActorsOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepMergeActorsOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewActorLabel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewActorLabel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDeleteMergedActors_MetaData[];
#endif
		static void NewProp_bDeleteMergedActors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDeleteMergedActors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDeleteMergedMeshes_MetaData[];
#endif
		static void NewProp_bDeleteMergedMeshes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDeleteMergedMeshes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MergeSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MergeSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPivotPointAtZero_MetaData[];
#endif
		static void NewProp_bPivotPointAtZero_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPivotPointAtZero;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ObjectOperation" },
		{ "DisplayName", "Merge" },
		{ "IncludePath", "DataprepEditingOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "Collect geometry from selected actors and merge them into single mesh." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_NewActorLabel_MetaData[] = {
		{ "Category", "MergeSettings" },
		{ "Comment", "/** Settings to use for the merge operation */" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "Settings to use for the merge operation" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_NewActorLabel = { "NewActorLabel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepMergeActorsOperation, NewActorLabel), METADATA_PARAMS(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_NewActorLabel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_NewActorLabel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedActors_MetaData[] = {
		{ "Comment", "/** Settings to use for the merge operation */" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "Settings to use for the merge operation" },
	};
#endif
	void Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedActors_SetBit(void* Obj)
	{
		((UDataprepMergeActorsOperation*)Obj)->bDeleteMergedActors_DEPRECATED = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedActors = { "bDeleteMergedActors", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepMergeActorsOperation), &Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedActors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedActors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedMeshes_MetaData[] = {
		{ "Comment", "/** Settings to use for the merge operation */" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "Settings to use for the merge operation" },
	};
#endif
	void Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedMeshes_SetBit(void* Obj)
	{
		((UDataprepMergeActorsOperation*)Obj)->bDeleteMergedMeshes_DEPRECATED = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedMeshes = { "bDeleteMergedMeshes", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepMergeActorsOperation), &Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedMeshes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedMeshes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_MergeSettings_MetaData[] = {
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_MergeSettings = { "MergeSettings", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepMergeActorsOperation, MergeSettings_DEPRECATED), Z_Construct_UScriptStruct_FMeshMergingSettings, METADATA_PARAMS(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_MergeSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_MergeSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bPivotPointAtZero_MetaData[] = {
		{ "Category", "MeshSettings" },
		{ "Comment", "/** Whether merged mesh should have pivot at world origin, or at first merged component otherwise */" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "Whether merged mesh should have pivot at world origin, or at first merged component otherwise" },
	};
#endif
	void Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bPivotPointAtZero_SetBit(void* Obj)
	{
		((UDataprepMergeActorsOperation*)Obj)->bPivotPointAtZero = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bPivotPointAtZero = { "bPivotPointAtZero", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepMergeActorsOperation), &Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bPivotPointAtZero_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bPivotPointAtZero_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bPivotPointAtZero_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_NewActorLabel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bDeleteMergedMeshes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_MergeSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::NewProp_bPivotPointAtZero,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepMergeActorsOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::ClassParams = {
		&UDataprepMergeActorsOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepMergeActorsOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepMergeActorsOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepMergeActorsOperation, 3021915350);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepMergeActorsOperation>()
	{
		return UDataprepMergeActorsOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepMergeActorsOperation(Z_Construct_UClass_UDataprepMergeActorsOperation, &UDataprepMergeActorsOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepMergeActorsOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepMergeActorsOperation);
	void UDataprepCreateProxyMeshOperation::StaticRegisterNativesUDataprepCreateProxyMeshOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepCreateProxyMeshOperation_NoRegister()
	{
		return UDataprepCreateProxyMeshOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewActorLabel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewActorLabel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Quality_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Quality;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ObjectOperation" },
		{ "DisplayName", "Create Proxy Mesh" },
		{ "IncludePath", "DataprepEditingOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "Collect geometry from selected actors and merge them into single mesh with reduction." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::NewProp_NewActorLabel_MetaData[] = {
		{ "Category", "ProxySettings" },
		{ "Comment", "/** Settings to use for the create proxy operation */" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "Settings to use for the create proxy operation" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::NewProp_NewActorLabel = { "NewActorLabel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepCreateProxyMeshOperation, NewActorLabel), METADATA_PARAMS(Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::NewProp_NewActorLabel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::NewProp_NewActorLabel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::NewProp_Quality_MetaData[] = {
		{ "Category", "ProxySettings" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "UIMax", "100" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::NewProp_Quality = { "Quality", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepCreateProxyMeshOperation, Quality), METADATA_PARAMS(Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::NewProp_Quality_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::NewProp_Quality_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::NewProp_NewActorLabel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::NewProp_Quality,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepCreateProxyMeshOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::ClassParams = {
		&UDataprepCreateProxyMeshOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepCreateProxyMeshOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepCreateProxyMeshOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepCreateProxyMeshOperation, 1215738878);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepCreateProxyMeshOperation>()
	{
		return UDataprepCreateProxyMeshOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepCreateProxyMeshOperation(Z_Construct_UClass_UDataprepCreateProxyMeshOperation, &UDataprepCreateProxyMeshOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepCreateProxyMeshOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepCreateProxyMeshOperation);
	void UDataprepDeleteUnusedAssetsOperation::StaticRegisterNativesUDataprepDeleteUnusedAssetsOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation_NoRegister()
	{
		return UDataprepDeleteUnusedAssetsOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ObjectOperation" },
		{ "DisplayName", "Delete Unused Assets" },
		{ "IncludePath", "DataprepEditingOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "Delete assets that are not referenced by any objects" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepDeleteUnusedAssetsOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation_Statics::ClassParams = {
		&UDataprepDeleteUnusedAssetsOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepDeleteUnusedAssetsOperation, 1341671882);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepDeleteUnusedAssetsOperation>()
	{
		return UDataprepDeleteUnusedAssetsOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepDeleteUnusedAssetsOperation(Z_Construct_UClass_UDataprepDeleteUnusedAssetsOperation, &UDataprepDeleteUnusedAssetsOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepDeleteUnusedAssetsOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepDeleteUnusedAssetsOperation);
	void UDataprepCompactSceneGraphOperation::StaticRegisterNativesUDataprepCompactSceneGraphOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepCompactSceneGraphOperation_NoRegister()
	{
		return UDataprepCompactSceneGraphOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepCompactSceneGraphOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepCompactSceneGraphOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepCompactSceneGraphOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ActorOperation" },
		{ "DisplayName", "Compact Scene Graph" },
		{ "IncludePath", "DataprepEditingOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "Delete actors that do not have visuals, but keep those needed to preserve hierarchy" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepCompactSceneGraphOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepCompactSceneGraphOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepCompactSceneGraphOperation_Statics::ClassParams = {
		&UDataprepCompactSceneGraphOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepCompactSceneGraphOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepCompactSceneGraphOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepCompactSceneGraphOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepCompactSceneGraphOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepCompactSceneGraphOperation, 4153838198);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepCompactSceneGraphOperation>()
	{
		return UDataprepCompactSceneGraphOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepCompactSceneGraphOperation(Z_Construct_UClass_UDataprepCompactSceneGraphOperation, &UDataprepCompactSceneGraphOperation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepCompactSceneGraphOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepCompactSceneGraphOperation);
	void UDataprepSpawnActorsAtLocation::StaticRegisterNativesUDataprepSpawnActorsAtLocation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSpawnActorsAtLocation_NoRegister()
	{
		return UDataprepSpawnActorsAtLocation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ActorOperation" },
		{ "DisplayName", "Spawn Actors At Location" },
		{ "IncludePath", "DataprepEditingOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
		{ "ToolTip", "For each actor in the input set, spawn an actor from the specified Asset at the same position and orientation than the reference" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::NewProp_SelectedAsset_MetaData[] = {
		{ "Category", "ActorOperation" },
		{ "ModuleRelativePath", "Private/DataprepEditingOperations.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::NewProp_SelectedAsset = { "SelectedAsset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSpawnActorsAtLocation, SelectedAsset), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::NewProp_SelectedAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::NewProp_SelectedAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::NewProp_SelectedAsset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSpawnActorsAtLocation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::ClassParams = {
		&UDataprepSpawnActorsAtLocation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSpawnActorsAtLocation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSpawnActorsAtLocation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSpawnActorsAtLocation, 2874891511);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepSpawnActorsAtLocation>()
	{
		return UDataprepSpawnActorsAtLocation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSpawnActorsAtLocation(Z_Construct_UClass_UDataprepSpawnActorsAtLocation, &UDataprepSpawnActorsAtLocation::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepSpawnActorsAtLocation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSpawnActorsAtLocation);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
