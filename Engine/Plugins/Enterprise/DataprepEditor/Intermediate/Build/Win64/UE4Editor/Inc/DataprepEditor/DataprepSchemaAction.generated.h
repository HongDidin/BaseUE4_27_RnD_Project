// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPEDITOR_DataprepSchemaAction_generated_h
#error "DataprepSchemaAction.generated.h already included, missing '#pragma once' in DataprepSchemaAction.h"
#endif
#define DATAPREPEDITOR_DataprepSchemaAction_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_SchemaActions_DataprepSchemaAction_h_51_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDataprepSchemaAction_Statics; \
	DATAPREPEDITOR_API static class UScriptStruct* StaticStruct(); \
	typedef FEdGraphSchemaAction Super;


template<> DATAPREPEDITOR_API UScriptStruct* StaticStruct<struct FDataprepSchemaAction>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepEditor_Private_SchemaActions_DataprepSchemaAction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
