// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepCore/Public/Blueprint/K2Node_DataprepActionCore.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_DataprepActionCore() {}
// Cross Module References
	DATAPREPCORE_API UClass* Z_Construct_UClass_UK2Node_DataprepActionCore_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UK2Node_DataprepActionCore();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_DataprepCore();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepActionAsset_NoRegister();
// End Cross Module References
	void UK2Node_DataprepActionCore::StaticRegisterNativesUK2Node_DataprepActionCore()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_DataprepActionCore_NoRegister()
	{
		return UK2Node_DataprepActionCore::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_DataprepActionCore_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataprepActionAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DataprepActionAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Blueprint/K2Node_DataprepActionCore.h" },
		{ "ModuleRelativePath", "Public/Blueprint/K2Node_DataprepActionCore.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::NewProp_DataprepActionAsset_MetaData[] = {
		{ "ModuleRelativePath", "Public/Blueprint/K2Node_DataprepActionCore.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::NewProp_DataprepActionAsset = { "DataprepActionAsset", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UK2Node_DataprepActionCore, DataprepActionAsset), Z_Construct_UClass_UDataprepActionAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::NewProp_DataprepActionAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::NewProp_DataprepActionAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::NewProp_DataprepActionAsset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_DataprepActionCore>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::ClassParams = {
		&UK2Node_DataprepActionCore::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_DataprepActionCore()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_DataprepActionCore_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_DataprepActionCore, 381107318);
	template<> DATAPREPCORE_API UClass* StaticClass<UK2Node_DataprepActionCore>()
	{
		return UK2Node_DataprepActionCore::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_DataprepActionCore(Z_Construct_UClass_UK2Node_DataprepActionCore, &UK2Node_DataprepActionCore::StaticClass, TEXT("/Script/DataprepCore"), TEXT("UK2Node_DataprepActionCore"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_DataprepActionCore);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
