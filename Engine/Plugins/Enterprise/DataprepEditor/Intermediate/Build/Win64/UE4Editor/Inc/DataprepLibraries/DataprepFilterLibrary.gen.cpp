// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepLibraries/Private/DataprepFilterLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepFilterLibrary() {}
// Cross Module References
	DATAPREPLIBRARIES_API UEnum* Z_Construct_UEnum_DataprepLibraries_EDataprepSizeFilterMode();
	UPackage* Z_Construct_UPackage__Script_DataprepLibraries();
	DATAPREPLIBRARIES_API UEnum* Z_Construct_UEnum_DataprepLibraries_EDataprepSizeSource();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepFilterLibrary_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepFilterLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	EDITORSCRIPTINGUTILITIES_API UEnum* Z_Construct_UEnum_EditorScriptingUtilities_EEditorScriptingStringMatchType();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	static UEnum* EDataprepSizeFilterMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DataprepLibraries_EDataprepSizeFilterMode, Z_Construct_UPackage__Script_DataprepLibraries(), TEXT("EDataprepSizeFilterMode"));
		}
		return Singleton;
	}
	template<> DATAPREPLIBRARIES_API UEnum* StaticEnum<EDataprepSizeFilterMode>()
	{
		return EDataprepSizeFilterMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDataprepSizeFilterMode(EDataprepSizeFilterMode_StaticEnum, TEXT("/Script/DataprepLibraries"), TEXT("EDataprepSizeFilterMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DataprepLibraries_EDataprepSizeFilterMode_Hash() { return 3388813769U; }
	UEnum* Z_Construct_UEnum_DataprepLibraries_EDataprepSizeFilterMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepLibraries();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDataprepSizeFilterMode"), 0, Get_Z_Construct_UEnum_DataprepLibraries_EDataprepSizeFilterMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDataprepSizeFilterMode::SmallerThan", (int64)EDataprepSizeFilterMode::SmallerThan },
				{ "EDataprepSizeFilterMode::BiggerThan", (int64)EDataprepSizeFilterMode::BiggerThan },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BiggerThan.Name", "EDataprepSizeFilterMode::BiggerThan" },
				{ "ModuleRelativePath", "Private/DataprepFilterLibrary.h" },
				{ "SmallerThan.Name", "EDataprepSizeFilterMode::SmallerThan" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DataprepLibraries,
				nullptr,
				"EDataprepSizeFilterMode",
				"EDataprepSizeFilterMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDataprepSizeSource_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DataprepLibraries_EDataprepSizeSource, Z_Construct_UPackage__Script_DataprepLibraries(), TEXT("EDataprepSizeSource"));
		}
		return Singleton;
	}
	template<> DATAPREPLIBRARIES_API UEnum* StaticEnum<EDataprepSizeSource>()
	{
		return EDataprepSizeSource_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDataprepSizeSource(EDataprepSizeSource_StaticEnum, TEXT("/Script/DataprepLibraries"), TEXT("EDataprepSizeSource"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DataprepLibraries_EDataprepSizeSource_Hash() { return 3904309704U; }
	UEnum* Z_Construct_UEnum_DataprepLibraries_EDataprepSizeSource()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepLibraries();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDataprepSizeSource"), 0, Get_Z_Construct_UEnum_DataprepLibraries_EDataprepSizeSource_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDataprepSizeSource::BoundingBoxVolume", (int64)EDataprepSizeSource::BoundingBoxVolume },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BoundingBoxVolume.Name", "EDataprepSizeSource::BoundingBoxVolume" },
				{ "ModuleRelativePath", "Private/DataprepFilterLibrary.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DataprepLibraries,
				nullptr,
				"EDataprepSizeSource",
				"EDataprepSizeSource",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UDataprepFilterLibrary::execFilterByTag)
	{
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_TargetArray);
		P_GET_PROPERTY(FNameProperty,Z_Param_Tag);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<AActor*>*)Z_Param__Result=UDataprepFilterLibrary::FilterByTag(Z_Param_Out_TargetArray,Z_Param_Tag);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepFilterLibrary::execFilterBySize)
	{
		P_GET_TARRAY_REF(UObject*,Z_Param_Out_TargetArray);
		P_GET_ENUM(EDataprepSizeSource,Z_Param_SizeSource);
		P_GET_ENUM(EDataprepSizeFilterMode,Z_Param_FilterMode);
		P_GET_PROPERTY(FFloatProperty,Z_Param_Threshold);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UObject*>*)Z_Param__Result=UDataprepFilterLibrary::FilterBySize(Z_Param_Out_TargetArray,EDataprepSizeSource(Z_Param_SizeSource),EDataprepSizeFilterMode(Z_Param_FilterMode),Z_Param_Threshold);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepFilterLibrary::execFilterByName)
	{
		P_GET_TARRAY_REF(UObject*,Z_Param_Out_TargetArray);
		P_GET_PROPERTY(FStrProperty,Z_Param_NameSubString);
		P_GET_ENUM(EEditorScriptingStringMatchType,Z_Param_StringMatch);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UObject*>*)Z_Param__Result=UDataprepFilterLibrary::FilterByName(Z_Param_Out_TargetArray,Z_Param_NameSubString,EEditorScriptingStringMatchType(Z_Param_StringMatch));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDataprepFilterLibrary::execFilterByClass)
	{
		P_GET_TARRAY_REF(UObject*,Z_Param_Out_TargetArray);
		P_GET_OBJECT(UClass,Z_Param_ObjectClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UObject*>*)Z_Param__Result=UDataprepFilterLibrary::FilterByClass(Z_Param_Out_TargetArray,Z_Param_ObjectClass);
		P_NATIVE_END;
	}
	void UDataprepFilterLibrary::StaticRegisterNativesUDataprepFilterLibrary()
	{
		UClass* Class = UDataprepFilterLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "FilterByClass", &UDataprepFilterLibrary::execFilterByClass },
			{ "FilterByName", &UDataprepFilterLibrary::execFilterByName },
			{ "FilterBySize", &UDataprepFilterLibrary::execFilterBySize },
			{ "FilterByTag", &UDataprepFilterLibrary::execFilterByTag },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics
	{
		struct DataprepFilterLibrary_eventFilterByClass_Parms
		{
			TArray<UObject*> TargetArray;
			TSubclassOf<UObject>  ObjectClass;
			TArray<UObject*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetArray;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ObjectClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_TargetArray_Inner = { "TargetArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_TargetArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_TargetArray = { "TargetArray", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterByClass_Parms, TargetArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_TargetArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_TargetArray_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_ObjectClass = { "ObjectClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterByClass_Parms, ObjectClass), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterByClass_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_TargetArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_TargetArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_ObjectClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "Dataprep | Filter" },
		{ "Comment", "/**\n\x09 * Filter the array based on the Object's class.\n\x09 * @param\x09TargetArray\x09\x09""Array of Object to filter. The array will not change.\n\x09 * @param\x09ObjectClass\x09\x09The Class of the object.\n\x09 * @return\x09The filtered list.\n\x09 */" },
		{ "DeterminesOutputType", "ObjectClass" },
		{ "ModuleRelativePath", "Private/DataprepFilterLibrary.h" },
		{ "ToolTip", "Filter the array based on the Object's class.\n@param       TargetArray             Array of Object to filter. The array will not change.\n@param       ObjectClass             The Class of the object.\n@return      The filtered list." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepFilterLibrary, nullptr, "FilterByClass", nullptr, nullptr, sizeof(DataprepFilterLibrary_eventFilterByClass_Parms), Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics
	{
		struct DataprepFilterLibrary_eventFilterByName_Parms
		{
			TArray<UObject*> TargetArray;
			FString NameSubString;
			EEditorScriptingStringMatchType StringMatch;
			TArray<UObject*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetArray;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameSubString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NameSubString;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_StringMatch_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_StringMatch;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_TargetArray_Inner = { "TargetArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_TargetArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_TargetArray = { "TargetArray", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterByName_Parms, TargetArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_TargetArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_TargetArray_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_NameSubString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_NameSubString = { "NameSubString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterByName_Parms, NameSubString), METADATA_PARAMS(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_NameSubString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_NameSubString_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_StringMatch_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_StringMatch = { "StringMatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterByName_Parms, StringMatch), Z_Construct_UEnum_EditorScriptingUtilities_EEditorScriptingStringMatchType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterByName_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_TargetArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_TargetArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_NameSubString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_StringMatch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_StringMatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Dataprep | Filter" },
		{ "Comment", "/**\n\x09 * Filter the array based on the Object name.\n\x09 * @param\x09TargetArray\x09\x09""Array of Object to filter. The array will not change.\n\x09 * @param\x09NameSubString\x09The name to filter with.\n\x09 * @param\x09StringMatch\x09\x09""Contains the NameSubString OR matches with the wildcard *? OR exactly the same value.\n\x09 * @return\x09The filtered list.\n\x09 */" },
		{ "CPP_Default_StringMatch", "Contains" },
		{ "DeterminesOutputType", "TargetArray" },
		{ "ModuleRelativePath", "Private/DataprepFilterLibrary.h" },
		{ "ToolTip", "Filter the array based on the Object name.\n@param       TargetArray             Array of Object to filter. The array will not change.\n@param       NameSubString   The name to filter with.\n@param       StringMatch             Contains the NameSubString OR matches with the wildcard *? OR exactly the same value.\n@return      The filtered list." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepFilterLibrary, nullptr, "FilterByName", nullptr, nullptr, sizeof(DataprepFilterLibrary_eventFilterByName_Parms), Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics
	{
		struct DataprepFilterLibrary_eventFilterBySize_Parms
		{
			TArray<UObject*> TargetArray;
			EDataprepSizeSource SizeSource;
			EDataprepSizeFilterMode FilterMode;
			float Threshold;
			TArray<UObject*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetArray;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SizeSource_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SizeSource;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FilterMode_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FilterMode;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Threshold;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_TargetArray_Inner = { "TargetArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_TargetArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_TargetArray = { "TargetArray", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterBySize_Parms, TargetArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_TargetArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_TargetArray_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_SizeSource_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_SizeSource = { "SizeSource", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterBySize_Parms, SizeSource), Z_Construct_UEnum_DataprepLibraries_EDataprepSizeSource, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_FilterMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_FilterMode = { "FilterMode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterBySize_Parms, FilterMode), Z_Construct_UEnum_DataprepLibraries_EDataprepSizeFilterMode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_Threshold = { "Threshold", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterBySize_Parms, Threshold), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterBySize_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_TargetArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_TargetArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_SizeSource_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_SizeSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_FilterMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_FilterMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_Threshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Dataprep | Filter" },
		{ "Comment", "/**\n\x09 * Filter the array based on the geometry size.\n\x09 * @param\x09TargetArray       Array of Actors or StaticMeshes to filter. The array will not change.\n\x09 * @param   SizeSource        The reference dimension\n\x09 * @param   FilterMode        How to compare the object size with the threshold\n\x09 * @param   Threshold         Limit value\n\x09 * @return\x09The filtered list.\n\x09 */" },
		{ "DeterminesOutputType", "TargetArray" },
		{ "ModuleRelativePath", "Private/DataprepFilterLibrary.h" },
		{ "ToolTip", "Filter the array based on the geometry size.\n@param       TargetArray       Array of Actors or StaticMeshes to filter. The array will not change.\n@param   SizeSource        The reference dimension\n@param   FilterMode        How to compare the object size with the threshold\n@param   Threshold         Limit value\n@return      The filtered list." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepFilterLibrary, nullptr, "FilterBySize", nullptr, nullptr, sizeof(DataprepFilterLibrary_eventFilterBySize_Parms), Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics
	{
		struct DataprepFilterLibrary_eventFilterByTag_Parms
		{
			TArray<AActor*> TargetArray;
			FName Tag;
			TArray<AActor*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetArray;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Tag;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_TargetArray_Inner = { "TargetArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_TargetArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_TargetArray = { "TargetArray", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterByTag_Parms, TargetArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_TargetArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_TargetArray_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_Tag = { "Tag", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterByTag_Parms, Tag), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataprepFilterLibrary_eventFilterByTag_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_TargetArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_TargetArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_Tag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::Function_MetaDataParams[] = {
		{ "Category", "Dataprep | Filter" },
		{ "Comment", "/**\n\x09 * Filter the array based on a tag.\n\x09 * @param\x09TargetArray\x09\x09""Array of Actors to filter. The array will not change.\n\x09 * @param\x09Tag\x09\x09\x09\x09The tag to filter with.\n\x09 * @return\x09The filtered list.\n\x09 */" },
		{ "DeterminesOutputType", "TargetArray" },
		{ "ModuleRelativePath", "Private/DataprepFilterLibrary.h" },
		{ "ToolTip", "Filter the array based on a tag.\n@param       TargetArray             Array of Actors to filter. The array will not change.\n@param       Tag                             The tag to filter with.\n@return      The filtered list." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataprepFilterLibrary, nullptr, "FilterByTag", nullptr, nullptr, sizeof(DataprepFilterLibrary_eventFilterByTag_Parms), Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDataprepFilterLibrary_NoRegister()
	{
		return UDataprepFilterLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepFilterLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepFilterLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDataprepFilterLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDataprepFilterLibrary_FilterByClass, "FilterByClass" }, // 2037438710
		{ &Z_Construct_UFunction_UDataprepFilterLibrary_FilterByName, "FilterByName" }, // 3331989079
		{ &Z_Construct_UFunction_UDataprepFilterLibrary_FilterBySize, "FilterBySize" }, // 2602153300
		{ &Z_Construct_UFunction_UDataprepFilterLibrary_FilterByTag, "FilterByTag" }, // 3221058053
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepFilterLibrary_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "Datasmith Data Preparation Filter Library" },
		{ "IncludePath", "DataprepFilterLibrary.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/DataprepFilterLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepFilterLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepFilterLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepFilterLibrary_Statics::ClassParams = {
		&UDataprepFilterLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepFilterLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepFilterLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepFilterLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepFilterLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepFilterLibrary, 1441601925);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepFilterLibrary>()
	{
		return UDataprepFilterLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepFilterLibrary(Z_Construct_UClass_UDataprepFilterLibrary, &UDataprepFilterLibrary::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepFilterLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepFilterLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
