// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepEditor/Private/SchemaActions/DataprepSchemaAction.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepSchemaAction() {}
// Cross Module References
	DATAPREPEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FDataprepSchemaAction();
	UPackage* Z_Construct_UPackage__Script_DataprepEditor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphSchemaAction();
// End Cross Module References

static_assert(std::is_polymorphic<FDataprepSchemaAction>() == std::is_polymorphic<FEdGraphSchemaAction>(), "USTRUCT FDataprepSchemaAction cannot be polymorphic unless super FEdGraphSchemaAction is polymorphic");

class UScriptStruct* FDataprepSchemaAction::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAPREPEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FDataprepSchemaAction_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataprepSchemaAction, Z_Construct_UPackage__Script_DataprepEditor(), TEXT("DataprepSchemaAction"), sizeof(FDataprepSchemaAction), Get_Z_Construct_UScriptStruct_FDataprepSchemaAction_Hash());
	}
	return Singleton;
}
template<> DATAPREPEDITOR_API UScriptStruct* StaticStruct<FDataprepSchemaAction>()
{
	return FDataprepSchemaAction::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataprepSchemaAction(FDataprepSchemaAction::StaticStruct, TEXT("/Script/DataprepEditor"), TEXT("DataprepSchemaAction"), false, nullptr, nullptr);
static struct FScriptStruct_DataprepEditor_StaticRegisterNativesFDataprepSchemaAction
{
	FScriptStruct_DataprepEditor_StaticRegisterNativesFDataprepSchemaAction()
	{
		UScriptStruct::DeferCppStructOps<FDataprepSchemaAction>(FName(TEXT("DataprepSchemaAction")));
	}
} ScriptStruct_DataprepEditor_StaticRegisterNativesFDataprepSchemaAction;
	struct Z_Construct_UScriptStruct_FDataprepSchemaAction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataprepSchemaAction_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * The DataprepSchemaAction is the data structure used to populate the Dataprep action menu and the Dataprep palette\n */" },
		{ "ModuleRelativePath", "Private/SchemaActions/DataprepSchemaAction.h" },
		{ "ToolTip", "The DataprepSchemaAction is the data structure used to populate the Dataprep action menu and the Dataprep palette" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataprepSchemaAction_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataprepSchemaAction>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataprepSchemaAction_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepEditor,
		Z_Construct_UScriptStruct_FEdGraphSchemaAction,
		&NewStructOps,
		"DataprepSchemaAction",
		sizeof(FDataprepSchemaAction),
		alignof(FDataprepSchemaAction),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataprepSchemaAction_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataprepSchemaAction_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataprepSchemaAction()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataprepSchemaAction_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataprepSchemaAction"), sizeof(FDataprepSchemaAction), Get_Z_Construct_UScriptStruct_FDataprepSchemaAction_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataprepSchemaAction_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataprepSchemaAction_Hash() { return 4233404628U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
