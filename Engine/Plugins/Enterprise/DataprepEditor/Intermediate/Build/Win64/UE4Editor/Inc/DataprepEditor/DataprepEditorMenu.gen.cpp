// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepEditor/Public/DataprepEditorMenu.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepEditorMenu() {}
// Cross Module References
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepEditorContextMenuContext_NoRegister();
	DATAPREPEDITOR_API UClass* Z_Construct_UClass_UDataprepEditorContextMenuContext();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DataprepEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepAssetInterface_NoRegister();
// End Cross Module References
	void UDataprepEditorContextMenuContext::StaticRegisterNativesUDataprepEditorContextMenuContext()
	{
	}
	UClass* Z_Construct_UClass_UDataprepEditorContextMenuContext_NoRegister()
	{
		return UDataprepEditorContextMenuContext::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SelectedObjects;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataprepAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DataprepAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DataprepEditorMenu.h" },
		{ "ModuleRelativePath", "Public/DataprepEditorMenu.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_SelectedObjects_Inner = { "SelectedObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_SelectedObjects_MetaData[] = {
		{ "ModuleRelativePath", "Public/DataprepEditorMenu.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_SelectedObjects = { "SelectedObjects", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepEditorContextMenuContext, SelectedObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_SelectedObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_SelectedObjects_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_DataprepAsset_MetaData[] = {
		{ "ModuleRelativePath", "Public/DataprepEditorMenu.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_DataprepAsset = { "DataprepAsset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepEditorContextMenuContext, DataprepAsset), Z_Construct_UClass_UDataprepAssetInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_DataprepAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_DataprepAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_SelectedObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_SelectedObjects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::NewProp_DataprepAsset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepEditorContextMenuContext>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::ClassParams = {
		&UDataprepEditorContextMenuContext::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepEditorContextMenuContext()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepEditorContextMenuContext_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepEditorContextMenuContext, 960816866);
	template<> DATAPREPEDITOR_API UClass* StaticClass<UDataprepEditorContextMenuContext>()
	{
		return UDataprepEditorContextMenuContext::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepEditorContextMenuContext(Z_Construct_UClass_UDataprepEditorContextMenuContext, &UDataprepEditorContextMenuContext::StaticClass, TEXT("/Script/DataprepEditor"), TEXT("UDataprepEditorContextMenuContext"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepEditorContextMenuContext);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
