// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPCORE_DataprepStringsArrayFilter_generated_h
#error "DataprepStringsArrayFilter.generated.h already included, missing '#pragma once' in DataprepStringsArrayFilter.h"
#endif
#define DATAPREPCORE_DataprepStringsArrayFilter_generated_h

#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_SPARSE_DATA
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_RPC_WRAPPERS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepStringsArrayFilter(); \
	friend struct Z_Construct_UClass_UDataprepStringsArrayFilter_Statics; \
public: \
	DECLARE_CLASS(UDataprepStringsArrayFilter, UDataprepFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepStringsArrayFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepStringsArrayFilter(); \
	friend struct Z_Construct_UClass_UDataprepStringsArrayFilter_Statics; \
public: \
	DECLARE_CLASS(UDataprepStringsArrayFilter, UDataprepFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepCore"), NO_API) \
	DECLARE_SERIALIZER(UDataprepStringsArrayFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepStringsArrayFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepStringsArrayFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepStringsArrayFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepStringsArrayFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepStringsArrayFilter(UDataprepStringsArrayFilter&&); \
	NO_API UDataprepStringsArrayFilter(const UDataprepStringsArrayFilter&); \
public:


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepStringsArrayFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepStringsArrayFilter(UDataprepStringsArrayFilter&&); \
	NO_API UDataprepStringsArrayFilter(const UDataprepStringsArrayFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepStringsArrayFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepStringsArrayFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepStringsArrayFilter)


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StringMatchingCriteria() { return STRUCT_OFFSET(UDataprepStringsArrayFilter, StringMatchingCriteria); } \
	FORCEINLINE static uint32 __PPO__UserString() { return STRUCT_OFFSET(UDataprepStringsArrayFilter, UserString); } \
	FORCEINLINE static uint32 __PPO__UserStringArray() { return STRUCT_OFFSET(UDataprepStringsArrayFilter, UserStringArray); } \
	FORCEINLINE static uint32 __PPO__bMatchInArray() { return STRUCT_OFFSET(UDataprepStringsArrayFilter, bMatchInArray); } \
	FORCEINLINE static uint32 __PPO__StringsArrayFetcher() { return STRUCT_OFFSET(UDataprepStringsArrayFilter, StringsArrayFetcher); }


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_11_PROLOG
#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_RPC_WRAPPERS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_INCLASS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_SPARSE_DATA \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPCORE_API UClass* StaticClass<class UDataprepStringsArrayFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Enterprise_DataprepEditor_Source_DataprepCore_Public_SelectionSystem_DataprepStringsArrayFilter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
