// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataprepLibraries/Public/Fetchers/DataprepBoolFetcherLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepBoolFetcherLibrary() {}
// Cross Module References
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepIsClassOfFetcher_NoRegister();
	DATAPREPLIBRARIES_API UClass* Z_Construct_UClass_UDataprepIsClassOfFetcher();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepBoolFetcher();
	UPackage* Z_Construct_UPackage__Script_DataprepLibraries();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UDataprepIsClassOfFetcher::StaticRegisterNativesUDataprepIsClassOfFetcher()
	{
	}
	UClass* Z_Construct_UClass_UDataprepIsClassOfFetcher_NoRegister()
	{
		return UDataprepIsClassOfFetcher::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Class;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldIncludeChildClass_MetaData[];
#endif
		static void NewProp_bShouldIncludeChildClass_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldIncludeChildClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepBoolFetcher,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepLibraries,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Is Class Of" },
		{ "IncludePath", "Fetchers/DataprepBoolFetcherLibrary.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/Fetchers/DataprepBoolFetcherLibrary.h" },
		{ "ToolTip", "Filter objects based of their selected class." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_Class_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// The key for the for the string\n" },
		{ "ModuleRelativePath", "Public/Fetchers/DataprepBoolFetcherLibrary.h" },
		{ "ToolTip", "The key for the for the string" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0014000000000805, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepIsClassOfFetcher, Class), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_Class_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_bShouldIncludeChildClass_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/Fetchers/DataprepBoolFetcherLibrary.h" },
	};
#endif
	void Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_bShouldIncludeChildClass_SetBit(void* Obj)
	{
		((UDataprepIsClassOfFetcher*)Obj)->bShouldIncludeChildClass = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_bShouldIncludeChildClass = { "bShouldIncludeChildClass", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepIsClassOfFetcher), &Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_bShouldIncludeChildClass_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_bShouldIncludeChildClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_bShouldIncludeChildClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::NewProp_bShouldIncludeChildClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepIsClassOfFetcher>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::ClassParams = {
		&UDataprepIsClassOfFetcher::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepIsClassOfFetcher()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepIsClassOfFetcher_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepIsClassOfFetcher, 4021004083);
	template<> DATAPREPLIBRARIES_API UClass* StaticClass<UDataprepIsClassOfFetcher>()
	{
		return UDataprepIsClassOfFetcher::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepIsClassOfFetcher(Z_Construct_UClass_UDataprepIsClassOfFetcher, &UDataprepIsClassOfFetcher::StaticClass, TEXT("/Script/DataprepLibraries"), TEXT("UDataprepIsClassOfFetcher"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepIsClassOfFetcher);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
