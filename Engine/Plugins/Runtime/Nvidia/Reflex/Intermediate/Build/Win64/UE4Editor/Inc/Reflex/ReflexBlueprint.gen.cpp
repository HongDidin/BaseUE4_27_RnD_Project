// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/ReflexBlueprint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReflexBlueprint() {}
// Cross Module References
	REFLEX_API UEnum* Z_Construct_UEnum_Reflex_EReflexMode();
	UPackage* Z_Construct_UPackage__Script_Reflex();
	REFLEX_API UClass* Z_Construct_UClass_UReflexBlueprintLibrary_NoRegister();
	REFLEX_API UClass* Z_Construct_UClass_UReflexBlueprintLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
// End Cross Module References
	static UEnum* EReflexMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Reflex_EReflexMode, Z_Construct_UPackage__Script_Reflex(), TEXT("EReflexMode"));
		}
		return Singleton;
	}
	template<> REFLEX_API UEnum* StaticEnum<EReflexMode>()
	{
		return EReflexMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EReflexMode(EReflexMode_StaticEnum, TEXT("/Script/Reflex"), TEXT("EReflexMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Reflex_EReflexMode_Hash() { return 3283731294U; }
	UEnum* Z_Construct_UEnum_Reflex_EReflexMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Reflex();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EReflexMode"), 0, Get_Z_Construct_UEnum_Reflex_EReflexMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EReflexMode::Disabled", (int64)EReflexMode::Disabled },
				{ "EReflexMode::Enabled", (int64)EReflexMode::Enabled },
				{ "EReflexMode::EnabledPlusBoost", (int64)EReflexMode::EnabledPlusBoost },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Disabled.DisplayName", "Disabled" },
				{ "Disabled.Name", "EReflexMode::Disabled" },
				{ "Enabled.DisplayName", "Enabled" },
				{ "Enabled.Name", "EReflexMode::Enabled" },
				{ "EnabledPlusBoost.DisplayName", "Enabled + Boost" },
				{ "EnabledPlusBoost.Name", "EReflexMode::EnabledPlusBoost" },
				{ "ModuleRelativePath", "Public/ReflexBlueprint.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Reflex,
				nullptr,
				"EReflexMode",
				"EReflexMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UReflexBlueprintLibrary::execGetRenderLatencyInMs)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UReflexBlueprintLibrary::GetRenderLatencyInMs();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReflexBlueprintLibrary::execGetGameLatencyInMs)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UReflexBlueprintLibrary::GetGameLatencyInMs();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReflexBlueprintLibrary::execGetGameToRenderLatencyInMs)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UReflexBlueprintLibrary::GetGameToRenderLatencyInMs();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReflexBlueprintLibrary::execGetFlashIndicatorEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UReflexBlueprintLibrary::GetFlashIndicatorEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReflexBlueprintLibrary::execSetFlashIndicatorEnabled)
	{
		P_GET_UBOOL(Z_Param_bEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		UReflexBlueprintLibrary::SetFlashIndicatorEnabled(Z_Param_bEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReflexBlueprintLibrary::execGetReflexMode)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EReflexMode*)Z_Param__Result=UReflexBlueprintLibrary::GetReflexMode();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReflexBlueprintLibrary::execSetReflexMode)
	{
		P_GET_ENUM(EReflexMode,Z_Param_Mode);
		P_FINISH;
		P_NATIVE_BEGIN;
		UReflexBlueprintLibrary::SetReflexMode(EReflexMode(Z_Param_Mode));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReflexBlueprintLibrary::execGetReflexAvailable)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UReflexBlueprintLibrary::GetReflexAvailable();
		P_NATIVE_END;
	}
	void UReflexBlueprintLibrary::StaticRegisterNativesUReflexBlueprintLibrary()
	{
		UClass* Class = UReflexBlueprintLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetFlashIndicatorEnabled", &UReflexBlueprintLibrary::execGetFlashIndicatorEnabled },
			{ "GetGameLatencyInMs", &UReflexBlueprintLibrary::execGetGameLatencyInMs },
			{ "GetGameToRenderLatencyInMs", &UReflexBlueprintLibrary::execGetGameToRenderLatencyInMs },
			{ "GetReflexAvailable", &UReflexBlueprintLibrary::execGetReflexAvailable },
			{ "GetReflexMode", &UReflexBlueprintLibrary::execGetReflexMode },
			{ "GetRenderLatencyInMs", &UReflexBlueprintLibrary::execGetRenderLatencyInMs },
			{ "SetFlashIndicatorEnabled", &UReflexBlueprintLibrary::execSetFlashIndicatorEnabled },
			{ "SetReflexMode", &UReflexBlueprintLibrary::execSetReflexMode },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics
	{
		struct ReflexBlueprintLibrary_eventGetFlashIndicatorEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ReflexBlueprintLibrary_eventGetFlashIndicatorEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ReflexBlueprintLibrary_eventGetFlashIndicatorEnabled_Parms), &Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Reflex" },
		{ "ModuleRelativePath", "Public/ReflexBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReflexBlueprintLibrary, nullptr, "GetFlashIndicatorEnabled", nullptr, nullptr, sizeof(ReflexBlueprintLibrary_eventGetFlashIndicatorEnabled_Parms), Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics
	{
		struct ReflexBlueprintLibrary_eventGetGameLatencyInMs_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReflexBlueprintLibrary_eventGetGameLatencyInMs_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Reflex" },
		{ "ModuleRelativePath", "Public/ReflexBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReflexBlueprintLibrary, nullptr, "GetGameLatencyInMs", nullptr, nullptr, sizeof(ReflexBlueprintLibrary_eventGetGameLatencyInMs_Parms), Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics
	{
		struct ReflexBlueprintLibrary_eventGetGameToRenderLatencyInMs_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReflexBlueprintLibrary_eventGetGameToRenderLatencyInMs_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Reflex" },
		{ "ModuleRelativePath", "Public/ReflexBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReflexBlueprintLibrary, nullptr, "GetGameToRenderLatencyInMs", nullptr, nullptr, sizeof(ReflexBlueprintLibrary_eventGetGameToRenderLatencyInMs_Parms), Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics
	{
		struct ReflexBlueprintLibrary_eventGetReflexAvailable_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ReflexBlueprintLibrary_eventGetReflexAvailable_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ReflexBlueprintLibrary_eventGetReflexAvailable_Parms), &Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::Function_MetaDataParams[] = {
		{ "Category", "Reflex" },
		{ "ModuleRelativePath", "Public/ReflexBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReflexBlueprintLibrary, nullptr, "GetReflexAvailable", nullptr, nullptr, sizeof(ReflexBlueprintLibrary_eventGetReflexAvailable_Parms), Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics
	{
		struct ReflexBlueprintLibrary_eventGetReflexMode_Parms
		{
			EReflexMode ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReflexBlueprintLibrary_eventGetReflexMode_Parms, ReturnValue), Z_Construct_UEnum_Reflex_EReflexMode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "Reflex" },
		{ "ModuleRelativePath", "Public/ReflexBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReflexBlueprintLibrary, nullptr, "GetReflexMode", nullptr, nullptr, sizeof(ReflexBlueprintLibrary_eventGetReflexMode_Parms), Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics
	{
		struct ReflexBlueprintLibrary_eventGetRenderLatencyInMs_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReflexBlueprintLibrary_eventGetRenderLatencyInMs_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Reflex" },
		{ "ModuleRelativePath", "Public/ReflexBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReflexBlueprintLibrary, nullptr, "GetRenderLatencyInMs", nullptr, nullptr, sizeof(ReflexBlueprintLibrary_eventGetRenderLatencyInMs_Parms), Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics
	{
		struct ReflexBlueprintLibrary_eventSetFlashIndicatorEnabled_Parms
		{
			bool bEnabled;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::NewProp_bEnabled_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((ReflexBlueprintLibrary_eventSetFlashIndicatorEnabled_Parms*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ReflexBlueprintLibrary_eventSetFlashIndicatorEnabled_Parms), &Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::NewProp_bEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::NewProp_bEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Reflex" },
		{ "ModuleRelativePath", "Public/ReflexBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReflexBlueprintLibrary, nullptr, "SetFlashIndicatorEnabled", nullptr, nullptr, sizeof(ReflexBlueprintLibrary_eventSetFlashIndicatorEnabled_Parms), Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics
	{
		struct ReflexBlueprintLibrary_eventSetReflexMode_Parms
		{
			EReflexMode Mode;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Mode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Mode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::NewProp_Mode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::NewProp_Mode_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::NewProp_Mode = { "Mode", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReflexBlueprintLibrary_eventSetReflexMode_Parms, Mode), Z_Construct_UEnum_Reflex_EReflexMode, METADATA_PARAMS(Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::NewProp_Mode_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::NewProp_Mode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::NewProp_Mode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::NewProp_Mode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "Reflex" },
		{ "ModuleRelativePath", "Public/ReflexBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReflexBlueprintLibrary, nullptr, "SetReflexMode", nullptr, nullptr, sizeof(ReflexBlueprintLibrary_eventSetReflexMode_Parms), Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UReflexBlueprintLibrary_NoRegister()
	{
		return UReflexBlueprintLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UReflexBlueprintLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReflexBlueprintLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_Reflex,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UReflexBlueprintLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UReflexBlueprintLibrary_GetFlashIndicatorEnabled, "GetFlashIndicatorEnabled" }, // 460620724
		{ &Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameLatencyInMs, "GetGameLatencyInMs" }, // 3071956279
		{ &Z_Construct_UFunction_UReflexBlueprintLibrary_GetGameToRenderLatencyInMs, "GetGameToRenderLatencyInMs" }, // 438510391
		{ &Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexAvailable, "GetReflexAvailable" }, // 3879633133
		{ &Z_Construct_UFunction_UReflexBlueprintLibrary_GetReflexMode, "GetReflexMode" }, // 587348842
		{ &Z_Construct_UFunction_UReflexBlueprintLibrary_GetRenderLatencyInMs, "GetRenderLatencyInMs" }, // 3482762878
		{ &Z_Construct_UFunction_UReflexBlueprintLibrary_SetFlashIndicatorEnabled, "SetFlashIndicatorEnabled" }, // 4218882476
		{ &Z_Construct_UFunction_UReflexBlueprintLibrary_SetReflexMode, "SetReflexMode" }, // 922010310
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReflexBlueprintLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ReflexBlueprint.h" },
		{ "ModuleRelativePath", "Public/ReflexBlueprint.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReflexBlueprintLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReflexBlueprintLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReflexBlueprintLibrary_Statics::ClassParams = {
		&UReflexBlueprintLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UReflexBlueprintLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReflexBlueprintLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReflexBlueprintLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReflexBlueprintLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReflexBlueprintLibrary, 53489257);
	template<> REFLEX_API UClass* StaticClass<UReflexBlueprintLibrary>()
	{
		return UReflexBlueprintLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReflexBlueprintLibrary(Z_Construct_UClass_UReflexBlueprintLibrary, &UReflexBlueprintLibrary::StaticClass, TEXT("/Script/Reflex"), TEXT("UReflexBlueprintLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReflexBlueprintLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
