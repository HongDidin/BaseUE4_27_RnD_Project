// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class APhysicsVolume;
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef NETWORKPREDICTIONEXTRAS_BaseMovementComponent_generated_h
#error "BaseMovementComponent.generated.h already included, missing '#pragma once' in BaseMovementComponent.h"
#endif
#define NETWORKPREDICTIONEXTRAS_BaseMovementComponent_generated_h

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPhysicsVolumeChanged); \
	DECLARE_FUNCTION(execOnBeginOverlap);


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPhysicsVolumeChanged); \
	DECLARE_FUNCTION(execOnBeginOverlap);


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBaseMovementComponent(); \
	friend struct Z_Construct_UClass_UBaseMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UBaseMovementComponent, UNetworkPredictionComponent, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UBaseMovementComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUBaseMovementComponent(); \
	friend struct Z_Construct_UClass_UBaseMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UBaseMovementComponent, UNetworkPredictionComponent, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UBaseMovementComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBaseMovementComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBaseMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseMovementComponent(UBaseMovementComponent&&); \
	NO_API UBaseMovementComponent(const UBaseMovementComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseMovementComponent(UBaseMovementComponent&&); \
	NO_API UBaseMovementComponent(const UBaseMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseMovementComponent); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UBaseMovementComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__UpdatedComponent() { return STRUCT_OFFSET(UBaseMovementComponent, UpdatedComponent); } \
	FORCEINLINE static uint32 __PPO__UpdatedPrimitive() { return STRUCT_OFFSET(UBaseMovementComponent, UpdatedPrimitive); }


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_13_PROLOG
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_INCLASS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<class UBaseMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_BaseMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
