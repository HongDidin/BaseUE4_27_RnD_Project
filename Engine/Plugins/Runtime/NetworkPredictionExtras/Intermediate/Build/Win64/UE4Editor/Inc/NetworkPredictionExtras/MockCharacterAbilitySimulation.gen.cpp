// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPredictionExtras/Public/MockCharacterAbilitySimulation.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMockCharacterAbilitySimulation() {}
// Cross Module References
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockCharacterAbilityComponent();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockCharacterAbilityComponent_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UCharacterMotionComponent();
	UPackage* Z_Construct_UPackage__Script_NetworkPredictionExtras();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockCharacterAbilityComponent, nullptr, "MockCharacterAbilityBlinkCueRollback__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics
	{
		struct MockCharacterAbilityComponent_eventMockCharacterAbilityBlinkCueEvent_Parms
		{
			FVector DestinationLocation;
			int32 RandomValue;
			float ElapsedTimeSeconds;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DestinationLocation;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RandomValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElapsedTimeSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_DestinationLocation = { "DestinationLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockCharacterAbilityComponent_eventMockCharacterAbilityBlinkCueEvent_Parms, DestinationLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_RandomValue = { "RandomValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockCharacterAbilityComponent_eventMockCharacterAbilityBlinkCueEvent_Parms, RandomValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_ElapsedTimeSeconds = { "ElapsedTimeSeconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockCharacterAbilityComponent_eventMockCharacterAbilityBlinkCueEvent_Parms, ElapsedTimeSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_DestinationLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_RandomValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_ElapsedTimeSeconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Blueprint assignable events for blinking. This allows the user/blueprint to implement rollback-able events\n" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "Blueprint assignable events for blinking. This allows the user/blueprint to implement rollback-able events" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockCharacterAbilityComponent, nullptr, "MockCharacterAbilityBlinkCueEvent__DelegateSignature", nullptr, nullptr, sizeof(MockCharacterAbilityComponent_eventMockCharacterAbilityBlinkCueEvent_Parms), Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00930000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics
	{
		struct MockCharacterAbilityComponent_eventMockCharacterAbilityNotifyStateChange_Parms
		{
			bool bNewStateValue;
		};
		static void NewProp_bNewStateValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNewStateValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue_SetBit(void* Obj)
	{
		((MockCharacterAbilityComponent_eventMockCharacterAbilityNotifyStateChange_Parms*)Obj)->bNewStateValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue = { "bNewStateValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockCharacterAbilityComponent_eventMockCharacterAbilityNotifyStateChange_Parms), &Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/*\n\x09// NetSimCues\n\x09void HandleCue(const FMockCharacterAbilityBlinkActivateCue& BlinkCue, const FNetSimCueSystemParamemters& SystemParameters);\n\x09void HandleCue(const FMockCharacterAbilityBlinkCue& BlinkCue, const FNetSimCueSystemParamemters& SystemParameters);\n\x09void HandleCue(const FMockCharacterAbilityPhysicsGunFireCue& FireCue, const FNetSimCueSystemParamemters& SystemParameters);\n\x09*/// -------------------------------------------------------------------------------------\n//\x09""Ability State and Notifications\n//\x09\x09-This allows user code/blueprints to respond to state changes.\n//\x09\x09-These values always reflect the latest simulation state\n//\x09\x09-StateChange events are just that: when the state changes. They are not emitted from the sim themselves.\n//\x09\x09\x09-This means they \"work\" for interpolated simulations and are resilient to packet loss and crazy network conditions\n//\x09\x09\x09-That said, its \"latest\" only. There is NO guarantee that you will receive every state transition\n//\n// -------------------------------------------------------------------------------------\n" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "// NetSimCues\nvoid HandleCue(const FMockCharacterAbilityBlinkActivateCue& BlinkCue, const FNetSimCueSystemParamemters& SystemParameters);\nvoid HandleCue(const FMockCharacterAbilityBlinkCue& BlinkCue, const FNetSimCueSystemParamemters& SystemParameters);\nvoid HandleCue(const FMockCharacterAbilityPhysicsGunFireCue& FireCue, const FNetSimCueSystemParamemters& SystemParameters);\n// -------------------------------------------------------------------------------------\n//     Ability State and Notifications\n//             -This allows user code/blueprints to respond to state changes.\n//             -These values always reflect the latest simulation state\n//             -StateChange events are just that: when the state changes. They are not emitted from the sim themselves.\n//                     -This means they \"work\" for interpolated simulations and are resilient to packet loss and crazy network conditions\n//                     -That said, its \"latest\" only. There is NO guarantee that you will receive every state transition\n//\n// -------------------------------------------------------------------------------------" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockCharacterAbilityComponent, nullptr, "MockCharacterAbilityNotifyStateChange__DelegateSignature", nullptr, nullptr, sizeof(MockCharacterAbilityComponent_eventMockCharacterAbilityNotifyStateChange_Parms), Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UMockCharacterAbilityComponent::execGetMaxStamina)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetMaxStamina();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockCharacterAbilityComponent::execGetStamina)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetStamina();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockCharacterAbilityComponent::execGetBlinkWarmupTimeSeconds)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetBlinkWarmupTimeSeconds();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockCharacterAbilityComponent::execIsJumping)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsJumping();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockCharacterAbilityComponent::execIsBlinking)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsBlinking();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockCharacterAbilityComponent::execIsDashing)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsDashing();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockCharacterAbilityComponent::execIsSprinting)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsSprinting();
		P_NATIVE_END;
	}
	void UMockCharacterAbilityComponent::StaticRegisterNativesUMockCharacterAbilityComponent()
	{
		UClass* Class = UMockCharacterAbilityComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetBlinkWarmupTimeSeconds", &UMockCharacterAbilityComponent::execGetBlinkWarmupTimeSeconds },
			{ "GetMaxStamina", &UMockCharacterAbilityComponent::execGetMaxStamina },
			{ "GetStamina", &UMockCharacterAbilityComponent::execGetStamina },
			{ "IsBlinking", &UMockCharacterAbilityComponent::execIsBlinking },
			{ "IsDashing", &UMockCharacterAbilityComponent::execIsDashing },
			{ "IsJumping", &UMockCharacterAbilityComponent::execIsJumping },
			{ "IsSprinting", &UMockCharacterAbilityComponent::execIsSprinting },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics
	{
		struct MockCharacterAbilityComponent_eventGetBlinkWarmupTimeSeconds_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockCharacterAbilityComponent_eventGetBlinkWarmupTimeSeconds_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockCharacterAbilityComponent, nullptr, "GetBlinkWarmupTimeSeconds", nullptr, nullptr, sizeof(MockCharacterAbilityComponent_eventGetBlinkWarmupTimeSeconds_Parms), Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics
	{
		struct MockCharacterAbilityComponent_eventGetMaxStamina_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockCharacterAbilityComponent_eventGetMaxStamina_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock Ability" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockCharacterAbilityComponent, nullptr, "GetMaxStamina", nullptr, nullptr, sizeof(MockCharacterAbilityComponent_eventGetMaxStamina_Parms), Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics
	{
		struct MockCharacterAbilityComponent_eventGetStamina_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockCharacterAbilityComponent_eventGetStamina_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock Ability" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockCharacterAbilityComponent, nullptr, "GetStamina", nullptr, nullptr, sizeof(MockCharacterAbilityComponent_eventGetStamina_Parms), Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics
	{
		struct MockCharacterAbilityComponent_eventIsBlinking_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MockCharacterAbilityComponent_eventIsBlinking_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockCharacterAbilityComponent_eventIsBlinking_Parms), &Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Are we currently in the blinking (startup) state\n" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "Are we currently in the blinking (startup) state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockCharacterAbilityComponent, nullptr, "IsBlinking", nullptr, nullptr, sizeof(MockCharacterAbilityComponent_eventIsBlinking_Parms), Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics
	{
		struct MockCharacterAbilityComponent_eventIsDashing_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MockCharacterAbilityComponent_eventIsDashing_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockCharacterAbilityComponent_eventIsDashing_Parms), &Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Are we currently in the dashing state\n" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "Are we currently in the dashing state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockCharacterAbilityComponent, nullptr, "IsDashing", nullptr, nullptr, sizeof(MockCharacterAbilityComponent_eventIsDashing_Parms), Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics
	{
		struct MockCharacterAbilityComponent_eventIsJumping_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MockCharacterAbilityComponent_eventIsJumping_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockCharacterAbilityComponent_eventIsJumping_Parms), &Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Are we currently in the jumping state\n" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "Are we currently in the jumping state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockCharacterAbilityComponent, nullptr, "IsJumping", nullptr, nullptr, sizeof(MockCharacterAbilityComponent_eventIsJumping_Parms), Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics
	{
		struct MockCharacterAbilityComponent_eventIsSprinting_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MockCharacterAbilityComponent_eventIsSprinting_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockCharacterAbilityComponent_eventIsSprinting_Parms), &Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Are we currently in the sprinting state\n" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "Are we currently in the sprinting state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockCharacterAbilityComponent, nullptr, "IsSprinting", nullptr, nullptr, sizeof(MockCharacterAbilityComponent_eventIsSprinting_Parms), Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMockCharacterAbilityComponent_NoRegister()
	{
		return UMockCharacterAbilityComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMockCharacterAbilityComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSprintStateChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSprintStateChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnDashStateChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnDashStateChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnBlinkStateChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnBlinkStateChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnJumpStateChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnJumpStateChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnBlinkActivateEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnBlinkActivateEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnBlinkActivateEventRollback_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnBlinkActivateEventRollback;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCharacterMotionComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMockCharacterAbilityComponent_GetBlinkWarmupTimeSeconds, "GetBlinkWarmupTimeSeconds" }, // 2059849953
		{ &Z_Construct_UFunction_UMockCharacterAbilityComponent_GetMaxStamina, "GetMaxStamina" }, // 443898829
		{ &Z_Construct_UFunction_UMockCharacterAbilityComponent_GetStamina, "GetStamina" }, // 2301933611
		{ &Z_Construct_UFunction_UMockCharacterAbilityComponent_IsBlinking, "IsBlinking" }, // 4173424333
		{ &Z_Construct_UFunction_UMockCharacterAbilityComponent_IsDashing, "IsDashing" }, // 1995538885
		{ &Z_Construct_UFunction_UMockCharacterAbilityComponent_IsJumping, "IsJumping" }, // 1444466387
		{ &Z_Construct_UFunction_UMockCharacterAbilityComponent_IsSprinting, "IsSprinting" }, // 2553158699
		{ &Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature, "MockCharacterAbilityBlinkCueEvent__DelegateSignature" }, // 4132189188
		{ &Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature, "MockCharacterAbilityBlinkCueRollback__DelegateSignature" }, // 46935163
		{ &Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature, "MockCharacterAbilityNotifyStateChange__DelegateSignature" }, // 1165914669
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "Comment", "// -------------------------------------------------------------------------------------------------------------------------------\n// ActorComponent for running Mock Ability Simulation for CharacterMotion example\n// -------------------------------------------------------------------------------------------------------------------------------\n" },
		{ "IncludePath", "MockCharacterAbilitySimulation.h" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "ActorComponent for running Mock Ability Simulation for CharacterMotion example" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnSprintStateChange_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Notifies when Sprint state changes\n" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "Notifies when Sprint state changes" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnSprintStateChange = { "OnSprintStateChange", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockCharacterAbilityComponent, OnSprintStateChange), Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnSprintStateChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnSprintStateChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnDashStateChange_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Notifies when Dash state changes\n" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "Notifies when Dash state changes" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnDashStateChange = { "OnDashStateChange", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockCharacterAbilityComponent, OnDashStateChange), Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnDashStateChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnDashStateChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkStateChange_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Notifies when Blink Changes\n" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "Notifies when Blink Changes" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkStateChange = { "OnBlinkStateChange", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockCharacterAbilityComponent, OnBlinkStateChange), Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkStateChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkStateChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnJumpStateChange_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Notifies when Jump Changes\n" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
		{ "ToolTip", "Notifies when Jump Changes" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnJumpStateChange = { "OnJumpStateChange", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockCharacterAbilityComponent, OnJumpStateChange), Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnJumpStateChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnJumpStateChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkActivateEvent_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkActivateEvent = { "OnBlinkActivateEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockCharacterAbilityComponent, OnBlinkActivateEvent), Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkActivateEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkActivateEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkActivateEventRollback_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "ModuleRelativePath", "Public/MockCharacterAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkActivateEventRollback = { "OnBlinkActivateEventRollback", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockCharacterAbilityComponent, OnBlinkActivateEventRollback), Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkActivateEventRollback_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkActivateEventRollback_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnSprintStateChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnDashStateChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkStateChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnJumpStateChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkActivateEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::NewProp_OnBlinkActivateEventRollback,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockCharacterAbilityComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::ClassParams = {
		&UMockCharacterAbilityComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockCharacterAbilityComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockCharacterAbilityComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockCharacterAbilityComponent, 2723028458);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UMockCharacterAbilityComponent>()
	{
		return UMockCharacterAbilityComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockCharacterAbilityComponent(Z_Construct_UClass_UMockCharacterAbilityComponent, &UMockCharacterAbilityComponent::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UMockCharacterAbilityComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockCharacterAbilityComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
