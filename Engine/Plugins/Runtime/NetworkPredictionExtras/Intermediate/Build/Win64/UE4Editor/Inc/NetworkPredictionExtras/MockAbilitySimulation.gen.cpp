// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPredictionExtras/Public/MockAbilitySimulation.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMockAbilitySimulation() {}
// Cross Module References
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockFlyingAbilityComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FVector_NetQuantize100();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockFlyingAbilityComponent_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UFlyingMovementComponent();
	UPackage* Z_Construct_UPackage__Script_NetworkPredictionExtras();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics
	{
		struct MockFlyingAbilityComponent_eventMockAbilityPhysicsGunFireEvent_Parms
		{
			FVector Start;
			FVector End;
			bool bHasCooldown;
			TArray<FVector_NetQuantize100> HitLocations;
			float ElapsedTimeSeconds;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Start;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_End;
		static void NewProp_bHasCooldown_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHasCooldown;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitLocations_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitLocations_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HitLocations;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElapsedTimeSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_Start = { "Start", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockFlyingAbilityComponent_eventMockAbilityPhysicsGunFireEvent_Parms, Start), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_End = { "End", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockFlyingAbilityComponent_eventMockAbilityPhysicsGunFireEvent_Parms, End), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_bHasCooldown_SetBit(void* Obj)
	{
		((MockFlyingAbilityComponent_eventMockAbilityPhysicsGunFireEvent_Parms*)Obj)->bHasCooldown = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_bHasCooldown = { "bHasCooldown", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockFlyingAbilityComponent_eventMockAbilityPhysicsGunFireEvent_Parms), &Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_bHasCooldown_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_HitLocations_Inner = { "HitLocations", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector_NetQuantize100, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_HitLocations_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_HitLocations = { "HitLocations", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockFlyingAbilityComponent_eventMockAbilityPhysicsGunFireEvent_Parms, HitLocations), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_HitLocations_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_HitLocations_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_ElapsedTimeSeconds = { "ElapsedTimeSeconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockFlyingAbilityComponent_eventMockAbilityPhysicsGunFireEvent_Parms, ElapsedTimeSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_Start,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_End,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_bHasCooldown,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_HitLocations_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_HitLocations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::NewProp_ElapsedTimeSeconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockFlyingAbilityComponent, nullptr, "MockAbilityPhysicsGunFireEvent__DelegateSignature", nullptr, nullptr, sizeof(MockFlyingAbilityComponent_eventMockAbilityPhysicsGunFireEvent_Parms), Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00D30000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockFlyingAbilityComponent, nullptr, "MockAbilityBlinkCueRollback__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics
	{
		struct MockFlyingAbilityComponent_eventMockAbilityBlinkCueEvent_Parms
		{
			FVector DestinationLocation;
			int32 RandomValue;
			float ElapsedTimeSeconds;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DestinationLocation;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RandomValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElapsedTimeSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_DestinationLocation = { "DestinationLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockFlyingAbilityComponent_eventMockAbilityBlinkCueEvent_Parms, DestinationLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_RandomValue = { "RandomValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockFlyingAbilityComponent_eventMockAbilityBlinkCueEvent_Parms, RandomValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_ElapsedTimeSeconds = { "ElapsedTimeSeconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockFlyingAbilityComponent_eventMockAbilityBlinkCueEvent_Parms, ElapsedTimeSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_DestinationLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_RandomValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::NewProp_ElapsedTimeSeconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Blueprint assignable events for blinking. THis allows the user/blueprint to implement rollback-able events\n" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
		{ "ToolTip", "Blueprint assignable events for blinking. THis allows the user/blueprint to implement rollback-able events" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockFlyingAbilityComponent, nullptr, "MockAbilityBlinkCueEvent__DelegateSignature", nullptr, nullptr, sizeof(MockFlyingAbilityComponent_eventMockAbilityBlinkCueEvent_Parms), Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00930000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics
	{
		struct MockFlyingAbilityComponent_eventMockAbilityNotifyStateChange_Parms
		{
			bool bNewStateValue;
		};
		static void NewProp_bNewStateValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNewStateValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue_SetBit(void* Obj)
	{
		((MockFlyingAbilityComponent_eventMockAbilityNotifyStateChange_Parms*)Obj)->bNewStateValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue = { "bNewStateValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockFlyingAbilityComponent_eventMockAbilityNotifyStateChange_Parms), &Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// -------------------------------------------------------------------------------------\n//\x09""Ability State and Notifications\n//\x09\x09-This allows user code/blueprints to respond to state changes.\n//\x09\x09-These values always reflect the latest simulation state\n//\x09\x09-StateChange events are just that: when the state changes. They are not emitted from the sim themselves.\n//\x09\x09\x09-This means they \"work\" for interpolated simulations and are resilient to packet loss and crazy network conditions\n//\x09\x09\x09-That said, its \"latest\" only. There is NO guarantee that you will receive every state transition\n//\n// -------------------------------------------------------------------------------------\n" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
		{ "ToolTip", "Ability State and Notifications\n        -This allows user code/blueprints to respond to state changes.\n        -These values always reflect the latest simulation state\n        -StateChange events are just that: when the state changes. They are not emitted from the sim themselves.\n                -This means they \"work\" for interpolated simulations and are resilient to packet loss and crazy network conditions\n                -That said, its \"latest\" only. There is NO guarantee that you will receive every state transition" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockFlyingAbilityComponent, nullptr, "MockAbilityNotifyStateChange__DelegateSignature", nullptr, nullptr, sizeof(MockFlyingAbilityComponent_eventMockAbilityNotifyStateChange_Parms), Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UMockFlyingAbilityComponent::execGetMaxStamina)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetMaxStamina();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockFlyingAbilityComponent::execGetStamina)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetStamina();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockFlyingAbilityComponent::execGetBlinkWarmupTimeSeconds)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetBlinkWarmupTimeSeconds();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockFlyingAbilityComponent::execIsBlinking)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsBlinking();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockFlyingAbilityComponent::execIsDashing)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsDashing();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockFlyingAbilityComponent::execIsSprinting)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsSprinting();
		P_NATIVE_END;
	}
	void UMockFlyingAbilityComponent::StaticRegisterNativesUMockFlyingAbilityComponent()
	{
		UClass* Class = UMockFlyingAbilityComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetBlinkWarmupTimeSeconds", &UMockFlyingAbilityComponent::execGetBlinkWarmupTimeSeconds },
			{ "GetMaxStamina", &UMockFlyingAbilityComponent::execGetMaxStamina },
			{ "GetStamina", &UMockFlyingAbilityComponent::execGetStamina },
			{ "IsBlinking", &UMockFlyingAbilityComponent::execIsBlinking },
			{ "IsDashing", &UMockFlyingAbilityComponent::execIsDashing },
			{ "IsSprinting", &UMockFlyingAbilityComponent::execIsSprinting },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics
	{
		struct MockFlyingAbilityComponent_eventGetBlinkWarmupTimeSeconds_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockFlyingAbilityComponent_eventGetBlinkWarmupTimeSeconds_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockFlyingAbilityComponent, nullptr, "GetBlinkWarmupTimeSeconds", nullptr, nullptr, sizeof(MockFlyingAbilityComponent_eventGetBlinkWarmupTimeSeconds_Parms), Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics
	{
		struct MockFlyingAbilityComponent_eventGetMaxStamina_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockFlyingAbilityComponent_eventGetMaxStamina_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock Ability" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockFlyingAbilityComponent, nullptr, "GetMaxStamina", nullptr, nullptr, sizeof(MockFlyingAbilityComponent_eventGetMaxStamina_Parms), Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics
	{
		struct MockFlyingAbilityComponent_eventGetStamina_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockFlyingAbilityComponent_eventGetStamina_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock Ability" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockFlyingAbilityComponent, nullptr, "GetStamina", nullptr, nullptr, sizeof(MockFlyingAbilityComponent_eventGetStamina_Parms), Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics
	{
		struct MockFlyingAbilityComponent_eventIsBlinking_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MockFlyingAbilityComponent_eventIsBlinking_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockFlyingAbilityComponent_eventIsBlinking_Parms), &Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Are we currently in the blinking (startup) state\n" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
		{ "ToolTip", "Are we currently in the blinking (startup) state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockFlyingAbilityComponent, nullptr, "IsBlinking", nullptr, nullptr, sizeof(MockFlyingAbilityComponent_eventIsBlinking_Parms), Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics
	{
		struct MockFlyingAbilityComponent_eventIsDashing_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MockFlyingAbilityComponent_eventIsDashing_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockFlyingAbilityComponent_eventIsDashing_Parms), &Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Are we currently in the dashing state\n" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
		{ "ToolTip", "Are we currently in the dashing state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockFlyingAbilityComponent, nullptr, "IsDashing", nullptr, nullptr, sizeof(MockFlyingAbilityComponent_eventIsDashing_Parms), Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics
	{
		struct MockFlyingAbilityComponent_eventIsSprinting_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MockFlyingAbilityComponent_eventIsSprinting_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockFlyingAbilityComponent_eventIsSprinting_Parms), &Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Are we currently in the sprinting state\n" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
		{ "ToolTip", "Are we currently in the sprinting state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockFlyingAbilityComponent, nullptr, "IsSprinting", nullptr, nullptr, sizeof(MockFlyingAbilityComponent_eventIsSprinting_Parms), Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMockFlyingAbilityComponent_NoRegister()
	{
		return UMockFlyingAbilityComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMockFlyingAbilityComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSprintStateChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSprintStateChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnDashStateChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnDashStateChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnBlinkStateChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnBlinkStateChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnBlinkActivateEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnBlinkActivateEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnBlinkActivateEventRollback_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnBlinkActivateEventRollback;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPhysicsGunFirEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPhysicsGunFirEvent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFlyingMovementComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMockFlyingAbilityComponent_GetBlinkWarmupTimeSeconds, "GetBlinkWarmupTimeSeconds" }, // 1471823512
		{ &Z_Construct_UFunction_UMockFlyingAbilityComponent_GetMaxStamina, "GetMaxStamina" }, // 3898179320
		{ &Z_Construct_UFunction_UMockFlyingAbilityComponent_GetStamina, "GetStamina" }, // 3701831673
		{ &Z_Construct_UFunction_UMockFlyingAbilityComponent_IsBlinking, "IsBlinking" }, // 515119382
		{ &Z_Construct_UFunction_UMockFlyingAbilityComponent_IsDashing, "IsDashing" }, // 1880509584
		{ &Z_Construct_UFunction_UMockFlyingAbilityComponent_IsSprinting, "IsSprinting" }, // 3981295127
		{ &Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature, "MockAbilityBlinkCueEvent__DelegateSignature" }, // 3501164326
		{ &Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature, "MockAbilityBlinkCueRollback__DelegateSignature" }, // 3383319747
		{ &Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature, "MockAbilityNotifyStateChange__DelegateSignature" }, // 3797281332
		{ &Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature, "MockAbilityPhysicsGunFireEvent__DelegateSignature" }, // 1958832880
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "Comment", "// -------------------------------------------------------------------------------------------------------------------------------\n// ActorComponent for running Mock Ability Simulation \n// -------------------------------------------------------------------------------------------------------------------------------\n" },
		{ "IncludePath", "MockAbilitySimulation.h" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
		{ "ToolTip", "ActorComponent for running Mock Ability Simulation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnSprintStateChange_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Notifies when Sprint state changes\n" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
		{ "ToolTip", "Notifies when Sprint state changes" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnSprintStateChange = { "OnSprintStateChange", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockFlyingAbilityComponent, OnSprintStateChange), Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnSprintStateChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnSprintStateChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnDashStateChange_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Notifies when Dash state changes\n" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
		{ "ToolTip", "Notifies when Dash state changes" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnDashStateChange = { "OnDashStateChange", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockFlyingAbilityComponent, OnDashStateChange), Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnDashStateChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnDashStateChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkStateChange_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "Comment", "// Notifies when Blink Changes\n" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
		{ "ToolTip", "Notifies when Blink Changes" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkStateChange = { "OnBlinkStateChange", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockFlyingAbilityComponent, OnBlinkStateChange), Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkStateChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkStateChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkActivateEvent_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkActivateEvent = { "OnBlinkActivateEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockFlyingAbilityComponent, OnBlinkActivateEvent), Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkActivateEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkActivateEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkActivateEventRollback_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkActivateEventRollback = { "OnBlinkActivateEventRollback", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockFlyingAbilityComponent, OnBlinkActivateEventRollback), Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkActivateEventRollback_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkActivateEventRollback_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnPhysicsGunFirEvent_MetaData[] = {
		{ "Category", "Mock AbilitySystem" },
		{ "ModuleRelativePath", "Public/MockAbilitySimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnPhysicsGunFirEvent = { "OnPhysicsGunFirEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockFlyingAbilityComponent, OnPhysicsGunFirEvent), Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnPhysicsGunFirEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnPhysicsGunFirEvent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnSprintStateChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnDashStateChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkStateChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkActivateEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnBlinkActivateEventRollback,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::NewProp_OnPhysicsGunFirEvent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockFlyingAbilityComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::ClassParams = {
		&UMockFlyingAbilityComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockFlyingAbilityComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockFlyingAbilityComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockFlyingAbilityComponent, 2240656766);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UMockFlyingAbilityComponent>()
	{
		return UMockFlyingAbilityComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockFlyingAbilityComponent(Z_Construct_UClass_UMockFlyingAbilityComponent, &UMockFlyingAbilityComponent::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UMockFlyingAbilityComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockFlyingAbilityComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
