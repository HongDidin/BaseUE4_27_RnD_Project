// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPredictionExtras/Public/MockRootMotionComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMockRootMotionComponent() {}
// Cross Module References
	NETWORKPREDICTIONEXTRAS_API UScriptStruct* Z_Construct_UScriptStruct_FRootMotionSourceCache();
	UPackage* Z_Construct_UPackage__Script_NetworkPredictionExtras();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSource_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionComponent_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionComponent();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UBaseMovementComponent();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
class UScriptStruct* FRootMotionSourceCache::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NETWORKPREDICTIONEXTRAS_API uint32 Get_Z_Construct_UScriptStruct_FRootMotionSourceCache_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRootMotionSourceCache, Z_Construct_UPackage__Script_NetworkPredictionExtras(), TEXT("RootMotionSourceCache"), sizeof(FRootMotionSourceCache), Get_Z_Construct_UScriptStruct_FRootMotionSourceCache_Hash());
	}
	return Singleton;
}
template<> NETWORKPREDICTIONEXTRAS_API UScriptStruct* StaticStruct<FRootMotionSourceCache>()
{
	return FRootMotionSourceCache::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRootMotionSourceCache(FRootMotionSourceCache::StaticStruct, TEXT("/Script/NetworkPredictionExtras"), TEXT("RootMotionSourceCache"), false, nullptr, nullptr);
static struct FScriptStruct_NetworkPredictionExtras_StaticRegisterNativesFRootMotionSourceCache
{
	FScriptStruct_NetworkPredictionExtras_StaticRegisterNativesFRootMotionSourceCache()
	{
		UScriptStruct::DeferCppStructOps<FRootMotionSourceCache>(FName(TEXT("RootMotionSourceCache")));
	}
} ScriptStruct_NetworkPredictionExtras_StaticRegisterNativesFRootMotionSourceCache;
	struct Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Instance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Instance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MockRootMotionComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRootMotionSourceCache>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::NewProp_Instance_MetaData[] = {
		{ "ModuleRelativePath", "Public/MockRootMotionComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::NewProp_Instance = { "Instance", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRootMotionSourceCache, Instance), Z_Construct_UClass_UMockRootMotionSource_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::NewProp_Instance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::NewProp_Instance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::NewProp_Instance,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
		nullptr,
		&NewStructOps,
		"RootMotionSourceCache",
		sizeof(FRootMotionSourceCache),
		alignof(FRootMotionSourceCache),
		Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRootMotionSourceCache()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRootMotionSourceCache_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPredictionExtras();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RootMotionSourceCache"), sizeof(FRootMotionSourceCache), Get_Z_Construct_UScriptStruct_FRootMotionSourceCache_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRootMotionSourceCache_Hash() { return 3238100050U; }
	DEFINE_FUNCTION(UMockRootMotionComponent::execPlayRootMotionSourceByClass)
	{
		P_GET_OBJECT(UClass,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PlayRootMotionSourceByClass(Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockRootMotionComponent::execPlayRootMotionSource)
	{
		P_GET_OBJECT(UMockRootMotionSource,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PlayRootMotionSource(Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockRootMotionComponent::execInput_PlayRootMotionSourceByClass)
	{
		P_GET_OBJECT(UClass,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Input_PlayRootMotionSourceByClass(Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockRootMotionComponent::execInput_PlayRootMotionSource)
	{
		P_GET_OBJECT(UMockRootMotionSource,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Input_PlayRootMotionSource(Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMockRootMotionComponent::execCreateRootMotionSource)
	{
		P_GET_OBJECT(UClass,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMockRootMotionSource**)Z_Param__Result=P_THIS->CreateRootMotionSource(Z_Param_Source);
		P_NATIVE_END;
	}
	void UMockRootMotionComponent::StaticRegisterNativesUMockRootMotionComponent()
	{
		UClass* Class = UMockRootMotionComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateRootMotionSource", &UMockRootMotionComponent::execCreateRootMotionSource },
			{ "Input_PlayRootMotionSource", &UMockRootMotionComponent::execInput_PlayRootMotionSource },
			{ "Input_PlayRootMotionSourceByClass", &UMockRootMotionComponent::execInput_PlayRootMotionSourceByClass },
			{ "PlayRootMotionSource", &UMockRootMotionComponent::execPlayRootMotionSource },
			{ "PlayRootMotionSourceByClass", &UMockRootMotionComponent::execPlayRootMotionSourceByClass },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics
	{
		struct MockRootMotionComponent_eventCreateRootMotionSource_Parms
		{
			TSubclassOf<UMockRootMotionSource>  Source;
			UMockRootMotionSource* ReturnValue;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockRootMotionComponent_eventCreateRootMotionSource_Parms, Source), Z_Construct_UClass_UMockRootMotionSource_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockRootMotionComponent_eventCreateRootMotionSource_Parms, ReturnValue), Z_Construct_UClass_UMockRootMotionSource_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::NewProp_Source,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "Comment", "// ------------------------------------------------------------------------\n" },
		{ "ModuleRelativePath", "Public/MockRootMotionComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockRootMotionComponent, nullptr, "CreateRootMotionSource", nullptr, nullptr, sizeof(MockRootMotionComponent_eventCreateRootMotionSource_Parms), Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics
	{
		struct MockRootMotionComponent_eventInput_PlayRootMotionSource_Parms
		{
			UMockRootMotionSource* Source;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockRootMotionComponent_eventInput_PlayRootMotionSource_Parms, Source), Z_Construct_UClass_UMockRootMotionSource_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/MockRootMotionComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockRootMotionComponent, nullptr, "Input_PlayRootMotionSource", nullptr, nullptr, sizeof(MockRootMotionComponent_eventInput_PlayRootMotionSource_Parms), Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics
	{
		struct MockRootMotionComponent_eventInput_PlayRootMotionSourceByClass_Parms
		{
			TSubclassOf<UMockRootMotionSource>  Source;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockRootMotionComponent_eventInput_PlayRootMotionSourceByClass_Parms, Source), Z_Construct_UClass_UMockRootMotionSource_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/MockRootMotionComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockRootMotionComponent, nullptr, "Input_PlayRootMotionSourceByClass", nullptr, nullptr, sizeof(MockRootMotionComponent_eventInput_PlayRootMotionSourceByClass_Parms), Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics
	{
		struct MockRootMotionComponent_eventPlayRootMotionSource_Parms
		{
			UMockRootMotionSource* Source;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockRootMotionComponent_eventPlayRootMotionSource_Parms, Source), Z_Construct_UClass_UMockRootMotionSource_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics::Function_MetaDataParams[] = {
		{ "Category", "Animation" },
		{ "Comment", "// Callable by authority. Plays \"out of band\" animation: e.g, directly sets the RootMotionSourceID on the sync state, rather than the pending InputCmd.\n// This is analogous to outside code teleporting the actor (outside of the core simulation function)\n" },
		{ "ModuleRelativePath", "Public/MockRootMotionComponent.h" },
		{ "ToolTip", "Callable by authority. Plays \"out of band\" animation: e.g, directly sets the RootMotionSourceID on the sync state, rather than the pending InputCmd.\nThis is analogous to outside code teleporting the actor (outside of the core simulation function)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockRootMotionComponent, nullptr, "PlayRootMotionSource", nullptr, nullptr, sizeof(MockRootMotionComponent_eventPlayRootMotionSource_Parms), Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics
	{
		struct MockRootMotionComponent_eventPlayRootMotionSourceByClass_Parms
		{
			TSubclassOf<UMockRootMotionSource>  Source;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockRootMotionComponent_eventPlayRootMotionSourceByClass_Parms, Source), Z_Construct_UClass_UMockRootMotionSource_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "Animation" },
		{ "ModuleRelativePath", "Public/MockRootMotionComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockRootMotionComponent, nullptr, "PlayRootMotionSourceByClass", nullptr, nullptr, sizeof(MockRootMotionComponent_eventPlayRootMotionSourceByClass_Parms), Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMockRootMotionComponent_NoRegister()
	{
		return UMockRootMotionComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMockRootMotionComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootMotionSourceCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RootMotionSourceCache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockRootMotionComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseMovementComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMockRootMotionComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMockRootMotionComponent_CreateRootMotionSource, "CreateRootMotionSource" }, // 1401529136
		{ &Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSource, "Input_PlayRootMotionSource" }, // 3560427831
		{ &Z_Construct_UFunction_UMockRootMotionComponent_Input_PlayRootMotionSourceByClass, "Input_PlayRootMotionSourceByClass" }, // 2601473371
		{ &Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSource, "PlayRootMotionSource" }, // 1977926943
		{ &Z_Construct_UFunction_UMockRootMotionComponent_PlayRootMotionSourceByClass, "PlayRootMotionSourceByClass" }, // 2949155771
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "Comment", "// This component acts as the Driver for the FMockRootMotionSimulation\n// It is essentially a standin for the movement component, and would be replaced by \"new movement system\" component.\n// If we support \"root motion without movement component\" then this could either be that component, or possibly\n// built into or inherit from a USkeletalMeshComponent.\n//\n// The main thing this provides is:\n//\x09\x09-Interface for initiating root motions through the NP system (via client Input and via server \"OOB\" writes)\n//\x09\x09-FinalizeFrame: take the output of the NP simulation and push it to the movement/animation components\n//\x09\x09-Place holder implementation of IRootMotionSourceStore(the temp thing that maps our RootMotionSourceIDs -> actual sources)\n" },
		{ "IncludePath", "MockRootMotionComponent.h" },
		{ "ModuleRelativePath", "Public/MockRootMotionComponent.h" },
		{ "ToolTip", "This component acts as the Driver for the FMockRootMotionSimulation\nIt is essentially a standin for the movement component, and would be replaced by \"new movement system\" component.\nIf we support \"root motion without movement component\" then this could either be that component, or possibly\nbuilt into or inherit from a USkeletalMeshComponent.\n\nThe main thing this provides is:\n              -Interface for initiating root motions through the NP system (via client Input and via server \"OOB\" writes)\n              -FinalizeFrame: take the output of the NP simulation and push it to the movement/animation components\n              -Place holder implementation of IRootMotionSourceStore(the temp thing that maps our RootMotionSourceIDs -> actual sources)" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionComponent_Statics::NewProp_RootMotionSourceCache_MetaData[] = {
		{ "ModuleRelativePath", "Public/MockRootMotionComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMockRootMotionComponent_Statics::NewProp_RootMotionSourceCache = { "RootMotionSourceCache", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionComponent, RootMotionSourceCache), Z_Construct_UScriptStruct_FRootMotionSourceCache, METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionComponent_Statics::NewProp_RootMotionSourceCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionComponent_Statics::NewProp_RootMotionSourceCache_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMockRootMotionComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionComponent_Statics::NewProp_RootMotionSourceCache,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockRootMotionComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockRootMotionComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockRootMotionComponent_Statics::ClassParams = {
		&UMockRootMotionComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMockRootMotionComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockRootMotionComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockRootMotionComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockRootMotionComponent, 1514758247);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UMockRootMotionComponent>()
	{
		return UMockRootMotionComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockRootMotionComponent(Z_Construct_UClass_UMockRootMotionComponent, &UMockRootMotionComponent::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UMockRootMotionComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockRootMotionComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
