// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPredictionExtras/Public/MockPhysicsSimulation.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMockPhysicsSimulation() {}
// Cross Module References
	NETWORKPREDICTIONEXTRAS_API UScriptStruct* Z_Construct_UScriptStruct_FMockPhysicsInputCmd();
	UPackage* Z_Construct_UPackage__Script_NetworkPredictionExtras();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
class UScriptStruct* FMockPhysicsInputCmd::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NETWORKPREDICTIONEXTRAS_API uint32 Get_Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMockPhysicsInputCmd, Z_Construct_UPackage__Script_NetworkPredictionExtras(), TEXT("MockPhysicsInputCmd"), sizeof(FMockPhysicsInputCmd), Get_Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Hash());
	}
	return Singleton;
}
template<> NETWORKPREDICTIONEXTRAS_API UScriptStruct* StaticStruct<FMockPhysicsInputCmd>()
{
	return FMockPhysicsInputCmd::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMockPhysicsInputCmd(FMockPhysicsInputCmd::StaticStruct, TEXT("/Script/NetworkPredictionExtras"), TEXT("MockPhysicsInputCmd"), false, nullptr, nullptr);
static struct FScriptStruct_NetworkPredictionExtras_StaticRegisterNativesFMockPhysicsInputCmd
{
	FScriptStruct_NetworkPredictionExtras_StaticRegisterNativesFMockPhysicsInputCmd()
	{
		UScriptStruct::DeferCppStructOps<FMockPhysicsInputCmd>(FName(TEXT("MockPhysicsInputCmd")));
	}
} ScriptStruct_NetworkPredictionExtras_StaticRegisterNativesFMockPhysicsInputCmd;
	struct Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovementInput_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MovementInput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bJumpedPressed_MetaData[];
#endif
		static void NewProp_bJumpedPressed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bJumpedPressed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bChargePressed_MetaData[];
#endif
		static void NewProp_bChargePressed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bChargePressed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Making this a blueprint type to that it can be filled out by BPs.\n" },
		{ "ModuleRelativePath", "Public/MockPhysicsSimulation.h" },
		{ "ToolTip", "Making this a blueprint type to that it can be filled out by BPs." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMockPhysicsInputCmd>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_MovementInput_MetaData[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/MockPhysicsSimulation.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_MovementInput = { "MovementInput", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMockPhysicsInputCmd, MovementInput), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_MovementInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_MovementInput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bJumpedPressed_MetaData[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/MockPhysicsSimulation.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bJumpedPressed_SetBit(void* Obj)
	{
		((FMockPhysicsInputCmd*)Obj)->bJumpedPressed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bJumpedPressed = { "bJumpedPressed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMockPhysicsInputCmd), &Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bJumpedPressed_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bJumpedPressed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bJumpedPressed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bChargePressed_MetaData[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/MockPhysicsSimulation.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bChargePressed_SetBit(void* Obj)
	{
		((FMockPhysicsInputCmd*)Obj)->bChargePressed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bChargePressed = { "bChargePressed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMockPhysicsInputCmd), &Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bChargePressed_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bChargePressed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bChargePressed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_MovementInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bJumpedPressed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::NewProp_bChargePressed,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
		nullptr,
		&NewStructOps,
		"MockPhysicsInputCmd",
		sizeof(FMockPhysicsInputCmd),
		alignof(FMockPhysicsInputCmd),
		Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMockPhysicsInputCmd()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPredictionExtras();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MockPhysicsInputCmd"), sizeof(FMockPhysicsInputCmd), Get_Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMockPhysicsInputCmd_Hash() { return 2191107247U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
