// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETWORKPREDICTIONEXTRAS_FlyingMovementComponent_generated_h
#error "FlyingMovementComponent.generated.h already included, missing '#pragma once' in FlyingMovementComponent.h"
#endif
#define NETWORKPREDICTIONEXTRAS_FlyingMovementComponent_generated_h

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_RPC_WRAPPERS
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFlyingMovementComponent(); \
	friend struct Z_Construct_UClass_UFlyingMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UFlyingMovementComponent, UBaseMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UFlyingMovementComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUFlyingMovementComponent(); \
	friend struct Z_Construct_UClass_UFlyingMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UFlyingMovementComponent, UBaseMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UFlyingMovementComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFlyingMovementComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFlyingMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFlyingMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFlyingMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFlyingMovementComponent(UFlyingMovementComponent&&); \
	NO_API UFlyingMovementComponent(const UFlyingMovementComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFlyingMovementComponent(UFlyingMovementComponent&&); \
	NO_API UFlyingMovementComponent(const UFlyingMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFlyingMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFlyingMovementComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UFlyingMovementComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_19_PROLOG
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_INCLASS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<class UFlyingMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_FlyingMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
