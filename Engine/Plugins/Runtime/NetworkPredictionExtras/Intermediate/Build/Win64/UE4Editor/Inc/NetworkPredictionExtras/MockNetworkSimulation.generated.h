// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETWORKPREDICTIONEXTRAS_MockNetworkSimulation_generated_h
#error "MockNetworkSimulation.generated.h already included, missing '#pragma once' in MockNetworkSimulation.h"
#endif
#define NETWORKPREDICTIONEXTRAS_MockNetworkSimulation_generated_h

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_RPC_WRAPPERS
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMockNetworkSimulationComponent(); \
	friend struct Z_Construct_UClass_UMockNetworkSimulationComponent_Statics; \
public: \
	DECLARE_CLASS(UMockNetworkSimulationComponent, UNetworkPredictionComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockNetworkSimulationComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_INCLASS \
private: \
	static void StaticRegisterNativesUMockNetworkSimulationComponent(); \
	friend struct Z_Construct_UClass_UMockNetworkSimulationComponent_Statics; \
public: \
	DECLARE_CLASS(UMockNetworkSimulationComponent, UNetworkPredictionComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockNetworkSimulationComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMockNetworkSimulationComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMockNetworkSimulationComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockNetworkSimulationComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockNetworkSimulationComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockNetworkSimulationComponent(UMockNetworkSimulationComponent&&); \
	NO_API UMockNetworkSimulationComponent(const UMockNetworkSimulationComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockNetworkSimulationComponent(UMockNetworkSimulationComponent&&); \
	NO_API UMockNetworkSimulationComponent(const UMockNetworkSimulationComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockNetworkSimulationComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockNetworkSimulationComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMockNetworkSimulationComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_179_PROLOG
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_INCLASS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h_182_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<class UMockNetworkSimulationComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockNetworkSimulation_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
