// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef NETWORKPREDICTIONEXTRAS_MockCharacterAbilitySimulation_generated_h
#error "MockCharacterAbilitySimulation.generated.h already included, missing '#pragma once' in MockCharacterAbilitySimulation.h"
#endif
#define NETWORKPREDICTIONEXTRAS_MockCharacterAbilitySimulation_generated_h

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_226_DELEGATE \
static inline void FMockCharacterAbilityBlinkCueRollback_DelegateWrapper(const FMulticastScriptDelegate& MockCharacterAbilityBlinkCueRollback) \
{ \
	MockCharacterAbilityBlinkCueRollback.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_222_DELEGATE \
struct MockCharacterAbilityComponent_eventMockCharacterAbilityBlinkCueEvent_Parms \
{ \
	FVector DestinationLocation; \
	int32 RandomValue; \
	float ElapsedTimeSeconds; \
}; \
static inline void FMockCharacterAbilityBlinkCueEvent_DelegateWrapper(const FMulticastScriptDelegate& MockCharacterAbilityBlinkCueEvent, FVector DestinationLocation, int32 RandomValue, float ElapsedTimeSeconds) \
{ \
	MockCharacterAbilityComponent_eventMockCharacterAbilityBlinkCueEvent_Parms Parms; \
	Parms.DestinationLocation=DestinationLocation; \
	Parms.RandomValue=RandomValue; \
	Parms.ElapsedTimeSeconds=ElapsedTimeSeconds; \
	MockCharacterAbilityBlinkCueEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_187_DELEGATE \
struct MockCharacterAbilityComponent_eventMockCharacterAbilityNotifyStateChange_Parms \
{ \
	bool bNewStateValue; \
}; \
static inline void FMockCharacterAbilityNotifyStateChange_DelegateWrapper(const FMulticastScriptDelegate& MockCharacterAbilityNotifyStateChange, bool bNewStateValue) \
{ \
	MockCharacterAbilityComponent_eventMockCharacterAbilityNotifyStateChange_Parms Parms; \
	Parms.bNewStateValue=bNewStateValue ? true : false; \
	MockCharacterAbilityNotifyStateChange.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetMaxStamina); \
	DECLARE_FUNCTION(execGetStamina); \
	DECLARE_FUNCTION(execGetBlinkWarmupTimeSeconds); \
	DECLARE_FUNCTION(execIsJumping); \
	DECLARE_FUNCTION(execIsBlinking); \
	DECLARE_FUNCTION(execIsDashing); \
	DECLARE_FUNCTION(execIsSprinting);


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetMaxStamina); \
	DECLARE_FUNCTION(execGetStamina); \
	DECLARE_FUNCTION(execGetBlinkWarmupTimeSeconds); \
	DECLARE_FUNCTION(execIsJumping); \
	DECLARE_FUNCTION(execIsBlinking); \
	DECLARE_FUNCTION(execIsDashing); \
	DECLARE_FUNCTION(execIsSprinting);


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMockCharacterAbilityComponent(); \
	friend struct Z_Construct_UClass_UMockCharacterAbilityComponent_Statics; \
public: \
	DECLARE_CLASS(UMockCharacterAbilityComponent, UCharacterMotionComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockCharacterAbilityComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_INCLASS \
private: \
	static void StaticRegisterNativesUMockCharacterAbilityComponent(); \
	friend struct Z_Construct_UClass_UMockCharacterAbilityComponent_Statics; \
public: \
	DECLARE_CLASS(UMockCharacterAbilityComponent, UCharacterMotionComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockCharacterAbilityComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMockCharacterAbilityComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMockCharacterAbilityComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockCharacterAbilityComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockCharacterAbilityComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockCharacterAbilityComponent(UMockCharacterAbilityComponent&&); \
	NO_API UMockCharacterAbilityComponent(const UMockCharacterAbilityComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockCharacterAbilityComponent(UMockCharacterAbilityComponent&&); \
	NO_API UMockCharacterAbilityComponent(const UMockCharacterAbilityComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockCharacterAbilityComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockCharacterAbilityComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMockCharacterAbilityComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_154_PROLOG
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_INCLASS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h_157_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<class UMockCharacterAbilityComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockCharacterAbilitySimulation_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
