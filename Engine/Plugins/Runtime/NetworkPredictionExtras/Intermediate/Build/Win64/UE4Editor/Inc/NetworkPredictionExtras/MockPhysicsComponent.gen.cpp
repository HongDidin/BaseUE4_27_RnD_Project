// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPredictionExtras/Public/MockPhysicsComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMockPhysicsComponent() {}
// Cross Module References
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockPhysicsComponent();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockPhysicsComponent_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UBaseMovementComponent();
	UPackage* Z_Construct_UPackage__Script_NetworkPredictionExtras();
	NETWORKPREDICTIONEXTRAS_API UScriptStruct* Z_Construct_UScriptStruct_FMockPhysicsInputCmd();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics
	{
		struct MockPhysicsComponent_eventMockPhysicsNotifyStateChange_Parms
		{
			bool bNewStateValue;
		};
		static void NewProp_bNewStateValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNewStateValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue_SetBit(void* Obj)
	{
		((MockPhysicsComponent_eventMockPhysicsNotifyStateChange_Parms*)Obj)->bNewStateValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue = { "bNewStateValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MockPhysicsComponent_eventMockPhysicsNotifyStateChange_Parms), &Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::NewProp_bNewStateValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Charge (not a Cue event, just state)\n" },
		{ "ModuleRelativePath", "Public/MockPhysicsComponent.h" },
		{ "ToolTip", "Charge (not a Cue event, just state)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockPhysicsComponent, nullptr, "MockPhysicsNotifyStateChange__DelegateSignature", nullptr, nullptr, sizeof(MockPhysicsComponent_eventMockPhysicsNotifyStateChange_Parms), Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics
	{
		struct MockPhysicsComponent_eventPhysicsChargeCueEvent_Parms
		{
			FVector Location;
			float ElapsedTimeSeconds;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElapsedTimeSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockPhysicsComponent_eventPhysicsChargeCueEvent_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::NewProp_ElapsedTimeSeconds = { "ElapsedTimeSeconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockPhysicsComponent_eventPhysicsChargeCueEvent_Parms, ElapsedTimeSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::NewProp_ElapsedTimeSeconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MockPhysicsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockPhysicsComponent, nullptr, "PhysicsChargeCueEvent__DelegateSignature", nullptr, nullptr, sizeof(MockPhysicsComponent_eventPhysicsChargeCueEvent_Parms), Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00930000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics
	{
		struct MockPhysicsComponent_eventPhysicsJumpCueEvent_Parms
		{
			FVector Location;
			float ElapsedTimeSeconds;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElapsedTimeSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockPhysicsComponent_eventPhysicsJumpCueEvent_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::NewProp_ElapsedTimeSeconds = { "ElapsedTimeSeconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockPhysicsComponent_eventPhysicsJumpCueEvent_Parms, ElapsedTimeSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::NewProp_ElapsedTimeSeconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Assignable delegates chosen so that owning actor can implement in BPs. May not be the best choice for all cases.\n" },
		{ "ModuleRelativePath", "Public/MockPhysicsComponent.h" },
		{ "ToolTip", "Assignable delegates chosen so that owning actor can implement in BPs. May not be the best choice for all cases." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockPhysicsComponent, nullptr, "PhysicsJumpCueEvent__DelegateSignature", nullptr, nullptr, sizeof(MockPhysicsComponent_eventPhysicsJumpCueEvent_Parms), Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00930000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UMockPhysicsComponent::StaticRegisterNativesUMockPhysicsComponent()
	{
	}
	UClass* Z_Construct_UClass_UMockPhysicsComponent_NoRegister()
	{
		return UMockPhysicsComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMockPhysicsComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_EDITOR
		static const FClassFunctionLinkInfo FuncInfo[];
#endif //WITH_EDITOR
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendingInputCmd_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PendingInputCmd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnJumpActivatedEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnJumpActivatedEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnChargeActivatedEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnChargeActivatedEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnChargeStateChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnChargeStateChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsCharging_MetaData[];
#endif
		static void NewProp_bIsCharging_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsCharging;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockPhysicsComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseMovementComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
#if WITH_EDITOR
	const FClassFunctionLinkInfo Z_Construct_UClass_UMockPhysicsComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature, "MockPhysicsNotifyStateChange__DelegateSignature" }, // 2701254647
		{ &Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature, "PhysicsChargeCueEvent__DelegateSignature" }, // 1101821697
		{ &Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature, "PhysicsJumpCueEvent__DelegateSignature" }, // 3394432237
	};
#endif //WITH_EDITOR
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "Comment", "// -------------------------------------------------------------------------------------------------------------------------------\n// ActorComponent for running MockPhysicsSimulation\n// -------------------------------------------------------------------------------------------------------------------------------\n" },
		{ "IncludePath", "MockPhysicsComponent.h" },
		{ "ModuleRelativePath", "Public/MockPhysicsComponent.h" },
		{ "ToolTip", "ActorComponent for running MockPhysicsSimulation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_PendingInputCmd_MetaData[] = {
		{ "Category", "INPUT" },
		{ "Comment", "// Next local InputCmd that will be submitted. This is just one way to do it.\n" },
		{ "ModuleRelativePath", "Public/MockPhysicsComponent.h" },
		{ "ToolTip", "Next local InputCmd that will be submitted. This is just one way to do it." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_PendingInputCmd = { "PendingInputCmd", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockPhysicsComponent, PendingInputCmd), Z_Construct_UScriptStruct_FMockPhysicsInputCmd, METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_PendingInputCmd_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_PendingInputCmd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnJumpActivatedEvent_MetaData[] = {
		{ "Category", "Mock Physics Cues" },
		{ "ModuleRelativePath", "Public/MockPhysicsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnJumpActivatedEvent = { "OnJumpActivatedEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockPhysicsComponent, OnJumpActivatedEvent), Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnJumpActivatedEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnJumpActivatedEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnChargeActivatedEvent_MetaData[] = {
		{ "Category", "Mock Physics Cues" },
		{ "ModuleRelativePath", "Public/MockPhysicsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnChargeActivatedEvent = { "OnChargeActivatedEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockPhysicsComponent, OnChargeActivatedEvent), Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnChargeActivatedEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnChargeActivatedEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnChargeStateChange_MetaData[] = {
		{ "Category", "Mock Physics Cues" },
		{ "ModuleRelativePath", "Public/MockPhysicsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnChargeStateChange = { "OnChargeStateChange", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockPhysicsComponent, OnChargeStateChange), Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnChargeStateChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnChargeStateChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_bIsCharging_MetaData[] = {
		{ "Category", "Mock Physics" },
		{ "Comment", "// Currently charging up charge attack\n" },
		{ "ModuleRelativePath", "Public/MockPhysicsComponent.h" },
		{ "ToolTip", "Currently charging up charge attack" },
	};
#endif
	void Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_bIsCharging_SetBit(void* Obj)
	{
		((UMockPhysicsComponent*)Obj)->bIsCharging = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_bIsCharging = { "bIsCharging", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMockPhysicsComponent), &Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_bIsCharging_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_bIsCharging_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_bIsCharging_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMockPhysicsComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_PendingInputCmd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnJumpActivatedEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnChargeActivatedEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_OnChargeStateChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockPhysicsComponent_Statics::NewProp_bIsCharging,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockPhysicsComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockPhysicsComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockPhysicsComponent_Statics::ClassParams = {
		&UMockPhysicsComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		IF_WITH_EDITOR(FuncInfo, nullptr),
		Z_Construct_UClass_UMockPhysicsComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		IF_WITH_EDITOR(UE_ARRAY_COUNT(FuncInfo), 0),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockPhysicsComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockPhysicsComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockPhysicsComponent, 720143298);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UMockPhysicsComponent>()
	{
		return UMockPhysicsComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockPhysicsComponent(Z_Construct_UClass_UMockPhysicsComponent, &UMockPhysicsComponent::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UMockPhysicsComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockPhysicsComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
