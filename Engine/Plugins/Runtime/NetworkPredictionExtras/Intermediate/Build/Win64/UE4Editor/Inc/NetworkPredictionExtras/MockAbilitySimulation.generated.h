// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
struct FVector_NetQuantize100;
#ifdef NETWORKPREDICTIONEXTRAS_MockAbilitySimulation_generated_h
#error "MockAbilitySimulation.generated.h already included, missing '#pragma once' in MockAbilitySimulation.h"
#endif
#define NETWORKPREDICTIONEXTRAS_MockAbilitySimulation_generated_h

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_419_DELEGATE \
struct MockFlyingAbilityComponent_eventMockAbilityPhysicsGunFireEvent_Parms \
{ \
	FVector Start; \
	FVector End; \
	bool bHasCooldown; \
	TArray<FVector_NetQuantize100> HitLocations; \
	float ElapsedTimeSeconds; \
}; \
static inline void FMockAbilityPhysicsGunFireEvent_DelegateWrapper(const FMulticastScriptDelegate& MockAbilityPhysicsGunFireEvent, FVector Start, FVector End, bool bHasCooldown, TArray<FVector_NetQuantize100> const& HitLocations, float ElapsedTimeSeconds) \
{ \
	MockFlyingAbilityComponent_eventMockAbilityPhysicsGunFireEvent_Parms Parms; \
	Parms.Start=Start; \
	Parms.End=End; \
	Parms.bHasCooldown=bHasCooldown ? true : false; \
	Parms.HitLocations=HitLocations; \
	Parms.ElapsedTimeSeconds=ElapsedTimeSeconds; \
	MockAbilityPhysicsGunFireEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_415_DELEGATE \
static inline void FMockAbilityBlinkCueRollback_DelegateWrapper(const FMulticastScriptDelegate& MockAbilityBlinkCueRollback) \
{ \
	MockAbilityBlinkCueRollback.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_411_DELEGATE \
struct MockFlyingAbilityComponent_eventMockAbilityBlinkCueEvent_Parms \
{ \
	FVector DestinationLocation; \
	int32 RandomValue; \
	float ElapsedTimeSeconds; \
}; \
static inline void FMockAbilityBlinkCueEvent_DelegateWrapper(const FMulticastScriptDelegate& MockAbilityBlinkCueEvent, FVector DestinationLocation, int32 RandomValue, float ElapsedTimeSeconds) \
{ \
	MockFlyingAbilityComponent_eventMockAbilityBlinkCueEvent_Parms Parms; \
	Parms.DestinationLocation=DestinationLocation; \
	Parms.RandomValue=RandomValue; \
	Parms.ElapsedTimeSeconds=ElapsedTimeSeconds; \
	MockAbilityBlinkCueEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_384_DELEGATE \
struct MockFlyingAbilityComponent_eventMockAbilityNotifyStateChange_Parms \
{ \
	bool bNewStateValue; \
}; \
static inline void FMockAbilityNotifyStateChange_DelegateWrapper(const FMulticastScriptDelegate& MockAbilityNotifyStateChange, bool bNewStateValue) \
{ \
	MockFlyingAbilityComponent_eventMockAbilityNotifyStateChange_Parms Parms; \
	Parms.bNewStateValue=bNewStateValue ? true : false; \
	MockAbilityNotifyStateChange.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetMaxStamina); \
	DECLARE_FUNCTION(execGetStamina); \
	DECLARE_FUNCTION(execGetBlinkWarmupTimeSeconds); \
	DECLARE_FUNCTION(execIsBlinking); \
	DECLARE_FUNCTION(execIsDashing); \
	DECLARE_FUNCTION(execIsSprinting);


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetMaxStamina); \
	DECLARE_FUNCTION(execGetStamina); \
	DECLARE_FUNCTION(execGetBlinkWarmupTimeSeconds); \
	DECLARE_FUNCTION(execIsBlinking); \
	DECLARE_FUNCTION(execIsDashing); \
	DECLARE_FUNCTION(execIsSprinting);


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMockFlyingAbilityComponent(); \
	friend struct Z_Construct_UClass_UMockFlyingAbilityComponent_Statics; \
public: \
	DECLARE_CLASS(UMockFlyingAbilityComponent, UFlyingMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockFlyingAbilityComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_INCLASS \
private: \
	static void StaticRegisterNativesUMockFlyingAbilityComponent(); \
	friend struct Z_Construct_UClass_UMockFlyingAbilityComponent_Statics; \
public: \
	DECLARE_CLASS(UMockFlyingAbilityComponent, UFlyingMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockFlyingAbilityComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMockFlyingAbilityComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMockFlyingAbilityComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockFlyingAbilityComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockFlyingAbilityComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockFlyingAbilityComponent(UMockFlyingAbilityComponent&&); \
	NO_API UMockFlyingAbilityComponent(const UMockFlyingAbilityComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockFlyingAbilityComponent(UMockFlyingAbilityComponent&&); \
	NO_API UMockFlyingAbilityComponent(const UMockFlyingAbilityComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockFlyingAbilityComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockFlyingAbilityComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMockFlyingAbilityComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_353_PROLOG
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_INCLASS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h_356_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<class UMockFlyingAbilityComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockAbilitySimulation_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
