// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMockRootMotionSource;
#ifdef NETWORKPREDICTIONEXTRAS_MockRootMotionComponent_generated_h
#error "MockRootMotionComponent.generated.h already included, missing '#pragma once' in MockRootMotionComponent.h"
#endif
#define NETWORKPREDICTIONEXTRAS_MockRootMotionComponent_generated_h

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRootMotionSourceCache_Statics; \
	NETWORKPREDICTIONEXTRAS_API static class UScriptStruct* StaticStruct();


template<> NETWORKPREDICTIONEXTRAS_API UScriptStruct* StaticStruct<struct FRootMotionSourceCache>();

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPlayRootMotionSourceByClass); \
	DECLARE_FUNCTION(execPlayRootMotionSource); \
	DECLARE_FUNCTION(execInput_PlayRootMotionSourceByClass); \
	DECLARE_FUNCTION(execInput_PlayRootMotionSource); \
	DECLARE_FUNCTION(execCreateRootMotionSource);


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPlayRootMotionSourceByClass); \
	DECLARE_FUNCTION(execPlayRootMotionSource); \
	DECLARE_FUNCTION(execInput_PlayRootMotionSourceByClass); \
	DECLARE_FUNCTION(execInput_PlayRootMotionSource); \
	DECLARE_FUNCTION(execCreateRootMotionSource);


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMockRootMotionComponent(); \
	friend struct Z_Construct_UClass_UMockRootMotionComponent_Statics; \
public: \
	DECLARE_CLASS(UMockRootMotionComponent, UBaseMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockRootMotionComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_INCLASS \
private: \
	static void StaticRegisterNativesUMockRootMotionComponent(); \
	friend struct Z_Construct_UClass_UMockRootMotionComponent_Statics; \
public: \
	DECLARE_CLASS(UMockRootMotionComponent, UBaseMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockRootMotionComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMockRootMotionComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMockRootMotionComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockRootMotionComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockRootMotionComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockRootMotionComponent(UMockRootMotionComponent&&); \
	NO_API UMockRootMotionComponent(const UMockRootMotionComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMockRootMotionComponent() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockRootMotionComponent(UMockRootMotionComponent&&); \
	NO_API UMockRootMotionComponent(const UMockRootMotionComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockRootMotionComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockRootMotionComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMockRootMotionComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RootMotionSourceCache() { return STRUCT_OFFSET(UMockRootMotionComponent, RootMotionSourceCache); }


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_36_PROLOG
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_INCLASS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h_41_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<class UMockRootMotionComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockRootMotionComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
