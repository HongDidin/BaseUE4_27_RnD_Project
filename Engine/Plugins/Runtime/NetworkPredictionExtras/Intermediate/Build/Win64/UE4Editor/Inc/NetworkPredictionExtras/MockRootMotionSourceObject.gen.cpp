// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPredictionExtras/Public/MockRootMotionSourceObject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMockRootMotionSourceObject() {}
// Cross Module References
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSource_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSource();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NetworkPredictionExtras();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSourceClassMap_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSourceClassMap();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSource_Montage_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSource_Montage();
	ENGINE_API UClass* Z_Construct_UClass_UAnimMontage_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSource_Curve_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSource_Curve();
	ENGINE_API UClass* Z_Construct_UClass_UCurveVector_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockRootMotionSource_MoveToLocation();
// End Cross Module References
	void UMockRootMotionSource::StaticRegisterNativesUMockRootMotionSource()
	{
	}
	UClass* Z_Construct_UClass_UMockRootMotionSource_NoRegister()
	{
		return UMockRootMotionSource::StaticClass();
	}
	struct Z_Construct_UClass_UMockRootMotionSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockRootMotionSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MockRootMotionSourceObject.h" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockRootMotionSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockRootMotionSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockRootMotionSource_Statics::ClassParams = {
		&UMockRootMotionSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockRootMotionSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockRootMotionSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockRootMotionSource, 3006668548);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UMockRootMotionSource>()
	{
		return UMockRootMotionSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockRootMotionSource(Z_Construct_UClass_UMockRootMotionSource, &UMockRootMotionSource::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UMockRootMotionSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockRootMotionSource);
	void UMockRootMotionSourceClassMap::StaticRegisterNativesUMockRootMotionSourceClassMap()
	{
	}
	UClass* Z_Construct_UClass_UMockRootMotionSourceClassMap_NoRegister()
	{
		return UMockRootMotionSourceClassMap::StaticClass();
	}
	struct Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FSoftClassPropertyParams NewProp_SourceList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SourceList;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MockRootMotionSourceObject.h" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftClassPropertyParams Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::NewProp_SourceList_Inner = { "SourceList", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftClass, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMockRootMotionSource_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::NewProp_SourceList_MetaData[] = {
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::NewProp_SourceList = { "SourceList", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionSourceClassMap, SourceList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::NewProp_SourceList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::NewProp_SourceList_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::NewProp_SourceList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::NewProp_SourceList,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockRootMotionSourceClassMap>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::ClassParams = {
		&UMockRootMotionSourceClassMap::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockRootMotionSourceClassMap()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockRootMotionSourceClassMap_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockRootMotionSourceClassMap, 4084415931);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UMockRootMotionSourceClassMap>()
	{
		return UMockRootMotionSourceClassMap::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockRootMotionSourceClassMap(Z_Construct_UClass_UMockRootMotionSourceClassMap, &UMockRootMotionSourceClassMap::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UMockRootMotionSourceClassMap"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockRootMotionSourceClassMap);
	void UMockRootMotionSource_Montage::StaticRegisterNativesUMockRootMotionSource_Montage()
	{
	}
	UClass* Z_Construct_UClass_UMockRootMotionSource_Montage_NoRegister()
	{
		return UMockRootMotionSource_Montage::StaticClass();
	}
	struct Z_Construct_UClass_UMockRootMotionSource_Montage_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Montage_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Montage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlayRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TranslationScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TranslationScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMockRootMotionSource,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// ---------------------------------------------------------------\n//\n// ---------------------------------------------------------------\n" },
		{ "IncludePath", "MockRootMotionSourceObject.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_Montage_MetaData[] = {
		{ "Category", "RootMotion" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_Montage = { "Montage", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionSource_Montage, Montage), Z_Construct_UClass_UAnimMontage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_Montage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_Montage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_PlayRate_MetaData[] = {
		{ "Category", "RootMotion" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_PlayRate = { "PlayRate", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionSource_Montage, PlayRate), METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_PlayRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_PlayRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_TranslationScale_MetaData[] = {
		{ "Category", "RootMotion" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_TranslationScale = { "TranslationScale", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionSource_Montage, TranslationScale), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_TranslationScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_TranslationScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_Montage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_PlayRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::NewProp_TranslationScale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockRootMotionSource_Montage>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::ClassParams = {
		&UMockRootMotionSource_Montage::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockRootMotionSource_Montage()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockRootMotionSource_Montage_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockRootMotionSource_Montage, 3143269344);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UMockRootMotionSource_Montage>()
	{
		return UMockRootMotionSource_Montage::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockRootMotionSource_Montage(Z_Construct_UClass_UMockRootMotionSource_Montage, &UMockRootMotionSource_Montage::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UMockRootMotionSource_Montage"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockRootMotionSource_Montage);
	void UMockRootMotionSource_Curve::StaticRegisterNativesUMockRootMotionSource_Curve()
	{
	}
	UClass* Z_Construct_UClass_UMockRootMotionSource_Curve_NoRegister()
	{
		return UMockRootMotionSource_Curve::StaticClass();
	}
	struct Z_Construct_UClass_UMockRootMotionSource_Curve_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curve_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Curve;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlayRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TranslationScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TranslationScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMockRootMotionSource,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MockRootMotionSourceObject.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_Curve_MetaData[] = {
		{ "Category", "RootMotion" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_Curve = { "Curve", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionSource_Curve, Curve), Z_Construct_UClass_UCurveVector_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_Curve_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_Curve_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_PlayRate_MetaData[] = {
		{ "Category", "RootMotion" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_PlayRate = { "PlayRate", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionSource_Curve, PlayRate), METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_PlayRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_PlayRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_TranslationScale_MetaData[] = {
		{ "Category", "RootMotion" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_TranslationScale = { "TranslationScale", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionSource_Curve, TranslationScale), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_TranslationScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_TranslationScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_Curve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_PlayRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::NewProp_TranslationScale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockRootMotionSource_Curve>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::ClassParams = {
		&UMockRootMotionSource_Curve::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockRootMotionSource_Curve()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockRootMotionSource_Curve_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockRootMotionSource_Curve, 2739221753);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UMockRootMotionSource_Curve>()
	{
		return UMockRootMotionSource_Curve::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockRootMotionSource_Curve(Z_Construct_UClass_UMockRootMotionSource_Curve, &UMockRootMotionSource_Curve::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UMockRootMotionSource_Curve"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockRootMotionSource_Curve);
	DEFINE_FUNCTION(UMockRootMotionSource_MoveToLocation::execSetDestination)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_InDestination);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDestination(Z_Param_Out_InDestination);
		P_NATIVE_END;
	}
	void UMockRootMotionSource_MoveToLocation::StaticRegisterNativesUMockRootMotionSource_MoveToLocation()
	{
		UClass* Class = UMockRootMotionSource_MoveToLocation::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetDestination", &UMockRootMotionSource_MoveToLocation::execSetDestination },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics
	{
		struct MockRootMotionSource_MoveToLocation_eventSetDestination_Parms
		{
			FVector InDestination;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InDestination_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InDestination;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::NewProp_InDestination_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::NewProp_InDestination = { "InDestination", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MockRootMotionSource_MoveToLocation_eventSetDestination_Parms, InDestination), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::NewProp_InDestination_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::NewProp_InDestination_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::NewProp_InDestination,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::Function_MetaDataParams[] = {
		{ "Category", "Root Motion" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockRootMotionSource_MoveToLocation, nullptr, "SetDestination", nullptr, nullptr, sizeof(MockRootMotionSource_MoveToLocation_eventSetDestination_Parms), Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C40401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_NoRegister()
	{
		return UMockRootMotionSource_MoveToLocation::StaticClass();
	}
	struct Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Destination_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Destination;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Velocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Velocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapToTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SnapToTolerance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMockRootMotionSource,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMockRootMotionSource_MoveToLocation_SetDestination, "SetDestination" }, // 785463247
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MockRootMotionSourceObject.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_Destination_MetaData[] = {
		{ "Category", "RootMotion" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_Destination = { "Destination", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionSource_MoveToLocation, Destination), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_Destination_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_Destination_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_Velocity_MetaData[] = {
		{ "Category", "RootMotion" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_Velocity = { "Velocity", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionSource_MoveToLocation, Velocity), METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_Velocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_Velocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_SnapToTolerance_MetaData[] = {
		{ "Category", "RootMotion" },
		{ "ModuleRelativePath", "Public/MockRootMotionSourceObject.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_SnapToTolerance = { "SnapToTolerance", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockRootMotionSource_MoveToLocation, SnapToTolerance), METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_SnapToTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_SnapToTolerance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_Destination,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_Velocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::NewProp_SnapToTolerance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockRootMotionSource_MoveToLocation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::ClassParams = {
		&UMockRootMotionSource_MoveToLocation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockRootMotionSource_MoveToLocation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockRootMotionSource_MoveToLocation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockRootMotionSource_MoveToLocation, 3467809971);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UMockRootMotionSource_MoveToLocation>()
	{
		return UMockRootMotionSource_MoveToLocation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockRootMotionSource_MoveToLocation(Z_Construct_UClass_UMockRootMotionSource_MoveToLocation, &UMockRootMotionSource_MoveToLocation::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UMockRootMotionSource_MoveToLocation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockRootMotionSource_MoveToLocation);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
