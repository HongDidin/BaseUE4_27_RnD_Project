// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPredictionExtras/Public/NetworkPredictionExtrasFlyingPawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetworkPredictionExtrasFlyingPawn() {}
// Cross Module References
	NETWORKPREDICTIONEXTRAS_API UEnum* Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasMockAbilityInputPreset();
	UPackage* Z_Construct_UPackage__Script_NetworkPredictionExtras();
	NETWORKPREDICTIONEXTRAS_API UEnum* Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasFlyingInputPreset();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UFlyingMovementComponent_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockFlyingAbilityComponent_NoRegister();
// End Cross Module References
	static UEnum* ENetworkPredictionExtrasMockAbilityInputPreset_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasMockAbilityInputPreset, Z_Construct_UPackage__Script_NetworkPredictionExtras(), TEXT("ENetworkPredictionExtrasMockAbilityInputPreset"));
		}
		return Singleton;
	}
	template<> NETWORKPREDICTIONEXTRAS_API UEnum* StaticEnum<ENetworkPredictionExtrasMockAbilityInputPreset>()
	{
		return ENetworkPredictionExtrasMockAbilityInputPreset_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENetworkPredictionExtrasMockAbilityInputPreset(ENetworkPredictionExtrasMockAbilityInputPreset_StaticEnum, TEXT("/Script/NetworkPredictionExtras"), TEXT("ENetworkPredictionExtrasMockAbilityInputPreset"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasMockAbilityInputPreset_Hash() { return 2338464355U; }
	UEnum* Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasMockAbilityInputPreset()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPredictionExtras();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENetworkPredictionExtrasMockAbilityInputPreset"), 0, Get_Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasMockAbilityInputPreset_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENetworkPredictionExtrasMockAbilityInputPreset::None", (int64)ENetworkPredictionExtrasMockAbilityInputPreset::None },
				{ "ENetworkPredictionExtrasMockAbilityInputPreset::Sprint", (int64)ENetworkPredictionExtrasMockAbilityInputPreset::Sprint },
				{ "ENetworkPredictionExtrasMockAbilityInputPreset::Dash", (int64)ENetworkPredictionExtrasMockAbilityInputPreset::Dash },
				{ "ENetworkPredictionExtrasMockAbilityInputPreset::Blink", (int64)ENetworkPredictionExtrasMockAbilityInputPreset::Blink },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Blink.Name", "ENetworkPredictionExtrasMockAbilityInputPreset::Blink" },
				{ "Dash.Name", "ENetworkPredictionExtrasMockAbilityInputPreset::Dash" },
				{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
				{ "None.Comment", "/** No input */" },
				{ "None.Name", "ENetworkPredictionExtrasMockAbilityInputPreset::None" },
				{ "None.ToolTip", "No input" },
				{ "Sprint.Name", "ENetworkPredictionExtrasMockAbilityInputPreset::Sprint" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
				nullptr,
				"ENetworkPredictionExtrasMockAbilityInputPreset",
				"ENetworkPredictionExtrasMockAbilityInputPreset",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENetworkPredictionExtrasFlyingInputPreset_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasFlyingInputPreset, Z_Construct_UPackage__Script_NetworkPredictionExtras(), TEXT("ENetworkPredictionExtrasFlyingInputPreset"));
		}
		return Singleton;
	}
	template<> NETWORKPREDICTIONEXTRAS_API UEnum* StaticEnum<ENetworkPredictionExtrasFlyingInputPreset>()
	{
		return ENetworkPredictionExtrasFlyingInputPreset_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENetworkPredictionExtrasFlyingInputPreset(ENetworkPredictionExtrasFlyingInputPreset_StaticEnum, TEXT("/Script/NetworkPredictionExtras"), TEXT("ENetworkPredictionExtrasFlyingInputPreset"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasFlyingInputPreset_Hash() { return 227114179U; }
	UEnum* Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasFlyingInputPreset()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPredictionExtras();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENetworkPredictionExtrasFlyingInputPreset"), 0, Get_Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasFlyingInputPreset_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENetworkPredictionExtrasFlyingInputPreset::None", (int64)ENetworkPredictionExtrasFlyingInputPreset::None },
				{ "ENetworkPredictionExtrasFlyingInputPreset::Forward", (int64)ENetworkPredictionExtrasFlyingInputPreset::Forward },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "// -------------------------------------------------------------------------------------------------------------------------------\n//\x09""ANetworkPredictionExtrasFlyingPawn\n//\n//\x09This provides a minimal pawn class that uses UFlyingMovementCompnent. This isn't really intended to be used in shipping games,\n//\x09rather just to serve as standalone example of using the system, contained completely in the NetworkPredictionExtras plugin.\n//\x09""Apart from the most basic glue/setup, this class provides an example of turning UE4 input event callbacks into the input commands\n//\x09that are used by the flying movement simulation. This includes some basic camera/aiming code.\n//\n//\x09Highlights:\n//\x09\x09""FlyingMovement::FMovementSystem::SimulationTick\x09\x09\x09\x09The \"core update\" function of the flying movement simulation.\n//\x09\x09""ANetworkPredictionExtrasFlyingPawn::GenerateLocalInput\x09\x09""Function that generates local input commands that are fed into the movement system.\n//\n//\x09Usage:\n//\x09\x09You should be able to just use this pawn like you would any other pawn. You can specify it as your pawn class in your game mode, or manually override in world settings, etc.\n//\x09\x09""Alternatively, you can just load the NetworkPredictionExtras/Content/TestMap.umap which will have everything setup.\n//\n//\x09Once spawned, there are some useful console commands:\n//\x09\x09NetworkPredictionExtras.FlyingPawn.CameraSyle [0-3]\x09\x09\x09""Changes camera mode style.\n//\x09\x09nms.Debug.LocallyControlledPawn 1\x09\x09\x09\x09\x09\x09\x09""Enables debug hud. binds to '9' by default, see ANetworkPredictionExtrasFlyingPawn()\n//\x09\x09nms.Debug.ToggleContinous 1\x09\x09\x09\x09\x09\x09\x09\x09\x09Toggles continuous updates of the debug hud. binds to '0' by default, see ANetworkPredictionExtrasFlyingPawn()\n//\n// -------------------------------------------------------------------------------------------------------------------------------\n" },
				{ "Forward.Comment", "/** Just moves forward */" },
				{ "Forward.Name", "ENetworkPredictionExtrasFlyingInputPreset::Forward" },
				{ "Forward.ToolTip", "Just moves forward" },
				{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
				{ "None.Comment", "/** No input */" },
				{ "None.Name", "ENetworkPredictionExtrasFlyingInputPreset::None" },
				{ "None.ToolTip", "No input" },
				{ "ToolTip", "ANetworkPredictionExtrasFlyingPawn\n\nThis provides a minimal pawn class that uses UFlyingMovementCompnent. This isn't really intended to be used in shipping games,\nrather just to serve as standalone example of using the system, contained completely in the NetworkPredictionExtras plugin.\nApart from the most basic glue/setup, this class provides an example of turning UE4 input event callbacks into the input commands\nthat are used by the flying movement simulation. This includes some basic camera/aiming code.\n\nHighlights:\n        FlyingMovement::FMovementSystem::SimulationTick                         The \"core update\" function of the flying movement simulation.\n        ANetworkPredictionExtrasFlyingPawn::GenerateLocalInput          Function that generates local input commands that are fed into the movement system.\n\nUsage:\n        You should be able to just use this pawn like you would any other pawn. You can specify it as your pawn class in your game mode, or manually override in world settings, etc.\n        Alternatively, you can just load the NetworkPredictionExtras/Content/TestMap.umap which will have everything setup.\n\nOnce spawned, there are some useful console commands:\n        NetworkPredictionExtras.FlyingPawn.CameraSyle [0-3]                     Changes camera mode style.\n        nms.Debug.LocallyControlledPawn 1                                                       Enables debug hud. binds to '9' by default, see ANetworkPredictionExtrasFlyingPawn()\n        nms.Debug.ToggleContinous 1                                                                     Toggles continuous updates of the debug hud. binds to '0' by default, see ANetworkPredictionExtrasFlyingPawn()" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
				nullptr,
				"ENetworkPredictionExtrasFlyingInputPreset",
				"ENetworkPredictionExtrasFlyingInputPreset",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(ANetworkPredictionExtrasFlyingPawn::execAddMaxMoveSpeed)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_AdditiveMaxMoveSpeed);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddMaxMoveSpeed(Z_Param_AdditiveMaxMoveSpeed);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANetworkPredictionExtrasFlyingPawn::execSetMaxMoveSpeed)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewMaxMoveSpeed);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetMaxMoveSpeed(Z_Param_NewMaxMoveSpeed);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANetworkPredictionExtrasFlyingPawn::execGetMaxMoveSpeed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetMaxMoveSpeed();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANetworkPredictionExtrasFlyingPawn::execPrintDebug)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PrintDebug();
		P_NATIVE_END;
	}
	void ANetworkPredictionExtrasFlyingPawn::StaticRegisterNativesANetworkPredictionExtrasFlyingPawn()
	{
		UClass* Class = ANetworkPredictionExtrasFlyingPawn::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddMaxMoveSpeed", &ANetworkPredictionExtrasFlyingPawn::execAddMaxMoveSpeed },
			{ "GetMaxMoveSpeed", &ANetworkPredictionExtrasFlyingPawn::execGetMaxMoveSpeed },
			{ "PrintDebug", &ANetworkPredictionExtrasFlyingPawn::execPrintDebug },
			{ "SetMaxMoveSpeed", &ANetworkPredictionExtrasFlyingPawn::execSetMaxMoveSpeed },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics
	{
		struct NetworkPredictionExtrasFlyingPawn_eventAddMaxMoveSpeed_Parms
		{
			float AdditiveMaxMoveSpeed;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AdditiveMaxMoveSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics::NewProp_AdditiveMaxMoveSpeed = { "AdditiveMaxMoveSpeed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NetworkPredictionExtrasFlyingPawn_eventAddMaxMoveSpeed_Parms, AdditiveMaxMoveSpeed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics::NewProp_AdditiveMaxMoveSpeed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Gameplay" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn, nullptr, "AddMaxMoveSpeed", nullptr, nullptr, sizeof(NetworkPredictionExtrasFlyingPawn_eventAddMaxMoveSpeed_Parms), Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics
	{
		struct NetworkPredictionExtrasFlyingPawn_eventGetMaxMoveSpeed_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NetworkPredictionExtrasFlyingPawn_eventGetMaxMoveSpeed_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Gameplay" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn, nullptr, "GetMaxMoveSpeed", nullptr, nullptr, sizeof(NetworkPredictionExtrasFlyingPawn_eventGetMaxMoveSpeed_Parms), Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_PrintDebug_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_PrintDebug_Statics::Function_MetaDataParams[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_PrintDebug_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn, nullptr, "PrintDebug", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_PrintDebug_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_PrintDebug_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_PrintDebug()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_PrintDebug_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics
	{
		struct NetworkPredictionExtrasFlyingPawn_eventSetMaxMoveSpeed_Parms
		{
			float NewMaxMoveSpeed;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewMaxMoveSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics::NewProp_NewMaxMoveSpeed = { "NewMaxMoveSpeed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NetworkPredictionExtrasFlyingPawn_eventSetMaxMoveSpeed_Parms, NewMaxMoveSpeed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics::NewProp_NewMaxMoveSpeed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Gameplay" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn, nullptr, "SetMaxMoveSpeed", nullptr, nullptr, sizeof(NetworkPredictionExtrasFlyingPawn_eventSetMaxMoveSpeed_Parms), Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_NoRegister()
	{
		return ANetworkPredictionExtrasFlyingPawn::StaticClass();
	}
	struct Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InputPreset_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputPreset_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InputPreset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFakeAutonomousProxy_MetaData[];
#endif
		static void NewProp_bFakeAutonomousProxy_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFakeAutonomousProxy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FlyingMovementComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FlyingMovementComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_AddMaxMoveSpeed, "AddMaxMoveSpeed" }, // 2293877681
		{ &Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_GetMaxMoveSpeed, "GetMaxMoveSpeed" }, // 1102710358
		{ &Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_PrintDebug, "PrintDebug" }, // 689561697
		{ &Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_SetMaxMoveSpeed, "SetMaxMoveSpeed" }, // 2027048628
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Sample pawn that uses UFlyingMovementComponent. The main thing this provides is actually producing user input for the component/simulation to consume. */" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "NetworkPredictionExtrasFlyingPawn.h" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Sample pawn that uses UFlyingMovementComponent. The main thing this provides is actually producing user input for the component/simulation to consume." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_InputPreset_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_InputPreset_MetaData[] = {
		{ "Category", "Automation" },
		{ "Comment", "// For bFakeAutonomousProxy only\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
		{ "ToolTip", "For bFakeAutonomousProxy only" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_InputPreset = { "InputPreset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANetworkPredictionExtrasFlyingPawn, InputPreset), Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasFlyingInputPreset, METADATA_PARAMS(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_InputPreset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_InputPreset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_bFakeAutonomousProxy_MetaData[] = {
		{ "Category", "Automation" },
		{ "Comment", "/** Actor will behave like autonomous proxy even though not posessed by an APlayercontroller. To be used in conjuction with InputPreset. */" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
		{ "ToolTip", "Actor will behave like autonomous proxy even though not posessed by an APlayercontroller. To be used in conjuction with InputPreset." },
	};
#endif
	void Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_bFakeAutonomousProxy_SetBit(void* Obj)
	{
		((ANetworkPredictionExtrasFlyingPawn*)Obj)->bFakeAutonomousProxy = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_bFakeAutonomousProxy = { "bFakeAutonomousProxy", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANetworkPredictionExtrasFlyingPawn), &Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_bFakeAutonomousProxy_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_bFakeAutonomousProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_bFakeAutonomousProxy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_FlyingMovementComponent_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_FlyingMovementComponent = { "FlyingMovementComponent", nullptr, (EPropertyFlags)0x00200800000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANetworkPredictionExtrasFlyingPawn, FlyingMovementComponent), Z_Construct_UClass_UFlyingMovementComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_FlyingMovementComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_FlyingMovementComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_InputPreset_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_InputPreset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_bFakeAutonomousProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::NewProp_FlyingMovementComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANetworkPredictionExtrasFlyingPawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::ClassParams = {
		&ANetworkPredictionExtrasFlyingPawn::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANetworkPredictionExtrasFlyingPawn, 3458184504);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<ANetworkPredictionExtrasFlyingPawn>()
	{
		return ANetworkPredictionExtrasFlyingPawn::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANetworkPredictionExtrasFlyingPawn(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn, &ANetworkPredictionExtrasFlyingPawn::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("ANetworkPredictionExtrasFlyingPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANetworkPredictionExtrasFlyingPawn);
	DEFINE_FUNCTION(ANetworkPredictionExtrasFlyingPawn_MockAbility::execGetMaxStamina)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetMaxStamina();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANetworkPredictionExtrasFlyingPawn_MockAbility::execGetStamina)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetStamina();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANetworkPredictionExtrasFlyingPawn_MockAbility::execGetMockFlyingAbilityComponent)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMockFlyingAbilityComponent**)Z_Param__Result=P_THIS->GetMockFlyingAbilityComponent();
		P_NATIVE_END;
	}
	void ANetworkPredictionExtrasFlyingPawn_MockAbility::StaticRegisterNativesANetworkPredictionExtrasFlyingPawn_MockAbility()
	{
		UClass* Class = ANetworkPredictionExtrasFlyingPawn_MockAbility::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetMaxStamina", &ANetworkPredictionExtrasFlyingPawn_MockAbility::execGetMaxStamina },
			{ "GetMockFlyingAbilityComponent", &ANetworkPredictionExtrasFlyingPawn_MockAbility::execGetMockFlyingAbilityComponent },
			{ "GetStamina", &ANetworkPredictionExtrasFlyingPawn_MockAbility::execGetStamina },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics
	{
		struct NetworkPredictionExtrasFlyingPawn_MockAbility_eventGetMaxStamina_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NetworkPredictionExtrasFlyingPawn_MockAbility_eventGetMaxStamina_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility, nullptr, "GetMaxStamina", nullptr, nullptr, sizeof(NetworkPredictionExtrasFlyingPawn_MockAbility_eventGetMaxStamina_Parms), Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics
	{
		struct NetworkPredictionExtrasFlyingPawn_MockAbility_eventGetMockFlyingAbilityComponent_Parms
		{
			UMockFlyingAbilityComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NetworkPredictionExtrasFlyingPawn_MockAbility_eventGetMockFlyingAbilityComponent_Parms, ReturnValue), Z_Construct_UClass_UMockFlyingAbilityComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility, nullptr, "GetMockFlyingAbilityComponent", nullptr, nullptr, sizeof(NetworkPredictionExtrasFlyingPawn_MockAbility_eventGetMockFlyingAbilityComponent_Parms), Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics
	{
		struct NetworkPredictionExtrasFlyingPawn_MockAbility_eventGetStamina_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NetworkPredictionExtrasFlyingPawn_MockAbility_eventGetStamina_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility, nullptr, "GetStamina", nullptr, nullptr, sizeof(NetworkPredictionExtrasFlyingPawn_MockAbility_eventGetStamina_Parms), Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_NoRegister()
	{
		return ANetworkPredictionExtrasFlyingPawn_MockAbility::StaticClass();
	}
	struct Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AbilityInputPreset_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AbilityInputPreset_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AbilityInputPreset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMaxStamina, "GetMaxStamina" }, // 1193986918
		{ &Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetMockFlyingAbilityComponent, "GetMockFlyingAbilityComponent" }, // 4125839784
		{ &Z_Construct_UFunction_ANetworkPredictionExtrasFlyingPawn_MockAbility_GetStamina, "GetStamina" }, // 1852095957
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Example subclass of ANetworkPredictionExtrasFlyingPawn that uses the MockAbility simulation\n" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "NetworkPredictionExtrasFlyingPawn.h" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Example subclass of ANetworkPredictionExtrasFlyingPawn that uses the MockAbility simulation" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::NewProp_AbilityInputPreset_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::NewProp_AbilityInputPreset_MetaData[] = {
		{ "Category", "Automation" },
		{ "ModuleRelativePath", "Public/NetworkPredictionExtrasFlyingPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::NewProp_AbilityInputPreset = { "AbilityInputPreset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANetworkPredictionExtrasFlyingPawn_MockAbility, AbilityInputPreset), Z_Construct_UEnum_NetworkPredictionExtras_ENetworkPredictionExtrasMockAbilityInputPreset, METADATA_PARAMS(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::NewProp_AbilityInputPreset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::NewProp_AbilityInputPreset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::NewProp_AbilityInputPreset_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::NewProp_AbilityInputPreset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANetworkPredictionExtrasFlyingPawn_MockAbility>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::ClassParams = {
		&ANetworkPredictionExtrasFlyingPawn_MockAbility::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANetworkPredictionExtrasFlyingPawn_MockAbility, 3869120015);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<ANetworkPredictionExtrasFlyingPawn_MockAbility>()
	{
		return ANetworkPredictionExtrasFlyingPawn_MockAbility::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility(Z_Construct_UClass_ANetworkPredictionExtrasFlyingPawn_MockAbility, &ANetworkPredictionExtrasFlyingPawn_MockAbility::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("ANetworkPredictionExtrasFlyingPawn_MockAbility"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANetworkPredictionExtrasFlyingPawn_MockAbility);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
