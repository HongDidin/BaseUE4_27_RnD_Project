// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETWORKPREDICTIONEXTRAS_MockPhysicsGrenade_generated_h
#error "MockPhysicsGrenade.generated.h already included, missing '#pragma once' in MockPhysicsGrenade.h"
#endif
#define NETWORKPREDICTIONEXTRAS_MockPhysicsGrenade_generated_h

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_74_DELEGATE \
static inline void FMockGrenadeOnExplode_DelegateWrapper(const FMulticastScriptDelegate& MockGrenadeOnExplode) \
{ \
	MockGrenadeOnExplode.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_RPC_WRAPPERS
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMockPhysicsGrenadeComponent(); \
	friend struct Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics; \
public: \
	DECLARE_CLASS(UMockPhysicsGrenadeComponent, UNetworkPredictionPhysicsComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockPhysicsGrenadeComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_INCLASS \
private: \
	static void StaticRegisterNativesUMockPhysicsGrenadeComponent(); \
	friend struct Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics; \
public: \
	DECLARE_CLASS(UMockPhysicsGrenadeComponent, UNetworkPredictionPhysicsComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockPhysicsGrenadeComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMockPhysicsGrenadeComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMockPhysicsGrenadeComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockPhysicsGrenadeComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockPhysicsGrenadeComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockPhysicsGrenadeComponent(UMockPhysicsGrenadeComponent&&); \
	NO_API UMockPhysicsGrenadeComponent(const UMockPhysicsGrenadeComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMockPhysicsGrenadeComponent() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockPhysicsGrenadeComponent(UMockPhysicsGrenadeComponent&&); \
	NO_API UMockPhysicsGrenadeComponent(const UMockPhysicsGrenadeComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockPhysicsGrenadeComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockPhysicsGrenadeComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMockPhysicsGrenadeComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FuseTimeSeconds() { return STRUCT_OFFSET(UMockPhysicsGrenadeComponent, FuseTimeSeconds); } \
	FORCEINLINE static uint32 __PPO__Radius() { return STRUCT_OFFSET(UMockPhysicsGrenadeComponent, Radius); } \
	FORCEINLINE static uint32 __PPO__Magnitude() { return STRUCT_OFFSET(UMockPhysicsGrenadeComponent, Magnitude); }


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_57_PROLOG
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_INCLASS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h_60_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<class UMockPhysicsGrenadeComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsGrenade_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
