// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPredictionExtras/Public/MockPhysicsGrenade.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMockPhysicsGrenade() {}
// Cross Module References
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockPhysicsGrenadeComponent();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UMockPhysicsGrenadeComponent_NoRegister();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_UNetworkPredictionPhysicsComponent();
	UPackage* Z_Construct_UPackage__Script_NetworkPredictionExtras();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MockPhysicsGrenade.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMockPhysicsGrenadeComponent, nullptr, "MockGrenadeOnExplode__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UMockPhysicsGrenadeComponent::StaticRegisterNativesUMockPhysicsGrenadeComponent()
	{
	}
	UClass* Z_Construct_UClass_UMockPhysicsGrenadeComponent_NoRegister()
	{
		return UMockPhysicsGrenadeComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_EDITOR
		static const FClassFunctionLinkInfo FuncInfo[];
#endif //WITH_EDITOR
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnExplode_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnExplode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FuseTimeSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FuseTimeSeconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Magnitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Magnitude;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNetworkPredictionPhysicsComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
#if WITH_EDITOR
	const FClassFunctionLinkInfo Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature, "MockGrenadeOnExplode__DelegateSignature" }, // 2130031406
	};
#endif //WITH_EDITOR
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "Comment", "// -----------------------------------------------------------------------------------------------------------\n// -----------------------------------------------------------------------------------------------------------\n" },
		{ "IncludePath", "MockPhysicsGrenade.h" },
		{ "ModuleRelativePath", "Public/MockPhysicsGrenade.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_OnExplode_MetaData[] = {
		{ "Category", "Mock Grenade" },
		{ "ModuleRelativePath", "Public/MockPhysicsGrenade.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_OnExplode = { "OnExplode", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockPhysicsGrenadeComponent, OnExplode), Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_OnExplode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_OnExplode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_FuseTimeSeconds_MetaData[] = {
		{ "Category", "Grenade" },
		{ "ModuleRelativePath", "Public/MockPhysicsGrenade.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_FuseTimeSeconds = { "FuseTimeSeconds", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockPhysicsGrenadeComponent, FuseTimeSeconds), METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_FuseTimeSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_FuseTimeSeconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "Grenade" },
		{ "ModuleRelativePath", "Public/MockPhysicsGrenade.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockPhysicsGrenadeComponent, Radius), METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_Magnitude_MetaData[] = {
		{ "Category", "Grenade" },
		{ "ModuleRelativePath", "Public/MockPhysicsGrenade.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_Magnitude = { "Magnitude", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMockPhysicsGrenadeComponent, Magnitude), METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_Magnitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_Magnitude_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_OnExplode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_FuseTimeSeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::NewProp_Magnitude,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockPhysicsGrenadeComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::ClassParams = {
		&UMockPhysicsGrenadeComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		IF_WITH_EDITOR(FuncInfo, nullptr),
		Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		IF_WITH_EDITOR(UE_ARRAY_COUNT(FuncInfo), 0),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockPhysicsGrenadeComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockPhysicsGrenadeComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockPhysicsGrenadeComponent, 3638965969);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UMockPhysicsGrenadeComponent>()
	{
		return UMockPhysicsGrenadeComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockPhysicsGrenadeComponent(Z_Construct_UClass_UMockPhysicsGrenadeComponent, &UMockPhysicsGrenadeComponent::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UMockPhysicsGrenadeComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockPhysicsGrenadeComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
