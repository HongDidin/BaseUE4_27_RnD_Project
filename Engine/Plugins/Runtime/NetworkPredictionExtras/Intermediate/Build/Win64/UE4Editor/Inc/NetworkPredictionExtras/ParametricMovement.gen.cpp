// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPredictionExtras/Public/ParametricMovement.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeParametricMovement() {}
// Cross Module References
	NETWORKPREDICTIONEXTRAS_API UScriptStruct* Z_Construct_UScriptStruct_FSimpleParametricMotion();
	UPackage* Z_Construct_UPackage__Script_NetworkPredictionExtras();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UParametricMovementComponent_NoRegister();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UParametricMovementComponent();
	NETWORKPREDICTIONEXTRAS_API UClass* Z_Construct_UClass_UBaseMovementComponent();
// End Cross Module References
class UScriptStruct* FSimpleParametricMotion::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NETWORKPREDICTIONEXTRAS_API uint32 Get_Z_Construct_UScriptStruct_FSimpleParametricMotion_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSimpleParametricMotion, Z_Construct_UPackage__Script_NetworkPredictionExtras(), TEXT("SimpleParametricMotion"), sizeof(FSimpleParametricMotion), Get_Z_Construct_UScriptStruct_FSimpleParametricMotion_Hash());
	}
	return Singleton;
}
template<> NETWORKPREDICTIONEXTRAS_API UScriptStruct* StaticStruct<FSimpleParametricMotion>()
{
	return FSimpleParametricMotion::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSimpleParametricMotion(FSimpleParametricMotion::StaticStruct, TEXT("/Script/NetworkPredictionExtras"), TEXT("SimpleParametricMotion"), false, nullptr, nullptr);
static struct FScriptStruct_NetworkPredictionExtras_StaticRegisterNativesFSimpleParametricMotion
{
	FScriptStruct_NetworkPredictionExtras_StaticRegisterNativesFSimpleParametricMotion()
	{
		UScriptStruct::DeferCppStructOps<FSimpleParametricMotion>(FName(TEXT("SimpleParametricMotion")));
	}
} ScriptStruct_NetworkPredictionExtras_StaticRegisterNativesFSimpleParametricMotion;
	struct Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParametricDelta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParametricDelta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Extremely simple struct for defining parametric motion. This is editable in UParametricMovementComponent's defaults, and also used by the simulation code. \n" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
		{ "ToolTip", "Extremely simple struct for defining parametric motion. This is editable in UParametricMovementComponent's defaults, and also used by the simulation code." },
	};
#endif
	void* Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSimpleParametricMotion>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_ParametricDelta_MetaData[] = {
		{ "Category", "ParametricMovement" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_ParametricDelta = { "ParametricDelta", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSimpleParametricMotion, ParametricDelta), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_ParametricDelta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_ParametricDelta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_MinTime_MetaData[] = {
		{ "Category", "ParametricMovement" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_MinTime = { "MinTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSimpleParametricMotion, MinTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_MinTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_MinTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_MaxTime_MetaData[] = {
		{ "Category", "ParametricMovement" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_MaxTime = { "MaxTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSimpleParametricMotion, MaxTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_MaxTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_MaxTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_ParametricDelta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_MinTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::NewProp_MaxTime,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
		nullptr,
		&NewStructOps,
		"SimpleParametricMotion",
		sizeof(FSimpleParametricMotion),
		alignof(FSimpleParametricMotion),
		Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSimpleParametricMotion()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSimpleParametricMotion_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPredictionExtras();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SimpleParametricMotion"), sizeof(FSimpleParametricMotion), Get_Z_Construct_UScriptStruct_FSimpleParametricMotion_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSimpleParametricMotion_Hash() { return 1482903359U; }
	DEFINE_FUNCTION(UParametricMovementComponent::execEnableInterpolationMode)
	{
		P_GET_UBOOL(Z_Param_bValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EnableInterpolationMode(Z_Param_bValue);
		P_NATIVE_END;
	}
	void UParametricMovementComponent::StaticRegisterNativesUParametricMovementComponent()
	{
		UClass* Class = UParametricMovementComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "EnableInterpolationMode", &UParametricMovementComponent::execEnableInterpolationMode },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics
	{
		struct ParametricMovementComponent_eventEnableInterpolationMode_Parms
		{
			bool bValue;
		};
		static void NewProp_bValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::NewProp_bValue_SetBit(void* Obj)
	{
		((ParametricMovementComponent_eventEnableInterpolationMode_Parms*)Obj)->bValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::NewProp_bValue = { "bValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ParametricMovementComponent_eventEnableInterpolationMode_Parms), &Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::NewProp_bValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::NewProp_bValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "Networking" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UParametricMovementComponent, nullptr, "EnableInterpolationMode", nullptr, nullptr, sizeof(ParametricMovementComponent_eventEnableInterpolationMode_Parms), Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UParametricMovementComponent_NoRegister()
	{
		return UParametricMovementComponent::StaticClass();
	}
	struct Z_Construct_UClass_UParametricMovementComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisableParametricMovementSimulation_MetaData[];
#endif
		static void NewProp_bDisableParametricMovementSimulation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisableParametricMovementSimulation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParametricMotion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParametricMotion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableDependentSimulation_MetaData[];
#endif
		static void NewProp_bEnableDependentSimulation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableDependentSimulation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableInterpolation_MetaData[];
#endif
		static void NewProp_bEnableInterpolation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableInterpolation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableForceNetUpdate_MetaData[];
#endif
		static void NewProp_bEnableForceNetUpdate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableForceNetUpdate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentNetUpdateFrequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ParentNetUpdateFrequency;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UParametricMovementComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseMovementComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPredictionExtras,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UParametricMovementComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UParametricMovementComponent_EnableInterpolationMode, "EnableInterpolationMode" }, // 2132101797
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParametricMovementComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "Comment", "// -------------------------------------------------------------------------------------------------------------------------------\n//\x09""ActorComponent for running basic Parametric movement. \n//\x09Parametric movement could be anything that takes a Time and returns an FTransform.\n//\x09\n//\x09Initially, we will support pushing (ie, we sweep as we update the mover's position).\n//\x09""But we will not allow a parametric mover from being blocked. \n//\n// -------------------------------------------------------------------------------------------------------------------------------\n" },
		{ "IncludePath", "ParametricMovement.h" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
		{ "ToolTip", "ActorComponent for running basic Parametric movement.\nParametric movement could be anything that takes a Time and returns an FTransform.\n\nInitially, we will support pushing (ie, we sweep as we update the mover's position).\nBut we will not allow a parametric mover from being blocked." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bDisableParametricMovementSimulation_MetaData[] = {
		{ "Category", "ParametricMovement" },
		{ "Comment", "/** Disables starting the simulation. For development/testing ease of use */" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
		{ "ToolTip", "Disables starting the simulation. For development/testing ease of use" },
	};
#endif
	void Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bDisableParametricMovementSimulation_SetBit(void* Obj)
	{
		((UParametricMovementComponent*)Obj)->bDisableParametricMovementSimulation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bDisableParametricMovementSimulation = { "bDisableParametricMovementSimulation", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UParametricMovementComponent), &Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bDisableParametricMovementSimulation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bDisableParametricMovementSimulation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bDisableParametricMovementSimulation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_ParametricMotion_MetaData[] = {
		{ "Category", "ParametricMovement" },
		{ "ExposeOnSpawn", "TRUE" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_ParametricMotion = { "ParametricMotion", nullptr, (EPropertyFlags)0x0021080000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParametricMovementComponent, ParametricMotion), Z_Construct_UScriptStruct_FSimpleParametricMotion, METADATA_PARAMS(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_ParametricMotion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_ParametricMotion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableDependentSimulation_MetaData[] = {
		{ "Category", "ParametricMovementNetworking" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
	};
#endif
	void Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableDependentSimulation_SetBit(void* Obj)
	{
		((UParametricMovementComponent*)Obj)->bEnableDependentSimulation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableDependentSimulation = { "bEnableDependentSimulation", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UParametricMovementComponent), &Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableDependentSimulation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableDependentSimulation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableDependentSimulation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableInterpolation_MetaData[] = {
		{ "Category", "ParametricMovementNetworking" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
	};
#endif
	void Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableInterpolation_SetBit(void* Obj)
	{
		((UParametricMovementComponent*)Obj)->bEnableInterpolation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableInterpolation = { "bEnableInterpolation", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UParametricMovementComponent), &Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableInterpolation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableInterpolation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableInterpolation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableForceNetUpdate_MetaData[] = {
		{ "Category", "ParametricMovementNetworking" },
		{ "Comment", "/** Calls ForceNetUpdate every frame. Has slightly different behavior than a very high NetUpdateFrequency */" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
		{ "ToolTip", "Calls ForceNetUpdate every frame. Has slightly different behavior than a very high NetUpdateFrequency" },
	};
#endif
	void Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableForceNetUpdate_SetBit(void* Obj)
	{
		((UParametricMovementComponent*)Obj)->bEnableForceNetUpdate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableForceNetUpdate = { "bEnableForceNetUpdate", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UParametricMovementComponent), &Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableForceNetUpdate_SetBit, METADATA_PARAMS(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableForceNetUpdate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableForceNetUpdate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_ParentNetUpdateFrequency_MetaData[] = {
		{ "Category", "ParametricMovementNetworking" },
		{ "Comment", "/** Sets NetUpdateFrequency on parent. This is editable on the component and really just meant for use during development/test maps */" },
		{ "ModuleRelativePath", "Public/ParametricMovement.h" },
		{ "ToolTip", "Sets NetUpdateFrequency on parent. This is editable on the component and really just meant for use during development/test maps" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_ParentNetUpdateFrequency = { "ParentNetUpdateFrequency", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParametricMovementComponent, ParentNetUpdateFrequency), METADATA_PARAMS(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_ParentNetUpdateFrequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_ParentNetUpdateFrequency_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UParametricMovementComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bDisableParametricMovementSimulation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_ParametricMotion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableDependentSimulation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableInterpolation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_bEnableForceNetUpdate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParametricMovementComponent_Statics::NewProp_ParentNetUpdateFrequency,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UParametricMovementComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UParametricMovementComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UParametricMovementComponent_Statics::ClassParams = {
		&UParametricMovementComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UParametricMovementComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UParametricMovementComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UParametricMovementComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UParametricMovementComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UParametricMovementComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UParametricMovementComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UParametricMovementComponent, 4012961578);
	template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<UParametricMovementComponent>()
	{
		return UParametricMovementComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UParametricMovementComponent(Z_Construct_UClass_UParametricMovementComponent, &UParametricMovementComponent::StaticClass, TEXT("/Script/NetworkPredictionExtras"), TEXT("UParametricMovementComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UParametricMovementComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
