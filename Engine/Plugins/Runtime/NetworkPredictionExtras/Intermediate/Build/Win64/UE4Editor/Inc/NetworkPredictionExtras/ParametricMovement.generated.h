// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETWORKPREDICTIONEXTRAS_ParametricMovement_generated_h
#error "ParametricMovement.generated.h already included, missing '#pragma once' in ParametricMovement.h"
#endif
#define NETWORKPREDICTIONEXTRAS_ParametricMovement_generated_h

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSimpleParametricMotion_Statics; \
	NETWORKPREDICTIONEXTRAS_API static class UScriptStruct* StaticStruct();


template<> NETWORKPREDICTIONEXTRAS_API UScriptStruct* StaticStruct<struct FSimpleParametricMotion>();

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execEnableInterpolationMode);


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execEnableInterpolationMode);


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUParametricMovementComponent(); \
	friend struct Z_Construct_UClass_UParametricMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UParametricMovementComponent, UBaseMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UParametricMovementComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_INCLASS \
private: \
	static void StaticRegisterNativesUParametricMovementComponent(); \
	friend struct Z_Construct_UClass_UParametricMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UParametricMovementComponent, UBaseMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UParametricMovementComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UParametricMovementComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UParametricMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UParametricMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UParametricMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UParametricMovementComponent(UParametricMovementComponent&&); \
	NO_API UParametricMovementComponent(const UParametricMovementComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UParametricMovementComponent(UParametricMovementComponent&&); \
	NO_API UParametricMovementComponent(const UParametricMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UParametricMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UParametricMovementComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UParametricMovementComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bDisableParametricMovementSimulation() { return STRUCT_OFFSET(UParametricMovementComponent, bDisableParametricMovementSimulation); } \
	FORCEINLINE static uint32 __PPO__ParametricMotion() { return STRUCT_OFFSET(UParametricMovementComponent, ParametricMotion); } \
	FORCEINLINE static uint32 __PPO__bEnableDependentSimulation() { return STRUCT_OFFSET(UParametricMovementComponent, bEnableDependentSimulation); } \
	FORCEINLINE static uint32 __PPO__bEnableInterpolation() { return STRUCT_OFFSET(UParametricMovementComponent, bEnableInterpolation); } \
	FORCEINLINE static uint32 __PPO__bEnableForceNetUpdate() { return STRUCT_OFFSET(UParametricMovementComponent, bEnableForceNetUpdate); } \
	FORCEINLINE static uint32 __PPO__ParentNetUpdateFrequency() { return STRUCT_OFFSET(UParametricMovementComponent, ParentNetUpdateFrequency); }


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_142_PROLOG
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_INCLASS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h_147_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<class UParametricMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_ParametricMovement_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
