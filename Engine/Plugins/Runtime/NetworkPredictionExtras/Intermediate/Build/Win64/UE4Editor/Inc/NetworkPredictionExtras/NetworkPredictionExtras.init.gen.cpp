// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetworkPredictionExtras_init() {}
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature();
	NETWORKPREDICTIONEXTRAS_API UFunction* Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_NetworkPredictionExtras()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityNotifyStateChange__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityBlinkCueRollback__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockFlyingAbilityComponent_MockAbilityPhysicsGunFireEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityNotifyStateChange__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockCharacterAbilityComponent_MockCharacterAbilityBlinkCueRollback__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsJumpCueEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockPhysicsComponent_PhysicsChargeCueEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockPhysicsComponent_MockPhysicsNotifyStateChange__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UMockPhysicsGrenadeComponent_MockGrenadeOnExplode__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/NetworkPredictionExtras",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x86E75ED3,
				0x7B472F28,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
