// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef NETWORKPREDICTIONEXTRAS_MockPhysicsComponent_generated_h
#error "MockPhysicsComponent.generated.h already included, missing '#pragma once' in MockPhysicsComponent.h"
#endif
#define NETWORKPREDICTIONEXTRAS_MockPhysicsComponent_generated_h

#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_70_DELEGATE \
struct MockPhysicsComponent_eventMockPhysicsNotifyStateChange_Parms \
{ \
	bool bNewStateValue; \
}; \
static inline void FMockPhysicsNotifyStateChange_DelegateWrapper(const FMulticastScriptDelegate& MockPhysicsNotifyStateChange, bool bNewStateValue) \
{ \
	MockPhysicsComponent_eventMockPhysicsNotifyStateChange_Parms Parms; \
	Parms.bNewStateValue=bNewStateValue ? true : false; \
	MockPhysicsNotifyStateChange.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_62_DELEGATE \
struct MockPhysicsComponent_eventPhysicsChargeCueEvent_Parms \
{ \
	FVector Location; \
	float ElapsedTimeSeconds; \
}; \
static inline void FPhysicsChargeCueEvent_DelegateWrapper(const FMulticastScriptDelegate& PhysicsChargeCueEvent, FVector Location, float ElapsedTimeSeconds) \
{ \
	MockPhysicsComponent_eventPhysicsChargeCueEvent_Parms Parms; \
	Parms.Location=Location; \
	Parms.ElapsedTimeSeconds=ElapsedTimeSeconds; \
	PhysicsChargeCueEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_56_DELEGATE \
struct MockPhysicsComponent_eventPhysicsJumpCueEvent_Parms \
{ \
	FVector Location; \
	float ElapsedTimeSeconds; \
}; \
static inline void FPhysicsJumpCueEvent_DelegateWrapper(const FMulticastScriptDelegate& PhysicsJumpCueEvent, FVector Location, float ElapsedTimeSeconds) \
{ \
	MockPhysicsComponent_eventPhysicsJumpCueEvent_Parms Parms; \
	Parms.Location=Location; \
	Parms.ElapsedTimeSeconds=ElapsedTimeSeconds; \
	PhysicsJumpCueEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_RPC_WRAPPERS
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMockPhysicsComponent(); \
	friend struct Z_Construct_UClass_UMockPhysicsComponent_Statics; \
public: \
	DECLARE_CLASS(UMockPhysicsComponent, UBaseMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockPhysicsComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUMockPhysicsComponent(); \
	friend struct Z_Construct_UClass_UMockPhysicsComponent_Statics; \
public: \
	DECLARE_CLASS(UMockPhysicsComponent, UBaseMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPredictionExtras"), NO_API) \
	DECLARE_SERIALIZER(UMockPhysicsComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMockPhysicsComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMockPhysicsComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockPhysicsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockPhysicsComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockPhysicsComponent(UMockPhysicsComponent&&); \
	NO_API UMockPhysicsComponent(const UMockPhysicsComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockPhysicsComponent(UMockPhysicsComponent&&); \
	NO_API UMockPhysicsComponent(const UMockPhysicsComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockPhysicsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockPhysicsComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMockPhysicsComponent)


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_20_PROLOG
#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_INCLASS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTIONEXTRAS_API UClass* StaticClass<class UMockPhysicsComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPredictionExtras_Source_NetworkPredictionExtras_Public_MockPhysicsComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
