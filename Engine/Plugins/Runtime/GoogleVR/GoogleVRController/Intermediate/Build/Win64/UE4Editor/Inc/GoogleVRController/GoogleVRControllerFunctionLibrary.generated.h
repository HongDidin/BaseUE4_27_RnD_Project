// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EControllerHand : uint8;
enum class EGoogleVRControllerBatteryLevel : uint8;
struct FVector;
class UGoogleVRControllerEventManager;
struct FRotator;
enum class EGoogleVRControllerHandedness : uint8;
enum class EGoogleVRControllerState : uint8;
enum class EGoogleVRControllerAPIStatus : uint8;
#ifdef GOOGLEVRCONTROLLER_GoogleVRControllerFunctionLibrary_generated_h
#error "GoogleVRControllerFunctionLibrary.generated.h already included, missing '#pragma once' in GoogleVRControllerFunctionLibrary.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRControllerFunctionLibrary_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetBatteryLevel); \
	DECLARE_FUNCTION(execGetBatteryCharging); \
	DECLARE_FUNCTION(execGetTooltipAlphaValue); \
	DECLARE_FUNCTION(execGetControllerAlphaValue); \
	DECLARE_FUNCTION(execSetTooltipMaxAngleFromCamera); \
	DECLARE_FUNCTION(execGetTooltipMaxAngleFromCamera); \
	DECLARE_FUNCTION(execSetTooltipMinDistanceFromFace); \
	DECLARE_FUNCTION(execGetTooltipMinDistanceFromFace); \
	DECLARE_FUNCTION(execSetFadeDistanceFromFace); \
	DECLARE_FUNCTION(execGetFadeDistanceFromFace); \
	DECLARE_FUNCTION(execGetArmModelIsLockedToHead); \
	DECLARE_FUNCTION(execSetArmModelIsLockedToHead); \
	DECLARE_FUNCTION(execSetWillArmModelUseAccelerometer); \
	DECLARE_FUNCTION(execWillArmModelUseAccelerometer); \
	DECLARE_FUNCTION(execSetArmModelPointerTiltAngle); \
	DECLARE_FUNCTION(execGetArmModelPointerTiltAngle); \
	DECLARE_FUNCTION(execSetArmModelAddedElbowDepth); \
	DECLARE_FUNCTION(execGetArmModelAddedElbowDepth); \
	DECLARE_FUNCTION(execSetArmModelAddedElbowHeight); \
	DECLARE_FUNCTION(execGetArmModelAddedElbowHeight); \
	DECLARE_FUNCTION(execGetArmModelPointerPositionOffset); \
	DECLARE_FUNCTION(execSetArmModelEnabled); \
	DECLARE_FUNCTION(execIsArmModelEnabled); \
	DECLARE_FUNCTION(execGetGoogleVRControllerEventManager); \
	DECLARE_FUNCTION(execGetGoogleVRControllerOrientation); \
	DECLARE_FUNCTION(execGetGoogleVRControllerRawGyro); \
	DECLARE_FUNCTION(execGetGoogleVRControllerRawAccel); \
	DECLARE_FUNCTION(execGetGoogleVRControllerHandedness); \
	DECLARE_FUNCTION(execGetGoogleVRControllerState); \
	DECLARE_FUNCTION(execGetGoogleVRControllerAPIStatus);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetBatteryLevel); \
	DECLARE_FUNCTION(execGetBatteryCharging); \
	DECLARE_FUNCTION(execGetTooltipAlphaValue); \
	DECLARE_FUNCTION(execGetControllerAlphaValue); \
	DECLARE_FUNCTION(execSetTooltipMaxAngleFromCamera); \
	DECLARE_FUNCTION(execGetTooltipMaxAngleFromCamera); \
	DECLARE_FUNCTION(execSetTooltipMinDistanceFromFace); \
	DECLARE_FUNCTION(execGetTooltipMinDistanceFromFace); \
	DECLARE_FUNCTION(execSetFadeDistanceFromFace); \
	DECLARE_FUNCTION(execGetFadeDistanceFromFace); \
	DECLARE_FUNCTION(execGetArmModelIsLockedToHead); \
	DECLARE_FUNCTION(execSetArmModelIsLockedToHead); \
	DECLARE_FUNCTION(execSetWillArmModelUseAccelerometer); \
	DECLARE_FUNCTION(execWillArmModelUseAccelerometer); \
	DECLARE_FUNCTION(execSetArmModelPointerTiltAngle); \
	DECLARE_FUNCTION(execGetArmModelPointerTiltAngle); \
	DECLARE_FUNCTION(execSetArmModelAddedElbowDepth); \
	DECLARE_FUNCTION(execGetArmModelAddedElbowDepth); \
	DECLARE_FUNCTION(execSetArmModelAddedElbowHeight); \
	DECLARE_FUNCTION(execGetArmModelAddedElbowHeight); \
	DECLARE_FUNCTION(execGetArmModelPointerPositionOffset); \
	DECLARE_FUNCTION(execSetArmModelEnabled); \
	DECLARE_FUNCTION(execIsArmModelEnabled); \
	DECLARE_FUNCTION(execGetGoogleVRControllerEventManager); \
	DECLARE_FUNCTION(execGetGoogleVRControllerOrientation); \
	DECLARE_FUNCTION(execGetGoogleVRControllerRawGyro); \
	DECLARE_FUNCTION(execGetGoogleVRControllerRawAccel); \
	DECLARE_FUNCTION(execGetGoogleVRControllerHandedness); \
	DECLARE_FUNCTION(execGetGoogleVRControllerState); \
	DECLARE_FUNCTION(execGetGoogleVRControllerAPIStatus);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRControllerFunctionLibrary(); \
	friend struct Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRControllerFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRControllerFunctionLibrary)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRControllerFunctionLibrary(); \
	friend struct Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRControllerFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRControllerFunctionLibrary)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRControllerFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRControllerFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRControllerFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRControllerFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRControllerFunctionLibrary(UGoogleVRControllerFunctionLibrary&&); \
	NO_API UGoogleVRControllerFunctionLibrary(const UGoogleVRControllerFunctionLibrary&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRControllerFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRControllerFunctionLibrary(UGoogleVRControllerFunctionLibrary&&); \
	NO_API UGoogleVRControllerFunctionLibrary(const UGoogleVRControllerFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRControllerFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRControllerFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRControllerFunctionLibrary)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_68_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h_71_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GoogleVRControllerFunctionLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRControllerFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerFunctionLibrary_h


#define FOREACH_ENUM_EGOOGLEVRCONTROLLERBATTERYLEVEL(op) \
	op(EGoogleVRControllerBatteryLevel::Unknown) \
	op(EGoogleVRControllerBatteryLevel::CriticalLow) \
	op(EGoogleVRControllerBatteryLevel::Low) \
	op(EGoogleVRControllerBatteryLevel::Medium) \
	op(EGoogleVRControllerBatteryLevel::AlmostFull) \
	op(EGoogleVRControllerBatteryLevel::Full) 

enum class EGoogleVRControllerBatteryLevel : uint8;
template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRControllerBatteryLevel>();

#define FOREACH_ENUM_EGOOGLEVRARMMODELFOLLOWGAZEBEHAVIOR(op) \
	op(EGoogleVRArmModelFollowGazeBehavior::Never) \
	op(EGoogleVRArmModelFollowGazeBehavior::DuringMotion) \
	op(EGoogleVRArmModelFollowGazeBehavior::Always) 

enum class EGoogleVRArmModelFollowGazeBehavior : uint8;
template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRArmModelFollowGazeBehavior>();

#define FOREACH_ENUM_EGOOGLEVRCONTROLLERHANDEDNESS(op) \
	op(EGoogleVRControllerHandedness::RightHanded) \
	op(EGoogleVRControllerHandedness::LeftHanded) \
	op(EGoogleVRControllerHandedness::Unknown) 

enum class EGoogleVRControllerHandedness : uint8;
template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRControllerHandedness>();

#define FOREACH_ENUM_EGOOGLEVRCONTROLLERAPISTATUS(op) \
	op(EGoogleVRControllerAPIStatus::OK) \
	op(EGoogleVRControllerAPIStatus::Unsupported) \
	op(EGoogleVRControllerAPIStatus::NotAuthorized) \
	op(EGoogleVRControllerAPIStatus::Unavailable) \
	op(EGoogleVRControllerAPIStatus::ServiceObsolete) \
	op(EGoogleVRControllerAPIStatus::ClientObsolete) \
	op(EGoogleVRControllerAPIStatus::Malfunction) \
	op(EGoogleVRControllerAPIStatus::Unknown) 

enum class EGoogleVRControllerAPIStatus : uint8;
template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRControllerAPIStatus>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
