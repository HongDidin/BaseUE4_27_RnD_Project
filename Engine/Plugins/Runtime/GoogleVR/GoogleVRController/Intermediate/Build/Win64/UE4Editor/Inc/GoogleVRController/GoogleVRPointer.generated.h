// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEVRCONTROLLER_GoogleVRPointer_generated_h
#error "GoogleVRPointer.generated.h already included, missing '#pragma once' in GoogleVRPointer.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRPointer_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_RPC_WRAPPERS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRPointer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRPointer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRPointer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRPointer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRPointer(UGoogleVRPointer&&); \
	NO_API UGoogleVRPointer(const UGoogleVRPointer&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRPointer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRPointer(UGoogleVRPointer&&); \
	NO_API UGoogleVRPointer(const UGoogleVRPointer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRPointer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRPointer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRPointer)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUGoogleVRPointer(); \
	friend struct Z_Construct_UClass_UGoogleVRPointer_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRPointer, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRPointer)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IGoogleVRPointer() {} \
public: \
	typedef UGoogleVRPointer UClassType; \
	typedef IGoogleVRPointer ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_INCLASS_IINTERFACE \
protected: \
	virtual ~IGoogleVRPointer() {} \
public: \
	typedef UGoogleVRPointer UClassType; \
	typedef IGoogleVRPointer ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_46_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_54_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_54_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h_49_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRPointer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointer_h


#define FOREACH_ENUM_EGOOGLEVRPOINTERINPUTMODE(op) \
	op(EGoogleVRPointerInputMode::Camera) \
	op(EGoogleVRPointerInputMode::Direct) \
	op(EGoogleVRPointerInputMode::HybridExperimental) 

enum class EGoogleVRPointerInputMode : uint8;
template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRPointerInputMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
