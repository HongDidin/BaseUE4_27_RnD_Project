// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRController/Classes/GoogleVRPointerInputComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRPointerInputComponent() {}
// Cross Module References
	GOOGLEVRCONTROLLER_API UFunction* Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_GoogleVRController();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	GOOGLEVRCONTROLLER_API UFunction* Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	GOOGLEVRCONTROLLER_API UFunction* Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRPointerInputComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRPointer_NoRegister();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRWidgetInteractionComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics
	{
		struct _Script_GoogleVRController_eventGoogleVRInputExitComponentDelegate_Parms
		{
			UPrimitiveComponent* PreviousComponent;
			FHitResult HitResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviousComponent;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::NewProp_PreviousComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::NewProp_PreviousComponent = { "PreviousComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_GoogleVRController_eventGoogleVRInputExitComponentDelegate_Parms, PreviousComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::NewProp_PreviousComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::NewProp_PreviousComponent_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_GoogleVRController_eventGoogleVRInputExitComponentDelegate_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::NewProp_PreviousComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::NewProp_HitResult,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController, nullptr, "GoogleVRInputExitComponentDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_GoogleVRController_eventGoogleVRInputExitComponentDelegate_Parms), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics
	{
		struct _Script_GoogleVRController_eventGoogleVRInputExitActorDelegate_Parms
		{
			AActor* PreviousActor;
			FHitResult HitResult;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviousActor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::NewProp_PreviousActor = { "PreviousActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_GoogleVRController_eventGoogleVRInputExitActorDelegate_Parms, PreviousActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_GoogleVRController_eventGoogleVRInputExitActorDelegate_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::NewProp_PreviousActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::NewProp_HitResult,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController, nullptr, "GoogleVRInputExitActorDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_GoogleVRController_eventGoogleVRInputExitActorDelegate_Parms), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics
	{
		struct _Script_GoogleVRController_eventGoogleVRInputDelegate_Parms
		{
			FHitResult HitResult;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_GoogleVRController_eventGoogleVRInputDelegate_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics::NewProp_HitResult,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController, nullptr, "GoogleVRInputDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_GoogleVRController_eventGoogleVRInputDelegate_Parms), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UGoogleVRPointerInputComponent::execGetLatestHitResult)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FHitResult*)Z_Param__Result=P_THIS->GetLatestHitResult();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRPointerInputComponent::execGetIntersectionLocation)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=P_THIS->GetIntersectionLocation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRPointerInputComponent::execGetHitComponent)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UPrimitiveComponent**)Z_Param__Result=P_THIS->GetHitComponent();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRPointerInputComponent::execGetHitActor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=P_THIS->GetHitActor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRPointerInputComponent::execIsBlockingHit)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsBlockingHit();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRPointerInputComponent::execGetPointer)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TScriptInterface<IGoogleVRPointer>*)Z_Param__Result=P_THIS->GetPointer();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRPointerInputComponent::execSetPointer)
	{
		P_GET_TINTERFACE(IGoogleVRPointer,Z_Param_NewPointer);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPointer(Z_Param_NewPointer);
		P_NATIVE_END;
	}
	void UGoogleVRPointerInputComponent::StaticRegisterNativesUGoogleVRPointerInputComponent()
	{
		UClass* Class = UGoogleVRPointerInputComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetHitActor", &UGoogleVRPointerInputComponent::execGetHitActor },
			{ "GetHitComponent", &UGoogleVRPointerInputComponent::execGetHitComponent },
			{ "GetIntersectionLocation", &UGoogleVRPointerInputComponent::execGetIntersectionLocation },
			{ "GetLatestHitResult", &UGoogleVRPointerInputComponent::execGetLatestHitResult },
			{ "GetPointer", &UGoogleVRPointerInputComponent::execGetPointer },
			{ "IsBlockingHit", &UGoogleVRPointerInputComponent::execIsBlockingHit },
			{ "SetPointer", &UGoogleVRPointerInputComponent::execSetPointer },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics
	{
		struct GoogleVRPointerInputComponent_eventGetHitActor_Parms
		{
			AActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRPointerInputComponent_eventGetHitActor_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRPointerInput" },
		{ "Comment", "/** The actor that is being pointed at. */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "The actor that is being pointed at." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRPointerInputComponent, nullptr, "GetHitActor", nullptr, nullptr, sizeof(GoogleVRPointerInputComponent_eventGetHitActor_Parms), Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics
	{
		struct GoogleVRPointerInputComponent_eventGetHitComponent_Parms
		{
			UPrimitiveComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRPointerInputComponent_eventGetHitComponent_Parms, ReturnValue), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRPointerInput" },
		{ "Comment", "/** The component that the actor being pointed at. */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "The component that the actor being pointed at." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRPointerInputComponent, nullptr, "GetHitComponent", nullptr, nullptr, sizeof(GoogleVRPointerInputComponent_eventGetHitComponent_Parms), Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics
	{
		struct GoogleVRPointerInputComponent_eventGetIntersectionLocation_Parms
		{
			FVector ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRPointerInputComponent_eventGetIntersectionLocation_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRPointerInput" },
		{ "Comment", "/** The world location where the pointer intersected with the hit actor. */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "The world location where the pointer intersected with the hit actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRPointerInputComponent, nullptr, "GetIntersectionLocation", nullptr, nullptr, sizeof(GoogleVRPointerInputComponent_eventGetIntersectionLocation_Parms), Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics
	{
		struct GoogleVRPointerInputComponent_eventGetLatestHitResult_Parms
		{
			FHitResult ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRPointerInputComponent_eventGetLatestHitResult_Parms, ReturnValue), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRPointerInput" },
		{ "Comment", "/** Get the result of the latest hit detection. */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Get the result of the latest hit detection." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRPointerInputComponent, nullptr, "GetLatestHitResult", nullptr, nullptr, sizeof(GoogleVRPointerInputComponent_eventGetLatestHitResult_Parms), Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics
	{
		struct GoogleVRPointerInputComponent_eventGetPointer_Parms
		{
			TScriptInterface<IGoogleVRPointer> ReturnValue;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0014000000000580, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRPointerInputComponent_eventGetPointer_Parms, ReturnValue), Z_Construct_UClass_UGoogleVRPointer_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRPointerInput" },
		{ "Comment", "/** Get the Pointer being used for this input component. */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Get the Pointer being used for this input component." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRPointerInputComponent, nullptr, "GetPointer", nullptr, nullptr, sizeof(GoogleVRPointerInputComponent_eventGetPointer_Parms), Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics
	{
		struct GoogleVRPointerInputComponent_eventIsBlockingHit_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRPointerInputComponent_eventIsBlockingHit_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRPointerInputComponent_eventIsBlockingHit_Parms), &Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRPointerInput" },
		{ "Comment", "/** Returns true if there was a blocking hit. */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Returns true if there was a blocking hit." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRPointerInputComponent, nullptr, "IsBlockingHit", nullptr, nullptr, sizeof(GoogleVRPointerInputComponent_eventIsBlockingHit_Parms), Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics
	{
		struct GoogleVRPointerInputComponent_eventSetPointer_Parms
		{
			TScriptInterface<IGoogleVRPointer> NewPointer;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_NewPointer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics::NewProp_NewPointer = { "NewPointer", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRPointerInputComponent_eventSetPointer_Parms, NewPointer), Z_Construct_UClass_UGoogleVRPointer_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics::NewProp_NewPointer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRPointerInput" },
		{ "Comment", "/** Set the Pointer to use for this input component. */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Set the Pointer to use for this input component." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRPointerInputComponent, nullptr, "SetPointer", nullptr, nullptr, sizeof(GoogleVRPointerInputComponent_eventSetPointer_Parms), Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister()
	{
		return UGoogleVRPointerInputComponent::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FarClippingDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FarClippingDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NearClippingDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NearClippingDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UseControllerClick_MetaData[];
#endif
		static void NewProp_UseControllerClick_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_UseControllerClick;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UseTouchClick_MetaData[];
#endif
		static void NewProp_UseTouchClick_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_UseTouchClick;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidgetInteraction_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WidgetInteraction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPointerEnterActorEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPointerEnterActorEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPointerEnterComponentEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPointerEnterComponentEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPointerExitActorEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPointerExitActorEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPointerExitComponentEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPointerExitComponentEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPointerHoverActorEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPointerHoverActorEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPointerHoverComponentEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPointerHoverComponentEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPointerClickActorEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPointerClickActorEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPointerClickComponentEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPointerClickComponentEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPointerPressedEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPointerPressedEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPointerReleasedEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPointerReleasedEvent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRController,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitActor, "GetHitActor" }, // 1956577405
		{ &Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetHitComponent, "GetHitComponent" }, // 3220421597
		{ &Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetIntersectionLocation, "GetIntersectionLocation" }, // 2463095557
		{ &Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetLatestHitResult, "GetLatestHitResult" }, // 1750214680
		{ &Z_Construct_UFunction_UGoogleVRPointerInputComponent_GetPointer, "GetPointer" }, // 3107118694
		{ &Z_Construct_UFunction_UGoogleVRPointerInputComponent_IsBlockingHit, "IsBlockingHit" }, // 3051746560
		{ &Z_Construct_UFunction_UGoogleVRPointerInputComponent_SetPointer, "SetPointer" }, // 572939749
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "GoogleVRController" },
		{ "Comment", "/**\n * GoogleVRPointerInputComponent is used to interact with Actors and Widgets by\n * using a 3D pointer. The pointer can be a cardboard reticle, or a daydream controller.\n *\n * @see UGoogleVRMotionControllerComponent\n * @see UGoogleVRGazeReticleComponent\n */" },
		{ "IncludePath", "GoogleVRPointerInputComponent.h" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "GoogleVRPointerInputComponent is used to interact with Actors and Widgets by\nusing a 3D pointer. The pointer can be a cardboard reticle, or a daydream controller.\n\n@see UGoogleVRMotionControllerComponent\n@see UGoogleVRGazeReticleComponent" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_FarClippingDistance_MetaData[] = {
		{ "Category", "Pointer" },
		{ "Comment", "/** The maximum distance an object can be from the start of the pointer for the pointer to hit it. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "The maximum distance an object can be from the start of the pointer for the pointer to hit it." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_FarClippingDistance = { "FarClippingDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, FarClippingDistance), METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_FarClippingDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_FarClippingDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_NearClippingDistance_MetaData[] = {
		{ "Category", "Pointer" },
		{ "Comment", "/** The minimum distance an object needs to be from the camera for the pointer to hit it.\n\x09 *  Note: Only used when PointerInputMode is set to Camera.\n\x09 */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "The minimum distance an object needs to be from the camera for the pointer to hit it.\nNote: Only used when PointerInputMode is set to Camera." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_NearClippingDistance = { "NearClippingDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, NearClippingDistance), METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_NearClippingDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_NearClippingDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseControllerClick_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Determines if pointer clicks will occur from controller clicks. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Determines if pointer clicks will occur from controller clicks." },
	};
#endif
	void Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseControllerClick_SetBit(void* Obj)
	{
		((UGoogleVRPointerInputComponent*)Obj)->UseControllerClick = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseControllerClick = { "UseControllerClick", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGoogleVRPointerInputComponent), &Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseControllerClick_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseControllerClick_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseControllerClick_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseTouchClick_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Determines if pointer clicks will occur from touching the screen. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Determines if pointer clicks will occur from touching the screen." },
	};
#endif
	void Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseTouchClick_SetBit(void* Obj)
	{
		((UGoogleVRPointerInputComponent*)Obj)->UseTouchClick = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseTouchClick = { "UseTouchClick", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGoogleVRPointerInputComponent), &Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseTouchClick_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseTouchClick_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseTouchClick_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_WidgetInteraction_MetaData[] = {
		{ "Category", "Text" },
		{ "Comment", "/** WidgetInteractionComponent used to integrate pointer input with UMG widgets. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "WidgetInteractionComponent used to integrate pointer input with UMG widgets." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_WidgetInteraction = { "WidgetInteraction", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, WidgetInteraction), Z_Construct_UClass_UGoogleVRWidgetInteractionComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_WidgetInteraction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_WidgetInteraction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerEnterActorEvent_MetaData[] = {
		{ "Category", "Event" },
		{ "Comment", "/** Event that occurs when the pointer enters an actor. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Event that occurs when the pointer enters an actor." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerEnterActorEvent = { "OnPointerEnterActorEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, OnPointerEnterActorEvent), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerEnterActorEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerEnterActorEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerEnterComponentEvent_MetaData[] = {
		{ "Category", "Event" },
		{ "Comment", "/** Event that occurs when the pointer enters a component. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Event that occurs when the pointer enters a component." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerEnterComponentEvent = { "OnPointerEnterComponentEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, OnPointerEnterComponentEvent), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerEnterComponentEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerEnterComponentEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerExitActorEvent_MetaData[] = {
		{ "Category", "Event" },
		{ "Comment", "/** Event that occurs when the pointer exits an actor. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Event that occurs when the pointer exits an actor." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerExitActorEvent = { "OnPointerExitActorEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, OnPointerExitActorEvent), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitActorDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerExitActorEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerExitActorEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerExitComponentEvent_MetaData[] = {
		{ "Category", "Event" },
		{ "Comment", "/** Event that occurs when the pointer exits a component. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Event that occurs when the pointer exits a component." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerExitComponentEvent = { "OnPointerExitComponentEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, OnPointerExitComponentEvent), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputExitComponentDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerExitComponentEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerExitComponentEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerHoverActorEvent_MetaData[] = {
		{ "Category", "Event" },
		{ "Comment", "/** Event that occurs once when the pointer is hovering over an actor. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Event that occurs once when the pointer is hovering over an actor." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerHoverActorEvent = { "OnPointerHoverActorEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, OnPointerHoverActorEvent), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerHoverActorEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerHoverActorEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerHoverComponentEvent_MetaData[] = {
		{ "Category", "Event" },
		{ "Comment", "/** Event that occurs once when the pointer is hovering over a component. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Event that occurs once when the pointer is hovering over a component." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerHoverComponentEvent = { "OnPointerHoverComponentEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, OnPointerHoverComponentEvent), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerHoverComponentEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerHoverComponentEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerClickActorEvent_MetaData[] = {
		{ "Category", "Event" },
		{ "Comment", "/** Event that occurs once when the pointer is clicked.\n\x09 *  A click is when the pointer is pressed and then released while pointing at the same actor.\n\x09 */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Event that occurs once when the pointer is clicked.\nA click is when the pointer is pressed and then released while pointing at the same actor." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerClickActorEvent = { "OnPointerClickActorEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, OnPointerClickActorEvent), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerClickActorEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerClickActorEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerClickComponentEvent_MetaData[] = {
		{ "Category", "Event" },
		{ "Comment", "/** Event that occurs once when the pointer is clicked.\n\x09 *  A click is when the pointer is pressed and then released while pointing at the same component.\n\x09 */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Event that occurs once when the pointer is clicked.\nA click is when the pointer is pressed and then released while pointing at the same component." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerClickComponentEvent = { "OnPointerClickComponentEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, OnPointerClickComponentEvent), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerClickComponentEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerClickComponentEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerPressedEvent_MetaData[] = {
		{ "Category", "Event" },
		{ "Comment", "/** Event that occurs once when the pointer initiates a click. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Event that occurs once when the pointer initiates a click." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerPressedEvent = { "OnPointerPressedEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, OnPointerPressedEvent), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerPressedEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerPressedEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerReleasedEvent_MetaData[] = {
		{ "Category", "Event" },
		{ "Comment", "/** Event that occurs once when the pointer ends a click. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointerInputComponent.h" },
		{ "ToolTip", "Event that occurs once when the pointer ends a click." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerReleasedEvent = { "OnPointerReleasedEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRPointerInputComponent, OnPointerReleasedEvent), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRInputDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerReleasedEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerReleasedEvent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_FarClippingDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_NearClippingDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseControllerClick,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_UseTouchClick,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_WidgetInteraction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerEnterActorEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerEnterComponentEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerExitActorEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerExitComponentEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerHoverActorEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerHoverComponentEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerClickActorEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerClickComponentEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerPressedEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::NewProp_OnPointerReleasedEvent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleVRPointerInputComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::ClassParams = {
		&UGoogleVRPointerInputComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRPointerInputComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRPointerInputComponent, 2399585113);
	template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<UGoogleVRPointerInputComponent>()
	{
		return UGoogleVRPointerInputComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRPointerInputComponent(Z_Construct_UClass_UGoogleVRPointerInputComponent, &UGoogleVRPointerInputComponent::StaticClass, TEXT("/Script/GoogleVRController"), TEXT("UGoogleVRPointerInputComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRPointerInputComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
