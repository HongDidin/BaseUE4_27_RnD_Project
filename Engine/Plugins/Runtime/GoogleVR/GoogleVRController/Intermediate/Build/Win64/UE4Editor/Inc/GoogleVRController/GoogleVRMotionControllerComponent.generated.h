// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMaterialInstanceDynamic;
class UStaticMeshComponent;
class UMotionControllerComponent;
#ifdef GOOGLEVRCONTROLLER_GoogleVRMotionControllerComponent_generated_h
#error "GoogleVRMotionControllerComponent.generated.h already included, missing '#pragma once' in GoogleVRMotionControllerComponent.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRMotionControllerComponent_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrentPointerDistance); \
	DECLARE_FUNCTION(execSetPointerDistance); \
	DECLARE_FUNCTION(execGetLaserMaterial); \
	DECLARE_FUNCTION(execGetControllerMesh); \
	DECLARE_FUNCTION(execGetMotionController);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrentPointerDistance); \
	DECLARE_FUNCTION(execSetPointerDistance); \
	DECLARE_FUNCTION(execGetLaserMaterial); \
	DECLARE_FUNCTION(execGetControllerMesh); \
	DECLARE_FUNCTION(execGetMotionController);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRMotionControllerComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRMotionControllerComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRMotionControllerComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRMotionControllerComponent) \
	virtual UObject* _getUObject() const override { return const_cast<UGoogleVRMotionControllerComponent*>(this); }


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRMotionControllerComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRMotionControllerComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRMotionControllerComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRMotionControllerComponent) \
	virtual UObject* _getUObject() const override { return const_cast<UGoogleVRMotionControllerComponent*>(this); }


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRMotionControllerComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRMotionControllerComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRMotionControllerComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRMotionControllerComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRMotionControllerComponent(UGoogleVRMotionControllerComponent&&); \
	NO_API UGoogleVRMotionControllerComponent(const UGoogleVRMotionControllerComponent&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRMotionControllerComponent(UGoogleVRMotionControllerComponent&&); \
	NO_API UGoogleVRMotionControllerComponent(const UGoogleVRMotionControllerComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRMotionControllerComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRMotionControllerComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGoogleVRMotionControllerComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_38_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h_41_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRMotionControllerComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRMotionControllerComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
