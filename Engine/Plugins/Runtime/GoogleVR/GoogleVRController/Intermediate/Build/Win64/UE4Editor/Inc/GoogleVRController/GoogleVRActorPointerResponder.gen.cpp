// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRController/Classes/GoogleVRActorPointerResponder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRActorPointerResponder() {}
// Cross Module References
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRActorPointerResponder_NoRegister();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRActorPointerResponder();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_GoogleVRController();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(IGoogleVRActorPointerResponder::execOnPointerReleased)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerReleased_Implementation(Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRActorPointerResponder::execOnPointerPressed)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerPressed_Implementation(Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRActorPointerResponder::execOnPointerClick)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerClick_Implementation(Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRActorPointerResponder::execOnPointerComponentChanged)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_PreviousComponent);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerComponentChanged_Implementation(Z_Param_PreviousComponent,Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRActorPointerResponder::execOnPointerHover)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerHover_Implementation(Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRActorPointerResponder::execOnPointerExit)
	{
		P_GET_OBJECT(AActor,Z_Param_PreviousActor);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerExit_Implementation(Z_Param_PreviousActor,Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRActorPointerResponder::execOnPointerEnter)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerEnter_Implementation(Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	void IGoogleVRActorPointerResponder::OnPointerClick(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerClick instead.");
	}
	void IGoogleVRActorPointerResponder::OnPointerComponentChanged(UPrimitiveComponent* PreviousComponent, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerComponentChanged instead.");
	}
	void IGoogleVRActorPointerResponder::OnPointerEnter(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerEnter instead.");
	}
	void IGoogleVRActorPointerResponder::OnPointerExit(AActor* PreviousActor, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerExit instead.");
	}
	void IGoogleVRActorPointerResponder::OnPointerHover(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerHover instead.");
	}
	void IGoogleVRActorPointerResponder::OnPointerPressed(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerPressed instead.");
	}
	void IGoogleVRActorPointerResponder::OnPointerReleased(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerReleased instead.");
	}
	void UGoogleVRActorPointerResponder::StaticRegisterNativesUGoogleVRActorPointerResponder()
	{
		UClass* Class = UGoogleVRActorPointerResponder::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnPointerClick", &IGoogleVRActorPointerResponder::execOnPointerClick },
			{ "OnPointerComponentChanged", &IGoogleVRActorPointerResponder::execOnPointerComponentChanged },
			{ "OnPointerEnter", &IGoogleVRActorPointerResponder::execOnPointerEnter },
			{ "OnPointerExit", &IGoogleVRActorPointerResponder::execOnPointerExit },
			{ "OnPointerHover", &IGoogleVRActorPointerResponder::execOnPointerHover },
			{ "OnPointerPressed", &IGoogleVRActorPointerResponder::execOnPointerPressed },
			{ "OnPointerReleased", &IGoogleVRActorPointerResponder::execOnPointerReleased },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerClick_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerClick_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRActorPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRActorPointerResponder, nullptr, "OnPointerClick", nullptr, nullptr, sizeof(GoogleVRActorPointerResponder_eventOnPointerClick_Parms), Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviousComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_PreviousComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_PreviousComponent = { "PreviousComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerComponentChanged_Parms, PreviousComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_PreviousComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_PreviousComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerComponentChanged_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerComponentChanged_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_PreviousComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRActorPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRActorPointerResponder, nullptr, "OnPointerComponentChanged", nullptr, nullptr, sizeof(GoogleVRActorPointerResponder_eventOnPointerComponentChanged_Parms), Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerEnter_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerEnter_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRActorPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRActorPointerResponder, nullptr, "OnPointerEnter", nullptr, nullptr, sizeof(GoogleVRActorPointerResponder_eventOnPointerEnter_Parms), Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviousActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_PreviousActor = { "PreviousActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerExit_Parms, PreviousActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerExit_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerExit_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_PreviousActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRActorPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRActorPointerResponder, nullptr, "OnPointerExit", nullptr, nullptr, sizeof(GoogleVRActorPointerResponder_eventOnPointerExit_Parms), Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerHover_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerHover_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRActorPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRActorPointerResponder, nullptr, "OnPointerHover", nullptr, nullptr, sizeof(GoogleVRActorPointerResponder_eventOnPointerHover_Parms), Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerPressed_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerPressed_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRActorPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRActorPointerResponder, nullptr, "OnPointerPressed", nullptr, nullptr, sizeof(GoogleVRActorPointerResponder_eventOnPointerPressed_Parms), Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerReleased_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRActorPointerResponder_eventOnPointerReleased_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRActorPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRActorPointerResponder, nullptr, "OnPointerReleased", nullptr, nullptr, sizeof(GoogleVRActorPointerResponder_eventOnPointerReleased_Parms), Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGoogleVRActorPointerResponder_NoRegister()
	{
		return UGoogleVRActorPointerResponder::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRActorPointerResponder_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRActorPointerResponder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRController,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGoogleVRActorPointerResponder_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerClick, "OnPointerClick" }, // 3537297275
		{ &Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerComponentChanged, "OnPointerComponentChanged" }, // 2769211116
		{ &Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerEnter, "OnPointerEnter" }, // 3607110775
		{ &Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerExit, "OnPointerExit" }, // 2068476214
		{ &Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerHover, "OnPointerHover" }, // 3750804746
		{ &Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerPressed, "OnPointerPressed" }, // 2091664125
		{ &Z_Construct_UFunction_UGoogleVRActorPointerResponder_OnPointerReleased, "OnPointerReleased" }, // 2366293749
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRActorPointerResponder_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Classes/GoogleVRActorPointerResponder.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRActorPointerResponder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IGoogleVRActorPointerResponder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRActorPointerResponder_Statics::ClassParams = {
		&UGoogleVRActorPointerResponder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRActorPointerResponder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRActorPointerResponder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRActorPointerResponder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRActorPointerResponder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRActorPointerResponder, 3713291006);
	template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<UGoogleVRActorPointerResponder>()
	{
		return UGoogleVRActorPointerResponder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRActorPointerResponder(Z_Construct_UClass_UGoogleVRActorPointerResponder, &UGoogleVRActorPointerResponder::StaticClass, TEXT("/Script/GoogleVRController"), TEXT("UGoogleVRActorPointerResponder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRActorPointerResponder);
	static FName NAME_UGoogleVRActorPointerResponder_OnPointerClick = FName(TEXT("OnPointerClick"));
	void IGoogleVRActorPointerResponder::Execute_OnPointerClick(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRActorPointerResponder::StaticClass()));
		GoogleVRActorPointerResponder_eventOnPointerClick_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRActorPointerResponder_OnPointerClick);
		if (Func)
		{
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRActorPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRActorPointerResponder::StaticClass())))
		{
			I->OnPointerClick_Implementation(HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRActorPointerResponder_OnPointerComponentChanged = FName(TEXT("OnPointerComponentChanged"));
	void IGoogleVRActorPointerResponder::Execute_OnPointerComponentChanged(UObject* O, UPrimitiveComponent* PreviousComponent, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRActorPointerResponder::StaticClass()));
		GoogleVRActorPointerResponder_eventOnPointerComponentChanged_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRActorPointerResponder_OnPointerComponentChanged);
		if (Func)
		{
			Parms.PreviousComponent=PreviousComponent;
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRActorPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRActorPointerResponder::StaticClass())))
		{
			I->OnPointerComponentChanged_Implementation(PreviousComponent,HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRActorPointerResponder_OnPointerEnter = FName(TEXT("OnPointerEnter"));
	void IGoogleVRActorPointerResponder::Execute_OnPointerEnter(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRActorPointerResponder::StaticClass()));
		GoogleVRActorPointerResponder_eventOnPointerEnter_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRActorPointerResponder_OnPointerEnter);
		if (Func)
		{
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRActorPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRActorPointerResponder::StaticClass())))
		{
			I->OnPointerEnter_Implementation(HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRActorPointerResponder_OnPointerExit = FName(TEXT("OnPointerExit"));
	void IGoogleVRActorPointerResponder::Execute_OnPointerExit(UObject* O, AActor* PreviousActor, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRActorPointerResponder::StaticClass()));
		GoogleVRActorPointerResponder_eventOnPointerExit_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRActorPointerResponder_OnPointerExit);
		if (Func)
		{
			Parms.PreviousActor=PreviousActor;
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRActorPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRActorPointerResponder::StaticClass())))
		{
			I->OnPointerExit_Implementation(PreviousActor,HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRActorPointerResponder_OnPointerHover = FName(TEXT("OnPointerHover"));
	void IGoogleVRActorPointerResponder::Execute_OnPointerHover(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRActorPointerResponder::StaticClass()));
		GoogleVRActorPointerResponder_eventOnPointerHover_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRActorPointerResponder_OnPointerHover);
		if (Func)
		{
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRActorPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRActorPointerResponder::StaticClass())))
		{
			I->OnPointerHover_Implementation(HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRActorPointerResponder_OnPointerPressed = FName(TEXT("OnPointerPressed"));
	void IGoogleVRActorPointerResponder::Execute_OnPointerPressed(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRActorPointerResponder::StaticClass()));
		GoogleVRActorPointerResponder_eventOnPointerPressed_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRActorPointerResponder_OnPointerPressed);
		if (Func)
		{
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRActorPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRActorPointerResponder::StaticClass())))
		{
			I->OnPointerPressed_Implementation(HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRActorPointerResponder_OnPointerReleased = FName(TEXT("OnPointerReleased"));
	void IGoogleVRActorPointerResponder::Execute_OnPointerReleased(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRActorPointerResponder::StaticClass()));
		GoogleVRActorPointerResponder_eventOnPointerReleased_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRActorPointerResponder_OnPointerReleased);
		if (Func)
		{
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRActorPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRActorPointerResponder::StaticClass())))
		{
			I->OnPointerReleased_Implementation(HitResult,Source);
		}
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
