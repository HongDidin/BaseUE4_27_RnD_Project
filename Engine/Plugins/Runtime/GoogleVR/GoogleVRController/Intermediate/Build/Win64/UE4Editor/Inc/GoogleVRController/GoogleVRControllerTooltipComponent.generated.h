// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEVRCONTROLLER_GoogleVRControllerTooltipComponent_generated_h
#error "GoogleVRControllerTooltipComponent.generated.h already included, missing '#pragma once' in GoogleVRControllerTooltipComponent.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRControllerTooltipComponent_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_RPC_WRAPPERS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_EVENT_PARMS \
	struct GoogleVRControllerTooltipComponent_eventReceiveOnSideChanged_Parms \
	{ \
		bool IsLocationOnLeft; \
	};


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_CALLBACK_WRAPPERS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRControllerTooltipComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRControllerTooltipComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRControllerTooltipComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRControllerTooltipComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRControllerTooltipComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRControllerTooltipComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRControllerTooltipComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRControllerTooltipComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRControllerTooltipComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRControllerTooltipComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRControllerTooltipComponent(UGoogleVRControllerTooltipComponent&&); \
	NO_API UGoogleVRControllerTooltipComponent(const UGoogleVRControllerTooltipComponent&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRControllerTooltipComponent(UGoogleVRControllerTooltipComponent&&); \
	NO_API UGoogleVRControllerTooltipComponent(const UGoogleVRControllerTooltipComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRControllerTooltipComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRControllerTooltipComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRControllerTooltipComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_22_PROLOG \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_EVENT_PARMS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRControllerTooltipComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerTooltipComponent_h


#define FOREACH_ENUM_EGOOGLEVRCONTROLLERTOOLTIPLOCATION(op) \
	op(EGoogleVRControllerTooltipLocation::TouchPadOutside) \
	op(EGoogleVRControllerTooltipLocation::TouchPadInside) \
	op(EGoogleVRControllerTooltipLocation::AppButtonOutside) \
	op(EGoogleVRControllerTooltipLocation::AppButtonInside) \
	op(EGoogleVRControllerTooltipLocation::None) 

enum class EGoogleVRControllerTooltipLocation : uint8;
template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRControllerTooltipLocation>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
