// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRController/Classes/GoogleVRLaserVisual.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRLaserVisual() {}
// Cross Module References
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRLaserVisual_NoRegister();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRLaserVisual();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_GoogleVRController();
// End Cross Module References
	void UGoogleVRLaserVisual::StaticRegisterNativesUGoogleVRLaserVisual()
	{
	}
	UClass* Z_Construct_UClass_UGoogleVRLaserVisual_NoRegister()
	{
		return UGoogleVRLaserVisual::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRLaserVisual_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRLaserVisual_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRLaserVisual_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* IGoogleVRLaserVisual is an interface for visual representation used with UGoogleVRLaserVisualComponent.\n*/" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "GoogleVRLaserVisual.h" },
		{ "ModuleRelativePath", "Classes/GoogleVRLaserVisual.h" },
		{ "ToolTip", "IGoogleVRLaserVisual is an interface for visual representation used with UGoogleVRLaserVisualComponent." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRLaserVisual_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleVRLaserVisual>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRLaserVisual_Statics::ClassParams = {
		&UGoogleVRLaserVisual::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A5u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRLaserVisual_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRLaserVisual_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRLaserVisual()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRLaserVisual_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRLaserVisual, 2114524035);
	template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<UGoogleVRLaserVisual>()
	{
		return UGoogleVRLaserVisual::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRLaserVisual(Z_Construct_UClass_UGoogleVRLaserVisual, &UGoogleVRLaserVisual::StaticClass, TEXT("/Script/GoogleVRController"), TEXT("UGoogleVRLaserVisual"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRLaserVisual);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
