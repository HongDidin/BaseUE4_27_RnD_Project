// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRController/Classes/GoogleVRControllerTooltipComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRControllerTooltipComponent() {}
// Cross Module References
	GOOGLEVRCONTROLLER_API UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerTooltipLocation();
	UPackage* Z_Construct_UPackage__Script_GoogleVRController();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRControllerTooltipComponent_NoRegister();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRControllerTooltipComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	ENGINE_API UClass* Z_Construct_UClass_UTextRenderComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialParameterCollection_NoRegister();
// End Cross Module References
	static UEnum* EGoogleVRControllerTooltipLocation_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerTooltipLocation, Z_Construct_UPackage__Script_GoogleVRController(), TEXT("EGoogleVRControllerTooltipLocation"));
		}
		return Singleton;
	}
	template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRControllerTooltipLocation>()
	{
		return EGoogleVRControllerTooltipLocation_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGoogleVRControllerTooltipLocation(EGoogleVRControllerTooltipLocation_StaticEnum, TEXT("/Script/GoogleVRController"), TEXT("EGoogleVRControllerTooltipLocation"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerTooltipLocation_Hash() { return 1863894617U; }
	UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerTooltipLocation()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleVRController();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGoogleVRControllerTooltipLocation"), 0, Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerTooltipLocation_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGoogleVRControllerTooltipLocation::TouchPadOutside", (int64)EGoogleVRControllerTooltipLocation::TouchPadOutside },
				{ "EGoogleVRControllerTooltipLocation::TouchPadInside", (int64)EGoogleVRControllerTooltipLocation::TouchPadInside },
				{ "EGoogleVRControllerTooltipLocation::AppButtonOutside", (int64)EGoogleVRControllerTooltipLocation::AppButtonOutside },
				{ "EGoogleVRControllerTooltipLocation::AppButtonInside", (int64)EGoogleVRControllerTooltipLocation::AppButtonInside },
				{ "EGoogleVRControllerTooltipLocation::None", (int64)EGoogleVRControllerTooltipLocation::None },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AppButtonInside.Name", "EGoogleVRControllerTooltipLocation::AppButtonInside" },
				{ "AppButtonOutside.Name", "EGoogleVRControllerTooltipLocation::AppButtonOutside" },
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Classes/GoogleVRControllerTooltipComponent.h" },
				{ "None.Name", "EGoogleVRControllerTooltipLocation::None" },
				{ "TouchPadInside.Name", "EGoogleVRControllerTooltipLocation::TouchPadInside" },
				{ "TouchPadOutside.Name", "EGoogleVRControllerTooltipLocation::TouchPadOutside" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController,
				nullptr,
				"EGoogleVRControllerTooltipLocation",
				"EGoogleVRControllerTooltipLocation",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static FName NAME_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged = FName(TEXT("ReceiveOnSideChanged"));
	void UGoogleVRControllerTooltipComponent::ReceiveOnSideChanged(bool IsLocationOnLeft)
	{
		GoogleVRControllerTooltipComponent_eventReceiveOnSideChanged_Parms Parms;
		Parms.IsLocationOnLeft=IsLocationOnLeft ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged),&Parms);
	}
	void UGoogleVRControllerTooltipComponent::StaticRegisterNativesUGoogleVRControllerTooltipComponent()
	{
	}
	struct Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics
	{
		static void NewProp_IsLocationOnLeft_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsLocationOnLeft;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::NewProp_IsLocationOnLeft_SetBit(void* Obj)
	{
		((GoogleVRControllerTooltipComponent_eventReceiveOnSideChanged_Parms*)Obj)->IsLocationOnLeft = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::NewProp_IsLocationOnLeft = { "IsLocationOnLeft", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRControllerTooltipComponent_eventReceiveOnSideChanged_Parms), &Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::NewProp_IsLocationOnLeft_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::NewProp_IsLocationOnLeft,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Blueprint implementable event for when the tooltip changes sides.\n\x09 *  This happens when the handedness of the user changes.\n\x09 *  Override to update the visual based on which side it is on.\n\x09 */" },
		{ "DisplayName", "On Side Changed" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerTooltipComponent.h" },
		{ "ToolTip", "Blueprint implementable event for when the tooltip changes sides.\nThis happens when the handedness of the user changes.\nOverride to update the visual based on which side it is on." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerTooltipComponent, nullptr, "ReceiveOnSideChanged", nullptr, nullptr, sizeof(GoogleVRControllerTooltipComponent_eventReceiveOnSideChanged_Parms), Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGoogleVRControllerTooltipComponent_NoRegister()
	{
		return UGoogleVRControllerTooltipComponent::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TooltipLocation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TooltipLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TooltipLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextRenderComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TextRenderComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParameterCollection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParameterCollection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRController,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGoogleVRControllerTooltipComponent_ReceiveOnSideChanged, "ReceiveOnSideChanged" }, // 1976991055
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "GoogleVRController" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "GoogleVRControllerTooltipComponent.h" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerTooltipComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TooltipLocation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TooltipLocation_MetaData[] = {
		{ "Category", "Location" },
		{ "Comment", "/** Determines the location of this tooltip. */" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerTooltipComponent.h" },
		{ "ToolTip", "Determines the location of this tooltip." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TooltipLocation = { "TooltipLocation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRControllerTooltipComponent, TooltipLocation), Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerTooltipLocation, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TooltipLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TooltipLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TextRenderComponent_MetaData[] = {
		{ "Category", "Text" },
		{ "Comment", "/** Text to display for the tooltip. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerTooltipComponent.h" },
		{ "ToolTip", "Text to display for the tooltip." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TextRenderComponent = { "TextRenderComponent", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRControllerTooltipComponent, TextRenderComponent), Z_Construct_UClass_UTextRenderComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TextRenderComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TextRenderComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_ParameterCollection_MetaData[] = {
		{ "Category", "Materials" },
		{ "Comment", "/** Parameter collection used to set the alpha of the tooltip.\n\x09 *  Must include property named \"GoogleVRControllerTooltipAlpha\".\n\x09 */" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerTooltipComponent.h" },
		{ "ToolTip", "Parameter collection used to set the alpha of the tooltip.\nMust include property named \"GoogleVRControllerTooltipAlpha\"." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_ParameterCollection = { "ParameterCollection", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRControllerTooltipComponent, ParameterCollection), Z_Construct_UClass_UMaterialParameterCollection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_ParameterCollection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_ParameterCollection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TooltipLocation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TooltipLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_TextRenderComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::NewProp_ParameterCollection,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleVRControllerTooltipComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::ClassParams = {
		&UGoogleVRControllerTooltipComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRControllerTooltipComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRControllerTooltipComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRControllerTooltipComponent, 3207538678);
	template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<UGoogleVRControllerTooltipComponent>()
	{
		return UGoogleVRControllerTooltipComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRControllerTooltipComponent(Z_Construct_UClass_UGoogleVRControllerTooltipComponent, &UGoogleVRControllerTooltipComponent::StaticClass, TEXT("/Script/GoogleVRController"), TEXT("UGoogleVRControllerTooltipComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRControllerTooltipComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
