// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FHitResult;
class UGoogleVRPointerInputComponent;
class UPrimitiveComponent;
#ifdef GOOGLEVRCONTROLLER_GoogleVRComponentPointerResponder_generated_h
#error "GoogleVRComponentPointerResponder.generated.h already included, missing '#pragma once' in GoogleVRComponentPointerResponder.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRComponentPointerResponder_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_RPC_WRAPPERS \
	virtual void OnPointerReleased_Implementation(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
	virtual void OnPointerPressed_Implementation(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
	virtual void OnPointerClick_Implementation(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
	virtual void OnPointerHover_Implementation(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
	virtual void OnPointerExit_Implementation(UPrimitiveComponent* PreviousComponent, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
	virtual void OnPointerEnter_Implementation(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
 \
	DECLARE_FUNCTION(execOnPointerReleased); \
	DECLARE_FUNCTION(execOnPointerPressed); \
	DECLARE_FUNCTION(execOnPointerClick); \
	DECLARE_FUNCTION(execOnPointerHover); \
	DECLARE_FUNCTION(execOnPointerExit); \
	DECLARE_FUNCTION(execOnPointerEnter);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnPointerReleased_Implementation(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
	virtual void OnPointerPressed_Implementation(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
	virtual void OnPointerClick_Implementation(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
	virtual void OnPointerHover_Implementation(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
	virtual void OnPointerExit_Implementation(UPrimitiveComponent* PreviousComponent, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
	virtual void OnPointerEnter_Implementation(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source) {}; \
 \
	DECLARE_FUNCTION(execOnPointerReleased); \
	DECLARE_FUNCTION(execOnPointerPressed); \
	DECLARE_FUNCTION(execOnPointerClick); \
	DECLARE_FUNCTION(execOnPointerHover); \
	DECLARE_FUNCTION(execOnPointerExit); \
	DECLARE_FUNCTION(execOnPointerEnter);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_EVENT_PARMS \
	struct GoogleVRComponentPointerResponder_eventOnPointerClick_Parms \
	{ \
		FHitResult HitResult; \
		UGoogleVRPointerInputComponent* Source; \
	}; \
	struct GoogleVRComponentPointerResponder_eventOnPointerEnter_Parms \
	{ \
		FHitResult HitResult; \
		UGoogleVRPointerInputComponent* Source; \
	}; \
	struct GoogleVRComponentPointerResponder_eventOnPointerExit_Parms \
	{ \
		UPrimitiveComponent* PreviousComponent; \
		FHitResult HitResult; \
		UGoogleVRPointerInputComponent* Source; \
	}; \
	struct GoogleVRComponentPointerResponder_eventOnPointerHover_Parms \
	{ \
		FHitResult HitResult; \
		UGoogleVRPointerInputComponent* Source; \
	}; \
	struct GoogleVRComponentPointerResponder_eventOnPointerPressed_Parms \
	{ \
		FHitResult HitResult; \
		UGoogleVRPointerInputComponent* Source; \
	}; \
	struct GoogleVRComponentPointerResponder_eventOnPointerReleased_Parms \
	{ \
		FHitResult HitResult; \
		UGoogleVRPointerInputComponent* Source; \
	};


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_CALLBACK_WRAPPERS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRComponentPointerResponder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRComponentPointerResponder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRComponentPointerResponder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRComponentPointerResponder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRComponentPointerResponder(UGoogleVRComponentPointerResponder&&); \
	NO_API UGoogleVRComponentPointerResponder(const UGoogleVRComponentPointerResponder&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRComponentPointerResponder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRComponentPointerResponder(UGoogleVRComponentPointerResponder&&); \
	NO_API UGoogleVRComponentPointerResponder(const UGoogleVRComponentPointerResponder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRComponentPointerResponder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRComponentPointerResponder); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRComponentPointerResponder)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUGoogleVRComponentPointerResponder(); \
	friend struct Z_Construct_UClass_UGoogleVRComponentPointerResponder_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRComponentPointerResponder, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRComponentPointerResponder)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IGoogleVRComponentPointerResponder() {} \
public: \
	typedef UGoogleVRComponentPointerResponder UClassType; \
	typedef IGoogleVRComponentPointerResponder ThisClass; \
	static void Execute_OnPointerClick(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	static void Execute_OnPointerEnter(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	static void Execute_OnPointerExit(UObject* O, UPrimitiveComponent* PreviousComponent, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	static void Execute_OnPointerHover(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	static void Execute_OnPointerPressed(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	static void Execute_OnPointerReleased(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_INCLASS_IINTERFACE \
protected: \
	virtual ~IGoogleVRComponentPointerResponder() {} \
public: \
	typedef UGoogleVRComponentPointerResponder UClassType; \
	typedef IGoogleVRComponentPointerResponder ThisClass; \
	static void Execute_OnPointerClick(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	static void Execute_OnPointerEnter(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	static void Execute_OnPointerExit(UObject* O, UPrimitiveComponent* PreviousComponent, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	static void Execute_OnPointerHover(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	static void Execute_OnPointerPressed(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	static void Execute_OnPointerReleased(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_18_PROLOG \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_EVENT_PARMS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h_21_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRComponentPointerResponder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRComponentPointerResponder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
