// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRController/Classes/GoogleVRComponentPointerResponder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRComponentPointerResponder() {}
// Cross Module References
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRComponentPointerResponder_NoRegister();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRComponentPointerResponder();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_GoogleVRController();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(IGoogleVRComponentPointerResponder::execOnPointerReleased)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerReleased_Implementation(Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRComponentPointerResponder::execOnPointerPressed)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerPressed_Implementation(Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRComponentPointerResponder::execOnPointerClick)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerClick_Implementation(Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRComponentPointerResponder::execOnPointerHover)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerHover_Implementation(Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRComponentPointerResponder::execOnPointerExit)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_PreviousComponent);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerExit_Implementation(Z_Param_PreviousComponent,Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IGoogleVRComponentPointerResponder::execOnPointerEnter)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_OBJECT(UGoogleVRPointerInputComponent,Z_Param_Source);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPointerEnter_Implementation(Z_Param_Out_HitResult,Z_Param_Source);
		P_NATIVE_END;
	}
	void IGoogleVRComponentPointerResponder::OnPointerClick(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerClick instead.");
	}
	void IGoogleVRComponentPointerResponder::OnPointerEnter(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerEnter instead.");
	}
	void IGoogleVRComponentPointerResponder::OnPointerExit(UPrimitiveComponent* PreviousComponent, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerExit instead.");
	}
	void IGoogleVRComponentPointerResponder::OnPointerHover(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerHover instead.");
	}
	void IGoogleVRComponentPointerResponder::OnPointerPressed(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerPressed instead.");
	}
	void IGoogleVRComponentPointerResponder::OnPointerReleased(FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnPointerReleased instead.");
	}
	void UGoogleVRComponentPointerResponder::StaticRegisterNativesUGoogleVRComponentPointerResponder()
	{
		UClass* Class = UGoogleVRComponentPointerResponder::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnPointerClick", &IGoogleVRComponentPointerResponder::execOnPointerClick },
			{ "OnPointerEnter", &IGoogleVRComponentPointerResponder::execOnPointerEnter },
			{ "OnPointerExit", &IGoogleVRComponentPointerResponder::execOnPointerExit },
			{ "OnPointerHover", &IGoogleVRComponentPointerResponder::execOnPointerHover },
			{ "OnPointerPressed", &IGoogleVRComponentPointerResponder::execOnPointerPressed },
			{ "OnPointerReleased", &IGoogleVRComponentPointerResponder::execOnPointerReleased },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerClick_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerClick_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRComponentPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRComponentPointerResponder, nullptr, "OnPointerClick", nullptr, nullptr, sizeof(GoogleVRComponentPointerResponder_eventOnPointerClick_Parms), Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerEnter_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerEnter_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRComponentPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRComponentPointerResponder, nullptr, "OnPointerEnter", nullptr, nullptr, sizeof(GoogleVRComponentPointerResponder_eventOnPointerEnter_Parms), Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviousComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_PreviousComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_PreviousComponent = { "PreviousComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerExit_Parms, PreviousComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_PreviousComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_PreviousComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerExit_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerExit_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_PreviousComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRComponentPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRComponentPointerResponder, nullptr, "OnPointerExit", nullptr, nullptr, sizeof(GoogleVRComponentPointerResponder_eventOnPointerExit_Parms), Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerHover_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerHover_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRComponentPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRComponentPointerResponder, nullptr, "OnPointerHover", nullptr, nullptr, sizeof(GoogleVRComponentPointerResponder_eventOnPointerHover_Parms), Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerPressed_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerPressed_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRComponentPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRComponentPointerResponder, nullptr, "OnPointerPressed", nullptr, nullptr, sizeof(GoogleVRComponentPointerResponder_eventOnPointerPressed_Parms), Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Source_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Source;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerReleased_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::NewProp_HitResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::NewProp_Source_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::NewProp_Source = { "Source", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRComponentPointerResponder_eventOnPointerReleased_Parms, Source), Z_Construct_UClass_UGoogleVRPointerInputComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::NewProp_Source_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::NewProp_Source_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::NewProp_HitResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::NewProp_Source,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::Function_MetaDataParams[] = {
		{ "Category", "PointerResponder" },
		{ "ModuleRelativePath", "Classes/GoogleVRComponentPointerResponder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRComponentPointerResponder, nullptr, "OnPointerReleased", nullptr, nullptr, sizeof(GoogleVRComponentPointerResponder_eventOnPointerReleased_Parms), Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGoogleVRComponentPointerResponder_NoRegister()
	{
		return UGoogleVRComponentPointerResponder::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRComponentPointerResponder_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRComponentPointerResponder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRController,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGoogleVRComponentPointerResponder_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerClick, "OnPointerClick" }, // 28097175
		{ &Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerEnter, "OnPointerEnter" }, // 824348304
		{ &Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerExit, "OnPointerExit" }, // 1580992145
		{ &Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerHover, "OnPointerHover" }, // 171258393
		{ &Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerPressed, "OnPointerPressed" }, // 1113100605
		{ &Z_Construct_UFunction_UGoogleVRComponentPointerResponder_OnPointerReleased, "OnPointerReleased" }, // 238636308
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRComponentPointerResponder_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Classes/GoogleVRComponentPointerResponder.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRComponentPointerResponder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IGoogleVRComponentPointerResponder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRComponentPointerResponder_Statics::ClassParams = {
		&UGoogleVRComponentPointerResponder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRComponentPointerResponder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRComponentPointerResponder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRComponentPointerResponder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRComponentPointerResponder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRComponentPointerResponder, 2079551129);
	template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<UGoogleVRComponentPointerResponder>()
	{
		return UGoogleVRComponentPointerResponder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRComponentPointerResponder(Z_Construct_UClass_UGoogleVRComponentPointerResponder, &UGoogleVRComponentPointerResponder::StaticClass, TEXT("/Script/GoogleVRController"), TEXT("UGoogleVRComponentPointerResponder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRComponentPointerResponder);
	static FName NAME_UGoogleVRComponentPointerResponder_OnPointerClick = FName(TEXT("OnPointerClick"));
	void IGoogleVRComponentPointerResponder::Execute_OnPointerClick(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRComponentPointerResponder::StaticClass()));
		GoogleVRComponentPointerResponder_eventOnPointerClick_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRComponentPointerResponder_OnPointerClick);
		if (Func)
		{
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRComponentPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRComponentPointerResponder::StaticClass())))
		{
			I->OnPointerClick_Implementation(HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRComponentPointerResponder_OnPointerEnter = FName(TEXT("OnPointerEnter"));
	void IGoogleVRComponentPointerResponder::Execute_OnPointerEnter(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRComponentPointerResponder::StaticClass()));
		GoogleVRComponentPointerResponder_eventOnPointerEnter_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRComponentPointerResponder_OnPointerEnter);
		if (Func)
		{
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRComponentPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRComponentPointerResponder::StaticClass())))
		{
			I->OnPointerEnter_Implementation(HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRComponentPointerResponder_OnPointerExit = FName(TEXT("OnPointerExit"));
	void IGoogleVRComponentPointerResponder::Execute_OnPointerExit(UObject* O, UPrimitiveComponent* PreviousComponent, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRComponentPointerResponder::StaticClass()));
		GoogleVRComponentPointerResponder_eventOnPointerExit_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRComponentPointerResponder_OnPointerExit);
		if (Func)
		{
			Parms.PreviousComponent=PreviousComponent;
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRComponentPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRComponentPointerResponder::StaticClass())))
		{
			I->OnPointerExit_Implementation(PreviousComponent,HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRComponentPointerResponder_OnPointerHover = FName(TEXT("OnPointerHover"));
	void IGoogleVRComponentPointerResponder::Execute_OnPointerHover(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRComponentPointerResponder::StaticClass()));
		GoogleVRComponentPointerResponder_eventOnPointerHover_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRComponentPointerResponder_OnPointerHover);
		if (Func)
		{
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRComponentPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRComponentPointerResponder::StaticClass())))
		{
			I->OnPointerHover_Implementation(HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRComponentPointerResponder_OnPointerPressed = FName(TEXT("OnPointerPressed"));
	void IGoogleVRComponentPointerResponder::Execute_OnPointerPressed(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRComponentPointerResponder::StaticClass()));
		GoogleVRComponentPointerResponder_eventOnPointerPressed_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRComponentPointerResponder_OnPointerPressed);
		if (Func)
		{
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRComponentPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRComponentPointerResponder::StaticClass())))
		{
			I->OnPointerPressed_Implementation(HitResult,Source);
		}
	}
	static FName NAME_UGoogleVRComponentPointerResponder_OnPointerReleased = FName(TEXT("OnPointerReleased"));
	void IGoogleVRComponentPointerResponder::Execute_OnPointerReleased(UObject* O, FHitResult const& HitResult, UGoogleVRPointerInputComponent* Source)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UGoogleVRComponentPointerResponder::StaticClass()));
		GoogleVRComponentPointerResponder_eventOnPointerReleased_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UGoogleVRComponentPointerResponder_OnPointerReleased);
		if (Func)
		{
			Parms.HitResult=HitResult;
			Parms.Source=Source;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IGoogleVRComponentPointerResponder*)(O->GetNativeInterfaceAddress(UGoogleVRComponentPointerResponder::StaticClass())))
		{
			I->OnPointerReleased_Implementation(HitResult,Source);
		}
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
