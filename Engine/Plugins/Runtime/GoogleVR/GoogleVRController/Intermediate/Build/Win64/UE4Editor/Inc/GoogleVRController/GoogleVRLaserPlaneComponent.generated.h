// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEVRCONTROLLER_GoogleVRLaserPlaneComponent_generated_h
#error "GoogleVRLaserPlaneComponent.generated.h already included, missing '#pragma once' in GoogleVRLaserPlaneComponent.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRLaserPlaneComponent_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_RPC_WRAPPERS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRLaserPlaneComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRLaserPlaneComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRLaserPlaneComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRLaserPlaneComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRLaserPlaneComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRLaserPlaneComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRLaserPlaneComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRLaserPlaneComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRLaserPlaneComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRLaserPlaneComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRLaserPlaneComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRLaserPlaneComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRLaserPlaneComponent(UGoogleVRLaserPlaneComponent&&); \
	NO_API UGoogleVRLaserPlaneComponent(const UGoogleVRLaserPlaneComponent&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRLaserPlaneComponent(UGoogleVRLaserPlaneComponent&&); \
	NO_API UGoogleVRLaserPlaneComponent(const UGoogleVRLaserPlaneComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRLaserPlaneComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRLaserPlaneComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGoogleVRLaserPlaneComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_20_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRLaserPlaneComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Private_GoogleVRLaserPlaneComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
