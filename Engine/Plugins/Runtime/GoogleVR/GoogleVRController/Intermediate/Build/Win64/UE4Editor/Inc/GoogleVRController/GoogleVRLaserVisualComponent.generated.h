// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
class UMaterialInstanceDynamic;
class UGoogleVRLaserPlaneComponent;
class UMaterialBillboardComponent;
#ifdef GOOGLEVRCONTROLLER_GoogleVRLaserVisualComponent_generated_h
#error "GoogleVRLaserVisualComponent.generated.h already included, missing '#pragma once' in GoogleVRLaserVisualComponent.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRLaserVisualComponent_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetPointerDistance); \
	DECLARE_FUNCTION(execGetLaserMaterial); \
	DECLARE_FUNCTION(execGetLaser); \
	DECLARE_FUNCTION(execGetReticle);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetPointerDistance); \
	DECLARE_FUNCTION(execGetLaserMaterial); \
	DECLARE_FUNCTION(execGetLaser); \
	DECLARE_FUNCTION(execGetReticle);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRLaserVisualComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRLaserVisualComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRLaserVisualComponent, UGoogleVRLaserVisual, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRLaserVisualComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRLaserVisualComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRLaserVisualComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRLaserVisualComponent, UGoogleVRLaserVisual, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRLaserVisualComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRLaserVisualComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRLaserVisualComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRLaserVisualComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRLaserVisualComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRLaserVisualComponent(UGoogleVRLaserVisualComponent&&); \
	NO_API UGoogleVRLaserVisualComponent(const UGoogleVRLaserVisualComponent&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRLaserVisualComponent(UGoogleVRLaserVisualComponent&&); \
	NO_API UGoogleVRLaserVisualComponent(const UGoogleVRLaserVisualComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRLaserVisualComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRLaserVisualComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGoogleVRLaserVisualComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_25_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRLaserVisualComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRLaserVisualComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
