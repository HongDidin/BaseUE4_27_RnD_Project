// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EGoogleVRControllerState : uint8;
#ifdef GOOGLEVRCONTROLLER_GoogleVRControllerEventManager_generated_h
#error "GoogleVRControllerEventManager.generated.h already included, missing '#pragma once' in GoogleVRControllerEventManager.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRControllerEventManager_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_21_DELEGATE \
struct _Script_GoogleVRController_eventGoogleVRControllerStateChangeDelegate_Parms \
{ \
	EGoogleVRControllerState NewControllerState; \
}; \
static inline void FGoogleVRControllerStateChangeDelegate_DelegateWrapper(const FMulticastScriptDelegate& GoogleVRControllerStateChangeDelegate, EGoogleVRControllerState NewControllerState) \
{ \
	_Script_GoogleVRController_eventGoogleVRControllerStateChangeDelegate_Parms Parms; \
	Parms.NewControllerState=NewControllerState; \
	GoogleVRControllerStateChangeDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_20_DELEGATE \
static inline void FGoogleVRControllerRecenterDelegate_DelegateWrapper(const FMulticastScriptDelegate& GoogleVRControllerRecenterDelegate) \
{ \
	GoogleVRControllerRecenterDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_RPC_WRAPPERS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRControllerEventManager(); \
	friend struct Z_Construct_UClass_UGoogleVRControllerEventManager_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRControllerEventManager, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRControllerEventManager)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRControllerEventManager(); \
	friend struct Z_Construct_UClass_UGoogleVRControllerEventManager_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRControllerEventManager, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRControllerEventManager)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRControllerEventManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRControllerEventManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRControllerEventManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRControllerEventManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRControllerEventManager(UGoogleVRControllerEventManager&&); \
	NO_API UGoogleVRControllerEventManager(const UGoogleVRControllerEventManager&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRControllerEventManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRControllerEventManager(UGoogleVRControllerEventManager&&); \
	NO_API UGoogleVRControllerEventManager(const UGoogleVRControllerEventManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRControllerEventManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRControllerEventManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRControllerEventManager)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_26_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h_29_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GoogleVRControllerEventManager."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRControllerEventManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRControllerEventManager_h


#define FOREACH_ENUM_EGOOGLEVRCONTROLLERSTATE(op) \
	op(EGoogleVRControllerState::Disconnected) \
	op(EGoogleVRControllerState::Scanning) \
	op(EGoogleVRControllerState::Connecting) \
	op(EGoogleVRControllerState::Connected) 

enum class EGoogleVRControllerState : uint8;
template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRControllerState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
