// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEVRCONTROLLER_GoogleVRGazeReticleComponent_generated_h
#error "GoogleVRGazeReticleComponent.generated.h already included, missing '#pragma once' in GoogleVRGazeReticleComponent.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRGazeReticleComponent_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_RPC_WRAPPERS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRGazeReticleComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRGazeReticleComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRGazeReticleComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRGazeReticleComponent) \
	virtual UObject* _getUObject() const override { return const_cast<UGoogleVRGazeReticleComponent*>(this); }


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRGazeReticleComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRGazeReticleComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRGazeReticleComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRGazeReticleComponent) \
	virtual UObject* _getUObject() const override { return const_cast<UGoogleVRGazeReticleComponent*>(this); }


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRGazeReticleComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRGazeReticleComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRGazeReticleComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRGazeReticleComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRGazeReticleComponent(UGoogleVRGazeReticleComponent&&); \
	NO_API UGoogleVRGazeReticleComponent(const UGoogleVRGazeReticleComponent&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRGazeReticleComponent(UGoogleVRGazeReticleComponent&&); \
	NO_API UGoogleVRGazeReticleComponent(const UGoogleVRGazeReticleComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRGazeReticleComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRGazeReticleComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGoogleVRGazeReticleComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_22_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRGazeReticleComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRGazeReticleComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
