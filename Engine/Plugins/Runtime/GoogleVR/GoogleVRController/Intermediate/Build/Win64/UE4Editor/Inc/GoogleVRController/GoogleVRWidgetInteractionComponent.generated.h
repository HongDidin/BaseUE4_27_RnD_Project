// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEVRCONTROLLER_GoogleVRWidgetInteractionComponent_generated_h
#error "GoogleVRWidgetInteractionComponent.generated.h already included, missing '#pragma once' in GoogleVRWidgetInteractionComponent.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRWidgetInteractionComponent_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_RPC_WRAPPERS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRWidgetInteractionComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRWidgetInteractionComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRWidgetInteractionComponent, UWidgetInteractionComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRWidgetInteractionComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRWidgetInteractionComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRWidgetInteractionComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRWidgetInteractionComponent, UWidgetInteractionComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRWidgetInteractionComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRWidgetInteractionComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRWidgetInteractionComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRWidgetInteractionComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRWidgetInteractionComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRWidgetInteractionComponent(UGoogleVRWidgetInteractionComponent&&); \
	NO_API UGoogleVRWidgetInteractionComponent(const UGoogleVRWidgetInteractionComponent&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRWidgetInteractionComponent(UGoogleVRWidgetInteractionComponent&&); \
	NO_API UGoogleVRWidgetInteractionComponent(const UGoogleVRWidgetInteractionComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRWidgetInteractionComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRWidgetInteractionComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRWidgetInteractionComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_8_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRWidgetInteractionComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRWidgetInteractionComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
