// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
struct FHitResult;
class AActor;
struct FVector;
class IGoogleVRPointer;
#ifdef GOOGLEVRCONTROLLER_GoogleVRPointerInputComponent_generated_h
#error "GoogleVRPointerInputComponent.generated.h already included, missing '#pragma once' in GoogleVRPointerInputComponent.h"
#endif
#define GOOGLEVRCONTROLLER_GoogleVRPointerInputComponent_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_14_DELEGATE \
struct _Script_GoogleVRController_eventGoogleVRInputExitComponentDelegate_Parms \
{ \
	UPrimitiveComponent* PreviousComponent; \
	FHitResult HitResult; \
}; \
static inline void FGoogleVRInputExitComponentDelegate_DelegateWrapper(const FMulticastScriptDelegate& GoogleVRInputExitComponentDelegate, UPrimitiveComponent* PreviousComponent, FHitResult HitResult) \
{ \
	_Script_GoogleVRController_eventGoogleVRInputExitComponentDelegate_Parms Parms; \
	Parms.PreviousComponent=PreviousComponent; \
	Parms.HitResult=HitResult; \
	GoogleVRInputExitComponentDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_13_DELEGATE \
struct _Script_GoogleVRController_eventGoogleVRInputExitActorDelegate_Parms \
{ \
	AActor* PreviousActor; \
	FHitResult HitResult; \
}; \
static inline void FGoogleVRInputExitActorDelegate_DelegateWrapper(const FMulticastScriptDelegate& GoogleVRInputExitActorDelegate, AActor* PreviousActor, FHitResult HitResult) \
{ \
	_Script_GoogleVRController_eventGoogleVRInputExitActorDelegate_Parms Parms; \
	Parms.PreviousActor=PreviousActor; \
	Parms.HitResult=HitResult; \
	GoogleVRInputExitActorDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_12_DELEGATE \
struct _Script_GoogleVRController_eventGoogleVRInputDelegate_Parms \
{ \
	FHitResult HitResult; \
}; \
static inline void FGoogleVRInputDelegate_DelegateWrapper(const FMulticastScriptDelegate& GoogleVRInputDelegate, FHitResult HitResult) \
{ \
	_Script_GoogleVRController_eventGoogleVRInputDelegate_Parms Parms; \
	Parms.HitResult=HitResult; \
	GoogleVRInputDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetLatestHitResult); \
	DECLARE_FUNCTION(execGetIntersectionLocation); \
	DECLARE_FUNCTION(execGetHitComponent); \
	DECLARE_FUNCTION(execGetHitActor); \
	DECLARE_FUNCTION(execIsBlockingHit); \
	DECLARE_FUNCTION(execGetPointer); \
	DECLARE_FUNCTION(execSetPointer);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetLatestHitResult); \
	DECLARE_FUNCTION(execGetIntersectionLocation); \
	DECLARE_FUNCTION(execGetHitComponent); \
	DECLARE_FUNCTION(execGetHitActor); \
	DECLARE_FUNCTION(execIsBlockingHit); \
	DECLARE_FUNCTION(execGetPointer); \
	DECLARE_FUNCTION(execSetPointer);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRPointerInputComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRPointerInputComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRPointerInputComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRPointerInputComponent(); \
	friend struct Z_Construct_UClass_UGoogleVRPointerInputComponent_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRPointerInputComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleVRController"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRPointerInputComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRPointerInputComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRPointerInputComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRPointerInputComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRPointerInputComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRPointerInputComponent(UGoogleVRPointerInputComponent&&); \
	NO_API UGoogleVRPointerInputComponent(const UGoogleVRPointerInputComponent&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRPointerInputComponent(UGoogleVRPointerInputComponent&&); \
	NO_API UGoogleVRPointerInputComponent(const UGoogleVRPointerInputComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRPointerInputComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRPointerInputComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRPointerInputComponent)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_23_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<class UGoogleVRPointerInputComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRController_Source_GoogleVRController_Classes_GoogleVRPointerInputComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
