// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRController/Classes/GoogleVRControllerFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRControllerFunctionLibrary() {}
// Cross Module References
	GOOGLEVRCONTROLLER_API UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerBatteryLevel();
	UPackage* Z_Construct_UPackage__Script_GoogleVRController();
	GOOGLEVRCONTROLLER_API UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRArmModelFollowGazeBehavior();
	GOOGLEVRCONTROLLER_API UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerHandedness();
	GOOGLEVRCONTROLLER_API UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerAPIStatus();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_NoRegister();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRControllerFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	INPUTCORE_API UEnum* Z_Construct_UEnum_InputCore_EControllerHand();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRControllerEventManager_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	GOOGLEVRCONTROLLER_API UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerState();
// End Cross Module References
	static UEnum* EGoogleVRControllerBatteryLevel_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerBatteryLevel, Z_Construct_UPackage__Script_GoogleVRController(), TEXT("EGoogleVRControllerBatteryLevel"));
		}
		return Singleton;
	}
	template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRControllerBatteryLevel>()
	{
		return EGoogleVRControllerBatteryLevel_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGoogleVRControllerBatteryLevel(EGoogleVRControllerBatteryLevel_StaticEnum, TEXT("/Script/GoogleVRController"), TEXT("EGoogleVRControllerBatteryLevel"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerBatteryLevel_Hash() { return 761933401U; }
	UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerBatteryLevel()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleVRController();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGoogleVRControllerBatteryLevel"), 0, Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerBatteryLevel_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGoogleVRControllerBatteryLevel::Unknown", (int64)EGoogleVRControllerBatteryLevel::Unknown },
				{ "EGoogleVRControllerBatteryLevel::CriticalLow", (int64)EGoogleVRControllerBatteryLevel::CriticalLow },
				{ "EGoogleVRControllerBatteryLevel::Low", (int64)EGoogleVRControllerBatteryLevel::Low },
				{ "EGoogleVRControllerBatteryLevel::Medium", (int64)EGoogleVRControllerBatteryLevel::Medium },
				{ "EGoogleVRControllerBatteryLevel::AlmostFull", (int64)EGoogleVRControllerBatteryLevel::AlmostFull },
				{ "EGoogleVRControllerBatteryLevel::Full", (int64)EGoogleVRControllerBatteryLevel::Full },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AlmostFull.Name", "EGoogleVRControllerBatteryLevel::AlmostFull" },
				{ "BlueprintType", "true" },
				{ "Comment", "// Represents the controller battery level.\n" },
				{ "CriticalLow.Name", "EGoogleVRControllerBatteryLevel::CriticalLow" },
				{ "Full.Name", "EGoogleVRControllerBatteryLevel::Full" },
				{ "Low.Name", "EGoogleVRControllerBatteryLevel::Low" },
				{ "Medium.Name", "EGoogleVRControllerBatteryLevel::Medium" },
				{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
				{ "ToolTip", "Represents the controller battery level." },
				{ "Unknown.Name", "EGoogleVRControllerBatteryLevel::Unknown" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController,
				nullptr,
				"EGoogleVRControllerBatteryLevel",
				"EGoogleVRControllerBatteryLevel",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EGoogleVRArmModelFollowGazeBehavior_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GoogleVRController_EGoogleVRArmModelFollowGazeBehavior, Z_Construct_UPackage__Script_GoogleVRController(), TEXT("EGoogleVRArmModelFollowGazeBehavior"));
		}
		return Singleton;
	}
	template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRArmModelFollowGazeBehavior>()
	{
		return EGoogleVRArmModelFollowGazeBehavior_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGoogleVRArmModelFollowGazeBehavior(EGoogleVRArmModelFollowGazeBehavior_StaticEnum, TEXT("/Script/GoogleVRController"), TEXT("EGoogleVRArmModelFollowGazeBehavior"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRArmModelFollowGazeBehavior_Hash() { return 221010608U; }
	UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRArmModelFollowGazeBehavior()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleVRController();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGoogleVRArmModelFollowGazeBehavior"), 0, Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRArmModelFollowGazeBehavior_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGoogleVRArmModelFollowGazeBehavior::Never", (int64)EGoogleVRArmModelFollowGazeBehavior::Never },
				{ "EGoogleVRArmModelFollowGazeBehavior::DuringMotion", (int64)EGoogleVRArmModelFollowGazeBehavior::DuringMotion },
				{ "EGoogleVRArmModelFollowGazeBehavior::Always", (int64)EGoogleVRArmModelFollowGazeBehavior::Always },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Always.Comment", "// The shoulder will follow the gaze during controller motion.\n" },
				{ "Always.Name", "EGoogleVRArmModelFollowGazeBehavior::Always" },
				{ "Always.ToolTip", "The shoulder will follow the gaze during controller motion." },
				{ "BlueprintType", "true" },
				{ "Comment", "// Represents when gaze-following behavior should occur in the ArmModel.\n// This is useful if you have an application that requires the user to turn around.\n" },
				{ "DuringMotion.Comment", "// The shoulder will never follow the gaze.\n" },
				{ "DuringMotion.Name", "EGoogleVRArmModelFollowGazeBehavior::DuringMotion" },
				{ "DuringMotion.ToolTip", "The shoulder will never follow the gaze." },
				{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
				{ "Never.Name", "EGoogleVRArmModelFollowGazeBehavior::Never" },
				{ "ToolTip", "Represents when gaze-following behavior should occur in the ArmModel.\nThis is useful if you have an application that requires the user to turn around." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController,
				nullptr,
				"EGoogleVRArmModelFollowGazeBehavior",
				"EGoogleVRArmModelFollowGazeBehavior",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EGoogleVRControllerHandedness_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerHandedness, Z_Construct_UPackage__Script_GoogleVRController(), TEXT("EGoogleVRControllerHandedness"));
		}
		return Singleton;
	}
	template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRControllerHandedness>()
	{
		return EGoogleVRControllerHandedness_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGoogleVRControllerHandedness(EGoogleVRControllerHandedness_StaticEnum, TEXT("/Script/GoogleVRController"), TEXT("EGoogleVRControllerHandedness"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerHandedness_Hash() { return 1875052537U; }
	UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerHandedness()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleVRController();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGoogleVRControllerHandedness"), 0, Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerHandedness_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGoogleVRControllerHandedness::RightHanded", (int64)EGoogleVRControllerHandedness::RightHanded },
				{ "EGoogleVRControllerHandedness::LeftHanded", (int64)EGoogleVRControllerHandedness::LeftHanded },
				{ "EGoogleVRControllerHandedness::Unknown", (int64)EGoogleVRControllerHandedness::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "LeftHanded.Name", "EGoogleVRControllerHandedness::LeftHanded" },
				{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
				{ "RightHanded.Name", "EGoogleVRControllerHandedness::RightHanded" },
				{ "Unknown.Name", "EGoogleVRControllerHandedness::Unknown" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController,
				nullptr,
				"EGoogleVRControllerHandedness",
				"EGoogleVRControllerHandedness",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EGoogleVRControllerAPIStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerAPIStatus, Z_Construct_UPackage__Script_GoogleVRController(), TEXT("EGoogleVRControllerAPIStatus"));
		}
		return Singleton;
	}
	template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRControllerAPIStatus>()
	{
		return EGoogleVRControllerAPIStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGoogleVRControllerAPIStatus(EGoogleVRControllerAPIStatus_StaticEnum, TEXT("/Script/GoogleVRController"), TEXT("EGoogleVRControllerAPIStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerAPIStatus_Hash() { return 2751538800U; }
	UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerAPIStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleVRController();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGoogleVRControllerAPIStatus"), 0, Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerAPIStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGoogleVRControllerAPIStatus::OK", (int64)EGoogleVRControllerAPIStatus::OK },
				{ "EGoogleVRControllerAPIStatus::Unsupported", (int64)EGoogleVRControllerAPIStatus::Unsupported },
				{ "EGoogleVRControllerAPIStatus::NotAuthorized", (int64)EGoogleVRControllerAPIStatus::NotAuthorized },
				{ "EGoogleVRControllerAPIStatus::Unavailable", (int64)EGoogleVRControllerAPIStatus::Unavailable },
				{ "EGoogleVRControllerAPIStatus::ServiceObsolete", (int64)EGoogleVRControllerAPIStatus::ServiceObsolete },
				{ "EGoogleVRControllerAPIStatus::ClientObsolete", (int64)EGoogleVRControllerAPIStatus::ClientObsolete },
				{ "EGoogleVRControllerAPIStatus::Malfunction", (int64)EGoogleVRControllerAPIStatus::Malfunction },
				{ "EGoogleVRControllerAPIStatus::Unknown", (int64)EGoogleVRControllerAPIStatus::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ClientObsolete.Comment", "// The underlying VR service is too new, is incompatible with current client.\n" },
				{ "ClientObsolete.Name", "EGoogleVRControllerAPIStatus::ClientObsolete" },
				{ "ClientObsolete.ToolTip", "The underlying VR service is too new, is incompatible with current client." },
				{ "Malfunction.Comment", "// The underlying VR service is malfunctioning. Try again later.\n" },
				{ "Malfunction.Name", "EGoogleVRControllerAPIStatus::Malfunction" },
				{ "Malfunction.ToolTip", "The underlying VR service is malfunctioning. Try again later." },
				{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
				{ "NotAuthorized.Comment", "// This app was not authorized to use the service (e.g., missing permissions,\n// the app is blacklisted by the underlying service, etc).\n" },
				{ "NotAuthorized.Name", "EGoogleVRControllerAPIStatus::NotAuthorized" },
				{ "NotAuthorized.ToolTip", "This app was not authorized to use the service (e.g., missing permissions,\nthe app is blacklisted by the underlying service, etc)." },
				{ "OK.Comment", "// API is happy and healthy. This doesn't mean the controller itself\n// is connected, it just means that the underlying service is working\n// properly.\n" },
				{ "OK.Name", "EGoogleVRControllerAPIStatus::OK" },
				{ "OK.ToolTip", "API is happy and healthy. This doesn't mean the controller itself\nis connected, it just means that the underlying service is working\nproperly." },
				{ "ServiceObsolete.Comment", "// The underlying VR service is too old, needs upgrade.\n" },
				{ "ServiceObsolete.Name", "EGoogleVRControllerAPIStatus::ServiceObsolete" },
				{ "ServiceObsolete.ToolTip", "The underlying VR service is too old, needs upgrade." },
				{ "Unavailable.Comment", "// The underlying VR service is not present.\n" },
				{ "Unavailable.Name", "EGoogleVRControllerAPIStatus::Unavailable" },
				{ "Unavailable.ToolTip", "The underlying VR service is not present." },
				{ "Unknown.Comment", "// This means GoogleVRController plugin is not support on the platform.\n" },
				{ "Unknown.Name", "EGoogleVRControllerAPIStatus::Unknown" },
				{ "Unknown.ToolTip", "This means GoogleVRController plugin is not support on the platform." },
				{ "Unsupported.Comment", "// API failed because this device does not support controllers (API is too\n// low, or other required feature not present).\n" },
				{ "Unsupported.Name", "EGoogleVRControllerAPIStatus::Unsupported" },
				{ "Unsupported.ToolTip", "API failed because this device does not support controllers (API is too\nlow, or other required feature not present)." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController,
				nullptr,
				"EGoogleVRControllerAPIStatus",
				"EGoogleVRControllerAPIStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetBatteryLevel)
	{
		P_GET_ENUM(EControllerHand,Z_Param_Hand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EGoogleVRControllerBatteryLevel*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetBatteryLevel(EControllerHand(Z_Param_Hand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetBatteryCharging)
	{
		P_GET_ENUM(EControllerHand,Z_Param_Hand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetBatteryCharging(EControllerHand(Z_Param_Hand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetTooltipAlphaValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetTooltipAlphaValue();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetControllerAlphaValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetControllerAlphaValue();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execSetTooltipMaxAngleFromCamera)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_AngleFromCamera);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRControllerFunctionLibrary::SetTooltipMaxAngleFromCamera(Z_Param_AngleFromCamera);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetTooltipMaxAngleFromCamera)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetTooltipMaxAngleFromCamera();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execSetTooltipMinDistanceFromFace)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DistanceFromFace);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRControllerFunctionLibrary::SetTooltipMinDistanceFromFace(Z_Param_DistanceFromFace);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetTooltipMinDistanceFromFace)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetTooltipMinDistanceFromFace();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execSetFadeDistanceFromFace)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DistanceFromFace);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRControllerFunctionLibrary::SetFadeDistanceFromFace(Z_Param_DistanceFromFace);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetFadeDistanceFromFace)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetFadeDistanceFromFace();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetArmModelIsLockedToHead)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetArmModelIsLockedToHead();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execSetArmModelIsLockedToHead)
	{
		P_GET_UBOOL(Z_Param_IsLockedToHead);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRControllerFunctionLibrary::SetArmModelIsLockedToHead(Z_Param_IsLockedToHead);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execSetWillArmModelUseAccelerometer)
	{
		P_GET_UBOOL(Z_Param_UseAccelerometer);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRControllerFunctionLibrary::SetWillArmModelUseAccelerometer(Z_Param_UseAccelerometer);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execWillArmModelUseAccelerometer)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::WillArmModelUseAccelerometer();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execSetArmModelPointerTiltAngle)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_TiltAngle);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRControllerFunctionLibrary::SetArmModelPointerTiltAngle(Z_Param_TiltAngle);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetArmModelPointerTiltAngle)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetArmModelPointerTiltAngle();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execSetArmModelAddedElbowDepth)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_ElbowDepth);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRControllerFunctionLibrary::SetArmModelAddedElbowDepth(Z_Param_ElbowDepth);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetArmModelAddedElbowDepth)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetArmModelAddedElbowDepth();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execSetArmModelAddedElbowHeight)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_ElbowHeight);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRControllerFunctionLibrary::SetArmModelAddedElbowHeight(Z_Param_ElbowHeight);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetArmModelAddedElbowHeight)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetArmModelAddedElbowHeight();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetArmModelPointerPositionOffset)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetArmModelPointerPositionOffset();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execSetArmModelEnabled)
	{
		P_GET_UBOOL(Z_Param_bArmModelEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRControllerFunctionLibrary::SetArmModelEnabled(Z_Param_bArmModelEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execIsArmModelEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::IsArmModelEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerEventManager)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UGoogleVRControllerEventManager**)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetGoogleVRControllerEventManager();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerOrientation)
	{
		P_GET_ENUM(EControllerHand,Z_Param_Hand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRotator*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetGoogleVRControllerOrientation(EControllerHand(Z_Param_Hand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerRawGyro)
	{
		P_GET_ENUM(EControllerHand,Z_Param_Hand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetGoogleVRControllerRawGyro(EControllerHand(Z_Param_Hand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerRawAccel)
	{
		P_GET_ENUM(EControllerHand,Z_Param_Hand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetGoogleVRControllerRawAccel(EControllerHand(Z_Param_Hand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerHandedness)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EGoogleVRControllerHandedness*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetGoogleVRControllerHandedness();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerState)
	{
		P_GET_ENUM(EControllerHand,Z_Param_Hand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EGoogleVRControllerState*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetGoogleVRControllerState(EControllerHand(Z_Param_Hand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerAPIStatus)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EGoogleVRControllerAPIStatus*)Z_Param__Result=UGoogleVRControllerFunctionLibrary::GetGoogleVRControllerAPIStatus();
		P_NATIVE_END;
	}
	void UGoogleVRControllerFunctionLibrary::StaticRegisterNativesUGoogleVRControllerFunctionLibrary()
	{
		UClass* Class = UGoogleVRControllerFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetArmModelAddedElbowDepth", &UGoogleVRControllerFunctionLibrary::execGetArmModelAddedElbowDepth },
			{ "GetArmModelAddedElbowHeight", &UGoogleVRControllerFunctionLibrary::execGetArmModelAddedElbowHeight },
			{ "GetArmModelIsLockedToHead", &UGoogleVRControllerFunctionLibrary::execGetArmModelIsLockedToHead },
			{ "GetArmModelPointerPositionOffset", &UGoogleVRControllerFunctionLibrary::execGetArmModelPointerPositionOffset },
			{ "GetArmModelPointerTiltAngle", &UGoogleVRControllerFunctionLibrary::execGetArmModelPointerTiltAngle },
			{ "GetBatteryCharging", &UGoogleVRControllerFunctionLibrary::execGetBatteryCharging },
			{ "GetBatteryLevel", &UGoogleVRControllerFunctionLibrary::execGetBatteryLevel },
			{ "GetControllerAlphaValue", &UGoogleVRControllerFunctionLibrary::execGetControllerAlphaValue },
			{ "GetFadeDistanceFromFace", &UGoogleVRControllerFunctionLibrary::execGetFadeDistanceFromFace },
			{ "GetGoogleVRControllerAPIStatus", &UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerAPIStatus },
			{ "GetGoogleVRControllerEventManager", &UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerEventManager },
			{ "GetGoogleVRControllerHandedness", &UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerHandedness },
			{ "GetGoogleVRControllerOrientation", &UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerOrientation },
			{ "GetGoogleVRControllerRawAccel", &UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerRawAccel },
			{ "GetGoogleVRControllerRawGyro", &UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerRawGyro },
			{ "GetGoogleVRControllerState", &UGoogleVRControllerFunctionLibrary::execGetGoogleVRControllerState },
			{ "GetTooltipAlphaValue", &UGoogleVRControllerFunctionLibrary::execGetTooltipAlphaValue },
			{ "GetTooltipMaxAngleFromCamera", &UGoogleVRControllerFunctionLibrary::execGetTooltipMaxAngleFromCamera },
			{ "GetTooltipMinDistanceFromFace", &UGoogleVRControllerFunctionLibrary::execGetTooltipMinDistanceFromFace },
			{ "IsArmModelEnabled", &UGoogleVRControllerFunctionLibrary::execIsArmModelEnabled },
			{ "SetArmModelAddedElbowDepth", &UGoogleVRControllerFunctionLibrary::execSetArmModelAddedElbowDepth },
			{ "SetArmModelAddedElbowHeight", &UGoogleVRControllerFunctionLibrary::execSetArmModelAddedElbowHeight },
			{ "SetArmModelEnabled", &UGoogleVRControllerFunctionLibrary::execSetArmModelEnabled },
			{ "SetArmModelIsLockedToHead", &UGoogleVRControllerFunctionLibrary::execSetArmModelIsLockedToHead },
			{ "SetArmModelPointerTiltAngle", &UGoogleVRControllerFunctionLibrary::execSetArmModelPointerTiltAngle },
			{ "SetFadeDistanceFromFace", &UGoogleVRControllerFunctionLibrary::execSetFadeDistanceFromFace },
			{ "SetTooltipMaxAngleFromCamera", &UGoogleVRControllerFunctionLibrary::execSetTooltipMaxAngleFromCamera },
			{ "SetTooltipMinDistanceFromFace", &UGoogleVRControllerFunctionLibrary::execSetTooltipMinDistanceFromFace },
			{ "SetWillArmModelUseAccelerometer", &UGoogleVRControllerFunctionLibrary::execSetWillArmModelUseAccelerometer },
			{ "WillArmModelUseAccelerometer", &UGoogleVRControllerFunctionLibrary::execWillArmModelUseAccelerometer },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetArmModelAddedElbowDepth_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetArmModelAddedElbowDepth_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Get the elbow depth used by the arm model in meters.\n\x09 *  Used in the mathematical model for calculating the controller position/rotation.\n\x09 *  @return user height.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get the elbow depth used by the arm model in meters.\nUsed in the mathematical model for calculating the controller position/rotation.\n@return user height." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetArmModelAddedElbowDepth", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetArmModelAddedElbowDepth_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetArmModelAddedElbowHeight_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetArmModelAddedElbowHeight_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Get the elbow height used by the arm model in meters.\n\x09 *  Used in the mathematical model for calculating the controller position/rotation.\n\x09 *  @return user height.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get the elbow height used by the arm model in meters.\nUsed in the mathematical model for calculating the controller position/rotation.\n@return user height." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetArmModelAddedElbowHeight", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetArmModelAddedElbowHeight_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetArmModelIsLockedToHead_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRControllerFunctionLibrary_eventGetArmModelIsLockedToHead_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRControllerFunctionLibrary_eventGetArmModelIsLockedToHead_Parms), &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Get if the Arm Model will be locked to the head Pose.\n\x09* @return true if it is locked to the Head Pose\n\x09*/" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get if the Arm Model will be locked to the head Pose.\n@return true if it is locked to the Head Pose" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetArmModelIsLockedToHead", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetArmModelIsLockedToHead_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetArmModelPointerPositionOffset_Parms
		{
			FVector ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetArmModelPointerPositionOffset_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Returns the local position of the pointer in the unreal coordinate system relative to the motion controller.\n\x09 *  The pointer is similar to the controller, except that it is slightly forward and rotated down by the\n\x09 *  pointer tilt angle. This is used to create more ergonomic comfort when pointing at things.\n\x09 *  This should be used for any reticle / laser implementation.\n\x09 *  @return pointer position.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Returns the local position of the pointer in the unreal coordinate system relative to the motion controller.\nThe pointer is similar to the controller, except that it is slightly forward and rotated down by the\npointer tilt angle. This is used to create more ergonomic comfort when pointing at things.\nThis should be used for any reticle / laser implementation.\n@return pointer position." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetArmModelPointerPositionOffset", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetArmModelPointerPositionOffset_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetArmModelPointerTiltAngle_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetArmModelPointerTiltAngle_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Get the pointer tilt angle.\n\x09 *  @return degrees downward that the pointer tilts.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get the pointer tilt angle.\n@return degrees downward that the pointer tilts." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetArmModelPointerTiltAngle", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetArmModelPointerTiltAngle_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetBatteryCharging_Parms
		{
			EControllerHand Hand;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetBatteryCharging_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRControllerFunctionLibrary_eventGetBatteryCharging_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRControllerFunctionLibrary_eventGetBatteryCharging_Parms), &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::NewProp_Hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Get whether the controller battery is currently charging.\n\x09 *  This may not be real time information and may be slow to be updated.\n\x09 *  @return true if the battery is charging.\n\x09 */" },
		{ "CPP_Default_Hand", "AnyHand" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get whether the controller battery is currently charging.\nThis may not be real time information and may be slow to be updated.\n@return true if the battery is charging." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetBatteryCharging", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetBatteryCharging_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetBatteryLevel_Parms
		{
			EControllerHand Hand;
			EGoogleVRControllerBatteryLevel ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetBatteryLevel_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetBatteryLevel_Parms, ReturnValue), Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerBatteryLevel, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::NewProp_Hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Get the bucketed controller battery level.\n\x09 *  Note this is an approximate level described by enumeration, not a percent.\n\x09 *  @return the approximate battery level, or unknown if the level can not be determined.\n\x09 */" },
		{ "CPP_Default_Hand", "AnyHand" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get the bucketed controller battery level.\nNote this is an approximate level described by enumeration, not a percent.\n@return the approximate battery level, or unknown if the level can not be determined." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetBatteryLevel", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetBatteryLevel_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetControllerAlphaValue_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetControllerAlphaValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Get the current desired alpha value of the controller visual.\n\x09 *  This changes based on the FadeDistanceFromFace, and is used to prevent the controller\n\x09 *  From clipping awkwardly into the user's face.\n\x09 *  @return value between 0 and 1.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get the current desired alpha value of the controller visual.\nThis changes based on the FadeDistanceFromFace, and is used to prevent the controller\nFrom clipping awkwardly into the user's face.\n@return value between 0 and 1." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetControllerAlphaValue", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetControllerAlphaValue_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetFadeDistanceFromFace_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetFadeDistanceFromFace_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Controller distance from the face after which the alpha value decreases (meters).\n\x09 *  @return fade distance from face in meters.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Controller distance from the face after which the alpha value decreases (meters).\n@return fade distance from face in meters." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetFadeDistanceFromFace", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetFadeDistanceFromFace_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerAPIStatus_Parms
		{
			EGoogleVRControllerAPIStatus ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerAPIStatus_Parms, ReturnValue), Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerAPIStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/**\n\x09 * Get the GoogleVR Controller API status\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get the GoogleVR Controller API status" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetGoogleVRControllerAPIStatus", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerAPIStatus_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerEventManager_Parms
		{
			UGoogleVRControllerEventManager* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerEventManager_Parms, ReturnValue), Z_Construct_UClass_UGoogleVRControllerEventManager_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/**\n\x09 * Return a pointer to the UGoogleVRControllerEventManager to hook up GoogleVR Controller specific event.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Return a pointer to the UGoogleVRControllerEventManager to hook up GoogleVR Controller specific event." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetGoogleVRControllerEventManager", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerEventManager_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerHandedness_Parms
		{
			EGoogleVRControllerHandedness ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerHandedness_Parms, ReturnValue), Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerHandedness, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/**\n\x09 * Get user's handedness preference from GVRSDK\n\x09 * @return A EGoogleVRControllerHandedness indicates the user's handedness preference in GoogleVR Settings.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get user's handedness preference from GVRSDK\n@return A EGoogleVRControllerHandedness indicates the user's handedness preference in GoogleVR Settings." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetGoogleVRControllerHandedness", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerHandedness_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerOrientation_Parms
		{
			EControllerHand Hand;
			FRotator ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerOrientation_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerOrientation_Parms, ReturnValue), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::NewProp_Hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/**\n\x09 * This function return the orientation of the controller in unreal space.\n\x09 */" },
		{ "CPP_Default_Hand", "AnyHand" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "This function return the orientation of the controller in unreal space." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetGoogleVRControllerOrientation", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerOrientation_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerRawAccel_Parms
		{
			EControllerHand Hand;
			FVector ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerRawAccel_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerRawAccel_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::NewProp_Hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/**\n\x09 * This function return the controller acceleration in gvr controller space.\n\x09 */" },
		{ "CPP_Default_Hand", "AnyHand" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "This function return the controller acceleration in gvr controller space." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetGoogleVRControllerRawAccel", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerRawAccel_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerRawGyro_Parms
		{
			EControllerHand Hand;
			FVector ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerRawGyro_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerRawGyro_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::NewProp_Hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/**\n\x09 * This function return the controller angular velocity about each axis (positive means clockwise when sighting along axis) in gvr controller space.\n\x09 */" },
		{ "CPP_Default_Hand", "AnyHand" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "This function return the controller angular velocity about each axis (positive means clockwise when sighting along axis) in gvr controller space." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetGoogleVRControllerRawGyro", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerRawGyro_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerState_Parms
		{
			EControllerHand Hand;
			EGoogleVRControllerState ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerState_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerState_Parms, ReturnValue), Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::NewProp_Hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/**\n\x09 * Get the GoogleVR Controller state\n\x09 */" },
		{ "CPP_Default_Hand", "AnyHand" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get the GoogleVR Controller state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetGoogleVRControllerState", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetGoogleVRControllerState_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetTooltipAlphaValue_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetTooltipAlphaValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Get the current desired alpha value of the tooltip visual.\n\x09 *  When the controller is farther than TooltipMinDistanceFromFace this becomes 0\n\x09 *  When the controller is closer than FadeDistanceFromFace this becomes 0\n\x09 *  This is used so that the tooltips are only visible when the controller is being held up.\n\x09 *  @return value between 0 and 1.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get the current desired alpha value of the tooltip visual.\nWhen the controller is farther than TooltipMinDistanceFromFace this becomes 0\nWhen the controller is closer than FadeDistanceFromFace this becomes 0\nThis is used so that the tooltips are only visible when the controller is being held up.\n@return value between 0 and 1." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetTooltipAlphaValue", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetTooltipAlphaValue_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetTooltipMaxAngleFromCamera_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetTooltipMaxAngleFromCamera_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** When the angle (degrees) between the controller and the head is larger than\n\x09 *  this value, the tooltip disappears.\n\x09 *  If the value is 180, then the tooltips are always shown.\n\x09 *  If the value is 90, the tooltips are only shown when they are facing the camera.\n\x09 *  @return tooltip max angle from camera in degrees.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "When the angle (degrees) between the controller and the head is larger than\nthis value, the tooltip disappears.\nIf the value is 180, then the tooltips are always shown.\nIf the value is 90, the tooltips are only shown when they are facing the camera.\n@return tooltip max angle from camera in degrees." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetTooltipMaxAngleFromCamera", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetTooltipMaxAngleFromCamera_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventGetTooltipMinDistanceFromFace_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventGetTooltipMinDistanceFromFace_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Controller distance from the face after which the tooltips appear (meters).\n\x09 *  @return tooltip mininum distance from face in meters.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Controller distance from the face after which the tooltips appear (meters).\n@return tooltip mininum distance from face in meters." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "GetTooltipMinDistanceFromFace", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventGetTooltipMinDistanceFromFace_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventIsArmModelEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRControllerFunctionLibrary_eventIsArmModelEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRControllerFunctionLibrary_eventIsArmModelEnabled_Parms), &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Determine if the arm model is enabled\n\x09 *  @return true if the arm model is enabled\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Determine if the arm model is enabled\n@return true if the arm model is enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "IsArmModelEnabled", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventIsArmModelEnabled_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventSetArmModelAddedElbowDepth_Parms
		{
			float ElbowDepth;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElbowDepth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics::NewProp_ElbowDepth = { "ElbowDepth", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventSetArmModelAddedElbowDepth_Parms, ElbowDepth), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics::NewProp_ElbowDepth,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Set the elbow depth used by the arm model in meters.\n\x09 *  Used in the mathematical model for calculating the controller position/rotation.\n\x09 *  @param HeightMeters - value to set\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Set the elbow depth used by the arm model in meters.\nUsed in the mathematical model for calculating the controller position/rotation.\n@param HeightMeters - value to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "SetArmModelAddedElbowDepth", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventSetArmModelAddedElbowDepth_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventSetArmModelAddedElbowHeight_Parms
		{
			float ElbowHeight;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElbowHeight;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics::NewProp_ElbowHeight = { "ElbowHeight", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventSetArmModelAddedElbowHeight_Parms, ElbowHeight), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics::NewProp_ElbowHeight,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Set the elbow height used by the arm model in meters.\n\x09 *  Used in the mathematical model for calculating the controller position/rotation.\n\x09 *  @param HeightMeters - value to set\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Set the elbow height used by the arm model in meters.\nUsed in the mathematical model for calculating the controller position/rotation.\n@param HeightMeters - value to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "SetArmModelAddedElbowHeight", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventSetArmModelAddedElbowHeight_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventSetArmModelEnabled_Parms
		{
			bool bArmModelEnabled;
		};
		static void NewProp_bArmModelEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bArmModelEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::NewProp_bArmModelEnabled_SetBit(void* Obj)
	{
		((GoogleVRControllerFunctionLibrary_eventSetArmModelEnabled_Parms*)Obj)->bArmModelEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::NewProp_bArmModelEnabled = { "bArmModelEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRControllerFunctionLibrary_eventSetArmModelEnabled_Parms), &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::NewProp_bArmModelEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::NewProp_bArmModelEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Set the arm model enabled/disabled\n\x09 *  @param bArmModelEnabled - value to set\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Set the arm model enabled/disabled\n@param bArmModelEnabled - value to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "SetArmModelEnabled", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventSetArmModelEnabled_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventSetArmModelIsLockedToHead_Parms
		{
			bool IsLockedToHead;
		};
		static void NewProp_IsLockedToHead_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsLockedToHead;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::NewProp_IsLockedToHead_SetBit(void* Obj)
	{
		((GoogleVRControllerFunctionLibrary_eventSetArmModelIsLockedToHead_Parms*)Obj)->IsLockedToHead = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::NewProp_IsLockedToHead = { "IsLockedToHead", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRControllerFunctionLibrary_eventSetArmModelIsLockedToHead_Parms), &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::NewProp_IsLockedToHead_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::NewProp_IsLockedToHead,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Set if the Arm Model will be locked to the head Pose.\n\x09* @param IsLockedToHead - value to set\n\x09*/" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Set if the Arm Model will be locked to the head Pose.\n@param IsLockedToHead - value to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "SetArmModelIsLockedToHead", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventSetArmModelIsLockedToHead_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventSetArmModelPointerTiltAngle_Parms
		{
			float TiltAngle;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TiltAngle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics::NewProp_TiltAngle = { "TiltAngle", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventSetArmModelPointerTiltAngle_Parms, TiltAngle), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics::NewProp_TiltAngle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Set the pointer tilt angle.\n\x09 *  Defaults to 15 degrees, which is comfortable for most use cases.\n\x09 *  @param TiltAngle - value to set\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Set the pointer tilt angle.\nDefaults to 15 degrees, which is comfortable for most use cases.\n@param TiltAngle - value to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "SetArmModelPointerTiltAngle", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventSetArmModelPointerTiltAngle_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventSetFadeDistanceFromFace_Parms
		{
			float DistanceFromFace;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DistanceFromFace;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics::NewProp_DistanceFromFace = { "DistanceFromFace", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventSetFadeDistanceFromFace_Parms, DistanceFromFace), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics::NewProp_DistanceFromFace,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Controller distance from the face after which the alpha value decreases (meters).\n\x09 *  @param DistanceFromFace - value to set\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Controller distance from the face after which the alpha value decreases (meters).\n@param DistanceFromFace - value to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "SetFadeDistanceFromFace", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventSetFadeDistanceFromFace_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventSetTooltipMaxAngleFromCamera_Parms
		{
			int32 AngleFromCamera;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_AngleFromCamera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics::NewProp_AngleFromCamera = { "AngleFromCamera", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventSetTooltipMaxAngleFromCamera_Parms, AngleFromCamera), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics::NewProp_AngleFromCamera,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** When the angle (degrees) between the controller and the head is larger than\n\x09 *  this value, the tooltip disappears.\n\x09 *  If the value is 180, then the tooltips are always shown.\n\x09 *  If the value is 90, the tooltips are only shown when they are facing the camera.\n\x09 *  @param AngleFromCamera - value to set\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "When the angle (degrees) between the controller and the head is larger than\nthis value, the tooltip disappears.\nIf the value is 180, then the tooltips are always shown.\nIf the value is 90, the tooltips are only shown when they are facing the camera.\n@param AngleFromCamera - value to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "SetTooltipMaxAngleFromCamera", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventSetTooltipMaxAngleFromCamera_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventSetTooltipMinDistanceFromFace_Parms
		{
			float DistanceFromFace;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DistanceFromFace;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics::NewProp_DistanceFromFace = { "DistanceFromFace", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRControllerFunctionLibrary_eventSetTooltipMinDistanceFromFace_Parms, DistanceFromFace), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics::NewProp_DistanceFromFace,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Controller distance from the face after which the tooltips appear (meters).\n\x09 *  @param DistanceFromFace - value to set\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Controller distance from the face after which the tooltips appear (meters).\n@param DistanceFromFace - value to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "SetTooltipMinDistanceFromFace", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventSetTooltipMinDistanceFromFace_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventSetWillArmModelUseAccelerometer_Parms
		{
			bool UseAccelerometer;
		};
		static void NewProp_UseAccelerometer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_UseAccelerometer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::NewProp_UseAccelerometer_SetBit(void* Obj)
	{
		((GoogleVRControllerFunctionLibrary_eventSetWillArmModelUseAccelerometer_Parms*)Obj)->UseAccelerometer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::NewProp_UseAccelerometer = { "UseAccelerometer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRControllerFunctionLibrary_eventSetWillArmModelUseAccelerometer_Parms), &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::NewProp_UseAccelerometer_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::NewProp_UseAccelerometer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Set if the arm model will use accelerometer data\n\x09 *  If this is turned on, then the arm model will estimate the position of the controller in space\n\x09 *  using accelerometer data. This is useful when trying to make the player feel like they are moving\n\x09 *  around a physical object. Not as useful when just interacting with UI.\n\x09 *  @param UseAccelerometer - value to set\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Set if the arm model will use accelerometer data\nIf this is turned on, then the arm model will estimate the position of the controller in space\nusing accelerometer data. This is useful when trying to make the player feel like they are moving\naround a physical object. Not as useful when just interacting with UI.\n@param UseAccelerometer - value to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "SetWillArmModelUseAccelerometer", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventSetWillArmModelUseAccelerometer_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics
	{
		struct GoogleVRControllerFunctionLibrary_eventWillArmModelUseAccelerometer_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRControllerFunctionLibrary_eventWillArmModelUseAccelerometer_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRControllerFunctionLibrary_eventWillArmModelUseAccelerometer_Parms), &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRController" },
		{ "Comment", "/** Get if the arm model will use accelerometer data\n\x09 *  If this is turned on, then the arm model will estimate the position of the controller in space\n\x09 *  using accelerometer data. This is useful when trying to make the player feel like they are moving\n\x09 *  around a physical object. Not as useful when just interacting with UI.\n\x09 *  @return true if accelerometer use is enabled\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "Get if the arm model will use accelerometer data\nIf this is turned on, then the arm model will estimate the position of the controller in space\nusing accelerometer data. This is useful when trying to make the player feel like they are moving\naround a physical object. Not as useful when just interacting with UI.\n@return true if accelerometer use is enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, nullptr, "WillArmModelUseAccelerometer", nullptr, nullptr, sizeof(GoogleVRControllerFunctionLibrary_eventWillArmModelUseAccelerometer_Parms), Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_NoRegister()
	{
		return UGoogleVRControllerFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRController,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowDepth, "GetArmModelAddedElbowDepth" }, // 1438867157
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelAddedElbowHeight, "GetArmModelAddedElbowHeight" }, // 2748800877
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelIsLockedToHead, "GetArmModelIsLockedToHead" }, // 2988437769
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerPositionOffset, "GetArmModelPointerPositionOffset" }, // 2026685088
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetArmModelPointerTiltAngle, "GetArmModelPointerTiltAngle" }, // 1797307342
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryCharging, "GetBatteryCharging" }, // 2274370564
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetBatteryLevel, "GetBatteryLevel" }, // 3305555282
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetControllerAlphaValue, "GetControllerAlphaValue" }, // 938434728
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetFadeDistanceFromFace, "GetFadeDistanceFromFace" }, // 2703723864
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerAPIStatus, "GetGoogleVRControllerAPIStatus" }, // 358225930
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerEventManager, "GetGoogleVRControllerEventManager" }, // 795102808
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerHandedness, "GetGoogleVRControllerHandedness" }, // 1511860497
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerOrientation, "GetGoogleVRControllerOrientation" }, // 2197279563
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawAccel, "GetGoogleVRControllerRawAccel" }, // 3060407497
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerRawGyro, "GetGoogleVRControllerRawGyro" }, // 3625807756
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetGoogleVRControllerState, "GetGoogleVRControllerState" }, // 462740391
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipAlphaValue, "GetTooltipAlphaValue" }, // 1981130876
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMaxAngleFromCamera, "GetTooltipMaxAngleFromCamera" }, // 1062282096
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_GetTooltipMinDistanceFromFace, "GetTooltipMinDistanceFromFace" }, // 1247978671
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_IsArmModelEnabled, "IsArmModelEnabled" }, // 2399357895
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowDepth, "SetArmModelAddedElbowDepth" }, // 975282956
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelAddedElbowHeight, "SetArmModelAddedElbowHeight" }, // 2203547248
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelEnabled, "SetArmModelEnabled" }, // 2695292459
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelIsLockedToHead, "SetArmModelIsLockedToHead" }, // 2985992825
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetArmModelPointerTiltAngle, "SetArmModelPointerTiltAngle" }, // 1715620669
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetFadeDistanceFromFace, "SetFadeDistanceFromFace" }, // 2787906752
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMaxAngleFromCamera, "SetTooltipMaxAngleFromCamera" }, // 259920703
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetTooltipMinDistanceFromFace, "SetTooltipMinDistanceFromFace" }, // 2301136382
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_SetWillArmModelUseAccelerometer, "SetWillArmModelUseAccelerometer" }, // 2643220493
		{ &Z_Construct_UFunction_UGoogleVRControllerFunctionLibrary_WillArmModelUseAccelerometer, "WillArmModelUseAccelerometer" }, // 3107410860
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * GoogleVRController Extensions Function Library\n */" },
		{ "IncludePath", "GoogleVRControllerFunctionLibrary.h" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerFunctionLibrary.h" },
		{ "ToolTip", "GoogleVRController Extensions Function Library" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleVRControllerFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics::ClassParams = {
		&UGoogleVRControllerFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRControllerFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRControllerFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRControllerFunctionLibrary, 306923252);
	template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<UGoogleVRControllerFunctionLibrary>()
	{
		return UGoogleVRControllerFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRControllerFunctionLibrary(Z_Construct_UClass_UGoogleVRControllerFunctionLibrary, &UGoogleVRControllerFunctionLibrary::StaticClass, TEXT("/Script/GoogleVRController"), TEXT("UGoogleVRControllerFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRControllerFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
