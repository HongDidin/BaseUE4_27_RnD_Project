// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRController/Classes/GoogleVRControllerEventManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRControllerEventManager() {}
// Cross Module References
	GOOGLEVRCONTROLLER_API UFunction* Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_GoogleVRController();
	GOOGLEVRCONTROLLER_API UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerState();
	GOOGLEVRCONTROLLER_API UFunction* Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerRecenterDelegate__DelegateSignature();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRControllerEventManager_NoRegister();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRControllerEventManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics
	{
		struct _Script_GoogleVRController_eventGoogleVRControllerStateChangeDelegate_Parms
		{
			EGoogleVRControllerState NewControllerState;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NewControllerState_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NewControllerState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::NewProp_NewControllerState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::NewProp_NewControllerState = { "NewControllerState", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_GoogleVRController_eventGoogleVRControllerStateChangeDelegate_Parms, NewControllerState), Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::NewProp_NewControllerState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::NewProp_NewControllerState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/GoogleVRControllerEventManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController, nullptr, "GoogleVRControllerStateChangeDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_GoogleVRController_eventGoogleVRControllerStateChangeDelegate_Parms), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerRecenterDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerRecenterDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/GoogleVRControllerEventManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerRecenterDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController, nullptr, "GoogleVRControllerRecenterDelegate__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerRecenterDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerRecenterDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerRecenterDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerRecenterDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EGoogleVRControllerState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerState, Z_Construct_UPackage__Script_GoogleVRController(), TEXT("EGoogleVRControllerState"));
		}
		return Singleton;
	}
	template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRControllerState>()
	{
		return EGoogleVRControllerState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGoogleVRControllerState(EGoogleVRControllerState_StaticEnum, TEXT("/Script/GoogleVRController"), TEXT("EGoogleVRControllerState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerState_Hash() { return 2417413265U; }
	UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleVRController();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGoogleVRControllerState"), 0, Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRControllerState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGoogleVRControllerState::Disconnected", (int64)EGoogleVRControllerState::Disconnected },
				{ "EGoogleVRControllerState::Scanning", (int64)EGoogleVRControllerState::Scanning },
				{ "EGoogleVRControllerState::Connecting", (int64)EGoogleVRControllerState::Connecting },
				{ "EGoogleVRControllerState::Connected", (int64)EGoogleVRControllerState::Connected },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Connected.Name", "EGoogleVRControllerState::Connected" },
				{ "Connecting.Name", "EGoogleVRControllerState::Connecting" },
				{ "Disconnected.Name", "EGoogleVRControllerState::Disconnected" },
				{ "ModuleRelativePath", "Classes/GoogleVRControllerEventManager.h" },
				{ "Scanning.Name", "EGoogleVRControllerState::Scanning" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController,
				nullptr,
				"EGoogleVRControllerState",
				"EGoogleVRControllerState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UGoogleVRControllerEventManager::StaticRegisterNativesUGoogleVRControllerEventManager()
	{
	}
	UClass* Z_Construct_UClass_UGoogleVRControllerEventManager_NoRegister()
	{
		return UGoogleVRControllerEventManager::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRControllerEventManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnControllerRecenteredDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnControllerRecenteredDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnControllerStateChangedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnControllerStateChangedDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * GoogleVRController Extensions Function Library\n */" },
		{ "IncludePath", "GoogleVRControllerEventManager.h" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerEventManager.h" },
		{ "ToolTip", "GoogleVRController Extensions Function Library" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::NewProp_OnControllerRecenteredDelegate_MetaData[] = {
		{ "Comment", "/** DEPRECATED:  Please use VRNotificationsComponent's VRControllerRecentered delegate instead! */" },
		{ "ModuleRelativePath", "Classes/GoogleVRControllerEventManager.h" },
		{ "ToolTip", "DEPRECATED:  Please use VRNotificationsComponent's VRControllerRecentered delegate instead!" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::NewProp_OnControllerRecenteredDelegate = { "OnControllerRecenteredDelegate", nullptr, (EPropertyFlags)0x0010000030080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRControllerEventManager, OnControllerRecenteredDelegate_DEPRECATED), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerRecenterDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::NewProp_OnControllerRecenteredDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::NewProp_OnControllerRecenteredDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::NewProp_OnControllerStateChangedDelegate_MetaData[] = {
		{ "ModuleRelativePath", "Classes/GoogleVRControllerEventManager.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::NewProp_OnControllerStateChangedDelegate = { "OnControllerStateChangedDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRControllerEventManager, OnControllerStateChangedDelegate), Z_Construct_UDelegateFunction_GoogleVRController_GoogleVRControllerStateChangeDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::NewProp_OnControllerStateChangedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::NewProp_OnControllerStateChangedDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::NewProp_OnControllerRecenteredDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::NewProp_OnControllerStateChangedDelegate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleVRControllerEventManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::ClassParams = {
		&UGoogleVRControllerEventManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRControllerEventManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRControllerEventManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRControllerEventManager, 1519823490);
	template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<UGoogleVRControllerEventManager>()
	{
		return UGoogleVRControllerEventManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRControllerEventManager(Z_Construct_UClass_UGoogleVRControllerEventManager, &UGoogleVRControllerEventManager::StaticClass, TEXT("/Script/GoogleVRController"), TEXT("UGoogleVRControllerEventManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRControllerEventManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
