// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRController/Classes/GoogleVRPointer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRPointer() {}
// Cross Module References
	GOOGLEVRCONTROLLER_API UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRPointerInputMode();
	UPackage* Z_Construct_UPackage__Script_GoogleVRController();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRPointer_NoRegister();
	GOOGLEVRCONTROLLER_API UClass* Z_Construct_UClass_UGoogleVRPointer();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
// End Cross Module References
	static UEnum* EGoogleVRPointerInputMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GoogleVRController_EGoogleVRPointerInputMode, Z_Construct_UPackage__Script_GoogleVRController(), TEXT("EGoogleVRPointerInputMode"));
		}
		return Singleton;
	}
	template<> GOOGLEVRCONTROLLER_API UEnum* StaticEnum<EGoogleVRPointerInputMode>()
	{
		return EGoogleVRPointerInputMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGoogleVRPointerInputMode(EGoogleVRPointerInputMode_StaticEnum, TEXT("/Script/GoogleVRController"), TEXT("EGoogleVRPointerInputMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRPointerInputMode_Hash() { return 3437941498U; }
	UEnum* Z_Construct_UEnum_GoogleVRController_EGoogleVRPointerInputMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleVRController();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGoogleVRPointerInputMode"), 0, Get_Z_Construct_UEnum_GoogleVRController_EGoogleVRPointerInputMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGoogleVRPointerInputMode::Camera", (int64)EGoogleVRPointerInputMode::Camera },
				{ "EGoogleVRPointerInputMode::Direct", (int64)EGoogleVRPointerInputMode::Direct },
				{ "EGoogleVRPointerInputMode::HybridExperimental", (int64)EGoogleVRPointerInputMode::HybridExperimental },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Camera.Comment", "/**\n\x09*  Sweep a sphere based on the pointer's radius from the camera through the target of the pointer.\n\x09*  This is ideal for reticles that are always rendered on top.\n\x09*  The object that is selected will always be the object that appears\n\x09*  underneath the reticle from the perspective of the camera.\n\x09*  This also prevents the reticle from appearing to \"jump\" when it starts/stops hitting an object.\n\x09*\n\x09*  Note: This will prevent the user from pointing around an object to hit something that is out of sight.\n\x09*  This isn't a problem in a typical use case.\n\x09*/" },
				{ "Camera.Name", "EGoogleVRPointerInputMode::Camera" },
				{ "Camera.ToolTip", "Sweep a sphere based on the pointer's radius from the camera through the target of the pointer.\nThis is ideal for reticles that are always rendered on top.\nThe object that is selected will always be the object that appears\nunderneath the reticle from the perspective of the camera.\nThis also prevents the reticle from appearing to \"jump\" when it starts/stops hitting an object.\n\nNote: This will prevent the user from pointing around an object to hit something that is out of sight.\nThis isn't a problem in a typical use case." },
				{ "Direct.Comment", "/**\n\x09*  Sweep a sphere based on the pointer's radius directly from the pointer origin.\n\x09*  This is ideal for full-length laser pointers.\n\x09*/" },
				{ "Direct.Name", "EGoogleVRPointerInputMode::Direct" },
				{ "Direct.ToolTip", "Sweep a sphere based on the pointer's radius directly from the pointer origin.\nThis is ideal for full-length laser pointers." },
				{ "HybridExperimental.Comment", "/*\n\x09*  Default method for casting ray.\n\x09*  Combines the Camera and Direct raycast modes.\n\x09*  Uses a Direct ray up until the CameraRayIntersectionDistance, and then switches to use\n\x09*  a Camera ray starting from the point where the two rays intersect.\n\x09*\n\x09*  This is the most versatile raycast mode. Like Camera mode, this prevents the reticle\n\x09*  appearing jumpy. Additionally, it still allows the user to target objects that are close\n\x09*  to them by using the laser as a visual reference.\n\x09*/" },
				{ "HybridExperimental.Name", "EGoogleVRPointerInputMode::HybridExperimental" },
				{ "HybridExperimental.ToolTip", "*  Default method for casting ray.\n*  Combines the Camera and Direct raycast modes.\n*  Uses a Direct ray up until the CameraRayIntersectionDistance, and then switches to use\n*  a Camera ray starting from the point where the two rays intersect.\n*\n*  This is the most versatile raycast mode. Like Camera mode, this prevents the reticle\n*  appearing jumpy. Additionally, it still allows the user to target objects that are close\n*  to them by using the laser as a visual reference." },
				{ "ModuleRelativePath", "Classes/GoogleVRPointer.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GoogleVRController,
				nullptr,
				"EGoogleVRPointerInputMode",
				"EGoogleVRPointerInputMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UGoogleVRPointer::StaticRegisterNativesUGoogleVRPointer()
	{
	}
	UClass* Z_Construct_UClass_UGoogleVRPointer_NoRegister()
	{
		return UGoogleVRPointer::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRPointer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRPointer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRPointer_Statics::Class_MetaDataParams[] = {
		{ "CannotImplementInterfaceInBlueprint", "" },
		{ "ModuleRelativePath", "Classes/GoogleVRPointer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRPointer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IGoogleVRPointer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRPointer_Statics::ClassParams = {
		&UGoogleVRPointer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRPointer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRPointer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRPointer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRPointer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRPointer, 1381017350);
	template<> GOOGLEVRCONTROLLER_API UClass* StaticClass<UGoogleVRPointer>()
	{
		return UGoogleVRPointer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRPointer(Z_Construct_UClass_UGoogleVRPointer, &UGoogleVRPointer::StaticClass, TEXT("/Script/GoogleVRController"), TEXT("UGoogleVRPointer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRPointer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
