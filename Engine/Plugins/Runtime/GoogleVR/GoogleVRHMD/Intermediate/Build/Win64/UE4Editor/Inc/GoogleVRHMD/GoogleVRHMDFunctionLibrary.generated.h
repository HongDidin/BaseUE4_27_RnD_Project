// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FQuat;
struct FVector;
enum class ESafetyRegionType : uint8;
class UTexture2D;
struct FVector2D;
struct FIntPoint;
enum class EDistortionMeshSizeEnum : uint8;
#ifdef GOOGLEVRHMD_GoogleVRHMDFunctionLibrary_generated_h
#error "GoogleVRHMDFunctionLibrary.generated.h already included, missing '#pragma once' in GoogleVRHMDFunctionLibrary.h"
#endif
#define GOOGLEVRHMD_GoogleVRHMDFunctionLibrary_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetRecenterControllerOnly); \
	DECLARE_FUNCTION(execGetRecenterTransform); \
	DECLARE_FUNCTION(execGetSafetyRegion); \
	DECLARE_FUNCTION(execGetSafetyCylinderOuterRadius); \
	DECLARE_FUNCTION(execGetSafetyCylinderInnerRadius); \
	DECLARE_FUNCTION(execGetFloorHeight); \
	DECLARE_FUNCTION(execClearDaydreamLoadingSplashScreenTexture); \
	DECLARE_FUNCTION(execSetDaydreamLoadingSplashScreenViewAngle); \
	DECLARE_FUNCTION(execGetDaydreamLoadingSplashScreenViewAngle); \
	DECLARE_FUNCTION(execSetDaydreamLoadingSplashScreenScale); \
	DECLARE_FUNCTION(execGetDaydreamLoadingSplashScreenScale); \
	DECLARE_FUNCTION(execSetDaydreamLoadingSplashScreenDistance); \
	DECLARE_FUNCTION(execGetDaydreamLoadingSplashScreenDistance); \
	DECLARE_FUNCTION(execSetDaydreamLoadingSplashScreenTexture); \
	DECLARE_FUNCTION(execSetDaydreamLoadingSplashScreenEnable); \
	DECLARE_FUNCTION(execGetIntentData); \
	DECLARE_FUNCTION(execGetNeckModelScale); \
	DECLARE_FUNCTION(execSetNeckModelScale); \
	DECLARE_FUNCTION(execSetGVRHMDRenderTargetSize); \
	DECLARE_FUNCTION(execSetGVRHMDRenderTargetScale); \
	DECLARE_FUNCTION(execSetRenderTargetSizeToDefault); \
	DECLARE_FUNCTION(execGetGVRHMDRenderTargetSize); \
	DECLARE_FUNCTION(execIsInDaydreamMode); \
	DECLARE_FUNCTION(execIsVrLaunch); \
	DECLARE_FUNCTION(execGetViewerVendor); \
	DECLARE_FUNCTION(execGetViewerModel); \
	DECLARE_FUNCTION(execGetDistortionCorrectionEnabled); \
	DECLARE_FUNCTION(execSetDistortionMeshSize); \
	DECLARE_FUNCTION(execSetDefaultViewerProfile); \
	DECLARE_FUNCTION(execSetDistortionCorrectionEnabled); \
	DECLARE_FUNCTION(execSetSustainedPerformanceModeEnabled); \
	DECLARE_FUNCTION(execIsGoogleVRStereoRenderingEnabled); \
	DECLARE_FUNCTION(execIsGoogleVRHMDEnabled);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetRecenterControllerOnly); \
	DECLARE_FUNCTION(execGetRecenterTransform); \
	DECLARE_FUNCTION(execGetSafetyRegion); \
	DECLARE_FUNCTION(execGetSafetyCylinderOuterRadius); \
	DECLARE_FUNCTION(execGetSafetyCylinderInnerRadius); \
	DECLARE_FUNCTION(execGetFloorHeight); \
	DECLARE_FUNCTION(execClearDaydreamLoadingSplashScreenTexture); \
	DECLARE_FUNCTION(execSetDaydreamLoadingSplashScreenViewAngle); \
	DECLARE_FUNCTION(execGetDaydreamLoadingSplashScreenViewAngle); \
	DECLARE_FUNCTION(execSetDaydreamLoadingSplashScreenScale); \
	DECLARE_FUNCTION(execGetDaydreamLoadingSplashScreenScale); \
	DECLARE_FUNCTION(execSetDaydreamLoadingSplashScreenDistance); \
	DECLARE_FUNCTION(execGetDaydreamLoadingSplashScreenDistance); \
	DECLARE_FUNCTION(execSetDaydreamLoadingSplashScreenTexture); \
	DECLARE_FUNCTION(execSetDaydreamLoadingSplashScreenEnable); \
	DECLARE_FUNCTION(execGetIntentData); \
	DECLARE_FUNCTION(execGetNeckModelScale); \
	DECLARE_FUNCTION(execSetNeckModelScale); \
	DECLARE_FUNCTION(execSetGVRHMDRenderTargetSize); \
	DECLARE_FUNCTION(execSetGVRHMDRenderTargetScale); \
	DECLARE_FUNCTION(execSetRenderTargetSizeToDefault); \
	DECLARE_FUNCTION(execGetGVRHMDRenderTargetSize); \
	DECLARE_FUNCTION(execIsInDaydreamMode); \
	DECLARE_FUNCTION(execIsVrLaunch); \
	DECLARE_FUNCTION(execGetViewerVendor); \
	DECLARE_FUNCTION(execGetViewerModel); \
	DECLARE_FUNCTION(execGetDistortionCorrectionEnabled); \
	DECLARE_FUNCTION(execSetDistortionMeshSize); \
	DECLARE_FUNCTION(execSetDefaultViewerProfile); \
	DECLARE_FUNCTION(execSetDistortionCorrectionEnabled); \
	DECLARE_FUNCTION(execSetSustainedPerformanceModeEnabled); \
	DECLARE_FUNCTION(execIsGoogleVRStereoRenderingEnabled); \
	DECLARE_FUNCTION(execIsGoogleVRHMDEnabled);


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRHMDFunctionLibrary(); \
	friend struct Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRHMDFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleVRHMD"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRHMDFunctionLibrary)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRHMDFunctionLibrary(); \
	friend struct Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRHMDFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleVRHMD"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRHMDFunctionLibrary)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRHMDFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRHMDFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRHMDFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRHMDFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRHMDFunctionLibrary(UGoogleVRHMDFunctionLibrary&&); \
	NO_API UGoogleVRHMDFunctionLibrary(const UGoogleVRHMDFunctionLibrary&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRHMDFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRHMDFunctionLibrary(UGoogleVRHMDFunctionLibrary&&); \
	NO_API UGoogleVRHMDFunctionLibrary(const UGoogleVRHMDFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRHMDFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRHMDFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRHMDFunctionLibrary)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_33_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h_36_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GoogleVRHMDFunctionLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRHMD_API UClass* StaticClass<class UGoogleVRHMDFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRHMD_Source_GoogleVRHMD_Classes_GoogleVRHMDFunctionLibrary_h


#define FOREACH_ENUM_EDISTORTIONMESHSIZEENUM(op) \
	op(EDistortionMeshSizeEnum::DMS_VERYSMALL) \
	op(EDistortionMeshSizeEnum::DMS_SMALL) \
	op(EDistortionMeshSizeEnum::DMS_MEDIUM) \
	op(EDistortionMeshSizeEnum::DMS_LARGE) \
	op(EDistortionMeshSizeEnum::DMS_VERYLARGE) 

enum class EDistortionMeshSizeEnum : uint8;
template<> GOOGLEVRHMD_API UEnum* StaticEnum<EDistortionMeshSizeEnum>();

#define FOREACH_ENUM_ESAFETYREGIONTYPE(op) \
	op(ESafetyRegionType::INVALID) \
	op(ESafetyRegionType::CYLINDER) 

enum class ESafetyRegionType : uint8;
template<> GOOGLEVRHMD_API UEnum* StaticEnum<ESafetyRegionType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
