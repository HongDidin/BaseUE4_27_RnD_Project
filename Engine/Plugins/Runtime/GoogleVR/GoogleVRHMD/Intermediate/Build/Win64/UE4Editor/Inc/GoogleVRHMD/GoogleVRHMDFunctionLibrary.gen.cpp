// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRHMD/Classes/GoogleVRHMDFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRHMDFunctionLibrary() {}
// Cross Module References
	GOOGLEVRHMD_API UEnum* Z_Construct_UEnum_GoogleVRHMD_EDistortionMeshSizeEnum();
	UPackage* Z_Construct_UPackage__Script_GoogleVRHMD();
	GOOGLEVRHMD_API UEnum* Z_Construct_UEnum_GoogleVRHMD_ESafetyRegionType();
	GOOGLEVRHMD_API UClass* Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_NoRegister();
	GOOGLEVRHMD_API UClass* Z_Construct_UClass_UGoogleVRHMDFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	static UEnum* EDistortionMeshSizeEnum_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GoogleVRHMD_EDistortionMeshSizeEnum, Z_Construct_UPackage__Script_GoogleVRHMD(), TEXT("EDistortionMeshSizeEnum"));
		}
		return Singleton;
	}
	template<> GOOGLEVRHMD_API UEnum* StaticEnum<EDistortionMeshSizeEnum>()
	{
		return EDistortionMeshSizeEnum_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDistortionMeshSizeEnum(EDistortionMeshSizeEnum_StaticEnum, TEXT("/Script/GoogleVRHMD"), TEXT("EDistortionMeshSizeEnum"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GoogleVRHMD_EDistortionMeshSizeEnum_Hash() { return 1161359418U; }
	UEnum* Z_Construct_UEnum_GoogleVRHMD_EDistortionMeshSizeEnum()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleVRHMD();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDistortionMeshSizeEnum"), 0, Get_Z_Construct_UEnum_GoogleVRHMD_EDistortionMeshSizeEnum_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDistortionMeshSizeEnum::DMS_VERYSMALL", (int64)EDistortionMeshSizeEnum::DMS_VERYSMALL },
				{ "EDistortionMeshSizeEnum::DMS_SMALL", (int64)EDistortionMeshSizeEnum::DMS_SMALL },
				{ "EDistortionMeshSizeEnum::DMS_MEDIUM", (int64)EDistortionMeshSizeEnum::DMS_MEDIUM },
				{ "EDistortionMeshSizeEnum::DMS_LARGE", (int64)EDistortionMeshSizeEnum::DMS_LARGE },
				{ "EDistortionMeshSizeEnum::DMS_VERYLARGE", (int64)EDistortionMeshSizeEnum::DMS_VERYLARGE },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Enum to specify distortion mesh size */" },
				{ "DMS_LARGE.DisplayName", "Distortion Mesh Size Large 80x80" },
				{ "DMS_LARGE.Name", "EDistortionMeshSizeEnum::DMS_LARGE" },
				{ "DMS_MEDIUM.DisplayName", "Distortion Mesh Size Medium 60x60" },
				{ "DMS_MEDIUM.Name", "EDistortionMeshSizeEnum::DMS_MEDIUM" },
				{ "DMS_SMALL.DisplayName", "Distortion Mesh Size Small 40x40" },
				{ "DMS_SMALL.Name", "EDistortionMeshSizeEnum::DMS_SMALL" },
				{ "DMS_VERYLARGE.DisplayName", "Distortion Mesh Size Very Large 100x100" },
				{ "DMS_VERYLARGE.Name", "EDistortionMeshSizeEnum::DMS_VERYLARGE" },
				{ "DMS_VERYSMALL.DisplayName", "Distortion Mesh Size Very Small 20x20" },
				{ "DMS_VERYSMALL.Name", "EDistortionMeshSizeEnum::DMS_VERYSMALL" },
				{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
				{ "ToolTip", "Enum to specify distortion mesh size" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GoogleVRHMD,
				nullptr,
				"EDistortionMeshSizeEnum",
				"EDistortionMeshSizeEnum",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ESafetyRegionType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GoogleVRHMD_ESafetyRegionType, Z_Construct_UPackage__Script_GoogleVRHMD(), TEXT("ESafetyRegionType"));
		}
		return Singleton;
	}
	template<> GOOGLEVRHMD_API UEnum* StaticEnum<ESafetyRegionType>()
	{
		return ESafetyRegionType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESafetyRegionType(ESafetyRegionType_StaticEnum, TEXT("/Script/GoogleVRHMD"), TEXT("ESafetyRegionType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GoogleVRHMD_ESafetyRegionType_Hash() { return 2989147646U; }
	UEnum* Z_Construct_UEnum_GoogleVRHMD_ESafetyRegionType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleVRHMD();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESafetyRegionType"), 0, Get_Z_Construct_UEnum_GoogleVRHMD_ESafetyRegionType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESafetyRegionType::INVALID", (int64)ESafetyRegionType::INVALID },
				{ "ESafetyRegionType::CYLINDER", (int64)ESafetyRegionType::CYLINDER },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Maps to gvr_safety_region_type */" },
				{ "CYLINDER.DisplayName", "Cylinder Safety Region Type" },
				{ "CYLINDER.Name", "ESafetyRegionType::CYLINDER" },
				{ "INVALID.DisplayName", "Invalid Safety Region Type" },
				{ "INVALID.Name", "ESafetyRegionType::INVALID" },
				{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
				{ "ToolTip", "Maps to gvr_safety_region_type" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GoogleVRHMD,
				nullptr,
				"ESafetyRegionType",
				"ESafetyRegionType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetRecenterControllerOnly)
	{
		P_GET_UBOOL(Z_Param_bIsRecenterControllerOnly);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::SetRecenterControllerOnly(Z_Param_bIsRecenterControllerOnly);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetRecenterTransform)
	{
		P_GET_STRUCT_REF(FQuat,Z_Param_Out_RecenterOrientation);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_RecenterPosition);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetRecenterTransform(Z_Param_Out_RecenterOrientation,Z_Param_Out_RecenterPosition);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetSafetyRegion)
	{
		P_GET_ENUM_REF(ESafetyRegionType,Z_Param_Out_RegionType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetSafetyRegion((ESafetyRegionType&)(Z_Param_Out_RegionType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetSafetyCylinderOuterRadius)
	{
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_OuterRadius);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetSafetyCylinderOuterRadius(Z_Param_Out_OuterRadius);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetSafetyCylinderInnerRadius)
	{
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_InnerRadius);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetSafetyCylinderInnerRadius(Z_Param_Out_InnerRadius);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetFloorHeight)
	{
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_FloorHeight);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetFloorHeight(Z_Param_Out_FloorHeight);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execClearDaydreamLoadingSplashScreenTexture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::ClearDaydreamLoadingSplashScreenTexture();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetDaydreamLoadingSplashScreenViewAngle)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewViewAngle);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::SetDaydreamLoadingSplashScreenViewAngle(Z_Param_NewViewAngle);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetDaydreamLoadingSplashScreenViewAngle)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetDaydreamLoadingSplashScreenViewAngle();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetDaydreamLoadingSplashScreenScale)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewSize);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::SetDaydreamLoadingSplashScreenScale(Z_Param_NewSize);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetDaydreamLoadingSplashScreenScale)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetDaydreamLoadingSplashScreenScale();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetDaydreamLoadingSplashScreenDistance)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewDistance);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::SetDaydreamLoadingSplashScreenDistance(Z_Param_NewDistance);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetDaydreamLoadingSplashScreenDistance)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetDaydreamLoadingSplashScreenDistance();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetDaydreamLoadingSplashScreenTexture)
	{
		P_GET_OBJECT(UTexture2D,Z_Param_Texture);
		P_GET_STRUCT(FVector2D,Z_Param_UVOffset);
		P_GET_STRUCT(FVector2D,Z_Param_UVSize);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::SetDaydreamLoadingSplashScreenTexture(Z_Param_Texture,Z_Param_UVOffset,Z_Param_UVSize);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetDaydreamLoadingSplashScreenEnable)
	{
		P_GET_UBOOL(Z_Param_enable);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::SetDaydreamLoadingSplashScreenEnable(Z_Param_enable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetIntentData)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetIntentData();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetNeckModelScale)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetNeckModelScale();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetNeckModelScale)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_ScaleFactor);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::SetNeckModelScale(Z_Param_ScaleFactor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetGVRHMDRenderTargetSize)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_DesiredWidth);
		P_GET_PROPERTY(FIntProperty,Z_Param_DesiredHeight);
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_OutRenderTargetSize);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::SetGVRHMDRenderTargetSize(Z_Param_DesiredWidth,Z_Param_DesiredHeight,Z_Param_Out_OutRenderTargetSize);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetGVRHMDRenderTargetScale)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_ScaleFactor);
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_OutRenderTargetSize);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::SetGVRHMDRenderTargetScale(Z_Param_ScaleFactor,Z_Param_Out_OutRenderTargetSize);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetRenderTargetSizeToDefault)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FIntPoint*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::SetRenderTargetSizeToDefault();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetGVRHMDRenderTargetSize)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FIntPoint*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetGVRHMDRenderTargetSize();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execIsInDaydreamMode)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::IsInDaydreamMode();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execIsVrLaunch)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::IsVrLaunch();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetViewerVendor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetViewerVendor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetViewerModel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetViewerModel();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execGetDistortionCorrectionEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::GetDistortionCorrectionEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetDistortionMeshSize)
	{
		P_GET_ENUM(EDistortionMeshSizeEnum,Z_Param_MeshSize);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::SetDistortionMeshSize(EDistortionMeshSizeEnum(Z_Param_MeshSize));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetDefaultViewerProfile)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewerProfileURL);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::SetDefaultViewerProfile(Z_Param_ViewerProfileURL);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetDistortionCorrectionEnabled)
	{
		P_GET_UBOOL(Z_Param_bEnable);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::SetDistortionCorrectionEnabled(Z_Param_bEnable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execSetSustainedPerformanceModeEnabled)
	{
		P_GET_UBOOL(Z_Param_bEnable);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRHMDFunctionLibrary::SetSustainedPerformanceModeEnabled(Z_Param_bEnable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execIsGoogleVRStereoRenderingEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::IsGoogleVRStereoRenderingEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRHMDFunctionLibrary::execIsGoogleVRHMDEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UGoogleVRHMDFunctionLibrary::IsGoogleVRHMDEnabled();
		P_NATIVE_END;
	}
	void UGoogleVRHMDFunctionLibrary::StaticRegisterNativesUGoogleVRHMDFunctionLibrary()
	{
		UClass* Class = UGoogleVRHMDFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ClearDaydreamLoadingSplashScreenTexture", &UGoogleVRHMDFunctionLibrary::execClearDaydreamLoadingSplashScreenTexture },
			{ "GetDaydreamLoadingSplashScreenDistance", &UGoogleVRHMDFunctionLibrary::execGetDaydreamLoadingSplashScreenDistance },
			{ "GetDaydreamLoadingSplashScreenScale", &UGoogleVRHMDFunctionLibrary::execGetDaydreamLoadingSplashScreenScale },
			{ "GetDaydreamLoadingSplashScreenViewAngle", &UGoogleVRHMDFunctionLibrary::execGetDaydreamLoadingSplashScreenViewAngle },
			{ "GetDistortionCorrectionEnabled", &UGoogleVRHMDFunctionLibrary::execGetDistortionCorrectionEnabled },
			{ "GetFloorHeight", &UGoogleVRHMDFunctionLibrary::execGetFloorHeight },
			{ "GetGVRHMDRenderTargetSize", &UGoogleVRHMDFunctionLibrary::execGetGVRHMDRenderTargetSize },
			{ "GetIntentData", &UGoogleVRHMDFunctionLibrary::execGetIntentData },
			{ "GetNeckModelScale", &UGoogleVRHMDFunctionLibrary::execGetNeckModelScale },
			{ "GetRecenterTransform", &UGoogleVRHMDFunctionLibrary::execGetRecenterTransform },
			{ "GetSafetyCylinderInnerRadius", &UGoogleVRHMDFunctionLibrary::execGetSafetyCylinderInnerRadius },
			{ "GetSafetyCylinderOuterRadius", &UGoogleVRHMDFunctionLibrary::execGetSafetyCylinderOuterRadius },
			{ "GetSafetyRegion", &UGoogleVRHMDFunctionLibrary::execGetSafetyRegion },
			{ "GetViewerModel", &UGoogleVRHMDFunctionLibrary::execGetViewerModel },
			{ "GetViewerVendor", &UGoogleVRHMDFunctionLibrary::execGetViewerVendor },
			{ "IsGoogleVRHMDEnabled", &UGoogleVRHMDFunctionLibrary::execIsGoogleVRHMDEnabled },
			{ "IsGoogleVRStereoRenderingEnabled", &UGoogleVRHMDFunctionLibrary::execIsGoogleVRStereoRenderingEnabled },
			{ "IsInDaydreamMode", &UGoogleVRHMDFunctionLibrary::execIsInDaydreamMode },
			{ "IsVrLaunch", &UGoogleVRHMDFunctionLibrary::execIsVrLaunch },
			{ "SetDaydreamLoadingSplashScreenDistance", &UGoogleVRHMDFunctionLibrary::execSetDaydreamLoadingSplashScreenDistance },
			{ "SetDaydreamLoadingSplashScreenEnable", &UGoogleVRHMDFunctionLibrary::execSetDaydreamLoadingSplashScreenEnable },
			{ "SetDaydreamLoadingSplashScreenScale", &UGoogleVRHMDFunctionLibrary::execSetDaydreamLoadingSplashScreenScale },
			{ "SetDaydreamLoadingSplashScreenTexture", &UGoogleVRHMDFunctionLibrary::execSetDaydreamLoadingSplashScreenTexture },
			{ "SetDaydreamLoadingSplashScreenViewAngle", &UGoogleVRHMDFunctionLibrary::execSetDaydreamLoadingSplashScreenViewAngle },
			{ "SetDefaultViewerProfile", &UGoogleVRHMDFunctionLibrary::execSetDefaultViewerProfile },
			{ "SetDistortionCorrectionEnabled", &UGoogleVRHMDFunctionLibrary::execSetDistortionCorrectionEnabled },
			{ "SetDistortionMeshSize", &UGoogleVRHMDFunctionLibrary::execSetDistortionMeshSize },
			{ "SetGVRHMDRenderTargetScale", &UGoogleVRHMDFunctionLibrary::execSetGVRHMDRenderTargetScale },
			{ "SetGVRHMDRenderTargetSize", &UGoogleVRHMDFunctionLibrary::execSetGVRHMDRenderTargetSize },
			{ "SetNeckModelScale", &UGoogleVRHMDFunctionLibrary::execSetNeckModelScale },
			{ "SetRecenterControllerOnly", &UGoogleVRHMDFunctionLibrary::execSetRecenterControllerOnly },
			{ "SetRenderTargetSizeToDefault", &UGoogleVRHMDFunctionLibrary::execSetRenderTargetSizeToDefault },
			{ "SetSustainedPerformanceModeEnabled", &UGoogleVRHMDFunctionLibrary::execSetSustainedPerformanceModeEnabled },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_ClearDaydreamLoadingSplashScreenTexture_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_ClearDaydreamLoadingSplashScreenTexture_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09 * Clear the loading splash texture it is current using. This will make the loading screen to black if the loading splash screen is still enabled.\n\x09 * Note that this function only works for daydream app.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR Splash" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Clear the loading splash texture it is current using. This will make the loading screen to black if the loading splash screen is still enabled.\nNote that this function only works for daydream app." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_ClearDaydreamLoadingSplashScreenTexture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "ClearDaydreamLoadingSplashScreenTexture", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_ClearDaydreamLoadingSplashScreenTexture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_ClearDaydreamLoadingSplashScreenTexture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_ClearDaydreamLoadingSplashScreenTexture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_ClearDaydreamLoadingSplashScreenTexture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetDaydreamLoadingSplashScreenDistance_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetDaydreamLoadingSplashScreenDistance_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09 * Get the distance in meter the daydream splash screen will be rendered at\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR Splash" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Get the distance in meter the daydream splash screen will be rendered at" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetDaydreamLoadingSplashScreenDistance", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetDaydreamLoadingSplashScreenDistance_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetDaydreamLoadingSplashScreenScale_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetDaydreamLoadingSplashScreenScale_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09 * Get the render scale of the dayderam splash screen\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR Splash" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Get the render scale of the dayderam splash screen" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetDaydreamLoadingSplashScreenScale", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetDaydreamLoadingSplashScreenScale_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetDaydreamLoadingSplashScreenViewAngle_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetDaydreamLoadingSplashScreenViewAngle_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09 * Get the view angle of the dayderam splash screen\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR Splash" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Get the view angle of the dayderam splash screen" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetDaydreamLoadingSplashScreenViewAngle", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetDaydreamLoadingSplashScreenViewAngle_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetDistortionCorrectionEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventGetDistortionCorrectionEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventGetDistortionCorrectionEnabled_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Check if distortion correction is enabled */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Check if distortion correction is enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetDistortionCorrectionEnabled", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetDistortionCorrectionEnabled_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetFloorHeight_Parms
		{
			float FloorHeight;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloorHeight;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::NewProp_FloorHeight = { "FloorHeight", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetFloorHeight_Parms, FloorHeight), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventGetFloorHeight_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventGetFloorHeight_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::NewProp_FloorHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09* Tries to get the floor height if available\n\x09* @param FloorHeight where the floor height read will get stored\n\x09* returns true is the read was successful, false otherwise\n\x09*/" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Tries to get the floor height if available\n@param FloorHeight where the floor height read will get stored\nreturns true is the read was successful, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetFloorHeight", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetFloorHeight_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetGVRHMDRenderTargetSize_Parms
		{
			FIntPoint ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetGVRHMDRenderTargetSize_Parms, ReturnValue), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Get the RenderTarget size GoogleVRHMD is using for rendering the scene.\n\x09 *  @return The render target size that is used when rendering the scene.\n\x09*/" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Get the RenderTarget size GoogleVRHMD is using for rendering the scene.\n@return The render target size that is used when rendering the scene." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetGVRHMDRenderTargetSize", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetGVRHMDRenderTargetSize_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetIntentData_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetIntentData_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09 * Returns the string representation of the data URI on which this activity's intent is operating.\n\x09 * See Intent.getDataString() in the Android documentation.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Returns the string representation of the data URI on which this activity's intent is operating.\nSee Intent.getDataString() in the Android documentation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetIntentData", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetIntentData_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetNeckModelScale_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetNeckModelScale_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** A scaling factor for the neck model offset, clamped from 0 to 1.\n\x09 * This should be 1 for most scenarios, while 0 will effectively disable\n\x09 * neck model application. This value can be animated to smoothly\n\x09 * interpolate between alternative (client-defined) neck models.\n\x09 *  @return the current neck model scale.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "A scaling factor for the neck model offset, clamped from 0 to 1.\nThis should be 1 for most scenarios, while 0 will effectively disable\nneck model application. This value can be animated to smoothly\ninterpolate between alternative (client-defined) neck models.\n@return the current neck model scale." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetNeckModelScale", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetNeckModelScale_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetRecenterTransform_Parms
		{
			FQuat RecenterOrientation;
			FVector RecenterPosition;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RecenterOrientation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RecenterPosition;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::NewProp_RecenterOrientation = { "RecenterOrientation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetRecenterTransform_Parms, RecenterOrientation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::NewProp_RecenterPosition = { "RecenterPosition", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetRecenterTransform_Parms, RecenterPosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventGetRecenterTransform_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventGetRecenterTransform_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::NewProp_RecenterOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::NewProp_RecenterPosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09* Tries to get the Recenter Transform if available\n\x09* @param RecenterOrientation where the Recenter Orientation read will get stored\n\x09* @param RecenterPosition where the Recenter Position read will get stored\n\x09* returns true is the read was successful, false otherwise\n\x09*/" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Tries to get the Recenter Transform if available\n@param RecenterOrientation where the Recenter Orientation read will get stored\n@param RecenterPosition where the Recenter Position read will get stored\nreturns true is the read was successful, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetRecenterTransform", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetRecenterTransform_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetSafetyCylinderInnerRadius_Parms
		{
			float InnerRadius;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InnerRadius;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::NewProp_InnerRadius = { "InnerRadius", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetSafetyCylinderInnerRadius_Parms, InnerRadius), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventGetSafetyCylinderInnerRadius_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventGetSafetyCylinderInnerRadius_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::NewProp_InnerRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09* Tries to get the Safety Cylinder Inner Radius if available\n\x09* @param InnerRadius where the Safety Cylinder Inner Radius read will get stored\n\x09* returns true is the read was successful, false otherwise\n\x09*/" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Tries to get the Safety Cylinder Inner Radius if available\n@param InnerRadius where the Safety Cylinder Inner Radius read will get stored\nreturns true is the read was successful, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetSafetyCylinderInnerRadius", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetSafetyCylinderInnerRadius_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetSafetyCylinderOuterRadius_Parms
		{
			float OuterRadius;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OuterRadius;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::NewProp_OuterRadius = { "OuterRadius", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetSafetyCylinderOuterRadius_Parms, OuterRadius), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventGetSafetyCylinderOuterRadius_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventGetSafetyCylinderOuterRadius_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::NewProp_OuterRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09* Tries to get the Safety Cylinder Outer Radius if available\n\x09* @param OuterRadius where the Safety Cylinder Outer Radius read will get stored\n\x09* returns true is the read was successful, false otherwise\n\x09*/" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Tries to get the Safety Cylinder Outer Radius if available\n@param OuterRadius where the Safety Cylinder Outer Radius read will get stored\nreturns true is the read was successful, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetSafetyCylinderOuterRadius", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetSafetyCylinderOuterRadius_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetSafetyRegion_Parms
		{
			ESafetyRegionType RegionType;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RegionType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RegionType;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::NewProp_RegionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::NewProp_RegionType = { "RegionType", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetSafetyRegion_Parms, RegionType), Z_Construct_UEnum_GoogleVRHMD_ESafetyRegionType, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventGetSafetyRegion_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventGetSafetyRegion_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::NewProp_RegionType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::NewProp_RegionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09* Tries to get the Safety Region Type if available\n\x09* @param RegionType where the Safety Region Type read will get stored\n\x09* returns true is the read was successful, false otherwise\n\x09*/" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Tries to get the Safety Region Type if available\n@param RegionType where the Safety Region Type read will get stored\nreturns true is the read was successful, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetSafetyRegion", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetSafetyRegion_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetViewerModel_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetViewerModel_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Get the currently set viewer model */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Get the currently set viewer model" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetViewerModel", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetViewerModel_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventGetViewerVendor_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventGetViewerVendor_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Get the currently set viewer vendor */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Get the currently set viewer vendor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "GetViewerVendor", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventGetViewerVendor_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventIsGoogleVRHMDEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventIsGoogleVRHMDEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventIsGoogleVRHMDEnabled_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "IsGoogleVRHMDEnabled", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventIsGoogleVRHMDEnabled_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventIsGoogleVRStereoRenderingEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventIsGoogleVRStereoRenderingEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventIsGoogleVRStereoRenderingEnabled_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "IsGoogleVRStereoRenderingEnabled", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventIsGoogleVRStereoRenderingEnabled_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventIsInDaydreamMode_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventIsInDaydreamMode_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventIsInDaydreamMode_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Is the application running in Daydream mode. */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Is the application running in Daydream mode." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "IsInDaydreamMode", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventIsInDaydreamMode_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventIsVrLaunch_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventIsVrLaunch_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventIsVrLaunch_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Was the application launched in Vr. */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Was the application launched in Vr." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "IsVrLaunch", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventIsVrLaunch_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenDistance_Parms
		{
			float NewDistance;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewDistance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics::NewProp_NewDistance = { "NewDistance", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenDistance_Parms, NewDistance), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics::NewProp_NewDistance,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09 * Set the distance in meter the daydream splash screen will be rendered at\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR Splash" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Set the distance in meter the daydream splash screen will be rendered at" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetDaydreamLoadingSplashScreenDistance", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenDistance_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenEnable_Parms
		{
			bool enable;
		};
		static void NewProp_enable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_enable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::NewProp_enable_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenEnable_Parms*)Obj)->enable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::NewProp_enable = { "enable", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenEnable_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::NewProp_enable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::NewProp_enable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09 * Set whether to enable the loading splash screen in daydream app.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR Splash" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Set whether to enable the loading splash screen in daydream app." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetDaydreamLoadingSplashScreenEnable", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenEnable_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenScale_Parms
		{
			float NewSize;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics::NewProp_NewSize = { "NewSize", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenScale_Parms, NewSize), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics::NewProp_NewSize,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09 * Set the render scale of the dayderam splash screen\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR Splash" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Set the render scale of the dayderam splash screen" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetDaydreamLoadingSplashScreenScale", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenScale_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenTexture_Parms
		{
			UTexture2D* Texture;
			FVector2D UVOffset;
			FVector2D UVSize;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Texture;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UVOffset;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UVSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::NewProp_Texture = { "Texture", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenTexture_Parms, Texture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::NewProp_UVOffset = { "UVOffset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenTexture_Parms, UVOffset), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::NewProp_UVSize = { "UVSize", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenTexture_Parms, UVSize), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::NewProp_Texture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::NewProp_UVOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::NewProp_UVSize,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09 * Set the loading splash screen texture the daydream app wil be using.\n\x09 * Note that this function only works for daydream app.\n\x09 *\n\x09 * @param Texture\x09\x09""A texture asset to be used for rendering the splash screen.\n\x09 * @param UVOffset\x09\x09""A 2D vector for offset the splash screen texture. Default value is (0.0, 0.0)\n\x09 * @param UVSize\x09\x09""A 2D vector specifies which part of the splash texture will be rendered on the screen. Default value is (1.0, 1.0)\n\x09 */" },
		{ "CPP_Default_UVOffset", "(X=0.000,Y=0.000)" },
		{ "CPP_Default_UVSize", "(X=1.000,Y=1.000)" },
		{ "Keywords", "Cardboard AVR GVR Splash" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Set the loading splash screen texture the daydream app wil be using.\nNote that this function only works for daydream app.\n\n@param Texture               A texture asset to be used for rendering the splash screen.\n@param UVOffset              A 2D vector for offset the splash screen texture. Default value is (0.0, 0.0)\n@param UVSize                A 2D vector specifies which part of the splash texture will be rendered on the screen. Default value is (1.0, 1.0)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetDaydreamLoadingSplashScreenTexture", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenTexture_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenViewAngle_Parms
		{
			float NewViewAngle;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewViewAngle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics::NewProp_NewViewAngle = { "NewViewAngle", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenViewAngle_Parms, NewViewAngle), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics::NewProp_NewViewAngle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09 * Set the view angle of the dayderam splash screen\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR Splash" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Set the view angle of the dayderam splash screen" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetDaydreamLoadingSplashScreenViewAngle", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetDaydreamLoadingSplashScreenViewAngle_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetDefaultViewerProfile_Parms
		{
			FString ViewerProfileURL;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewerProfileURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewerProfileURL;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::NewProp_ViewerProfileURL_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::NewProp_ViewerProfileURL = { "ViewerProfileURL", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetDefaultViewerProfile_Parms, ViewerProfileURL), METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::NewProp_ViewerProfileURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::NewProp_ViewerProfileURL_MetaData)) };
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventSetDefaultViewerProfile_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventSetDefaultViewerProfile_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::NewProp_ViewerProfileURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Change the default viewer profile */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Change the default viewer profile" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetDefaultViewerProfile", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetDefaultViewerProfile_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetDistortionCorrectionEnabled_Parms
		{
			bool bEnable;
		};
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventSetDistortionCorrectionEnabled_Parms*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventSetDistortionCorrectionEnabled_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::NewProp_bEnable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Enable/disable distortion correction */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Enable/disable distortion correction" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetDistortionCorrectionEnabled", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetDistortionCorrectionEnabled_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetDistortionMeshSize_Parms
		{
			EDistortionMeshSizeEnum MeshSize;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MeshSize_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MeshSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::NewProp_MeshSize_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::NewProp_MeshSize = { "MeshSize", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetDistortionMeshSize_Parms, MeshSize), Z_Construct_UEnum_GoogleVRHMD_EDistortionMeshSizeEnum, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::NewProp_MeshSize_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::NewProp_MeshSize,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Change the size of Distortion mesh */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Change the size of Distortion mesh" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetDistortionMeshSize", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetDistortionMeshSize_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetScale_Parms
		{
			float ScaleFactor;
			FIntPoint OutRenderTargetSize;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScaleFactor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutRenderTargetSize;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::NewProp_ScaleFactor = { "ScaleFactor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetScale_Parms, ScaleFactor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::NewProp_OutRenderTargetSize = { "OutRenderTargetSize", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetScale_Parms, OutRenderTargetSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetScale_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetScale_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::NewProp_ScaleFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::NewProp_OutRenderTargetSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Set the RenderTarget size with a scale factor.\n\x09 *  The scale factor will be multiplied by the maximal effective render target size based on the window size and the viewer.\n\x09 *\n\x09 *  @param ScaleFactor - A float number that is within [0.1, 1.0].\n\x09 *  @param OutRenderTargetSize - The actual render target size it is set to.\n\x09 *  @return true if the render target size changed.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Set the RenderTarget size with a scale factor.\nThe scale factor will be multiplied by the maximal effective render target size based on the window size and the viewer.\n\n@param ScaleFactor - A float number that is within [0.1, 1.0].\n@param OutRenderTargetSize - The actual render target size it is set to.\n@return true if the render target size changed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetGVRHMDRenderTargetScale", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetScale_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetSize_Parms
		{
			int32 DesiredWidth;
			int32 DesiredHeight;
			FIntPoint OutRenderTargetSize;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_DesiredWidth;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_DesiredHeight;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutRenderTargetSize;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::NewProp_DesiredWidth = { "DesiredWidth", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetSize_Parms, DesiredWidth), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::NewProp_DesiredHeight = { "DesiredHeight", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetSize_Parms, DesiredHeight), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::NewProp_OutRenderTargetSize = { "OutRenderTargetSize", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetSize_Parms, OutRenderTargetSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetSize_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetSize_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::NewProp_DesiredWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::NewProp_DesiredHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::NewProp_OutRenderTargetSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Set the RenderTargetSize with the desired resolution.\n\x09 *  @param DesiredWidth - The width of the render target.\n\x09 *  @param DesiredHeight - The height of the render target.\n\x09 *  @param OutRenderTargetSize - The actually render target size it is set to.\n\x09 *  @return true if the render target size changed.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Set the RenderTargetSize with the desired resolution.\n@param DesiredWidth - The width of the render target.\n@param DesiredHeight - The height of the render target.\n@param OutRenderTargetSize - The actually render target size it is set to.\n@return true if the render target size changed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetGVRHMDRenderTargetSize", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetGVRHMDRenderTargetSize_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetNeckModelScale_Parms
		{
			float ScaleFactor;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScaleFactor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics::NewProp_ScaleFactor = { "ScaleFactor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetNeckModelScale_Parms, ScaleFactor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics::NewProp_ScaleFactor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** A scaling factor for the neck model offset, clamped from 0 to 1.\n\x09 * This should be 1 for most scenarios, while 0 will effectively disable\n\x09 * neck model application. This value can be animated to smoothly\n\x09 * interpolate between alternative (client-defined) neck models.\n\x09 *  @param ScaleFactor The new neck model scale.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "A scaling factor for the neck model offset, clamped from 0 to 1.\nThis should be 1 for most scenarios, while 0 will effectively disable\nneck model application. This value can be animated to smoothly\ninterpolate between alternative (client-defined) neck models.\n@param ScaleFactor The new neck model scale." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetNeckModelScale", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetNeckModelScale_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetRecenterControllerOnly_Parms
		{
			bool bIsRecenterControllerOnly;
		};
		static void NewProp_bIsRecenterControllerOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsRecenterControllerOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::NewProp_bIsRecenterControllerOnly_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventSetRecenterControllerOnly_Parms*)Obj)->bIsRecenterControllerOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::NewProp_bIsRecenterControllerOnly = { "bIsRecenterControllerOnly", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventSetRecenterControllerOnly_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::NewProp_bIsRecenterControllerOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::NewProp_bIsRecenterControllerOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/**\n\x09* Sets whether or not the Recenter event only recenters the Controller.\n\x09* @param isRecenterControllerOnly, true will only recenter the Controller\n\x09*/" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Sets whether or not the Recenter event only recenters the Controller.\n@param isRecenterControllerOnly, true will only recenter the Controller" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetRecenterControllerOnly", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetRecenterControllerOnly_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetRenderTargetSizeToDefault_Parms
		{
			FIntPoint ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRHMDFunctionLibrary_eventSetRenderTargetSizeToDefault_Parms, ReturnValue), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Set the GoogleVR render target size to default value.\n\x09 *  @return The default render target size.\n\x09 */" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Set the GoogleVR render target size to default value.\n@return The default render target size." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetRenderTargetSizeToDefault", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetRenderTargetSizeToDefault_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics
	{
		struct GoogleVRHMDFunctionLibrary_eventSetSustainedPerformanceModeEnabled_Parms
		{
			bool bEnable;
		};
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((GoogleVRHMDFunctionLibrary_eventSetSustainedPerformanceModeEnabled_Parms*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoogleVRHMDFunctionLibrary_eventSetSustainedPerformanceModeEnabled_Parms), &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::NewProp_bEnable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleVRHMD" },
		{ "Comment", "/** Set if the app use sustained performance mode. This can be toggled at run time but note that this function only works on Android build*/" },
		{ "Keywords", "Cardboard AVR GVR" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "Set if the app use sustained performance mode. This can be toggled at run time but note that this function only works on Android build" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, nullptr, "SetSustainedPerformanceModeEnabled", nullptr, nullptr, sizeof(GoogleVRHMDFunctionLibrary_eventSetSustainedPerformanceModeEnabled_Parms), Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_NoRegister()
	{
		return UGoogleVRHMDFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRHMD,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_ClearDaydreamLoadingSplashScreenTexture, "ClearDaydreamLoadingSplashScreenTexture" }, // 3940169090
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenDistance, "GetDaydreamLoadingSplashScreenDistance" }, // 3340980499
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenScale, "GetDaydreamLoadingSplashScreenScale" }, // 49457455
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDaydreamLoadingSplashScreenViewAngle, "GetDaydreamLoadingSplashScreenViewAngle" }, // 209922361
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetDistortionCorrectionEnabled, "GetDistortionCorrectionEnabled" }, // 1265317103
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetFloorHeight, "GetFloorHeight" }, // 887663520
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetGVRHMDRenderTargetSize, "GetGVRHMDRenderTargetSize" }, // 4011930313
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetIntentData, "GetIntentData" }, // 4034940943
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetNeckModelScale, "GetNeckModelScale" }, // 3953396284
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetRecenterTransform, "GetRecenterTransform" }, // 2378412081
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderInnerRadius, "GetSafetyCylinderInnerRadius" }, // 2272014562
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyCylinderOuterRadius, "GetSafetyCylinderOuterRadius" }, // 253218891
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetSafetyRegion, "GetSafetyRegion" }, // 4223963558
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerModel, "GetViewerModel" }, // 850411594
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_GetViewerVendor, "GetViewerVendor" }, // 3552159328
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRHMDEnabled, "IsGoogleVRHMDEnabled" }, // 472340917
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsGoogleVRStereoRenderingEnabled, "IsGoogleVRStereoRenderingEnabled" }, // 1485281746
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsInDaydreamMode, "IsInDaydreamMode" }, // 4194687988
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_IsVrLaunch, "IsVrLaunch" }, // 124948220
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenDistance, "SetDaydreamLoadingSplashScreenDistance" }, // 4092586039
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenEnable, "SetDaydreamLoadingSplashScreenEnable" }, // 1998311816
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenScale, "SetDaydreamLoadingSplashScreenScale" }, // 1482607195
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenTexture, "SetDaydreamLoadingSplashScreenTexture" }, // 1506523996
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDaydreamLoadingSplashScreenViewAngle, "SetDaydreamLoadingSplashScreenViewAngle" }, // 2348997703
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDefaultViewerProfile, "SetDefaultViewerProfile" }, // 365992202
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionCorrectionEnabled, "SetDistortionCorrectionEnabled" }, // 3418737211
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetDistortionMeshSize, "SetDistortionMeshSize" }, // 789743843
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetScale, "SetGVRHMDRenderTargetScale" }, // 1176847410
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetGVRHMDRenderTargetSize, "SetGVRHMDRenderTargetSize" }, // 3106257145
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetNeckModelScale, "SetNeckModelScale" }, // 1976075186
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRecenterControllerOnly, "SetRecenterControllerOnly" }, // 1007665786
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetRenderTargetSizeToDefault, "SetRenderTargetSizeToDefault" }, // 1323094752
		{ &Z_Construct_UFunction_UGoogleVRHMDFunctionLibrary_SetSustainedPerformanceModeEnabled, "SetSustainedPerformanceModeEnabled" }, // 1167378362
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * GoogleVRHMD Extensions Function Library\n */" },
		{ "IncludePath", "GoogleVRHMDFunctionLibrary.h" },
		{ "ModuleRelativePath", "Classes/GoogleVRHMDFunctionLibrary.h" },
		{ "ToolTip", "GoogleVRHMD Extensions Function Library" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleVRHMDFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics::ClassParams = {
		&UGoogleVRHMDFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRHMDFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRHMDFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRHMDFunctionLibrary, 3943241664);
	template<> GOOGLEVRHMD_API UClass* StaticClass<UGoogleVRHMDFunctionLibrary>()
	{
		return UGoogleVRHMDFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRHMDFunctionLibrary(Z_Construct_UClass_UGoogleVRHMDFunctionLibrary, &UGoogleVRHMDFunctionLibrary::StaticClass, TEXT("/Script/GoogleVRHMD"), TEXT("UGoogleVRHMDFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRHMDFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
