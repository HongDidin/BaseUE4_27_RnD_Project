// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRTransition2D/Public/GoogleVRTransition2DBPLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRTransition2DBPLibrary() {}
// Cross Module References
	GOOGLEVRTRANSITION2D_API UClass* Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_NoRegister();
	GOOGLEVRTRANSITION2D_API UClass* Z_Construct_UClass_UGoogleVRTransition2DBPLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_GoogleVRTransition2D();
	GOOGLEVRTRANSITION2D_API UClass* Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UGoogleVRTransition2DBPLibrary::execTransitionToVR)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UGoogleVRTransition2DBPLibrary::TransitionToVR();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleVRTransition2DBPLibrary::execTransitionTo2D)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UGoogleVRTransition2DCallbackProxy**)Z_Param__Result=UGoogleVRTransition2DBPLibrary::TransitionTo2D();
		P_NATIVE_END;
	}
	void UGoogleVRTransition2DBPLibrary::StaticRegisterNativesUGoogleVRTransition2DBPLibrary()
	{
		UClass* Class = UGoogleVRTransition2DBPLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "TransitionTo2D", &UGoogleVRTransition2DBPLibrary::execTransitionTo2D },
			{ "TransitionToVR", &UGoogleVRTransition2DBPLibrary::execTransitionToVR },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics
	{
		struct GoogleVRTransition2DBPLibrary_eventTransitionTo2D_Parms
		{
			UGoogleVRTransition2DCallbackProxy* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleVRTransition2DBPLibrary_eventTransitionTo2D_Parms, ReturnValue), Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics::Function_MetaDataParams[] = {
		{ "Category", "Google VR" },
		{ "Comment", "//transition to 2D with the visual guidance. a black 2D screen will be displayed after transition\n//this function returns a singleton callback proxy object to handle OnTransition2D delegate\n" },
		{ "DisplayName", "Transition to 2D" },
		{ "ModuleRelativePath", "Public/GoogleVRTransition2DBPLibrary.h" },
		{ "ToolTip", "transition to 2D with the visual guidance. a black 2D screen will be displayed after transition\nthis function returns a singleton callback proxy object to handle OnTransition2D delegate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRTransition2DBPLibrary, nullptr, "TransitionTo2D", nullptr, nullptr, sizeof(GoogleVRTransition2DBPLibrary_eventTransitionTo2D_Parms), Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionToVR_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionToVR_Statics::Function_MetaDataParams[] = {
		{ "Category", "Google VR" },
		{ "Comment", "//transition back from 2D to VR. will see Back to VR button which make you resume to game by clicking it\n" },
		{ "DisplayName", "Transition back to VR" },
		{ "ModuleRelativePath", "Public/GoogleVRTransition2DBPLibrary.h" },
		{ "ToolTip", "transition back from 2D to VR. will see Back to VR button which make you resume to game by clicking it" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionToVR_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleVRTransition2DBPLibrary, nullptr, "TransitionToVR", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionToVR_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionToVR_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionToVR()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionToVR_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_NoRegister()
	{
		return UGoogleVRTransition2DBPLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRTransition2D,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionTo2D, "TransitionTo2D" }, // 3462089377
		{ &Z_Construct_UFunction_UGoogleVRTransition2DBPLibrary_TransitionToVR, "TransitionToVR" }, // 1879402130
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GoogleVRTransition2DBPLibrary.h" },
		{ "ModuleRelativePath", "Public/GoogleVRTransition2DBPLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleVRTransition2DBPLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_Statics::ClassParams = {
		&UGoogleVRTransition2DBPLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRTransition2DBPLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRTransition2DBPLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRTransition2DBPLibrary, 1576004924);
	template<> GOOGLEVRTRANSITION2D_API UClass* StaticClass<UGoogleVRTransition2DBPLibrary>()
	{
		return UGoogleVRTransition2DBPLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRTransition2DBPLibrary(Z_Construct_UClass_UGoogleVRTransition2DBPLibrary, &UGoogleVRTransition2DBPLibrary::StaticClass, TEXT("/Script/GoogleVRTransition2D"), TEXT("UGoogleVRTransition2DBPLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRTransition2DBPLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
