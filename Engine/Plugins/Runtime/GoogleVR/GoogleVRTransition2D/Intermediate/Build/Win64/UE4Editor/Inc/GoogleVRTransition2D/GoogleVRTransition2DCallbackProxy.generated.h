// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEVRTRANSITION2D_GoogleVRTransition2DCallbackProxy_generated_h
#error "GoogleVRTransition2DCallbackProxy.generated.h already included, missing '#pragma once' in GoogleVRTransition2DCallbackProxy.h"
#endif
#define GOOGLEVRTRANSITION2D_GoogleVRTransition2DCallbackProxy_generated_h

#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_12_DELEGATE \
static inline void FGoogleVRTransition2DDelegate_DelegateWrapper(const FMulticastScriptDelegate& GoogleVRTransition2DDelegate) \
{ \
	GoogleVRTransition2DDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_RPC_WRAPPERS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleVRTransition2DCallbackProxy(); \
	friend struct Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRTransition2DCallbackProxy, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleVRTransition2D"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRTransition2DCallbackProxy)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleVRTransition2DCallbackProxy(); \
	friend struct Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics; \
public: \
	DECLARE_CLASS(UGoogleVRTransition2DCallbackProxy, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleVRTransition2D"), NO_API) \
	DECLARE_SERIALIZER(UGoogleVRTransition2DCallbackProxy)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRTransition2DCallbackProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRTransition2DCallbackProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRTransition2DCallbackProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRTransition2DCallbackProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRTransition2DCallbackProxy(UGoogleVRTransition2DCallbackProxy&&); \
	NO_API UGoogleVRTransition2DCallbackProxy(const UGoogleVRTransition2DCallbackProxy&); \
public:


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleVRTransition2DCallbackProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleVRTransition2DCallbackProxy(UGoogleVRTransition2DCallbackProxy&&); \
	NO_API UGoogleVRTransition2DCallbackProxy(const UGoogleVRTransition2DCallbackProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleVRTransition2DCallbackProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleVRTransition2DCallbackProxy); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleVRTransition2DCallbackProxy)


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_14_PROLOG
#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_INCLASS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEVRTRANSITION2D_API UClass* StaticClass<class UGoogleVRTransition2DCallbackProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GoogleVR_GoogleVRTransition2D_Source_GoogleVRTransition2D_Classes_GoogleVRTransition2DCallbackProxy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
