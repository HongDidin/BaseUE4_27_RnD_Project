// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleVRTransition2D/Classes/GoogleVRTransition2DCallbackProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleVRTransition2DCallbackProxy() {}
// Cross Module References
	GOOGLEVRTRANSITION2D_API UFunction* Z_Construct_UDelegateFunction_GoogleVRTransition2D_GoogleVRTransition2DDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_GoogleVRTransition2D();
	GOOGLEVRTRANSITION2D_API UClass* Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_NoRegister();
	GOOGLEVRTRANSITION2D_API UClass* Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_GoogleVRTransition2D_GoogleVRTransition2DDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_GoogleVRTransition2D_GoogleVRTransition2DDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/GoogleVRTransition2DCallbackProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_GoogleVRTransition2D_GoogleVRTransition2DDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_GoogleVRTransition2D, nullptr, "GoogleVRTransition2DDelegate__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_GoogleVRTransition2D_GoogleVRTransition2DDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_GoogleVRTransition2D_GoogleVRTransition2DDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_GoogleVRTransition2D_GoogleVRTransition2DDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_GoogleVRTransition2D_GoogleVRTransition2DDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UGoogleVRTransition2DCallbackProxy::StaticRegisterNativesUGoogleVRTransition2DCallbackProxy()
	{
	}
	UClass* Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_NoRegister()
	{
		return UGoogleVRTransition2DCallbackProxy::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnTransitionTo2D_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnTransitionTo2D;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleVRTransition2D,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GoogleVRTransition2DCallbackProxy.h" },
		{ "ModuleRelativePath", "Classes/GoogleVRTransition2DCallbackProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::NewProp_OnTransitionTo2D_MetaData[] = {
		{ "Category", "Google VR" },
		{ "Comment", "// delegate to handle the completion of the 2D transition\n" },
		{ "ModuleRelativePath", "Classes/GoogleVRTransition2DCallbackProxy.h" },
		{ "ToolTip", "delegate to handle the completion of the 2D transition" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::NewProp_OnTransitionTo2D = { "OnTransitionTo2D", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleVRTransition2DCallbackProxy, OnTransitionTo2D), Z_Construct_UDelegateFunction_GoogleVRTransition2D_GoogleVRTransition2DDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::NewProp_OnTransitionTo2D_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::NewProp_OnTransitionTo2D_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::NewProp_OnTransitionTo2D,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleVRTransition2DCallbackProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::ClassParams = {
		&UGoogleVRTransition2DCallbackProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleVRTransition2DCallbackProxy, 4222762228);
	template<> GOOGLEVRTRANSITION2D_API UClass* StaticClass<UGoogleVRTransition2DCallbackProxy>()
	{
		return UGoogleVRTransition2DCallbackProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleVRTransition2DCallbackProxy(Z_Construct_UClass_UGoogleVRTransition2DCallbackProxy, &UGoogleVRTransition2DCallbackProxy::StaticClass, TEXT("/Script/GoogleVRTransition2D"), TEXT("UGoogleVRTransition2DCallbackProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleVRTransition2DCallbackProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
