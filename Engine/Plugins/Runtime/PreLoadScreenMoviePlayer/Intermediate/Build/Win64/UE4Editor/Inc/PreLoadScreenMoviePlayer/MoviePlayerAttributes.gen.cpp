// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PreLoadScreenMoviePlayer/Public/MoviePlayerAttributes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePlayerAttributes() {}
// Cross Module References
	PRELOADSCREENMOVIEPLAYER_API UEnum* Z_Construct_UEnum_PreLoadScreenMoviePlayer_EMovieScreenPlaybackType();
	UPackage* Z_Construct_UPackage__Script_PreLoadScreenMoviePlayer();
// End Cross Module References
	static UEnum* EMovieScreenPlaybackType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PreLoadScreenMoviePlayer_EMovieScreenPlaybackType, Z_Construct_UPackage__Script_PreLoadScreenMoviePlayer(), TEXT("EMovieScreenPlaybackType"));
		}
		return Singleton;
	}
	template<> PRELOADSCREENMOVIEPLAYER_API UEnum* StaticEnum<EMovieScreenPlaybackType>()
	{
		return EMovieScreenPlaybackType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMovieScreenPlaybackType(EMovieScreenPlaybackType_StaticEnum, TEXT("/Script/PreLoadScreenMoviePlayer"), TEXT("EMovieScreenPlaybackType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PreLoadScreenMoviePlayer_EMovieScreenPlaybackType_Hash() { return 3024472039U; }
	UEnum* Z_Construct_UEnum_PreLoadScreenMoviePlayer_EMovieScreenPlaybackType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PreLoadScreenMoviePlayer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMovieScreenPlaybackType"), 0, Get_Z_Construct_UEnum_PreLoadScreenMoviePlayer_EMovieScreenPlaybackType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "MT_MS_Normal", (int64)MT_MS_Normal },
				{ "MT_MS_Looped", (int64)MT_MS_Looped },
				{ "MT_MS_LoadingLoop", (int64)MT_MS_LoadingLoop },
				{ "MT_MS_MAX", (int64)MT_MS_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/MoviePlayerAttributes.h" },
				{ "MT_MS_LoadingLoop.Comment", "/** Alternate Looped mode.  Play all of the movies in the play list and loop just the last movie until loading is finished. */" },
				{ "MT_MS_LoadingLoop.DisplayName", "Looped Last Playback" },
				{ "MT_MS_LoadingLoop.Name", "MT_MS_LoadingLoop" },
				{ "MT_MS_LoadingLoop.ToolTip", "Alternate Looped mode.  Play all of the movies in the play list and loop just the last movie until loading is finished." },
				{ "MT_MS_Looped.Comment", "/** Looped playback mode.  Play all movies in the play list in order then start over until manually canceled */" },
				{ "MT_MS_Looped.DisplayName", "Looped Playback" },
				{ "MT_MS_Looped.Name", "MT_MS_Looped" },
				{ "MT_MS_Looped.ToolTip", "Looped playback mode.  Play all movies in the play list in order then start over until manually canceled" },
				{ "MT_MS_MAX.Hidden", "" },
				{ "MT_MS_MAX.Name", "MT_MS_MAX" },
				{ "MT_MS_Normal.Comment", "/** Normal playback mode.  Play each movie in the play list a single time */" },
				{ "MT_MS_Normal.DisplayName", "Normal Playback" },
				{ "MT_MS_Normal.Name", "MT_MS_Normal" },
				{ "MT_MS_Normal.ToolTip", "Normal playback mode.  Play each movie in the play list a single time" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PreLoadScreenMoviePlayer,
				nullptr,
				"EMovieScreenPlaybackType",
				"EMovieScreenPlaybackType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Regular,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
