// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoundModImporter/Classes/SoundModImporterFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundModImporterFactory() {}
// Cross Module References
	SOUNDMODIMPORTER_API UClass* Z_Construct_UClass_USoundModImporterFactory_NoRegister();
	SOUNDMODIMPORTER_API UClass* Z_Construct_UClass_USoundModImporterFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_SoundModImporter();
// End Cross Module References
	void USoundModImporterFactory::StaticRegisterNativesUSoundModImporterFactory()
	{
	}
	UClass* Z_Construct_UClass_USoundModImporterFactory_NoRegister()
	{
		return USoundModImporterFactory::StaticClass();
	}
	struct Z_Construct_UClass_USoundModImporterFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModImporterFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundModImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModImporterFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Imports a sound module file\n" },
		{ "IncludePath", "SoundModImporterFactory.h" },
		{ "ModuleRelativePath", "Classes/SoundModImporterFactory.h" },
		{ "ToolTip", "Imports a sound module file" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModImporterFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModImporterFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModImporterFactory_Statics::ClassParams = {
		&USoundModImporterFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModImporterFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModImporterFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModImporterFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModImporterFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModImporterFactory, 2622525412);
	template<> SOUNDMODIMPORTER_API UClass* StaticClass<USoundModImporterFactory>()
	{
		return USoundModImporterFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModImporterFactory(Z_Construct_UClass_USoundModImporterFactory, &USoundModImporterFactory::StaticClass, TEXT("/Script/SoundModImporter"), TEXT("USoundModImporterFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModImporterFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
