// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoundMod/Public/SoundNodeModPlayer.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundNodeModPlayer() {}
// Cross Module References
	SOUNDMOD_API UClass* Z_Construct_UClass_USoundNodeModPlayer_NoRegister();
	SOUNDMOD_API UClass* Z_Construct_UClass_USoundNodeModPlayer();
	ENGINE_API UClass* Z_Construct_UClass_USoundNodeAssetReferencer();
	UPackage* Z_Construct_UPackage__Script_SoundMod();
	SOUNDMOD_API UClass* Z_Construct_UClass_USoundMod_NoRegister();
// End Cross Module References
	void USoundNodeModPlayer::StaticRegisterNativesUSoundNodeModPlayer()
	{
	}
	UClass* Z_Construct_UClass_USoundNodeModPlayer_NoRegister()
	{
		return USoundNodeModPlayer::StaticClass();
	}
	struct Z_Construct_UClass_USoundNodeModPlayer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoundModAssetPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SoundModAssetPtr;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoundMod_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SoundMod;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLooping_MetaData[];
#endif
		static void NewProp_bLooping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLooping;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundNodeModPlayer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundNodeAssetReferencer,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundMod,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundNodeModPlayer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * Sound node that contains a reference to the mod file to be played\n */" },
		{ "DisplayName", "Mod Player" },
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "SoundNodeModPlayer.h" },
		{ "ModuleRelativePath", "Public/SoundNodeModPlayer.h" },
		{ "ToolTip", "Sound node that contains a reference to the mod file to be played" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_SoundModAssetPtr_MetaData[] = {
		{ "Category", "ModPlayer" },
		{ "DisplayName", "Sound Mod" },
		{ "ModuleRelativePath", "Public/SoundNodeModPlayer.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_SoundModAssetPtr = { "SoundModAssetPtr", nullptr, (EPropertyFlags)0x0044000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundNodeModPlayer, SoundModAssetPtr), Z_Construct_UClass_USoundMod_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_SoundModAssetPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_SoundModAssetPtr_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_SoundMod_MetaData[] = {
		{ "ModuleRelativePath", "Public/SoundNodeModPlayer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_SoundMod = { "SoundMod", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundNodeModPlayer, SoundMod), Z_Construct_UClass_USoundMod_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_SoundMod_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_SoundMod_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_bLooping_MetaData[] = {
		{ "Category", "ModPlayer" },
		{ "ModuleRelativePath", "Public/SoundNodeModPlayer.h" },
	};
#endif
	void Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_bLooping_SetBit(void* Obj)
	{
		((USoundNodeModPlayer*)Obj)->bLooping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_bLooping = { "bLooping", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(USoundNodeModPlayer), &Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_bLooping_SetBit, METADATA_PARAMS(Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_bLooping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_bLooping_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundNodeModPlayer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_SoundModAssetPtr,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_SoundMod,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundNodeModPlayer_Statics::NewProp_bLooping,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundNodeModPlayer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundNodeModPlayer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundNodeModPlayer_Statics::ClassParams = {
		&USoundNodeModPlayer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundNodeModPlayer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundNodeModPlayer_Statics::PropPointers),
		0,
		0x000810A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundNodeModPlayer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundNodeModPlayer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundNodeModPlayer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundNodeModPlayer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundNodeModPlayer, 4041594784);
	template<> SOUNDMOD_API UClass* StaticClass<USoundNodeModPlayer>()
	{
		return USoundNodeModPlayer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundNodeModPlayer(Z_Construct_UClass_USoundNodeModPlayer, &USoundNodeModPlayer::StaticClass, TEXT("/Script/SoundMod"), TEXT("USoundNodeModPlayer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundNodeModPlayer);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(USoundNodeModPlayer)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
