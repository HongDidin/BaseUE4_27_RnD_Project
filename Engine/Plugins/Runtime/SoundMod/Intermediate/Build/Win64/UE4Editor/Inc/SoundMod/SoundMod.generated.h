// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOUNDMOD_SoundMod_generated_h
#error "SoundMod.generated.h already included, missing '#pragma once' in SoundMod.h"
#endif
#define SOUNDMOD_SoundMod_generated_h

#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_SPARSE_DATA
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_RPC_WRAPPERS
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(USoundMod, SOUNDMOD_API)


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundMod(); \
	friend struct Z_Construct_UClass_USoundMod_Statics; \
public: \
	DECLARE_CLASS(USoundMod, USoundBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundMod"), SOUNDMOD_API) \
	DECLARE_SERIALIZER(USoundMod) \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUSoundMod(); \
	friend struct Z_Construct_UClass_USoundMod_Statics; \
public: \
	DECLARE_CLASS(USoundMod, USoundBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundMod"), SOUNDMOD_API) \
	DECLARE_SERIALIZER(USoundMod) \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SOUNDMOD_API USoundMod(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundMod) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SOUNDMOD_API, USoundMod); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundMod); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SOUNDMOD_API USoundMod(USoundMod&&); \
	SOUNDMOD_API USoundMod(const USoundMod&); \
public:


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SOUNDMOD_API USoundMod(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SOUNDMOD_API USoundMod(USoundMod&&); \
	SOUNDMOD_API USoundMod(const USoundMod&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SOUNDMOD_API, USoundMod); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundMod); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundMod)


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_18_PROLOG
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_INCLASS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h_21_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundMod."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOUNDMOD_API UClass* StaticClass<class USoundMod>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundMod_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
