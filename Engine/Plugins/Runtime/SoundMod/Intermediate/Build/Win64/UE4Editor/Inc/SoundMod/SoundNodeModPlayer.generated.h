// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOUNDMOD_SoundNodeModPlayer_generated_h
#error "SoundNodeModPlayer.generated.h already included, missing '#pragma once' in SoundNodeModPlayer.h"
#endif
#define SOUNDMOD_SoundNodeModPlayer_generated_h

#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_SPARSE_DATA
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_RPC_WRAPPERS
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(USoundNodeModPlayer, SOUNDMOD_API)


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundNodeModPlayer(); \
	friend struct Z_Construct_UClass_USoundNodeModPlayer_Statics; \
public: \
	DECLARE_CLASS(USoundNodeModPlayer, USoundNodeAssetReferencer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundMod"), SOUNDMOD_API) \
	DECLARE_SERIALIZER(USoundNodeModPlayer) \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUSoundNodeModPlayer(); \
	friend struct Z_Construct_UClass_USoundNodeModPlayer_Statics; \
public: \
	DECLARE_CLASS(USoundNodeModPlayer, USoundNodeAssetReferencer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundMod"), SOUNDMOD_API) \
	DECLARE_SERIALIZER(USoundNodeModPlayer) \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SOUNDMOD_API USoundNodeModPlayer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundNodeModPlayer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SOUNDMOD_API, USoundNodeModPlayer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundNodeModPlayer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SOUNDMOD_API USoundNodeModPlayer(USoundNodeModPlayer&&); \
	SOUNDMOD_API USoundNodeModPlayer(const USoundNodeModPlayer&); \
public:


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SOUNDMOD_API USoundNodeModPlayer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SOUNDMOD_API USoundNodeModPlayer(USoundNodeModPlayer&&); \
	SOUNDMOD_API USoundNodeModPlayer(const USoundNodeModPlayer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SOUNDMOD_API, USoundNodeModPlayer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundNodeModPlayer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundNodeModPlayer)


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SoundModAssetPtr() { return STRUCT_OFFSET(USoundNodeModPlayer, SoundModAssetPtr); } \
	FORCEINLINE static uint32 __PPO__SoundMod() { return STRUCT_OFFSET(USoundNodeModPlayer, SoundMod); }


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_22_PROLOG
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_INCLASS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOUNDMOD_API UClass* StaticClass<class USoundNodeModPlayer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Public_SoundNodeModPlayer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
