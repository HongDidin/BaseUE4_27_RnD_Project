// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoundMod/Classes/SoundMod.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundMod() {}
// Cross Module References
	SOUNDMOD_API UClass* Z_Construct_UClass_USoundMod_NoRegister();
	SOUNDMOD_API UClass* Z_Construct_UClass_USoundMod();
	ENGINE_API UClass* Z_Construct_UClass_USoundBase();
	UPackage* Z_Construct_UPackage__Script_SoundMod();
// End Cross Module References
	void USoundMod::StaticRegisterNativesUSoundMod()
	{
	}
	UClass* Z_Construct_UClass_USoundMod_NoRegister()
	{
		return USoundMod::StaticClass();
	}
	struct Z_Construct_UClass_USoundMod_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLooping_MetaData[];
#endif
		static void NewProp_bLooping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLooping;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundMod_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundMod,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundMod_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "SoundMod.h" },
		{ "ModuleRelativePath", "Classes/SoundMod.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundMod_Statics::NewProp_bLooping_MetaData[] = {
		{ "Category", "SoundMod" },
		{ "Comment", "/** If set, when played directly (not through a sound cue) the nid will be played looping. */" },
		{ "ModuleRelativePath", "Classes/SoundMod.h" },
		{ "ToolTip", "If set, when played directly (not through a sound cue) the nid will be played looping." },
	};
#endif
	void Z_Construct_UClass_USoundMod_Statics::NewProp_bLooping_SetBit(void* Obj)
	{
		((USoundMod*)Obj)->bLooping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USoundMod_Statics::NewProp_bLooping = { "bLooping", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(USoundMod), &Z_Construct_UClass_USoundMod_Statics::NewProp_bLooping_SetBit, METADATA_PARAMS(Z_Construct_UClass_USoundMod_Statics::NewProp_bLooping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundMod_Statics::NewProp_bLooping_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundMod_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundMod_Statics::NewProp_bLooping,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundMod_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundMod>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundMod_Statics::ClassParams = {
		&USoundMod::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundMod_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundMod_Statics::PropPointers),
		0,
		0x008810A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundMod_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundMod_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundMod()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundMod_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundMod, 3499146561);
	template<> SOUNDMOD_API UClass* StaticClass<USoundMod>()
	{
		return USoundMod::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundMod(Z_Construct_UClass_USoundMod, &USoundMod::StaticClass, TEXT("/Script/SoundMod"), TEXT("USoundMod"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundMod);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(USoundMod)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
