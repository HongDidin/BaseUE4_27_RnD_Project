// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOUNDMOD_SoundModWave_generated_h
#error "SoundModWave.generated.h already included, missing '#pragma once' in SoundModWave.h"
#endif
#define SOUNDMOD_SoundModWave_generated_h

#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_SPARSE_DATA
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_RPC_WRAPPERS
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundModWave(); \
	friend struct Z_Construct_UClass_USoundModWave_Statics; \
public: \
	DECLARE_CLASS(USoundModWave, USoundWaveProcedural, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundMod"), NO_API) \
	DECLARE_SERIALIZER(USoundModWave)


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUSoundModWave(); \
	friend struct Z_Construct_UClass_USoundModWave_Statics; \
public: \
	DECLARE_CLASS(USoundModWave, USoundWaveProcedural, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundMod"), NO_API) \
	DECLARE_SERIALIZER(USoundModWave)


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModWave(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModWave) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModWave); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModWave); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModWave(USoundModWave&&); \
	NO_API USoundModWave(const USoundModWave&); \
public:


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModWave(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModWave(USoundModWave&&); \
	NO_API USoundModWave(const USoundModWave&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModWave); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModWave); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModWave)


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_15_PROLOG
#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_INCLASS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundModWave."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOUNDMOD_API UClass* StaticClass<class USoundModWave>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SoundMod_Source_SoundMod_Classes_SoundModWave_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
