// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeoReferencing/Public/GeographicCoordinates.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeographicCoordinates() {}
// Cross Module References
	GEOREFERENCING_API UScriptStruct* Z_Construct_UScriptStruct_FGeographicCoordinates();
	UPackage* Z_Construct_UPackage__Script_GeoReferencing();
	GEOREFERENCING_API UClass* Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_NoRegister();
	GEOREFERENCING_API UClass* Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
// End Cross Module References
class UScriptStruct* FGeographicCoordinates::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern GEOREFERENCING_API uint32 Get_Z_Construct_UScriptStruct_FGeographicCoordinates_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGeographicCoordinates, Z_Construct_UPackage__Script_GeoReferencing(), TEXT("GeographicCoordinates"), sizeof(FGeographicCoordinates), Get_Z_Construct_UScriptStruct_FGeographicCoordinates_Hash());
	}
	return Singleton;
}
template<> GEOREFERENCING_API UScriptStruct* StaticStruct<FGeographicCoordinates>()
{
	return FGeographicCoordinates::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGeographicCoordinates(FGeographicCoordinates::StaticStruct, TEXT("/Script/GeoReferencing"), TEXT("GeographicCoordinates"), false, nullptr, nullptr);
static struct FScriptStruct_GeoReferencing_StaticRegisterNativesFGeographicCoordinates
{
	FScriptStruct_GeoReferencing_StaticRegisterNativesFGeographicCoordinates()
	{
		UScriptStruct::DeferCppStructOps<FGeographicCoordinates>(FName(TEXT("GeographicCoordinates")));
	}
} ScriptStruct_GeoReferencing_StaticRegisterNativesFGeographicCoordinates;
	struct Z_Construct_UScriptStruct_FGeographicCoordinates_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGeographicCoordinates_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GeographicCoordinates.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGeographicCoordinates_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGeographicCoordinates>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGeographicCoordinates_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_GeoReferencing,
		nullptr,
		&NewStructOps,
		"GeographicCoordinates",
		sizeof(FGeographicCoordinates),
		alignof(FGeographicCoordinates),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGeographicCoordinates_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGeographicCoordinates_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGeographicCoordinates()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGeographicCoordinates_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_GeoReferencing();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GeographicCoordinates"), sizeof(FGeographicCoordinates), Get_Z_Construct_UScriptStruct_FGeographicCoordinates_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGeographicCoordinates_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGeographicCoordinates_Hash() { return 1342161244U; }
	DEFINE_FUNCTION(UGeographicCoordinatesFunctionLibrary::execMakeGeographicCoordinatesApproximation)
	{
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_InLatitude);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_InLongitude);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_InAltitude);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGeographicCoordinates*)Z_Param__Result=UGeographicCoordinatesFunctionLibrary::MakeGeographicCoordinatesApproximation(Z_Param_Out_InLatitude,Z_Param_Out_InLongitude,Z_Param_Out_InAltitude);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGeographicCoordinatesFunctionLibrary::execToFloatApproximation)
	{
		P_GET_STRUCT_REF(FGeographicCoordinates,Z_Param_Out_GeographicCoordinates);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_OutLatitude);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_OutLongitude);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_OutAltitude);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGeographicCoordinatesFunctionLibrary::ToFloatApproximation(Z_Param_Out_GeographicCoordinates,Z_Param_Out_OutLatitude,Z_Param_Out_OutLongitude,Z_Param_Out_OutAltitude);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGeographicCoordinatesFunctionLibrary::execToSeparateTexts)
	{
		P_GET_STRUCT_REF(FGeographicCoordinates,Z_Param_Out_GeographicCoordinates);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutLatitude);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutLongitude);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutAltitude);
		P_GET_PROPERTY(FIntProperty,Z_Param_IntegralDigitsLatLon);
		P_GET_PROPERTY(FIntProperty,Z_Param_IntegralDigitsAlti);
		P_GET_UBOOL(Z_Param_bAsDMS);
		P_FINISH;
		P_NATIVE_BEGIN;
		UGeographicCoordinatesFunctionLibrary::ToSeparateTexts(Z_Param_Out_GeographicCoordinates,Z_Param_Out_OutLatitude,Z_Param_Out_OutLongitude,Z_Param_Out_OutAltitude,Z_Param_IntegralDigitsLatLon,Z_Param_IntegralDigitsAlti,Z_Param_bAsDMS);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGeographicCoordinatesFunctionLibrary::execToCompactText)
	{
		P_GET_STRUCT_REF(FGeographicCoordinates,Z_Param_Out_GeographicCoordinates);
		P_GET_PROPERTY(FIntProperty,Z_Param_IntegralDigitsLatLon);
		P_GET_PROPERTY(FIntProperty,Z_Param_IntegralDigitsAlti);
		P_GET_UBOOL(Z_Param_bAsDMS);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=UGeographicCoordinatesFunctionLibrary::ToCompactText(Z_Param_Out_GeographicCoordinates,Z_Param_IntegralDigitsLatLon,Z_Param_IntegralDigitsAlti,Z_Param_bAsDMS);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGeographicCoordinatesFunctionLibrary::execToFullText)
	{
		P_GET_STRUCT_REF(FGeographicCoordinates,Z_Param_Out_GeographicCoordinates);
		P_GET_PROPERTY(FIntProperty,Z_Param_IntegralDigitsLatLon);
		P_GET_PROPERTY(FIntProperty,Z_Param_IntegralDigitsAlti);
		P_GET_UBOOL(Z_Param_bAsDMS);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=UGeographicCoordinatesFunctionLibrary::ToFullText(Z_Param_Out_GeographicCoordinates,Z_Param_IntegralDigitsLatLon,Z_Param_IntegralDigitsAlti,Z_Param_bAsDMS);
		P_NATIVE_END;
	}
	void UGeographicCoordinatesFunctionLibrary::StaticRegisterNativesUGeographicCoordinatesFunctionLibrary()
	{
		UClass* Class = UGeographicCoordinatesFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "MakeGeographicCoordinatesApproximation", &UGeographicCoordinatesFunctionLibrary::execMakeGeographicCoordinatesApproximation },
			{ "ToCompactText", &UGeographicCoordinatesFunctionLibrary::execToCompactText },
			{ "ToFloatApproximation", &UGeographicCoordinatesFunctionLibrary::execToFloatApproximation },
			{ "ToFullText", &UGeographicCoordinatesFunctionLibrary::execToFullText },
			{ "ToSeparateTexts", &UGeographicCoordinatesFunctionLibrary::execToSeparateTexts },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics
	{
		struct GeographicCoordinatesFunctionLibrary_eventMakeGeographicCoordinatesApproximation_Parms
		{
			float InLatitude;
			float InLongitude;
			float InAltitude;
			FGeographicCoordinates ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InLatitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InLatitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InLongitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InLongitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InAltitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InAltitude;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InLatitude_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InLatitude = { "InLatitude", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventMakeGeographicCoordinatesApproximation_Parms, InLatitude), METADATA_PARAMS(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InLatitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InLatitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InLongitude_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InLongitude = { "InLongitude", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventMakeGeographicCoordinatesApproximation_Parms, InLongitude), METADATA_PARAMS(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InLongitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InLongitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InAltitude_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InAltitude = { "InAltitude", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventMakeGeographicCoordinatesApproximation_Parms, InAltitude), METADATA_PARAMS(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InAltitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InAltitude_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventMakeGeographicCoordinatesApproximation_Parms, ReturnValue), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InLatitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InLongitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_InAltitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Coordinates" },
		{ "Comment", "/**\n\x09 * Set the Coordinates from float approximation.\n\x09 * USE WISELY as we can't guarantee there will no be rounding due to IEEE754 float encoding !\n\x09 **/" },
		{ "ModuleRelativePath", "Public/GeographicCoordinates.h" },
		{ "ToolTip", "Set the Coordinates from float approximation.\nUSE WISELY as we can't guarantee there will no be rounding due to IEEE754 float encoding !" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary, nullptr, "MakeGeographicCoordinatesApproximation", nullptr, nullptr, sizeof(GeographicCoordinatesFunctionLibrary_eventMakeGeographicCoordinatesApproximation_Parms), Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics
	{
		struct GeographicCoordinatesFunctionLibrary_eventToCompactText_Parms
		{
			FGeographicCoordinates GeographicCoordinates;
			int32 IntegralDigitsLatLon;
			int32 IntegralDigitsAlti;
			bool bAsDMS;
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeographicCoordinates;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegralDigitsLatLon;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegralDigitsAlti;
		static void NewProp_bAsDMS_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAsDMS;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_GeographicCoordinates = { "GeographicCoordinates", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToCompactText_Parms, GeographicCoordinates), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_IntegralDigitsLatLon = { "IntegralDigitsLatLon", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToCompactText_Parms, IntegralDigitsLatLon), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_IntegralDigitsAlti = { "IntegralDigitsAlti", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToCompactText_Parms, IntegralDigitsAlti), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_bAsDMS_SetBit(void* Obj)
	{
		((GeographicCoordinatesFunctionLibrary_eventToCompactText_Parms*)Obj)->bAsDMS = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_bAsDMS = { "bAsDMS", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GeographicCoordinatesFunctionLibrary_eventToCompactText_Parms), &Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_bAsDMS_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToCompactText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_GeographicCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_IntegralDigitsLatLon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_IntegralDigitsAlti,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_bAsDMS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "1" },
		{ "BlueprintAutocast", "" },
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * Converts a GeographicCoordinates value to formatted text, in the form '(X, Y, Z)'\n\x09 **/" },
		{ "CPP_Default_bAsDMS", "false" },
		{ "CPP_Default_IntegralDigitsAlti", "2" },
		{ "CPP_Default_IntegralDigitsLatLon", "8" },
		{ "DisplayName", "ToCompactText" },
		{ "ModuleRelativePath", "Public/GeographicCoordinates.h" },
		{ "ToolTip", "Converts a GeographicCoordinates value to formatted text, in the form '(X, Y, Z)'" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary, nullptr, "ToCompactText", nullptr, nullptr, sizeof(GeographicCoordinatesFunctionLibrary_eventToCompactText_Parms), Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics
	{
		struct GeographicCoordinatesFunctionLibrary_eventToFloatApproximation_Parms
		{
			FGeographicCoordinates GeographicCoordinates;
			float OutLatitude;
			float OutLongitude;
			float OutAltitude;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeographicCoordinates;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutLatitude;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutLongitude;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutAltitude;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_GeographicCoordinates = { "GeographicCoordinates", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToFloatApproximation_Parms, GeographicCoordinates), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutLatitude = { "OutLatitude", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToFloatApproximation_Parms, OutLatitude), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutLongitude = { "OutLongitude", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToFloatApproximation_Parms, OutLongitude), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutAltitude = { "OutAltitude", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToFloatApproximation_Parms, OutAltitude), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_GeographicCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutLatitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutLongitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutAltitude,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * Get the Coordinates as a float approximation.\n\x09 * USE WISELY as we can't guarantee there will no be rounding due to IEEE754 float encoding !\n\x09 **/" },
		{ "ModuleRelativePath", "Public/GeographicCoordinates.h" },
		{ "ToolTip", "Get the Coordinates as a float approximation.\nUSE WISELY as we can't guarantee there will no be rounding due to IEEE754 float encoding !" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary, nullptr, "ToFloatApproximation", nullptr, nullptr, sizeof(GeographicCoordinatesFunctionLibrary_eventToFloatApproximation_Parms), Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics
	{
		struct GeographicCoordinatesFunctionLibrary_eventToFullText_Parms
		{
			FGeographicCoordinates GeographicCoordinates;
			int32 IntegralDigitsLatLon;
			int32 IntegralDigitsAlti;
			bool bAsDMS;
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeographicCoordinates;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegralDigitsLatLon;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegralDigitsAlti;
		static void NewProp_bAsDMS_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAsDMS;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_GeographicCoordinates = { "GeographicCoordinates", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToFullText_Parms, GeographicCoordinates), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_IntegralDigitsLatLon = { "IntegralDigitsLatLon", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToFullText_Parms, IntegralDigitsLatLon), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_IntegralDigitsAlti = { "IntegralDigitsAlti", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToFullText_Parms, IntegralDigitsAlti), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_bAsDMS_SetBit(void* Obj)
	{
		((GeographicCoordinatesFunctionLibrary_eventToFullText_Parms*)Obj)->bAsDMS = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_bAsDMS = { "bAsDMS", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GeographicCoordinatesFunctionLibrary_eventToFullText_Parms), &Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_bAsDMS_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToFullText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_GeographicCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_IntegralDigitsLatLon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_IntegralDigitsAlti,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_bAsDMS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "1" },
		{ "BlueprintAutocast", "" },
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * Converts a GeographicCoordinates value to localized formatted text, in the form 'X= Y= Z='\n\x09 **/" },
		{ "CPP_Default_bAsDMS", "false" },
		{ "CPP_Default_IntegralDigitsAlti", "2" },
		{ "CPP_Default_IntegralDigitsLatLon", "8" },
		{ "DisplayName", "ToFullText" },
		{ "ModuleRelativePath", "Public/GeographicCoordinates.h" },
		{ "ToolTip", "Converts a GeographicCoordinates value to localized formatted text, in the form 'X= Y= Z='" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary, nullptr, "ToFullText", nullptr, nullptr, sizeof(GeographicCoordinatesFunctionLibrary_eventToFullText_Parms), Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics
	{
		struct GeographicCoordinatesFunctionLibrary_eventToSeparateTexts_Parms
		{
			FGeographicCoordinates GeographicCoordinates;
			FText OutLatitude;
			FText OutLongitude;
			FText OutAltitude;
			int32 IntegralDigitsLatLon;
			int32 IntegralDigitsAlti;
			bool bAsDMS;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeographicCoordinates;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutLatitude;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutLongitude;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutAltitude;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegralDigitsLatLon;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegralDigitsAlti;
		static void NewProp_bAsDMS_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAsDMS;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_GeographicCoordinates = { "GeographicCoordinates", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, GeographicCoordinates), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutLatitude = { "OutLatitude", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, OutLatitude), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutLongitude = { "OutLongitude", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, OutLongitude), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutAltitude = { "OutAltitude", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, OutAltitude), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_IntegralDigitsLatLon = { "IntegralDigitsLatLon", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, IntegralDigitsLatLon), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_IntegralDigitsAlti = { "IntegralDigitsAlti", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeographicCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, IntegralDigitsAlti), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_bAsDMS_SetBit(void* Obj)
	{
		((GeographicCoordinatesFunctionLibrary_eventToSeparateTexts_Parms*)Obj)->bAsDMS = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_bAsDMS = { "bAsDMS", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GeographicCoordinatesFunctionLibrary_eventToSeparateTexts_Parms), &Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_bAsDMS_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_GeographicCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutLatitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutLongitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutAltitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_IntegralDigitsLatLon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_IntegralDigitsAlti,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_bAsDMS,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "4" },
		{ "BlueprintAutocast", "" },
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * Converts a GeographicCoordinates value to 3 separate text values\n\x09 **/" },
		{ "CPP_Default_bAsDMS", "false" },
		{ "CPP_Default_IntegralDigitsAlti", "2" },
		{ "CPP_Default_IntegralDigitsLatLon", "8" },
		{ "DisplayName", "ToSeparateTexts" },
		{ "ModuleRelativePath", "Public/GeographicCoordinates.h" },
		{ "ToolTip", "Converts a GeographicCoordinates value to 3 separate text values" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary, nullptr, "ToSeparateTexts", nullptr, nullptr, sizeof(GeographicCoordinatesFunctionLibrary_eventToSeparateTexts_Parms), Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_NoRegister()
	{
		return UGeographicCoordinatesFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_GeoReferencing,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_MakeGeographicCoordinatesApproximation, "MakeGeographicCoordinatesApproximation" }, // 532661961
		{ &Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToCompactText, "ToCompactText" }, // 646884622
		{ &Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFloatApproximation, "ToFloatApproximation" }, // 1608445721
		{ &Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToFullText, "ToFullText" }, // 446645565
		{ &Z_Construct_UFunction_UGeographicCoordinatesFunctionLibrary_ToSeparateTexts, "ToSeparateTexts" }, // 301728752
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GeographicCoordinates.h" },
		{ "ModuleRelativePath", "Public/GeographicCoordinates.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGeographicCoordinatesFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_Statics::ClassParams = {
		&UGeographicCoordinatesFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGeographicCoordinatesFunctionLibrary, 3913203955);
	template<> GEOREFERENCING_API UClass* StaticClass<UGeographicCoordinatesFunctionLibrary>()
	{
		return UGeographicCoordinatesFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGeographicCoordinatesFunctionLibrary(Z_Construct_UClass_UGeographicCoordinatesFunctionLibrary, &UGeographicCoordinatesFunctionLibrary::StaticClass, TEXT("/Script/GeoReferencing"), TEXT("UGeographicCoordinatesFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGeographicCoordinatesFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
