// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeoReferencing/Public/Ellipsoid.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEllipsoid() {}
// Cross Module References
	GEOREFERENCING_API UScriptStruct* Z_Construct_UScriptStruct_FEllipsoid();
	UPackage* Z_Construct_UPackage__Script_GeoReferencing();
// End Cross Module References
class UScriptStruct* FEllipsoid::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern GEOREFERENCING_API uint32 Get_Z_Construct_UScriptStruct_FEllipsoid_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEllipsoid, Z_Construct_UPackage__Script_GeoReferencing(), TEXT("Ellipsoid"), sizeof(FEllipsoid), Get_Z_Construct_UScriptStruct_FEllipsoid_Hash());
	}
	return Singleton;
}
template<> GEOREFERENCING_API UScriptStruct* StaticStruct<FEllipsoid>()
{
	return FEllipsoid::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEllipsoid(FEllipsoid::StaticStruct, TEXT("/Script/GeoReferencing"), TEXT("Ellipsoid"), false, nullptr, nullptr);
static struct FScriptStruct_GeoReferencing_StaticRegisterNativesFEllipsoid
{
	FScriptStruct_GeoReferencing_StaticRegisterNativesFEllipsoid()
	{
		UScriptStruct::DeferCppStructOps<FEllipsoid>(FName(TEXT("Ellipsoid")));
	}
} ScriptStruct_GeoReferencing_StaticRegisterNativesFEllipsoid;
	struct Z_Construct_UScriptStruct_FEllipsoid_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEllipsoid_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Ellipsoid.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEllipsoid_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEllipsoid>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEllipsoid_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_GeoReferencing,
		nullptr,
		&NewStructOps,
		"Ellipsoid",
		sizeof(FEllipsoid),
		alignof(FEllipsoid),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEllipsoid_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEllipsoid_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEllipsoid()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEllipsoid_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_GeoReferencing();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("Ellipsoid"), sizeof(FEllipsoid), Get_Z_Construct_UScriptStruct_FEllipsoid_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEllipsoid_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEllipsoid_Hash() { return 3407156242U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
