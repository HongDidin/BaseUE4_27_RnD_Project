// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FTransform;
struct FCartesianCoordinates;
struct FGeographicCoordinates;
struct FVector;
class UObject;
class AGeoReferencingSystem;
#ifdef GEOREFERENCING_GeoReferencingSystem_generated_h
#error "GeoReferencingSystem.generated.h already included, missing '#pragma once' in GeoReferencingSystem.h"
#endif
#define GEOREFERENCING_GeoReferencingSystem_generated_h

#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_SPARSE_DATA
#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsCRSStringValid); \
	DECLARE_FUNCTION(execGetPlanetCenterTransform); \
	DECLARE_FUNCTION(execGetTangentTransformAtECEFLocation); \
	DECLARE_FUNCTION(execGetTangentTransformAtGeographicLocation); \
	DECLARE_FUNCTION(execGetTangentTransformAtProjectedLocation); \
	DECLARE_FUNCTION(execGetTangentTransformAtEngineLocation); \
	DECLARE_FUNCTION(execGetECEFENUVectorsAtECEFLocation); \
	DECLARE_FUNCTION(execGetENUVectorsAtECEFLocation); \
	DECLARE_FUNCTION(execGetENUVectorsAtGeographicLocation); \
	DECLARE_FUNCTION(execGetENUVectorsAtProjectedLocation); \
	DECLARE_FUNCTION(execGetENUVectorsAtEngineLocation); \
	DECLARE_FUNCTION(execECEFToGeographic); \
	DECLARE_FUNCTION(execGeographicToECEF); \
	DECLARE_FUNCTION(execECEFToProjected); \
	DECLARE_FUNCTION(execProjectedToECEF); \
	DECLARE_FUNCTION(execGeographicToProjected); \
	DECLARE_FUNCTION(execProjectedToGeographic); \
	DECLARE_FUNCTION(execECEFToEngine); \
	DECLARE_FUNCTION(execEngineToECEF); \
	DECLARE_FUNCTION(execProjectedToEngine); \
	DECLARE_FUNCTION(execEngineToProjected); \
	DECLARE_FUNCTION(execGetGeoReferencingSystem);


#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsCRSStringValid); \
	DECLARE_FUNCTION(execGetPlanetCenterTransform); \
	DECLARE_FUNCTION(execGetTangentTransformAtECEFLocation); \
	DECLARE_FUNCTION(execGetTangentTransformAtGeographicLocation); \
	DECLARE_FUNCTION(execGetTangentTransformAtProjectedLocation); \
	DECLARE_FUNCTION(execGetTangentTransformAtEngineLocation); \
	DECLARE_FUNCTION(execGetECEFENUVectorsAtECEFLocation); \
	DECLARE_FUNCTION(execGetENUVectorsAtECEFLocation); \
	DECLARE_FUNCTION(execGetENUVectorsAtGeographicLocation); \
	DECLARE_FUNCTION(execGetENUVectorsAtProjectedLocation); \
	DECLARE_FUNCTION(execGetENUVectorsAtEngineLocation); \
	DECLARE_FUNCTION(execECEFToGeographic); \
	DECLARE_FUNCTION(execGeographicToECEF); \
	DECLARE_FUNCTION(execECEFToProjected); \
	DECLARE_FUNCTION(execProjectedToECEF); \
	DECLARE_FUNCTION(execGeographicToProjected); \
	DECLARE_FUNCTION(execProjectedToGeographic); \
	DECLARE_FUNCTION(execECEFToEngine); \
	DECLARE_FUNCTION(execEngineToECEF); \
	DECLARE_FUNCTION(execProjectedToEngine); \
	DECLARE_FUNCTION(execEngineToProjected); \
	DECLARE_FUNCTION(execGetGeoReferencingSystem);


#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGeoReferencingSystem(); \
	friend struct Z_Construct_UClass_AGeoReferencingSystem_Statics; \
public: \
	DECLARE_CLASS(AGeoReferencingSystem, AInfo, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeoReferencing"), NO_API) \
	DECLARE_SERIALIZER(AGeoReferencingSystem)


#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_INCLASS \
private: \
	static void StaticRegisterNativesAGeoReferencingSystem(); \
	friend struct Z_Construct_UClass_AGeoReferencingSystem_Statics; \
public: \
	DECLARE_CLASS(AGeoReferencingSystem, AInfo, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeoReferencing"), NO_API) \
	DECLARE_SERIALIZER(AGeoReferencingSystem)


#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGeoReferencingSystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGeoReferencingSystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGeoReferencingSystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGeoReferencingSystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGeoReferencingSystem(AGeoReferencingSystem&&); \
	NO_API AGeoReferencingSystem(const AGeoReferencingSystem&); \
public:


#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGeoReferencingSystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGeoReferencingSystem(AGeoReferencingSystem&&); \
	NO_API AGeoReferencingSystem(const AGeoReferencingSystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGeoReferencingSystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGeoReferencingSystem); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGeoReferencingSystem)


#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_40_PROLOG
#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_SPARSE_DATA \
	Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_INCLASS \
	Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_SPARSE_DATA \
	Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h_43_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GEOREFERENCING_API UClass* StaticClass<class AGeoReferencingSystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GeoReferencing_Source_GeoReferencing_Public_GeoReferencingSystem_h


#define FOREACH_ENUM_EPLANETSHAPE(op) \
	op(EPlanetShape::FlatPlanet) \
	op(EPlanetShape::RoundPlanet) 

enum class EPlanetShape : uint8;
template<> GEOREFERENCING_API UEnum* StaticEnum<EPlanetShape>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
