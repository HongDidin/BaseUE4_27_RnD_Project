// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeoReferencing/Public/CartesianCoordinates.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCartesianCoordinates() {}
// Cross Module References
	GEOREFERENCING_API UScriptStruct* Z_Construct_UScriptStruct_FCartesianCoordinates();
	UPackage* Z_Construct_UPackage__Script_GeoReferencing();
	GEOREFERENCING_API UClass* Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_NoRegister();
	GEOREFERENCING_API UClass* Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
// End Cross Module References
class UScriptStruct* FCartesianCoordinates::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern GEOREFERENCING_API uint32 Get_Z_Construct_UScriptStruct_FCartesianCoordinates_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCartesianCoordinates, Z_Construct_UPackage__Script_GeoReferencing(), TEXT("CartesianCoordinates"), sizeof(FCartesianCoordinates), Get_Z_Construct_UScriptStruct_FCartesianCoordinates_Hash());
	}
	return Singleton;
}
template<> GEOREFERENCING_API UScriptStruct* StaticStruct<FCartesianCoordinates>()
{
	return FCartesianCoordinates::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCartesianCoordinates(FCartesianCoordinates::StaticStruct, TEXT("/Script/GeoReferencing"), TEXT("CartesianCoordinates"), false, nullptr, nullptr);
static struct FScriptStruct_GeoReferencing_StaticRegisterNativesFCartesianCoordinates
{
	FScriptStruct_GeoReferencing_StaticRegisterNativesFCartesianCoordinates()
	{
		UScriptStruct::DeferCppStructOps<FCartesianCoordinates>(FName(TEXT("CartesianCoordinates")));
	}
} ScriptStruct_GeoReferencing_StaticRegisterNativesFCartesianCoordinates;
	struct Z_Construct_UScriptStruct_FCartesianCoordinates_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCartesianCoordinates_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/// PEER_REVIEW - I used an Fxxx instead of UObject-based as I thought it was cheaper memorywise, \n/// but I needed to add a BP_FL to make the conversions since Fstruct can't contain UFUNCTIONS. Is that a good choice ?\n" },
		{ "ModuleRelativePath", "Public/CartesianCoordinates.h" },
		{ "ToolTip", "PEER_REVIEW - I used an Fxxx instead of UObject-based as I thought it was cheaper memorywise,\nbut I needed to add a BP_FL to make the conversions since Fstruct can't contain UFUNCTIONS. Is that a good choice ?" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCartesianCoordinates_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCartesianCoordinates>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCartesianCoordinates_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_GeoReferencing,
		nullptr,
		&NewStructOps,
		"CartesianCoordinates",
		sizeof(FCartesianCoordinates),
		alignof(FCartesianCoordinates),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCartesianCoordinates_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCartesianCoordinates_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCartesianCoordinates()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCartesianCoordinates_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_GeoReferencing();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CartesianCoordinates"), sizeof(FCartesianCoordinates), Get_Z_Construct_UScriptStruct_FCartesianCoordinates_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCartesianCoordinates_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCartesianCoordinates_Hash() { return 4200911368U; }
	DEFINE_FUNCTION(UCartesianCoordinatesFunctionLibrary::execMakeCartesianCoordinatesApproximation)
	{
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_InX);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_InY);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_InZ);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FCartesianCoordinates*)Z_Param__Result=UCartesianCoordinatesFunctionLibrary::MakeCartesianCoordinatesApproximation(Z_Param_Out_InX,Z_Param_Out_InY,Z_Param_Out_InZ);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCartesianCoordinatesFunctionLibrary::execToFloatApproximation)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_CartesianCoordinates);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_OutX);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_OutY);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_OutZ);
		P_FINISH;
		P_NATIVE_BEGIN;
		UCartesianCoordinatesFunctionLibrary::ToFloatApproximation(Z_Param_Out_CartesianCoordinates,Z_Param_Out_OutX,Z_Param_Out_OutY,Z_Param_Out_OutZ);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCartesianCoordinatesFunctionLibrary::execToSeparateTexts)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_CartesianCoordinates);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutX);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutY);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutZ);
		P_GET_PROPERTY(FIntProperty,Z_Param_IntegralDigits);
		P_FINISH;
		P_NATIVE_BEGIN;
		UCartesianCoordinatesFunctionLibrary::ToSeparateTexts(Z_Param_Out_CartesianCoordinates,Z_Param_Out_OutX,Z_Param_Out_OutY,Z_Param_Out_OutZ,Z_Param_IntegralDigits);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCartesianCoordinatesFunctionLibrary::execToCompactText)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_CartesianCoordinates);
		P_GET_PROPERTY(FIntProperty,Z_Param_IntegralDigits);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=UCartesianCoordinatesFunctionLibrary::ToCompactText(Z_Param_Out_CartesianCoordinates,Z_Param_IntegralDigits);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCartesianCoordinatesFunctionLibrary::execToFullText)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_CartesianCoordinates);
		P_GET_PROPERTY(FIntProperty,Z_Param_IntegralDigits);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=UCartesianCoordinatesFunctionLibrary::ToFullText(Z_Param_Out_CartesianCoordinates,Z_Param_IntegralDigits);
		P_NATIVE_END;
	}
	void UCartesianCoordinatesFunctionLibrary::StaticRegisterNativesUCartesianCoordinatesFunctionLibrary()
	{
		UClass* Class = UCartesianCoordinatesFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "MakeCartesianCoordinatesApproximation", &UCartesianCoordinatesFunctionLibrary::execMakeCartesianCoordinatesApproximation },
			{ "ToCompactText", &UCartesianCoordinatesFunctionLibrary::execToCompactText },
			{ "ToFloatApproximation", &UCartesianCoordinatesFunctionLibrary::execToFloatApproximation },
			{ "ToFullText", &UCartesianCoordinatesFunctionLibrary::execToFullText },
			{ "ToSeparateTexts", &UCartesianCoordinatesFunctionLibrary::execToSeparateTexts },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics
	{
		struct CartesianCoordinatesFunctionLibrary_eventMakeCartesianCoordinatesApproximation_Parms
		{
			float InX;
			float InY;
			float InZ;
			FCartesianCoordinates ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InX_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InY_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InZ_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZ;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InX_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InX = { "InX", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventMakeCartesianCoordinatesApproximation_Parms, InX), METADATA_PARAMS(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InX_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InY_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InY = { "InY", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventMakeCartesianCoordinatesApproximation_Parms, InY), METADATA_PARAMS(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InY_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InZ_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InZ = { "InZ", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventMakeCartesianCoordinatesApproximation_Parms, InZ), METADATA_PARAMS(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InZ_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InZ_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventMakeCartesianCoordinatesApproximation_Parms, ReturnValue), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_InZ,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * Set the Coordinates from float approximation.\n\x09 * USE WISELY as we can't guarantee there will no be rounding due to IEEE754 float encoding !\n\x09 **/" },
		{ "ModuleRelativePath", "Public/CartesianCoordinates.h" },
		{ "ToolTip", "Set the Coordinates from float approximation.\nUSE WISELY as we can't guarantee there will no be rounding due to IEEE754 float encoding !" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary, nullptr, "MakeCartesianCoordinatesApproximation", nullptr, nullptr, sizeof(CartesianCoordinatesFunctionLibrary_eventMakeCartesianCoordinatesApproximation_Parms), Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics
	{
		struct CartesianCoordinatesFunctionLibrary_eventToCompactText_Parms
		{
			FCartesianCoordinates CartesianCoordinates;
			int32 IntegralDigits;
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CartesianCoordinates;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegralDigits;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_CartesianCoordinates = { "CartesianCoordinates", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToCompactText_Parms, CartesianCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_IntegralDigits = { "IntegralDigits", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToCompactText_Parms, IntegralDigits), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToCompactText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_CartesianCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_IntegralDigits,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "1" },
		{ "BlueprintAutocast", "" },
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * Converts a LargeCoordinates value to formatted text, in the form '(X, Y, Z)'\n\x09 **/" },
		{ "CPP_Default_IntegralDigits", "3" },
		{ "DisplayName", "ToCompactText" },
		{ "ModuleRelativePath", "Public/CartesianCoordinates.h" },
		{ "ToolTip", "Converts a LargeCoordinates value to formatted text, in the form '(X, Y, Z)'" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary, nullptr, "ToCompactText", nullptr, nullptr, sizeof(CartesianCoordinatesFunctionLibrary_eventToCompactText_Parms), Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics
	{
		struct CartesianCoordinatesFunctionLibrary_eventToFloatApproximation_Parms
		{
			FCartesianCoordinates CartesianCoordinates;
			float OutX;
			float OutY;
			float OutZ;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CartesianCoordinates;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutX;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutY;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutZ;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_CartesianCoordinates = { "CartesianCoordinates", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToFloatApproximation_Parms, CartesianCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutX = { "OutX", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToFloatApproximation_Parms, OutX), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutY = { "OutY", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToFloatApproximation_Parms, OutY), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutZ = { "OutZ", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToFloatApproximation_Parms, OutZ), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_CartesianCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::NewProp_OutZ,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * Get the Coordinates as a float approximation.\n\x09 * USE WISELY as we can't guarantee there will no be rounding due to IEEE754 float encoding !\n\x09 **/" },
		{ "ModuleRelativePath", "Public/CartesianCoordinates.h" },
		{ "ToolTip", "Get the Coordinates as a float approximation.\nUSE WISELY as we can't guarantee there will no be rounding due to IEEE754 float encoding !" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary, nullptr, "ToFloatApproximation", nullptr, nullptr, sizeof(CartesianCoordinatesFunctionLibrary_eventToFloatApproximation_Parms), Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics
	{
		struct CartesianCoordinatesFunctionLibrary_eventToFullText_Parms
		{
			FCartesianCoordinates CartesianCoordinates;
			int32 IntegralDigits;
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CartesianCoordinates;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegralDigits;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_CartesianCoordinates = { "CartesianCoordinates", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToFullText_Parms, CartesianCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_IntegralDigits = { "IntegralDigits", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToFullText_Parms, IntegralDigits), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToFullText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_CartesianCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_IntegralDigits,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "1" },
		{ "BlueprintAutocast", "" },
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * Converts a LargeCoordinates value to localized formatted text, in the form 'X= Y= Z='\n\x09 **/" },
		{ "CPP_Default_IntegralDigits", "3" },
		{ "DisplayName", "ToFullText" },
		{ "ModuleRelativePath", "Public/CartesianCoordinates.h" },
		{ "ToolTip", "Converts a LargeCoordinates value to localized formatted text, in the form 'X= Y= Z='" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary, nullptr, "ToFullText", nullptr, nullptr, sizeof(CartesianCoordinatesFunctionLibrary_eventToFullText_Parms), Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics
	{
		struct CartesianCoordinatesFunctionLibrary_eventToSeparateTexts_Parms
		{
			FCartesianCoordinates CartesianCoordinates;
			FText OutX;
			FText OutY;
			FText OutZ;
			int32 IntegralDigits;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CartesianCoordinates;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutX;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutY;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutZ;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegralDigits;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_CartesianCoordinates = { "CartesianCoordinates", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, CartesianCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutX = { "OutX", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, OutX), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutY = { "OutY", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, OutY), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutZ = { "OutZ", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, OutZ), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_IntegralDigits = { "IntegralDigits", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CartesianCoordinatesFunctionLibrary_eventToSeparateTexts_Parms, IntegralDigits), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_CartesianCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_OutZ,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::NewProp_IntegralDigits,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "4" },
		{ "BlueprintAutocast", "" },
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * Converts a LargeCoordinates value to 3 separate text values\n\x09 **/" },
		{ "CPP_Default_IntegralDigits", "3" },
		{ "DisplayName", "ToSeparateTexts" },
		{ "ModuleRelativePath", "Public/CartesianCoordinates.h" },
		{ "ToolTip", "Converts a LargeCoordinates value to 3 separate text values" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary, nullptr, "ToSeparateTexts", nullptr, nullptr, sizeof(CartesianCoordinatesFunctionLibrary_eventToSeparateTexts_Parms), Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_NoRegister()
	{
		return UCartesianCoordinatesFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_GeoReferencing,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_MakeCartesianCoordinatesApproximation, "MakeCartesianCoordinatesApproximation" }, // 195503641
		{ &Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToCompactText, "ToCompactText" }, // 3252127283
		{ &Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFloatApproximation, "ToFloatApproximation" }, // 1263536244
		{ &Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToFullText, "ToFullText" }, // 1302074480
		{ &Z_Construct_UFunction_UCartesianCoordinatesFunctionLibrary_ToSeparateTexts, "ToSeparateTexts" }, // 3891583063
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CartesianCoordinates.h" },
		{ "ModuleRelativePath", "Public/CartesianCoordinates.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCartesianCoordinatesFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_Statics::ClassParams = {
		&UCartesianCoordinatesFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCartesianCoordinatesFunctionLibrary, 3075342204);
	template<> GEOREFERENCING_API UClass* StaticClass<UCartesianCoordinatesFunctionLibrary>()
	{
		return UCartesianCoordinatesFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCartesianCoordinatesFunctionLibrary(Z_Construct_UClass_UCartesianCoordinatesFunctionLibrary, &UCartesianCoordinatesFunctionLibrary::StaticClass, TEXT("/Script/GeoReferencing"), TEXT("UCartesianCoordinatesFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCartesianCoordinatesFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
