// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeoReferencing/Public/GeoReferencingSystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeoReferencingSystem() {}
// Cross Module References
	GEOREFERENCING_API UEnum* Z_Construct_UEnum_GeoReferencing_EPlanetShape();
	UPackage* Z_Construct_UPackage__Script_GeoReferencing();
	GEOREFERENCING_API UClass* Z_Construct_UClass_AGeoReferencingSystem_NoRegister();
	GEOREFERENCING_API UClass* Z_Construct_UClass_AGeoReferencingSystem();
	ENGINE_API UClass* Z_Construct_UClass_AInfo();
	GEOREFERENCING_API UScriptStruct* Z_Construct_UScriptStruct_FCartesianCoordinates();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	GEOREFERENCING_API UScriptStruct* Z_Construct_UScriptStruct_FGeographicCoordinates();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References
	static UEnum* EPlanetShape_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GeoReferencing_EPlanetShape, Z_Construct_UPackage__Script_GeoReferencing(), TEXT("EPlanetShape"));
		}
		return Singleton;
	}
	template<> GEOREFERENCING_API UEnum* StaticEnum<EPlanetShape>()
	{
		return EPlanetShape_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPlanetShape(EPlanetShape_StaticEnum, TEXT("/Script/GeoReferencing"), TEXT("EPlanetShape"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GeoReferencing_EPlanetShape_Hash() { return 1357933996U; }
	UEnum* Z_Construct_UEnum_GeoReferencing_EPlanetShape()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GeoReferencing();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPlanetShape"), 0, Get_Z_Construct_UEnum_GeoReferencing_EPlanetShape_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPlanetShape::FlatPlanet", (int64)EPlanetShape::FlatPlanet },
				{ "EPlanetShape::RoundPlanet", (int64)EPlanetShape::RoundPlanet },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "FlatPlanet.Comment", "/**\n\x09 * The world geometry coordinates are expressed in a projected space such as a Mercator projection.\n\x09 * In this mode, Planet curvature is not considered and you might face errors related to projection on large environments\n\x09 */" },
				{ "FlatPlanet.DisplayName", "Flat Planet" },
				{ "FlatPlanet.Name", "EPlanetShape::FlatPlanet" },
				{ "FlatPlanet.ToolTip", "The world geometry coordinates are expressed in a projected space such as a Mercator projection.\nIn this mode, Planet curvature is not considered and you might face errors related to projection on large environments" },
				{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
				{ "RoundPlanet.Comment", "/**\n\x09 * The world geometry coordinates are expressed in a planet wide Cartesian frame,\n\x09 * placed on a specific location or at earth, or at the planet center.\n\x09 * You might need dynamic rebasing to avoid precision issues at large scales.\n\x09 */" },
				{ "RoundPlanet.DisplayName", "Round Planet" },
				{ "RoundPlanet.Name", "EPlanetShape::RoundPlanet" },
				{ "RoundPlanet.ToolTip", "The world geometry coordinates are expressed in a planet wide Cartesian frame,\nplaced on a specific location or at earth, or at the planet center.\nYou might need dynamic rebasing to avoid precision issues at large scales." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GeoReferencing,
				nullptr,
				"EPlanetShape",
				"EPlanetShape",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execIsCRSStringValid)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_CRSString);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_Error);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsCRSStringValid(Z_Param_CRSString,Z_Param_Out_Error);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetPlanetCenterTransform)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetPlanetCenterTransform();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetTangentTransformAtECEFLocation)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ECEFCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetTangentTransformAtECEFLocation(Z_Param_Out_ECEFCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetTangentTransformAtGeographicLocation)
	{
		P_GET_STRUCT_REF(FGeographicCoordinates,Z_Param_Out_GeographicCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetTangentTransformAtGeographicLocation(Z_Param_Out_GeographicCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetTangentTransformAtProjectedLocation)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetTangentTransformAtProjectedLocation(Z_Param_Out_ProjectedCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetTangentTransformAtEngineLocation)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_EngineCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetTangentTransformAtEngineLocation(Z_Param_Out_EngineCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetECEFENUVectorsAtECEFLocation)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ECEFCoordinates);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ECEFEast);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ECEFNorth);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ECEFUp);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetECEFENUVectorsAtECEFLocation(Z_Param_Out_ECEFCoordinates,Z_Param_Out_ECEFEast,Z_Param_Out_ECEFNorth,Z_Param_Out_ECEFUp);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetENUVectorsAtECEFLocation)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ECEFCoordinates);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_East);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_North);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Up);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetENUVectorsAtECEFLocation(Z_Param_Out_ECEFCoordinates,Z_Param_Out_East,Z_Param_Out_North,Z_Param_Out_Up);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetENUVectorsAtGeographicLocation)
	{
		P_GET_STRUCT_REF(FGeographicCoordinates,Z_Param_Out_GeographicCoordinates);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_East);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_North);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Up);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetENUVectorsAtGeographicLocation(Z_Param_Out_GeographicCoordinates,Z_Param_Out_East,Z_Param_Out_North,Z_Param_Out_Up);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetENUVectorsAtProjectedLocation)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_East);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_North);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Up);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetENUVectorsAtProjectedLocation(Z_Param_Out_ProjectedCoordinates,Z_Param_Out_East,Z_Param_Out_North,Z_Param_Out_Up);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetENUVectorsAtEngineLocation)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_EngineCoordinates);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_East);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_North);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Up);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetENUVectorsAtEngineLocation(Z_Param_Out_EngineCoordinates,Z_Param_Out_East,Z_Param_Out_North,Z_Param_Out_Up);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execECEFToGeographic)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ECEFCoordinates);
		P_GET_STRUCT_REF(FGeographicCoordinates,Z_Param_Out_GeographicCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ECEFToGeographic(Z_Param_Out_ECEFCoordinates,Z_Param_Out_GeographicCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGeographicToECEF)
	{
		P_GET_STRUCT_REF(FGeographicCoordinates,Z_Param_Out_GeographicCoordinates);
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ECEFCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GeographicToECEF(Z_Param_Out_GeographicCoordinates,Z_Param_Out_ECEFCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execECEFToProjected)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ECEFCoordinates);
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ECEFToProjected(Z_Param_Out_ECEFCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execProjectedToECEF)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ECEFCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ProjectedToECEF(Z_Param_Out_ProjectedCoordinates,Z_Param_Out_ECEFCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGeographicToProjected)
	{
		P_GET_STRUCT_REF(FGeographicCoordinates,Z_Param_Out_GeographicCoordinates);
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GeographicToProjected(Z_Param_Out_GeographicCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execProjectedToGeographic)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_GET_STRUCT_REF(FGeographicCoordinates,Z_Param_Out_GeographicCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ProjectedToGeographic(Z_Param_Out_ProjectedCoordinates,Z_Param_Out_GeographicCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execECEFToEngine)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ECEFCoordinates);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_EngineCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ECEFToEngine(Z_Param_Out_ECEFCoordinates,Z_Param_Out_EngineCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execEngineToECEF)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_EngineCoordinates);
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ECEFCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EngineToECEF(Z_Param_Out_EngineCoordinates,Z_Param_Out_ECEFCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execProjectedToEngine)
	{
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_EngineCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ProjectedToEngine(Z_Param_Out_ProjectedCoordinates,Z_Param_Out_EngineCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execEngineToProjected)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_EngineCoordinates);
		P_GET_STRUCT_REF(FCartesianCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EngineToProjected(Z_Param_Out_EngineCoordinates,Z_Param_Out_ProjectedCoordinates);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AGeoReferencingSystem::execGetGeoReferencingSystem)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AGeoReferencingSystem**)Z_Param__Result=AGeoReferencingSystem::GetGeoReferencingSystem(Z_Param_WorldContextObject);
		P_NATIVE_END;
	}
	void AGeoReferencingSystem::StaticRegisterNativesAGeoReferencingSystem()
	{
		UClass* Class = AGeoReferencingSystem::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ECEFToEngine", &AGeoReferencingSystem::execECEFToEngine },
			{ "ECEFToGeographic", &AGeoReferencingSystem::execECEFToGeographic },
			{ "ECEFToProjected", &AGeoReferencingSystem::execECEFToProjected },
			{ "EngineToECEF", &AGeoReferencingSystem::execEngineToECEF },
			{ "EngineToProjected", &AGeoReferencingSystem::execEngineToProjected },
			{ "GeographicToECEF", &AGeoReferencingSystem::execGeographicToECEF },
			{ "GeographicToProjected", &AGeoReferencingSystem::execGeographicToProjected },
			{ "GetECEFENUVectorsAtECEFLocation", &AGeoReferencingSystem::execGetECEFENUVectorsAtECEFLocation },
			{ "GetENUVectorsAtECEFLocation", &AGeoReferencingSystem::execGetENUVectorsAtECEFLocation },
			{ "GetENUVectorsAtEngineLocation", &AGeoReferencingSystem::execGetENUVectorsAtEngineLocation },
			{ "GetENUVectorsAtGeographicLocation", &AGeoReferencingSystem::execGetENUVectorsAtGeographicLocation },
			{ "GetENUVectorsAtProjectedLocation", &AGeoReferencingSystem::execGetENUVectorsAtProjectedLocation },
			{ "GetGeoReferencingSystem", &AGeoReferencingSystem::execGetGeoReferencingSystem },
			{ "GetPlanetCenterTransform", &AGeoReferencingSystem::execGetPlanetCenterTransform },
			{ "GetTangentTransformAtECEFLocation", &AGeoReferencingSystem::execGetTangentTransformAtECEFLocation },
			{ "GetTangentTransformAtEngineLocation", &AGeoReferencingSystem::execGetTangentTransformAtEngineLocation },
			{ "GetTangentTransformAtGeographicLocation", &AGeoReferencingSystem::execGetTangentTransformAtGeographicLocation },
			{ "GetTangentTransformAtProjectedLocation", &AGeoReferencingSystem::execGetTangentTransformAtProjectedLocation },
			{ "IsCRSStringValid", &AGeoReferencingSystem::execIsCRSStringValid },
			{ "ProjectedToECEF", &AGeoReferencingSystem::execProjectedToECEF },
			{ "ProjectedToEngine", &AGeoReferencingSystem::execProjectedToEngine },
			{ "ProjectedToGeographic", &AGeoReferencingSystem::execProjectedToGeographic },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics
	{
		struct GeoReferencingSystem_eventECEFToEngine_Parms
		{
			FCartesianCoordinates ECEFCoordinates;
			FVector EngineCoordinates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ECEFCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EngineCoordinates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::NewProp_ECEFCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::NewProp_ECEFCoordinates = { "ECEFCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventECEFToEngine_Parms, ECEFCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::NewProp_ECEFCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::NewProp_ECEFCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::NewProp_EngineCoordinates = { "EngineCoordinates", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventECEFToEngine_Parms, EngineCoordinates), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::NewProp_ECEFCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::NewProp_EngineCoordinates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Transformations" },
		{ "Comment", "/**\n\x09 * Convert a Vector expressed in ECEF CRS to ENGINE space\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Convert a Vector expressed in ECEF CRS to ENGINE space" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "ECEFToEngine", nullptr, nullptr, sizeof(GeoReferencingSystem_eventECEFToEngine_Parms), Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics
	{
		struct GeoReferencingSystem_eventECEFToGeographic_Parms
		{
			FCartesianCoordinates ECEFCoordinates;
			FGeographicCoordinates GeographicCoordinates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ECEFCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeographicCoordinates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::NewProp_ECEFCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::NewProp_ECEFCoordinates = { "ECEFCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventECEFToGeographic_Parms, ECEFCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::NewProp_ECEFCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::NewProp_ECEFCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::NewProp_GeographicCoordinates = { "GeographicCoordinates", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventECEFToGeographic_Parms, GeographicCoordinates), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::NewProp_ECEFCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::NewProp_GeographicCoordinates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Transformations" },
		{ "Comment", "/**\n\x09 * Convert a Coordinate expressed in ECEF CRS to GEOGRAPHIC CRS\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Convert a Coordinate expressed in ECEF CRS to GEOGRAPHIC CRS" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "ECEFToGeographic", nullptr, nullptr, sizeof(GeoReferencingSystem_eventECEFToGeographic_Parms), Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics
	{
		struct GeoReferencingSystem_eventECEFToProjected_Parms
		{
			FCartesianCoordinates ECEFCoordinates;
			FCartesianCoordinates ProjectedCoordinates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ECEFCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectedCoordinates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::NewProp_ECEFCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::NewProp_ECEFCoordinates = { "ECEFCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventECEFToProjected_Parms, ECEFCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::NewProp_ECEFCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::NewProp_ECEFCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::NewProp_ProjectedCoordinates = { "ProjectedCoordinates", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventECEFToProjected_Parms, ProjectedCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::NewProp_ECEFCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::NewProp_ProjectedCoordinates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Transformations" },
		{ "Comment", "/**\n\x09 * Convert a Coordinate expressed in ECEF CRS to PROJECTED CRS\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Convert a Coordinate expressed in ECEF CRS to PROJECTED CRS" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "ECEFToProjected", nullptr, nullptr, sizeof(GeoReferencingSystem_eventECEFToProjected_Parms), Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics
	{
		struct GeoReferencingSystem_eventEngineToECEF_Parms
		{
			FVector EngineCoordinates;
			FCartesianCoordinates ECEFCoordinates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EngineCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EngineCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFCoordinates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::NewProp_EngineCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::NewProp_EngineCoordinates = { "EngineCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventEngineToECEF_Parms, EngineCoordinates), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::NewProp_EngineCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::NewProp_EngineCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::NewProp_ECEFCoordinates = { "ECEFCoordinates", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventEngineToECEF_Parms, ECEFCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::NewProp_EngineCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::NewProp_ECEFCoordinates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Transformations" },
		{ "Comment", "/**\n\x09 * Convert a Vector expressed in ENGINE space to the ECEF CRS\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Convert a Vector expressed in ENGINE space to the ECEF CRS" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "EngineToECEF", nullptr, nullptr, sizeof(GeoReferencingSystem_eventEngineToECEF_Parms), Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics
	{
		struct GeoReferencingSystem_eventEngineToProjected_Parms
		{
			FVector EngineCoordinates;
			FCartesianCoordinates ProjectedCoordinates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EngineCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EngineCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectedCoordinates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::NewProp_EngineCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::NewProp_EngineCoordinates = { "EngineCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventEngineToProjected_Parms, EngineCoordinates), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::NewProp_EngineCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::NewProp_EngineCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::NewProp_ProjectedCoordinates = { "ProjectedCoordinates", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventEngineToProjected_Parms, ProjectedCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::NewProp_EngineCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::NewProp_ProjectedCoordinates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Transformations" },
		{ "Comment", "/**\n\x09  * Convert a Vector expressed in ENGINE space to the PROJECTED CRS\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Convert a Vector expressed in ENGINE space to the PROJECTED CRS" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "EngineToProjected", nullptr, nullptr, sizeof(GeoReferencingSystem_eventEngineToProjected_Parms), Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics
	{
		struct GeoReferencingSystem_eventGeographicToECEF_Parms
		{
			FGeographicCoordinates GeographicCoordinates;
			FCartesianCoordinates ECEFCoordinates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeographicCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeographicCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFCoordinates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::NewProp_GeographicCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::NewProp_GeographicCoordinates = { "GeographicCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGeographicToECEF_Parms, GeographicCoordinates), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::NewProp_GeographicCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::NewProp_GeographicCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::NewProp_ECEFCoordinates = { "ECEFCoordinates", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGeographicToECEF_Parms, ECEFCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::NewProp_GeographicCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::NewProp_ECEFCoordinates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Transformations" },
		{ "Comment", "/**\n\x09 * Convert a Coordinate expressed in GEOGRAPHIC CRS to ECEF CRS\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Convert a Coordinate expressed in GEOGRAPHIC CRS to ECEF CRS" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GeographicToECEF", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGeographicToECEF_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics
	{
		struct GeoReferencingSystem_eventGeographicToProjected_Parms
		{
			FGeographicCoordinates GeographicCoordinates;
			FCartesianCoordinates ProjectedCoordinates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeographicCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeographicCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectedCoordinates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::NewProp_GeographicCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::NewProp_GeographicCoordinates = { "GeographicCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGeographicToProjected_Parms, GeographicCoordinates), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::NewProp_GeographicCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::NewProp_GeographicCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::NewProp_ProjectedCoordinates = { "ProjectedCoordinates", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGeographicToProjected_Parms, ProjectedCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::NewProp_GeographicCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::NewProp_ProjectedCoordinates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Transformations" },
		{ "Comment", "/**\n\x09 * Convert a Coordinate expressed in GEOGRAPHIC CRS to PROJECTED CRS\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Convert a Coordinate expressed in GEOGRAPHIC CRS to PROJECTED CRS" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GeographicToProjected", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGeographicToProjected_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics
	{
		struct GeoReferencingSystem_eventGetECEFENUVectorsAtECEFLocation_Parms
		{
			FCartesianCoordinates ECEFCoordinates;
			FVector ECEFEast;
			FVector ECEFNorth;
			FVector ECEFUp;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ECEFCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFEast;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFNorth;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFUp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFCoordinates = { "ECEFCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetECEFENUVectorsAtECEFLocation_Parms, ECEFCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFEast = { "ECEFEast", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetECEFENUVectorsAtECEFLocation_Parms, ECEFEast), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFNorth = { "ECEFNorth", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetECEFENUVectorsAtECEFLocation_Parms, ECEFNorth), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFUp = { "ECEFUp", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetECEFENUVectorsAtECEFLocation_Parms, ECEFUp), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFEast,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFNorth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::NewProp_ECEFUp,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|ENU" },
		{ "Comment", "/**\n\x09 * Get the East North Up vectors at a specific location - Not in engine frame, but in pure ECEF Frame !\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Get the East North Up vectors at a specific location - Not in engine frame, but in pure ECEF Frame !" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetECEFENUVectorsAtECEFLocation", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetECEFENUVectorsAtECEFLocation_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics
	{
		struct GeoReferencingSystem_eventGetENUVectorsAtECEFLocation_Parms
		{
			FCartesianCoordinates ECEFCoordinates;
			FVector East;
			FVector North;
			FVector Up;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ECEFCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_East;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_North;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Up;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_ECEFCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_ECEFCoordinates = { "ECEFCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtECEFLocation_Parms, ECEFCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_ECEFCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_ECEFCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_East = { "East", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtECEFLocation_Parms, East), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_North = { "North", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtECEFLocation_Parms, North), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_Up = { "Up", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtECEFLocation_Parms, Up), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_ECEFCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_East,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_North,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::NewProp_Up,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|ENU" },
		{ "Comment", "/**\n\x09 * Get the East North Up vectors at a specific location\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Get the East North Up vectors at a specific location" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetENUVectorsAtECEFLocation", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetENUVectorsAtECEFLocation_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics
	{
		struct GeoReferencingSystem_eventGetENUVectorsAtEngineLocation_Parms
		{
			FVector EngineCoordinates;
			FVector East;
			FVector North;
			FVector Up;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EngineCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EngineCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_East;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_North;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Up;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_EngineCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_EngineCoordinates = { "EngineCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtEngineLocation_Parms, EngineCoordinates), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_EngineCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_EngineCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_East = { "East", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtEngineLocation_Parms, East), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_North = { "North", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtEngineLocation_Parms, North), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_Up = { "Up", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtEngineLocation_Parms, Up), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_EngineCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_East,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_North,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::NewProp_Up,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|ENU" },
		{ "Comment", "/**\n\x09 * Get the East North Up vectors at a specific location\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Get the East North Up vectors at a specific location" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetENUVectorsAtEngineLocation", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetENUVectorsAtEngineLocation_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics
	{
		struct GeoReferencingSystem_eventGetENUVectorsAtGeographicLocation_Parms
		{
			FGeographicCoordinates GeographicCoordinates;
			FVector East;
			FVector North;
			FVector Up;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeographicCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeographicCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_East;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_North;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Up;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_GeographicCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_GeographicCoordinates = { "GeographicCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtGeographicLocation_Parms, GeographicCoordinates), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_GeographicCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_GeographicCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_East = { "East", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtGeographicLocation_Parms, East), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_North = { "North", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtGeographicLocation_Parms, North), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_Up = { "Up", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtGeographicLocation_Parms, Up), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_GeographicCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_East,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_North,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::NewProp_Up,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|ENU" },
		{ "Comment", "/**\n\x09 * Get the East North Up vectors at a specific location\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Get the East North Up vectors at a specific location" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetENUVectorsAtGeographicLocation", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetENUVectorsAtGeographicLocation_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics
	{
		struct GeoReferencingSystem_eventGetENUVectorsAtProjectedLocation_Parms
		{
			FCartesianCoordinates ProjectedCoordinates;
			FVector East;
			FVector North;
			FVector Up;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectedCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectedCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_East;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_North;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Up;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_ProjectedCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_ProjectedCoordinates = { "ProjectedCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtProjectedLocation_Parms, ProjectedCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_ProjectedCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_ProjectedCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_East = { "East", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtProjectedLocation_Parms, East), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_North = { "North", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtProjectedLocation_Parms, North), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_Up = { "Up", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetENUVectorsAtProjectedLocation_Parms, Up), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_ProjectedCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_East,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_North,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::NewProp_Up,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|ENU" },
		{ "Comment", "/**\n\x09 * Get the East North Up vectors at a specific location\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Get the East North Up vectors at a specific location" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetENUVectorsAtProjectedLocation", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetENUVectorsAtProjectedLocation_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics
	{
		struct GeoReferencingSystem_eventGetGeoReferencingSystem_Parms
		{
			UObject* WorldContextObject;
			AGeoReferencingSystem* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetGeoReferencingSystem_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetGeoReferencingSystem_Parms, ReturnValue), Z_Construct_UClass_AGeoReferencingSystem_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetGeoReferencingSystem", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetGeoReferencingSystem_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics
	{
		struct GeoReferencingSystem_eventGetPlanetCenterTransform_Parms
		{
			FTransform ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetPlanetCenterTransform_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Misc" },
		{ "Comment", "/**\n\x09 * Set this transform to an Ellipsoid to have it positioned tangent to the origin.\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Set this transform to an Ellipsoid to have it positioned tangent to the origin." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetPlanetCenterTransform", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetPlanetCenterTransform_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics
	{
		struct GeoReferencingSystem_eventGetTangentTransformAtECEFLocation_Parms
		{
			FCartesianCoordinates ECEFCoordinates;
			FTransform ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ECEFCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::NewProp_ECEFCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::NewProp_ECEFCoordinates = { "ECEFCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetTangentTransformAtECEFLocation_Parms, ECEFCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::NewProp_ECEFCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::NewProp_ECEFCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetTangentTransformAtECEFLocation_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::NewProp_ECEFCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|TangentTransforms" },
		{ "Comment", "/**\n\x09 * Get the the transform to locate an object tangent to Ellipsoid at a specific location\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Get the the transform to locate an object tangent to Ellipsoid at a specific location" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetTangentTransformAtECEFLocation", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetTangentTransformAtECEFLocation_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics
	{
		struct GeoReferencingSystem_eventGetTangentTransformAtEngineLocation_Parms
		{
			FVector EngineCoordinates;
			FTransform ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EngineCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EngineCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::NewProp_EngineCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::NewProp_EngineCoordinates = { "EngineCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetTangentTransformAtEngineLocation_Parms, EngineCoordinates), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::NewProp_EngineCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::NewProp_EngineCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetTangentTransformAtEngineLocation_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::NewProp_EngineCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|TangentTransforms" },
		{ "Comment", "/**\n\x09 * Get the the transform to locate an object tangent to Ellipsoid at a specific location\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Get the the transform to locate an object tangent to Ellipsoid at a specific location" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetTangentTransformAtEngineLocation", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetTangentTransformAtEngineLocation_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics
	{
		struct GeoReferencingSystem_eventGetTangentTransformAtGeographicLocation_Parms
		{
			FGeographicCoordinates GeographicCoordinates;
			FTransform ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeographicCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeographicCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::NewProp_GeographicCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::NewProp_GeographicCoordinates = { "GeographicCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetTangentTransformAtGeographicLocation_Parms, GeographicCoordinates), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::NewProp_GeographicCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::NewProp_GeographicCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetTangentTransformAtGeographicLocation_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::NewProp_GeographicCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|TangentTransforms" },
		{ "Comment", "/**\n\x09 * Get the the transform to locate an object tangent to Ellipsoid at a specific location\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Get the the transform to locate an object tangent to Ellipsoid at a specific location" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetTangentTransformAtGeographicLocation", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetTangentTransformAtGeographicLocation_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics
	{
		struct GeoReferencingSystem_eventGetTangentTransformAtProjectedLocation_Parms
		{
			FCartesianCoordinates ProjectedCoordinates;
			FTransform ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectedCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectedCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::NewProp_ProjectedCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::NewProp_ProjectedCoordinates = { "ProjectedCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetTangentTransformAtProjectedLocation_Parms, ProjectedCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::NewProp_ProjectedCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::NewProp_ProjectedCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventGetTangentTransformAtProjectedLocation_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::NewProp_ProjectedCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|TangentTransforms" },
		{ "Comment", "/**\n\x09 * Get the the transform to locate an object tangent to Ellipsoid at a specific location\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Get the the transform to locate an object tangent to Ellipsoid at a specific location" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "GetTangentTransformAtProjectedLocation", nullptr, nullptr, sizeof(GeoReferencingSystem_eventGetTangentTransformAtProjectedLocation_Parms), Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics
	{
		struct GeoReferencingSystem_eventIsCRSStringValid_Parms
		{
			FString CRSString;
			FString Error;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CRSString;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Error;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::NewProp_CRSString = { "CRSString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventIsCRSStringValid_Parms, CRSString), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::NewProp_Error = { "Error", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventIsCRSStringValid_Parms, Error), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GeoReferencingSystem_eventIsCRSStringValid_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GeoReferencingSystem_eventIsCRSStringValid_Parms), &Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::NewProp_CRSString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::NewProp_Error,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Misc" },
		{ "Comment", "/**\n\x09 * Check if the string corresponds to a valid CRS descriptor\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Check if the string corresponds to a valid CRS descriptor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "IsCRSStringValid", nullptr, nullptr, sizeof(GeoReferencingSystem_eventIsCRSStringValid_Parms), Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics
	{
		struct GeoReferencingSystem_eventProjectedToECEF_Parms
		{
			FCartesianCoordinates ProjectedCoordinates;
			FCartesianCoordinates ECEFCoordinates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectedCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectedCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ECEFCoordinates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::NewProp_ProjectedCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::NewProp_ProjectedCoordinates = { "ProjectedCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventProjectedToECEF_Parms, ProjectedCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::NewProp_ProjectedCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::NewProp_ProjectedCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::NewProp_ECEFCoordinates = { "ECEFCoordinates", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventProjectedToECEF_Parms, ECEFCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::NewProp_ProjectedCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::NewProp_ECEFCoordinates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Transformations" },
		{ "Comment", "/**\n\x09 * Convert a Coordinate expressed in PROJECTED CRS to ECEF CRS\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Convert a Coordinate expressed in PROJECTED CRS to ECEF CRS" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "ProjectedToECEF", nullptr, nullptr, sizeof(GeoReferencingSystem_eventProjectedToECEF_Parms), Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics
	{
		struct GeoReferencingSystem_eventProjectedToEngine_Parms
		{
			FCartesianCoordinates ProjectedCoordinates;
			FVector EngineCoordinates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectedCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectedCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EngineCoordinates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::NewProp_ProjectedCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::NewProp_ProjectedCoordinates = { "ProjectedCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventProjectedToEngine_Parms, ProjectedCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::NewProp_ProjectedCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::NewProp_ProjectedCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::NewProp_EngineCoordinates = { "EngineCoordinates", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventProjectedToEngine_Parms, EngineCoordinates), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::NewProp_ProjectedCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::NewProp_EngineCoordinates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Transformations" },
		{ "Comment", "/**\n\x09 * Convert a Vector expressed in PROJECTED CRS to ENGINE space\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Convert a Vector expressed in PROJECTED CRS to ENGINE space" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "ProjectedToEngine", nullptr, nullptr, sizeof(GeoReferencingSystem_eventProjectedToEngine_Parms), Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics
	{
		struct GeoReferencingSystem_eventProjectedToGeographic_Parms
		{
			FCartesianCoordinates ProjectedCoordinates;
			FGeographicCoordinates GeographicCoordinates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectedCoordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectedCoordinates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeographicCoordinates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::NewProp_ProjectedCoordinates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::NewProp_ProjectedCoordinates = { "ProjectedCoordinates", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventProjectedToGeographic_Parms, ProjectedCoordinates), Z_Construct_UScriptStruct_FCartesianCoordinates, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::NewProp_ProjectedCoordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::NewProp_ProjectedCoordinates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::NewProp_GeographicCoordinates = { "GeographicCoordinates", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GeoReferencingSystem_eventProjectedToGeographic_Parms, GeographicCoordinates), Z_Construct_UScriptStruct_FGeographicCoordinates, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::NewProp_ProjectedCoordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::NewProp_GeographicCoordinates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::Function_MetaDataParams[] = {
		{ "Category", "GeoReferencing|Transformations" },
		{ "Comment", "/**\n\x09 * Convert a Coordinate expressed in PROJECTED CRS to GEOGRAPHIC CRS\n\x09 */" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Convert a Coordinate expressed in PROJECTED CRS to GEOGRAPHIC CRS" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGeoReferencingSystem, nullptr, "ProjectedToGeographic", nullptr, nullptr, sizeof(GeoReferencingSystem_eventProjectedToGeographic_Parms), Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AGeoReferencingSystem_NoRegister()
	{
		return AGeoReferencingSystem::StaticClass();
	}
	struct Z_Construct_UClass_AGeoReferencingSystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PlanetShape_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlanetShape_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PlanetShape;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectedCRS_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProjectedCRS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeographicCRS_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_GeographicCRS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOriginAtPlanetCenter_MetaData[];
#endif
		static void NewProp_bOriginAtPlanetCenter_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOriginAtPlanetCenter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOriginLocationInProjectedCRS_MetaData[];
#endif
		static void NewProp_bOriginLocationInProjectedCRS_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOriginLocationInProjectedCRS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginLatitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OriginLatitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginLongitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OriginLongitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginAltitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OriginAltitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginProjectedCoordinatesEasting_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OriginProjectedCoordinatesEasting;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginProjectedCoordinatesNorthing_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OriginProjectedCoordinatesNorthing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginProjectedCoordinatesUp_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OriginProjectedCoordinatesUp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGeoReferencingSystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AInfo,
		(UObject* (*)())Z_Construct_UPackage__Script_GeoReferencing,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AGeoReferencingSystem_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AGeoReferencingSystem_ECEFToEngine, "ECEFToEngine" }, // 595613514
		{ &Z_Construct_UFunction_AGeoReferencingSystem_ECEFToGeographic, "ECEFToGeographic" }, // 2409643006
		{ &Z_Construct_UFunction_AGeoReferencingSystem_ECEFToProjected, "ECEFToProjected" }, // 522280411
		{ &Z_Construct_UFunction_AGeoReferencingSystem_EngineToECEF, "EngineToECEF" }, // 2708692323
		{ &Z_Construct_UFunction_AGeoReferencingSystem_EngineToProjected, "EngineToProjected" }, // 2376269408
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GeographicToECEF, "GeographicToECEF" }, // 186648732
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GeographicToProjected, "GeographicToProjected" }, // 3870176179
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetECEFENUVectorsAtECEFLocation, "GetECEFENUVectorsAtECEFLocation" }, // 3634386148
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtECEFLocation, "GetENUVectorsAtECEFLocation" }, // 2770944684
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtEngineLocation, "GetENUVectorsAtEngineLocation" }, // 1800857653
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtGeographicLocation, "GetENUVectorsAtGeographicLocation" }, // 102217857
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetENUVectorsAtProjectedLocation, "GetENUVectorsAtProjectedLocation" }, // 1318000683
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetGeoReferencingSystem, "GetGeoReferencingSystem" }, // 2576695197
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetPlanetCenterTransform, "GetPlanetCenterTransform" }, // 1426007382
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtECEFLocation, "GetTangentTransformAtECEFLocation" }, // 2735567577
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtEngineLocation, "GetTangentTransformAtEngineLocation" }, // 2446223330
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtGeographicLocation, "GetTangentTransformAtGeographicLocation" }, // 4274977873
		{ &Z_Construct_UFunction_AGeoReferencingSystem_GetTangentTransformAtProjectedLocation, "GetTangentTransformAtProjectedLocation" }, // 3727175609
		{ &Z_Construct_UFunction_AGeoReferencingSystem_IsCRSStringValid, "IsCRSStringValid" }, // 1045932424
		{ &Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToECEF, "ProjectedToECEF" }, // 979732528
		{ &Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToEngine, "ProjectedToEngine" }, // 3029639740
		{ &Z_Construct_UFunction_AGeoReferencingSystem_ProjectedToGeographic, "ProjectedToGeographic" }, // 3644407250
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "HideCategories", "Transform Replication Actor LOD Cooking Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "GeoReferencingSystem.h" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_PlanetShape_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_PlanetShape_MetaData[] = {
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * This mode has to be set consistently with the way you authored your ground geometry.\n\x09 *  - For small environments, a projection is often applied and the world is considered as Flat\n\x09 *  - For planet scale environments, projections is not suitable and the geometry is Rounded.\n\x09 **/" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "This mode has to be set consistently with the way you authored your ground geometry.\n - For small environments, a projection is often applied and the world is considered as Flat\n - For planet scale environments, projections is not suitable and the geometry is Rounded." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_PlanetShape = { "PlanetShape", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGeoReferencingSystem, PlanetShape), Z_Construct_UEnum_GeoReferencing_EPlanetShape, METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_PlanetShape_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_PlanetShape_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_ProjectedCRS_MetaData[] = {
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * String that describes the PROJECTED CRS of choice. \n\x09 *    CRS can be identified by their code (EPSG:4326), a well-known text(WKT) string, or PROJ strings...\n\x09 **/" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "String that describes the PROJECTED CRS of choice.\n   CRS can be identified by their code (EPSG:4326), a well-known text(WKT) string, or PROJ strings..." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_ProjectedCRS = { "ProjectedCRS", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGeoReferencingSystem, ProjectedCRS), METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_ProjectedCRS_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_ProjectedCRS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_GeographicCRS_MetaData[] = {
		{ "Category", "GeoReferencing" },
		{ "Comment", "/**\n\x09 * String that describes the GEOGRAPHIC CRS of choice.\n\x09 *    CRS can be identified by their code (EPSG:4326), a well-known text(WKT) string, or PROJ strings...\n\x09 **/" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "String that describes the GEOGRAPHIC CRS of choice.\n   CRS can be identified by their code (EPSG:4326), a well-known text(WKT) string, or PROJ strings..." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_GeographicCRS = { "GeographicCRS", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGeoReferencingSystem, GeographicCRS), METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_GeographicCRS_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_GeographicCRS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginAtPlanetCenter_MetaData[] = {
		{ "Category", "GeoReferencing|Origin Location" },
		{ "Comment", "/**\n\x09 * if true, the UE origin is located at the Planet Center, otherwise, \n\x09 * the UE origin is assuming to be defined at one specific point of \n\x09 * the planet surface, defined by the properties below. \n\x09 **/" },
		{ "EditCondition", "PlanetShape==EPlanetShape::RoundPlanet" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "if true, the UE origin is located at the Planet Center, otherwise,\nthe UE origin is assuming to be defined at one specific point of\nthe planet surface, defined by the properties below." },
	};
#endif
	void Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginAtPlanetCenter_SetBit(void* Obj)
	{
		((AGeoReferencingSystem*)Obj)->bOriginAtPlanetCenter = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginAtPlanetCenter = { "bOriginAtPlanetCenter", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AGeoReferencingSystem), &Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginAtPlanetCenter_SetBit, METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginAtPlanetCenter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginAtPlanetCenter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginLocationInProjectedCRS_MetaData[] = {
		{ "Category", "GeoReferencing|Origin Location" },
		{ "Comment", "/**\n\x09 * if true, the UE origin georeference is expressed in the PROJECTED CRS. \n\x09 *     (NOT in ECEF ! - Projected worlds are the most frequent use case...)\n\x09 * if false, the origin is located using geographic coordinates. \n\x09 * \n\x09 * WARNING : the location has to be expressed as Integer values because of accuracy. \n\x09 * Be very careful about that when authoring your data in external tools ! \n\x09 **/" },
		{ "EditCondition", "bOriginAtPlanetCenter==false" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "if true, the UE origin georeference is expressed in the PROJECTED CRS.\n    (NOT in ECEF ! - Projected worlds are the most frequent use case...)\nif false, the origin is located using geographic coordinates.\n\nWARNING : the location has to be expressed as Integer values because of accuracy.\nBe very careful about that when authoring your data in external tools !" },
	};
#endif
	void Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginLocationInProjectedCRS_SetBit(void* Obj)
	{
		((AGeoReferencingSystem*)Obj)->bOriginLocationInProjectedCRS = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginLocationInProjectedCRS = { "bOriginLocationInProjectedCRS", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AGeoReferencingSystem), &Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginLocationInProjectedCRS_SetBit, METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginLocationInProjectedCRS_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginLocationInProjectedCRS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginLatitude_MetaData[] = {
		{ "Category", "GeoReferencing|Origin Location" },
		{ "ClampMax", "90" },
		{ "ClampMin", "-90" },
		{ "Comment", "/**\n\x09 * Latitude of UE Origin on planet\n\x09 **/" },
		{ "EditCondition", "!bOriginLocationInProjectedCRS && !bOriginAtPlanetCenter" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Latitude of UE Origin on planet" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginLatitude = { "OriginLatitude", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGeoReferencingSystem, OriginLatitude), METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginLatitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginLatitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginLongitude_MetaData[] = {
		{ "Category", "GeoReferencing|Origin Location" },
		{ "ClampMax", "180" },
		{ "ClampMin", "-180" },
		{ "Comment", "/**\n\x09 * Longitude of UE Origin on planet\n\x09 **/" },
		{ "EditCondition", "!bOriginLocationInProjectedCRS && !bOriginAtPlanetCenter" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Longitude of UE Origin on planet" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginLongitude = { "OriginLongitude", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGeoReferencingSystem, OriginLongitude), METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginLongitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginLongitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginAltitude_MetaData[] = {
		{ "Category", "GeoReferencing|Origin Location" },
		{ "Comment", "/**\n\x09 * Altitude of UE Origin on planet\n\x09 **/" },
		{ "EditCondition", "!bOriginLocationInProjectedCRS && !bOriginAtPlanetCenter" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Altitude of UE Origin on planet" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginAltitude = { "OriginAltitude", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGeoReferencingSystem, OriginAltitude), METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginAltitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginAltitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesEasting_MetaData[] = {
		{ "Category", "GeoReferencing|Origin Location" },
		{ "Comment", "/**\n\x09 * Easting position of UE Origin on planet, express in the Projected CRS Frame\n\x09 **/" },
		{ "EditCondition", "bOriginLocationInProjectedCRS && !bOriginAtPlanetCenter" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Easting position of UE Origin on planet, express in the Projected CRS Frame" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesEasting = { "OriginProjectedCoordinatesEasting", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGeoReferencingSystem, OriginProjectedCoordinatesEasting), METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesEasting_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesEasting_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesNorthing_MetaData[] = {
		{ "Category", "GeoReferencing|Origin Location" },
		{ "Comment", "/**\n\x09 * Northing position of UE Origin on planet, express in the Projected CRS Frame\n\x09 **/" },
		{ "EditCondition", "bOriginLocationInProjectedCRS && !bOriginAtPlanetCenter" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Northing position of UE Origin on planet, express in the Projected CRS Frame" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesNorthing = { "OriginProjectedCoordinatesNorthing", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGeoReferencingSystem, OriginProjectedCoordinatesNorthing), METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesNorthing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesNorthing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesUp_MetaData[] = {
		{ "Category", "GeoReferencing|Origin Location" },
		{ "Comment", "/**\n\x09 * Up position of UE Origin on planet, express in the Projected CRS Frame\n\x09 **/" },
		{ "EditCondition", "bOriginLocationInProjectedCRS && !bOriginAtPlanetCenter" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/GeoReferencingSystem.h" },
		{ "ToolTip", "Up position of UE Origin on planet, express in the Projected CRS Frame" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesUp = { "OriginProjectedCoordinatesUp", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGeoReferencingSystem, OriginProjectedCoordinatesUp), METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesUp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesUp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGeoReferencingSystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_PlanetShape_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_PlanetShape,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_ProjectedCRS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_GeographicCRS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginAtPlanetCenter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_bOriginLocationInProjectedCRS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginLatitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginLongitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginAltitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesEasting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesNorthing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeoReferencingSystem_Statics::NewProp_OriginProjectedCoordinatesUp,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGeoReferencingSystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGeoReferencingSystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGeoReferencingSystem_Statics::ClassParams = {
		&AGeoReferencingSystem::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AGeoReferencingSystem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AGeoReferencingSystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AGeoReferencingSystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGeoReferencingSystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGeoReferencingSystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGeoReferencingSystem, 3281927983);
	template<> GEOREFERENCING_API UClass* StaticClass<AGeoReferencingSystem>()
	{
		return AGeoReferencingSystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGeoReferencingSystem(Z_Construct_UClass_AGeoReferencingSystem, &AGeoReferencingSystem::StaticClass, TEXT("/Script/GeoReferencing"), TEXT("AGeoReferencingSystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGeoReferencingSystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
