// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ResonanceAudioEditor/Private/ResonanceAudioReverbPluginPresetFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeResonanceAudioReverbPluginPresetFactory() {}
// Cross Module References
	RESONANCEAUDIOEDITOR_API UClass* Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_NoRegister();
	RESONANCEAUDIOEDITOR_API UClass* Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_ResonanceAudioEditor();
// End Cross Module References
	void UResonanceAudioReverbPluginPresetFactory::StaticRegisterNativesUResonanceAudioReverbPluginPresetFactory()
	{
	}
	UClass* Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_NoRegister()
	{
		return UResonanceAudioReverbPluginPresetFactory::StaticClass();
	}
	struct Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ResonanceAudioEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "ResonanceAudioReverbPluginPresetFactory.h" },
		{ "ModuleRelativePath", "Private/ResonanceAudioReverbPluginPresetFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UResonanceAudioReverbPluginPresetFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_Statics::ClassParams = {
		&UResonanceAudioReverbPluginPresetFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UResonanceAudioReverbPluginPresetFactory, 2866141208);
	template<> RESONANCEAUDIOEDITOR_API UClass* StaticClass<UResonanceAudioReverbPluginPresetFactory>()
	{
		return UResonanceAudioReverbPluginPresetFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UResonanceAudioReverbPluginPresetFactory(Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory, &UResonanceAudioReverbPluginPresetFactory::StaticClass, TEXT("/Script/ResonanceAudioEditor"), TEXT("UResonanceAudioReverbPluginPresetFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UResonanceAudioReverbPluginPresetFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
