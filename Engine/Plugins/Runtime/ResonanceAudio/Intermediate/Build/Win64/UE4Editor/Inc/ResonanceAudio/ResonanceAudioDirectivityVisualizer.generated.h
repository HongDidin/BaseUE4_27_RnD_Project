// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RESONANCEAUDIO_ResonanceAudioDirectivityVisualizer_generated_h
#error "ResonanceAudioDirectivityVisualizer.generated.h already included, missing '#pragma once' in ResonanceAudioDirectivityVisualizer.h"
#endif
#define RESONANCEAUDIO_ResonanceAudioDirectivityVisualizer_generated_h

#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_SPARSE_DATA
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAResonanceAudioDirectivityVisualizer(); \
	friend struct Z_Construct_UClass_AResonanceAudioDirectivityVisualizer_Statics; \
public: \
	DECLARE_CLASS(AResonanceAudioDirectivityVisualizer, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ResonanceAudio"), NO_API) \
	DECLARE_SERIALIZER(AResonanceAudioDirectivityVisualizer)


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAResonanceAudioDirectivityVisualizer(); \
	friend struct Z_Construct_UClass_AResonanceAudioDirectivityVisualizer_Statics; \
public: \
	DECLARE_CLASS(AResonanceAudioDirectivityVisualizer, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ResonanceAudio"), NO_API) \
	DECLARE_SERIALIZER(AResonanceAudioDirectivityVisualizer)


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AResonanceAudioDirectivityVisualizer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AResonanceAudioDirectivityVisualizer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AResonanceAudioDirectivityVisualizer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AResonanceAudioDirectivityVisualizer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AResonanceAudioDirectivityVisualizer(AResonanceAudioDirectivityVisualizer&&); \
	NO_API AResonanceAudioDirectivityVisualizer(const AResonanceAudioDirectivityVisualizer&); \
public:


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AResonanceAudioDirectivityVisualizer(AResonanceAudioDirectivityVisualizer&&); \
	NO_API AResonanceAudioDirectivityVisualizer(const AResonanceAudioDirectivityVisualizer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AResonanceAudioDirectivityVisualizer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AResonanceAudioDirectivityVisualizer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AResonanceAudioDirectivityVisualizer)


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Material() { return STRUCT_OFFSET(AResonanceAudioDirectivityVisualizer, Material); } \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(AResonanceAudioDirectivityVisualizer, Settings); }


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_16_PROLOG
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_SPARSE_DATA \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_INCLASS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_SPARSE_DATA \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RESONANCEAUDIO_API UClass* StaticClass<class AResonanceAudioDirectivityVisualizer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioDirectivityVisualizer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
