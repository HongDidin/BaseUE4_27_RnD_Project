// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RESONANCEAUDIOEDITOR_ResonanceAudioReverbPluginPresetFactory_generated_h
#error "ResonanceAudioReverbPluginPresetFactory.generated.h already included, missing '#pragma once' in ResonanceAudioReverbPluginPresetFactory.h"
#endif
#define RESONANCEAUDIOEDITOR_ResonanceAudioReverbPluginPresetFactory_generated_h

#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_SPARSE_DATA
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUResonanceAudioReverbPluginPresetFactory(); \
	friend struct Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_Statics; \
public: \
	DECLARE_CLASS(UResonanceAudioReverbPluginPresetFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ResonanceAudioEditor"), RESONANCEAUDIOEDITOR_API) \
	DECLARE_SERIALIZER(UResonanceAudioReverbPluginPresetFactory)


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUResonanceAudioReverbPluginPresetFactory(); \
	friend struct Z_Construct_UClass_UResonanceAudioReverbPluginPresetFactory_Statics; \
public: \
	DECLARE_CLASS(UResonanceAudioReverbPluginPresetFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ResonanceAudioEditor"), RESONANCEAUDIOEDITOR_API) \
	DECLARE_SERIALIZER(UResonanceAudioReverbPluginPresetFactory)


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	RESONANCEAUDIOEDITOR_API UResonanceAudioReverbPluginPresetFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UResonanceAudioReverbPluginPresetFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(RESONANCEAUDIOEDITOR_API, UResonanceAudioReverbPluginPresetFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UResonanceAudioReverbPluginPresetFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	RESONANCEAUDIOEDITOR_API UResonanceAudioReverbPluginPresetFactory(UResonanceAudioReverbPluginPresetFactory&&); \
	RESONANCEAUDIOEDITOR_API UResonanceAudioReverbPluginPresetFactory(const UResonanceAudioReverbPluginPresetFactory&); \
public:


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	RESONANCEAUDIOEDITOR_API UResonanceAudioReverbPluginPresetFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	RESONANCEAUDIOEDITOR_API UResonanceAudioReverbPluginPresetFactory(UResonanceAudioReverbPluginPresetFactory&&); \
	RESONANCEAUDIOEDITOR_API UResonanceAudioReverbPluginPresetFactory(const UResonanceAudioReverbPluginPresetFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(RESONANCEAUDIOEDITOR_API, UResonanceAudioReverbPluginPresetFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UResonanceAudioReverbPluginPresetFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UResonanceAudioReverbPluginPresetFactory)


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_21_PROLOG
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_INCLASS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h_24_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ResonanceAudioReverbPluginPresetFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RESONANCEAUDIOEDITOR_API UClass* StaticClass<class UResonanceAudioReverbPluginPresetFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudioEditor_Private_ResonanceAudioReverbPluginPresetFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
