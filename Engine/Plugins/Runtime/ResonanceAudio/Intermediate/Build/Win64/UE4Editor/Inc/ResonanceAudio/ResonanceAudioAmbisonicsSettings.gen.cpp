// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ResonanceAudio/Public/ResonanceAudioAmbisonicsSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeResonanceAudioAmbisonicsSettings() {}
// Cross Module References
	RESONANCEAUDIO_API UEnum* Z_Construct_UEnum_ResonanceAudio_EResonanceRenderMode();
	UPackage* Z_Construct_UPackage__Script_ResonanceAudio();
	RESONANCEAUDIO_API UClass* Z_Construct_UClass_UResonanceAudioSoundfieldSettings_NoRegister();
	RESONANCEAUDIO_API UClass* Z_Construct_UClass_UResonanceAudioSoundfieldSettings();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_USoundfieldEncodingSettingsBase();
// End Cross Module References
	static UEnum* EResonanceRenderMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ResonanceAudio_EResonanceRenderMode, Z_Construct_UPackage__Script_ResonanceAudio(), TEXT("EResonanceRenderMode"));
		}
		return Singleton;
	}
	template<> RESONANCEAUDIO_API UEnum* StaticEnum<EResonanceRenderMode>()
	{
		return EResonanceRenderMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EResonanceRenderMode(EResonanceRenderMode_StaticEnum, TEXT("/Script/ResonanceAudio"), TEXT("EResonanceRenderMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ResonanceAudio_EResonanceRenderMode_Hash() { return 993892285U; }
	UEnum* Z_Construct_UEnum_ResonanceAudio_EResonanceRenderMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ResonanceAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EResonanceRenderMode"), 0, Get_Z_Construct_UEnum_ResonanceAudio_EResonanceRenderMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EResonanceRenderMode::StereoPanning", (int64)EResonanceRenderMode::StereoPanning },
				{ "EResonanceRenderMode::BinauralLowQuality", (int64)EResonanceRenderMode::BinauralLowQuality },
				{ "EResonanceRenderMode::BinauralMediumQuality", (int64)EResonanceRenderMode::BinauralMediumQuality },
				{ "EResonanceRenderMode::BinauralHighQuality", (int64)EResonanceRenderMode::BinauralHighQuality },
				{ "EResonanceRenderMode::RoomEffectsOnly", (int64)EResonanceRenderMode::RoomEffectsOnly },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BinauralHighQuality.Comment", "// HRTF-based rendering using Third Order Ambisonics, over a virtual array of\n// 26 loudspeakers arranged in a Lebedev grid: https://goo.gl/DX1wh3.\n" },
				{ "BinauralHighQuality.Name", "EResonanceRenderMode::BinauralHighQuality" },
				{ "BinauralHighQuality.ToolTip", "HRTF-based rendering using Third Order Ambisonics, over a virtual array of\n26 loudspeakers arranged in a Lebedev grid: https:goo.gl/DX1wh3." },
				{ "BinauralLowQuality.Comment", "// HRTF-based rendering using First Order Ambisonics, over a virtual array of\n// 8 loudspeakers arranged in a cube configuration around the listener's head.\n" },
				{ "BinauralLowQuality.Name", "EResonanceRenderMode::BinauralLowQuality" },
				{ "BinauralLowQuality.ToolTip", "HRTF-based rendering using First Order Ambisonics, over a virtual array of\n8 loudspeakers arranged in a cube configuration around the listener's head." },
				{ "BinauralMediumQuality.Comment", "// HRTF-based rendering using Second Order Ambisonics, over a virtual array of\n// 12 loudspeakers arranged in a dodecahedral configuration (using faces of\n// the dodecahedron).\n" },
				{ "BinauralMediumQuality.Name", "EResonanceRenderMode::BinauralMediumQuality" },
				{ "BinauralMediumQuality.ToolTip", "HRTF-based rendering using Second Order Ambisonics, over a virtual array of\n12 loudspeakers arranged in a dodecahedral configuration (using faces of\nthe dodecahedron)." },
				{ "ModuleRelativePath", "Public/ResonanceAudioAmbisonicsSettings.h" },
				{ "RoomEffectsOnly.Comment", "// Room effects only rendering. This disables HRTF-based rendering and direct\n// (dry) output of a sound object. Note that this rendering mode should *not*\n// be used for general-purpose sound object spatialization, as it will only\n// render the corresponding room effects of given sound objects without the\n// direct spatialization.\n" },
				{ "RoomEffectsOnly.Name", "EResonanceRenderMode::RoomEffectsOnly" },
				{ "RoomEffectsOnly.ToolTip", "Room effects only rendering. This disables HRTF-based rendering and direct\n(dry) output of a sound object. Note that this rendering mode should *not*\nbe used for general-purpose sound object spatialization, as it will only\nrender the corresponding room effects of given sound objects without the\ndirect spatialization." },
				{ "StereoPanning.Comment", "// Stereo panning, i.e., this disables HRTF-based rendering.\n" },
				{ "StereoPanning.Name", "EResonanceRenderMode::StereoPanning" },
				{ "StereoPanning.ToolTip", "Stereo panning, i.e., this disables HRTF-based rendering." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ResonanceAudio,
				nullptr,
				"EResonanceRenderMode",
				"EResonanceRenderMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UResonanceAudioSoundfieldSettings::StaticRegisterNativesUResonanceAudioSoundfieldSettings()
	{
	}
	UClass* Z_Construct_UClass_UResonanceAudioSoundfieldSettings_NoRegister()
	{
		return UResonanceAudioSoundfieldSettings::StaticClass();
	}
	struct Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RenderMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RenderMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundfieldEncodingSettingsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_ResonanceAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ResonanceAudioAmbisonicsSettings.h" },
		{ "ModuleRelativePath", "Public/ResonanceAudioAmbisonicsSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::NewProp_RenderMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::NewProp_RenderMode_MetaData[] = {
		{ "Category", "Ambisonics" },
		{ "Comment", "//Which order of ambisonics to use for this submix.\n" },
		{ "ModuleRelativePath", "Public/ResonanceAudioAmbisonicsSettings.h" },
		{ "ToolTip", "Which order of ambisonics to use for this submix." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::NewProp_RenderMode = { "RenderMode", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UResonanceAudioSoundfieldSettings, RenderMode), Z_Construct_UEnum_ResonanceAudio_EResonanceRenderMode, METADATA_PARAMS(Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::NewProp_RenderMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::NewProp_RenderMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::NewProp_RenderMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::NewProp_RenderMode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UResonanceAudioSoundfieldSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::ClassParams = {
		&UResonanceAudioSoundfieldSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UResonanceAudioSoundfieldSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UResonanceAudioSoundfieldSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UResonanceAudioSoundfieldSettings, 1037087683);
	template<> RESONANCEAUDIO_API UClass* StaticClass<UResonanceAudioSoundfieldSettings>()
	{
		return UResonanceAudioSoundfieldSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UResonanceAudioSoundfieldSettings(Z_Construct_UClass_UResonanceAudioSoundfieldSettings, &UResonanceAudioSoundfieldSettings::StaticClass, TEXT("/Script/ResonanceAudio"), TEXT("UResonanceAudioSoundfieldSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UResonanceAudioSoundfieldSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
