// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RESONANCEAUDIO_ResonanceAudioSettings_generated_h
#error "ResonanceAudioSettings.generated.h already included, missing '#pragma once' in ResonanceAudioSettings.h"
#endif
#define RESONANCEAUDIO_ResonanceAudioSettings_generated_h

#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_SPARSE_DATA
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUResonanceAudioSettings(); \
	friend struct Z_Construct_UClass_UResonanceAudioSettings_Statics; \
public: \
	DECLARE_CLASS(UResonanceAudioSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/ResonanceAudio"), NO_API) \
	DECLARE_SERIALIZER(UResonanceAudioSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUResonanceAudioSettings(); \
	friend struct Z_Construct_UClass_UResonanceAudioSettings_Statics; \
public: \
	DECLARE_CLASS(UResonanceAudioSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/ResonanceAudio"), NO_API) \
	DECLARE_SERIALIZER(UResonanceAudioSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UResonanceAudioSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UResonanceAudioSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UResonanceAudioSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UResonanceAudioSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UResonanceAudioSettings(UResonanceAudioSettings&&); \
	NO_API UResonanceAudioSettings(const UResonanceAudioSettings&); \
public:


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UResonanceAudioSettings(UResonanceAudioSettings&&); \
	NO_API UResonanceAudioSettings(const UResonanceAudioSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UResonanceAudioSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UResonanceAudioSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UResonanceAudioSettings)


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_10_PROLOG
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_INCLASS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RESONANCEAUDIO_API UClass* StaticClass<class UResonanceAudioSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
