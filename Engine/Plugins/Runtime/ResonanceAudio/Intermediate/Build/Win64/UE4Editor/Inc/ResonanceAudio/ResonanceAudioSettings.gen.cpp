// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ResonanceAudio/Private/ResonanceAudioSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeResonanceAudioSettings() {}
// Cross Module References
	RESONANCEAUDIO_API UClass* Z_Construct_UClass_UResonanceAudioSettings_NoRegister();
	RESONANCEAUDIO_API UClass* Z_Construct_UClass_UResonanceAudioSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ResonanceAudio();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	RESONANCEAUDIO_API UEnum* Z_Construct_UEnum_ResonanceAudio_ERaQualityMode();
// End Cross Module References
	void UResonanceAudioSettings::StaticRegisterNativesUResonanceAudioSettings()
	{
	}
	UClass* Z_Construct_UClass_UResonanceAudioSettings_NoRegister()
	{
		return UResonanceAudioSettings::StaticClass();
	}
	struct Z_Construct_UClass_UResonanceAudioSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputSubmix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputSubmix;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_QualityMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_QualityMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_QualityMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GlobalReverbPreset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GlobalReverbPreset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GlobalSourcePreset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GlobalSourcePreset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UResonanceAudioSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ResonanceAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UResonanceAudioSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ResonanceAudioSettings.h" },
		{ "ModuleRelativePath", "Private/ResonanceAudioSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_OutputSubmix_MetaData[] = {
		{ "AllowedClasses", "SoundSubmix" },
		{ "Category", "Reverb" },
		{ "Comment", "// Reference to submix where reverb plugin audio is routed.\n" },
		{ "ModuleRelativePath", "Private/ResonanceAudioSettings.h" },
		{ "ToolTip", "Reference to submix where reverb plugin audio is routed." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_OutputSubmix = { "OutputSubmix", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UResonanceAudioSettings, OutputSubmix), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_OutputSubmix_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_OutputSubmix_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_QualityMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_QualityMode_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "// Global Quality mode to use for directional sound sources.\n" },
		{ "ModuleRelativePath", "Private/ResonanceAudioSettings.h" },
		{ "ToolTip", "Global Quality mode to use for directional sound sources." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_QualityMode = { "QualityMode", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UResonanceAudioSettings, QualityMode), Z_Construct_UEnum_ResonanceAudio_ERaQualityMode, METADATA_PARAMS(Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_QualityMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_QualityMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_GlobalReverbPreset_MetaData[] = {
		{ "AllowedClasses", "ResonanceAudioReverbPluginPreset" },
		{ "Category", "General" },
		{ "Comment", "// Default settings for global reverb: This is overridden when a player enters Audio Volumes.\n" },
		{ "ModuleRelativePath", "Private/ResonanceAudioSettings.h" },
		{ "ToolTip", "Default settings for global reverb: This is overridden when a player enters Audio Volumes." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_GlobalReverbPreset = { "GlobalReverbPreset", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UResonanceAudioSettings, GlobalReverbPreset), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_GlobalReverbPreset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_GlobalReverbPreset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_GlobalSourcePreset_MetaData[] = {
		{ "AllowedClasses", "ResonanceAudioSpatializationSourceSettings" },
		{ "Category", "General" },
		{ "Comment", "// Default settings for global source settings\n" },
		{ "ModuleRelativePath", "Private/ResonanceAudioSettings.h" },
		{ "ToolTip", "Default settings for global source settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_GlobalSourcePreset = { "GlobalSourcePreset", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UResonanceAudioSettings, GlobalSourcePreset), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_GlobalSourcePreset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_GlobalSourcePreset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UResonanceAudioSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_OutputSubmix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_QualityMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_QualityMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_GlobalReverbPreset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UResonanceAudioSettings_Statics::NewProp_GlobalSourcePreset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UResonanceAudioSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UResonanceAudioSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UResonanceAudioSettings_Statics::ClassParams = {
		&UResonanceAudioSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UResonanceAudioSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UResonanceAudioSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UResonanceAudioSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UResonanceAudioSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UResonanceAudioSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UResonanceAudioSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UResonanceAudioSettings, 381071354);
	template<> RESONANCEAUDIO_API UClass* StaticClass<UResonanceAudioSettings>()
	{
		return UResonanceAudioSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UResonanceAudioSettings(Z_Construct_UClass_UResonanceAudioSettings, &UResonanceAudioSettings::StaticClass, TEXT("/Script/ResonanceAudio"), TEXT("UResonanceAudioSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UResonanceAudioSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
