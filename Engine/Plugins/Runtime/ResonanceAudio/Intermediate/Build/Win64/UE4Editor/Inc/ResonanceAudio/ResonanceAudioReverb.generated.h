// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class ERaMaterialName : uint8;
struct FVector;
struct FQuat;
#ifdef RESONANCEAUDIO_ResonanceAudioReverb_generated_h
#error "ResonanceAudioReverb.generated.h already included, missing '#pragma once' in ResonanceAudioReverb.h"
#endif
#define RESONANCEAUDIO_ResonanceAudioReverb_generated_h

#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FResonanceAudioReverbPluginSettings_Statics; \
	RESONANCEAUDIO_API static class UScriptStruct* StaticStruct();


template<> RESONANCEAUDIO_API UScriptStruct* StaticStruct<struct FResonanceAudioReverbPluginSettings>();

#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_SPARSE_DATA
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetReverbBrightness); \
	DECLARE_FUNCTION(execSetReverbTimeModifier); \
	DECLARE_FUNCTION(execSetReverbGain); \
	DECLARE_FUNCTION(execSetReflectionScalar); \
	DECLARE_FUNCTION(execSetRoomMaterials); \
	DECLARE_FUNCTION(execSetRoomDimensions); \
	DECLARE_FUNCTION(execSetRoomRotation); \
	DECLARE_FUNCTION(execSetRoomPosition); \
	DECLARE_FUNCTION(execSetEnableRoomEffects);


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetReverbBrightness); \
	DECLARE_FUNCTION(execSetReverbTimeModifier); \
	DECLARE_FUNCTION(execSetReverbGain); \
	DECLARE_FUNCTION(execSetReflectionScalar); \
	DECLARE_FUNCTION(execSetRoomMaterials); \
	DECLARE_FUNCTION(execSetRoomDimensions); \
	DECLARE_FUNCTION(execSetRoomRotation); \
	DECLARE_FUNCTION(execSetRoomPosition); \
	DECLARE_FUNCTION(execSetEnableRoomEffects);


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUResonanceAudioReverbPluginPreset(); \
	friend struct Z_Construct_UClass_UResonanceAudioReverbPluginPreset_Statics; \
public: \
	DECLARE_CLASS(UResonanceAudioReverbPluginPreset, USoundEffectSubmixPreset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ResonanceAudio"), NO_API) \
	DECLARE_SERIALIZER(UResonanceAudioReverbPluginPreset)


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_INCLASS \
private: \
	static void StaticRegisterNativesUResonanceAudioReverbPluginPreset(); \
	friend struct Z_Construct_UClass_UResonanceAudioReverbPluginPreset_Statics; \
public: \
	DECLARE_CLASS(UResonanceAudioReverbPluginPreset, USoundEffectSubmixPreset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ResonanceAudio"), NO_API) \
	DECLARE_SERIALIZER(UResonanceAudioReverbPluginPreset)


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UResonanceAudioReverbPluginPreset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UResonanceAudioReverbPluginPreset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UResonanceAudioReverbPluginPreset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UResonanceAudioReverbPluginPreset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UResonanceAudioReverbPluginPreset(UResonanceAudioReverbPluginPreset&&); \
	NO_API UResonanceAudioReverbPluginPreset(const UResonanceAudioReverbPluginPreset&); \
public:


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UResonanceAudioReverbPluginPreset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UResonanceAudioReverbPluginPreset(UResonanceAudioReverbPluginPreset&&); \
	NO_API UResonanceAudioReverbPluginPreset(const UResonanceAudioReverbPluginPreset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UResonanceAudioReverbPluginPreset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UResonanceAudioReverbPluginPreset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UResonanceAudioReverbPluginPreset)


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_174_PROLOG
#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_SPARSE_DATA \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_INCLASS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_SPARSE_DATA \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h_177_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RESONANCEAUDIO_API UClass* StaticClass<class UResonanceAudioReverbPluginPreset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ResonanceAudio_Source_ResonanceAudio_Private_ResonanceAudioReverb_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
