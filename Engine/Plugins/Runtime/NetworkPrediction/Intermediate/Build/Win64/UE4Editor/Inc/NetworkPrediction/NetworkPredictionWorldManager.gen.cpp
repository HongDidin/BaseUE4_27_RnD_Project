// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPrediction/Public/NetworkPredictionWorldManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetworkPredictionWorldManager() {}
// Cross Module References
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_UNetworkPredictionWorldManager_NoRegister();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_UNetworkPredictionWorldManager();
	ENGINE_API UClass* Z_Construct_UClass_UWorldSubsystem();
	UPackage* Z_Construct_UPackage__Script_NetworkPrediction();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_ANetworkPredictionReplicatedManager_NoRegister();
// End Cross Module References
	void UNetworkPredictionWorldManager::StaticRegisterNativesUNetworkPredictionWorldManager()
	{
	}
	UClass* Z_Construct_UClass_UNetworkPredictionWorldManager_NoRegister()
	{
		return UNetworkPredictionWorldManager::StaticClass();
	}
	struct Z_Construct_UClass_UNetworkPredictionWorldManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplicatedManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReplicatedManager;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWorldSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NetworkPredictionWorldManager.h" },
		{ "ModuleRelativePath", "Public/NetworkPredictionWorldManager.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::NewProp_ReplicatedManager_MetaData[] = {
		{ "Comment", "// Server created, replicated manager (only used for centralized/system wide data replication)\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionWorldManager.h" },
		{ "ToolTip", "Server created, replicated manager (only used for centralized/system wide data replication)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::NewProp_ReplicatedManager = { "ReplicatedManager", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNetworkPredictionWorldManager, ReplicatedManager), Z_Construct_UClass_ANetworkPredictionReplicatedManager_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::NewProp_ReplicatedManager_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::NewProp_ReplicatedManager_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::NewProp_ReplicatedManager,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNetworkPredictionWorldManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::ClassParams = {
		&UNetworkPredictionWorldManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNetworkPredictionWorldManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNetworkPredictionWorldManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNetworkPredictionWorldManager, 304946951);
	template<> NETWORKPREDICTION_API UClass* StaticClass<UNetworkPredictionWorldManager>()
	{
		return UNetworkPredictionWorldManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNetworkPredictionWorldManager(Z_Construct_UClass_UNetworkPredictionWorldManager, &UNetworkPredictionWorldManager::StaticClass, TEXT("/Script/NetworkPrediction"), TEXT("UNetworkPredictionWorldManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNetworkPredictionWorldManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
