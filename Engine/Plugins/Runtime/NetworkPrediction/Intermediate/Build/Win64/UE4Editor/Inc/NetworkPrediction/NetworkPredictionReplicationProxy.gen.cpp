// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPrediction/Public/NetworkPredictionReplicationProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetworkPredictionReplicationProxy() {}
// Cross Module References
	NETWORKPREDICTION_API UScriptStruct* Z_Construct_UScriptStruct_FServerReplicationRPCParameter();
	UPackage* Z_Construct_UPackage__Script_NetworkPrediction();
	NETWORKPREDICTION_API UScriptStruct* Z_Construct_UScriptStruct_FReplicationProxy();
// End Cross Module References
class UScriptStruct* FServerReplicationRPCParameter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NETWORKPREDICTION_API uint32 Get_Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FServerReplicationRPCParameter, Z_Construct_UPackage__Script_NetworkPrediction(), TEXT("ServerReplicationRPCParameter"), sizeof(FServerReplicationRPCParameter), Get_Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Hash());
	}
	return Singleton;
}
template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<FServerReplicationRPCParameter>()
{
	return FServerReplicationRPCParameter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FServerReplicationRPCParameter(FServerReplicationRPCParameter::StaticStruct, TEXT("/Script/NetworkPrediction"), TEXT("ServerReplicationRPCParameter"), false, nullptr, nullptr);
static struct FScriptStruct_NetworkPrediction_StaticRegisterNativesFServerReplicationRPCParameter
{
	FScriptStruct_NetworkPrediction_StaticRegisterNativesFServerReplicationRPCParameter()
	{
		UScriptStruct::DeferCppStructOps<FServerReplicationRPCParameter>(FName(TEXT("ServerReplicationRPCParameter")));
	}
} ScriptStruct_NetworkPrediction_StaticRegisterNativesFServerReplicationRPCParameter;
	struct Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// -------------------------------------------------------------------------------------------------------------------------------\n//\x09""FServerRPCProxyParameter\n//\x09Used for the client->Server RPC. Since this is instantiated on the stack by the replication system prior to net serializing,\n//\x09we have no opportunity to point the RPC parameter to the member variables we want. So we serialize into a generic temp byte buffer\n//\x09""and move them into the real buffers on the component in the RPC body (via ::NetSerialzeToProxy).\n// -------------------------------------------------------------------------------------------------------------------------------\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionReplicationProxy.h" },
		{ "ToolTip", "FServerRPCProxyParameter\nUsed for the client->Server RPC. Since this is instantiated on the stack by the replication system prior to net serializing,\nwe have no opportunity to point the RPC parameter to the member variables we want. So we serialize into a generic temp byte buffer\nand move them into the real buffers on the component in the RPC body (via ::NetSerialzeToProxy)." },
	};
#endif
	void* Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FServerReplicationRPCParameter>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
		nullptr,
		&NewStructOps,
		"ServerReplicationRPCParameter",
		sizeof(FServerReplicationRPCParameter),
		alignof(FServerReplicationRPCParameter),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FServerReplicationRPCParameter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPrediction();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ServerReplicationRPCParameter"), sizeof(FServerReplicationRPCParameter), Get_Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Hash() { return 217864969U; }
class UScriptStruct* FReplicationProxy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NETWORKPREDICTION_API uint32 Get_Z_Construct_UScriptStruct_FReplicationProxy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FReplicationProxy, Z_Construct_UPackage__Script_NetworkPrediction(), TEXT("ReplicationProxy"), sizeof(FReplicationProxy), Get_Z_Construct_UScriptStruct_FReplicationProxy_Hash());
	}
	return Singleton;
}
template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<FReplicationProxy>()
{
	return FReplicationProxy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FReplicationProxy(FReplicationProxy::StaticStruct, TEXT("/Script/NetworkPrediction"), TEXT("ReplicationProxy"), false, nullptr, nullptr);
static struct FScriptStruct_NetworkPrediction_StaticRegisterNativesFReplicationProxy
{
	FScriptStruct_NetworkPrediction_StaticRegisterNativesFReplicationProxy()
	{
		UScriptStruct::DeferCppStructOps<FReplicationProxy>(FName(TEXT("ReplicationProxy")));
	}
} ScriptStruct_NetworkPrediction_StaticRegisterNativesFReplicationProxy;
	struct Z_Construct_UScriptStruct_FReplicationProxy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FReplicationProxy_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// Redirects NetSerialize to a dynamically set NetSerializeFunc.\n// This is how we hook into the replication systems role-based serialization\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionReplicationProxy.h" },
		{ "ToolTip", "Redirects NetSerialize to a dynamically set NetSerializeFunc.\nThis is how we hook into the replication systems role-based serialization" },
	};
#endif
	void* Z_Construct_UScriptStruct_FReplicationProxy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FReplicationProxy>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FReplicationProxy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
		nullptr,
		&NewStructOps,
		"ReplicationProxy",
		sizeof(FReplicationProxy),
		alignof(FReplicationProxy),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FReplicationProxy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FReplicationProxy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FReplicationProxy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FReplicationProxy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPrediction();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ReplicationProxy"), sizeof(FReplicationProxy), Get_Z_Construct_UScriptStruct_FReplicationProxy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FReplicationProxy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FReplicationProxy_Hash() { return 1419554116U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
