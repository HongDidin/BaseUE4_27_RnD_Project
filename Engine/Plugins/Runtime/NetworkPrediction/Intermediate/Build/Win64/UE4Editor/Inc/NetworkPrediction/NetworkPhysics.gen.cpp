// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPrediction/Public/NetworkPhysics.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetworkPhysics() {}
// Cross Module References
	NETWORKPREDICTION_API UScriptStruct* Z_Construct_UScriptStruct_FNetworkPhysicsState();
	UPackage* Z_Construct_UPackage__Script_NetworkPrediction();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_UNetworkPhysicsManager_NoRegister();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_UNetworkPhysicsManager();
	ENGINE_API UClass* Z_Construct_UClass_UWorldSubsystem();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_UNetworkPhysicsComponent_NoRegister();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_UNetworkPhysicsComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
// End Cross Module References
class UScriptStruct* FNetworkPhysicsState::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NETWORKPREDICTION_API uint32 Get_Z_Construct_UScriptStruct_FNetworkPhysicsState_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNetworkPhysicsState, Z_Construct_UPackage__Script_NetworkPrediction(), TEXT("NetworkPhysicsState"), sizeof(FNetworkPhysicsState), Get_Z_Construct_UScriptStruct_FNetworkPhysicsState_Hash());
	}
	return Singleton;
}
template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<FNetworkPhysicsState>()
{
	return FNetworkPhysicsState::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNetworkPhysicsState(FNetworkPhysicsState::StaticStruct, TEXT("/Script/NetworkPrediction"), TEXT("NetworkPhysicsState"), false, nullptr, nullptr);
static struct FScriptStruct_NetworkPrediction_StaticRegisterNativesFNetworkPhysicsState
{
	FScriptStruct_NetworkPrediction_StaticRegisterNativesFNetworkPhysicsState()
	{
		UScriptStruct::DeferCppStructOps<FNetworkPhysicsState>(FName(TEXT("NetworkPhysicsState")));
	}
} ScriptStruct_NetworkPrediction_StaticRegisterNativesFNetworkPhysicsState;
	struct Z_Construct_UScriptStruct_FNetworkPhysicsState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPhysicsState_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// PhysicsState that is networked and marshelled between GT and PT\n" },
		{ "ModuleRelativePath", "Public/NetworkPhysics.h" },
		{ "ToolTip", "PhysicsState that is networked and marshelled between GT and PT" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNetworkPhysicsState_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNetworkPhysicsState>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNetworkPhysicsState_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
		nullptr,
		&NewStructOps,
		"NetworkPhysicsState",
		sizeof(FNetworkPhysicsState),
		alignof(FNetworkPhysicsState),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPhysicsState_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPhysicsState_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNetworkPhysicsState()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNetworkPhysicsState_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPrediction();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NetworkPhysicsState"), sizeof(FNetworkPhysicsState), Get_Z_Construct_UScriptStruct_FNetworkPhysicsState_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNetworkPhysicsState_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNetworkPhysicsState_Hash() { return 2277870132U; }
	void UNetworkPhysicsManager::StaticRegisterNativesUNetworkPhysicsManager()
	{
	}
	UClass* Z_Construct_UClass_UNetworkPhysicsManager_NoRegister()
	{
		return UNetworkPhysicsManager::StaticClass();
	}
	struct Z_Construct_UClass_UNetworkPhysicsManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNetworkPhysicsManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWorldSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetworkPhysicsManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NetworkPhysics.h" },
		{ "ModuleRelativePath", "Public/NetworkPhysics.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNetworkPhysicsManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNetworkPhysicsManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNetworkPhysicsManager_Statics::ClassParams = {
		&UNetworkPhysicsManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNetworkPhysicsManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNetworkPhysicsManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNetworkPhysicsManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNetworkPhysicsManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNetworkPhysicsManager, 1409254758);
	template<> NETWORKPREDICTION_API UClass* StaticClass<UNetworkPhysicsManager>()
	{
		return UNetworkPhysicsManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNetworkPhysicsManager(Z_Construct_UClass_UNetworkPhysicsManager, &UNetworkPhysicsManager::StaticClass, TEXT("/Script/NetworkPrediction"), TEXT("UNetworkPhysicsManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNetworkPhysicsManager);
	void UNetworkPhysicsComponent::StaticRegisterNativesUNetworkPhysicsComponent()
	{
	}
	UClass* Z_Construct_UClass_UNetworkPhysicsComponent_NoRegister()
	{
		return UNetworkPhysicsComponent::StaticClass();
	}
	struct Z_Construct_UClass_UNetworkPhysicsComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NetworkPhysicsState_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NetworkPhysicsState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNetworkPhysicsComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetworkPhysicsComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "Comment", "// Helper component for testing\n" },
		{ "IncludePath", "NetworkPhysics.h" },
		{ "ModuleRelativePath", "Public/NetworkPhysics.h" },
		{ "ToolTip", "Helper component for testing" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetworkPhysicsComponent_Statics::NewProp_NetworkPhysicsState_MetaData[] = {
		{ "ModuleRelativePath", "Public/NetworkPhysics.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNetworkPhysicsComponent_Statics::NewProp_NetworkPhysicsState = { "NetworkPhysicsState", nullptr, (EPropertyFlags)0x0010000000002020, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNetworkPhysicsComponent, NetworkPhysicsState), Z_Construct_UScriptStruct_FNetworkPhysicsState, METADATA_PARAMS(Z_Construct_UClass_UNetworkPhysicsComponent_Statics::NewProp_NetworkPhysicsState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNetworkPhysicsComponent_Statics::NewProp_NetworkPhysicsState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNetworkPhysicsComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetworkPhysicsComponent_Statics::NewProp_NetworkPhysicsState,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNetworkPhysicsComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNetworkPhysicsComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNetworkPhysicsComponent_Statics::ClassParams = {
		&UNetworkPhysicsComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNetworkPhysicsComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNetworkPhysicsComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UNetworkPhysicsComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNetworkPhysicsComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNetworkPhysicsComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNetworkPhysicsComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNetworkPhysicsComponent, 2696868680);
	template<> NETWORKPREDICTION_API UClass* StaticClass<UNetworkPhysicsComponent>()
	{
		return UNetworkPhysicsComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNetworkPhysicsComponent(Z_Construct_UClass_UNetworkPhysicsComponent, &UNetworkPhysicsComponent::StaticClass, TEXT("/Script/NetworkPrediction"), TEXT("UNetworkPhysicsComponent"), false, nullptr, nullptr, nullptr);

	void UNetworkPhysicsComponent::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_NetworkPhysicsState(TEXT("NetworkPhysicsState"));

		const bool bIsValid = true
			&& Name_NetworkPhysicsState == ClassReps[(int32)ENetFields_Private::NetworkPhysicsState].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in UNetworkPhysicsComponent"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNetworkPhysicsComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
