// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPrediction/Public/NetworkPredictionSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetworkPredictionSettings() {}
// Cross Module References
	NETWORKPREDICTION_API UScriptStruct* Z_Construct_UScriptStruct_FNetworkPredictionSettings();
	UPackage* Z_Construct_UPackage__Script_NetworkPrediction();
	NETWORKPREDICTION_API UEnum* Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionTickingPolicy();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_ANetworkPredictionReplicatedManager_NoRegister();
	NETWORKPREDICTION_API UEnum* Z_Construct_UEnum_NetworkPrediction_ENetworkLOD();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_UNetworkPredictionSettingsObject_NoRegister();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_UNetworkPredictionSettingsObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FNetworkPredictionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NETWORKPREDICTION_API uint32 Get_Z_Construct_UScriptStruct_FNetworkPredictionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNetworkPredictionSettings, Z_Construct_UPackage__Script_NetworkPrediction(), TEXT("NetworkPredictionSettings"), sizeof(FNetworkPredictionSettings), Get_Z_Construct_UScriptStruct_FNetworkPredictionSettings_Hash());
	}
	return Singleton;
}
template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<FNetworkPredictionSettings>()
{
	return FNetworkPredictionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNetworkPredictionSettings(FNetworkPredictionSettings::StaticStruct, TEXT("/Script/NetworkPrediction"), TEXT("NetworkPredictionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_NetworkPrediction_StaticRegisterNativesFNetworkPredictionSettings
{
	FScriptStruct_NetworkPrediction_StaticRegisterNativesFNetworkPredictionSettings()
	{
		UScriptStruct::DeferCppStructOps<FNetworkPredictionSettings>(FName(TEXT("NetworkPredictionSettings")));
	}
} ScriptStruct_NetworkPrediction_StaticRegisterNativesFNetworkPredictionSettings;
	struct Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PreferredTickingPolicy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreferredTickingPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PreferredTickingPolicy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplicatedManagerClassOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ReplicatedManagerClassOverride;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixedTickFrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FixedTickFrameRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bForceEngineFixTickForcePhysics_MetaData[];
#endif
		static void NewProp_bForceEngineFixTickForcePhysics_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bForceEngineFixTickForcePhysics;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SimulatedProxyNetworkLOD_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimulatedProxyNetworkLOD_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SimulatedProxyNetworkLOD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixedTickInterpolationBufferedMS_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FixedTickInterpolationBufferedMS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IndependentTickInterpolationBufferedMS_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IndependentTickInterpolationBufferedMS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IndependentTickInterpolationMaxBufferedMS_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IndependentTickInterpolationMaxBufferedMS;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNetworkPredictionSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_PreferredTickingPolicy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_PreferredTickingPolicy_MetaData[] = {
		{ "Category", "Global" },
		{ "Comment", "// Which ticking policy to use in cases where both are supported by the underlying simulation.\n// Leave this on Fixed if you intend to use physics based simulations.\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
		{ "ToolTip", "Which ticking policy to use in cases where both are supported by the underlying simulation.\nLeave this on Fixed if you intend to use physics based simulations." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_PreferredTickingPolicy = { "PreferredTickingPolicy", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkPredictionSettings, PreferredTickingPolicy), Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionTickingPolicy, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_PreferredTickingPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_PreferredTickingPolicy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_ReplicatedManagerClassOverride_MetaData[] = {
		{ "Category", "Global" },
		{ "Comment", "// Replicated Manager class\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
		{ "ToolTip", "Replicated Manager class" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_ReplicatedManagerClassOverride = { "ReplicatedManagerClassOverride", nullptr, (EPropertyFlags)0x0014000000004001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkPredictionSettings, ReplicatedManagerClassOverride), Z_Construct_UClass_ANetworkPredictionReplicatedManager_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_ReplicatedManagerClassOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_ReplicatedManagerClassOverride_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_FixedTickFrameRate_MetaData[] = {
		{ "Category", "FixedTick" },
		{ "Comment", "// Frame rate to use when running Fixed Tick simulations. Note: Engine::FixedFrameRate will take precedence if manually set.\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
		{ "ToolTip", "Frame rate to use when running Fixed Tick simulations. Note: Engine::FixedFrameRate will take precedence if manually set." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_FixedTickFrameRate = { "FixedTickFrameRate", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkPredictionSettings, FixedTickFrameRate), METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_FixedTickFrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_FixedTickFrameRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_bForceEngineFixTickForcePhysics_MetaData[] = {
		{ "Category", "FixedTick" },
		{ "Comment", "// Forces the engine to run in fixed tick mode when a NP physics simulation is running.\n// This is the same as settings UEngine::bUseFixedFrameRate / FixedFrameRate manually.\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
		{ "ToolTip", "Forces the engine to run in fixed tick mode when a NP physics simulation is running.\nThis is the same as settings UEngine::bUseFixedFrameRate / FixedFrameRate manually." },
	};
#endif
	void Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_bForceEngineFixTickForcePhysics_SetBit(void* Obj)
	{
		((FNetworkPredictionSettings*)Obj)->bForceEngineFixTickForcePhysics = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_bForceEngineFixTickForcePhysics = { "bForceEngineFixTickForcePhysics", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNetworkPredictionSettings), &Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_bForceEngineFixTickForcePhysics_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_bForceEngineFixTickForcePhysics_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_bForceEngineFixTickForcePhysics_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_SimulatedProxyNetworkLOD_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_SimulatedProxyNetworkLOD_MetaData[] = {
		{ "Category", "FixedTick" },
		{ "Comment", "// Default NetworkLOD for simulated proxy simulations.\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
		{ "ToolTip", "Default NetworkLOD for simulated proxy simulations." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_SimulatedProxyNetworkLOD = { "SimulatedProxyNetworkLOD", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkPredictionSettings, SimulatedProxyNetworkLOD), Z_Construct_UEnum_NetworkPrediction_ENetworkLOD, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_SimulatedProxyNetworkLOD_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_SimulatedProxyNetworkLOD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_FixedTickInterpolationBufferedMS_MetaData[] = {
		{ "Category", "Interpolation" },
		{ "Comment", "// How much buffered time to keep for fixed ticking interpolated sims (client only).\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
		{ "ToolTip", "How much buffered time to keep for fixed ticking interpolated sims (client only)." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_FixedTickInterpolationBufferedMS = { "FixedTickInterpolationBufferedMS", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkPredictionSettings, FixedTickInterpolationBufferedMS), METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_FixedTickInterpolationBufferedMS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_FixedTickInterpolationBufferedMS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_IndependentTickInterpolationBufferedMS_MetaData[] = {
		{ "Category", "Interpolation" },
		{ "Comment", "// How much buffered time to keep for fixed independent interpolated sims (client only).\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
		{ "ToolTip", "How much buffered time to keep for fixed independent interpolated sims (client only)." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_IndependentTickInterpolationBufferedMS = { "IndependentTickInterpolationBufferedMS", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkPredictionSettings, IndependentTickInterpolationBufferedMS), METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_IndependentTickInterpolationBufferedMS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_IndependentTickInterpolationBufferedMS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_IndependentTickInterpolationMaxBufferedMS_MetaData[] = {
		{ "Category", "Interpolation" },
		{ "Comment", "// Max buffered time to keep for fixed independent interpolated sims (client only).\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
		{ "ToolTip", "Max buffered time to keep for fixed independent interpolated sims (client only)." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_IndependentTickInterpolationMaxBufferedMS = { "IndependentTickInterpolationMaxBufferedMS", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkPredictionSettings, IndependentTickInterpolationMaxBufferedMS), METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_IndependentTickInterpolationMaxBufferedMS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_IndependentTickInterpolationMaxBufferedMS_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_PreferredTickingPolicy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_PreferredTickingPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_ReplicatedManagerClassOverride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_FixedTickFrameRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_bForceEngineFixTickForcePhysics,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_SimulatedProxyNetworkLOD_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_SimulatedProxyNetworkLOD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_FixedTickInterpolationBufferedMS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_IndependentTickInterpolationBufferedMS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::NewProp_IndependentTickInterpolationMaxBufferedMS,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
		nullptr,
		&NewStructOps,
		"NetworkPredictionSettings",
		sizeof(FNetworkPredictionSettings),
		alignof(FNetworkPredictionSettings),
		Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNetworkPredictionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNetworkPredictionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPrediction();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NetworkPredictionSettings"), sizeof(FNetworkPredictionSettings), Get_Z_Construct_UScriptStruct_FNetworkPredictionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNetworkPredictionSettings_Hash() { return 4118237313U; }
	void UNetworkPredictionSettingsObject::StaticRegisterNativesUNetworkPredictionSettingsObject()
	{
	}
	UClass* Z_Construct_UClass_UNetworkPredictionSettingsObject_NoRegister()
	{
		return UNetworkPredictionSettingsObject::StaticClass();
	}
	struct Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Network Prediction" },
		{ "IncludePath", "NetworkPredictionSettings.h" },
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::NewProp_Settings_MetaData[] = {
		{ "Category", "Network Prediction" },
		{ "ModuleRelativePath", "Public/NetworkPredictionSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNetworkPredictionSettingsObject, Settings), Z_Construct_UScriptStruct_FNetworkPredictionSettings, METADATA_PARAMS(Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::NewProp_Settings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNetworkPredictionSettingsObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::ClassParams = {
		&UNetworkPredictionSettingsObject::StaticClass,
		"NetworkPrediction",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::PropPointers),
		0,
		0x000002A6u,
		METADATA_PARAMS(Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNetworkPredictionSettingsObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNetworkPredictionSettingsObject, 920260998);
	template<> NETWORKPREDICTION_API UClass* StaticClass<UNetworkPredictionSettingsObject>()
	{
		return UNetworkPredictionSettingsObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNetworkPredictionSettingsObject(Z_Construct_UClass_UNetworkPredictionSettingsObject, &UNetworkPredictionSettingsObject::StaticClass, TEXT("/Script/NetworkPrediction"), TEXT("UNetworkPredictionSettingsObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNetworkPredictionSettingsObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
