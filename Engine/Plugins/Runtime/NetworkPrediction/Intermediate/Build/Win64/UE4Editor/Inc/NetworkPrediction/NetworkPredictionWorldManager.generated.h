// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETWORKPREDICTION_NetworkPredictionWorldManager_generated_h
#error "NetworkPredictionWorldManager.generated.h already included, missing '#pragma once' in NetworkPredictionWorldManager.h"
#endif
#define NETWORKPREDICTION_NetworkPredictionWorldManager_generated_h

#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_RPC_WRAPPERS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNetworkPredictionWorldManager(); \
	friend struct Z_Construct_UClass_UNetworkPredictionWorldManager_Statics; \
public: \
	DECLARE_CLASS(UNetworkPredictionWorldManager, UWorldSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPredictionWorldManager)


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUNetworkPredictionWorldManager(); \
	friend struct Z_Construct_UClass_UNetworkPredictionWorldManager_Statics; \
public: \
	DECLARE_CLASS(UNetworkPredictionWorldManager, UWorldSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPredictionWorldManager)


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetworkPredictionWorldManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetworkPredictionWorldManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPredictionWorldManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPredictionWorldManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPredictionWorldManager(UNetworkPredictionWorldManager&&); \
	NO_API UNetworkPredictionWorldManager(const UNetworkPredictionWorldManager&); \
public:


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPredictionWorldManager(UNetworkPredictionWorldManager&&); \
	NO_API UNetworkPredictionWorldManager(const UNetworkPredictionWorldManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPredictionWorldManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPredictionWorldManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNetworkPredictionWorldManager)


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_22_PROLOG
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_INCLASS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTION_API UClass* StaticClass<class UNetworkPredictionWorldManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionWorldManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
