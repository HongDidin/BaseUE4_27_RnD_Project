// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETWORKPREDICTION_NetworkPredictionSettings_generated_h
#error "NetworkPredictionSettings.generated.h already included, missing '#pragma once' in NetworkPredictionSettings.h"
#endif
#define NETWORKPREDICTION_NetworkPredictionSettings_generated_h

#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNetworkPredictionSettings_Statics; \
	NETWORKPREDICTION_API static class UScriptStruct* StaticStruct();


template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<struct FNetworkPredictionSettings>();

#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_RPC_WRAPPERS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNetworkPredictionSettingsObject(); \
	friend struct Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics; \
public: \
	DECLARE_CLASS(UNetworkPredictionSettingsObject, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPredictionSettingsObject) \
	static const TCHAR* StaticConfigName() {return TEXT("NetworkPrediction");} \



#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_INCLASS \
private: \
	static void StaticRegisterNativesUNetworkPredictionSettingsObject(); \
	friend struct Z_Construct_UClass_UNetworkPredictionSettingsObject_Statics; \
public: \
	DECLARE_CLASS(UNetworkPredictionSettingsObject, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPredictionSettingsObject) \
	static const TCHAR* StaticConfigName() {return TEXT("NetworkPrediction");} \



#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetworkPredictionSettingsObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetworkPredictionSettingsObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPredictionSettingsObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPredictionSettingsObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPredictionSettingsObject(UNetworkPredictionSettingsObject&&); \
	NO_API UNetworkPredictionSettingsObject(const UNetworkPredictionSettingsObject&); \
public:


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetworkPredictionSettingsObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPredictionSettingsObject(UNetworkPredictionSettingsObject&&); \
	NO_API UNetworkPredictionSettingsObject(const UNetworkPredictionSettingsObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPredictionSettingsObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPredictionSettingsObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetworkPredictionSettingsObject)


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_56_PROLOG
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_INCLASS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h_59_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTION_API UClass* StaticClass<class UNetworkPredictionSettingsObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
