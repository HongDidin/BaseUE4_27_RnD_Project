// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPrediction/Public/NetworkPredictionProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetworkPredictionProxy() {}
// Cross Module References
	NETWORKPREDICTION_API UEnum* Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionStateRead();
	UPackage* Z_Construct_UPackage__Script_NetworkPrediction();
	NETWORKPREDICTION_API UScriptStruct* Z_Construct_UScriptStruct_FNetworkPredictionProxy();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_UNetworkPredictionWorldManager_NoRegister();
// End Cross Module References
	static UEnum* ENetworkPredictionStateRead_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionStateRead, Z_Construct_UPackage__Script_NetworkPrediction(), TEXT("ENetworkPredictionStateRead"));
		}
		return Singleton;
	}
	template<> NETWORKPREDICTION_API UEnum* StaticEnum<ENetworkPredictionStateRead>()
	{
		return ENetworkPredictionStateRead_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENetworkPredictionStateRead(ENetworkPredictionStateRead_StaticEnum, TEXT("/Script/NetworkPrediction"), TEXT("ENetworkPredictionStateRead"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionStateRead_Hash() { return 1199970544U; }
	UEnum* Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionStateRead()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPrediction();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENetworkPredictionStateRead"), 0, Get_Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionStateRead_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENetworkPredictionStateRead::Simulation", (int64)ENetworkPredictionStateRead::Simulation },
				{ "ENetworkPredictionStateRead::Presentation", (int64)ENetworkPredictionStateRead::Presentation },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/NetworkPredictionProxy.h" },
				{ "Presentation.Comment", "// The local \"smoothed\" or \"corrected\" state values. If no explicit presentation value is set, Simulation value is implied.\n// Presentation values never feed back into the simulation.\n" },
				{ "Presentation.Name", "ENetworkPredictionStateRead::Presentation" },
				{ "Presentation.ToolTip", "The local \"smoothed\" or \"corrected\" state values. If no explicit presentation value is set, Simulation value is implied.\nPresentation values never feed back into the simulation." },
				{ "Simulation.Comment", "// The authoritative, networked state values.\n" },
				{ "Simulation.Name", "ENetworkPredictionStateRead::Simulation" },
				{ "Simulation.ToolTip", "The authoritative, networked state values." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NetworkPrediction,
				nullptr,
				"ENetworkPredictionStateRead",
				"ENetworkPredictionStateRead",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FNetworkPredictionProxy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NETWORKPREDICTION_API uint32 Get_Z_Construct_UScriptStruct_FNetworkPredictionProxy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNetworkPredictionProxy, Z_Construct_UPackage__Script_NetworkPrediction(), TEXT("NetworkPredictionProxy"), sizeof(FNetworkPredictionProxy), Get_Z_Construct_UScriptStruct_FNetworkPredictionProxy_Hash());
	}
	return Singleton;
}
template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<FNetworkPredictionProxy>()
{
	return FNetworkPredictionProxy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNetworkPredictionProxy(FNetworkPredictionProxy::StaticStruct, TEXT("/Script/NetworkPrediction"), TEXT("NetworkPredictionProxy"), false, nullptr, nullptr);
static struct FScriptStruct_NetworkPrediction_StaticRegisterNativesFNetworkPredictionProxy
{
	FScriptStruct_NetworkPrediction_StaticRegisterNativesFNetworkPredictionProxy()
	{
		UScriptStruct::DeferCppStructOps<FNetworkPredictionProxy>(FName(TEXT("NetworkPredictionProxy")));
	}
} ScriptStruct_NetworkPrediction_StaticRegisterNativesFNetworkPredictionProxy;
	struct Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldManager;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NetworkPredictionProxy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNetworkPredictionProxy>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::NewProp_WorldManager_MetaData[] = {
		{ "ModuleRelativePath", "Public/NetworkPredictionProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::NewProp_WorldManager = { "WorldManager", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkPredictionProxy, WorldManager), Z_Construct_UClass_UNetworkPredictionWorldManager_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::NewProp_WorldManager_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::NewProp_WorldManager_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::NewProp_WorldManager,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
		nullptr,
		&NewStructOps,
		"NetworkPredictionProxy",
		sizeof(FNetworkPredictionProxy),
		alignof(FNetworkPredictionProxy),
		Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNetworkPredictionProxy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNetworkPredictionProxy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPrediction();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NetworkPredictionProxy"), sizeof(FNetworkPredictionProxy), Get_Z_Construct_UScriptStruct_FNetworkPredictionProxy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNetworkPredictionProxy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNetworkPredictionProxy_Hash() { return 814432120U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
