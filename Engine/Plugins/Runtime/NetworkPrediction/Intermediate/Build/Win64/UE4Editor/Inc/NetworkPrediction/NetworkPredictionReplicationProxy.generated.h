// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETWORKPREDICTION_NetworkPredictionReplicationProxy_generated_h
#error "NetworkPredictionReplicationProxy.generated.h already included, missing '#pragma once' in NetworkPredictionReplicationProxy.h"
#endif
#define NETWORKPREDICTION_NetworkPredictionReplicationProxy_generated_h

#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionReplicationProxy_h_90_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FServerReplicationRPCParameter_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<struct FServerReplicationRPCParameter>();

#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionReplicationProxy_h_37_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FReplicationProxy_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<struct FReplicationProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionReplicationProxy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
