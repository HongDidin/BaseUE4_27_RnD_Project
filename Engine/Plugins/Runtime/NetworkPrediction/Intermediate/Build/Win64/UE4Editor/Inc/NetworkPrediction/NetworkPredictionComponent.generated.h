// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FServerReplicationRPCParameter;
#ifdef NETWORKPREDICTION_NetworkPredictionComponent_generated_h
#error "NetworkPredictionComponent.generated.h already included, missing '#pragma once' in NetworkPredictionComponent.h"
#endif
#define NETWORKPREDICTION_NetworkPredictionComponent_generated_h

#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_RPC_WRAPPERS \
	virtual bool ServerReceiveClientInput_Validate(FServerReplicationRPCParameter const& ); \
	virtual void ServerReceiveClientInput_Implementation(FServerReplicationRPCParameter const& ProxyParameter); \
 \
	DECLARE_FUNCTION(execServerReceiveClientInput);


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual bool ServerReceiveClientInput_Validate(FServerReplicationRPCParameter const& ); \
	virtual void ServerReceiveClientInput_Implementation(FServerReplicationRPCParameter const& ProxyParameter); \
 \
	DECLARE_FUNCTION(execServerReceiveClientInput);


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_EVENT_PARMS \
	struct NetworkPredictionComponent_eventServerReceiveClientInput_Parms \
	{ \
		FServerReplicationRPCParameter ProxyParameter; \
	};


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_CALLBACK_WRAPPERS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNetworkPredictionComponent(); \
	friend struct Z_Construct_UClass_UNetworkPredictionComponent_Statics; \
public: \
	DECLARE_CLASS(UNetworkPredictionComponent, UActorComponent, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPredictionComponent) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		NetworkPredictionProxy=NETFIELD_REP_START, \
		ReplicationProxy_Autonomous, \
		ReplicationProxy_Simulated, \
		ReplicationProxy_Replay, \
		NETFIELD_REP_END=ReplicationProxy_Replay	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUNetworkPredictionComponent(); \
	friend struct Z_Construct_UClass_UNetworkPredictionComponent_Statics; \
public: \
	DECLARE_CLASS(UNetworkPredictionComponent, UActorComponent, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPredictionComponent) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		NetworkPredictionProxy=NETFIELD_REP_START, \
		ReplicationProxy_Autonomous, \
		ReplicationProxy_Simulated, \
		ReplicationProxy_Replay, \
		NETFIELD_REP_END=ReplicationProxy_Replay	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetworkPredictionComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetworkPredictionComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPredictionComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPredictionComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPredictionComponent(UNetworkPredictionComponent&&); \
	NO_API UNetworkPredictionComponent(const UNetworkPredictionComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPredictionComponent(UNetworkPredictionComponent&&); \
	NO_API UNetworkPredictionComponent(const UNetworkPredictionComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPredictionComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPredictionComponent); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UNetworkPredictionComponent)


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__NetworkPredictionProxy() { return STRUCT_OFFSET(UNetworkPredictionComponent, NetworkPredictionProxy); } \
	FORCEINLINE static uint32 __PPO__ReplicationProxy_ServerRPC() { return STRUCT_OFFSET(UNetworkPredictionComponent, ReplicationProxy_ServerRPC); } \
	FORCEINLINE static uint32 __PPO__ReplicationProxy_Autonomous() { return STRUCT_OFFSET(UNetworkPredictionComponent, ReplicationProxy_Autonomous); } \
	FORCEINLINE static uint32 __PPO__ReplicationProxy_Simulated() { return STRUCT_OFFSET(UNetworkPredictionComponent, ReplicationProxy_Simulated); } \
	FORCEINLINE static uint32 __PPO__ReplicationProxy_Replay() { return STRUCT_OFFSET(UNetworkPredictionComponent, ReplicationProxy_Replay); }


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_21_PROLOG \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_EVENT_PARMS


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_INCLASS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTION_API UClass* StaticClass<class UNetworkPredictionComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
