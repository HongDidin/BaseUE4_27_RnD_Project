// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETWORKPREDICTION_NetworkPredictionPhysicsComponent_generated_h
#error "NetworkPredictionPhysicsComponent.generated.h already included, missing '#pragma once' in NetworkPredictionPhysicsComponent.h"
#endif
#define NETWORKPREDICTION_NetworkPredictionPhysicsComponent_generated_h

#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_RPC_WRAPPERS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNetworkPredictionPhysicsComponent(); \
	friend struct Z_Construct_UClass_UNetworkPredictionPhysicsComponent_Statics; \
public: \
	DECLARE_CLASS(UNetworkPredictionPhysicsComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPredictionPhysicsComponent) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		NetworkPredictionProxy=NETFIELD_REP_START, \
		ReplicationProxy, \
		NETFIELD_REP_END=ReplicationProxy	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUNetworkPredictionPhysicsComponent(); \
	friend struct Z_Construct_UClass_UNetworkPredictionPhysicsComponent_Statics; \
public: \
	DECLARE_CLASS(UNetworkPredictionPhysicsComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPredictionPhysicsComponent) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		NetworkPredictionProxy=NETFIELD_REP_START, \
		ReplicationProxy, \
		NETFIELD_REP_END=ReplicationProxy	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetworkPredictionPhysicsComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetworkPredictionPhysicsComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPredictionPhysicsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPredictionPhysicsComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPredictionPhysicsComponent(UNetworkPredictionPhysicsComponent&&); \
	NO_API UNetworkPredictionPhysicsComponent(const UNetworkPredictionPhysicsComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPredictionPhysicsComponent(UNetworkPredictionPhysicsComponent&&); \
	NO_API UNetworkPredictionPhysicsComponent(const UNetworkPredictionPhysicsComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPredictionPhysicsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPredictionPhysicsComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNetworkPredictionPhysicsComponent)


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__NetworkPredictionProxy() { return STRUCT_OFFSET(UNetworkPredictionPhysicsComponent, NetworkPredictionProxy); } \
	FORCEINLINE static uint32 __PPO__UpdatedPrimitive() { return STRUCT_OFFSET(UNetworkPredictionPhysicsComponent, UpdatedPrimitive); } \
	FORCEINLINE static uint32 __PPO__ReplicationProxy() { return STRUCT_OFFSET(UNetworkPredictionPhysicsComponent, ReplicationProxy); }


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_24_PROLOG
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_INCLASS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTION_API UClass* StaticClass<class UNetworkPredictionPhysicsComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPredictionPhysicsComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
