// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPrediction/Public/NetworkPredictionReplicatedManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetworkPredictionReplicatedManager() {}
// Cross Module References
	NETWORKPREDICTION_API UScriptStruct* Z_Construct_UScriptStruct_FSharedPackageMap();
	UPackage* Z_Construct_UPackage__Script_NetworkPrediction();
	NETWORKPREDICTION_API UScriptStruct* Z_Construct_UScriptStruct_FSharedPackageMapItem();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_ANetworkPredictionReplicatedManager_NoRegister();
	NETWORKPREDICTION_API UClass* Z_Construct_UClass_ANetworkPredictionReplicatedManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
// End Cross Module References
class UScriptStruct* FSharedPackageMap::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NETWORKPREDICTION_API uint32 Get_Z_Construct_UScriptStruct_FSharedPackageMap_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSharedPackageMap, Z_Construct_UPackage__Script_NetworkPrediction(), TEXT("SharedPackageMap"), sizeof(FSharedPackageMap), Get_Z_Construct_UScriptStruct_FSharedPackageMap_Hash());
	}
	return Singleton;
}
template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<FSharedPackageMap>()
{
	return FSharedPackageMap::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSharedPackageMap(FSharedPackageMap::StaticStruct, TEXT("/Script/NetworkPrediction"), TEXT("SharedPackageMap"), false, nullptr, nullptr);
static struct FScriptStruct_NetworkPrediction_StaticRegisterNativesFSharedPackageMap
{
	FScriptStruct_NetworkPrediction_StaticRegisterNativesFSharedPackageMap()
	{
		UScriptStruct::DeferCppStructOps<FSharedPackageMap>(FName(TEXT("SharedPackageMap")));
	}
} ScriptStruct_NetworkPrediction_StaticRegisterNativesFSharedPackageMap;
	struct Z_Construct_UScriptStruct_FSharedPackageMap_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Items_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Items_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Items;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSharedPackageMap_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NetworkPredictionReplicatedManager.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSharedPackageMap_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSharedPackageMap>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSharedPackageMap_Statics::NewProp_Items_Inner = { "Items", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSharedPackageMapItem, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSharedPackageMap_Statics::NewProp_Items_MetaData[] = {
		{ "ModuleRelativePath", "Public/NetworkPredictionReplicatedManager.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSharedPackageMap_Statics::NewProp_Items = { "Items", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSharedPackageMap, Items), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSharedPackageMap_Statics::NewProp_Items_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSharedPackageMap_Statics::NewProp_Items_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSharedPackageMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSharedPackageMap_Statics::NewProp_Items_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSharedPackageMap_Statics::NewProp_Items,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSharedPackageMap_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
		nullptr,
		&NewStructOps,
		"SharedPackageMap",
		sizeof(FSharedPackageMap),
		alignof(FSharedPackageMap),
		Z_Construct_UScriptStruct_FSharedPackageMap_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSharedPackageMap_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSharedPackageMap_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSharedPackageMap_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSharedPackageMap()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSharedPackageMap_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPrediction();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SharedPackageMap"), sizeof(FSharedPackageMap), Get_Z_Construct_UScriptStruct_FSharedPackageMap_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSharedPackageMap_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSharedPackageMap_Hash() { return 1297883203U; }
class UScriptStruct* FSharedPackageMapItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NETWORKPREDICTION_API uint32 Get_Z_Construct_UScriptStruct_FSharedPackageMapItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSharedPackageMapItem, Z_Construct_UPackage__Script_NetworkPrediction(), TEXT("SharedPackageMapItem"), sizeof(FSharedPackageMapItem), Get_Z_Construct_UScriptStruct_FSharedPackageMapItem_Hash());
	}
	return Singleton;
}
template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<FSharedPackageMapItem>()
{
	return FSharedPackageMapItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSharedPackageMapItem(FSharedPackageMapItem::StaticStruct, TEXT("/Script/NetworkPrediction"), TEXT("SharedPackageMapItem"), false, nullptr, nullptr);
static struct FScriptStruct_NetworkPrediction_StaticRegisterNativesFSharedPackageMapItem
{
	FScriptStruct_NetworkPrediction_StaticRegisterNativesFSharedPackageMapItem()
	{
		UScriptStruct::DeferCppStructOps<FSharedPackageMapItem>(FName(TEXT("SharedPackageMapItem")));
	}
} ScriptStruct_NetworkPrediction_StaticRegisterNativesFSharedPackageMapItem;
	struct Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SoftPtr;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// This is a replicated \"manager\" for network prediction. Its purpose is only to replicate system-wide data that is not bound to an actor.\n// Currently this is only to house a \"mini packagemap\" which allows stable shared indices that map to a small set of uobjects to be.\n// UPackageMap can assign per-client net indices which invalidates sharing as well as forces 32 bit guis. this is a more specialzed case\n// where we want to replicate IDs as btyes.\n" },
		{ "ModuleRelativePath", "Public/NetworkPredictionReplicatedManager.h" },
		{ "ToolTip", "This is a replicated \"manager\" for network prediction. Its purpose is only to replicate system-wide data that is not bound to an actor.\nCurrently this is only to house a \"mini packagemap\" which allows stable shared indices that map to a small set of uobjects to be.\nUPackageMap can assign per-client net indices which invalidates sharing as well as forces 32 bit guis. this is a more specialzed case\nwhere we want to replicate IDs as btyes." },
	};
#endif
	void* Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSharedPackageMapItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::NewProp_SoftPtr_MetaData[] = {
		{ "ModuleRelativePath", "Public/NetworkPredictionReplicatedManager.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::NewProp_SoftPtr = { "SoftPtr", nullptr, (EPropertyFlags)0x0014000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSharedPackageMapItem, SoftPtr), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::NewProp_SoftPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::NewProp_SoftPtr_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::NewProp_SoftPtr,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
		nullptr,
		&NewStructOps,
		"SharedPackageMapItem",
		sizeof(FSharedPackageMapItem),
		alignof(FSharedPackageMapItem),
		Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSharedPackageMapItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSharedPackageMapItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPrediction();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SharedPackageMapItem"), sizeof(FSharedPackageMapItem), Get_Z_Construct_UScriptStruct_FSharedPackageMapItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSharedPackageMapItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSharedPackageMapItem_Hash() { return 108458613U; }
	void ANetworkPredictionReplicatedManager::StaticRegisterNativesANetworkPredictionReplicatedManager()
	{
	}
	UClass* Z_Construct_UClass_ANetworkPredictionReplicatedManager_NoRegister()
	{
		return ANetworkPredictionReplicatedManager::StaticClass();
	}
	struct Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SharedPackageMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SharedPackageMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_NetworkPrediction,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NetworkPredictionReplicatedManager.h" },
		{ "ModuleRelativePath", "Public/NetworkPredictionReplicatedManager.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::NewProp_SharedPackageMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/NetworkPredictionReplicatedManager.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::NewProp_SharedPackageMap = { "SharedPackageMap", nullptr, (EPropertyFlags)0x0040000000000020, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANetworkPredictionReplicatedManager, SharedPackageMap), Z_Construct_UScriptStruct_FSharedPackageMap, METADATA_PARAMS(Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::NewProp_SharedPackageMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::NewProp_SharedPackageMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::NewProp_SharedPackageMap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANetworkPredictionReplicatedManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::ClassParams = {
		&ANetworkPredictionReplicatedManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANetworkPredictionReplicatedManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANetworkPredictionReplicatedManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANetworkPredictionReplicatedManager, 3865707446);
	template<> NETWORKPREDICTION_API UClass* StaticClass<ANetworkPredictionReplicatedManager>()
	{
		return ANetworkPredictionReplicatedManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANetworkPredictionReplicatedManager(Z_Construct_UClass_ANetworkPredictionReplicatedManager, &ANetworkPredictionReplicatedManager::StaticClass, TEXT("/Script/NetworkPrediction"), TEXT("ANetworkPredictionReplicatedManager"), false, nullptr, nullptr, nullptr);

	void ANetworkPredictionReplicatedManager::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_SharedPackageMap(TEXT("SharedPackageMap"));

		const bool bIsValid = true
			&& Name_SharedPackageMap == ClassReps[(int32)ENetFields_Private::SharedPackageMap].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in ANetworkPredictionReplicatedManager"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANetworkPredictionReplicatedManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
