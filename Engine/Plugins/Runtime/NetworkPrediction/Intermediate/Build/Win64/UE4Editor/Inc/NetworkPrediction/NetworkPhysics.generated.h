// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETWORKPREDICTION_NetworkPhysics_generated_h
#error "NetworkPhysics.generated.h already included, missing '#pragma once' in NetworkPhysics.h"
#endif
#define NETWORKPREDICTION_NetworkPhysics_generated_h

#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNetworkPhysicsState_Statics; \
	NETWORKPREDICTION_API static class UScriptStruct* StaticStruct();


template<> NETWORKPREDICTION_API UScriptStruct* StaticStruct<struct FNetworkPhysicsState>();

#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_RPC_WRAPPERS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNetworkPhysicsManager(); \
	friend struct Z_Construct_UClass_UNetworkPhysicsManager_Statics; \
public: \
	DECLARE_CLASS(UNetworkPhysicsManager, UWorldSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPhysicsManager)


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_INCLASS \
private: \
	static void StaticRegisterNativesUNetworkPhysicsManager(); \
	friend struct Z_Construct_UClass_UNetworkPhysicsManager_Statics; \
public: \
	DECLARE_CLASS(UNetworkPhysicsManager, UWorldSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPhysicsManager)


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetworkPhysicsManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetworkPhysicsManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPhysicsManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPhysicsManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPhysicsManager(UNetworkPhysicsManager&&); \
	NO_API UNetworkPhysicsManager(const UNetworkPhysicsManager&); \
public:


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPhysicsManager(UNetworkPhysicsManager&&); \
	NO_API UNetworkPhysicsManager(const UNetworkPhysicsManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPhysicsManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPhysicsManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNetworkPhysicsManager)


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_72_PROLOG
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_INCLASS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_77_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTION_API UClass* StaticClass<class UNetworkPhysicsManager>();

#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_SPARSE_DATA
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_RPC_WRAPPERS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNetworkPhysicsComponent(); \
	friend struct Z_Construct_UClass_UNetworkPhysicsComponent_Statics; \
public: \
	DECLARE_CLASS(UNetworkPhysicsComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPhysicsComponent) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		NetworkPhysicsState=NETFIELD_REP_START, \
		NETFIELD_REP_END=NetworkPhysicsState	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_INCLASS \
private: \
	static void StaticRegisterNativesUNetworkPhysicsComponent(); \
	friend struct Z_Construct_UClass_UNetworkPhysicsComponent_Statics; \
public: \
	DECLARE_CLASS(UNetworkPhysicsComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetworkPrediction"), NO_API) \
	DECLARE_SERIALIZER(UNetworkPhysicsComponent) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		NetworkPhysicsState=NETFIELD_REP_START, \
		NETFIELD_REP_END=NetworkPhysicsState	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetworkPhysicsComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetworkPhysicsComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPhysicsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPhysicsComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPhysicsComponent(UNetworkPhysicsComponent&&); \
	NO_API UNetworkPhysicsComponent(const UNetworkPhysicsComponent&); \
public:


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetworkPhysicsComponent(UNetworkPhysicsComponent&&); \
	NO_API UNetworkPhysicsComponent(const UNetworkPhysicsComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetworkPhysicsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetworkPhysicsComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNetworkPhysicsComponent)


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_114_PROLOG
#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_RPC_WRAPPERS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_INCLASS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_SPARSE_DATA \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h_117_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETWORKPREDICTION_API UClass* StaticClass<class UNetworkPhysicsComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_NetworkPrediction_Source_NetworkPrediction_Public_NetworkPhysics_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
