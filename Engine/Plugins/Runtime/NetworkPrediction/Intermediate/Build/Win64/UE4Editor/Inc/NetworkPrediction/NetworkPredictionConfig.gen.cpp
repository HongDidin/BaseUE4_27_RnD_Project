// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkPrediction/Public/NetworkPredictionConfig.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetworkPredictionConfig() {}
// Cross Module References
	NETWORKPREDICTION_API UEnum* Z_Construct_UEnum_NetworkPrediction_ENetworkLOD();
	UPackage* Z_Construct_UPackage__Script_NetworkPrediction();
	NETWORKPREDICTION_API UEnum* Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionTickingPolicy();
// End Cross Module References
	static UEnum* ENetworkLOD_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NetworkPrediction_ENetworkLOD, Z_Construct_UPackage__Script_NetworkPrediction(), TEXT("ENetworkLOD"));
		}
		return Singleton;
	}
	template<> NETWORKPREDICTION_API UEnum* StaticEnum<ENetworkLOD>()
	{
		return ENetworkLOD_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENetworkLOD(ENetworkLOD_StaticEnum, TEXT("/Script/NetworkPrediction"), TEXT("ENetworkLOD"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NetworkPrediction_ENetworkLOD_Hash() { return 2867087508U; }
	UEnum* Z_Construct_UEnum_NetworkPrediction_ENetworkLOD()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPrediction();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENetworkLOD"), 0, Get_Z_Construct_UEnum_NetworkPrediction_ENetworkLOD_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENetworkLOD::Interpolated", (int64)ENetworkLOD::Interpolated },
				{ "ENetworkLOD::SimExtrapolate", (int64)ENetworkLOD::SimExtrapolate },
				{ "ENetworkLOD::ForwardPredict", (int64)ENetworkLOD::ForwardPredict },
				{ "ENetworkLOD::All", (int64)ENetworkLOD::All },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "All.Hidden", "" },
				{ "All.Name", "ENetworkLOD::All" },
				{ "Comment", "// Must be kept in sync with ENP_NetworkLOD\n" },
				{ "ForwardPredict.Comment", "// Not currently implemented\n" },
				{ "ForwardPredict.Name", "ENetworkLOD::ForwardPredict" },
				{ "ForwardPredict.ToolTip", "Not currently implemented" },
				{ "Interpolated.Name", "ENetworkLOD::Interpolated" },
				{ "ModuleRelativePath", "Public/NetworkPredictionConfig.h" },
				{ "SimExtrapolate.Hidden", "" },
				{ "SimExtrapolate.Name", "ENetworkLOD::SimExtrapolate" },
				{ "ToolTip", "Must be kept in sync with ENP_NetworkLOD" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NetworkPrediction,
				nullptr,
				"ENetworkLOD",
				"ENetworkLOD",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENetworkPredictionTickingPolicy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionTickingPolicy, Z_Construct_UPackage__Script_NetworkPrediction(), TEXT("ENetworkPredictionTickingPolicy"));
		}
		return Singleton;
	}
	template<> NETWORKPREDICTION_API UEnum* StaticEnum<ENetworkPredictionTickingPolicy>()
	{
		return ENetworkPredictionTickingPolicy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENetworkPredictionTickingPolicy(ENetworkPredictionTickingPolicy_StaticEnum, TEXT("/Script/NetworkPrediction"), TEXT("ENetworkPredictionTickingPolicy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionTickingPolicy_Hash() { return 2278903551U; }
	UEnum* Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionTickingPolicy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NetworkPrediction();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENetworkPredictionTickingPolicy"), 0, Get_Z_Construct_UEnum_NetworkPrediction_ENetworkPredictionTickingPolicy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENetworkPredictionTickingPolicy::Independent", (int64)ENetworkPredictionTickingPolicy::Independent },
				{ "ENetworkPredictionTickingPolicy::Fixed", (int64)ENetworkPredictionTickingPolicy::Fixed },
				{ "ENetworkPredictionTickingPolicy::All", (int64)ENetworkPredictionTickingPolicy::All },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "All.Comment", "// Everyone ticks at same fixed rate. Supports group rollback and physics.\n" },
				{ "All.Hidden", "" },
				{ "All.Name", "ENetworkPredictionTickingPolicy::All" },
				{ "All.ToolTip", "Everyone ticks at same fixed rate. Supports group rollback and physics." },
				{ "Comment", "// Must be kept in sync with ENP_TickingPolicy\n" },
				{ "Fixed.Comment", "// Client ticks at local frame rate. Server ticks clients independently at client input cmd rate.\n" },
				{ "Fixed.Name", "ENetworkPredictionTickingPolicy::Fixed" },
				{ "Fixed.ToolTip", "Client ticks at local frame rate. Server ticks clients independently at client input cmd rate." },
				{ "Independent.Name", "ENetworkPredictionTickingPolicy::Independent" },
				{ "ModuleRelativePath", "Public/NetworkPredictionConfig.h" },
				{ "ToolTip", "Must be kept in sync with ENP_TickingPolicy" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NetworkPrediction,
				nullptr,
				"ENetworkPredictionTickingPolicy",
				"ENetworkPredictionTickingPolicy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
