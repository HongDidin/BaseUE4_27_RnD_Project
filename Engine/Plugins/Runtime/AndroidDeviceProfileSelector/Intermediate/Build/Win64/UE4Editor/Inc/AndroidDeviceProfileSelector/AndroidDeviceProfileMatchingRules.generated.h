// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ANDROIDDEVICEPROFILESELECTOR_AndroidDeviceProfileMatchingRules_generated_h
#error "AndroidDeviceProfileMatchingRules.generated.h already included, missing '#pragma once' in AndroidDeviceProfileMatchingRules.h"
#endif
#define ANDROIDDEVICEPROFILESELECTOR_AndroidDeviceProfileMatchingRules_generated_h

#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_67_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FProfileMatch_Statics; \
	ANDROIDDEVICEPROFILESELECTOR_API static class UScriptStruct* StaticStruct();


template<> ANDROIDDEVICEPROFILESELECTOR_API UScriptStruct* StaticStruct<struct FProfileMatch>();

#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_52_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FProfileMatchItem_Statics; \
	ANDROIDDEVICEPROFILESELECTOR_API static class UScriptStruct* StaticStruct();


template<> ANDROIDDEVICEPROFILESELECTOR_API UScriptStruct* StaticStruct<struct FProfileMatchItem>();

#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_SPARSE_DATA
#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAndroidDeviceProfileMatchingRules(); \
	friend struct Z_Construct_UClass_UAndroidDeviceProfileMatchingRules_Statics; \
public: \
	DECLARE_CLASS(UAndroidDeviceProfileMatchingRules, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AndroidDeviceProfileSelector"), NO_API) \
	DECLARE_SERIALIZER(UAndroidDeviceProfileMatchingRules) \
	static const TCHAR* StaticConfigName() {return TEXT("DeviceProfiles");} \



#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_INCLASS \
private: \
	static void StaticRegisterNativesUAndroidDeviceProfileMatchingRules(); \
	friend struct Z_Construct_UClass_UAndroidDeviceProfileMatchingRules_Statics; \
public: \
	DECLARE_CLASS(UAndroidDeviceProfileMatchingRules, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AndroidDeviceProfileSelector"), NO_API) \
	DECLARE_SERIALIZER(UAndroidDeviceProfileMatchingRules) \
	static const TCHAR* StaticConfigName() {return TEXT("DeviceProfiles");} \



#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAndroidDeviceProfileMatchingRules(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAndroidDeviceProfileMatchingRules) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAndroidDeviceProfileMatchingRules); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAndroidDeviceProfileMatchingRules); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAndroidDeviceProfileMatchingRules(UAndroidDeviceProfileMatchingRules&&); \
	NO_API UAndroidDeviceProfileMatchingRules(const UAndroidDeviceProfileMatchingRules&); \
public:


#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAndroidDeviceProfileMatchingRules(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAndroidDeviceProfileMatchingRules(UAndroidDeviceProfileMatchingRules&&); \
	NO_API UAndroidDeviceProfileMatchingRules(const UAndroidDeviceProfileMatchingRules&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAndroidDeviceProfileMatchingRules); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAndroidDeviceProfileMatchingRules); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAndroidDeviceProfileMatchingRules)


#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_76_PROLOG
#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_SPARSE_DATA \
	Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_INCLASS \
	Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_SPARSE_DATA \
	Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h_79_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AndroidDeviceProfileMatchingRules."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ANDROIDDEVICEPROFILESELECTOR_API UClass* StaticClass<class UAndroidDeviceProfileMatchingRules>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AndroidDeviceProfileSelector_Source_AndroidDeviceProfileSelector_Private_AndroidDeviceProfileMatchingRules_h


#define FOREACH_ENUM_ECOMPARETYPE(op) \
	op(CMP_Equal) \
	op(CMP_Less) \
	op(CMP_LessEqual) \
	op(CMP_Greater) \
	op(CMP_GreaterEqual) \
	op(CMP_NotEqual) \
	op(CMP_Regex) \
	op(CMP_EqualIgnore) \
	op(CMP_LessIgnore) \
	op(CMP_LessEqualIgnore) \
	op(CMP_GreaterIgnore) \
	op(CMP_GreaterEqualIgnore) \
	op(CMP_NotEqualIgnore) \
	op(CMP_Hash) 
#define FOREACH_ENUM_ESOURCETYPE(op) \
	op(SRC_PreviousRegexMatch) \
	op(SRC_GpuFamily) \
	op(SRC_GlVersion) \
	op(SRC_AndroidVersion) \
	op(SRC_DeviceMake) \
	op(SRC_DeviceModel) \
	op(SRC_DeviceBuildNumber) \
	op(SRC_VulkanVersion) \
	op(SRC_UsingHoudini) \
	op(SRC_VulkanAvailable) \
	op(SRC_CommandLine) \
	op(SRC_Hardware) \
	op(SRC_Chipset) \
	op(SRC_ConfigRuleVar) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
