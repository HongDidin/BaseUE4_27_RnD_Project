// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOUNDCUETEMPLATES_SoundCueTemplate_generated_h
#error "SoundCueTemplate.generated.h already included, missing '#pragma once' in SoundCueTemplate.h"
#endif
#define SOUNDCUETEMPLATES_SoundCueTemplate_generated_h

#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_SPARSE_DATA
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_RPC_WRAPPERS
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundCueTemplate(); \
	friend struct Z_Construct_UClass_USoundCueTemplate_Statics; \
public: \
	DECLARE_CLASS(USoundCueTemplate, USoundCue, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/SoundCueTemplates"), NO_API) \
	DECLARE_SERIALIZER(USoundCueTemplate)


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUSoundCueTemplate(); \
	friend struct Z_Construct_UClass_USoundCueTemplate_Statics; \
public: \
	DECLARE_CLASS(USoundCueTemplate, USoundCue, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/SoundCueTemplates"), NO_API) \
	DECLARE_SERIALIZER(USoundCueTemplate)


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundCueTemplate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundCueTemplate) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundCueTemplate); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundCueTemplate); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundCueTemplate(USoundCueTemplate&&); \
	NO_API USoundCueTemplate(const USoundCueTemplate&); \
public:


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundCueTemplate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundCueTemplate(USoundCueTemplate&&); \
	NO_API USoundCueTemplate(const USoundCueTemplate&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundCueTemplate); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundCueTemplate); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundCueTemplate)


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_21_PROLOG
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_INCLASS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h_24_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundCueTemplate."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOUNDCUETEMPLATES_API UClass* StaticClass<class USoundCueTemplate>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplate_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
