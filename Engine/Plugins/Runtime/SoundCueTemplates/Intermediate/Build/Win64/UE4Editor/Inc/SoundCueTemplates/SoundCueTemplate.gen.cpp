// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoundCueTemplates/Public/SoundCueTemplate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundCueTemplate() {}
// Cross Module References
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueTemplate_NoRegister();
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueTemplate();
	ENGINE_API UClass* Z_Construct_UClass_USoundCue();
	UPackage* Z_Construct_UPackage__Script_SoundCueTemplates();
// End Cross Module References
	void USoundCueTemplate::StaticRegisterNativesUSoundCueTemplate()
	{
	}
	UClass* Z_Construct_UClass_USoundCueTemplate_NoRegister()
	{
		return USoundCueTemplate::StaticClass();
	}
	struct Z_Construct_UClass_USoundCueTemplate_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundCueTemplate_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundCue,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundCueTemplates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueTemplate_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Base Sound Cue Template class, which builds the sound node graph procedurally and hides more complex Sound Cue functionality\n// to streamline implementation defined in child classes.\n" },
		{ "HideCategories", "object Object" },
		{ "IncludePath", "SoundCueTemplate.h" },
		{ "ModuleRelativePath", "Public/SoundCueTemplate.h" },
		{ "ToolTip", "Base Sound Cue Template class, which builds the sound node graph procedurally and hides more complex Sound Cue functionality\nto streamline implementation defined in child classes." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundCueTemplate_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundCueTemplate>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundCueTemplate_Statics::ClassParams = {
		&USoundCueTemplate::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009010A1u,
		METADATA_PARAMS(Z_Construct_UClass_USoundCueTemplate_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplate_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundCueTemplate()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundCueTemplate_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundCueTemplate, 728992297);
	template<> SOUNDCUETEMPLATES_API UClass* StaticClass<USoundCueTemplate>()
	{
		return USoundCueTemplate::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundCueTemplate(Z_Construct_UClass_USoundCueTemplate, &USoundCueTemplate::StaticClass, TEXT("/Script/SoundCueTemplates"), TEXT("USoundCueTemplate"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundCueTemplate);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
