// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoundCueTemplates/Public/SoundCueContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundCueContainer() {}
// Cross Module References
	SOUNDCUETEMPLATES_API UEnum* Z_Construct_UEnum_SoundCueTemplates_ESoundContainerType();
	UPackage* Z_Construct_UPackage__Script_SoundCueTemplates();
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueContainer_NoRegister();
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueContainer();
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueTemplate();
	ENGINE_API UClass* Z_Construct_UClass_USoundWave_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	static UEnum* ESoundContainerType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SoundCueTemplates_ESoundContainerType, Z_Construct_UPackage__Script_SoundCueTemplates(), TEXT("ESoundContainerType"));
		}
		return Singleton;
	}
	template<> SOUNDCUETEMPLATES_API UEnum* StaticEnum<ESoundContainerType>()
	{
		return ESoundContainerType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESoundContainerType(ESoundContainerType_StaticEnum, TEXT("/Script/SoundCueTemplates"), TEXT("ESoundContainerType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SoundCueTemplates_ESoundContainerType_Hash() { return 3460298704U; }
	UEnum* Z_Construct_UEnum_SoundCueTemplates_ESoundContainerType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SoundCueTemplates();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESoundContainerType"), 0, Get_Z_Construct_UEnum_SoundCueTemplates_ESoundContainerType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESoundContainerType::Concatenate", (int64)ESoundContainerType::Concatenate },
				{ "ESoundContainerType::Randomize", (int64)ESoundContainerType::Randomize },
				{ "ESoundContainerType::Mix", (int64)ESoundContainerType::Mix },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "// ========================================================================\n// USoundCueContainer\n// Sound Cue template class which implements USoundCueTemplate.\n//\n// Simple example showing how to expose or hide template\n// parameters in the editor such as the looping and soundwave\n// fields of a USoundNodeWavePlayer.\n//\n// In order for proper data hiding to occur for inherited properties,\n// Customization Detail's 'Register' must be called in during initialization\n// (eg. in module's StartupModule()) like so:\n// #include \"SoundCueContainer.h\"\n// ...\n// FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>(\"PropertyEditor\");\n// FSoundCueContainerDetailCustomization::Register(PropertyModule);\n// ========================================================================\n" },
				{ "Concatenate.Name", "ESoundContainerType::Concatenate" },
				{ "Mix.Name", "ESoundContainerType::Mix" },
				{ "ModuleRelativePath", "Public/SoundCueContainer.h" },
				{ "Randomize.Name", "ESoundContainerType::Randomize" },
				{ "ToolTip", "USoundCueContainer\nSound Cue template class which implements USoundCueTemplate.\n\nSimple example showing how to expose or hide template\nparameters in the editor such as the looping and soundwave\nfields of a USoundNodeWavePlayer.\n\nIn order for proper data hiding to occur for inherited properties,\nCustomization Detail's 'Register' must be called in during initialization\n(eg. in module's StartupModule()) like so:\n#include \"SoundCueContainer.h\"\n...\nFPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>(\"PropertyEditor\");\nFSoundCueContainerDetailCustomization::Register(PropertyModule);" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SoundCueTemplates,
				nullptr,
				"ESoundContainerType",
				"ESoundContainerType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void USoundCueContainer::StaticRegisterNativesUSoundCueContainer()
	{
	}
	UClass* Z_Construct_UClass_USoundCueContainer_NoRegister()
	{
		return USoundCueContainer::StaticClass();
	}
	struct Z_Construct_UClass_USoundCueContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ContainerType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ContainerType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ContainerType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLooping_MetaData[];
#endif
		static void NewProp_bLooping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLooping;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Variations_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Variations_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_Variations;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PitchModulation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PitchModulation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VolumeModulation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VolumeModulation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundCueContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundCueTemplate,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundCueTemplates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueContainer_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "object object Object" },
		{ "IncludePath", "SoundCueContainer.h" },
		{ "ModuleRelativePath", "Public/SoundCueContainer.h" },
	};
#endif
#if WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_USoundCueContainer_Statics::NewProp_ContainerType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueContainer_Statics::NewProp_ContainerType_MetaData[] = {
		{ "Category", "Variation" },
		{ "ModuleRelativePath", "Public/SoundCueContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USoundCueContainer_Statics::NewProp_ContainerType = { "ContainerType", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundCueContainer, ContainerType), Z_Construct_UEnum_SoundCueTemplates_ESoundContainerType, METADATA_PARAMS(Z_Construct_UClass_USoundCueContainer_Statics::NewProp_ContainerType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueContainer_Statics::NewProp_ContainerType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueContainer_Statics::NewProp_bLooping_MetaData[] = {
		{ "Category", "Variation" },
		{ "ModuleRelativePath", "Public/SoundCueContainer.h" },
	};
#endif
	void Z_Construct_UClass_USoundCueContainer_Statics::NewProp_bLooping_SetBit(void* Obj)
	{
		((USoundCueContainer*)Obj)->bLooping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USoundCueContainer_Statics::NewProp_bLooping = { "bLooping", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USoundCueContainer), &Z_Construct_UClass_USoundCueContainer_Statics::NewProp_bLooping_SetBit, METADATA_PARAMS(Z_Construct_UClass_USoundCueContainer_Statics::NewProp_bLooping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueContainer_Statics::NewProp_bLooping_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USoundCueContainer_Statics::NewProp_Variations_ElementProp = { "Variations", nullptr, (EPropertyFlags)0x0000000800000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_USoundWave_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueContainer_Statics::NewProp_Variations_MetaData[] = {
		{ "Category", "Variation" },
		{ "ModuleRelativePath", "Public/SoundCueContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_USoundCueContainer_Statics::NewProp_Variations = { "Variations", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundCueContainer, Variations), METADATA_PARAMS(Z_Construct_UClass_USoundCueContainer_Statics::NewProp_Variations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueContainer_Statics::NewProp_Variations_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueContainer_Statics::NewProp_PitchModulation_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ModuleRelativePath", "Public/SoundCueContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USoundCueContainer_Statics::NewProp_PitchModulation = { "PitchModulation", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundCueContainer, PitchModulation), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_USoundCueContainer_Statics::NewProp_PitchModulation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueContainer_Statics::NewProp_PitchModulation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueContainer_Statics::NewProp_VolumeModulation_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ModuleRelativePath", "Public/SoundCueContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USoundCueContainer_Statics::NewProp_VolumeModulation = { "VolumeModulation", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundCueContainer, VolumeModulation), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_USoundCueContainer_Statics::NewProp_VolumeModulation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueContainer_Statics::NewProp_VolumeModulation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundCueContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueContainer_Statics::NewProp_ContainerType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueContainer_Statics::NewProp_ContainerType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueContainer_Statics::NewProp_bLooping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueContainer_Statics::NewProp_Variations_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueContainer_Statics::NewProp_Variations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueContainer_Statics::NewProp_PitchModulation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueContainer_Statics::NewProp_VolumeModulation,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundCueContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundCueContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundCueContainer_Statics::ClassParams = {
		&USoundCueContainer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_USoundCueContainer_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueContainer_Statics::PropPointers), 0),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundCueContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundCueContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundCueContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundCueContainer, 187194777);
	template<> SOUNDCUETEMPLATES_API UClass* StaticClass<USoundCueContainer>()
	{
		return USoundCueContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundCueContainer(Z_Construct_UClass_USoundCueContainer, &USoundCueContainer::StaticClass, TEXT("/Script/SoundCueTemplates"), TEXT("USoundCueContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundCueContainer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
