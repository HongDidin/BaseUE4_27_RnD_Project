// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOUNDCUETEMPLATES_SoundCueContainer_generated_h
#error "SoundCueContainer.generated.h already included, missing '#pragma once' in SoundCueContainer.h"
#endif
#define SOUNDCUETEMPLATES_SoundCueContainer_generated_h

#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_SPARSE_DATA
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_RPC_WRAPPERS
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundCueContainer(); \
	friend struct Z_Construct_UClass_USoundCueContainer_Statics; \
public: \
	DECLARE_CLASS(USoundCueContainer, USoundCueTemplate, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundCueTemplates"), NO_API) \
	DECLARE_SERIALIZER(USoundCueContainer)


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_INCLASS \
private: \
	static void StaticRegisterNativesUSoundCueContainer(); \
	friend struct Z_Construct_UClass_USoundCueContainer_Statics; \
public: \
	DECLARE_CLASS(USoundCueContainer, USoundCueTemplate, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundCueTemplates"), NO_API) \
	DECLARE_SERIALIZER(USoundCueContainer)


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundCueContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundCueContainer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundCueContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundCueContainer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundCueContainer(USoundCueContainer&&); \
	NO_API USoundCueContainer(const USoundCueContainer&); \
public:


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundCueContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundCueContainer(USoundCueContainer&&); \
	NO_API USoundCueContainer(const USoundCueContainer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundCueContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundCueContainer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundCueContainer)


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_46_PROLOG
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_INCLASS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h_49_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundCueContainer."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOUNDCUETEMPLATES_API UClass* StaticClass<class USoundCueContainer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueContainer_h


#define FOREACH_ENUM_ESOUNDCONTAINERTYPE(op) \
	op(ESoundContainerType::Concatenate) \
	op(ESoundContainerType::Randomize) \
	op(ESoundContainerType::Mix) 

enum class ESoundContainerType : uint8;
template<> SOUNDCUETEMPLATES_API UEnum* StaticEnum<ESoundContainerType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
