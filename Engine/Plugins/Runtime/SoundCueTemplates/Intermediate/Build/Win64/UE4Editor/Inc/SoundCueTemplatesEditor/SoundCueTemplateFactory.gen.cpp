// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoundCueTemplatesEditor/Public/SoundCueTemplateFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundCueTemplateFactory() {}
// Cross Module References
	SOUNDCUETEMPLATESEDITOR_API UClass* Z_Construct_UClass_USoundCueTemplateCopyFactory_NoRegister();
	SOUNDCUETEMPLATESEDITOR_API UClass* Z_Construct_UClass_USoundCueTemplateCopyFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_SoundCueTemplatesEditor();
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueTemplate_NoRegister();
	SOUNDCUETEMPLATESEDITOR_API UClass* Z_Construct_UClass_USoundCueTemplateFactory_NoRegister();
	SOUNDCUETEMPLATESEDITOR_API UClass* Z_Construct_UClass_USoundCueTemplateFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SOUNDCUETEMPLATESEDITOR_API UClass* Z_Construct_UClass_USoundCueTemplateClassTemplate_NoRegister();
	SOUNDCUETEMPLATESEDITOR_API UClass* Z_Construct_UClass_USoundCueTemplateClassTemplate();
	GAMEPROJECTGENERATION_API UClass* Z_Construct_UClass_UPluginClassTemplate();
// End Cross Module References
	void USoundCueTemplateCopyFactory::StaticRegisterNativesUSoundCueTemplateCopyFactory()
	{
	}
	UClass* Z_Construct_UClass_USoundCueTemplateCopyFactory_NoRegister()
	{
		return USoundCueTemplateCopyFactory::StaticClass();
	}
	struct Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoundCueTemplate_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_SoundCueTemplate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundCueTemplatesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "SoundCueTemplateFactory.h" },
		{ "ModuleRelativePath", "Public/SoundCueTemplateFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::NewProp_SoundCueTemplate_MetaData[] = {
		{ "ModuleRelativePath", "Public/SoundCueTemplateFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::NewProp_SoundCueTemplate = { "SoundCueTemplate", nullptr, (EPropertyFlags)0x0014000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundCueTemplateCopyFactory, SoundCueTemplate), Z_Construct_UClass_USoundCueTemplate_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::NewProp_SoundCueTemplate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::NewProp_SoundCueTemplate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::NewProp_SoundCueTemplate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundCueTemplateCopyFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::ClassParams = {
		&USoundCueTemplateCopyFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundCueTemplateCopyFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundCueTemplateCopyFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundCueTemplateCopyFactory, 2223653556);
	template<> SOUNDCUETEMPLATESEDITOR_API UClass* StaticClass<USoundCueTemplateCopyFactory>()
	{
		return USoundCueTemplateCopyFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundCueTemplateCopyFactory(Z_Construct_UClass_USoundCueTemplateCopyFactory, &USoundCueTemplateCopyFactory::StaticClass, TEXT("/Script/SoundCueTemplatesEditor"), TEXT("USoundCueTemplateCopyFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundCueTemplateCopyFactory);
	void USoundCueTemplateFactory::StaticRegisterNativesUSoundCueTemplateFactory()
	{
	}
	UClass* Z_Construct_UClass_USoundCueTemplateFactory_NoRegister()
	{
		return USoundCueTemplateFactory::StaticClass();
	}
	struct Z_Construct_UClass_USoundCueTemplateFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoundCueTemplateClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SoundCueTemplateClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundCueTemplateFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundCueTemplatesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueTemplateFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "SoundCueTemplateFactory.h" },
		{ "ModuleRelativePath", "Public/SoundCueTemplateFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueTemplateFactory_Statics::NewProp_SoundCueTemplateClass_MetaData[] = {
		{ "Category", "SoundCueTemplateFacotry" },
		{ "ModuleRelativePath", "Public/SoundCueTemplateFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_USoundCueTemplateFactory_Statics::NewProp_SoundCueTemplateClass = { "SoundCueTemplateClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundCueTemplateFactory, SoundCueTemplateClass), Z_Construct_UClass_USoundCueTemplate_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_USoundCueTemplateFactory_Statics::NewProp_SoundCueTemplateClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplateFactory_Statics::NewProp_SoundCueTemplateClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundCueTemplateFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueTemplateFactory_Statics::NewProp_SoundCueTemplateClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundCueTemplateFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundCueTemplateFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundCueTemplateFactory_Statics::ClassParams = {
		&USoundCueTemplateFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundCueTemplateFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplateFactory_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundCueTemplateFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplateFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundCueTemplateFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundCueTemplateFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundCueTemplateFactory, 489576023);
	template<> SOUNDCUETEMPLATESEDITOR_API UClass* StaticClass<USoundCueTemplateFactory>()
	{
		return USoundCueTemplateFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundCueTemplateFactory(Z_Construct_UClass_USoundCueTemplateFactory, &USoundCueTemplateFactory::StaticClass, TEXT("/Script/SoundCueTemplatesEditor"), TEXT("USoundCueTemplateFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundCueTemplateFactory);
	void USoundCueTemplateClassTemplate::StaticRegisterNativesUSoundCueTemplateClassTemplate()
	{
	}
	UClass* Z_Construct_UClass_USoundCueTemplateClassTemplate_NoRegister()
	{
		return USoundCueTemplateClassTemplate::StaticClass();
	}
	struct Z_Construct_UClass_USoundCueTemplateClassTemplate_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundCueTemplateClassTemplate_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPluginClassTemplate,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundCueTemplatesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueTemplateClassTemplate_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SoundCueTemplateFactory.h" },
		{ "ModuleRelativePath", "Public/SoundCueTemplateFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundCueTemplateClassTemplate_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundCueTemplateClassTemplate>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundCueTemplateClassTemplate_Statics::ClassParams = {
		&USoundCueTemplateClassTemplate::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundCueTemplateClassTemplate_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplateClassTemplate_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundCueTemplateClassTemplate()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundCueTemplateClassTemplate_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundCueTemplateClassTemplate, 2323035285);
	template<> SOUNDCUETEMPLATESEDITOR_API UClass* StaticClass<USoundCueTemplateClassTemplate>()
	{
		return USoundCueTemplateClassTemplate::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundCueTemplateClassTemplate(Z_Construct_UClass_USoundCueTemplateClassTemplate, &USoundCueTemplateClassTemplate::StaticClass, TEXT("/Script/SoundCueTemplatesEditor"), TEXT("USoundCueTemplateClassTemplate"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundCueTemplateClassTemplate);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
