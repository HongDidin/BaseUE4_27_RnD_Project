// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOUNDCUETEMPLATES_SoundCueTemplateSettings_generated_h
#error "SoundCueTemplateSettings.generated.h already included, missing '#pragma once' in SoundCueTemplateSettings.h"
#endif
#define SOUNDCUETEMPLATES_SoundCueTemplateSettings_generated_h

#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics; \
	SOUNDCUETEMPLATES_API static class UScriptStruct* StaticStruct();


template<> SOUNDCUETEMPLATES_API UScriptStruct* StaticStruct<struct FSoundCueTemplateQualitySettings>();

#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_SPARSE_DATA
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_RPC_WRAPPERS
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundCueTemplateSettings(); \
	friend struct Z_Construct_UClass_USoundCueTemplateSettings_Statics; \
public: \
	DECLARE_CLASS(USoundCueTemplateSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoundCueTemplates"), NO_API) \
	DECLARE_SERIALIZER(USoundCueTemplateSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_INCLASS \
private: \
	static void StaticRegisterNativesUSoundCueTemplateSettings(); \
	friend struct Z_Construct_UClass_USoundCueTemplateSettings_Statics; \
public: \
	DECLARE_CLASS(USoundCueTemplateSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoundCueTemplates"), NO_API) \
	DECLARE_SERIALIZER(USoundCueTemplateSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundCueTemplateSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundCueTemplateSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundCueTemplateSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundCueTemplateSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundCueTemplateSettings(USoundCueTemplateSettings&&); \
	NO_API USoundCueTemplateSettings(const USoundCueTemplateSettings&); \
public:


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundCueTemplateSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundCueTemplateSettings(USoundCueTemplateSettings&&); \
	NO_API USoundCueTemplateSettings(const USoundCueTemplateSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundCueTemplateSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundCueTemplateSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundCueTemplateSettings)


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_41_PROLOG
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_INCLASS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h_44_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundCueTemplateSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOUNDCUETEMPLATES_API UClass* StaticClass<class USoundCueTemplateSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueTemplateSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
