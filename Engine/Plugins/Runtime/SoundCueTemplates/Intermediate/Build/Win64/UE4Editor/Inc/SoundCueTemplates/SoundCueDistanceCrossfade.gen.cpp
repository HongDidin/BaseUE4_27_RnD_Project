// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoundCueTemplates/Public/SoundCueDistanceCrossfade.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundCueDistanceCrossfade() {}
// Cross Module References
	SOUNDCUETEMPLATES_API UScriptStruct* Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo();
	UPackage* Z_Construct_UPackage__Script_SoundCueTemplates();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDistanceDatum();
	ENGINE_API UClass* Z_Construct_UClass_USoundWave_NoRegister();
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueDistanceCrossfade_NoRegister();
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueDistanceCrossfade();
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueTemplate();
// End Cross Module References
class UScriptStruct* FSoundCueCrossfadeInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SOUNDCUETEMPLATES_API uint32 Get_Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo, Z_Construct_UPackage__Script_SoundCueTemplates(), TEXT("SoundCueCrossfadeInfo"), sizeof(FSoundCueCrossfadeInfo), Get_Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Hash());
	}
	return Singleton;
}
template<> SOUNDCUETEMPLATES_API UScriptStruct* StaticStruct<FSoundCueCrossfadeInfo>()
{
	return FSoundCueCrossfadeInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSoundCueCrossfadeInfo(FSoundCueCrossfadeInfo::StaticStruct, TEXT("/Script/SoundCueTemplates"), TEXT("SoundCueCrossfadeInfo"), false, nullptr, nullptr);
static struct FScriptStruct_SoundCueTemplates_StaticRegisterNativesFSoundCueCrossfadeInfo
{
	FScriptStruct_SoundCueTemplates_StaticRegisterNativesFSoundCueCrossfadeInfo()
	{
		UScriptStruct::DeferCppStructOps<FSoundCueCrossfadeInfo>(FName(TEXT("SoundCueCrossfadeInfo")));
	}
} ScriptStruct_SoundCueTemplates_StaticRegisterNativesFSoundCueCrossfadeInfo;
	struct Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistanceInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Sound;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// ========================================================================\n// USoundCueDistanceCrossfade\n// Sound Cue template class which implements USoundCueTemplate.\n//\n// Simple near/distant mix design for provided, spatialized sounds.\n// ========================================================================\n" },
		{ "ModuleRelativePath", "Public/SoundCueDistanceCrossfade.h" },
		{ "ToolTip", "USoundCueDistanceCrossfade\nSound Cue template class which implements USoundCueTemplate.\n\nSimple near/distant mix design for provided, spatialized sounds." },
	};
#endif
	void* Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSoundCueCrossfadeInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewProp_DistanceInfo_MetaData[] = {
		{ "Category", "Crossfade Info" },
		{ "ModuleRelativePath", "Public/SoundCueDistanceCrossfade.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewProp_DistanceInfo = { "DistanceInfo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundCueCrossfadeInfo, DistanceInfo), Z_Construct_UScriptStruct_FDistanceDatum, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewProp_DistanceInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewProp_DistanceInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewProp_Sound_MetaData[] = {
		{ "Category", "Crossfade Info" },
		{ "ModuleRelativePath", "Public/SoundCueDistanceCrossfade.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewProp_Sound = { "Sound", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundCueCrossfadeInfo, Sound), Z_Construct_UClass_USoundWave_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewProp_Sound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewProp_Sound_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewProp_DistanceInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::NewProp_Sound,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SoundCueTemplates,
		nullptr,
		&NewStructOps,
		"SoundCueCrossfadeInfo",
		sizeof(FSoundCueCrossfadeInfo),
		alignof(FSoundCueCrossfadeInfo),
		Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SoundCueTemplates();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SoundCueCrossfadeInfo"), sizeof(FSoundCueCrossfadeInfo), Get_Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Hash() { return 401606566U; }
	void USoundCueDistanceCrossfade::StaticRegisterNativesUSoundCueDistanceCrossfade()
	{
	}
	UClass* Z_Construct_UClass_USoundCueDistanceCrossfade_NoRegister()
	{
		return USoundCueDistanceCrossfade::StaticClass();
	}
	struct Z_Construct_UClass_USoundCueDistanceCrossfade_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLooping_MetaData[];
#endif
		static void NewProp_bLooping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLooping;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Variations_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Variations_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Variations;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundCueTemplate,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundCueTemplates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "object object Object" },
		{ "IncludePath", "SoundCueDistanceCrossfade.h" },
		{ "ModuleRelativePath", "Public/SoundCueDistanceCrossfade.h" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_bLooping_MetaData[] = {
		{ "Category", "Wave Parameters" },
		{ "ModuleRelativePath", "Public/SoundCueDistanceCrossfade.h" },
	};
#endif
	void Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_bLooping_SetBit(void* Obj)
	{
		((USoundCueDistanceCrossfade*)Obj)->bLooping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_bLooping = { "bLooping", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USoundCueDistanceCrossfade), &Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_bLooping_SetBit, METADATA_PARAMS(Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_bLooping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_bLooping_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_Variations_Inner = { "Variations", nullptr, (EPropertyFlags)0x0000000800000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_Variations_MetaData[] = {
		{ "Category", "Wave Parameters" },
		{ "ModuleRelativePath", "Public/SoundCueDistanceCrossfade.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_Variations = { "Variations", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundCueDistanceCrossfade, Variations), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_Variations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_Variations_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_bLooping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_Variations_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::NewProp_Variations,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundCueDistanceCrossfade>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::ClassParams = {
		&USoundCueDistanceCrossfade::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::PropPointers), 0),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundCueDistanceCrossfade()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundCueDistanceCrossfade_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundCueDistanceCrossfade, 1116351861);
	template<> SOUNDCUETEMPLATES_API UClass* StaticClass<USoundCueDistanceCrossfade>()
	{
		return USoundCueDistanceCrossfade::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundCueDistanceCrossfade(Z_Construct_UClass_USoundCueDistanceCrossfade, &USoundCueDistanceCrossfade::StaticClass, TEXT("/Script/SoundCueTemplates"), TEXT("USoundCueDistanceCrossfade"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundCueDistanceCrossfade);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
