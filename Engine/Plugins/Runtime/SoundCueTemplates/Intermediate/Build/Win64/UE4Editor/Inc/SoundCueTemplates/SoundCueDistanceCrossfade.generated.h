// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOUNDCUETEMPLATES_SoundCueDistanceCrossfade_generated_h
#error "SoundCueDistanceCrossfade.generated.h already included, missing '#pragma once' in SoundCueDistanceCrossfade.h"
#endif
#define SOUNDCUETEMPLATES_SoundCueDistanceCrossfade_generated_h

#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_30_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSoundCueCrossfadeInfo_Statics; \
	SOUNDCUETEMPLATES_API static class UScriptStruct* StaticStruct();


template<> SOUNDCUETEMPLATES_API UScriptStruct* StaticStruct<struct FSoundCueCrossfadeInfo>();

#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_SPARSE_DATA
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_RPC_WRAPPERS
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundCueDistanceCrossfade(); \
	friend struct Z_Construct_UClass_USoundCueDistanceCrossfade_Statics; \
public: \
	DECLARE_CLASS(USoundCueDistanceCrossfade, USoundCueTemplate, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundCueTemplates"), NO_API) \
	DECLARE_SERIALIZER(USoundCueDistanceCrossfade)


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_INCLASS \
private: \
	static void StaticRegisterNativesUSoundCueDistanceCrossfade(); \
	friend struct Z_Construct_UClass_USoundCueDistanceCrossfade_Statics; \
public: \
	DECLARE_CLASS(USoundCueDistanceCrossfade, USoundCueTemplate, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundCueTemplates"), NO_API) \
	DECLARE_SERIALIZER(USoundCueDistanceCrossfade)


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundCueDistanceCrossfade(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundCueDistanceCrossfade) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundCueDistanceCrossfade); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundCueDistanceCrossfade); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundCueDistanceCrossfade(USoundCueDistanceCrossfade&&); \
	NO_API USoundCueDistanceCrossfade(const USoundCueDistanceCrossfade&); \
public:


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundCueDistanceCrossfade(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundCueDistanceCrossfade(USoundCueDistanceCrossfade&&); \
	NO_API USoundCueDistanceCrossfade(const USoundCueDistanceCrossfade&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundCueDistanceCrossfade); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundCueDistanceCrossfade); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundCueDistanceCrossfade)


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_39_PROLOG
#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_INCLASS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h_42_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundCueDistanceCrossfade."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOUNDCUETEMPLATES_API UClass* StaticClass<class USoundCueDistanceCrossfade>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SoundCueTemplates_Source_SoundCueTemplates_Public_SoundCueDistanceCrossfade_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
