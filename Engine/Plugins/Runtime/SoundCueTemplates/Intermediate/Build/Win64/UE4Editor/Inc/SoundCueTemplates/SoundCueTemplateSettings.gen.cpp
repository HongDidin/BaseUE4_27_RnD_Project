// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoundCueTemplates/Public/SoundCueTemplateSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundCueTemplateSettings() {}
// Cross Module References
	SOUNDCUETEMPLATES_API UScriptStruct* Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings();
	UPackage* Z_Construct_UPackage__Script_SoundCueTemplates();
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueTemplateSettings_NoRegister();
	SOUNDCUETEMPLATES_API UClass* Z_Construct_UClass_USoundCueTemplateSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
// End Cross Module References
class UScriptStruct* FSoundCueTemplateQualitySettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SOUNDCUETEMPLATES_API uint32 Get_Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings, Z_Construct_UPackage__Script_SoundCueTemplates(), TEXT("SoundCueTemplateQualitySettings"), sizeof(FSoundCueTemplateQualitySettings), Get_Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Hash());
	}
	return Singleton;
}
template<> SOUNDCUETEMPLATES_API UScriptStruct* StaticStruct<FSoundCueTemplateQualitySettings>()
{
	return FSoundCueTemplateQualitySettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSoundCueTemplateQualitySettings(FSoundCueTemplateQualitySettings::StaticStruct, TEXT("/Script/SoundCueTemplates"), TEXT("SoundCueTemplateQualitySettings"), false, nullptr, nullptr);
static struct FScriptStruct_SoundCueTemplates_StaticRegisterNativesFSoundCueTemplateQualitySettings
{
	FScriptStruct_SoundCueTemplates_StaticRegisterNativesFSoundCueTemplateQualitySettings()
	{
		UScriptStruct::DeferCppStructOps<FSoundCueTemplateQualitySettings>(FName(TEXT("SoundCueTemplateQualitySettings")));
	}
} ScriptStruct_SoundCueTemplates_StaticRegisterNativesFSoundCueTemplateQualitySettings;
	struct Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxConcatenatedVariations_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxConcatenatedVariations;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxRandomizedVariations_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxRandomizedVariations;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxMixVariations_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxMixVariations;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SoundCueTemplateSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSoundCueTemplateQualitySettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "Quality" },
		{ "ModuleRelativePath", "Public/SoundCueTemplateSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundCueTemplateQualitySettings, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxConcatenatedVariations_MetaData[] = {
		{ "Category", "Quality" },
		{ "ClampMin", "1" },
		{ "Comment", "// The max number of variations to include for the given quality in a SoundCueContainer set to 'Concatenate'.\n" },
		{ "ModuleRelativePath", "Public/SoundCueTemplateSettings.h" },
		{ "ToolTip", "The max number of variations to include for the given quality in a SoundCueContainer set to 'Concatenate'." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxConcatenatedVariations = { "MaxConcatenatedVariations", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundCueTemplateQualitySettings, MaxConcatenatedVariations), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxConcatenatedVariations_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxConcatenatedVariations_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxRandomizedVariations_MetaData[] = {
		{ "Category", "Quality" },
		{ "ClampMin", "1" },
		{ "Comment", "// The max number of variations to include for the given quality in a SoundCueContainer set to 'Randomized'.\n" },
		{ "ModuleRelativePath", "Public/SoundCueTemplateSettings.h" },
		{ "ToolTip", "The max number of variations to include for the given quality in a SoundCueContainer set to 'Randomized'." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxRandomizedVariations = { "MaxRandomizedVariations", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundCueTemplateQualitySettings, MaxRandomizedVariations), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxRandomizedVariations_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxRandomizedVariations_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxMixVariations_MetaData[] = {
		{ "Category", "Quality" },
		{ "ClampMin", "1" },
		{ "Comment", "// The max number of variations to include for the given quality in a SoundCueContainer set to 'Mix'.\n" },
		{ "ModuleRelativePath", "Public/SoundCueTemplateSettings.h" },
		{ "ToolTip", "The max number of variations to include for the given quality in a SoundCueContainer set to 'Mix'." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxMixVariations = { "MaxMixVariations", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundCueTemplateQualitySettings, MaxMixVariations), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxMixVariations_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxMixVariations_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxConcatenatedVariations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxRandomizedVariations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::NewProp_MaxMixVariations,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SoundCueTemplates,
		nullptr,
		&NewStructOps,
		"SoundCueTemplateQualitySettings",
		sizeof(FSoundCueTemplateQualitySettings),
		alignof(FSoundCueTemplateQualitySettings),
		Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SoundCueTemplates();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SoundCueTemplateQualitySettings"), sizeof(FSoundCueTemplateQualitySettings), Get_Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings_Hash() { return 1933174076U; }
	void USoundCueTemplateSettings::StaticRegisterNativesUSoundCueTemplateSettings()
	{
	}
	UClass* Z_Construct_UClass_USoundCueTemplateSettings_NoRegister()
	{
		return USoundCueTemplateSettings::StaticClass();
	}
	struct Z_Construct_UClass_USoundCueTemplateSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_QualityLevels_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_QualityLevels_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_QualityLevels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundCueTemplateSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundCueTemplates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueTemplateSettings_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Sound Cue Templates" },
		{ "IncludePath", "SoundCueTemplateSettings.h" },
		{ "ModuleRelativePath", "Public/SoundCueTemplateSettings.h" },
	};
#endif
#if WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USoundCueTemplateSettings_Statics::NewProp_QualityLevels_Inner = { "QualityLevels", nullptr, (EPropertyFlags)0x0000000800004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoundCueTemplateQualitySettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundCueTemplateSettings_Statics::NewProp_QualityLevels_MetaData[] = {
		{ "Category", "Quality" },
		{ "ModuleRelativePath", "Public/SoundCueTemplateSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USoundCueTemplateSettings_Statics::NewProp_QualityLevels = { "QualityLevels", nullptr, (EPropertyFlags)0x0010000800004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundCueTemplateSettings, QualityLevels), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USoundCueTemplateSettings_Statics::NewProp_QualityLevels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplateSettings_Statics::NewProp_QualityLevels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundCueTemplateSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueTemplateSettings_Statics::NewProp_QualityLevels_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundCueTemplateSettings_Statics::NewProp_QualityLevels,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundCueTemplateSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundCueTemplateSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundCueTemplateSettings_Statics::ClassParams = {
		&USoundCueTemplateSettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_USoundCueTemplateSettings_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplateSettings_Statics::PropPointers), 0),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_USoundCueTemplateSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundCueTemplateSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundCueTemplateSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundCueTemplateSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundCueTemplateSettings, 3694341610);
	template<> SOUNDCUETEMPLATES_API UClass* StaticClass<USoundCueTemplateSettings>()
	{
		return USoundCueTemplateSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundCueTemplateSettings(Z_Construct_UClass_USoundCueTemplateSettings, &USoundCueTemplateSettings::StaticClass, TEXT("/Script/SoundCueTemplates"), TEXT("USoundCueTemplateSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundCueTemplateSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
