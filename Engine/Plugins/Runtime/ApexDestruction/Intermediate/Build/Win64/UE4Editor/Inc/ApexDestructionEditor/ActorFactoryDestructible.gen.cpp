// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ApexDestructionEditor/Private/ActorFactoryDestructible.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorFactoryDestructible() {}
// Cross Module References
	APEXDESTRUCTIONEDITOR_API UClass* Z_Construct_UClass_UActorFactoryDestructible_NoRegister();
	APEXDESTRUCTIONEDITOR_API UClass* Z_Construct_UClass_UActorFactoryDestructible();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_ApexDestructionEditor();
// End Cross Module References
	void UActorFactoryDestructible::StaticRegisterNativesUActorFactoryDestructible()
	{
	}
	UClass* Z_Construct_UClass_UActorFactoryDestructible_NoRegister()
	{
		return UActorFactoryDestructible::StaticClass();
	}
	struct Z_Construct_UClass_UActorFactoryDestructible_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorFactoryDestructible_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ApexDestructionEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorFactoryDestructible_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "ActorFactoryDestructible.h" },
		{ "ModuleRelativePath", "Private/ActorFactoryDestructible.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorFactoryDestructible_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorFactoryDestructible>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorFactoryDestructible_Statics::ClassParams = {
		&UActorFactoryDestructible::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UActorFactoryDestructible_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorFactoryDestructible_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorFactoryDestructible()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorFactoryDestructible_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorFactoryDestructible, 2750803239);
	template<> APEXDESTRUCTIONEDITOR_API UClass* StaticClass<UActorFactoryDestructible>()
	{
		return UActorFactoryDestructible::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorFactoryDestructible(Z_Construct_UClass_UActorFactoryDestructible, &UActorFactoryDestructible::StaticClass, TEXT("/Script/ApexDestructionEditor"), TEXT("UActorFactoryDestructible"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorFactoryDestructible);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
