// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APEXDESTRUCTIONEDITOR_DestructibleChunkParamsProxy_generated_h
#error "DestructibleChunkParamsProxy.generated.h already included, missing '#pragma once' in DestructibleChunkParamsProxy.h"
#endif
#define APEXDESTRUCTIONEDITOR_DestructibleChunkParamsProxy_generated_h

#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDestructibleChunkParamsProxy(); \
	friend struct Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics; \
public: \
	DECLARE_CLASS(UDestructibleChunkParamsProxy, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ApexDestructionEditor"), NO_API) \
	DECLARE_SERIALIZER(UDestructibleChunkParamsProxy)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUDestructibleChunkParamsProxy(); \
	friend struct Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics; \
public: \
	DECLARE_CLASS(UDestructibleChunkParamsProxy, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ApexDestructionEditor"), NO_API) \
	DECLARE_SERIALIZER(UDestructibleChunkParamsProxy)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDestructibleChunkParamsProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDestructibleChunkParamsProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDestructibleChunkParamsProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDestructibleChunkParamsProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDestructibleChunkParamsProxy(UDestructibleChunkParamsProxy&&); \
	NO_API UDestructibleChunkParamsProxy(const UDestructibleChunkParamsProxy&); \
public:


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDestructibleChunkParamsProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDestructibleChunkParamsProxy(UDestructibleChunkParamsProxy&&); \
	NO_API UDestructibleChunkParamsProxy(const UDestructibleChunkParamsProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDestructibleChunkParamsProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDestructibleChunkParamsProxy); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDestructibleChunkParamsProxy)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_14_PROLOG
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_INCLASS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class DestructibleChunkParamsProxy."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APEXDESTRUCTIONEDITOR_API UClass* StaticClass<class UDestructibleChunkParamsProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleChunkParamsProxy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
