// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ApexDestructionEditor/Private/DestructibleChunkParamsProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDestructibleChunkParamsProxy() {}
// Cross Module References
	APEXDESTRUCTIONEDITOR_API UClass* Z_Construct_UClass_UDestructibleChunkParamsProxy_NoRegister();
	APEXDESTRUCTIONEDITOR_API UClass* Z_Construct_UClass_UDestructibleChunkParamsProxy();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ApexDestructionEditor();
	APEXDESTRUCTION_API UClass* Z_Construct_UClass_UDestructibleMesh_NoRegister();
	APEXDESTRUCTION_API UScriptStruct* Z_Construct_UScriptStruct_FDestructibleChunkParameters();
// End Cross Module References
	void UDestructibleChunkParamsProxy::StaticRegisterNativesUDestructibleChunkParamsProxy()
	{
	}
	UClass* Z_Construct_UClass_UDestructibleChunkParamsProxy_NoRegister()
	{
		return UDestructibleChunkParamsProxy::StaticClass();
	}
	struct Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestructibleMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DestructibleMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChunkIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ChunkIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChunkParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChunkParams;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ApexDestructionEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DestructibleChunkParamsProxy.h" },
		{ "ModuleRelativePath", "Private/DestructibleChunkParamsProxy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_DestructibleMesh_MetaData[] = {
		{ "ModuleRelativePath", "Private/DestructibleChunkParamsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_DestructibleMesh = { "DestructibleMesh", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDestructibleChunkParamsProxy, DestructibleMesh), Z_Construct_UClass_UDestructibleMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_DestructibleMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_DestructibleMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_ChunkIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/DestructibleChunkParamsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_ChunkIndex = { "ChunkIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDestructibleChunkParamsProxy, ChunkIndex), METADATA_PARAMS(Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_ChunkIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_ChunkIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_ChunkParams_MetaData[] = {
		{ "Category", "Chunks" },
		{ "ModuleRelativePath", "Private/DestructibleChunkParamsProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_ChunkParams = { "ChunkParams", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDestructibleChunkParamsProxy, ChunkParams), Z_Construct_UScriptStruct_FDestructibleChunkParameters, METADATA_PARAMS(Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_ChunkParams_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_ChunkParams_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_DestructibleMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_ChunkIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::NewProp_ChunkParams,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDestructibleChunkParamsProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::ClassParams = {
		&UDestructibleChunkParamsProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDestructibleChunkParamsProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDestructibleChunkParamsProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDestructibleChunkParamsProxy, 1334136721);
	template<> APEXDESTRUCTIONEDITOR_API UClass* StaticClass<UDestructibleChunkParamsProxy>()
	{
		return UDestructibleChunkParamsProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDestructibleChunkParamsProxy(Z_Construct_UClass_UDestructibleChunkParamsProxy, &UDestructibleChunkParamsProxy::StaticClass, TEXT("/Script/ApexDestructionEditor"), TEXT("UDestructibleChunkParamsProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDestructibleChunkParamsProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
