// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APEXDESTRUCTIONEDITOR_DestructibleMeshFactory_generated_h
#error "DestructibleMeshFactory.generated.h already included, missing '#pragma once' in DestructibleMeshFactory.h"
#endif
#define APEXDESTRUCTIONEDITOR_DestructibleMeshFactory_generated_h

#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDestructibleMeshFactory(); \
	friend struct Z_Construct_UClass_UDestructibleMeshFactory_Statics; \
public: \
	DECLARE_CLASS(UDestructibleMeshFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ApexDestructionEditor"), APEXDESTRUCTIONEDITOR_API) \
	DECLARE_SERIALIZER(UDestructibleMeshFactory)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDestructibleMeshFactory(); \
	friend struct Z_Construct_UClass_UDestructibleMeshFactory_Statics; \
public: \
	DECLARE_CLASS(UDestructibleMeshFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ApexDestructionEditor"), APEXDESTRUCTIONEDITOR_API) \
	DECLARE_SERIALIZER(UDestructibleMeshFactory)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	APEXDESTRUCTIONEDITOR_API UDestructibleMeshFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDestructibleMeshFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(APEXDESTRUCTIONEDITOR_API, UDestructibleMeshFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDestructibleMeshFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	APEXDESTRUCTIONEDITOR_API UDestructibleMeshFactory(UDestructibleMeshFactory&&); \
	APEXDESTRUCTIONEDITOR_API UDestructibleMeshFactory(const UDestructibleMeshFactory&); \
public:


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	APEXDESTRUCTIONEDITOR_API UDestructibleMeshFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	APEXDESTRUCTIONEDITOR_API UDestructibleMeshFactory(UDestructibleMeshFactory&&); \
	APEXDESTRUCTIONEDITOR_API UDestructibleMeshFactory(const UDestructibleMeshFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(APEXDESTRUCTIONEDITOR_API, UDestructibleMeshFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDestructibleMeshFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDestructibleMeshFactory)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_13_PROLOG
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_INCLASS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class DestructibleMeshFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APEXDESTRUCTIONEDITOR_API UClass* StaticClass<class UDestructibleMeshFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_DestructibleMeshFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
