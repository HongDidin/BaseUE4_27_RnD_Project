// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ApexDestruction/Public/DestructibleActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDestructibleActor() {}
// Cross Module References
	APEXDESTRUCTION_API UFunction* Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_ApexDestruction();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	APEXDESTRUCTION_API UClass* Z_Construct_UClass_ADestructibleActor_NoRegister();
	APEXDESTRUCTION_API UClass* Z_Construct_UClass_ADestructibleActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	APEXDESTRUCTION_API UClass* Z_Construct_UClass_UDestructibleComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics
	{
		struct _Script_ApexDestruction_eventActorFractureSignature_Parms
		{
			FVector HitPoint;
			FVector HitDirection;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitDirection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitDirection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::NewProp_HitPoint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::NewProp_HitPoint = { "HitPoint", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_ApexDestruction_eventActorFractureSignature_Parms, HitPoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::NewProp_HitPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::NewProp_HitPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::NewProp_HitDirection_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::NewProp_HitDirection = { "HitDirection", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_ApexDestruction_eventActorFractureSignature_Parms, HitDirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::NewProp_HitDirection_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::NewProp_HitDirection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::NewProp_HitPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::NewProp_HitDirection,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate for notification when fracture occurs */" },
		{ "ModuleRelativePath", "Public/DestructibleActor.h" },
		{ "ToolTip", "Delegate for notification when fracture occurs" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_ApexDestruction, nullptr, "ActorFractureSignature__DelegateSignature", nullptr, nullptr, sizeof(_Script_ApexDestruction_eventActorFractureSignature_Parms), Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void ADestructibleActor::StaticRegisterNativesADestructibleActor()
	{
	}
	UClass* Z_Construct_UClass_ADestructibleActor_NoRegister()
	{
		return ADestructibleActor::StaticClass();
	}
	struct Z_Construct_UClass_ADestructibleActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestructibleComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DestructibleComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnActorFracture_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnActorFracture;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAffectNavigation_MetaData[];
#endif
		static void NewProp_bAffectNavigation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAffectNavigation;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADestructibleActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ApexDestruction,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADestructibleActor_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Input" },
		{ "IgnoreCategoryKeywordsInSubclasses", "true" },
		{ "IncludePath", "DestructibleActor.h" },
		{ "ModuleRelativePath", "Public/DestructibleActor.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADestructibleActor_Statics::NewProp_DestructibleComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Destruction" },
		{ "EditInline", "true" },
		{ "ExposeFunctionCategories", "Destruction,Components|Destructible" },
		{ "ModuleRelativePath", "Public/DestructibleActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADestructibleActor_Statics::NewProp_DestructibleComponent = { "DestructibleComponent", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADestructibleActor, DestructibleComponent), Z_Construct_UClass_UDestructibleComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADestructibleActor_Statics::NewProp_DestructibleComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADestructibleActor_Statics::NewProp_DestructibleComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADestructibleActor_Statics::NewProp_OnActorFracture_MetaData[] = {
		{ "Category", "Components|Destructible" },
		{ "ModuleRelativePath", "Public/DestructibleActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ADestructibleActor_Statics::NewProp_OnActorFracture = { "OnActorFracture", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADestructibleActor, OnActorFracture), Z_Construct_UDelegateFunction_ApexDestruction_ActorFractureSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ADestructibleActor_Statics::NewProp_OnActorFracture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADestructibleActor_Statics::NewProp_OnActorFracture_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADestructibleActor_Statics::NewProp_bAffectNavigation_MetaData[] = {
		{ "Category", "Navigation" },
		{ "DeprecationMessage", "Setting the value from Blueprint script does nothing. Please use bCanEverAffectNavigation in DestructionComponent." },
		{ "ModuleRelativePath", "Public/DestructibleActor.h" },
	};
#endif
	void Z_Construct_UClass_ADestructibleActor_Statics::NewProp_bAffectNavigation_SetBit(void* Obj)
	{
		((ADestructibleActor*)Obj)->bAffectNavigation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADestructibleActor_Statics::NewProp_bAffectNavigation = { "bAffectNavigation", nullptr, (EPropertyFlags)0x0010000800004004, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(ADestructibleActor), &Z_Construct_UClass_ADestructibleActor_Statics::NewProp_bAffectNavigation_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADestructibleActor_Statics::NewProp_bAffectNavigation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADestructibleActor_Statics::NewProp_bAffectNavigation_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADestructibleActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADestructibleActor_Statics::NewProp_DestructibleComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADestructibleActor_Statics::NewProp_OnActorFracture,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADestructibleActor_Statics::NewProp_bAffectNavigation,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADestructibleActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADestructibleActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADestructibleActor_Statics::ClassParams = {
		&ADestructibleActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ADestructibleActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ADestructibleActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADestructibleActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADestructibleActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADestructibleActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADestructibleActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADestructibleActor, 1849170510);
	template<> APEXDESTRUCTION_API UClass* StaticClass<ADestructibleActor>()
	{
		return ADestructibleActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADestructibleActor(Z_Construct_UClass_ADestructibleActor, &ADestructibleActor::StaticClass, TEXT("/Script/ApexDestruction"), TEXT("ADestructibleActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADestructibleActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
