// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ApexDestructionEditor/Private/DestructibleMeshFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDestructibleMeshFactory() {}
// Cross Module References
	APEXDESTRUCTIONEDITOR_API UClass* Z_Construct_UClass_UDestructibleMeshFactory_NoRegister();
	APEXDESTRUCTIONEDITOR_API UClass* Z_Construct_UClass_UDestructibleMeshFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_ApexDestructionEditor();
// End Cross Module References
	void UDestructibleMeshFactory::StaticRegisterNativesUDestructibleMeshFactory()
	{
	}
	UClass* Z_Construct_UClass_UDestructibleMeshFactory_NoRegister()
	{
		return UDestructibleMeshFactory::StaticClass();
	}
	struct Z_Construct_UClass_UDestructibleMeshFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDestructibleMeshFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ApexDestructionEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDestructibleMeshFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "DestructibleMeshFactory.h" },
		{ "ModuleRelativePath", "Private/DestructibleMeshFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDestructibleMeshFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDestructibleMeshFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDestructibleMeshFactory_Statics::ClassParams = {
		&UDestructibleMeshFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDestructibleMeshFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDestructibleMeshFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDestructibleMeshFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDestructibleMeshFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDestructibleMeshFactory, 3181651029);
	template<> APEXDESTRUCTIONEDITOR_API UClass* StaticClass<UDestructibleMeshFactory>()
	{
		return UDestructibleMeshFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDestructibleMeshFactory(Z_Construct_UClass_UDestructibleMeshFactory, &UDestructibleMeshFactory::StaticClass, TEXT("/Script/ApexDestructionEditor"), TEXT("UDestructibleMeshFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDestructibleMeshFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
