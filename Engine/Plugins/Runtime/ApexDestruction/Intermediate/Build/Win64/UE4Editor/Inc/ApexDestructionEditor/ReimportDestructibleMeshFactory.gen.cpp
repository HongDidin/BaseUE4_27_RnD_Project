// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ApexDestructionEditor/Private/ReimportDestructibleMeshFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReimportDestructibleMeshFactory() {}
// Cross Module References
	APEXDESTRUCTIONEDITOR_API UClass* Z_Construct_UClass_UReimportDestructibleMeshFactory_NoRegister();
	APEXDESTRUCTIONEDITOR_API UClass* Z_Construct_UClass_UReimportDestructibleMeshFactory();
	APEXDESTRUCTIONEDITOR_API UClass* Z_Construct_UClass_UDestructibleMeshFactory();
	UPackage* Z_Construct_UPackage__Script_ApexDestructionEditor();
// End Cross Module References
	void UReimportDestructibleMeshFactory::StaticRegisterNativesUReimportDestructibleMeshFactory()
	{
	}
	UClass* Z_Construct_UClass_UReimportDestructibleMeshFactory_NoRegister()
	{
		return UReimportDestructibleMeshFactory::StaticClass();
	}
	struct Z_Construct_UClass_UReimportDestructibleMeshFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReimportDestructibleMeshFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDestructibleMeshFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ApexDestructionEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReimportDestructibleMeshFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "ReimportDestructibleMeshFactory.h" },
		{ "ModuleRelativePath", "Private/ReimportDestructibleMeshFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReimportDestructibleMeshFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReimportDestructibleMeshFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReimportDestructibleMeshFactory_Statics::ClassParams = {
		&UReimportDestructibleMeshFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UReimportDestructibleMeshFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReimportDestructibleMeshFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReimportDestructibleMeshFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReimportDestructibleMeshFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReimportDestructibleMeshFactory, 212535399);
	template<> APEXDESTRUCTIONEDITOR_API UClass* StaticClass<UReimportDestructibleMeshFactory>()
	{
		return UReimportDestructibleMeshFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReimportDestructibleMeshFactory(Z_Construct_UClass_UReimportDestructibleMeshFactory, &UReimportDestructibleMeshFactory::StaticClass, TEXT("/Script/ApexDestructionEditor"), TEXT("UReimportDestructibleMeshFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReimportDestructibleMeshFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
