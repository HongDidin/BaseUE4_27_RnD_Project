// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APEXDESTRUCTIONEDITOR_ActorFactoryDestructible_generated_h
#error "ActorFactoryDestructible.generated.h already included, missing '#pragma once' in ActorFactoryDestructible.h"
#endif
#define APEXDESTRUCTIONEDITOR_ActorFactoryDestructible_generated_h

#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorFactoryDestructible(); \
	friend struct Z_Construct_UClass_UActorFactoryDestructible_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryDestructible, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ApexDestructionEditor"), APEXDESTRUCTIONEDITOR_API) \
	DECLARE_SERIALIZER(UActorFactoryDestructible)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUActorFactoryDestructible(); \
	friend struct Z_Construct_UClass_UActorFactoryDestructible_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryDestructible, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ApexDestructionEditor"), APEXDESTRUCTIONEDITOR_API) \
	DECLARE_SERIALIZER(UActorFactoryDestructible)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	APEXDESTRUCTIONEDITOR_API UActorFactoryDestructible(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryDestructible) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(APEXDESTRUCTIONEDITOR_API, UActorFactoryDestructible); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryDestructible); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	APEXDESTRUCTIONEDITOR_API UActorFactoryDestructible(UActorFactoryDestructible&&); \
	APEXDESTRUCTIONEDITOR_API UActorFactoryDestructible(const UActorFactoryDestructible&); \
public:


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	APEXDESTRUCTIONEDITOR_API UActorFactoryDestructible(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	APEXDESTRUCTIONEDITOR_API UActorFactoryDestructible(UActorFactoryDestructible&&); \
	APEXDESTRUCTIONEDITOR_API UActorFactoryDestructible(const UActorFactoryDestructible&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(APEXDESTRUCTIONEDITOR_API, UActorFactoryDestructible); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryDestructible); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryDestructible)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_14_PROLOG
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_INCLASS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ActorFactoryDestructible."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APEXDESTRUCTIONEDITOR_API UClass* StaticClass<class UActorFactoryDestructible>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestructionEditor_Private_ActorFactoryDestructible_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
