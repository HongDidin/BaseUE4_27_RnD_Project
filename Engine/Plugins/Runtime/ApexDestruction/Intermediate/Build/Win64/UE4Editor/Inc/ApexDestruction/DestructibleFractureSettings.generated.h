// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APEXDESTRUCTION_DestructibleFractureSettings_generated_h
#error "DestructibleFractureSettings.generated.h already included, missing '#pragma once' in DestructibleFractureSettings.h"
#endif
#define APEXDESTRUCTION_DestructibleFractureSettings_generated_h

#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_111_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDestructibleChunkParameters_Statics; \
	APEXDESTRUCTION_API static class UScriptStruct* StaticStruct();


template<> APEXDESTRUCTION_API UScriptStruct* StaticStruct<struct FDestructibleChunkParameters>();

#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_53_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFractureMaterial_Statics; \
	APEXDESTRUCTION_API static class UScriptStruct* StaticStruct();


template<> APEXDESTRUCTION_API UScriptStruct* StaticStruct<struct FFractureMaterial>();

#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_SPARSE_DATA
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UDestructibleFractureSettings, APEXDESTRUCTION_API)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDestructibleFractureSettings(); \
	friend struct Z_Construct_UClass_UDestructibleFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UDestructibleFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ApexDestruction"), APEXDESTRUCTION_API) \
	DECLARE_SERIALIZER(UDestructibleFractureSettings) \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_INCLASS \
private: \
	static void StaticRegisterNativesUDestructibleFractureSettings(); \
	friend struct Z_Construct_UClass_UDestructibleFractureSettings_Statics; \
public: \
	DECLARE_CLASS(UDestructibleFractureSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ApexDestruction"), APEXDESTRUCTION_API) \
	DECLARE_SERIALIZER(UDestructibleFractureSettings) \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	APEXDESTRUCTION_API UDestructibleFractureSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDestructibleFractureSettings) \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDestructibleFractureSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	APEXDESTRUCTION_API UDestructibleFractureSettings(UDestructibleFractureSettings&&); \
	APEXDESTRUCTION_API UDestructibleFractureSettings(const UDestructibleFractureSettings&); \
public:


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	APEXDESTRUCTION_API UDestructibleFractureSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	APEXDESTRUCTION_API UDestructibleFractureSettings(UDestructibleFractureSettings&&); \
	APEXDESTRUCTION_API UDestructibleFractureSettings(const UDestructibleFractureSettings&); \
public: \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDestructibleFractureSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDestructibleFractureSettings)


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_158_PROLOG
#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_SPARSE_DATA \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_INCLASS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_SPARSE_DATA \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h_162_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class DestructibleFractureSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APEXDESTRUCTION_API UClass* StaticClass<class UDestructibleFractureSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ApexDestruction_Source_ApexDestruction_Public_DestructibleFractureSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
