// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/BasicReplicationGraph.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBasicReplicationGraph() {}
// Cross Module References
	REPLICATIONGRAPH_API UScriptStruct* Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair();
	UPackage* Z_Construct_UPackage__Script_ReplicationGraph();
	ENGINE_API UClass* Z_Construct_UClass_UNetConnection_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UBasicReplicationGraph_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UBasicReplicationGraph();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraph();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_ActorList_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
class UScriptStruct* FConnectionAlwaysRelevantNodePair::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REPLICATIONGRAPH_API uint32 Get_Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair, Z_Construct_UPackage__Script_ReplicationGraph(), TEXT("ConnectionAlwaysRelevantNodePair"), sizeof(FConnectionAlwaysRelevantNodePair), Get_Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Hash());
	}
	return Singleton;
}
template<> REPLICATIONGRAPH_API UScriptStruct* StaticStruct<FConnectionAlwaysRelevantNodePair>()
{
	return FConnectionAlwaysRelevantNodePair::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConnectionAlwaysRelevantNodePair(FConnectionAlwaysRelevantNodePair::StaticStruct, TEXT("/Script/ReplicationGraph"), TEXT("ConnectionAlwaysRelevantNodePair"), false, nullptr, nullptr);
static struct FScriptStruct_ReplicationGraph_StaticRegisterNativesFConnectionAlwaysRelevantNodePair
{
	FScriptStruct_ReplicationGraph_StaticRegisterNativesFConnectionAlwaysRelevantNodePair()
	{
		UScriptStruct::DeferCppStructOps<FConnectionAlwaysRelevantNodePair>(FName(TEXT("ConnectionAlwaysRelevantNodePair")));
	}
} ScriptStruct_ReplicationGraph_StaticRegisterNativesFConnectionAlwaysRelevantNodePair;
	struct Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NetConnection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NetConnection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Node_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Node;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BasicReplicationGraph.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConnectionAlwaysRelevantNodePair>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewProp_NetConnection_MetaData[] = {
		{ "ModuleRelativePath", "Public/BasicReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewProp_NetConnection = { "NetConnection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConnectionAlwaysRelevantNodePair, NetConnection), Z_Construct_UClass_UNetConnection_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewProp_NetConnection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewProp_NetConnection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewProp_Node_MetaData[] = {
		{ "ModuleRelativePath", "Public/BasicReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewProp_Node = { "Node", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConnectionAlwaysRelevantNodePair, Node), Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewProp_Node_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewProp_Node_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewProp_NetConnection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::NewProp_Node,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
		nullptr,
		&NewStructOps,
		"ConnectionAlwaysRelevantNodePair",
		sizeof(FConnectionAlwaysRelevantNodePair),
		alignof(FConnectionAlwaysRelevantNodePair),
		Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ReplicationGraph();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConnectionAlwaysRelevantNodePair"), sizeof(FConnectionAlwaysRelevantNodePair), Get_Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Hash() { return 2415846526U; }
	void UBasicReplicationGraph::StaticRegisterNativesUBasicReplicationGraph()
	{
	}
	UClass* Z_Construct_UClass_UBasicReplicationGraph_NoRegister()
	{
		return UBasicReplicationGraph::StaticClass();
	}
	struct Z_Construct_UClass_UBasicReplicationGraph_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GridNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GridNode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlwaysRelevantNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AlwaysRelevantNode;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AlwaysRelevantForConnectionList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlwaysRelevantForConnectionList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AlwaysRelevantForConnectionList;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorsWithoutNetConnection_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorsWithoutNetConnection_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActorsWithoutNetConnection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBasicReplicationGraph_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraph,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBasicReplicationGraph_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * A basic implementation of replication graph. It only supports NetCullDistanceSquared, bAlwaysRelevant, bOnlyRelevantToOwner. These values cannot change per-actor at runtime. \n * This is meant to provide a simple example implementation. More robust implementations will be required for more complex games. ShootGame is another example to check out.\n * \n * To enable this via ini:\n * [/Script/OnlineSubsystemUtils.IpNetDriver]\n * ReplicationDriverClassName=\"/Script/ReplicationGraph.BasicReplicationGraph\"\n * \n **/" },
		{ "IncludePath", "BasicReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/BasicReplicationGraph.h" },
		{ "ToolTip", "A basic implementation of replication graph. It only supports NetCullDistanceSquared, bAlwaysRelevant, bOnlyRelevantToOwner. These values cannot change per-actor at runtime.\nThis is meant to provide a simple example implementation. More robust implementations will be required for more complex games. ShootGame is another example to check out.\n\nTo enable this via ini:\n[/Script/OnlineSubsystemUtils.IpNetDriver]\nReplicationDriverClassName=\"/Script/ReplicationGraph.BasicReplicationGraph\"" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_GridNode_MetaData[] = {
		{ "ModuleRelativePath", "Public/BasicReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_GridNode = { "GridNode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBasicReplicationGraph, GridNode), Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_GridNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_GridNode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantNode_MetaData[] = {
		{ "ModuleRelativePath", "Public/BasicReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantNode = { "AlwaysRelevantNode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBasicReplicationGraph, AlwaysRelevantNode), Z_Construct_UClass_UReplicationGraphNode_ActorList_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantNode_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantForConnectionList_Inner = { "AlwaysRelevantForConnectionList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantForConnectionList_MetaData[] = {
		{ "ModuleRelativePath", "Public/BasicReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantForConnectionList = { "AlwaysRelevantForConnectionList", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBasicReplicationGraph, AlwaysRelevantForConnectionList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantForConnectionList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantForConnectionList_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_ActorsWithoutNetConnection_Inner = { "ActorsWithoutNetConnection", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_ActorsWithoutNetConnection_MetaData[] = {
		{ "Comment", "/** Actors that are only supposed to replicate to their owning connection, but that did not have a connection on spawn */" },
		{ "ModuleRelativePath", "Public/BasicReplicationGraph.h" },
		{ "ToolTip", "Actors that are only supposed to replicate to their owning connection, but that did not have a connection on spawn" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_ActorsWithoutNetConnection = { "ActorsWithoutNetConnection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBasicReplicationGraph, ActorsWithoutNetConnection), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_ActorsWithoutNetConnection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_ActorsWithoutNetConnection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBasicReplicationGraph_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_GridNode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantNode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantForConnectionList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_AlwaysRelevantForConnectionList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_ActorsWithoutNetConnection_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBasicReplicationGraph_Statics::NewProp_ActorsWithoutNetConnection,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBasicReplicationGraph_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBasicReplicationGraph>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBasicReplicationGraph_Statics::ClassParams = {
		&UBasicReplicationGraph::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBasicReplicationGraph_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBasicReplicationGraph_Statics::PropPointers),
		0,
		0x000000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UBasicReplicationGraph_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBasicReplicationGraph_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBasicReplicationGraph()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBasicReplicationGraph_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBasicReplicationGraph, 386260798);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UBasicReplicationGraph>()
	{
		return UBasicReplicationGraph::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBasicReplicationGraph(Z_Construct_UClass_UBasicReplicationGraph, &UBasicReplicationGraph::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UBasicReplicationGraph"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBasicReplicationGraph);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
