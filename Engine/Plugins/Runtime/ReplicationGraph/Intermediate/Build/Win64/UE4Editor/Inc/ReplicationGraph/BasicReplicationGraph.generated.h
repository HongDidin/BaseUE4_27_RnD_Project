// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REPLICATIONGRAPH_BasicReplicationGraph_generated_h
#error "BasicReplicationGraph.generated.h already included, missing '#pragma once' in BasicReplicationGraph.h"
#endif
#define REPLICATIONGRAPH_BasicReplicationGraph_generated_h

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConnectionAlwaysRelevantNodePair_Statics; \
	REPLICATIONGRAPH_API static class UScriptStruct* StaticStruct();


template<> REPLICATIONGRAPH_API UScriptStruct* StaticStruct<struct FConnectionAlwaysRelevantNodePair>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBasicReplicationGraph(); \
	friend struct Z_Construct_UClass_UBasicReplicationGraph_Statics; \
public: \
	DECLARE_CLASS(UBasicReplicationGraph, UReplicationGraph, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UBasicReplicationGraph)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUBasicReplicationGraph(); \
	friend struct Z_Construct_UClass_UBasicReplicationGraph_Statics; \
public: \
	DECLARE_CLASS(UBasicReplicationGraph, UReplicationGraph, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UBasicReplicationGraph)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBasicReplicationGraph(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBasicReplicationGraph) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBasicReplicationGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBasicReplicationGraph); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBasicReplicationGraph(UBasicReplicationGraph&&); \
	NO_API UBasicReplicationGraph(const UBasicReplicationGraph&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBasicReplicationGraph(UBasicReplicationGraph&&); \
	NO_API UBasicReplicationGraph(const UBasicReplicationGraph&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBasicReplicationGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBasicReplicationGraph); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBasicReplicationGraph)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_34_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UBasicReplicationGraph>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ReplicationGraph_Source_Public_BasicReplicationGraph_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
