// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REPLICATIONGRAPH_ReplicationGraphTypes_generated_h
#error "ReplicationGraphTypes.generated.h already included, missing '#pragma once' in ReplicationGraphTypes.h"
#endif
#define REPLICATIONGRAPH_ReplicationGraphTypes_generated_h

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraphTypes_h_434_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FClassReplicationInfo_Statics; \
	REPLICATIONGRAPH_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__CullDistance() { return STRUCT_OFFSET(FClassReplicationInfo, CullDistance); } \
	FORCEINLINE static uint32 __PPO__CullDistanceSquared() { return STRUCT_OFFSET(FClassReplicationInfo, CullDistanceSquared); }


template<> REPLICATIONGRAPH_API UScriptStruct* StaticStruct<struct FClassReplicationInfo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraphTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
