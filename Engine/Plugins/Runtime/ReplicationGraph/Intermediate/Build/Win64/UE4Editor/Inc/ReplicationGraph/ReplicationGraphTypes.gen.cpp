// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/ReplicationGraphTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReplicationGraphTypes() {}
// Cross Module References
	REPLICATIONGRAPH_API UScriptStruct* Z_Construct_UScriptStruct_FClassReplicationInfo();
	UPackage* Z_Construct_UPackage__Script_ReplicationGraph();
// End Cross Module References
class UScriptStruct* FClassReplicationInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REPLICATIONGRAPH_API uint32 Get_Z_Construct_UScriptStruct_FClassReplicationInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FClassReplicationInfo, Z_Construct_UPackage__Script_ReplicationGraph(), TEXT("ClassReplicationInfo"), sizeof(FClassReplicationInfo), Get_Z_Construct_UScriptStruct_FClassReplicationInfo_Hash());
	}
	return Singleton;
}
template<> REPLICATIONGRAPH_API UScriptStruct* StaticStruct<FClassReplicationInfo>()
{
	return FClassReplicationInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FClassReplicationInfo(FClassReplicationInfo::StaticStruct, TEXT("/Script/ReplicationGraph"), TEXT("ClassReplicationInfo"), false, nullptr, nullptr);
static struct FScriptStruct_ReplicationGraph_StaticRegisterNativesFClassReplicationInfo
{
	FScriptStruct_ReplicationGraph_StaticRegisterNativesFClassReplicationInfo()
	{
		UScriptStruct::DeferCppStructOps<FClassReplicationInfo>(FName(TEXT("ClassReplicationInfo")));
	}
} ScriptStruct_ReplicationGraph_StaticRegisterNativesFClassReplicationInfo;
	struct Z_Construct_UScriptStruct_FClassReplicationInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistancePriorityScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DistancePriorityScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StarvationPriorityScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StarvationPriorityScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccumulatedNetPriorityBias_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AccumulatedNetPriorityBias;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplicationPeriodFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ReplicationPeriodFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FastPath_ReplicationPeriodFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_FastPath_ReplicationPeriodFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorChannelFrameTimeout_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ActorChannelFrameTimeout;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CullDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CullDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CullDistanceSquared_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CullDistanceSquared;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Per-Class actor data about how the actor replicates */" },
		{ "ModuleRelativePath", "Public/ReplicationGraphTypes.h" },
		{ "ToolTip", "Per-Class actor data about how the actor replicates" },
	};
#endif
	void* Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FClassReplicationInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_DistancePriorityScale_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraphTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_DistancePriorityScale = { "DistancePriorityScale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClassReplicationInfo, DistancePriorityScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_DistancePriorityScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_DistancePriorityScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_StarvationPriorityScale_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraphTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_StarvationPriorityScale = { "StarvationPriorityScale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClassReplicationInfo, StarvationPriorityScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_StarvationPriorityScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_StarvationPriorityScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_AccumulatedNetPriorityBias_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraphTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_AccumulatedNetPriorityBias = { "AccumulatedNetPriorityBias", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClassReplicationInfo, AccumulatedNetPriorityBias), METADATA_PARAMS(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_AccumulatedNetPriorityBias_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_AccumulatedNetPriorityBias_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_ReplicationPeriodFrame_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraphTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_ReplicationPeriodFrame = { "ReplicationPeriodFrame", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClassReplicationInfo, ReplicationPeriodFrame), METADATA_PARAMS(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_ReplicationPeriodFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_ReplicationPeriodFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_FastPath_ReplicationPeriodFrame_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraphTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_FastPath_ReplicationPeriodFrame = { "FastPath_ReplicationPeriodFrame", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClassReplicationInfo, FastPath_ReplicationPeriodFrame), METADATA_PARAMS(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_FastPath_ReplicationPeriodFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_FastPath_ReplicationPeriodFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_ActorChannelFrameTimeout_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraphTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_ActorChannelFrameTimeout = { "ActorChannelFrameTimeout", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClassReplicationInfo, ActorChannelFrameTimeout), METADATA_PARAMS(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_ActorChannelFrameTimeout_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_ActorChannelFrameTimeout_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_CullDistance_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraphTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_CullDistance = { "CullDistance", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClassReplicationInfo, CullDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_CullDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_CullDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_CullDistanceSquared_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraphTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_CullDistanceSquared = { "CullDistanceSquared", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClassReplicationInfo, CullDistanceSquared), METADATA_PARAMS(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_CullDistanceSquared_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_CullDistanceSquared_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_DistancePriorityScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_StarvationPriorityScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_AccumulatedNetPriorityBias,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_ReplicationPeriodFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_FastPath_ReplicationPeriodFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_ActorChannelFrameTimeout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_CullDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::NewProp_CullDistanceSquared,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
		nullptr,
		&NewStructOps,
		"ClassReplicationInfo",
		sizeof(FClassReplicationInfo),
		alignof(FClassReplicationInfo),
		Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FClassReplicationInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FClassReplicationInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ReplicationGraph();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ClassReplicationInfo"), sizeof(FClassReplicationInfo), Get_Z_Construct_UScriptStruct_FClassReplicationInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FClassReplicationInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FClassReplicationInfo_Hash() { return 2697759254U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
