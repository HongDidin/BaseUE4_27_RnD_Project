// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/ReplicationGraph.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReplicationGraph() {}
// Cross Module References
	REPLICATIONGRAPH_API UScriptStruct* Z_Construct_UScriptStruct_FLastLocationGatherInfo();
	UPackage* Z_Construct_UPackage__Script_ReplicationGraph();
	ENGINE_API UClass* Z_Construct_UClass_UNetConnection_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	REPLICATIONGRAPH_API UScriptStruct* Z_Construct_UScriptStruct_FTearOffActorInfo();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	REPLICATIONGRAPH_API UScriptStruct* Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_ActorList_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_ActorList();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_DormancyNode_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_DormancyNode();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_GridCell_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_GridCell();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraph_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UReplicationGraph();
	ENGINE_API UClass* Z_Construct_UClass_UReplicationDriver();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UNetReplicationGraphConnection_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UNetDriver_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_UNetReplicationGraphConnection();
	ENGINE_API UClass* Z_Construct_UClass_UReplicationConnectionDriver();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_AReplicationGraphDebugActor_NoRegister();
	REPLICATIONGRAPH_API UClass* Z_Construct_UClass_AReplicationGraphDebugActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
class UScriptStruct* FLastLocationGatherInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REPLICATIONGRAPH_API uint32 Get_Z_Construct_UScriptStruct_FLastLocationGatherInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLastLocationGatherInfo, Z_Construct_UPackage__Script_ReplicationGraph(), TEXT("LastLocationGatherInfo"), sizeof(FLastLocationGatherInfo), Get_Z_Construct_UScriptStruct_FLastLocationGatherInfo_Hash());
	}
	return Singleton;
}
template<> REPLICATIONGRAPH_API UScriptStruct* StaticStruct<FLastLocationGatherInfo>()
{
	return FLastLocationGatherInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLastLocationGatherInfo(FLastLocationGatherInfo::StaticStruct, TEXT("/Script/ReplicationGraph"), TEXT("LastLocationGatherInfo"), false, nullptr, nullptr);
static struct FScriptStruct_ReplicationGraph_StaticRegisterNativesFLastLocationGatherInfo
{
	FScriptStruct_ReplicationGraph_StaticRegisterNativesFLastLocationGatherInfo()
	{
		UScriptStruct::DeferCppStructOps<FLastLocationGatherInfo>(FName(TEXT("LastLocationGatherInfo")));
	}
} ScriptStruct_ReplicationGraph_StaticRegisterNativesFLastLocationGatherInfo;
	struct Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Connection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Connection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastOutOfRangeLocationCheck_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastOutOfRangeLocationCheck;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// --------------------------------------------------------------------------------------------------------------------------------------------\n// --------------------------------------------------------------------------------------------------------------------------------------------\n// --------------------------------------------------------------------------------------------------------------------------------------------\n" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLastLocationGatherInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_Connection_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_Connection = { "Connection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLastLocationGatherInfo, Connection), Z_Construct_UClass_UNetConnection_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_Connection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_Connection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_LastLocation_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_LastLocation = { "LastLocation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLastLocationGatherInfo, LastLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_LastLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_LastLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_LastOutOfRangeLocationCheck_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_LastOutOfRangeLocationCheck = { "LastOutOfRangeLocationCheck", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLastLocationGatherInfo, LastOutOfRangeLocationCheck), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_LastOutOfRangeLocationCheck_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_LastOutOfRangeLocationCheck_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_Connection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_LastLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::NewProp_LastOutOfRangeLocationCheck,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
		nullptr,
		&NewStructOps,
		"LastLocationGatherInfo",
		sizeof(FLastLocationGatherInfo),
		alignof(FLastLocationGatherInfo),
		Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLastLocationGatherInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLastLocationGatherInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ReplicationGraph();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LastLocationGatherInfo"), sizeof(FLastLocationGatherInfo), Get_Z_Construct_UScriptStruct_FLastLocationGatherInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLastLocationGatherInfo_Hash() { return 77871315U; }
class UScriptStruct* FTearOffActorInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REPLICATIONGRAPH_API uint32 Get_Z_Construct_UScriptStruct_FTearOffActorInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTearOffActorInfo, Z_Construct_UPackage__Script_ReplicationGraph(), TEXT("TearOffActorInfo"), sizeof(FTearOffActorInfo), Get_Z_Construct_UScriptStruct_FTearOffActorInfo_Hash());
	}
	return Singleton;
}
template<> REPLICATIONGRAPH_API UScriptStruct* StaticStruct<FTearOffActorInfo>()
{
	return FTearOffActorInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTearOffActorInfo(FTearOffActorInfo::StaticStruct, TEXT("/Script/ReplicationGraph"), TEXT("TearOffActorInfo"), false, nullptr, nullptr);
static struct FScriptStruct_ReplicationGraph_StaticRegisterNativesFTearOffActorInfo
{
	FScriptStruct_ReplicationGraph_StaticRegisterNativesFTearOffActorInfo()
	{
		UScriptStruct::DeferCppStructOps<FTearOffActorInfo>(FName(TEXT("TearOffActorInfo")));
	}
} ScriptStruct_ReplicationGraph_StaticRegisterNativesFTearOffActorInfo;
	struct Z_Construct_UScriptStruct_FTearOffActorInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// -----------------------------------\n" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTearOffActorInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::NewProp_Actor_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTearOffActorInfo, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::NewProp_Actor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::NewProp_Actor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::NewProp_Actor,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
		nullptr,
		&NewStructOps,
		"TearOffActorInfo",
		sizeof(FTearOffActorInfo),
		alignof(FTearOffActorInfo),
		Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTearOffActorInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTearOffActorInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ReplicationGraph();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TearOffActorInfo"), sizeof(FTearOffActorInfo), Get_Z_Construct_UScriptStruct_FTearOffActorInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTearOffActorInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTearOffActorInfo_Hash() { return 3454099632U; }
class UScriptStruct* FAlwaysRelevantActorInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REPLICATIONGRAPH_API uint32 Get_Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo, Z_Construct_UPackage__Script_ReplicationGraph(), TEXT("AlwaysRelevantActorInfo"), sizeof(FAlwaysRelevantActorInfo), Get_Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Hash());
	}
	return Singleton;
}
template<> REPLICATIONGRAPH_API UScriptStruct* StaticStruct<FAlwaysRelevantActorInfo>()
{
	return FAlwaysRelevantActorInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAlwaysRelevantActorInfo(FAlwaysRelevantActorInfo::StaticStruct, TEXT("/Script/ReplicationGraph"), TEXT("AlwaysRelevantActorInfo"), false, nullptr, nullptr);
static struct FScriptStruct_ReplicationGraph_StaticRegisterNativesFAlwaysRelevantActorInfo
{
	FScriptStruct_ReplicationGraph_StaticRegisterNativesFAlwaysRelevantActorInfo()
	{
		UScriptStruct::DeferCppStructOps<FAlwaysRelevantActorInfo>(FName(TEXT("AlwaysRelevantActorInfo")));
	}
} ScriptStruct_ReplicationGraph_StaticRegisterNativesFAlwaysRelevantActorInfo;
	struct Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Connection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Connection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastViewer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LastViewer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastViewTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LastViewTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// -----------------------------------\n" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAlwaysRelevantActorInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_Connection_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_Connection = { "Connection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAlwaysRelevantActorInfo, Connection), Z_Construct_UClass_UNetConnection_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_Connection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_Connection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_LastViewer_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_LastViewer = { "LastViewer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAlwaysRelevantActorInfo, LastViewer), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_LastViewer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_LastViewer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_LastViewTarget_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_LastViewTarget = { "LastViewTarget", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAlwaysRelevantActorInfo, LastViewTarget), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_LastViewTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_LastViewTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_Connection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_LastViewer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::NewProp_LastViewTarget,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
		nullptr,
		&NewStructOps,
		"AlwaysRelevantActorInfo",
		sizeof(FAlwaysRelevantActorInfo),
		alignof(FAlwaysRelevantActorInfo),
		Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ReplicationGraph();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AlwaysRelevantActorInfo"), sizeof(FAlwaysRelevantActorInfo), Get_Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Hash() { return 1779506167U; }
	void UReplicationGraphNode::StaticRegisterNativesUReplicationGraphNode()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_NoRegister()
	{
		return UReplicationGraphNode::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AllChildNodes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllChildNodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllChildNodes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReplicationGraphNode_Statics::NewProp_AllChildNodes_Inner = { "AllChildNodes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UReplicationGraphNode_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_Statics::NewProp_AllChildNodes_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UReplicationGraphNode_Statics::NewProp_AllChildNodes = { "AllChildNodes", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraphNode, AllChildNodes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_Statics::NewProp_AllChildNodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_Statics::NewProp_AllChildNodes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UReplicationGraphNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraphNode_Statics::NewProp_AllChildNodes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraphNode_Statics::NewProp_AllChildNodes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_Statics::ClassParams = {
		&UReplicationGraphNode::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UReplicationGraphNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_Statics::PropPointers),
		0,
		0x001000A9u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode, 700399140);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode>()
	{
		return UReplicationGraphNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode(Z_Construct_UClass_UReplicationGraphNode, &UReplicationGraphNode::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode);
	void UReplicationGraphNode_ActorList::StaticRegisterNativesUReplicationGraphNode_ActorList()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_ActorList_NoRegister()
	{
		return UReplicationGraphNode_ActorList::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_ActorList_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_ActorList_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_ActorList_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A Node that contains ReplicateActorLists. This contains 1 \"base\" list and a TArray of lists that are conditioned on a streaming level being loaded. */" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "A Node that contains ReplicateActorLists. This contains 1 \"base\" list and a TArray of lists that are conditioned on a streaming level being loaded." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_ActorList_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode_ActorList>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_ActorList_Statics::ClassParams = {
		&UReplicationGraphNode_ActorList::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_ActorList_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_ActorList_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode_ActorList()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_ActorList_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode_ActorList, 310628414);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode_ActorList>()
	{
		return UReplicationGraphNode_ActorList::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode_ActorList(Z_Construct_UClass_UReplicationGraphNode_ActorList, &UReplicationGraphNode_ActorList::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode_ActorList"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode_ActorList);
	void UReplicationGraphNode_ActorListFrequencyBuckets::StaticRegisterNativesUReplicationGraphNode_ActorListFrequencyBuckets()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_NoRegister()
	{
		return UReplicationGraphNode_ActorListFrequencyBuckets::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A Node that contains ReplicateActorLists. This contains multiple buckets for non streaming level actors and will pull from each bucket on alternating frames. It is a way to broadly load balance. */" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "A Node that contains ReplicateActorLists. This contains multiple buckets for non streaming level actors and will pull from each bucket on alternating frames. It is a way to broadly load balance." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode_ActorListFrequencyBuckets>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_Statics::ClassParams = {
		&UReplicationGraphNode_ActorListFrequencyBuckets::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode_ActorListFrequencyBuckets, 1501611272);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode_ActorListFrequencyBuckets>()
	{
		return UReplicationGraphNode_ActorListFrequencyBuckets::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode_ActorListFrequencyBuckets(Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets, &UReplicationGraphNode_ActorListFrequencyBuckets::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode_ActorListFrequencyBuckets"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode_ActorListFrequencyBuckets);
	void UReplicationGraphNode_DynamicSpatialFrequency::StaticRegisterNativesUReplicationGraphNode_DynamicSpatialFrequency()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_NoRegister()
	{
		return UReplicationGraphNode_DynamicSpatialFrequency::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraphNode_ActorList,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A node intended for dynamic (moving) actors where replication frequency is based on distance to the connection's view location */" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "A node intended for dynamic (moving) actors where replication frequency is based on distance to the connection's view location" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode_DynamicSpatialFrequency>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_Statics::ClassParams = {
		&UReplicationGraphNode_DynamicSpatialFrequency::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode_DynamicSpatialFrequency, 12094269);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode_DynamicSpatialFrequency>()
	{
		return UReplicationGraphNode_DynamicSpatialFrequency::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode_DynamicSpatialFrequency(Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency, &UReplicationGraphNode_DynamicSpatialFrequency::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode_DynamicSpatialFrequency"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode_DynamicSpatialFrequency);
	void UReplicationGraphNode_ConnectionDormancyNode::StaticRegisterNativesUReplicationGraphNode_ConnectionDormancyNode()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_NoRegister()
	{
		return UReplicationGraphNode_ConnectionDormancyNode::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraphNode_ActorList,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Removes dormant (on connection) actors from its rep lists */" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "Removes dormant (on connection) actors from its rep lists" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode_ConnectionDormancyNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_Statics::ClassParams = {
		&UReplicationGraphNode_ConnectionDormancyNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode_ConnectionDormancyNode, 2809271839);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode_ConnectionDormancyNode>()
	{
		return UReplicationGraphNode_ConnectionDormancyNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode_ConnectionDormancyNode(Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode, &UReplicationGraphNode_ConnectionDormancyNode::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode_ConnectionDormancyNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode_ConnectionDormancyNode);
	void UReplicationGraphNode_DormancyNode::StaticRegisterNativesUReplicationGraphNode_DormancyNode()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_DormancyNode_NoRegister()
	{
		return UReplicationGraphNode_DormancyNode::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_DormancyNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_DormancyNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraphNode_ActorList,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_DormancyNode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Stores per-connection copies of a master actor list. Skips and removes elements from per connection list that are fully dormant */" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "Stores per-connection copies of a master actor list. Skips and removes elements from per connection list that are fully dormant" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_DormancyNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode_DormancyNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_DormancyNode_Statics::ClassParams = {
		&UReplicationGraphNode_DormancyNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_DormancyNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_DormancyNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode_DormancyNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_DormancyNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode_DormancyNode, 3024779121);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode_DormancyNode>()
	{
		return UReplicationGraphNode_DormancyNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode_DormancyNode(Z_Construct_UClass_UReplicationGraphNode_DormancyNode, &UReplicationGraphNode_DormancyNode::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode_DormancyNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode_DormancyNode);
	void UReplicationGraphNode_GridCell::StaticRegisterNativesUReplicationGraphNode_GridCell()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_GridCell_NoRegister()
	{
		return UReplicationGraphNode_GridCell::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicNode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DormancyNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DormancyNode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraphNode_ActorList,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::NewProp_DynamicNode_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::NewProp_DynamicNode = { "DynamicNode", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraphNode_GridCell, DynamicNode), Z_Construct_UClass_UReplicationGraphNode_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::NewProp_DynamicNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::NewProp_DynamicNode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::NewProp_DormancyNode_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::NewProp_DormancyNode = { "DormancyNode", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraphNode_GridCell, DormancyNode), Z_Construct_UClass_UReplicationGraphNode_DormancyNode_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::NewProp_DormancyNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::NewProp_DormancyNode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::NewProp_DynamicNode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::NewProp_DormancyNode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode_GridCell>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::ClassParams = {
		&UReplicationGraphNode_GridCell::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode_GridCell()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode_GridCell, 3976565480);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode_GridCell>()
	{
		return UReplicationGraphNode_GridCell::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode_GridCell(Z_Construct_UClass_UReplicationGraphNode_GridCell, &UReplicationGraphNode_GridCell::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode_GridCell"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode_GridCell);
	void UReplicationGraphNode_GridSpatialization2D::StaticRegisterNativesUReplicationGraphNode_GridSpatialization2D()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_NoRegister()
	{
		return UReplicationGraphNode_GridSpatialization2D::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// -----------------------------------\n" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode_GridSpatialization2D>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_Statics::ClassParams = {
		&UReplicationGraphNode_GridSpatialization2D::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode_GridSpatialization2D, 4077802152);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode_GridSpatialization2D>()
	{
		return UReplicationGraphNode_GridSpatialization2D::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode_GridSpatialization2D(Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D, &UReplicationGraphNode_GridSpatialization2D::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode_GridSpatialization2D"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode_GridSpatialization2D);
	void UReplicationGraphNode_AlwaysRelevant::StaticRegisterNativesUReplicationGraphNode_AlwaysRelevant()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_NoRegister()
	{
		return UReplicationGraphNode_AlwaysRelevant::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChildNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ChildNode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// -----------------------------------\n" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::NewProp_ChildNode_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::NewProp_ChildNode = { "ChildNode", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraphNode_AlwaysRelevant, ChildNode), Z_Construct_UClass_UReplicationGraphNode_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::NewProp_ChildNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::NewProp_ChildNode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::NewProp_ChildNode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode_AlwaysRelevant>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::ClassParams = {
		&UReplicationGraphNode_AlwaysRelevant::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode_AlwaysRelevant, 3352357296);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode_AlwaysRelevant>()
	{
		return UReplicationGraphNode_AlwaysRelevant::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode_AlwaysRelevant(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant, &UReplicationGraphNode_AlwaysRelevant::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode_AlwaysRelevant"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode_AlwaysRelevant);
	void UReplicationGraphNode_AlwaysRelevant_ForConnection::StaticRegisterNativesUReplicationGraphNode_AlwaysRelevant_ForConnection()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_NoRegister()
	{
		return UReplicationGraphNode_AlwaysRelevant_ForConnection::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PastRelevantActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PastRelevantActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PastRelevantActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraphNode_ActorList,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Adds actors that are always relevant for a connection. This engine version just adds the PlayerController and ViewTarget (usually the pawn) */" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "Adds actors that are always relevant for a connection. This engine version just adds the PlayerController and ViewTarget (usually the pawn)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::NewProp_PastRelevantActors_Inner = { "PastRelevantActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::NewProp_PastRelevantActors_MetaData[] = {
		{ "Comment", "/** List of previously (or currently if nothing changed last tick) focused actor data per connection */" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "List of previously (or currently if nothing changed last tick) focused actor data per connection" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::NewProp_PastRelevantActors = { "PastRelevantActors", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraphNode_AlwaysRelevant_ForConnection, PastRelevantActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::NewProp_PastRelevantActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::NewProp_PastRelevantActors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::NewProp_PastRelevantActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::NewProp_PastRelevantActors,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode_AlwaysRelevant_ForConnection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::ClassParams = {
		&UReplicationGraphNode_AlwaysRelevant_ForConnection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode_AlwaysRelevant_ForConnection, 1282820460);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode_AlwaysRelevant_ForConnection>()
	{
		return UReplicationGraphNode_AlwaysRelevant_ForConnection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection(Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection, &UReplicationGraphNode_AlwaysRelevant_ForConnection::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode_AlwaysRelevant_ForConnection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode_AlwaysRelevant_ForConnection);
	void UReplicationGraphNode_TearOff_ForConnection::StaticRegisterNativesUReplicationGraphNode_TearOff_ForConnection()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_NoRegister()
	{
		return UReplicationGraphNode_TearOff_ForConnection::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TearOffActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TearOffActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TearOffActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Manages actors that are Tear Off. We will try to replicate these actors one last time to each connection. */" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "Manages actors that are Tear Off. We will try to replicate these actors one last time to each connection." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::NewProp_TearOffActors_Inner = { "TearOffActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTearOffActorInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::NewProp_TearOffActors_MetaData[] = {
		{ "Comment", "// Fixme: not safe to have persistent FActorRepListrefViews yet, so need a uproperty based list to hold the persistent items.\n" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "Fixme: not safe to have persistent FActorRepListrefViews yet, so need a uproperty based list to hold the persistent items." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::NewProp_TearOffActors = { "TearOffActors", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraphNode_TearOff_ForConnection, TearOffActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::NewProp_TearOffActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::NewProp_TearOffActors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::NewProp_TearOffActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::NewProp_TearOffActors,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraphNode_TearOff_ForConnection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::ClassParams = {
		&UReplicationGraphNode_TearOff_ForConnection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraphNode_TearOff_ForConnection, 2049904855);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraphNode_TearOff_ForConnection>()
	{
		return UReplicationGraphNode_TearOff_ForConnection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraphNode_TearOff_ForConnection(Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection, &UReplicationGraphNode_TearOff_ForConnection::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraphNode_TearOff_ForConnection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraphNode_TearOff_ForConnection);
	void UReplicationGraph::StaticRegisterNativesUReplicationGraph()
	{
	}
	UClass* Z_Construct_UClass_UReplicationGraph_NoRegister()
	{
		return UReplicationGraph::StaticClass();
	}
	struct Z_Construct_UClass_UReplicationGraph_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplicationConnectionManagerClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ReplicationConnectionManagerClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NetDriver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NetDriver;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Connections_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Connections_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Connections;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PendingConnections_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendingConnections_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PendingConnections;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GlobalGraphNodes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GlobalGraphNodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GlobalGraphNodes;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PrepareForReplicationNodes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrepareForReplicationNodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PrepareForReplicationNodes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplicationGraph_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationDriver,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraph_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Manages actor replication for an entire world / net driver */" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "Manages actor replication for an entire world / net driver" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraph_Statics::NewProp_ReplicationConnectionManagerClass_MetaData[] = {
		{ "Comment", "/** The per-connection manager class to instantiate. This will be read off the instantiated UNetReplicationManager. */" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "The per-connection manager class to instantiate. This will be read off the instantiated UNetReplicationManager." },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UReplicationGraph_Statics::NewProp_ReplicationConnectionManagerClass = { "ReplicationConnectionManagerClass", nullptr, (EPropertyFlags)0x0014000000004000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraph, ReplicationConnectionManagerClass), Z_Construct_UClass_UNetReplicationGraphConnection_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_ReplicationConnectionManagerClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_ReplicationConnectionManagerClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraph_Statics::NewProp_NetDriver_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReplicationGraph_Statics::NewProp_NetDriver = { "NetDriver", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraph, NetDriver), Z_Construct_UClass_UNetDriver_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_NetDriver_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_NetDriver_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReplicationGraph_Statics::NewProp_Connections_Inner = { "Connections", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNetReplicationGraphConnection_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraph_Statics::NewProp_Connections_MetaData[] = {
		{ "Comment", "/** List of connection managers. This list is not sorted and not stable. */" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "List of connection managers. This list is not sorted and not stable." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UReplicationGraph_Statics::NewProp_Connections = { "Connections", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraph, Connections), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_Connections_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_Connections_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PendingConnections_Inner = { "PendingConnections", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNetReplicationGraphConnection_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PendingConnections_MetaData[] = {
		{ "Comment", "/** ConnectionManagers that we have created but haven't officially been added to the net driver's ClientConnection list. This is a  hack for silly order of initialization issues. */" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "ConnectionManagers that we have created but haven't officially been added to the net driver's ClientConnection list. This is a  hack for silly order of initialization issues." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PendingConnections = { "PendingConnections", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraph, PendingConnections), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PendingConnections_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PendingConnections_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReplicationGraph_Statics::NewProp_GlobalGraphNodes_Inner = { "GlobalGraphNodes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UReplicationGraphNode_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraph_Statics::NewProp_GlobalGraphNodes_MetaData[] = {
		{ "Comment", "/** A list of nodes that can add actors to all connections. They don't necessarily *have to* add actors to each connection, but they will get a chance to. These are added via ::AddGlobalGraphNode  */" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "A list of nodes that can add actors to all connections. They don't necessarily *have to* add actors to each connection, but they will get a chance to. These are added via ::AddGlobalGraphNode" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UReplicationGraph_Statics::NewProp_GlobalGraphNodes = { "GlobalGraphNodes", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraph, GlobalGraphNodes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_GlobalGraphNodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_GlobalGraphNodes_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PrepareForReplicationNodes_Inner = { "PrepareForReplicationNodes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UReplicationGraphNode_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PrepareForReplicationNodes_MetaData[] = {
		{ "Comment", "/** A list of nodes that want PrepareForReplication() to be called on them at the top of the replication frame. */" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "A list of nodes that want PrepareForReplication() to be called on them at the top of the replication frame." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PrepareForReplicationNodes = { "PrepareForReplicationNodes", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReplicationGraph, PrepareForReplicationNodes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PrepareForReplicationNodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PrepareForReplicationNodes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UReplicationGraph_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraph_Statics::NewProp_ReplicationConnectionManagerClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraph_Statics::NewProp_NetDriver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraph_Statics::NewProp_Connections_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraph_Statics::NewProp_Connections,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PendingConnections_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PendingConnections,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraph_Statics::NewProp_GlobalGraphNodes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraph_Statics::NewProp_GlobalGraphNodes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PrepareForReplicationNodes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReplicationGraph_Statics::NewProp_PrepareForReplicationNodes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplicationGraph_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplicationGraph>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplicationGraph_Statics::ClassParams = {
		&UReplicationGraph::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UReplicationGraph_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraph_Statics::PropPointers),
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UReplicationGraph_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplicationGraph_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplicationGraph()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplicationGraph_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplicationGraph, 703851287);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UReplicationGraph>()
	{
		return UReplicationGraph::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplicationGraph(Z_Construct_UClass_UReplicationGraph, &UReplicationGraph::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UReplicationGraph"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplicationGraph);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UReplicationGraph)
	void UNetReplicationGraphConnection::StaticRegisterNativesUNetReplicationGraphConnection()
	{
	}
	UClass* Z_Construct_UClass_UNetReplicationGraphConnection_NoRegister()
	{
		return UNetReplicationGraphConnection::StaticClass();
	}
	struct Z_Construct_UClass_UNetReplicationGraphConnection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NetConnection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NetConnection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DebugActor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastGatherLocations_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastGatherLocations_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LastGatherLocations;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConnectionGraphNodes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionGraphNodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ConnectionGraphNodes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TearOffNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TearOffNode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNetReplicationGraphConnection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReplicationConnectionDriver,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetReplicationGraphConnection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Manages actor replication for a specific connection */" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "Manages actor replication for a specific connection" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_NetConnection_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_NetConnection = { "NetConnection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNetReplicationGraphConnection, NetConnection), Z_Construct_UClass_UNetConnection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_NetConnection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_NetConnection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_DebugActor_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_DebugActor = { "DebugActor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNetReplicationGraphConnection, DebugActor), Z_Construct_UClass_AReplicationGraphDebugActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_DebugActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_DebugActor_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_LastGatherLocations_Inner = { "LastGatherLocations", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLastLocationGatherInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_LastGatherLocations_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_LastGatherLocations = { "LastGatherLocations", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNetReplicationGraphConnection, LastGatherLocations), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_LastGatherLocations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_LastGatherLocations_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_ConnectionGraphNodes_Inner = { "ConnectionGraphNodes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UReplicationGraphNode_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_ConnectionGraphNodes_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_ConnectionGraphNodes = { "ConnectionGraphNodes", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNetReplicationGraphConnection, ConnectionGraphNodes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_ConnectionGraphNodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_ConnectionGraphNodes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_TearOffNode_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_TearOffNode = { "TearOffNode", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNetReplicationGraphConnection, TearOffNode), Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_TearOffNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_TearOffNode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNetReplicationGraphConnection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_NetConnection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_DebugActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_LastGatherLocations_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_LastGatherLocations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_ConnectionGraphNodes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_ConnectionGraphNodes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNetReplicationGraphConnection_Statics::NewProp_TearOffNode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNetReplicationGraphConnection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNetReplicationGraphConnection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNetReplicationGraphConnection_Statics::ClassParams = {
		&UNetReplicationGraphConnection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNetReplicationGraphConnection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNetReplicationGraphConnection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNetReplicationGraphConnection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNetReplicationGraphConnection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNetReplicationGraphConnection, 407965790);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<UNetReplicationGraphConnection>()
	{
		return UNetReplicationGraphConnection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNetReplicationGraphConnection(Z_Construct_UClass_UNetReplicationGraphConnection, &UNetReplicationGraphConnection::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("UNetReplicationGraphConnection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNetReplicationGraphConnection);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UNetReplicationGraphConnection)
	DEFINE_FUNCTION(AReplicationGraphDebugActor::execClientCellInfo)
	{
		P_GET_STRUCT(FVector,Z_Param_CellLocation);
		P_GET_STRUCT(FVector,Z_Param_CellExtent);
		P_GET_TARRAY(AActor*,Z_Param_Actors);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClientCellInfo_Implementation(Z_Param_CellLocation,Z_Param_CellExtent,Z_Param_Actors);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AReplicationGraphDebugActor::execServerPrintCullDistances)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ServerPrintCullDistances_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AReplicationGraphDebugActor::execServerSetConditionalActorBreakpoint)
	{
		P_GET_OBJECT(AActor,Z_Param_Actor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ServerSetConditionalActorBreakpoint_Implementation(Z_Param_Actor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AReplicationGraphDebugActor::execServerSetPeriodFrameForClass)
	{
		P_GET_OBJECT(UClass,Z_Param_Class);
		P_GET_PROPERTY(FIntProperty,Z_Param_PeriodFrame);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ServerSetPeriodFrameForClass_Implementation(Z_Param_Class,Z_Param_PeriodFrame);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AReplicationGraphDebugActor::execServerSetCullDistanceForClass)
	{
		P_GET_OBJECT(UClass,Z_Param_Class);
		P_GET_PROPERTY(FFloatProperty,Z_Param_CullDistance);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ServerSetCullDistanceForClass_Implementation(Z_Param_Class,Z_Param_CullDistance);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AReplicationGraphDebugActor::execServerPrintAllActorInfo)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Str);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ServerPrintAllActorInfo_Implementation(Z_Param_Str);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AReplicationGraphDebugActor::execServerCellInfo)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ServerCellInfo_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AReplicationGraphDebugActor::execServerStopDebugging)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ServerStopDebugging_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AReplicationGraphDebugActor::execServerStartDebugging)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ServerStartDebugging_Implementation();
		P_NATIVE_END;
	}
	static FName NAME_AReplicationGraphDebugActor_ClientCellInfo = FName(TEXT("ClientCellInfo"));
	void AReplicationGraphDebugActor::ClientCellInfo(FVector CellLocation, FVector CellExtent, TArray<AActor*> const& Actors)
	{
		ReplicationGraphDebugActor_eventClientCellInfo_Parms Parms;
		Parms.CellLocation=CellLocation;
		Parms.CellExtent=CellExtent;
		Parms.Actors=Actors;
		ProcessEvent(FindFunctionChecked(NAME_AReplicationGraphDebugActor_ClientCellInfo),&Parms);
	}
	static FName NAME_AReplicationGraphDebugActor_ServerCellInfo = FName(TEXT("ServerCellInfo"));
	void AReplicationGraphDebugActor::ServerCellInfo()
	{
		ProcessEvent(FindFunctionChecked(NAME_AReplicationGraphDebugActor_ServerCellInfo),NULL);
	}
	static FName NAME_AReplicationGraphDebugActor_ServerPrintAllActorInfo = FName(TEXT("ServerPrintAllActorInfo"));
	void AReplicationGraphDebugActor::ServerPrintAllActorInfo(const FString& Str)
	{
		ReplicationGraphDebugActor_eventServerPrintAllActorInfo_Parms Parms;
		Parms.Str=Str;
		ProcessEvent(FindFunctionChecked(NAME_AReplicationGraphDebugActor_ServerPrintAllActorInfo),&Parms);
	}
	static FName NAME_AReplicationGraphDebugActor_ServerPrintCullDistances = FName(TEXT("ServerPrintCullDistances"));
	void AReplicationGraphDebugActor::ServerPrintCullDistances()
	{
		ProcessEvent(FindFunctionChecked(NAME_AReplicationGraphDebugActor_ServerPrintCullDistances),NULL);
	}
	static FName NAME_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint = FName(TEXT("ServerSetConditionalActorBreakpoint"));
	void AReplicationGraphDebugActor::ServerSetConditionalActorBreakpoint(AActor* Actor)
	{
		ReplicationGraphDebugActor_eventServerSetConditionalActorBreakpoint_Parms Parms;
		Parms.Actor=Actor;
		ProcessEvent(FindFunctionChecked(NAME_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint),&Parms);
	}
	static FName NAME_AReplicationGraphDebugActor_ServerSetCullDistanceForClass = FName(TEXT("ServerSetCullDistanceForClass"));
	void AReplicationGraphDebugActor::ServerSetCullDistanceForClass(UClass* Class, float CullDistance)
	{
		ReplicationGraphDebugActor_eventServerSetCullDistanceForClass_Parms Parms;
		Parms.Class=Class;
		Parms.CullDistance=CullDistance;
		ProcessEvent(FindFunctionChecked(NAME_AReplicationGraphDebugActor_ServerSetCullDistanceForClass),&Parms);
	}
	static FName NAME_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass = FName(TEXT("ServerSetPeriodFrameForClass"));
	void AReplicationGraphDebugActor::ServerSetPeriodFrameForClass(UClass* Class, int32 PeriodFrame)
	{
		ReplicationGraphDebugActor_eventServerSetPeriodFrameForClass_Parms Parms;
		Parms.Class=Class;
		Parms.PeriodFrame=PeriodFrame;
		ProcessEvent(FindFunctionChecked(NAME_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass),&Parms);
	}
	static FName NAME_AReplicationGraphDebugActor_ServerStartDebugging = FName(TEXT("ServerStartDebugging"));
	void AReplicationGraphDebugActor::ServerStartDebugging()
	{
		ProcessEvent(FindFunctionChecked(NAME_AReplicationGraphDebugActor_ServerStartDebugging),NULL);
	}
	static FName NAME_AReplicationGraphDebugActor_ServerStopDebugging = FName(TEXT("ServerStopDebugging"));
	void AReplicationGraphDebugActor::ServerStopDebugging()
	{
		ProcessEvent(FindFunctionChecked(NAME_AReplicationGraphDebugActor_ServerStopDebugging),NULL);
	}
	void AReplicationGraphDebugActor::StaticRegisterNativesAReplicationGraphDebugActor()
	{
		UClass* Class = AReplicationGraphDebugActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ClientCellInfo", &AReplicationGraphDebugActor::execClientCellInfo },
			{ "ServerCellInfo", &AReplicationGraphDebugActor::execServerCellInfo },
			{ "ServerPrintAllActorInfo", &AReplicationGraphDebugActor::execServerPrintAllActorInfo },
			{ "ServerPrintCullDistances", &AReplicationGraphDebugActor::execServerPrintCullDistances },
			{ "ServerSetConditionalActorBreakpoint", &AReplicationGraphDebugActor::execServerSetConditionalActorBreakpoint },
			{ "ServerSetCullDistanceForClass", &AReplicationGraphDebugActor::execServerSetCullDistanceForClass },
			{ "ServerSetPeriodFrameForClass", &AReplicationGraphDebugActor::execServerSetPeriodFrameForClass },
			{ "ServerStartDebugging", &AReplicationGraphDebugActor::execServerStartDebugging },
			{ "ServerStopDebugging", &AReplicationGraphDebugActor::execServerStopDebugging },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics
	{
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellLocation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellExtent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_CellLocation = { "CellLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReplicationGraphDebugActor_eventClientCellInfo_Parms, CellLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_CellExtent = { "CellExtent", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReplicationGraphDebugActor_eventClientCellInfo_Parms, CellExtent), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_Actors_Inner = { "Actors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_Actors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_Actors = { "Actors", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReplicationGraphDebugActor_eventClientCellInfo_Parms, Actors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_Actors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_Actors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_CellLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_CellExtent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_Actors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::NewProp_Actors,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AReplicationGraphDebugActor, nullptr, "ClientCellInfo", nullptr, nullptr, sizeof(ReplicationGraphDebugActor_eventClientCellInfo_Parms), Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x01820CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AReplicationGraphDebugActor_ServerCellInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ServerCellInfo_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerCellInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AReplicationGraphDebugActor, nullptr, "ServerCellInfo", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerCellInfo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerCellInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AReplicationGraphDebugActor_ServerCellInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AReplicationGraphDebugActor_ServerCellInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Str_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Str;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::NewProp_Str_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::NewProp_Str = { "Str", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReplicationGraphDebugActor_eventServerPrintAllActorInfo_Parms, Str), METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::NewProp_Str_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::NewProp_Str_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::NewProp_Str,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AReplicationGraphDebugActor, nullptr, "ServerPrintAllActorInfo", nullptr, nullptr, sizeof(ReplicationGraphDebugActor_eventServerPrintAllActorInfo_Parms), Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintCullDistances_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintCullDistances_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintCullDistances_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AReplicationGraphDebugActor, nullptr, "ServerPrintCullDistances", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintCullDistances_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintCullDistances_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintCullDistances()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintCullDistances_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReplicationGraphDebugActor_eventServerSetConditionalActorBreakpoint_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics::NewProp_Actor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AReplicationGraphDebugActor, nullptr, "ServerSetConditionalActorBreakpoint", nullptr, nullptr, sizeof(ReplicationGraphDebugActor_eventServerSetConditionalActorBreakpoint_Parms), Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics
	{
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Class;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CullDistance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReplicationGraphDebugActor_eventServerSetCullDistanceForClass_Parms, Class), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::NewProp_CullDistance = { "CullDistance", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReplicationGraphDebugActor_eventServerSetCullDistanceForClass_Parms, CullDistance), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::NewProp_CullDistance,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AReplicationGraphDebugActor, nullptr, "ServerSetCullDistanceForClass", nullptr, nullptr, sizeof(ReplicationGraphDebugActor_eventServerSetCullDistanceForClass_Parms), Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics
	{
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Class;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PeriodFrame;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReplicationGraphDebugActor_eventServerSetPeriodFrameForClass_Parms, Class), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::NewProp_PeriodFrame = { "PeriodFrame", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ReplicationGraphDebugActor_eventServerSetPeriodFrameForClass_Parms, PeriodFrame), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::NewProp_PeriodFrame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AReplicationGraphDebugActor, nullptr, "ServerSetPeriodFrameForClass", nullptr, nullptr, sizeof(ReplicationGraphDebugActor_eventServerSetPeriodFrameForClass_Parms), Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStartDebugging_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStartDebugging_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStartDebugging_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AReplicationGraphDebugActor, nullptr, "ServerStartDebugging", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStartDebugging_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStartDebugging_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStartDebugging()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStartDebugging_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStopDebugging_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStopDebugging_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStopDebugging_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AReplicationGraphDebugActor, nullptr, "ServerStopDebugging", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStopDebugging_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStopDebugging_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStopDebugging()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStopDebugging_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AReplicationGraphDebugActor_NoRegister()
	{
		return AReplicationGraphDebugActor::StaticClass();
	}
	struct Z_Construct_UClass_AReplicationGraphDebugActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplicationGraph_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReplicationGraph;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConnectionManager;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AReplicationGraphDebugActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplicationGraph,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AReplicationGraphDebugActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AReplicationGraphDebugActor_ClientCellInfo, "ClientCellInfo" }, // 3844701899
		{ &Z_Construct_UFunction_AReplicationGraphDebugActor_ServerCellInfo, "ServerCellInfo" }, // 4246970039
		{ &Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintAllActorInfo, "ServerPrintAllActorInfo" }, // 2088360738
		{ &Z_Construct_UFunction_AReplicationGraphDebugActor_ServerPrintCullDistances, "ServerPrintCullDistances" }, // 3617659561
		{ &Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetConditionalActorBreakpoint, "ServerSetConditionalActorBreakpoint" }, // 4037101265
		{ &Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetCullDistanceForClass, "ServerSetCullDistanceForClass" }, // 3884526134
		{ &Z_Construct_UFunction_AReplicationGraphDebugActor_ServerSetPeriodFrameForClass, "ServerSetPeriodFrameForClass" }, // 1415086495
		{ &Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStartDebugging, "ServerStartDebugging" }, // 3110107014
		{ &Z_Construct_UFunction_AReplicationGraphDebugActor_ServerStopDebugging, "ServerStopDebugging" }, // 746972601
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AReplicationGraphDebugActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Specialized actor for replicating debug information about replication to specific connections. This actor is never spawned in shipping builds and never counts towards bandwidth limits */" },
		{ "IncludePath", "ReplicationGraph.h" },
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
		{ "ToolTip", "Specialized actor for replicating debug information about replication to specific connections. This actor is never spawned in shipping builds and never counts towards bandwidth limits" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AReplicationGraphDebugActor_Statics::NewProp_ReplicationGraph_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AReplicationGraphDebugActor_Statics::NewProp_ReplicationGraph = { "ReplicationGraph", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AReplicationGraphDebugActor, ReplicationGraph), Z_Construct_UClass_UReplicationGraph_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AReplicationGraphDebugActor_Statics::NewProp_ReplicationGraph_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AReplicationGraphDebugActor_Statics::NewProp_ReplicationGraph_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AReplicationGraphDebugActor_Statics::NewProp_ConnectionManager_MetaData[] = {
		{ "ModuleRelativePath", "Public/ReplicationGraph.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AReplicationGraphDebugActor_Statics::NewProp_ConnectionManager = { "ConnectionManager", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AReplicationGraphDebugActor, ConnectionManager), Z_Construct_UClass_UNetReplicationGraphConnection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AReplicationGraphDebugActor_Statics::NewProp_ConnectionManager_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AReplicationGraphDebugActor_Statics::NewProp_ConnectionManager_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AReplicationGraphDebugActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AReplicationGraphDebugActor_Statics::NewProp_ReplicationGraph,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AReplicationGraphDebugActor_Statics::NewProp_ConnectionManager,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AReplicationGraphDebugActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AReplicationGraphDebugActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AReplicationGraphDebugActor_Statics::ClassParams = {
		&AReplicationGraphDebugActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AReplicationGraphDebugActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AReplicationGraphDebugActor_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AReplicationGraphDebugActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AReplicationGraphDebugActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AReplicationGraphDebugActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AReplicationGraphDebugActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AReplicationGraphDebugActor, 3360086648);
	template<> REPLICATIONGRAPH_API UClass* StaticClass<AReplicationGraphDebugActor>()
	{
		return AReplicationGraphDebugActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AReplicationGraphDebugActor(Z_Construct_UClass_AReplicationGraphDebugActor, &AReplicationGraphDebugActor::StaticClass, TEXT("/Script/ReplicationGraph"), TEXT("AReplicationGraphDebugActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AReplicationGraphDebugActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
