// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
class AActor;
class UObject;
#ifdef REPLICATIONGRAPH_ReplicationGraph_generated_h
#error "ReplicationGraph.generated.h already included, missing '#pragma once' in ReplicationGraph.h"
#endif
#define REPLICATIONGRAPH_ReplicationGraph_generated_h

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1135_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLastLocationGatherInfo_Statics; \
	REPLICATIONGRAPH_API static class UScriptStruct* StaticStruct();


template<> REPLICATIONGRAPH_API UScriptStruct* StaticStruct<struct FLastLocationGatherInfo>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_819_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTearOffActorInfo_Statics; \
	REPLICATIONGRAPH_API static class UScriptStruct* StaticStruct();


template<> REPLICATIONGRAPH_API UScriptStruct* StaticStruct<struct FTearOffActorInfo>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_771_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAlwaysRelevantActorInfo_Statics; \
	REPLICATIONGRAPH_API static class UScriptStruct* StaticStruct();


template<> REPLICATIONGRAPH_API UScriptStruct* StaticStruct<struct FAlwaysRelevantActorInfo>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode, UObject, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode, UObject, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode(UReplicationGraphNode&&); \
	NO_API UReplicationGraphNode(const UReplicationGraphNode&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode(UReplicationGraphNode&&); \
	NO_API UReplicationGraphNode(const UReplicationGraphNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AllChildNodes() { return STRUCT_OFFSET(UReplicationGraphNode, AllChildNodes); }


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_72_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_75_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_ActorList(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_ActorList_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_ActorList, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_ActorList)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_ActorList(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_ActorList_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_ActorList, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_ActorList)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_ActorList(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode_ActorList) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_ActorList); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_ActorList); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_ActorList(UReplicationGraphNode_ActorList&&); \
	NO_API UReplicationGraphNode_ActorList(const UReplicationGraphNode_ActorList&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_ActorList() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_ActorList(UReplicationGraphNode_ActorList&&); \
	NO_API UReplicationGraphNode_ActorList(const UReplicationGraphNode_ActorList&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_ActorList); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_ActorList); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode_ActorList)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_197_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_200_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode_ActorList>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_ActorListFrequencyBuckets(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_ActorListFrequencyBuckets, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_ActorListFrequencyBuckets)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_ActorListFrequencyBuckets(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_ActorListFrequencyBuckets_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_ActorListFrequencyBuckets, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_ActorListFrequencyBuckets)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_ActorListFrequencyBuckets(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode_ActorListFrequencyBuckets) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_ActorListFrequencyBuckets); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_ActorListFrequencyBuckets); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_ActorListFrequencyBuckets(UReplicationGraphNode_ActorListFrequencyBuckets&&); \
	NO_API UReplicationGraphNode_ActorListFrequencyBuckets(const UReplicationGraphNode_ActorListFrequencyBuckets&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_ActorListFrequencyBuckets(UReplicationGraphNode_ActorListFrequencyBuckets&&); \
	NO_API UReplicationGraphNode_ActorListFrequencyBuckets(const UReplicationGraphNode_ActorListFrequencyBuckets&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_ActorListFrequencyBuckets); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_ActorListFrequencyBuckets); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode_ActorListFrequencyBuckets)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_243_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_246_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode_ActorListFrequencyBuckets>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_DynamicSpatialFrequency(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_DynamicSpatialFrequency, UReplicationGraphNode_ActorList, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_DynamicSpatialFrequency)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_DynamicSpatialFrequency(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_DynamicSpatialFrequency_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_DynamicSpatialFrequency, UReplicationGraphNode_ActorList, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_DynamicSpatialFrequency)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_DynamicSpatialFrequency(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode_DynamicSpatialFrequency) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_DynamicSpatialFrequency); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_DynamicSpatialFrequency); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_DynamicSpatialFrequency(UReplicationGraphNode_DynamicSpatialFrequency&&); \
	NO_API UReplicationGraphNode_DynamicSpatialFrequency(const UReplicationGraphNode_DynamicSpatialFrequency&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_DynamicSpatialFrequency(UReplicationGraphNode_DynamicSpatialFrequency&&); \
	NO_API UReplicationGraphNode_DynamicSpatialFrequency(const UReplicationGraphNode_DynamicSpatialFrequency&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_DynamicSpatialFrequency); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_DynamicSpatialFrequency); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode_DynamicSpatialFrequency)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_315_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_318_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode_DynamicSpatialFrequency>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_ConnectionDormancyNode(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_ConnectionDormancyNode, UReplicationGraphNode_ActorList, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_ConnectionDormancyNode)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_ConnectionDormancyNode(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_ConnectionDormancyNode_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_ConnectionDormancyNode, UReplicationGraphNode_ActorList, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_ConnectionDormancyNode)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_ConnectionDormancyNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode_ConnectionDormancyNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_ConnectionDormancyNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_ConnectionDormancyNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_ConnectionDormancyNode(UReplicationGraphNode_ConnectionDormancyNode&&); \
	NO_API UReplicationGraphNode_ConnectionDormancyNode(const UReplicationGraphNode_ConnectionDormancyNode&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_ConnectionDormancyNode() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_ConnectionDormancyNode(UReplicationGraphNode_ConnectionDormancyNode&&); \
	NO_API UReplicationGraphNode_ConnectionDormancyNode(const UReplicationGraphNode_ConnectionDormancyNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_ConnectionDormancyNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_ConnectionDormancyNode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode_ConnectionDormancyNode)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_420_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_423_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode_ConnectionDormancyNode>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_DormancyNode(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_DormancyNode_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_DormancyNode, UReplicationGraphNode_ActorList, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_DormancyNode)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_DormancyNode(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_DormancyNode_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_DormancyNode, UReplicationGraphNode_ActorList, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_DormancyNode)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_DormancyNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode_DormancyNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_DormancyNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_DormancyNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_DormancyNode(UReplicationGraphNode_DormancyNode&&); \
	NO_API UReplicationGraphNode_DormancyNode(const UReplicationGraphNode_DormancyNode&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_DormancyNode() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_DormancyNode(UReplicationGraphNode_DormancyNode&&); \
	NO_API UReplicationGraphNode_DormancyNode(const UReplicationGraphNode_DormancyNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_DormancyNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_DormancyNode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode_DormancyNode)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_475_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_478_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode_DormancyNode>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_GridCell(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_GridCell, UReplicationGraphNode_ActorList, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_GridCell)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_GridCell(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_GridCell_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_GridCell, UReplicationGraphNode_ActorList, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_GridCell)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_GridCell(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode_GridCell) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_GridCell); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_GridCell); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_GridCell(UReplicationGraphNode_GridCell&&); \
	NO_API UReplicationGraphNode_GridCell(const UReplicationGraphNode_GridCell&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_GridCell() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_GridCell(UReplicationGraphNode_GridCell&&); \
	NO_API UReplicationGraphNode_GridCell(const UReplicationGraphNode_GridCell&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_GridCell); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_GridCell); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode_GridCell)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DynamicNode() { return STRUCT_OFFSET(UReplicationGraphNode_GridCell, DynamicNode); } \
	FORCEINLINE static uint32 __PPO__DormancyNode() { return STRUCT_OFFSET(UReplicationGraphNode_GridCell, DormancyNode); }


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_523_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_526_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode_GridCell>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_GridSpatialization2D(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_GridSpatialization2D, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_GridSpatialization2D)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_GridSpatialization2D(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_GridSpatialization2D_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_GridSpatialization2D, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_GridSpatialization2D)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_GridSpatialization2D(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode_GridSpatialization2D) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_GridSpatialization2D); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_GridSpatialization2D); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_GridSpatialization2D(UReplicationGraphNode_GridSpatialization2D&&); \
	NO_API UReplicationGraphNode_GridSpatialization2D(const UReplicationGraphNode_GridSpatialization2D&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_GridSpatialization2D(UReplicationGraphNode_GridSpatialization2D&&); \
	NO_API UReplicationGraphNode_GridSpatialization2D(const UReplicationGraphNode_GridSpatialization2D&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_GridSpatialization2D); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_GridSpatialization2D); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode_GridSpatialization2D)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_564_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_567_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode_GridSpatialization2D>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_AlwaysRelevant(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_AlwaysRelevant, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_AlwaysRelevant)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_AlwaysRelevant(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_AlwaysRelevant, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_AlwaysRelevant)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_AlwaysRelevant(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode_AlwaysRelevant) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_AlwaysRelevant); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_AlwaysRelevant); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_AlwaysRelevant(UReplicationGraphNode_AlwaysRelevant&&); \
	NO_API UReplicationGraphNode_AlwaysRelevant(const UReplicationGraphNode_AlwaysRelevant&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_AlwaysRelevant(UReplicationGraphNode_AlwaysRelevant&&); \
	NO_API UReplicationGraphNode_AlwaysRelevant(const UReplicationGraphNode_AlwaysRelevant&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_AlwaysRelevant); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_AlwaysRelevant); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode_AlwaysRelevant)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ChildNode() { return STRUCT_OFFSET(UReplicationGraphNode_AlwaysRelevant, ChildNode); }


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_740_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_743_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode_AlwaysRelevant>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_AlwaysRelevant_ForConnection(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_AlwaysRelevant_ForConnection, UReplicationGraphNode_ActorList, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_AlwaysRelevant_ForConnection)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_AlwaysRelevant_ForConnection(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_AlwaysRelevant_ForConnection_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_AlwaysRelevant_ForConnection, UReplicationGraphNode_ActorList, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_AlwaysRelevant_ForConnection)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_AlwaysRelevant_ForConnection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode_AlwaysRelevant_ForConnection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_AlwaysRelevant_ForConnection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_AlwaysRelevant_ForConnection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_AlwaysRelevant_ForConnection(UReplicationGraphNode_AlwaysRelevant_ForConnection&&); \
	NO_API UReplicationGraphNode_AlwaysRelevant_ForConnection(const UReplicationGraphNode_AlwaysRelevant_ForConnection&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_AlwaysRelevant_ForConnection() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_AlwaysRelevant_ForConnection(UReplicationGraphNode_AlwaysRelevant_ForConnection&&); \
	NO_API UReplicationGraphNode_AlwaysRelevant_ForConnection(const UReplicationGraphNode_AlwaysRelevant_ForConnection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_AlwaysRelevant_ForConnection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_AlwaysRelevant_ForConnection); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode_AlwaysRelevant_ForConnection)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_789_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_792_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode_AlwaysRelevant_ForConnection>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_TearOff_ForConnection(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_TearOff_ForConnection, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_TearOff_ForConnection)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraphNode_TearOff_ForConnection(); \
	friend struct Z_Construct_UClass_UReplicationGraphNode_TearOff_ForConnection_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraphNode_TearOff_ForConnection, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraphNode_TearOff_ForConnection)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_TearOff_ForConnection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraphNode_TearOff_ForConnection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_TearOff_ForConnection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_TearOff_ForConnection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_TearOff_ForConnection(UReplicationGraphNode_TearOff_ForConnection&&); \
	NO_API UReplicationGraphNode_TearOff_ForConnection(const UReplicationGraphNode_TearOff_ForConnection&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraphNode_TearOff_ForConnection() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraphNode_TearOff_ForConnection(UReplicationGraphNode_TearOff_ForConnection&&); \
	NO_API UReplicationGraphNode_TearOff_ForConnection(const UReplicationGraphNode_TearOff_ForConnection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraphNode_TearOff_ForConnection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraphNode_TearOff_ForConnection); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraphNode_TearOff_ForConnection)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_832_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_835_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraphNode_TearOff_ForConnection>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UReplicationGraph, NO_API)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReplicationGraph(); \
	friend struct Z_Construct_UClass_UReplicationGraph_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraph, UReplicationDriver, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraph) \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_INCLASS \
private: \
	static void StaticRegisterNativesUReplicationGraph(); \
	friend struct Z_Construct_UClass_UReplicationGraph_Statics; \
public: \
	DECLARE_CLASS(UReplicationGraph, UReplicationDriver, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UReplicationGraph) \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReplicationGraph(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReplicationGraph) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraph); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraph(UReplicationGraph&&); \
	NO_API UReplicationGraph(const UReplicationGraph&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReplicationGraph(UReplicationGraph&&); \
	NO_API UReplicationGraph(const UReplicationGraph&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReplicationGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReplicationGraph); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReplicationGraph)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GlobalGraphNodes() { return STRUCT_OFFSET(UReplicationGraph, GlobalGraphNodes); } \
	FORCEINLINE static uint32 __PPO__PrepareForReplicationNodes() { return STRUCT_OFFSET(UReplicationGraph, PrepareForReplicationNodes); }


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_861_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_864_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UReplicationGraph>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_RPC_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UNetReplicationGraphConnection, NO_API)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNetReplicationGraphConnection(); \
	friend struct Z_Construct_UClass_UNetReplicationGraphConnection_Statics; \
public: \
	DECLARE_CLASS(UNetReplicationGraphConnection, UReplicationConnectionDriver, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UNetReplicationGraphConnection) \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_INCLASS \
private: \
	static void StaticRegisterNativesUNetReplicationGraphConnection(); \
	friend struct Z_Construct_UClass_UNetReplicationGraphConnection_Statics; \
public: \
	DECLARE_CLASS(UNetReplicationGraphConnection, UReplicationConnectionDriver, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(UNetReplicationGraphConnection) \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetReplicationGraphConnection(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetReplicationGraphConnection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetReplicationGraphConnection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetReplicationGraphConnection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetReplicationGraphConnection(UNetReplicationGraphConnection&&); \
	NO_API UNetReplicationGraphConnection(const UNetReplicationGraphConnection&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetReplicationGraphConnection(UNetReplicationGraphConnection&&); \
	NO_API UNetReplicationGraphConnection(const UNetReplicationGraphConnection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetReplicationGraphConnection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetReplicationGraphConnection); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNetReplicationGraphConnection)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ConnectionGraphNodes() { return STRUCT_OFFSET(UNetReplicationGraphConnection, ConnectionGraphNodes); } \
	FORCEINLINE static uint32 __PPO__TearOffNode() { return STRUCT_OFFSET(UNetReplicationGraphConnection, TearOffNode); }


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1156_PROLOG
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1159_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class UNetReplicationGraphConnection>();

#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_SPARSE_DATA
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_RPC_WRAPPERS \
	virtual void ClientCellInfo_Implementation(FVector CellLocation, FVector CellExtent, TArray<AActor*> const& Actors); \
	virtual void ServerPrintCullDistances_Implementation(); \
	virtual void ServerSetConditionalActorBreakpoint_Implementation(AActor* Actor); \
	virtual void ServerSetPeriodFrameForClass_Implementation(UClass* Class, int32 PeriodFrame); \
	virtual void ServerSetCullDistanceForClass_Implementation(UClass* Class, float CullDistance); \
	virtual void ServerPrintAllActorInfo_Implementation(const FString& Str); \
	virtual void ServerCellInfo_Implementation(); \
	virtual void ServerStopDebugging_Implementation(); \
	virtual void ServerStartDebugging_Implementation(); \
 \
	DECLARE_FUNCTION(execClientCellInfo); \
	DECLARE_FUNCTION(execServerPrintCullDistances); \
	DECLARE_FUNCTION(execServerSetConditionalActorBreakpoint); \
	DECLARE_FUNCTION(execServerSetPeriodFrameForClass); \
	DECLARE_FUNCTION(execServerSetCullDistanceForClass); \
	DECLARE_FUNCTION(execServerPrintAllActorInfo); \
	DECLARE_FUNCTION(execServerCellInfo); \
	DECLARE_FUNCTION(execServerStopDebugging); \
	DECLARE_FUNCTION(execServerStartDebugging);


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void ClientCellInfo_Implementation(FVector CellLocation, FVector CellExtent, TArray<AActor*> const& Actors); \
	virtual void ServerPrintCullDistances_Implementation(); \
	virtual void ServerSetConditionalActorBreakpoint_Implementation(AActor* Actor); \
	virtual void ServerSetPeriodFrameForClass_Implementation(UClass* Class, int32 PeriodFrame); \
	virtual void ServerSetCullDistanceForClass_Implementation(UClass* Class, float CullDistance); \
	virtual void ServerPrintAllActorInfo_Implementation(const FString& Str); \
	virtual void ServerCellInfo_Implementation(); \
	virtual void ServerStopDebugging_Implementation(); \
	virtual void ServerStartDebugging_Implementation(); \
 \
	DECLARE_FUNCTION(execClientCellInfo); \
	DECLARE_FUNCTION(execServerPrintCullDistances); \
	DECLARE_FUNCTION(execServerSetConditionalActorBreakpoint); \
	DECLARE_FUNCTION(execServerSetPeriodFrameForClass); \
	DECLARE_FUNCTION(execServerSetCullDistanceForClass); \
	DECLARE_FUNCTION(execServerPrintAllActorInfo); \
	DECLARE_FUNCTION(execServerCellInfo); \
	DECLARE_FUNCTION(execServerStopDebugging); \
	DECLARE_FUNCTION(execServerStartDebugging);


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_EVENT_PARMS \
	struct ReplicationGraphDebugActor_eventClientCellInfo_Parms \
	{ \
		FVector CellLocation; \
		FVector CellExtent; \
		TArray<AActor*> Actors; \
	}; \
	struct ReplicationGraphDebugActor_eventServerPrintAllActorInfo_Parms \
	{ \
		FString Str; \
	}; \
	struct ReplicationGraphDebugActor_eventServerSetConditionalActorBreakpoint_Parms \
	{ \
		AActor* Actor; \
	}; \
	struct ReplicationGraphDebugActor_eventServerSetCullDistanceForClass_Parms \
	{ \
		UClass* Class; \
		float CullDistance; \
	}; \
	struct ReplicationGraphDebugActor_eventServerSetPeriodFrameForClass_Parms \
	{ \
		UClass* Class; \
		int32 PeriodFrame; \
	};


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_CALLBACK_WRAPPERS
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAReplicationGraphDebugActor(); \
	friend struct Z_Construct_UClass_AReplicationGraphDebugActor_Statics; \
public: \
	DECLARE_CLASS(AReplicationGraphDebugActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(AReplicationGraphDebugActor)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_INCLASS \
private: \
	static void StaticRegisterNativesAReplicationGraphDebugActor(); \
	friend struct Z_Construct_UClass_AReplicationGraphDebugActor_Statics; \
public: \
	DECLARE_CLASS(AReplicationGraphDebugActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ReplicationGraph"), NO_API) \
	DECLARE_SERIALIZER(AReplicationGraphDebugActor)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AReplicationGraphDebugActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AReplicationGraphDebugActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AReplicationGraphDebugActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AReplicationGraphDebugActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AReplicationGraphDebugActor(AReplicationGraphDebugActor&&); \
	NO_API AReplicationGraphDebugActor(const AReplicationGraphDebugActor&); \
public:


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AReplicationGraphDebugActor(AReplicationGraphDebugActor&&); \
	NO_API AReplicationGraphDebugActor(const AReplicationGraphDebugActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AReplicationGraphDebugActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AReplicationGraphDebugActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AReplicationGraphDebugActor)


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1324_PROLOG \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_EVENT_PARMS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_RPC_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_INCLASS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_SPARSE_DATA \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h_1327_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLICATIONGRAPH_API UClass* StaticClass<class AReplicationGraphDebugActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_ReplicationGraph_Source_Public_ReplicationGraph_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
