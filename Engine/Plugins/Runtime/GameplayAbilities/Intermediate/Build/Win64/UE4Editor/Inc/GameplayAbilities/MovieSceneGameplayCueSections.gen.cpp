// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameplayAbilities/Public/Sequencer/MovieSceneGameplayCueSections.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneGameplayCueSections() {}
// Cross Module References
	GAMEPLAYABILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel();
	UPackage* Z_Construct_UPackage__Script_GameplayAbilities();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneChannel();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameNumber();
	GAMEPLAYABILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey();
	GAMEPLAYABILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayCueTag();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneObjectBindingID();
	PHYSICSCORE_API UClass* Z_Construct_UClass_UPhysicalMaterial_NoRegister();
	GAMEPLAYABILITIES_API UClass* Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_NoRegister();
	GAMEPLAYABILITIES_API UClass* Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneHookSection();
	GAMEPLAYABILITIES_API UClass* Z_Construct_UClass_UMovieSceneGameplayCueSection_NoRegister();
	GAMEPLAYABILITIES_API UClass* Z_Construct_UClass_UMovieSceneGameplayCueSection();
// End Cross Module References

static_assert(std::is_polymorphic<FMovieSceneGameplayCueChannel>() == std::is_polymorphic<FMovieSceneChannel>(), "USTRUCT FMovieSceneGameplayCueChannel cannot be polymorphic unless super FMovieSceneChannel is polymorphic");

class UScriptStruct* FMovieSceneGameplayCueChannel::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern GAMEPLAYABILITIES_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel, Z_Construct_UPackage__Script_GameplayAbilities(), TEXT("MovieSceneGameplayCueChannel"), sizeof(FMovieSceneGameplayCueChannel), Get_Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Hash());
	}
	return Singleton;
}
template<> GAMEPLAYABILITIES_API UScriptStruct* StaticStruct<FMovieSceneGameplayCueChannel>()
{
	return FMovieSceneGameplayCueChannel::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneGameplayCueChannel(FMovieSceneGameplayCueChannel::StaticStruct, TEXT("/Script/GameplayAbilities"), TEXT("MovieSceneGameplayCueChannel"), false, nullptr, nullptr);
static struct FScriptStruct_GameplayAbilities_StaticRegisterNativesFMovieSceneGameplayCueChannel
{
	FScriptStruct_GameplayAbilities_StaticRegisterNativesFMovieSceneGameplayCueChannel()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneGameplayCueChannel>(FName(TEXT("MovieSceneGameplayCueChannel")));
	}
} ScriptStruct_GameplayAbilities_StaticRegisterNativesFMovieSceneGameplayCueChannel;
	struct Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyTimes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyTimes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_KeyTimes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyValues_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyValues_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_KeyValues;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneGameplayCueChannel>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyTimes_Inner = { "KeyTimes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFrameNumber, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyTimes_MetaData[] = {
		{ "Comment", "/** Array of times for each key */" },
		{ "KeyTimes", "" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "Array of times for each key" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyTimes = { "KeyTimes", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueChannel, KeyTimes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyTimes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyTimes_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyValues_Inner = { "KeyValues", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyValues_MetaData[] = {
		{ "Comment", "/** Array of values that correspond to each key time */" },
		{ "KeyValues", "" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "Array of values that correspond to each key time" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyValues = { "KeyValues", nullptr, (EPropertyFlags)0x0040010000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueChannel, KeyValues), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyValues_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyValues_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyTimes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyTimes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyValues_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::NewProp_KeyValues,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_GameplayAbilities,
		Z_Construct_UScriptStruct_FMovieSceneChannel,
		&NewStructOps,
		"MovieSceneGameplayCueChannel",
		sizeof(FMovieSceneGameplayCueChannel),
		alignof(FMovieSceneGameplayCueChannel),
		Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_GameplayAbilities();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneGameplayCueChannel"), sizeof(FMovieSceneGameplayCueChannel), Get_Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel_Hash() { return 358129525U; }
class UScriptStruct* FMovieSceneGameplayCueKey::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern GAMEPLAYABILITIES_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey, Z_Construct_UPackage__Script_GameplayAbilities(), TEXT("MovieSceneGameplayCueKey"), sizeof(FMovieSceneGameplayCueKey), Get_Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Hash());
	}
	return Singleton;
}
template<> GAMEPLAYABILITIES_API UScriptStruct* StaticStruct<FMovieSceneGameplayCueKey>()
{
	return FMovieSceneGameplayCueKey::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneGameplayCueKey(FMovieSceneGameplayCueKey::StaticStruct, TEXT("/Script/GameplayAbilities"), TEXT("MovieSceneGameplayCueKey"), false, nullptr, nullptr);
static struct FScriptStruct_GameplayAbilities_StaticRegisterNativesFMovieSceneGameplayCueKey
{
	FScriptStruct_GameplayAbilities_StaticRegisterNativesFMovieSceneGameplayCueKey()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneGameplayCueKey>(FName(TEXT("MovieSceneGameplayCueKey")));
	}
} ScriptStruct_GameplayAbilities_StaticRegisterNativesFMovieSceneGameplayCueKey;
	struct Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Location_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Normal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Normal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttachSocketName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_AttachSocketName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalizedMagnitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NormalizedMagnitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Instigator_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Instigator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EffectCauser_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EffectCauser;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhysicalMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PhysicalMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameplayEffectLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_GameplayEffectLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AbilityLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AbilityLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAttachToBinding_MetaData[];
#endif
		static void NewProp_bAttachToBinding_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAttachToBinding;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneGameplayCueKey>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Cue_MetaData[] = {
		{ "Category", "Gameplay Cue" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Cue = { "Cue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueKey, Cue), Z_Construct_UScriptStruct_FGameplayCueTag, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Cue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Cue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Location_MetaData[] = {
		{ "Category", "Position" },
		{ "Comment", "/** Location cue took place at - relative to the attached component if applicable */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "Location cue took place at - relative to the attached component if applicable" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueKey, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Location_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Location_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Normal_MetaData[] = {
		{ "Category", "Position" },
		{ "Comment", "/** Normal of impact that caused cue */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "Normal of impact that caused cue" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Normal = { "Normal", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueKey, Normal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Normal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Normal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_AttachSocketName_MetaData[] = {
		{ "Category", "Position" },
		{ "Comment", "/** When attached to a skeletal mesh component, specifies a socket to trigger the cue at */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "When attached to a skeletal mesh component, specifies a socket to trigger the cue at" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_AttachSocketName = { "AttachSocketName", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueKey, AttachSocketName), METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_AttachSocketName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_AttachSocketName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_NormalizedMagnitude_MetaData[] = {
		{ "Category", "Gameplay Cue" },
		{ "ClampMax", "1.000000" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/** Magnitude of source gameplay effect, normalzed from 0-1. Use this for \"how strong is the gameplay effect\" (0=min, 1=,max) */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "Magnitude of source gameplay effect, normalzed from 0-1. Use this for \"how strong is the gameplay effect\" (0=min, 1=,max)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_NormalizedMagnitude = { "NormalizedMagnitude", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueKey, NormalizedMagnitude), METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_NormalizedMagnitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_NormalizedMagnitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Instigator_MetaData[] = {
		{ "Category", "Gameplay Cue" },
		{ "Comment", "/** Instigator actor, the actor that owns the ability system component. */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "Instigator actor, the actor that owns the ability system component." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Instigator = { "Instigator", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueKey, Instigator), Z_Construct_UScriptStruct_FMovieSceneObjectBindingID, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Instigator_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Instigator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_EffectCauser_MetaData[] = {
		{ "Category", "Gameplay Cue" },
		{ "Comment", "/** The physical actor that actually did the damage, can be a weapon or projectile */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "The physical actor that actually did the damage, can be a weapon or projectile" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_EffectCauser = { "EffectCauser", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueKey, EffectCauser), Z_Construct_UScriptStruct_FMovieSceneObjectBindingID, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_EffectCauser_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_EffectCauser_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_PhysicalMaterial_MetaData[] = {
		{ "Category", "Gameplay Cue" },
		{ "Comment", "/** PhysMat of the hit, if there was a hit. */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "NativeConst", "" },
		{ "ToolTip", "PhysMat of the hit, if there was a hit." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_PhysicalMaterial = { "PhysicalMaterial", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueKey, PhysicalMaterial), Z_Construct_UClass_UPhysicalMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_PhysicalMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_PhysicalMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_GameplayEffectLevel_MetaData[] = {
		{ "Category", "Gameplay Cue" },
		{ "Comment", "/** The level of that GameplayEffect */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "The level of that GameplayEffect" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_GameplayEffectLevel = { "GameplayEffectLevel", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueKey, GameplayEffectLevel), METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_GameplayEffectLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_GameplayEffectLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_AbilityLevel_MetaData[] = {
		{ "Category", "Gameplay Cue" },
		{ "Comment", "/** If originating from an ability, this will be the level of that ability */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "If originating from an ability, this will be the level of that ability" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_AbilityLevel = { "AbilityLevel", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGameplayCueKey, AbilityLevel), METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_AbilityLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_AbilityLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_bAttachToBinding_MetaData[] = {
		{ "Category", "Position" },
		{ "Comment", "/** Attach the gameplay cue to the track's bound object in sequencer */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ToolTip", "Attach the gameplay cue to the track's bound object in sequencer" },
	};
#endif
	void Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_bAttachToBinding_SetBit(void* Obj)
	{
		((FMovieSceneGameplayCueKey*)Obj)->bAttachToBinding = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_bAttachToBinding = { "bAttachToBinding", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMovieSceneGameplayCueKey), &Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_bAttachToBinding_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_bAttachToBinding_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_bAttachToBinding_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Cue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Normal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_AttachSocketName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_NormalizedMagnitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_Instigator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_EffectCauser,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_PhysicalMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_GameplayEffectLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_AbilityLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::NewProp_bAttachToBinding,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_GameplayAbilities,
		nullptr,
		&NewStructOps,
		"MovieSceneGameplayCueKey",
		sizeof(FMovieSceneGameplayCueKey),
		alignof(FMovieSceneGameplayCueKey),
		Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_GameplayAbilities();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneGameplayCueKey"), sizeof(FMovieSceneGameplayCueKey), Get_Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey_Hash() { return 4249794796U; }
	void UMovieSceneGameplayCueTriggerSection::StaticRegisterNativesUMovieSceneGameplayCueTriggerSection()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_NoRegister()
	{
		return UMovieSceneGameplayCueTriggerSection::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Channel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Channel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneHookSection,
		(UObject* (*)())Z_Construct_UPackage__Script_GameplayAbilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a movie scene section that triggers gameplay cues\n */" },
		{ "IncludePath", "Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Implements a movie scene section that triggers gameplay cues" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::NewProp_Channel_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0040010000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneGameplayCueTriggerSection, Channel), Z_Construct_UScriptStruct_FMovieSceneGameplayCueChannel, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::NewProp_Channel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::NewProp_Channel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::NewProp_Channel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneGameplayCueTriggerSection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::ClassParams = {
		&UMovieSceneGameplayCueTriggerSection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::PropPointers),
		0,
		0x002800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneGameplayCueTriggerSection, 2620762601);
	template<> GAMEPLAYABILITIES_API UClass* StaticClass<UMovieSceneGameplayCueTriggerSection>()
	{
		return UMovieSceneGameplayCueTriggerSection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneGameplayCueTriggerSection(Z_Construct_UClass_UMovieSceneGameplayCueTriggerSection, &UMovieSceneGameplayCueTriggerSection::StaticClass, TEXT("/Script/GameplayAbilities"), TEXT("UMovieSceneGameplayCueTriggerSection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneGameplayCueTriggerSection);
	void UMovieSceneGameplayCueSection::StaticRegisterNativesUMovieSceneGameplayCueSection()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneGameplayCueSection_NoRegister()
	{
		return UMovieSceneGameplayCueSection::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneHookSection,
		(UObject* (*)())Z_Construct_UPackage__Script_GameplayAbilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a movie scene section that triggers gameplay cues\n */" },
		{ "IncludePath", "Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Implements a movie scene section that triggers gameplay cues" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::NewProp_Cue_MetaData[] = {
		{ "Category", "Cue" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneGameplayCueSections.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::NewProp_Cue = { "Cue", nullptr, (EPropertyFlags)0x0040010000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneGameplayCueSection, Cue), Z_Construct_UScriptStruct_FMovieSceneGameplayCueKey, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::NewProp_Cue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::NewProp_Cue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::NewProp_Cue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneGameplayCueSection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::ClassParams = {
		&UMovieSceneGameplayCueSection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::PropPointers),
		0,
		0x002800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneGameplayCueSection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneGameplayCueSection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneGameplayCueSection, 2653003920);
	template<> GAMEPLAYABILITIES_API UClass* StaticClass<UMovieSceneGameplayCueSection>()
	{
		return UMovieSceneGameplayCueSection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneGameplayCueSection(Z_Construct_UClass_UMovieSceneGameplayCueSection, &UMovieSceneGameplayCueSection::StaticClass, TEXT("/Script/GameplayAbilities"), TEXT("UMovieSceneGameplayCueSection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneGameplayCueSection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
