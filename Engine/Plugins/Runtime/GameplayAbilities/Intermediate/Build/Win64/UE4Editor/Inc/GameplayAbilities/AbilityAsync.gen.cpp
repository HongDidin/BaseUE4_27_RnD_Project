// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameplayAbilities/Public/Abilities/Async/AbilityAsync.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityAsync() {}
// Cross Module References
	GAMEPLAYABILITIES_API UClass* Z_Construct_UClass_UAbilityAsync_NoRegister();
	GAMEPLAYABILITIES_API UClass* Z_Construct_UClass_UAbilityAsync();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintAsyncActionBase();
	UPackage* Z_Construct_UPackage__Script_GameplayAbilities();
// End Cross Module References
	DEFINE_FUNCTION(UAbilityAsync::execEndAction)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndAction();
		P_NATIVE_END;
	}
	void UAbilityAsync::StaticRegisterNativesUAbilityAsync()
	{
		UClass* Class = UAbilityAsync::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "EndAction", &UAbilityAsync::execEndAction },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAbilityAsync_EndAction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityAsync_EndAction_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Async" },
		{ "Comment", "/** Explicitly end the action, will disable any callbacks and allow action to be deleted */" },
		{ "ModuleRelativePath", "Public/Abilities/Async/AbilityAsync.h" },
		{ "ToolTip", "Explicitly end the action, will disable any callbacks and allow action to be deleted" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityAsync_EndAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityAsync, nullptr, "EndAction", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityAsync_EndAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityAsync_EndAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityAsync_EndAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAbilityAsync_EndAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAbilityAsync_NoRegister()
	{
		return UAbilityAsync::StaticClass();
	}
	struct Z_Construct_UClass_UAbilityAsync_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAbilityAsync_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintAsyncActionBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GameplayAbilities,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAbilityAsync_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAbilityAsync_EndAction, "EndAction" }, // 557054252
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityAsync_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\x09""AbilityAsync is a base class for ability-specific BlueprintAsyncActions.\n *  These are similar to ability tasks, but they can be executed from any blueprint like an actor and are not tied to a specific ability lifespan.\n */" },
		{ "ExposedAsyncProxy", "AsyncAction" },
		{ "IncludePath", "Abilities/Async/AbilityAsync.h" },
		{ "ModuleRelativePath", "Public/Abilities/Async/AbilityAsync.h" },
		{ "ToolTip", "AbilityAsync is a base class for ability-specific BlueprintAsyncActions.\nThese are similar to ability tasks, but they can be executed from any blueprint like an actor and are not tied to a specific ability lifespan." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAbilityAsync_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAbilityAsync>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAbilityAsync_Statics::ClassParams = {
		&UAbilityAsync::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UAbilityAsync_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityAsync_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAbilityAsync()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAbilityAsync_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAbilityAsync, 1655585334);
	template<> GAMEPLAYABILITIES_API UClass* StaticClass<UAbilityAsync>()
	{
		return UAbilityAsync::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAbilityAsync(Z_Construct_UClass_UAbilityAsync, &UAbilityAsync::StaticClass, TEXT("/Script/GameplayAbilities"), TEXT("UAbilityAsync"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAbilityAsync);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
