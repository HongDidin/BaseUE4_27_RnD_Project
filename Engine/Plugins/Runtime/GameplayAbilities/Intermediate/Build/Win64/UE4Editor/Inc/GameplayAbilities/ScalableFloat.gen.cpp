// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GameplayAbilities/Public/ScalableFloat.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeScalableFloat() {}
// Cross Module References
	GAMEPLAYABILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FScalableFloat();
	UPackage* Z_Construct_UPackage__Script_GameplayAbilities();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FCurveTableRowHandle();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryType();
// End Cross Module References
class UScriptStruct* FScalableFloat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern GAMEPLAYABILITIES_API uint32 Get_Z_Construct_UScriptStruct_FScalableFloat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FScalableFloat, Z_Construct_UPackage__Script_GameplayAbilities(), TEXT("ScalableFloat"), sizeof(FScalableFloat), Get_Z_Construct_UScriptStruct_FScalableFloat_Hash());
	}
	return Singleton;
}
template<> GAMEPLAYABILITIES_API UScriptStruct* StaticStruct<FScalableFloat>()
{
	return FScalableFloat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FScalableFloat(FScalableFloat::StaticStruct, TEXT("/Script/GameplayAbilities"), TEXT("ScalableFloat"), false, nullptr, nullptr);
static struct FScriptStruct_GameplayAbilities_StaticRegisterNativesFScalableFloat
{
	FScriptStruct_GameplayAbilities_StaticRegisterNativesFScalableFloat()
	{
		UScriptStruct::DeferCppStructOps<FScalableFloat>(FName(TEXT("ScalableFloat")));
	}
} ScriptStruct_GameplayAbilities_StaticRegisterNativesFScalableFloat;
	struct Z_Construct_UScriptStruct_FScalableFloat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curve_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Curve;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegistryType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RegistryType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FScalableFloat_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Generic numerical value in the form Value * Curve[Level] */" },
		{ "ModuleRelativePath", "Public/ScalableFloat.h" },
		{ "ToolTip", "Generic numerical value in the form Value * Curve[Level]" },
	};
#endif
	void* Z_Construct_UScriptStruct_FScalableFloat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FScalableFloat>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_Value_MetaData[] = {
		{ "Category", "ScalableFloat" },
		{ "Comment", "/** Raw value, is multiplied by curve */" },
		{ "ModuleRelativePath", "Public/ScalableFloat.h" },
		{ "ToolTip", "Raw value, is multiplied by curve" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FScalableFloat, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_Curve_MetaData[] = {
		{ "Category", "ScalableFloat" },
		{ "Comment", "/** Curve that is evaluated at a specific level. If found, it is multipled by Value */" },
		{ "ModuleRelativePath", "Public/ScalableFloat.h" },
		{ "ToolTip", "Curve that is evaluated at a specific level. If found, it is multipled by Value" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_Curve = { "Curve", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FScalableFloat, Curve), Z_Construct_UScriptStruct_FCurveTableRowHandle, METADATA_PARAMS(Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_Curve_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_Curve_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_RegistryType_MetaData[] = {
		{ "Category", "ScalableFloat" },
		{ "Comment", "/** Name of Data Registry containing curve to use. If set the RowName inside Curve is used as the item name */" },
		{ "ModuleRelativePath", "Public/ScalableFloat.h" },
		{ "ToolTip", "Name of Data Registry containing curve to use. If set the RowName inside Curve is used as the item name" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_RegistryType = { "RegistryType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FScalableFloat, RegistryType), Z_Construct_UScriptStruct_FDataRegistryType, METADATA_PARAMS(Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_RegistryType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_RegistryType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FScalableFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_Curve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FScalableFloat_Statics::NewProp_RegistryType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FScalableFloat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_GameplayAbilities,
		nullptr,
		&NewStructOps,
		"ScalableFloat",
		sizeof(FScalableFloat),
		alignof(FScalableFloat),
		Z_Construct_UScriptStruct_FScalableFloat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FScalableFloat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FScalableFloat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FScalableFloat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FScalableFloat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FScalableFloat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_GameplayAbilities();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ScalableFloat"), sizeof(FScalableFloat), Get_Z_Construct_UScriptStruct_FScalableFloat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FScalableFloat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FScalableFloat_Hash() { return 2748846618U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
