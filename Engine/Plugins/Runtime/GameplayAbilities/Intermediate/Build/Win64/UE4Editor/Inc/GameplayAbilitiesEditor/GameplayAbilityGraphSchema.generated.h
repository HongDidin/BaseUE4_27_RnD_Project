// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEPLAYABILITIESEDITOR_GameplayAbilityGraphSchema_generated_h
#error "GameplayAbilityGraphSchema.generated.h already included, missing '#pragma once' in GameplayAbilityGraphSchema.h"
#endif
#define GAMEPLAYABILITIESEDITOR_GameplayAbilityGraphSchema_generated_h

#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_SPARSE_DATA
#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_RPC_WRAPPERS
#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameplayAbilityGraphSchema(); \
	friend struct Z_Construct_UClass_UGameplayAbilityGraphSchema_Statics; \
public: \
	DECLARE_CLASS(UGameplayAbilityGraphSchema, UEdGraphSchema_K2, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GameplayAbilitiesEditor"), GAMEPLAYABILITIESEDITOR_API) \
	DECLARE_SERIALIZER(UGameplayAbilityGraphSchema)


#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUGameplayAbilityGraphSchema(); \
	friend struct Z_Construct_UClass_UGameplayAbilityGraphSchema_Statics; \
public: \
	DECLARE_CLASS(UGameplayAbilityGraphSchema, UEdGraphSchema_K2, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GameplayAbilitiesEditor"), GAMEPLAYABILITIESEDITOR_API) \
	DECLARE_SERIALIZER(UGameplayAbilityGraphSchema)


#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAMEPLAYABILITIESEDITOR_API UGameplayAbilityGraphSchema(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameplayAbilityGraphSchema) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMEPLAYABILITIESEDITOR_API, UGameplayAbilityGraphSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameplayAbilityGraphSchema); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMEPLAYABILITIESEDITOR_API UGameplayAbilityGraphSchema(UGameplayAbilityGraphSchema&&); \
	GAMEPLAYABILITIESEDITOR_API UGameplayAbilityGraphSchema(const UGameplayAbilityGraphSchema&); \
public:


#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAMEPLAYABILITIESEDITOR_API UGameplayAbilityGraphSchema(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMEPLAYABILITIESEDITOR_API UGameplayAbilityGraphSchema(UGameplayAbilityGraphSchema&&); \
	GAMEPLAYABILITIESEDITOR_API UGameplayAbilityGraphSchema(const UGameplayAbilityGraphSchema&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMEPLAYABILITIESEDITOR_API, UGameplayAbilityGraphSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameplayAbilityGraphSchema); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameplayAbilityGraphSchema)


#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_10_PROLOG
#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_INCLASS \
	Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GameplayAbilityGraphSchema."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEPLAYABILITIESEDITOR_API UClass* StaticClass<class UGameplayAbilityGraphSchema>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GameplayAbilities_Source_GameplayAbilitiesEditor_Public_GameplayAbilityGraphSchema_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
