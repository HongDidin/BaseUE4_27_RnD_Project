// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ANIMATIONBUDGETALLOCATOR_SkeletalMeshComponentBudgeted_generated_h
#error "SkeletalMeshComponentBudgeted.generated.h already included, missing '#pragma once' in SkeletalMeshComponentBudgeted.h"
#endif
#define ANIMATIONBUDGETALLOCATOR_SkeletalMeshComponentBudgeted_generated_h

#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_SPARSE_DATA
#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetAutoRegisterWithBudgetAllocator);


#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetAutoRegisterWithBudgetAllocator);


#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSkeletalMeshComponentBudgeted(); \
	friend struct Z_Construct_UClass_USkeletalMeshComponentBudgeted_Statics; \
public: \
	DECLARE_CLASS(USkeletalMeshComponentBudgeted, USkeletalMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AnimationBudgetAllocator"), NO_API) \
	DECLARE_SERIALIZER(USkeletalMeshComponentBudgeted)


#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUSkeletalMeshComponentBudgeted(); \
	friend struct Z_Construct_UClass_USkeletalMeshComponentBudgeted_Statics; \
public: \
	DECLARE_CLASS(USkeletalMeshComponentBudgeted, USkeletalMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AnimationBudgetAllocator"), NO_API) \
	DECLARE_SERIALIZER(USkeletalMeshComponentBudgeted)


#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkeletalMeshComponentBudgeted(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkeletalMeshComponentBudgeted) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkeletalMeshComponentBudgeted); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkeletalMeshComponentBudgeted); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkeletalMeshComponentBudgeted(USkeletalMeshComponentBudgeted&&); \
	NO_API USkeletalMeshComponentBudgeted(const USkeletalMeshComponentBudgeted&); \
public:


#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkeletalMeshComponentBudgeted(USkeletalMeshComponentBudgeted&&); \
	NO_API USkeletalMeshComponentBudgeted(const USkeletalMeshComponentBudgeted&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkeletalMeshComponentBudgeted); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkeletalMeshComponentBudgeted); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkeletalMeshComponentBudgeted)


#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_18_PROLOG
#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_INCLASS \
	Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ANIMATIONBUDGETALLOCATOR_API UClass* StaticClass<class USkeletalMeshComponentBudgeted>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AnimationBudgetAllocator_Source_AnimationBudgetAllocator_Public_SkeletalMeshComponentBudgeted_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
