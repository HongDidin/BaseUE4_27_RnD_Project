// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GooglePADEditor/Public/GooglePADRuntimeSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGooglePADRuntimeSettings() {}
// Cross Module References
	GOOGLEPADEDITOR_API UClass* Z_Construct_UClass_UGooglePADRuntimeSettings_NoRegister();
	GOOGLEPADEDITOR_API UClass* Z_Construct_UClass_UGooglePADRuntimeSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_GooglePADEditor();
// End Cross Module References
	void UGooglePADRuntimeSettings::StaticRegisterNativesUGooglePADRuntimeSettings()
	{
	}
	UClass* Z_Construct_UClass_UGooglePADRuntimeSettings_NoRegister()
	{
		return UGooglePADRuntimeSettings::StaticClass();
	}
	struct Z_Construct_UClass_UGooglePADRuntimeSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnablePlugin_MetaData[];
#endif
		static void NewProp_bEnablePlugin_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnablePlugin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyDistribution_MetaData[];
#endif
		static void NewProp_bOnlyDistribution_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyDistribution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyShipping_MetaData[];
#endif
		static void NewProp_bOnlyShipping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyShipping;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GooglePADEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Implements the settings for the GooglePAD plugin.\n*/" },
		{ "IncludePath", "GooglePADRuntimeSettings.h" },
		{ "ModuleRelativePath", "Public/GooglePADRuntimeSettings.h" },
		{ "ToolTip", "Implements the settings for the GooglePAD plugin." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bEnablePlugin_MetaData[] = {
		{ "Category", "Packaging" },
		{ "Comment", "// Enable GooglePAD plugin\n" },
		{ "ModuleRelativePath", "Public/GooglePADRuntimeSettings.h" },
		{ "ToolTip", "Enable GooglePAD plugin" },
	};
#endif
	void Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bEnablePlugin_SetBit(void* Obj)
	{
		((UGooglePADRuntimeSettings*)Obj)->bEnablePlugin = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bEnablePlugin = { "bEnablePlugin", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGooglePADRuntimeSettings), &Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bEnablePlugin_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bEnablePlugin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bEnablePlugin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyDistribution_MetaData[] = {
		{ "Category", "Packaging" },
		{ "Comment", "// Only for distribution builds\n" },
		{ "ModuleRelativePath", "Public/GooglePADRuntimeSettings.h" },
		{ "ToolTip", "Only for distribution builds" },
	};
#endif
	void Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyDistribution_SetBit(void* Obj)
	{
		((UGooglePADRuntimeSettings*)Obj)->bOnlyDistribution = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyDistribution = { "bOnlyDistribution", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGooglePADRuntimeSettings), &Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyDistribution_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyDistribution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyDistribution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyShipping_MetaData[] = {
		{ "Category", "Packaging" },
		{ "Comment", "// Only for shipping builds\n" },
		{ "ModuleRelativePath", "Public/GooglePADRuntimeSettings.h" },
		{ "ToolTip", "Only for shipping builds" },
	};
#endif
	void Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyShipping_SetBit(void* Obj)
	{
		((UGooglePADRuntimeSettings*)Obj)->bOnlyShipping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyShipping = { "bOnlyShipping", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGooglePADRuntimeSettings), &Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyShipping_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyShipping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyShipping_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bEnablePlugin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyDistribution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::NewProp_bOnlyShipping,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGooglePADRuntimeSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::ClassParams = {
		&UGooglePADRuntimeSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGooglePADRuntimeSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGooglePADRuntimeSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGooglePADRuntimeSettings, 575296562);
	template<> GOOGLEPADEDITOR_API UClass* StaticClass<UGooglePADRuntimeSettings>()
	{
		return UGooglePADRuntimeSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGooglePADRuntimeSettings(Z_Construct_UClass_UGooglePADRuntimeSettings, &UGooglePADRuntimeSettings::StaticClass, TEXT("/Script/GooglePADEditor"), TEXT("UGooglePADRuntimeSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGooglePADRuntimeSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
