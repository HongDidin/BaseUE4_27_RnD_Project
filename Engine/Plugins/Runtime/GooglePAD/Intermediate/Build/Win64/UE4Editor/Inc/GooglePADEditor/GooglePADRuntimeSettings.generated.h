// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEPADEDITOR_GooglePADRuntimeSettings_generated_h
#error "GooglePADRuntimeSettings.generated.h already included, missing '#pragma once' in GooglePADRuntimeSettings.h"
#endif
#define GOOGLEPADEDITOR_GooglePADRuntimeSettings_generated_h

#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_RPC_WRAPPERS
#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGooglePADRuntimeSettings(); \
	friend struct Z_Construct_UClass_UGooglePADRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(UGooglePADRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/GooglePADEditor"), NO_API) \
	DECLARE_SERIALIZER(UGooglePADRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUGooglePADRuntimeSettings(); \
	friend struct Z_Construct_UClass_UGooglePADRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(UGooglePADRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/GooglePADEditor"), NO_API) \
	DECLARE_SERIALIZER(UGooglePADRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGooglePADRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGooglePADRuntimeSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGooglePADRuntimeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGooglePADRuntimeSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGooglePADRuntimeSettings(UGooglePADRuntimeSettings&&); \
	NO_API UGooglePADRuntimeSettings(const UGooglePADRuntimeSettings&); \
public:


#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGooglePADRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGooglePADRuntimeSettings(UGooglePADRuntimeSettings&&); \
	NO_API UGooglePADRuntimeSettings(const UGooglePADRuntimeSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGooglePADRuntimeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGooglePADRuntimeSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGooglePADRuntimeSettings)


#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_14_PROLOG
#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_INCLASS \
	Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GooglePADRuntimeSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEPADEDITOR_API UClass* StaticClass<class UGooglePADRuntimeSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_GooglePAD_Source_GooglePADEditor_Public_GooglePADRuntimeSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
