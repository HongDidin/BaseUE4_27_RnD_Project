// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LocationServicesAndroidEditor/Classes/LocationServicesAndroidSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLocationServicesAndroidSettings() {}
// Cross Module References
	LOCATIONSERVICESANDROIDEDITOR_API UClass* Z_Construct_UClass_ULocationServicesAndroidSettings_NoRegister();
	LOCATIONSERVICESANDROIDEDITOR_API UClass* Z_Construct_UClass_ULocationServicesAndroidSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LocationServicesAndroidEditor();
// End Cross Module References
	void ULocationServicesAndroidSettings::StaticRegisterNativesULocationServicesAndroidSettings()
	{
	}
	UClass* Z_Construct_UClass_ULocationServicesAndroidSettings_NoRegister()
	{
		return ULocationServicesAndroidSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULocationServicesAndroidSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCoarseLocationEnabled_MetaData[];
#endif
		static void NewProp_bCoarseLocationEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCoarseLocationEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFineLocationEnabled_MetaData[];
#endif
		static void NewProp_bFineLocationEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFineLocationEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLocationUpdatesEnabled_MetaData[];
#endif
		static void NewProp_bLocationUpdatesEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLocationUpdatesEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LocationServicesAndroidEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LocationServicesAndroidSettings.h" },
		{ "ModuleRelativePath", "Classes/LocationServicesAndroidSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bCoarseLocationEnabled_MetaData[] = {
		{ "Category", "LocationServices" },
		{ "DisplayName", "Enable Coarse Location Accuracy (Network Provider)" },
		{ "ModuleRelativePath", "Classes/LocationServicesAndroidSettings.h" },
	};
#endif
	void Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bCoarseLocationEnabled_SetBit(void* Obj)
	{
		((ULocationServicesAndroidSettings*)Obj)->bCoarseLocationEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bCoarseLocationEnabled = { "bCoarseLocationEnabled", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULocationServicesAndroidSettings), &Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bCoarseLocationEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bCoarseLocationEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bCoarseLocationEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bFineLocationEnabled_MetaData[] = {
		{ "Category", "LocationServices" },
		{ "DisplayName", "Enable Fine Location Accuracy (GPS Provider)" },
		{ "ModuleRelativePath", "Classes/LocationServicesAndroidSettings.h" },
	};
#endif
	void Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bFineLocationEnabled_SetBit(void* Obj)
	{
		((ULocationServicesAndroidSettings*)Obj)->bFineLocationEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bFineLocationEnabled = { "bFineLocationEnabled", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULocationServicesAndroidSettings), &Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bFineLocationEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bFineLocationEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bFineLocationEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bLocationUpdatesEnabled_MetaData[] = {
		{ "Category", "LocationServices" },
		{ "DisplayName", "Enable Location Updates" },
		{ "ModuleRelativePath", "Classes/LocationServicesAndroidSettings.h" },
	};
#endif
	void Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bLocationUpdatesEnabled_SetBit(void* Obj)
	{
		((ULocationServicesAndroidSettings*)Obj)->bLocationUpdatesEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bLocationUpdatesEnabled = { "bLocationUpdatesEnabled", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULocationServicesAndroidSettings), &Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bLocationUpdatesEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bLocationUpdatesEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bLocationUpdatesEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bCoarseLocationEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bFineLocationEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::NewProp_bLocationUpdatesEnabled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULocationServicesAndroidSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::ClassParams = {
		&ULocationServicesAndroidSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULocationServicesAndroidSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULocationServicesAndroidSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULocationServicesAndroidSettings, 3251014642);
	template<> LOCATIONSERVICESANDROIDEDITOR_API UClass* StaticClass<ULocationServicesAndroidSettings>()
	{
		return ULocationServicesAndroidSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULocationServicesAndroidSettings(Z_Construct_UClass_ULocationServicesAndroidSettings, &ULocationServicesAndroidSettings::StaticClass, TEXT("/Script/LocationServicesAndroidEditor"), TEXT("ULocationServicesAndroidSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULocationServicesAndroidSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
