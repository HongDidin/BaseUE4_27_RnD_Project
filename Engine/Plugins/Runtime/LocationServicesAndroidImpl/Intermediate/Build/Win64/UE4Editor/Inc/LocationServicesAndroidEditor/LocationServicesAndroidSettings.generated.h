// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOCATIONSERVICESANDROIDEDITOR_LocationServicesAndroidSettings_generated_h
#error "LocationServicesAndroidSettings.generated.h already included, missing '#pragma once' in LocationServicesAndroidSettings.h"
#endif
#define LOCATIONSERVICESANDROIDEDITOR_LocationServicesAndroidSettings_generated_h

#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_SPARSE_DATA
#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_RPC_WRAPPERS
#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULocationServicesAndroidSettings(); \
	friend struct Z_Construct_UClass_ULocationServicesAndroidSettings_Statics; \
public: \
	DECLARE_CLASS(ULocationServicesAndroidSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/LocationServicesAndroidEditor"), NO_API) \
	DECLARE_SERIALIZER(ULocationServicesAndroidSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_INCLASS \
private: \
	static void StaticRegisterNativesULocationServicesAndroidSettings(); \
	friend struct Z_Construct_UClass_ULocationServicesAndroidSettings_Statics; \
public: \
	DECLARE_CLASS(ULocationServicesAndroidSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/LocationServicesAndroidEditor"), NO_API) \
	DECLARE_SERIALIZER(ULocationServicesAndroidSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULocationServicesAndroidSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULocationServicesAndroidSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULocationServicesAndroidSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULocationServicesAndroidSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULocationServicesAndroidSettings(ULocationServicesAndroidSettings&&); \
	NO_API ULocationServicesAndroidSettings(const ULocationServicesAndroidSettings&); \
public:


#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULocationServicesAndroidSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULocationServicesAndroidSettings(ULocationServicesAndroidSettings&&); \
	NO_API ULocationServicesAndroidSettings(const ULocationServicesAndroidSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULocationServicesAndroidSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULocationServicesAndroidSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULocationServicesAndroidSettings)


#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_8_PROLOG
#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_RPC_WRAPPERS \
	Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_INCLASS \
	Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h_12_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LocationServicesAndroidSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LOCATIONSERVICESANDROIDEDITOR_API UClass* StaticClass<class ULocationServicesAndroidSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_LocationServicesAndroidImpl_Source_LocationServicesAndroidEditor_Classes_LocationServicesAndroidSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
