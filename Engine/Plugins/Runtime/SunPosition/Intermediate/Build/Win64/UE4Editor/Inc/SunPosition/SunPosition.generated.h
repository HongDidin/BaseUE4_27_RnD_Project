// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FSunPositionData;
#ifdef SUNPOSITION_SunPosition_generated_h
#error "SunPosition.generated.h already included, missing '#pragma once' in SunPosition.h"
#endif
#define SUNPOSITION_SunPosition_generated_h

#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSunPositionData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> SUNPOSITION_API UScriptStruct* StaticStruct<struct FSunPositionData>();

#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_SPARSE_DATA
#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetSunPosition);


#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetSunPosition);


#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSunPositionFunctionLibrary(); \
	friend struct Z_Construct_UClass_USunPositionFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(USunPositionFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SunPosition"), NO_API) \
	DECLARE_SERIALIZER(USunPositionFunctionLibrary)


#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_INCLASS \
private: \
	static void StaticRegisterNativesUSunPositionFunctionLibrary(); \
	friend struct Z_Construct_UClass_USunPositionFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(USunPositionFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SunPosition"), NO_API) \
	DECLARE_SERIALIZER(USunPositionFunctionLibrary)


#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USunPositionFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USunPositionFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USunPositionFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USunPositionFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USunPositionFunctionLibrary(USunPositionFunctionLibrary&&); \
	NO_API USunPositionFunctionLibrary(const USunPositionFunctionLibrary&); \
public:


#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USunPositionFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USunPositionFunctionLibrary(USunPositionFunctionLibrary&&); \
	NO_API USunPositionFunctionLibrary(const USunPositionFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USunPositionFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USunPositionFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USunPositionFunctionLibrary)


#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_44_PROLOG
#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_SPARSE_DATA \
	Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_INCLASS \
	Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_SPARSE_DATA \
	Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h_47_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SUNPOSITION_API UClass* StaticClass<class USunPositionFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SunPosition_Source_SunPosition_Public_SunPosition_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
