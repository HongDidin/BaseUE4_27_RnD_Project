// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SunPosition/Public/SunPosition.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSunPosition() {}
// Cross Module References
	SUNPOSITION_API UScriptStruct* Z_Construct_UScriptStruct_FSunPositionData();
	UPackage* Z_Construct_UPackage__Script_SunPosition();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTimespan();
	SUNPOSITION_API UClass* Z_Construct_UClass_USunPositionFunctionLibrary_NoRegister();
	SUNPOSITION_API UClass* Z_Construct_UClass_USunPositionFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
// End Cross Module References
class UScriptStruct* FSunPositionData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SUNPOSITION_API uint32 Get_Z_Construct_UScriptStruct_FSunPositionData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSunPositionData, Z_Construct_UPackage__Script_SunPosition(), TEXT("SunPositionData"), sizeof(FSunPositionData), Get_Z_Construct_UScriptStruct_FSunPositionData_Hash());
	}
	return Singleton;
}
template<> SUNPOSITION_API UScriptStruct* StaticStruct<FSunPositionData>()
{
	return FSunPositionData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSunPositionData(FSunPositionData::StaticStruct, TEXT("/Script/SunPosition"), TEXT("SunPositionData"), false, nullptr, nullptr);
static struct FScriptStruct_SunPosition_StaticRegisterNativesFSunPositionData
{
	FScriptStruct_SunPosition_StaticRegisterNativesFSunPositionData()
	{
		UScriptStruct::DeferCppStructOps<FSunPositionData>(FName(TEXT("SunPositionData")));
	}
} ScriptStruct_SunPosition_StaticRegisterNativesFSunPositionData;
	struct Z_Construct_UScriptStruct_FSunPositionData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Elevation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Elevation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CorrectedElevation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CorrectedElevation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Azimuth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Azimuth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SunriseTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SunriseTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SunsetTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SunsetTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SolarNoon_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SolarNoon;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSunPositionData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SunPosition.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSunPositionData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSunPositionData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_Elevation_MetaData[] = {
		{ "Category", "Sun Position" },
		{ "Comment", "/** Sun Elevation */" },
		{ "ModuleRelativePath", "Public/SunPosition.h" },
		{ "ToolTip", "Sun Elevation" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_Elevation = { "Elevation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSunPositionData, Elevation), METADATA_PARAMS(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_Elevation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_Elevation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_CorrectedElevation_MetaData[] = {
		{ "Category", "Sun Position" },
		{ "Comment", "/** Sun Elevation, corrected for atmospheric diffraction */" },
		{ "ModuleRelativePath", "Public/SunPosition.h" },
		{ "ToolTip", "Sun Elevation, corrected for atmospheric diffraction" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_CorrectedElevation = { "CorrectedElevation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSunPositionData, CorrectedElevation), METADATA_PARAMS(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_CorrectedElevation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_CorrectedElevation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_Azimuth_MetaData[] = {
		{ "Category", "Sun Position" },
		{ "Comment", "/** Sun azimuth */" },
		{ "ModuleRelativePath", "Public/SunPosition.h" },
		{ "ToolTip", "Sun azimuth" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_Azimuth = { "Azimuth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSunPositionData, Azimuth), METADATA_PARAMS(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_Azimuth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_Azimuth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SunriseTime_MetaData[] = {
		{ "Category", "Sun Position" },
		{ "Comment", "/** Sunrise time */" },
		{ "ModuleRelativePath", "Public/SunPosition.h" },
		{ "ToolTip", "Sunrise time" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SunriseTime = { "SunriseTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSunPositionData, SunriseTime), Z_Construct_UScriptStruct_FTimespan, METADATA_PARAMS(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SunriseTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SunriseTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SunsetTime_MetaData[] = {
		{ "Category", "Sun Position" },
		{ "Comment", "/** Sunset time */" },
		{ "ModuleRelativePath", "Public/SunPosition.h" },
		{ "ToolTip", "Sunset time" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SunsetTime = { "SunsetTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSunPositionData, SunsetTime), Z_Construct_UScriptStruct_FTimespan, METADATA_PARAMS(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SunsetTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SunsetTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SolarNoon_MetaData[] = {
		{ "Category", "Sun Position" },
		{ "Comment", "/** Solar noon */" },
		{ "ModuleRelativePath", "Public/SunPosition.h" },
		{ "ToolTip", "Solar noon" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SolarNoon = { "SolarNoon", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSunPositionData, SolarNoon), Z_Construct_UScriptStruct_FTimespan, METADATA_PARAMS(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SolarNoon_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SolarNoon_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSunPositionData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_Elevation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_CorrectedElevation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_Azimuth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SunriseTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SunsetTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSunPositionData_Statics::NewProp_SolarNoon,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSunPositionData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SunPosition,
		nullptr,
		&NewStructOps,
		"SunPositionData",
		sizeof(FSunPositionData),
		alignof(FSunPositionData),
		Z_Construct_UScriptStruct_FSunPositionData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSunPositionData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSunPositionData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSunPositionData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSunPositionData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSunPositionData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SunPosition();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SunPositionData"), sizeof(FSunPositionData), Get_Z_Construct_UScriptStruct_FSunPositionData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSunPositionData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSunPositionData_Hash() { return 4065638196U; }
	DEFINE_FUNCTION(USunPositionFunctionLibrary::execGetSunPosition)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_Latitude);
		P_GET_PROPERTY(FFloatProperty,Z_Param_Longitude);
		P_GET_PROPERTY(FFloatProperty,Z_Param_TimeZone);
		P_GET_UBOOL(Z_Param_bIsDaylightSavingTime);
		P_GET_PROPERTY(FIntProperty,Z_Param_Year);
		P_GET_PROPERTY(FIntProperty,Z_Param_Month);
		P_GET_PROPERTY(FIntProperty,Z_Param_Day);
		P_GET_PROPERTY(FIntProperty,Z_Param_Hours);
		P_GET_PROPERTY(FIntProperty,Z_Param_Minutes);
		P_GET_PROPERTY(FIntProperty,Z_Param_Seconds);
		P_GET_STRUCT_REF(FSunPositionData,Z_Param_Out_SunPositionData);
		P_FINISH;
		P_NATIVE_BEGIN;
		USunPositionFunctionLibrary::GetSunPosition(Z_Param_Latitude,Z_Param_Longitude,Z_Param_TimeZone,Z_Param_bIsDaylightSavingTime,Z_Param_Year,Z_Param_Month,Z_Param_Day,Z_Param_Hours,Z_Param_Minutes,Z_Param_Seconds,Z_Param_Out_SunPositionData);
		P_NATIVE_END;
	}
	void USunPositionFunctionLibrary::StaticRegisterNativesUSunPositionFunctionLibrary()
	{
		UClass* Class = USunPositionFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetSunPosition", &USunPositionFunctionLibrary::execGetSunPosition },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics
	{
		struct SunPositionFunctionLibrary_eventGetSunPosition_Parms
		{
			float Latitude;
			float Longitude;
			float TimeZone;
			bool bIsDaylightSavingTime;
			int32 Year;
			int32 Month;
			int32 Day;
			int32 Hours;
			int32 Minutes;
			int32 Seconds;
			FSunPositionData SunPositionData;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Latitude;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Longitude;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeZone;
		static void NewProp_bIsDaylightSavingTime_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsDaylightSavingTime;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Year;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Month;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Day;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Hours;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Minutes;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Seconds;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SunPositionData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Latitude = { "Latitude", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SunPositionFunctionLibrary_eventGetSunPosition_Parms, Latitude), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Longitude = { "Longitude", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SunPositionFunctionLibrary_eventGetSunPosition_Parms, Longitude), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_TimeZone = { "TimeZone", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SunPositionFunctionLibrary_eventGetSunPosition_Parms, TimeZone), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_bIsDaylightSavingTime_SetBit(void* Obj)
	{
		((SunPositionFunctionLibrary_eventGetSunPosition_Parms*)Obj)->bIsDaylightSavingTime = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_bIsDaylightSavingTime = { "bIsDaylightSavingTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SunPositionFunctionLibrary_eventGetSunPosition_Parms), &Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_bIsDaylightSavingTime_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Year = { "Year", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SunPositionFunctionLibrary_eventGetSunPosition_Parms, Year), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Month = { "Month", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SunPositionFunctionLibrary_eventGetSunPosition_Parms, Month), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Day = { "Day", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SunPositionFunctionLibrary_eventGetSunPosition_Parms, Day), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Hours = { "Hours", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SunPositionFunctionLibrary_eventGetSunPosition_Parms, Hours), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Minutes = { "Minutes", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SunPositionFunctionLibrary_eventGetSunPosition_Parms, Minutes), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Seconds = { "Seconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SunPositionFunctionLibrary_eventGetSunPosition_Parms, Seconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_SunPositionData = { "SunPositionData", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SunPositionFunctionLibrary_eventGetSunPosition_Parms, SunPositionData), Z_Construct_UScriptStruct_FSunPositionData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Latitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Longitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_TimeZone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_bIsDaylightSavingTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Year,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Month,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Day,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Hours,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Minutes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_Seconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::NewProp_SunPositionData,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sun Position" },
		{ "Comment", "/** Get the sun's position data based on position, date and time */" },
		{ "ModuleRelativePath", "Public/SunPosition.h" },
		{ "ToolTip", "Get the sun's position data based on position, date and time" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USunPositionFunctionLibrary, nullptr, "GetSunPosition", nullptr, nullptr, sizeof(SunPositionFunctionLibrary_eventGetSunPosition_Parms), Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_USunPositionFunctionLibrary_NoRegister()
	{
		return USunPositionFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_USunPositionFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USunPositionFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_SunPosition,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USunPositionFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USunPositionFunctionLibrary_GetSunPosition, "GetSunPosition" }, // 2544061090
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USunPositionFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SunPosition.h" },
		{ "ModuleRelativePath", "Public/SunPosition.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USunPositionFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USunPositionFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USunPositionFunctionLibrary_Statics::ClassParams = {
		&USunPositionFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USunPositionFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USunPositionFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USunPositionFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USunPositionFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USunPositionFunctionLibrary, 906437696);
	template<> SUNPOSITION_API UClass* StaticClass<USunPositionFunctionLibrary>()
	{
		return USunPositionFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USunPositionFunctionLibrary(Z_Construct_UClass_USunPositionFunctionLibrary, &USunPositionFunctionLibrary::StaticClass, TEXT("/Script/SunPosition"), TEXT("USunPositionFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USunPositionFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
