// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomAsset.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomAsset() {}
// Cross Module References
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupInfo();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsMaterial();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomAsset_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomAsset();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsRendering();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsPhysics();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsInterpolation();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsLOD();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsMeshesSourceDescription();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationType();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EHairLODSelectionType();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPerPlatformInt();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPerPlatformBool();
	ENGINE_API UClass* Z_Construct_UClass_UAssetImportData_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UAssetUserData_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UInterface_AssetUserData_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FHairGroupInfoWithVisibility>() == std::is_polymorphic<FHairGroupInfo>(), "USTRUCT FHairGroupInfoWithVisibility cannot be polymorphic unless super FHairGroupInfo is polymorphic");

class UScriptStruct* FHairGroupInfoWithVisibility::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupInfoWithVisibility"), sizeof(FHairGroupInfoWithVisibility), Get_Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupInfoWithVisibility>()
{
	return FHairGroupInfoWithVisibility::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupInfoWithVisibility(FHairGroupInfoWithVisibility::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupInfoWithVisibility"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupInfoWithVisibility
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupInfoWithVisibility()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupInfoWithVisibility>(FName(TEXT("HairGroupInfoWithVisibility")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupInfoWithVisibility;
	struct Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsVisible_MetaData[];
#endif
		static void NewProp_bIsVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsVisible;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupInfoWithVisibility>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::NewProp_bIsVisible_MetaData[] = {
		{ "Category", "Info" },
		{ "Comment", "/** Toggle hair group visibility. This visibility flag is not persistent to the asset, and exists only as a preview/helper mechanism */" },
		{ "DisplayName", "Visible" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Toggle hair group visibility. This visibility flag is not persistent to the asset, and exists only as a preview/helper mechanism" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::NewProp_bIsVisible_SetBit(void* Obj)
	{
		((FHairGroupInfoWithVisibility*)Obj)->bIsVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::NewProp_bIsVisible = { "bIsVisible", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairGroupInfoWithVisibility), &Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::NewProp_bIsVisible_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::NewProp_bIsVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::NewProp_bIsVisible_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::NewProp_bIsVisible,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		Z_Construct_UScriptStruct_FHairGroupInfo,
		&NewStructOps,
		"HairGroupInfoWithVisibility",
		sizeof(FHairGroupInfoWithVisibility),
		alignof(FHairGroupInfoWithVisibility),
		Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupInfoWithVisibility"), sizeof(FHairGroupInfoWithVisibility), Get_Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Hash() { return 203950700U; }
class UScriptStruct* FHairGroupsMaterial::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupsMaterial_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupsMaterial, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupsMaterial"), sizeof(FHairGroupsMaterial), Get_Z_Construct_UScriptStruct_FHairGroupsMaterial_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupsMaterial>()
{
	return FHairGroupsMaterial::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupsMaterial(FHairGroupsMaterial::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupsMaterial"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsMaterial
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsMaterial()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupsMaterial>(FName(TEXT("HairGroupsMaterial")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsMaterial;
	struct Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlotName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SlotName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupsMaterial>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewProp_Material_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsMaterial, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewProp_Material_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewProp_SlotName_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewProp_SlotName = { "SlotName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsMaterial, SlotName), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewProp_SlotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewProp_SlotName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewProp_Material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::NewProp_SlotName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGroupsMaterial",
		sizeof(FHairGroupsMaterial),
		alignof(FHairGroupsMaterial),
		Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsMaterial()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupsMaterial_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupsMaterial"), sizeof(FHairGroupsMaterial), Get_Z_Construct_UScriptStruct_FHairGroupsMaterial_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupsMaterial_Hash() { return 3309616333U; }
class UScriptStruct* FHairGroupInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupInfo, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupInfo"), sizeof(FHairGroupInfo), Get_Z_Construct_UScriptStruct_FHairGroupInfo_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupInfo>()
{
	return FHairGroupInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupInfo(FHairGroupInfo::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupInfo"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupInfo
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupInfo()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupInfo>(FName(TEXT("HairGroupInfo")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupInfo;
	struct Z_Construct_UScriptStruct_FHairGroupInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_GroupID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumCurves_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumCurves;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumGuides_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumGuides;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumCurveVertices_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumCurveVertices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumGuideVertices_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumGuideVertices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxCurveLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxCurveLength;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_GroupID_MetaData[] = {
		{ "Category", "Info" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_GroupID = { "GroupID", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupInfo, GroupID), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_GroupID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_GroupID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumCurves_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Curve Count" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumCurves = { "NumCurves", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupInfo, NumCurves), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumCurves_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumCurves_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumGuides_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Guide Count" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumGuides = { "NumGuides", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupInfo, NumGuides), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumGuides_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumGuides_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumCurveVertices_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Curve Vertex Count" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumCurveVertices = { "NumCurveVertices", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupInfo, NumCurveVertices), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumCurveVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumCurveVertices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumGuideVertices_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Guide Vertex Count" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumGuideVertices = { "NumGuideVertices", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupInfo, NumGuideVertices), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumGuideVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumGuideVertices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_MaxCurveLength_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Length of the longest hair strands" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_MaxCurveLength = { "MaxCurveLength", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupInfo, MaxCurveLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_MaxCurveLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_MaxCurveLength_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_GroupID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumCurves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumGuides,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumCurveVertices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_NumGuideVertices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupInfo_Statics::NewProp_MaxCurveLength,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGroupInfo",
		sizeof(FHairGroupInfo),
		alignof(FHairGroupInfo),
		Z_Construct_UScriptStruct_FHairGroupInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupInfo"), sizeof(FHairGroupInfo), Get_Z_Construct_UScriptStruct_FHairGroupInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupInfo_Hash() { return 1258979782U; }
	void UGroomAsset::StaticRegisterNativesUGroomAsset()
	{
	}
	UClass* Z_Construct_UClass_UGroomAsset_NoRegister()
	{
		return UGroomAsset::StaticClass();
	}
	struct Z_Construct_UClass_UGroomAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HairGroupsInfo_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairGroupsInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HairGroupsInfo;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HairGroupsRendering_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairGroupsRendering_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HairGroupsRendering;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HairGroupsPhysics_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairGroupsPhysics_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HairGroupsPhysics;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HairGroupsInterpolation_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairGroupsInterpolation_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HairGroupsInterpolation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HairGroupsLOD_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairGroupsLOD_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HairGroupsLOD;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HairGroupsCards_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairGroupsCards_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HairGroupsCards;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HairGroupsMeshes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairGroupsMeshes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HairGroupsMeshes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HairGroupsMaterials_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairGroupsMaterials_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HairGroupsMaterials;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnableGlobalInterpolation_MetaData[];
#endif
		static void NewProp_EnableGlobalInterpolation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_EnableGlobalInterpolation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_HairInterpolationType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairInterpolationType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_HairInterpolationType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LODSelectionType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LODSelectionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LODSelectionType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinLOD_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MinLOD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisableBelowMinLodStripping_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DisableBelowMinLodStripping;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EffectiveLODBias_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EffectiveLODBias_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EffectiveLODBias;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetImportData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetImportData;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetUserData_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetUserData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetUserData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AssetUserData;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsCacheable_MetaData[];
#endif
		static void NewProp_bIsCacheable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsCacheable;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::Class_MetaDataParams[] = {
		{ "AutoExpandCategories", "HairRendering HairPhysics HairInterpolation" },
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Implements an asset that can be used to store hair strands\n */" },
		{ "HideCategories", "Object Hidden" },
		{ "IncludePath", "GroomAsset.h" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Implements an asset that can be used to store hair strands" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInfo_Inner = { "HairGroupsInfo", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInfo_MetaData[] = {
		{ "Category", "HairInfo" },
		{ "DisplayName", "Group" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInfo = { "HairGroupsInfo", nullptr, (EPropertyFlags)0x0010000000002041, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, HairGroupsInfo), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInfo_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsRendering_Inner = { "HairGroupsRendering", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairGroupsRendering, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsRendering_MetaData[] = {
		{ "Category", "HairRendering" },
		{ "DisplayName", "Group" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsRendering = { "HairGroupsRendering", nullptr, (EPropertyFlags)0x0010000000000045, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, HairGroupsRendering), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsRendering_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsRendering_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsPhysics_Inner = { "HairGroupsPhysics", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairGroupsPhysics, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsPhysics_MetaData[] = {
		{ "Category", "HairPhysics" },
		{ "DisplayName", "Group" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsPhysics = { "HairGroupsPhysics", nullptr, (EPropertyFlags)0x0010000000000045, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, HairGroupsPhysics), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsPhysics_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsPhysics_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInterpolation_Inner = { "HairGroupsInterpolation", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairGroupsInterpolation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInterpolation_MetaData[] = {
		{ "Category", "HairInterpolation" },
		{ "DisplayName", "Group" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInterpolation = { "HairGroupsInterpolation", nullptr, (EPropertyFlags)0x0010000000000045, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, HairGroupsInterpolation), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInterpolation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInterpolation_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsLOD_Inner = { "HairGroupsLOD", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairGroupsLOD, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsLOD_MetaData[] = {
		{ "Category", "HairLOD" },
		{ "DisplayName", "Group" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsLOD = { "HairGroupsLOD", nullptr, (EPropertyFlags)0x0010000000000045, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, HairGroupsLOD), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsLOD_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsLOD_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsCards_Inner = { "HairGroupsCards", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsCards_MetaData[] = {
		{ "Category", "HairCards" },
		{ "Comment", "/** Cards - Source description data */" },
		{ "DisplayName", "Group" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Cards - Source description data" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsCards = { "HairGroupsCards", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, HairGroupsCards), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsCards_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsCards_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMeshes_Inner = { "HairGroupsMeshes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairGroupsMeshesSourceDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMeshes_MetaData[] = {
		{ "Category", "HairMeshes" },
		{ "Comment", "/** Meshes - Source description data */" },
		{ "DisplayName", "Group" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Meshes - Source description data" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMeshes = { "HairGroupsMeshes", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, HairGroupsMeshes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMeshes_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMaterials_Inner = { "HairGroupsMaterials", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairGroupsMaterial, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMaterials_MetaData[] = {
		{ "Category", "HairMaterials" },
		{ "Comment", "/** Meshes - Source description data */" },
		{ "DisplayName", "Group" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Meshes - Source description data" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMaterials = { "HairGroupsMaterials", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, HairGroupsMaterials), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMaterials_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_EnableGlobalInterpolation_MetaData[] = {
		{ "Category", "HairInterpolation" },
		{ "DisplayName", "RBF Interpolation" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Enable radial basis function interpolation to be used instead of the local skin rigid transform (WIP)" },
	};
#endif
	void Z_Construct_UClass_UGroomAsset_Statics::NewProp_EnableGlobalInterpolation_SetBit(void* Obj)
	{
		((UGroomAsset*)Obj)->EnableGlobalInterpolation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_EnableGlobalInterpolation = { "EnableGlobalInterpolation", nullptr, (EPropertyFlags)0x0010000000000045, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomAsset), &Z_Construct_UClass_UGroomAsset_Statics::NewProp_EnableGlobalInterpolation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_EnableGlobalInterpolation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_EnableGlobalInterpolation_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairInterpolationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairInterpolationType_MetaData[] = {
		{ "Category", "HairInterpolation" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Type of interpolation used (WIP)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairInterpolationType = { "HairInterpolationType", nullptr, (EPropertyFlags)0x0010000000000045, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, HairInterpolationType), Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationType, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairInterpolationType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairInterpolationType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_LODSelectionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_LODSelectionType_MetaData[] = {
		{ "Category", "LOD" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "LOD selection type (CPU/GPU)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_LODSelectionType = { "LODSelectionType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, LODSelectionType), Z_Construct_UEnum_HairStrandsCore_EHairLODSelectionType, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_LODSelectionType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_LODSelectionType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_MinLOD_MetaData[] = {
		{ "Category", "LOD" },
		{ "Comment", "/** Minimum LOD to cook */" },
		{ "DisplayName", "Minimum LOD" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Minimum LOD to cook" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_MinLOD = { "MinLOD", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, MinLOD), Z_Construct_UScriptStruct_FPerPlatformInt, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_MinLOD_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_MinLOD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_DisableBelowMinLodStripping_MetaData[] = {
		{ "Category", "LOD" },
		{ "Comment", "/** When true all LODs below MinLod will still be cooked */" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "When true all LODs below MinLod will still be cooked" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_DisableBelowMinLodStripping = { "DisableBelowMinLodStripping", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, DisableBelowMinLodStripping), Z_Construct_UScriptStruct_FPerPlatformBool, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_DisableBelowMinLodStripping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_DisableBelowMinLodStripping_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_EffectiveLODBias_Inner = { "EffectiveLODBias", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_EffectiveLODBias_MetaData[] = {
		{ "Comment", "/** The LOD bias to use after LOD stripping, regardless of MinLOD. Computed at cook time */" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "The LOD bias to use after LOD stripping, regardless of MinLOD. Computed at cook time" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_EffectiveLODBias = { "EffectiveLODBias", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, EffectiveLODBias), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_EffectiveLODBias_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_EffectiveLODBias_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetImportData_MetaData[] = {
		{ "Category", "ImportSettings" },
		{ "Comment", "/** Asset data to be used when re-importing */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Asset data to be used when re-importing" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetImportData = { "AssetImportData", nullptr, (EPropertyFlags)0x00120008000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, AssetImportData), Z_Construct_UClass_UAssetImportData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetImportData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetImportData_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetUserData_Inner_MetaData[] = {
		{ "Category", "Hidden" },
		{ "Comment", "/** Array of user data stored with the asset */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Array of user data stored with the asset" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetUserData_Inner = { "AssetUserData", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UAssetUserData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetUserData_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetUserData_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetUserData_MetaData[] = {
		{ "Category", "Hidden" },
		{ "Comment", "/** Array of user data stored with the asset */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
		{ "ToolTip", "Array of user data stored with the asset" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetUserData = { "AssetUserData", nullptr, (EPropertyFlags)0x0010048000000009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAsset, AssetUserData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetUserData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetUserData_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAsset_Statics::NewProp_bIsCacheable_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomAsset.h" },
	};
#endif
	void Z_Construct_UClass_UGroomAsset_Statics::NewProp_bIsCacheable_SetBit(void* Obj)
	{
		((UGroomAsset*)Obj)->bIsCacheable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomAsset_Statics::NewProp_bIsCacheable = { "bIsCacheable", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomAsset), &Z_Construct_UClass_UGroomAsset_Statics::NewProp_bIsCacheable_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::NewProp_bIsCacheable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::NewProp_bIsCacheable_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInfo_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsRendering_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsRendering,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsPhysics_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsPhysics,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInterpolation_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsInterpolation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsLOD_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsLOD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsCards_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsCards,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMeshes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMeshes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMaterials_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairGroupsMaterials,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_EnableGlobalInterpolation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairInterpolationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_HairInterpolationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_LODSelectionType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_LODSelectionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_MinLOD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_DisableBelowMinLodStripping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_EffectiveLODBias_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_EffectiveLODBias,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetImportData,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetUserData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_AssetUserData,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAsset_Statics::NewProp_bIsCacheable,
#endif // WITH_EDITORONLY_DATA
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UGroomAsset_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInterface_AssetUserData_NoRegister, (int32)VTABLE_OFFSET(UGroomAsset, IInterface_AssetUserData), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomAsset_Statics::ClassParams = {
		&UGroomAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomAsset_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomAsset, 32256159);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomAsset>()
	{
		return UGroomAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomAsset(Z_Construct_UClass_UGroomAsset, &UGroomAsset::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomAsset);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UGroomAsset)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
