// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomAssetImportData_generated_h
#error "GroomAssetImportData.generated.h already included, missing '#pragma once' in GroomAssetImportData.h"
#endif
#define HAIRSTRANDSCORE_GroomAssetImportData_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomAssetImportData(); \
	friend struct Z_Construct_UClass_UGroomAssetImportData_Statics; \
public: \
	DECLARE_CLASS(UGroomAssetImportData, UAssetImportData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomAssetImportData)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUGroomAssetImportData(); \
	friend struct Z_Construct_UClass_UGroomAssetImportData_Statics; \
public: \
	DECLARE_CLASS(UGroomAssetImportData, UAssetImportData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomAssetImportData)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomAssetImportData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomAssetImportData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomAssetImportData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomAssetImportData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomAssetImportData(UGroomAssetImportData&&); \
	NO_API UGroomAssetImportData(const UGroomAssetImportData&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomAssetImportData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomAssetImportData(UGroomAssetImportData&&); \
	NO_API UGroomAssetImportData(const UGroomAssetImportData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomAssetImportData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomAssetImportData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomAssetImportData)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_10_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GroomAssetImportData."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class UGroomAssetImportData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetImportData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
