// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomAssetInterpolation_generated_h
#error "GroomAssetInterpolation.generated.h already included, missing '#pragma once' in GroomAssetInterpolation.h"
#endif
#define HAIRSTRANDSCORE_GroomAssetInterpolation_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetInterpolation_h_152_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupsLOD_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupsLOD>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetInterpolation_h_134_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupsInterpolation>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetInterpolation_h_100_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairInterpolationSettings>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetInterpolation_h_82_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairDecimationSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairDecimationSettings>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetInterpolation_h_46_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairLODSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairLODSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetInterpolation_h


#define FOREACH_ENUM_EGROOMGEOMETRYTYPE(op) \
	op(EGroomGeometryType::Strands) \
	op(EGroomGeometryType::Cards) \
	op(EGroomGeometryType::Meshes) 

enum class EGroomGeometryType : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomGeometryType>();

#define FOREACH_ENUM_EHAIRLODSELECTIONTYPE(op) \
	op(EHairLODSelectionType::Cpu) \
	op(EHairLODSelectionType::Gpu) 

enum class EHairLODSelectionType : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairLODSelectionType>();

#define FOREACH_ENUM_EHAIRINTERPOLATIONWEIGHT(op) \
	op(EHairInterpolationWeight::Parametric) \
	op(EHairInterpolationWeight::Root) \
	op(EHairInterpolationWeight::Index) \
	op(EHairInterpolationWeight::Unknown) 

enum class EHairInterpolationWeight : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairInterpolationWeight>();

#define FOREACH_ENUM_EHAIRINTERPOLATIONQUALITY(op) \
	op(EHairInterpolationQuality::Low) \
	op(EHairInterpolationQuality::Medium) \
	op(EHairInterpolationQuality::High) \
	op(EHairInterpolationQuality::Unknown) 

enum class EHairInterpolationQuality : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairInterpolationQuality>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
