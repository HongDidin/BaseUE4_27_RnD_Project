// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomPluginSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomPluginSettings() {}
// Cross Module References
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomPluginSettings_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomPluginSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
// End Cross Module References
	void UGroomPluginSettings::StaticRegisterNativesUGroomPluginSettings()
	{
	}
	UClass* Z_Construct_UClass_UGroomPluginSettings_NoRegister()
	{
		return UGroomPluginSettings::StaticClass();
	}
	struct Z_Construct_UClass_UGroomPluginSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroomCacheLookAheadBuffer_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GroomCacheLookAheadBuffer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomPluginSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomPluginSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings for the groom plug-in */" },
		{ "IncludePath", "GroomPluginSettings.h" },
		{ "ModuleRelativePath", "Public/GroomPluginSettings.h" },
		{ "ToolTip", "Settings for the groom plug-in" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomPluginSettings_Statics::NewProp_GroomCacheLookAheadBuffer_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ClampMax", "60.0" },
		{ "ClampMin", "0.05" },
		{ "Comment", "/** The amount of groom cache animation (in seconds) to pre-load ahead of time by the streaming manager */" },
		{ "DisplayName", "Look-Ahead Buffer (in seconds)" },
		{ "ModuleRelativePath", "Public/GroomPluginSettings.h" },
		{ "ToolTip", "The amount of groom cache animation (in seconds) to pre-load ahead of time by the streaming manager" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGroomPluginSettings_Statics::NewProp_GroomCacheLookAheadBuffer = { "GroomCacheLookAheadBuffer", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomPluginSettings, GroomCacheLookAheadBuffer), METADATA_PARAMS(Z_Construct_UClass_UGroomPluginSettings_Statics::NewProp_GroomCacheLookAheadBuffer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomPluginSettings_Statics::NewProp_GroomCacheLookAheadBuffer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomPluginSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomPluginSettings_Statics::NewProp_GroomCacheLookAheadBuffer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomPluginSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomPluginSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomPluginSettings_Statics::ClassParams = {
		&UGroomPluginSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomPluginSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomPluginSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomPluginSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomPluginSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomPluginSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomPluginSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomPluginSettings, 507997126);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomPluginSettings>()
	{
		return UGroomPluginSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomPluginSettings(Z_Construct_UClass_UGroomPluginSettings, &UGroomPluginSettings::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomPluginSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomPluginSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
