// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/Niagara/NiagaraDataInterfacePhysicsAsset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraDataInterfacePhysicsAsset() {}
// Cross Module References
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraDataInterface();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	ENGINE_API UClass* Z_Construct_UClass_UPhysicsAsset_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void UNiagaraDataInterfacePhysicsAsset::StaticRegisterNativesUNiagaraDataInterfacePhysicsAsset()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_NoRegister()
	{
		return UNiagaraDataInterfacePhysicsAsset::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraDataInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::Class_MetaDataParams[] = {
		{ "Category", "Strands" },
		{ "Comment", "/** Data Interface for the strand base */" },
		{ "DisplayName", "Physics Asset" },
		{ "IncludePath", "Niagara/NiagaraDataInterfacePhysicsAsset.h" },
		{ "ModuleRelativePath", "Public/Niagara/NiagaraDataInterfacePhysicsAsset.h" },
		{ "ToolTip", "Data Interface for the strand base" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::NewProp_DefaultSource_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Skeletal Mesh from which the Physics Asset will be found. */" },
		{ "ModuleRelativePath", "Public/Niagara/NiagaraDataInterfacePhysicsAsset.h" },
		{ "ToolTip", "Skeletal Mesh from which the Physics Asset will be found." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::NewProp_DefaultSource = { "DefaultSource", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraDataInterfacePhysicsAsset, DefaultSource), Z_Construct_UClass_UPhysicsAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::NewProp_DefaultSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::NewProp_DefaultSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::NewProp_SourceActor_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** The source actor from which to sample */" },
		{ "ModuleRelativePath", "Public/Niagara/NiagaraDataInterfacePhysicsAsset.h" },
		{ "ToolTip", "The source actor from which to sample" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::NewProp_SourceActor = { "SourceActor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraDataInterfacePhysicsAsset, SourceActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::NewProp_SourceActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::NewProp_SourceActor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::NewProp_DefaultSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::NewProp_SourceActor,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraDataInterfacePhysicsAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::ClassParams = {
		&UNiagaraDataInterfacePhysicsAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraDataInterfacePhysicsAsset, 2501408681);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UNiagaraDataInterfacePhysicsAsset>()
	{
		return UNiagaraDataInterfacePhysicsAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraDataInterfacePhysicsAsset(Z_Construct_UClass_UNiagaraDataInterfacePhysicsAsset, &UNiagaraDataInterfacePhysicsAsset::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UNiagaraDataInterfacePhysicsAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraDataInterfacePhysicsAsset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
