// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomCreateFollicleMaskOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomCreateFollicleMaskOptions() {}
// Cross Module References
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EFollicleMaskChannel();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFollicleMaskOptions();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomAsset_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCreateFollicleMaskOptions_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCreateFollicleMaskOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	static UEnum* EFollicleMaskChannel_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EFollicleMaskChannel, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EFollicleMaskChannel"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EFollicleMaskChannel>()
	{
		return EFollicleMaskChannel_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFollicleMaskChannel(EFollicleMaskChannel_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EFollicleMaskChannel"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EFollicleMaskChannel_Hash() { return 3978165551U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EFollicleMaskChannel()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFollicleMaskChannel"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EFollicleMaskChannel_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFollicleMaskChannel::R", (int64)EFollicleMaskChannel::R },
				{ "EFollicleMaskChannel::G", (int64)EFollicleMaskChannel::G },
				{ "EFollicleMaskChannel::B", (int64)EFollicleMaskChannel::B },
				{ "EFollicleMaskChannel::A", (int64)EFollicleMaskChannel::A },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "A.DisplatName", "Alpha" },
				{ "A.Name", "EFollicleMaskChannel::A" },
				{ "B.DisplatName", "Blue" },
				{ "B.Name", "EFollicleMaskChannel::B" },
				{ "BlueprintType", "true" },
				{ "Comment", "/** List of channel */" },
				{ "G.DisplatName", "Green" },
				{ "G.Name", "EFollicleMaskChannel::G" },
				{ "ModuleRelativePath", "Public/GroomCreateFollicleMaskOptions.h" },
				{ "R.DisplatName", "Red" },
				{ "R.Name", "EFollicleMaskChannel::R" },
				{ "ToolTip", "List of channel" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EFollicleMaskChannel",
				"EFollicleMaskChannel",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FFollicleMaskOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FFollicleMaskOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFollicleMaskOptions, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("FollicleMaskOptions"), sizeof(FFollicleMaskOptions), Get_Z_Construct_UScriptStruct_FFollicleMaskOptions_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FFollicleMaskOptions>()
{
	return FFollicleMaskOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFollicleMaskOptions(FFollicleMaskOptions::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("FollicleMaskOptions"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFFollicleMaskOptions
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFFollicleMaskOptions()
	{
		UScriptStruct::DeferCppStructOps<FFollicleMaskOptions>(FName(TEXT("FollicleMaskOptions")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFFollicleMaskOptions;
	struct Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Groom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Groom;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Channel_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Channel_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Channel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomCreateFollicleMaskOptions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFollicleMaskOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Groom_MetaData[] = {
		{ "Category", "Groom" },
		{ "Comment", "/** Groom asset */" },
		{ "ModuleRelativePath", "Public/GroomCreateFollicleMaskOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Groom asset" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Groom = { "Groom", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFollicleMaskOptions, Groom), Z_Construct_UClass_UGroomAsset_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Groom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Groom_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Channel_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Channel_MetaData[] = {
		{ "Category", "Groom" },
		{ "Comment", "/** Texture channel in which the groom's roots mask will be writtent to. */" },
		{ "ModuleRelativePath", "Public/GroomCreateFollicleMaskOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Texture channel in which the groom's roots mask will be writtent to." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFollicleMaskOptions, Channel), Z_Construct_UEnum_HairStrandsCore_EFollicleMaskChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Channel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Channel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Groom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Channel_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::NewProp_Channel,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"FollicleMaskOptions",
		sizeof(FFollicleMaskOptions),
		alignof(FFollicleMaskOptions),
		Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFollicleMaskOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFollicleMaskOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FollicleMaskOptions"), sizeof(FFollicleMaskOptions), Get_Z_Construct_UScriptStruct_FFollicleMaskOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFollicleMaskOptions_Hash() { return 1837606465U; }
	void UGroomCreateFollicleMaskOptions::StaticRegisterNativesUGroomCreateFollicleMaskOptions()
	{
	}
	UClass* Z_Construct_UClass_UGroomCreateFollicleMaskOptions_NoRegister()
	{
		return UGroomCreateFollicleMaskOptions::StaticClass();
	}
	struct Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Resolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Resolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RootRadius;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Grooms_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Grooms_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Grooms;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Hidden" },
		{ "IncludePath", "GroomCreateFollicleMaskOptions.h" },
		{ "ModuleRelativePath", "Public/GroomCreateFollicleMaskOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Resolution_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Follicle mask texture resolution. The resolution will be rounded to the closest power of two. */" },
		{ "ModuleRelativePath", "Public/GroomCreateFollicleMaskOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Follicle mask texture resolution. The resolution will be rounded to the closest power of two." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Resolution = { "Resolution", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateFollicleMaskOptions, Resolution), METADATA_PARAMS(Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Resolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Resolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_RootRadius_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Size of the root in the follicle mask (in pixels) */" },
		{ "ModuleRelativePath", "Public/GroomCreateFollicleMaskOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Size of the root in the follicle mask (in pixels)" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_RootRadius = { "RootRadius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateFollicleMaskOptions, RootRadius), METADATA_PARAMS(Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_RootRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_RootRadius_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Grooms_Inner = { "Grooms", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFollicleMaskOptions, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Grooms_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Grooms which will be use to create the follicle texture */" },
		{ "ModuleRelativePath", "Public/GroomCreateFollicleMaskOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Grooms which will be use to create the follicle texture" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Grooms = { "Grooms", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateFollicleMaskOptions, Grooms), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Grooms_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Grooms_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Resolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_RootRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Grooms_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::NewProp_Grooms,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomCreateFollicleMaskOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::ClassParams = {
		&UGroomCreateFollicleMaskOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomCreateFollicleMaskOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomCreateFollicleMaskOptions, 1214225439);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomCreateFollicleMaskOptions>()
	{
		return UGroomCreateFollicleMaskOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomCreateFollicleMaskOptions(Z_Construct_UClass_UGroomCreateFollicleMaskOptions, &UGroomCreateFollicleMaskOptions::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomCreateFollicleMaskOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomCreateFollicleMaskOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
