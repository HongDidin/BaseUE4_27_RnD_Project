// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsEditor/Public/GroomBindingFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomBindingFactory() {}
// Cross Module References
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UGroomBindingFactory_NoRegister();
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UGroomBindingFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_HairStrandsEditor();
// End Cross Module References
	void UGroomBindingFactory::StaticRegisterNativesUGroomBindingFactory()
	{
	}
	UClass* Z_Construct_UClass_UGroomBindingFactory_NoRegister()
	{
		return UGroomBindingFactory::StaticClass();
	}
	struct Z_Construct_UClass_UGroomBindingFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomBindingFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GroomBindingFactory.h" },
		{ "ModuleRelativePath", "Public/GroomBindingFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomBindingFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomBindingFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomBindingFactory_Statics::ClassParams = {
		&UGroomBindingFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomBindingFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomBindingFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomBindingFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomBindingFactory, 3155492965);
	template<> HAIRSTRANDSEDITOR_API UClass* StaticClass<UGroomBindingFactory>()
	{
		return UGroomBindingFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomBindingFactory(Z_Construct_UClass_UGroomBindingFactory, &UGroomBindingFactory::StaticClass, TEXT("/Script/HairStrandsEditor"), TEXT("UGroomBindingFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomBindingFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
