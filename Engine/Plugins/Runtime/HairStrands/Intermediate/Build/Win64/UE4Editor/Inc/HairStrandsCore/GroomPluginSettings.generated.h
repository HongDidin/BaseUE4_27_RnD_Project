// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomPluginSettings_generated_h
#error "GroomPluginSettings.generated.h already included, missing '#pragma once' in GroomPluginSettings.h"
#endif
#define HAIRSTRANDSCORE_GroomPluginSettings_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomPluginSettings(); \
	friend struct Z_Construct_UClass_UGroomPluginSettings_Statics; \
public: \
	DECLARE_CLASS(UGroomPluginSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomPluginSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUGroomPluginSettings(); \
	friend struct Z_Construct_UClass_UGroomPluginSettings_Statics; \
public: \
	DECLARE_CLASS(UGroomPluginSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomPluginSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomPluginSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomPluginSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomPluginSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomPluginSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomPluginSettings(UGroomPluginSettings&&); \
	NO_API UGroomPluginSettings(const UGroomPluginSettings&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomPluginSettings(UGroomPluginSettings&&); \
	NO_API UGroomPluginSettings(const UGroomPluginSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomPluginSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomPluginSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGroomPluginSettings)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_11_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class UGroomPluginSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomPluginSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
