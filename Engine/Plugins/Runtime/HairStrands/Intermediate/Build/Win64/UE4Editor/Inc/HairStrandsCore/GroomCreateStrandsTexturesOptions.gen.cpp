// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomCreateStrandsTexturesOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomCreateStrandsTexturesOptions() {}
// Cross Module References
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesMeshType();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesTraceType();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCreateStrandsTexturesOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMesh_NoRegister();
// End Cross Module References
	static UEnum* EStrandsTexturesMeshType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesMeshType, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EStrandsTexturesMeshType"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EStrandsTexturesMeshType>()
	{
		return EStrandsTexturesMeshType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EStrandsTexturesMeshType(EStrandsTexturesMeshType_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EStrandsTexturesMeshType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesMeshType_Hash() { return 467761826U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesMeshType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EStrandsTexturesMeshType"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesMeshType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EStrandsTexturesMeshType::Static", (int64)EStrandsTexturesMeshType::Static },
				{ "EStrandsTexturesMeshType::Skeletal", (int64)EStrandsTexturesMeshType::Skeletal },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Size of each strands*/" },
				{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
				{ "Skeletal.DisplatName", "Use skeletal mesh" },
				{ "Skeletal.Name", "EStrandsTexturesMeshType::Skeletal" },
				{ "Static.DisplatName", "Use static mesh" },
				{ "Static.Name", "EStrandsTexturesMeshType::Static" },
				{ "ToolTip", "Size of each strands" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EStrandsTexturesMeshType",
				"EStrandsTexturesMeshType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EStrandsTexturesTraceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesTraceType, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EStrandsTexturesTraceType"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EStrandsTexturesTraceType>()
	{
		return EStrandsTexturesTraceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EStrandsTexturesTraceType(EStrandsTexturesTraceType_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EStrandsTexturesTraceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesTraceType_Hash() { return 559774481U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesTraceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EStrandsTexturesTraceType"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesTraceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EStrandsTexturesTraceType::TraceInside", (int64)EStrandsTexturesTraceType::TraceInside },
				{ "EStrandsTexturesTraceType::TraceOuside", (int64)EStrandsTexturesTraceType::TraceOuside },
				{ "EStrandsTexturesTraceType::TraceBidirectional", (int64)EStrandsTexturesTraceType::TraceBidirectional },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Size of each strands*/" },
				{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
				{ "ToolTip", "Size of each strands" },
				{ "TraceBidirectional.DisplatName", "Trace both sides" },
				{ "TraceBidirectional.Name", "EStrandsTexturesTraceType::TraceBidirectional" },
				{ "TraceInside.DisplatName", "Trace inside" },
				{ "TraceInside.Name", "EStrandsTexturesTraceType::TraceInside" },
				{ "TraceOuside.DisplatName", "Trace outside" },
				{ "TraceOuside.Name", "EStrandsTexturesTraceType::TraceOuside" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EStrandsTexturesTraceType",
				"EStrandsTexturesTraceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UGroomCreateStrandsTexturesOptions::StaticRegisterNativesUGroomCreateStrandsTexturesOptions()
	{
	}
	UClass* Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_NoRegister()
	{
		return UGroomCreateStrandsTexturesOptions::StaticClass();
	}
	struct Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Resolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Resolution;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TraceType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TraceType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TraceDistance;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MeshType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MeshType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LODIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SectionIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SectionIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVChannelIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UVChannelIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_GroupIndex_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GroupIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Hidden" },
		{ "IncludePath", "GroomCreateStrandsTexturesOptions.h" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_Resolution_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Resolution of the output texture maps (tangent, coverage, ...) */" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Resolution of the output texture maps (tangent, coverage, ...)" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_Resolution = { "Resolution", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateStrandsTexturesOptions, Resolution), METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_Resolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_Resolution_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceType_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Direction in which the tracing will be done: either from the mesh's surface to the outside, or from the mesh's surface to the inside. */" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Direction in which the tracing will be done: either from the mesh's surface to the outside, or from the mesh's surface to the inside." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceType = { "TraceType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateStrandsTexturesOptions, TraceType), Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesTraceType, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceDistance_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Distance from the mesh surface until hair are projected onto the mesh. */" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Distance from the mesh surface until hair are projected onto the mesh." },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceDistance = { "TraceDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateStrandsTexturesOptions, TraceDistance), METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceDistance_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_MeshType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_MeshType_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Select which mesh should be used for tracing */" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Select which mesh should be used for tracing" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_MeshType = { "MeshType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateStrandsTexturesOptions, MeshType), Z_Construct_UEnum_HairStrandsCore_EStrandsTexturesMeshType, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_MeshType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_MeshType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_StaticMesh_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Mesh on which the groom strands will be projected on. If non empty and if the skeletal mesh entry is empty, the static mesh will be used for generating the textures. */" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Mesh on which the groom strands will be projected on. If non empty and if the skeletal mesh entry is empty, the static mesh will be used for generating the textures." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateStrandsTexturesOptions, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_StaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_StaticMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_SkeletalMesh_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Mesh on which the groom strands will be projected on. If non empty, the skeletal mesh will be used for generating the textures.*/" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Mesh on which the groom strands will be projected on. If non empty, the skeletal mesh will be used for generating the textures." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_SkeletalMesh = { "SkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateStrandsTexturesOptions, SkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_SkeletalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_SkeletalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_LODIndex_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** LOD of the mesh, on which the texture projection is done */" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "LOD of the mesh, on which the texture projection is done" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateStrandsTexturesOptions, LODIndex), METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_LODIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_LODIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_SectionIndex_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Section of the mesh, on which the texture projection is done */" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Section of the mesh, on which the texture projection is done" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_SectionIndex = { "SectionIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateStrandsTexturesOptions, SectionIndex), METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_SectionIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_SectionIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_UVChannelIndex_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** UV channel to use */" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "UV channel to use" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_UVChannelIndex = { "UVChannelIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateStrandsTexturesOptions, UVChannelIndex), METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_UVChannelIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_UVChannelIndex_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_GroupIndex_Inner = { "GroupIndex", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_GroupIndex_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Groom index which should be baked into the textures. When the array is empty, all groups will be included (Default). */" },
		{ "ModuleRelativePath", "Public/GroomCreateStrandsTexturesOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Groom index which should be baked into the textures. When the array is empty, all groups will be included (Default)." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_GroupIndex = { "GroupIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateStrandsTexturesOptions, GroupIndex), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_GroupIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_GroupIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_Resolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_TraceDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_MeshType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_MeshType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_SkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_SectionIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_UVChannelIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_GroupIndex_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::NewProp_GroupIndex,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomCreateStrandsTexturesOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::ClassParams = {
		&UGroomCreateStrandsTexturesOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomCreateStrandsTexturesOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomCreateStrandsTexturesOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomCreateStrandsTexturesOptions, 941654549);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomCreateStrandsTexturesOptions>()
	{
		return UGroomCreateStrandsTexturesOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomCreateStrandsTexturesOptions(Z_Construct_UClass_UGroomCreateStrandsTexturesOptions, &UGroomCreateStrandsTexturesOptions::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomCreateStrandsTexturesOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomCreateStrandsTexturesOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
