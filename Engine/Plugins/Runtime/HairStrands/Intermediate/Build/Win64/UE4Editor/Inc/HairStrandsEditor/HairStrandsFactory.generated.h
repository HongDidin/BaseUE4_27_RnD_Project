// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSEDITOR_HairStrandsFactory_generated_h
#error "HairStrandsFactory.generated.h already included, missing '#pragma once' in HairStrandsFactory.h"
#endif
#define HAIRSTRANDSEDITOR_HairStrandsFactory_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHairStrandsFactory(); \
	friend struct Z_Construct_UClass_UHairStrandsFactory_Statics; \
public: \
	DECLARE_CLASS(UHairStrandsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsEditor"), NO_API) \
	DECLARE_SERIALIZER(UHairStrandsFactory)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUHairStrandsFactory(); \
	friend struct Z_Construct_UClass_UHairStrandsFactory_Statics; \
public: \
	DECLARE_CLASS(UHairStrandsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsEditor"), NO_API) \
	DECLARE_SERIALIZER(UHairStrandsFactory)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHairStrandsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHairStrandsFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHairStrandsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHairStrandsFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHairStrandsFactory(UHairStrandsFactory&&); \
	NO_API UHairStrandsFactory(const UHairStrandsFactory&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHairStrandsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHairStrandsFactory(UHairStrandsFactory&&); \
	NO_API UHairStrandsFactory(const UHairStrandsFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHairStrandsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHairStrandsFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHairStrandsFactory)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_15_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class HairStrandsFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSEDITOR_API UClass* StaticClass<class UHairStrandsFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_HairStrandsFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
