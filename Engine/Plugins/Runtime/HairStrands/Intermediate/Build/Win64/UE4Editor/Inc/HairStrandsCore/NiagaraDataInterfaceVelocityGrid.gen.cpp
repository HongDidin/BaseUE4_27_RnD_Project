// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/Niagara/NiagaraDataInterfaceVelocityGrid.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraDataInterfaceVelocityGrid() {}
// Cross Module References
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraDataInterfaceRWBase();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntVector();
// End Cross Module References
	void UNiagaraDataInterfaceVelocityGrid::StaticRegisterNativesUNiagaraDataInterfaceVelocityGrid()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_NoRegister()
	{
		return UNiagaraDataInterfaceVelocityGrid::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GridSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GridSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraDataInterfaceRWBase,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::Class_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "Comment", "/** Data Interface for the strand base */" },
		{ "DisplayName", "Velocity Grid" },
		{ "IncludePath", "Niagara/NiagaraDataInterfaceVelocityGrid.h" },
		{ "ModuleRelativePath", "Public/Niagara/NiagaraDataInterfaceVelocityGrid.h" },
		{ "ToolTip", "Data Interface for the strand base" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::NewProp_GridSize_MetaData[] = {
		{ "Category", "Spawn" },
		{ "Comment", "/** Grid size along the X axis. */" },
		{ "ModuleRelativePath", "Public/Niagara/NiagaraDataInterfaceVelocityGrid.h" },
		{ "ToolTip", "Grid size along the X axis." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::NewProp_GridSize = { "GridSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraDataInterfaceVelocityGrid, GridSize), Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::NewProp_GridSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::NewProp_GridSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::NewProp_GridSize,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraDataInterfaceVelocityGrid>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::ClassParams = {
		&UNiagaraDataInterfaceVelocityGrid::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraDataInterfaceVelocityGrid, 1866946905);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UNiagaraDataInterfaceVelocityGrid>()
	{
		return UNiagaraDataInterfaceVelocityGrid::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraDataInterfaceVelocityGrid(Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid, &UNiagaraDataInterfaceVelocityGrid::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UNiagaraDataInterfaceVelocityGrid"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraDataInterfaceVelocityGrid);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
