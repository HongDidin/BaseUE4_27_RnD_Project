// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomAsset_generated_h
#error "GroomAsset.generated.h already included, missing '#pragma once' in GroomAsset.h"
#endif
#define HAIRSTRANDSCORE_GroomAsset_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_260_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupInfoWithVisibility_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FHairGroupInfo Super;


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupInfoWithVisibility>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_59_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupsMaterial_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupsMaterial>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_35_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupInfo>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UGroomAsset, NO_API)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomAsset(); \
	friend struct Z_Construct_UClass_UGroomAsset_Statics; \
public: \
	DECLARE_CLASS(UGroomAsset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomAsset) \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_ARCHIVESERIALIZER \
	virtual UObject* _getUObject() const override { return const_cast<UGroomAsset*>(this); }


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_INCLASS \
private: \
	static void StaticRegisterNativesUGroomAsset(); \
	friend struct Z_Construct_UClass_UGroomAsset_Statics; \
public: \
	DECLARE_CLASS(UGroomAsset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomAsset) \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_ARCHIVESERIALIZER \
	virtual UObject* _getUObject() const override { return const_cast<UGroomAsset*>(this); }


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomAsset(UGroomAsset&&); \
	NO_API UGroomAsset(const UGroomAsset&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomAsset(UGroomAsset&&); \
	NO_API UGroomAsset(const UGroomAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomAsset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomAsset)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_270_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h_273_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GroomAsset."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class UGroomAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAsset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
