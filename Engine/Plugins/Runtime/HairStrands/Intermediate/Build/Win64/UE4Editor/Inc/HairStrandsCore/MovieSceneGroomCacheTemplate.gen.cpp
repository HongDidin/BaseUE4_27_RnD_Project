// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Private/MovieSceneGroomCacheTemplate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneGroomCacheTemplate() {}
// Cross Module References
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneEvalTemplate();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneGroomCacheParams();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameNumber();
// End Cross Module References

static_assert(std::is_polymorphic<FMovieSceneGroomCacheSectionTemplate>() == std::is_polymorphic<FMovieSceneEvalTemplate>(), "USTRUCT FMovieSceneGroomCacheSectionTemplate cannot be polymorphic unless super FMovieSceneEvalTemplate is polymorphic");

class UScriptStruct* FMovieSceneGroomCacheSectionTemplate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("MovieSceneGroomCacheSectionTemplate"), sizeof(FMovieSceneGroomCacheSectionTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FMovieSceneGroomCacheSectionTemplate>()
{
	return FMovieSceneGroomCacheSectionTemplate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate(FMovieSceneGroomCacheSectionTemplate::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("MovieSceneGroomCacheSectionTemplate"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFMovieSceneGroomCacheSectionTemplate
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFMovieSceneGroomCacheSectionTemplate()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneGroomCacheSectionTemplate>(FName(TEXT("MovieSceneGroomCacheSectionTemplate")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFMovieSceneGroomCacheSectionTemplate;
	struct Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Params_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Params;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/MovieSceneGroomCacheTemplate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneGroomCacheSectionTemplate>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::NewProp_Params_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieSceneGroomCacheTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::NewProp_Params = { "Params", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGroomCacheSectionTemplate, Params), Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::NewProp_Params_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::NewProp_Params_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::NewProp_Params,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		Z_Construct_UScriptStruct_FMovieSceneEvalTemplate,
		&NewStructOps,
		"MovieSceneGroomCacheSectionTemplate",
		sizeof(FMovieSceneGroomCacheSectionTemplate),
		alignof(FMovieSceneGroomCacheSectionTemplate),
		Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneGroomCacheSectionTemplate"), sizeof(FMovieSceneGroomCacheSectionTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplate_Hash() { return 3314623060U; }

static_assert(std::is_polymorphic<FMovieSceneGroomCacheSectionTemplateParameters>() == std::is_polymorphic<FMovieSceneGroomCacheParams>(), "USTRUCT FMovieSceneGroomCacheSectionTemplateParameters cannot be polymorphic unless super FMovieSceneGroomCacheParams is polymorphic");

class UScriptStruct* FMovieSceneGroomCacheSectionTemplateParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("MovieSceneGroomCacheSectionTemplateParameters"), sizeof(FMovieSceneGroomCacheSectionTemplateParameters), Get_Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FMovieSceneGroomCacheSectionTemplateParameters>()
{
	return FMovieSceneGroomCacheSectionTemplateParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters(FMovieSceneGroomCacheSectionTemplateParameters::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("MovieSceneGroomCacheSectionTemplateParameters"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFMovieSceneGroomCacheSectionTemplateParameters
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFMovieSceneGroomCacheSectionTemplateParameters()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneGroomCacheSectionTemplateParameters>(FName(TEXT("MovieSceneGroomCacheSectionTemplateParameters")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFMovieSceneGroomCacheSectionTemplateParameters;
	struct Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SectionStartTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SectionStartTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SectionEndTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SectionEndTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/MovieSceneGroomCacheTemplate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneGroomCacheSectionTemplateParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewProp_SectionStartTime_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieSceneGroomCacheTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewProp_SectionStartTime = { "SectionStartTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGroomCacheSectionTemplateParameters, SectionStartTime), Z_Construct_UScriptStruct_FFrameNumber, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewProp_SectionStartTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewProp_SectionStartTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewProp_SectionEndTime_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieSceneGroomCacheTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewProp_SectionEndTime = { "SectionEndTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneGroomCacheSectionTemplateParameters, SectionEndTime), Z_Construct_UScriptStruct_FFrameNumber, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewProp_SectionEndTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewProp_SectionEndTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewProp_SectionStartTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::NewProp_SectionEndTime,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		Z_Construct_UScriptStruct_FMovieSceneGroomCacheParams,
		&NewStructOps,
		"MovieSceneGroomCacheSectionTemplateParameters",
		sizeof(FMovieSceneGroomCacheSectionTemplateParameters),
		alignof(FMovieSceneGroomCacheSectionTemplateParameters),
		Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneGroomCacheSectionTemplateParameters"), sizeof(FMovieSceneGroomCacheSectionTemplateParameters), Get_Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneGroomCacheSectionTemplateParameters_Hash() { return 3243013563U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
