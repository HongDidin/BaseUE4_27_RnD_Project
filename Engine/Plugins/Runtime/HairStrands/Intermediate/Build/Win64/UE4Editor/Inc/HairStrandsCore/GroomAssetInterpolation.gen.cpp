// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomAssetInterpolation.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomAssetInterpolation() {}
// Cross Module References
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomGeometryType();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EHairLODSelectionType();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EHairInterpolationWeight();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EHairInterpolationQuality();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsLOD();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairLODSettings();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsInterpolation();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairDecimationSettings();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairInterpolationSettings();
// End Cross Module References
	static UEnum* EGroomGeometryType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EGroomGeometryType, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EGroomGeometryType"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomGeometryType>()
	{
		return EGroomGeometryType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroomGeometryType(EGroomGeometryType_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EGroomGeometryType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EGroomGeometryType_Hash() { return 754593981U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomGeometryType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroomGeometryType"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EGroomGeometryType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroomGeometryType::Strands", (int64)EGroomGeometryType::Strands },
				{ "EGroomGeometryType::Cards", (int64)EGroomGeometryType::Cards },
				{ "EGroomGeometryType::Meshes", (int64)EGroomGeometryType::Meshes },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Cards.Name", "EGroomGeometryType::Cards" },
				{ "Meshes.Name", "EGroomGeometryType::Meshes" },
				{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
				{ "Strands.Name", "EGroomGeometryType::Strands" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EGroomGeometryType",
				"EGroomGeometryType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EHairLODSelectionType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EHairLODSelectionType, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EHairLODSelectionType"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairLODSelectionType>()
	{
		return EHairLODSelectionType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHairLODSelectionType(EHairLODSelectionType_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EHairLODSelectionType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EHairLODSelectionType_Hash() { return 1696504583U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EHairLODSelectionType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHairLODSelectionType"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EHairLODSelectionType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHairLODSelectionType::Cpu", (int64)EHairLODSelectionType::Cpu },
				{ "EHairLODSelectionType::Gpu", (int64)EHairLODSelectionType::Gpu },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Cpu.DisplayName", "CPU" },
				{ "Cpu.Name", "EHairLODSelectionType::Cpu" },
				{ "Cpu.ToolTip", "Use CPU information for computing the LOD selection. This used the CPU object bounds, which is not always accurate, especially when the groom is bound to a skeletal mesh." },
				{ "Gpu.DisplayName", "GPU" },
				{ "Gpu.Name", "EHairLODSelectionType::Gpu" },
				{ "Gpu.ToolTip", "Use GPU information for computing the LOD selection. This use GPU hair clusters information which are more precise, but also more expensive to compute" },
				{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EHairLODSelectionType",
				"EHairLODSelectionType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EHairInterpolationWeight_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EHairInterpolationWeight, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EHairInterpolationWeight"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairInterpolationWeight>()
	{
		return EHairInterpolationWeight_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHairInterpolationWeight(EHairInterpolationWeight_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EHairInterpolationWeight"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EHairInterpolationWeight_Hash() { return 836391836U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EHairInterpolationWeight()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHairInterpolationWeight"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EHairInterpolationWeight_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHairInterpolationWeight::Parametric", (int64)EHairInterpolationWeight::Parametric },
				{ "EHairInterpolationWeight::Root", (int64)EHairInterpolationWeight::Root },
				{ "EHairInterpolationWeight::Index", (int64)EHairInterpolationWeight::Index },
				{ "EHairInterpolationWeight::Unknown", (int64)EHairInterpolationWeight::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Index.DisplayName", "Index" },
				{ "Index.Name", "EHairInterpolationWeight::Index" },
				{ "Index.ToolTip", "Build interpolation data based on guide and strands vertex indices" },
				{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
				{ "Parametric.DisplayName", "Parametric" },
				{ "Parametric.Name", "EHairInterpolationWeight::Parametric" },
				{ "Parametric.ToolTip", "Build interpolation data based on curve parametric distance" },
				{ "Root.DisplayName", "Root" },
				{ "Root.Name", "EHairInterpolationWeight::Root" },
				{ "Root.ToolTip", "Build interpolation data based on distance between guide's root and strands's root" },
				{ "Unknown.Hidden", "" },
				{ "Unknown.Name", "EHairInterpolationWeight::Unknown" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EHairInterpolationWeight",
				"EHairInterpolationWeight",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EHairInterpolationQuality_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EHairInterpolationQuality, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EHairInterpolationQuality"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairInterpolationQuality>()
	{
		return EHairInterpolationQuality_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHairInterpolationQuality(EHairInterpolationQuality_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EHairInterpolationQuality"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EHairInterpolationQuality_Hash() { return 2659001883U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EHairInterpolationQuality()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHairInterpolationQuality"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EHairInterpolationQuality_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHairInterpolationQuality::Low", (int64)EHairInterpolationQuality::Low },
				{ "EHairInterpolationQuality::Medium", (int64)EHairInterpolationQuality::Medium },
				{ "EHairInterpolationQuality::High", (int64)EHairInterpolationQuality::High },
				{ "EHairInterpolationQuality::Unknown", (int64)EHairInterpolationQuality::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "High.DisplayName", "High" },
				{ "High.Name", "EHairInterpolationQuality::High" },
				{ "High.ToolTip", "Build interpolation data using curve shape matching search. This result in high quality interpolation data, but is relatively slow to build (can takes several dozen of minutes)" },
				{ "Low.DisplayName", "Low" },
				{ "Low.Name", "EHairInterpolationQuality::Low" },
				{ "Low.ToolTip", "Build interpolation data based on nearst neighbor search. Low quality interpolation data, but fast to build (takes a few minutes)" },
				{ "Medium.DisplayName", "Medium" },
				{ "Medium.Name", "EHairInterpolationQuality::Medium" },
				{ "Medium.ToolTip", "Build interpolation data using curve shape matching search but within a limited spatial range. This is a tradeoff between Low and high quality in term of quality & build time (can takes several dozen of minutes)" },
				{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
				{ "Unknown.Hidden", "" },
				{ "Unknown.Name", "EHairInterpolationQuality::Unknown" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EHairInterpolationQuality",
				"EHairInterpolationQuality",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FHairGroupsLOD::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupsLOD_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupsLOD, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupsLOD"), sizeof(FHairGroupsLOD), Get_Z_Construct_UScriptStruct_FHairGroupsLOD_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupsLOD>()
{
	return FHairGroupsLOD::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupsLOD(FHairGroupsLOD::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupsLOD"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsLOD
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsLOD()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupsLOD>(FName(TEXT("HairGroupsLOD")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsLOD;
	struct Z_Construct_UScriptStruct_FHairGroupsLOD_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LODs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LODs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LODs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterWorldSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterWorldSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterScreenSizeScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterScreenSizeScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupsLOD>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_LODs_Inner = { "LODs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairLODSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_LODs_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Reduce the number of hair strands in a uniform manner" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_LODs = { "LODs", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsLOD, LODs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_LODs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_LODs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_ClusterWorldSize_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "If enable this LOD version will use cards representation" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_ClusterWorldSize = { "ClusterWorldSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsLOD, ClusterWorldSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_ClusterWorldSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_ClusterWorldSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_ClusterScreenSizeScale_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "If enable this LOD version will use cards representation" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_ClusterScreenSizeScale = { "ClusterScreenSizeScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsLOD, ClusterScreenSizeScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_ClusterScreenSizeScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_ClusterScreenSizeScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_LODs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_LODs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_ClusterWorldSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::NewProp_ClusterScreenSizeScale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGroupsLOD",
		sizeof(FHairGroupsLOD),
		alignof(FHairGroupsLOD),
		Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsLOD()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupsLOD_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupsLOD"), sizeof(FHairGroupsLOD), Get_Z_Construct_UScriptStruct_FHairGroupsLOD_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupsLOD_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupsLOD_Hash() { return 789401608U; }
class UScriptStruct* FHairGroupsInterpolation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupsInterpolation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupsInterpolation, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupsInterpolation"), sizeof(FHairGroupsInterpolation), Get_Z_Construct_UScriptStruct_FHairGroupsInterpolation_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupsInterpolation>()
{
	return FHairGroupsInterpolation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupsInterpolation(FHairGroupsInterpolation::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupsInterpolation"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsInterpolation
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsInterpolation()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupsInterpolation>(FName(TEXT("HairGroupsInterpolation")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsInterpolation;
	struct Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DecimationSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DecimationSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InterpolationSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupsInterpolation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewProp_DecimationSettings_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Decimation settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewProp_DecimationSettings = { "DecimationSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsInterpolation, DecimationSettings), Z_Construct_UScriptStruct_FHairDecimationSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewProp_DecimationSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewProp_DecimationSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewProp_InterpolationSettings_MetaData[] = {
		{ "Category", "InterpolationSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Interpolation settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewProp_InterpolationSettings = { "InterpolationSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsInterpolation, InterpolationSettings), Z_Construct_UScriptStruct_FHairInterpolationSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewProp_InterpolationSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewProp_InterpolationSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewProp_DecimationSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::NewProp_InterpolationSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGroupsInterpolation",
		sizeof(FHairGroupsInterpolation),
		alignof(FHairGroupsInterpolation),
		Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsInterpolation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupsInterpolation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupsInterpolation"), sizeof(FHairGroupsInterpolation), Get_Z_Construct_UScriptStruct_FHairGroupsInterpolation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupsInterpolation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupsInterpolation_Hash() { return 3246717314U; }
class UScriptStruct* FHairInterpolationSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairInterpolationSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairInterpolationSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairInterpolationSettings"), sizeof(FHairInterpolationSettings), Get_Z_Construct_UScriptStruct_FHairInterpolationSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairInterpolationSettings>()
{
	return FHairInterpolationSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairInterpolationSettings(FHairInterpolationSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairInterpolationSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairInterpolationSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairInterpolationSettings()
	{
		UScriptStruct::DeferCppStructOps<FHairInterpolationSettings>(FName(TEXT("HairInterpolationSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairInterpolationSettings;
	struct Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideGuides_MetaData[];
#endif
		static void NewProp_bOverrideGuides_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideGuides;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairToGuideDensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HairToGuideDensity;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InterpolationQuality_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationQuality_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InterpolationQuality;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InterpolationDistance_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InterpolationDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeGuide_MetaData[];
#endif
		static void NewProp_bRandomizeGuide_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeGuide;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseUniqueGuide_MetaData[];
#endif
		static void NewProp_bUseUniqueGuide_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseUniqueGuide;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairInterpolationSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bOverrideGuides_MetaData[] = {
		{ "Category", "InterpolationSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "If checked, override imported guides with generated ones." },
	};
#endif
	void Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bOverrideGuides_SetBit(void* Obj)
	{
		((FHairInterpolationSettings*)Obj)->bOverrideGuides = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bOverrideGuides = { "bOverrideGuides", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairInterpolationSettings), &Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bOverrideGuides_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bOverrideGuides_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bOverrideGuides_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_HairToGuideDensity_MetaData[] = {
		{ "Category", "InterpolationSettings" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Density factor for converting hair into guide curve if no guides are provided. */" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Density factor for converting hair into guide curve if no guides are provided." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_HairToGuideDensity = { "HairToGuideDensity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairInterpolationSettings, HairToGuideDensity), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_HairToGuideDensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_HairToGuideDensity_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationQuality_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationQuality_MetaData[] = {
		{ "Category", "InterpolationSettings" },
		{ "Comment", "/** Interpolation data quality. */" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Interpolation data quality." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationQuality = { "InterpolationQuality", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairInterpolationSettings, InterpolationQuality), Z_Construct_UEnum_HairStrandsCore_EHairInterpolationQuality, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationQuality_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationQuality_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationDistance_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationDistance_MetaData[] = {
		{ "Category", "InterpolationSettings" },
		{ "Comment", "/** Interpolation distance metric. */" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Interpolation distance metric." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationDistance = { "InterpolationDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairInterpolationSettings, InterpolationDistance), Z_Construct_UEnum_HairStrandsCore_EHairInterpolationWeight, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bRandomizeGuide_MetaData[] = {
		{ "Category", "InterpolationSettings" },
		{ "Comment", "/** Randomize which guides affect a given hair strand. */" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Randomize which guides affect a given hair strand." },
	};
#endif
	void Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bRandomizeGuide_SetBit(void* Obj)
	{
		((FHairInterpolationSettings*)Obj)->bRandomizeGuide = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bRandomizeGuide = { "bRandomizeGuide", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairInterpolationSettings), &Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bRandomizeGuide_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bRandomizeGuide_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bRandomizeGuide_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bUseUniqueGuide_MetaData[] = {
		{ "Category", "InterpolationSettings" },
		{ "Comment", "/** Force a hair strand to be affected by a unique guide. */" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Force a hair strand to be affected by a unique guide." },
	};
#endif
	void Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bUseUniqueGuide_SetBit(void* Obj)
	{
		((FHairInterpolationSettings*)Obj)->bUseUniqueGuide = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bUseUniqueGuide = { "bUseUniqueGuide", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairInterpolationSettings), &Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bUseUniqueGuide_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bUseUniqueGuide_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bUseUniqueGuide_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bOverrideGuides,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_HairToGuideDensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationQuality_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationQuality,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationDistance_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_InterpolationDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bRandomizeGuide,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::NewProp_bUseUniqueGuide,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairInterpolationSettings",
		sizeof(FHairInterpolationSettings),
		alignof(FHairInterpolationSettings),
		Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairInterpolationSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairInterpolationSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairInterpolationSettings"), sizeof(FHairInterpolationSettings), Get_Z_Construct_UScriptStruct_FHairInterpolationSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairInterpolationSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairInterpolationSettings_Hash() { return 1767118990U; }
class UScriptStruct* FHairDecimationSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairDecimationSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairDecimationSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairDecimationSettings"), sizeof(FHairDecimationSettings), Get_Z_Construct_UScriptStruct_FHairDecimationSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairDecimationSettings>()
{
	return FHairDecimationSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairDecimationSettings(FHairDecimationSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairDecimationSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairDecimationSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairDecimationSettings()
	{
		UScriptStruct::DeferCppStructOps<FHairDecimationSettings>(FName(TEXT("HairDecimationSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairDecimationSettings;
	struct Z_Construct_UScriptStruct_FHairDecimationSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveDecimation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurveDecimation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VertexDecimation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VertexDecimation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairDecimationSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewProp_CurveDecimation_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Reduce the number of hair strands in a uniform manner" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewProp_CurveDecimation = { "CurveDecimation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairDecimationSettings, CurveDecimation), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewProp_CurveDecimation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewProp_CurveDecimation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewProp_VertexDecimation_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Reduce the number of verties for each hair strands in a uniform manner" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewProp_VertexDecimation = { "VertexDecimation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairDecimationSettings, VertexDecimation), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewProp_VertexDecimation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewProp_VertexDecimation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewProp_CurveDecimation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::NewProp_VertexDecimation,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairDecimationSettings",
		sizeof(FHairDecimationSettings),
		alignof(FHairDecimationSettings),
		Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairDecimationSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairDecimationSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairDecimationSettings"), sizeof(FHairDecimationSettings), Get_Z_Construct_UScriptStruct_FHairDecimationSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairDecimationSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairDecimationSettings_Hash() { return 711144525U; }
class UScriptStruct* FHairLODSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairLODSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairLODSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairLODSettings"), sizeof(FHairLODSettings), Get_Z_Construct_UScriptStruct_FHairLODSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairLODSettings>()
{
	return FHairLODSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairLODSettings(FHairLODSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairLODSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairLODSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairLODSettings()
	{
		UScriptStruct::DeferCppStructOps<FHairLODSettings>(FName(TEXT("HairLODSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairLODSettings;
	struct Z_Construct_UScriptStruct_FHairLODSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveDecimation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurveDecimation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VertexDecimation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VertexDecimation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngularThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngularThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThicknessScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ThicknessScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bVisible_MetaData[];
#endif
		static void NewProp_bVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bVisible;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_GeometryType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeometryType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_GeometryType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairLODSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairLODSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_CurveDecimation_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Reduce the number of hair strands in a uniform manner" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_CurveDecimation = { "CurveDecimation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairLODSettings, CurveDecimation), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_CurveDecimation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_CurveDecimation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_VertexDecimation_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Reduce the number of vertices per strands in a uniform manner" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_VertexDecimation = { "VertexDecimation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairLODSettings, VertexDecimation), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_VertexDecimation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_VertexDecimation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_AngularThreshold_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMax", "45" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Reduce the number of vertices per strands in a uniform manner" },
		{ "UIMax", "45.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_AngularThreshold = { "AngularThreshold", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairLODSettings, AngularThreshold), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_AngularThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_AngularThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_ScreenSize_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Reduce the number of hair strands in a uniform manner" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_ScreenSize = { "ScreenSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairLODSettings, ScreenSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_ScreenSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_ScreenSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_ThicknessScale_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "Reduce the number of hair strands in a uniform manner" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_ThicknessScale = { "ThicknessScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairLODSettings, ThicknessScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_ThicknessScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_ThicknessScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_bVisible_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "If disable, the hair strands won't be rendered" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_bVisible_SetBit(void* Obj)
	{
		((FHairLODSettings*)Obj)->bVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_bVisible = { "bVisible", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairLODSettings), &Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_bVisible_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_bVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_bVisible_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_GeometryType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_GeometryType_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetInterpolation.h" },
		{ "ToolTip", "If enable this LOD version will use cards representation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_GeometryType = { "GeometryType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairLODSettings, GeometryType), Z_Construct_UEnum_HairStrandsCore_EGroomGeometryType, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_GeometryType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_GeometryType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairLODSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_CurveDecimation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_VertexDecimation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_AngularThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_ScreenSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_ThicknessScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_bVisible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_GeometryType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairLODSettings_Statics::NewProp_GeometryType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairLODSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairLODSettings",
		sizeof(FHairLODSettings),
		alignof(FHairLODSettings),
		Z_Construct_UScriptStruct_FHairLODSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairLODSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairLODSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairLODSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairLODSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairLODSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairLODSettings"), sizeof(FHairLODSettings), Get_Z_Construct_UScriptStruct_FHairLODSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairLODSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairLODSettings_Hash() { return 2322195592U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
