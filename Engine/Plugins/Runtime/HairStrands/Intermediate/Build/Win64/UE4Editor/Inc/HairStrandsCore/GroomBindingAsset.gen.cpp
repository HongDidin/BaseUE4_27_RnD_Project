// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomBindingAsset.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomBindingAsset() {}
// Cross Module References
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomBindingMeshType();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FGoomBindingGroupInfo();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomBindingAsset_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomBindingAsset();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomAsset_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMesh_NoRegister();
	GEOMETRYCACHE_API UClass* Z_Construct_UClass_UGeometryCache_NoRegister();
// End Cross Module References
	static UEnum* EGroomBindingMeshType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EGroomBindingMeshType, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EGroomBindingMeshType"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomBindingMeshType>()
	{
		return EGroomBindingMeshType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroomBindingMeshType(EGroomBindingMeshType_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EGroomBindingMeshType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EGroomBindingMeshType_Hash() { return 541411424U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomBindingMeshType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroomBindingMeshType"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EGroomBindingMeshType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroomBindingMeshType::SkeletalMesh", (int64)EGroomBindingMeshType::SkeletalMesh },
				{ "EGroomBindingMeshType::GeometryCache", (int64)EGroomBindingMeshType::GeometryCache },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Enum that describes the type of mesh to bind to */" },
				{ "GeometryCache.Name", "EGroomBindingMeshType::GeometryCache" },
				{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
				{ "SkeletalMesh.Name", "EGroomBindingMeshType::SkeletalMesh" },
				{ "ToolTip", "Enum that describes the type of mesh to bind to" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EGroomBindingMeshType",
				"EGroomBindingMeshType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FGoomBindingGroupInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGoomBindingGroupInfo, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("GoomBindingGroupInfo"), sizeof(FGoomBindingGroupInfo), Get_Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FGoomBindingGroupInfo>()
{
	return FGoomBindingGroupInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGoomBindingGroupInfo(FGoomBindingGroupInfo::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("GoomBindingGroupInfo"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFGoomBindingGroupInfo
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFGoomBindingGroupInfo()
	{
		UScriptStruct::DeferCppStructOps<FGoomBindingGroupInfo>(FName(TEXT("GoomBindingGroupInfo")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFGoomBindingGroupInfo;
	struct Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenRootCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RenRootCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenLODCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RenLODCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimRootCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SimRootCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimLODCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SimLODCount;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGoomBindingGroupInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_RenRootCount_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Curve Count" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_RenRootCount = { "RenRootCount", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoomBindingGroupInfo, RenRootCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_RenRootCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_RenRootCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_RenLODCount_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Curve LOD" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_RenLODCount = { "RenLODCount", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoomBindingGroupInfo, RenLODCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_RenLODCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_RenLODCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_SimRootCount_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Guide Count" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_SimRootCount = { "SimRootCount", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoomBindingGroupInfo, SimRootCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_SimRootCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_SimRootCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_SimLODCount_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Guide LOD" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_SimLODCount = { "SimLODCount", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoomBindingGroupInfo, SimLODCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_SimLODCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_SimLODCount_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_RenRootCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_RenLODCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_SimRootCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::NewProp_SimLODCount,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"GoomBindingGroupInfo",
		sizeof(FGoomBindingGroupInfo),
		alignof(FGoomBindingGroupInfo),
		Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGoomBindingGroupInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GoomBindingGroupInfo"), sizeof(FGoomBindingGroupInfo), Get_Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGoomBindingGroupInfo_Hash() { return 3841779857U; }
	void UGroomBindingAsset::StaticRegisterNativesUGroomBindingAsset()
	{
	}
	UClass* Z_Construct_UClass_UGroomBindingAsset_NoRegister()
	{
		return UGroomBindingAsset::StaticClass();
	}
	struct Z_Construct_UClass_UGroomBindingAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_GroomBindingType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroomBindingType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_GroomBindingType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Groom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Groom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceSkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceSkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetSkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetSkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceGeometryCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceGeometryCache;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetGeometryCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetGeometryCache;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumInterpolationPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumInterpolationPoints;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MatchingSection_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MatchingSection;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GroupInfos_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupInfos_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GroupInfos;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomBindingAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingAsset_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Implements an asset that can be used to store binding information between a groom and a skeletal mesh\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "GroomBindingAsset.h" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
		{ "ToolTip", "Implements an asset that can be used to store binding information between a groom and a skeletal mesh" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroomBindingType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroomBindingType_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Type of mesh to create groom binding for */" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
		{ "ToolTip", "Type of mesh to create groom binding for" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroomBindingType = { "GroomBindingType", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomBindingAsset, GroomBindingType), Z_Construct_UEnum_HairStrandsCore_EGroomBindingMeshType, METADATA_PARAMS(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroomBindingType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroomBindingType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_Groom_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Groom to bind. */" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
		{ "ToolTip", "Groom to bind." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_Groom = { "Groom", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomBindingAsset, Groom), Z_Construct_UClass_UGroomAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_Groom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_Groom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_SourceSkeletalMesh_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Skeletal mesh on which the groom has been authored. This is optional, and used only if the hair\n\x09\x09""binding is done a different mesh than the one which it has been authored */" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
		{ "ToolTip", "Skeletal mesh on which the groom has been authored. This is optional, and used only if the hair\n              binding is done a different mesh than the one which it has been authored" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_SourceSkeletalMesh = { "SourceSkeletalMesh", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomBindingAsset, SourceSkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_SourceSkeletalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_SourceSkeletalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_TargetSkeletalMesh_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Skeletal mesh on which the groom is attached to. */" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
		{ "ToolTip", "Skeletal mesh on which the groom is attached to." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_TargetSkeletalMesh = { "TargetSkeletalMesh", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomBindingAsset, TargetSkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_TargetSkeletalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_TargetSkeletalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_SourceGeometryCache_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_SourceGeometryCache = { "SourceGeometryCache", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomBindingAsset, SourceGeometryCache), Z_Construct_UClass_UGeometryCache_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_SourceGeometryCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_SourceGeometryCache_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_TargetGeometryCache_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_TargetGeometryCache = { "TargetGeometryCache", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomBindingAsset, TargetGeometryCache), Z_Construct_UClass_UGeometryCache_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_TargetGeometryCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_TargetGeometryCache_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_NumInterpolationPoints_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Number of points used for the rbf interpolation */" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
		{ "ToolTip", "Number of points used for the rbf interpolation" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_NumInterpolationPoints = { "NumInterpolationPoints", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomBindingAsset, NumInterpolationPoints), METADATA_PARAMS(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_NumInterpolationPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_NumInterpolationPoints_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_MatchingSection_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Number of points used for the rbf interpolation */" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
		{ "ToolTip", "Number of points used for the rbf interpolation" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_MatchingSection = { "MatchingSection", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomBindingAsset, MatchingSection), METADATA_PARAMS(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_MatchingSection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_MatchingSection_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroupInfos_Inner = { "GroupInfos", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGoomBindingGroupInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroupInfos_MetaData[] = {
		{ "Category", "HairGroups" },
		{ "DisplayName", "Group" },
		{ "ModuleRelativePath", "Public/GroomBindingAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroupInfos = { "GroupInfos", nullptr, (EPropertyFlags)0x0010000000000045, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomBindingAsset, GroupInfos), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroupInfos_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroupInfos_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomBindingAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroomBindingType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroomBindingType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_Groom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_SourceSkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_TargetSkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_SourceGeometryCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_TargetGeometryCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_NumInterpolationPoints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_MatchingSection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroupInfos_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomBindingAsset_Statics::NewProp_GroupInfos,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomBindingAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomBindingAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomBindingAsset_Statics::ClassParams = {
		&UGroomBindingAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomBindingAsset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomBindingAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomBindingAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomBindingAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomBindingAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomBindingAsset, 1026420586);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomBindingAsset>()
	{
		return UGroomBindingAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomBindingAsset(Z_Construct_UClass_UGroomBindingAsset, &UGroomBindingAsset::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomBindingAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomBindingAsset);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UGroomBindingAsset)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
