// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomAssetImportData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomAssetImportData() {}
// Cross Module References
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomAssetImportData_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomAssetImportData();
	ENGINE_API UClass* Z_Construct_UClass_UAssetImportData();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomImportOptions_NoRegister();
// End Cross Module References
	void UGroomAssetImportData::StaticRegisterNativesUGroomAssetImportData()
	{
	}
	UClass* Z_Construct_UClass_UGroomAssetImportData_NoRegister()
	{
		return UGroomAssetImportData::StaticClass();
	}
	struct Z_Construct_UClass_UGroomAssetImportData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomAssetImportData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetImportData,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAssetImportData_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GroomAssetImportData.h" },
		{ "ModuleRelativePath", "Public/GroomAssetImportData.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomAssetImportData_Statics::NewProp_ImportOptions_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomAssetImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomAssetImportData_Statics::NewProp_ImportOptions = { "ImportOptions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomAssetImportData, ImportOptions), Z_Construct_UClass_UGroomImportOptions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomAssetImportData_Statics::NewProp_ImportOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAssetImportData_Statics::NewProp_ImportOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomAssetImportData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomAssetImportData_Statics::NewProp_ImportOptions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomAssetImportData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomAssetImportData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomAssetImportData_Statics::ClassParams = {
		&UGroomAssetImportData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomAssetImportData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAssetImportData_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomAssetImportData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomAssetImportData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomAssetImportData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomAssetImportData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomAssetImportData, 3328105655);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomAssetImportData>()
	{
		return UGroomAssetImportData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomAssetImportData(Z_Construct_UClass_UGroomAssetImportData, &UGroomAssetImportData::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomAssetImportData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomAssetImportData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
