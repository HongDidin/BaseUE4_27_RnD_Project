// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomAssetRendering_generated_h
#error "GroomAssetRendering.generated.h already included, missing '#pragma once' in GroomAssetRendering.h"
#endif
#define HAIRSTRANDSCORE_GroomAssetRendering_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetRendering_h_87_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupsRendering_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupsRendering>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetRendering_h_69_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairAdvancedRenderingSettings>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetRendering_h_43_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairShadowSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairShadowSettings>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetRendering_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGeometrySettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGeometrySettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetRendering_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
