// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomCacheImportOptions_generated_h
#error "GroomCacheImportOptions.generated.h already included, missing '#pragma once' in GroomCacheImportOptions.h"
#endif
#define HAIRSTRANDSCORE_GroomCacheImportOptions_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FGroomCacheImportSettings>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomCacheImportOptions(); \
	friend struct Z_Construct_UClass_UGroomCacheImportOptions_Statics; \
public: \
	DECLARE_CLASS(UGroomCacheImportOptions, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomCacheImportOptions)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUGroomCacheImportOptions(); \
	friend struct Z_Construct_UClass_UGroomCacheImportOptions_Statics; \
public: \
	DECLARE_CLASS(UGroomCacheImportOptions, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomCacheImportOptions)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomCacheImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomCacheImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomCacheImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomCacheImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomCacheImportOptions(UGroomCacheImportOptions&&); \
	NO_API UGroomCacheImportOptions(const UGroomCacheImportOptions&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomCacheImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomCacheImportOptions(UGroomCacheImportOptions&&); \
	NO_API UGroomCacheImportOptions(const UGroomCacheImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomCacheImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomCacheImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomCacheImportOptions)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_27_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_30_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class UGroomCacheImportOptions>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomCacheImportData(); \
	friend struct Z_Construct_UClass_UGroomCacheImportData_Statics; \
public: \
	DECLARE_CLASS(UGroomCacheImportData, UAssetImportData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomCacheImportData)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_INCLASS \
private: \
	static void StaticRegisterNativesUGroomCacheImportData(); \
	friend struct Z_Construct_UClass_UGroomCacheImportData_Statics; \
public: \
	DECLARE_CLASS(UGroomCacheImportData, UAssetImportData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomCacheImportData)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomCacheImportData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomCacheImportData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomCacheImportData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomCacheImportData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomCacheImportData(UGroomCacheImportData&&); \
	NO_API UGroomCacheImportData(const UGroomCacheImportData&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomCacheImportData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomCacheImportData(UGroomCacheImportData&&); \
	NO_API UGroomCacheImportData(const UGroomCacheImportData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomCacheImportData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomCacheImportData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomCacheImportData)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_39_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h_42_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class UGroomCacheImportData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheImportOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
