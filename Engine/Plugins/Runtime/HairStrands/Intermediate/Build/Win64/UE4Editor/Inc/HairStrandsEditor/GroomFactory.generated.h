// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSEDITOR_GroomFactory_generated_h
#error "GroomFactory.generated.h already included, missing '#pragma once' in GroomFactory.h"
#endif
#define HAIRSTRANDSEDITOR_GroomFactory_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomFactory(); \
	friend struct Z_Construct_UClass_UGroomFactory_Statics; \
public: \
	DECLARE_CLASS(UGroomFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsEditor"), HAIRSTRANDSEDITOR_API) \
	DECLARE_SERIALIZER(UGroomFactory)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUGroomFactory(); \
	friend struct Z_Construct_UClass_UGroomFactory_Statics; \
public: \
	DECLARE_CLASS(UGroomFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsEditor"), HAIRSTRANDSEDITOR_API) \
	DECLARE_SERIALIZER(UGroomFactory)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	HAIRSTRANDSEDITOR_API UGroomFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(HAIRSTRANDSEDITOR_API, UGroomFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	HAIRSTRANDSEDITOR_API UGroomFactory(UGroomFactory&&); \
	HAIRSTRANDSEDITOR_API UGroomFactory(const UGroomFactory&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	HAIRSTRANDSEDITOR_API UGroomFactory(UGroomFactory&&); \
	HAIRSTRANDSEDITOR_API UGroomFactory(const UGroomFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(HAIRSTRANDSEDITOR_API, UGroomFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGroomFactory)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_11_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSEDITOR_API UClass* StaticClass<class UGroomFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsEditor_Public_GroomFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
