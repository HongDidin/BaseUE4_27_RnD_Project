// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsEditor/Public/HairStrandsFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHairStrandsFactory() {}
// Cross Module References
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UHairStrandsFactory_NoRegister();
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UHairStrandsFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_HairStrandsEditor();
// End Cross Module References
	void UHairStrandsFactory::StaticRegisterNativesUHairStrandsFactory()
	{
	}
	UClass* Z_Construct_UClass_UHairStrandsFactory_NoRegister()
	{
		return UHairStrandsFactory::StaticClass();
	}
	struct Z_Construct_UClass_UHairStrandsFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHairStrandsFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHairStrandsFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UHairStrands objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "HairStrandsFactory.h" },
		{ "ModuleRelativePath", "Public/HairStrandsFactory.h" },
		{ "ToolTip", "Implements a factory for UHairStrands objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHairStrandsFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHairStrandsFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHairStrandsFactory_Statics::ClassParams = {
		&UHairStrandsFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UHairStrandsFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHairStrandsFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHairStrandsFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHairStrandsFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHairStrandsFactory, 1472583738);
	template<> HAIRSTRANDSEDITOR_API UClass* StaticClass<UHairStrandsFactory>()
	{
		return UHairStrandsFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHairStrandsFactory(Z_Construct_UClass_UHairStrandsFactory, &UHairStrandsFactory::StaticClass, TEXT("/Script/HairStrandsEditor"), TEXT("UHairStrandsFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHairStrandsFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
