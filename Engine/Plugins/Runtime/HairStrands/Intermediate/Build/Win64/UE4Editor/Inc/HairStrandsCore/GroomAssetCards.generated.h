// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomAssetCards_generated_h
#error "GroomAssetCards.generated.h already included, missing '#pragma once' in GroomAssetCards.h"
#endif
#define HAIRSTRANDSCORE_GroomAssetCards_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetCards_h_190_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupsCardsSourceDescription>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetCards_h_167_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupCardsTextures>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetCards_h_155_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupCardsInfo>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetCards_h_122_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupsProceduralCards>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetCards_h_95_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairCardsTextureSettings>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetCards_h_56_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairCardsGeometrySettings>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetCards_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairCardsClusterSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetCards_h


#define FOREACH_ENUM_EHAIRCARDSSOURCETYPE(op) \
	op(EHairCardsSourceType::Procedural) \
	op(EHairCardsSourceType::Imported) 

enum class EHairCardsSourceType : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairCardsSourceType>();

#define FOREACH_ENUM_EHAIRCARDSGENERATIONTYPE(op) \
	op(EHairCardsGenerationType::CardsCount) \
	op(EHairCardsGenerationType::UseGuides) 

enum class EHairCardsGenerationType : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairCardsGenerationType>();

#define FOREACH_ENUM_EHAIRCARDSCLUSTERTYPE(op) \
	op(EHairCardsClusterType::Low) \
	op(EHairCardsClusterType::High) 

enum class EHairCardsClusterType : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairCardsClusterType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
