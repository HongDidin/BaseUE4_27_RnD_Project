// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsEditor/Public/GroomFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomFactory() {}
// Cross Module References
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UGroomFactory_NoRegister();
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UGroomFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_HairStrandsEditor();
// End Cross Module References
	void UGroomFactory::StaticRegisterNativesUGroomFactory()
	{
	}
	UClass* Z_Construct_UClass_UGroomFactory_NoRegister()
	{
		return UGroomFactory::StaticClass();
	}
	struct Z_Construct_UClass_UGroomFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GroomFactory.h" },
		{ "ModuleRelativePath", "Public/GroomFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomFactory_Statics::ClassParams = {
		&UGroomFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomFactory, 1420353621);
	template<> HAIRSTRANDSEDITOR_API UClass* StaticClass<UGroomFactory>()
	{
		return UGroomFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomFactory(Z_Construct_UClass_UGroomFactory, &UGroomFactory::StaticClass, TEXT("/Script/HairStrandsEditor"), TEXT("UGroomFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
