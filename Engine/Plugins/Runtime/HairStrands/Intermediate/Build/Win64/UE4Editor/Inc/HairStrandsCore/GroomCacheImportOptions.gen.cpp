// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomCacheImportOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomCacheImportOptions() {}
// Cross Module References
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FGroomCacheImportSettings();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCacheImportOptions_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCacheImportOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCacheImportData_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCacheImportData();
	ENGINE_API UClass* Z_Construct_UClass_UAssetImportData();
// End Cross Module References
class UScriptStruct* FGroomCacheImportSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FGroomCacheImportSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGroomCacheImportSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("GroomCacheImportSettings"), sizeof(FGroomCacheImportSettings), Get_Z_Construct_UScriptStruct_FGroomCacheImportSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FGroomCacheImportSettings>()
{
	return FGroomCacheImportSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGroomCacheImportSettings(FGroomCacheImportSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("GroomCacheImportSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomCacheImportSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomCacheImportSettings()
	{
		UScriptStruct::DeferCppStructOps<FGroomCacheImportSettings>(FName(TEXT("GroomCacheImportSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomCacheImportSettings;
	struct Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportGroomCache_MetaData[];
#endif
		static void NewProp_bImportGroomCache_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportGroomCache;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportGroomAsset_MetaData[];
#endif
		static void NewProp_bImportGroomAsset_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportGroomAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroomAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GroomAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomCacheImportOptions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGroomCacheImportSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomCache_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "Comment", "/** Import the animated groom that was detected in this file */" },
		{ "ModuleRelativePath", "Public/GroomCacheImportOptions.h" },
		{ "ToolTip", "Import the animated groom that was detected in this file" },
	};
#endif
	void Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomCache_SetBit(void* Obj)
	{
		((FGroomCacheImportSettings*)Obj)->bImportGroomCache = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomCache = { "bImportGroomCache", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FGroomCacheImportSettings), &Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomCache_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomCache_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomAsset_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "Comment", "/** Import or re-import the groom asset from this file */" },
		{ "ModuleRelativePath", "Public/GroomCacheImportOptions.h" },
		{ "ToolTip", "Import or re-import the groom asset from this file" },
	};
#endif
	void Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomAsset_SetBit(void* Obj)
	{
		((FGroomCacheImportSettings*)Obj)->bImportGroomAsset = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomAsset = { "bImportGroomAsset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FGroomCacheImportSettings), &Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomAsset_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_GroomAsset_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "Comment", "/** The groom asset the groom cache will be built from (must be compatible) */" },
		{ "MetaClass", "GroomAsset" },
		{ "ModuleRelativePath", "Public/GroomCacheImportOptions.h" },
		{ "ToolTip", "The groom asset the groom cache will be built from (must be compatible)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_GroomAsset = { "GroomAsset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomCacheImportSettings, GroomAsset), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_GroomAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_GroomAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_bImportGroomAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::NewProp_GroomAsset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"GroomCacheImportSettings",
		sizeof(FGroomCacheImportSettings),
		alignof(FGroomCacheImportSettings),
		Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGroomCacheImportSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGroomCacheImportSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GroomCacheImportSettings"), sizeof(FGroomCacheImportSettings), Get_Z_Construct_UScriptStruct_FGroomCacheImportSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGroomCacheImportSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGroomCacheImportSettings_Hash() { return 3258391936U; }
	void UGroomCacheImportOptions::StaticRegisterNativesUGroomCacheImportOptions()
	{
	}
	UClass* Z_Construct_UClass_UGroomCacheImportOptions_NoRegister()
	{
		return UGroomCacheImportOptions::StaticClass();
	}
	struct Z_Construct_UClass_UGroomCacheImportOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImportSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomCacheImportOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCacheImportOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "GroomCacheImportOptions.h" },
		{ "ModuleRelativePath", "Public/GroomCacheImportOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCacheImportOptions_Statics::NewProp_ImportSettings_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomCacheImportOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomCacheImportOptions_Statics::NewProp_ImportSettings = { "ImportSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCacheImportOptions, ImportSettings), Z_Construct_UScriptStruct_FGroomCacheImportSettings, METADATA_PARAMS(Z_Construct_UClass_UGroomCacheImportOptions_Statics::NewProp_ImportSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCacheImportOptions_Statics::NewProp_ImportSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomCacheImportOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCacheImportOptions_Statics::NewProp_ImportSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomCacheImportOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomCacheImportOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomCacheImportOptions_Statics::ClassParams = {
		&UGroomCacheImportOptions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomCacheImportOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCacheImportOptions_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomCacheImportOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCacheImportOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomCacheImportOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomCacheImportOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomCacheImportOptions, 40203933);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomCacheImportOptions>()
	{
		return UGroomCacheImportOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomCacheImportOptions(Z_Construct_UClass_UGroomCacheImportOptions, &UGroomCacheImportOptions::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomCacheImportOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomCacheImportOptions);
	void UGroomCacheImportData::StaticRegisterNativesUGroomCacheImportData()
	{
	}
	UClass* Z_Construct_UClass_UGroomCacheImportData_NoRegister()
	{
		return UGroomCacheImportData::StaticClass();
	}
	struct Z_Construct_UClass_UGroomCacheImportData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomCacheImportData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetImportData,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCacheImportData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** The asset import data to store the import settings within the GroomCache asset */" },
		{ "IncludePath", "GroomCacheImportOptions.h" },
		{ "ModuleRelativePath", "Public/GroomCacheImportOptions.h" },
		{ "ToolTip", "The asset import data to store the import settings within the GroomCache asset" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCacheImportData_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomCacheImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomCacheImportData_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCacheImportData, Settings), Z_Construct_UScriptStruct_FGroomCacheImportSettings, METADATA_PARAMS(Z_Construct_UClass_UGroomCacheImportData_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCacheImportData_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomCacheImportData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCacheImportData_Statics::NewProp_Settings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomCacheImportData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomCacheImportData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomCacheImportData_Statics::ClassParams = {
		&UGroomCacheImportData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomCacheImportData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCacheImportData_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomCacheImportData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCacheImportData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomCacheImportData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomCacheImportData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomCacheImportData, 918390456);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomCacheImportData>()
	{
		return UGroomCacheImportData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomCacheImportData(Z_Construct_UClass_UGroomCacheImportData, &UGroomCacheImportData::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomCacheImportData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomCacheImportData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
