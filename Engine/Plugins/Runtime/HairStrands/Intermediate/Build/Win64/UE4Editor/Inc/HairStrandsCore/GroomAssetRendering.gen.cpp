// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomAssetRendering.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomAssetRendering() {}
// Cross Module References
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsRendering();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGeometrySettings();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairShadowSettings();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings();
// End Cross Module References
class UScriptStruct* FHairGroupsRendering::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupsRendering_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupsRendering, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupsRendering"), sizeof(FHairGroupsRendering), Get_Z_Construct_UScriptStruct_FHairGroupsRendering_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupsRendering>()
{
	return FHairGroupsRendering::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupsRendering(FHairGroupsRendering::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupsRendering"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsRendering
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsRendering()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupsRendering>(FName(TEXT("HairGroupsRendering")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsRendering;
	struct Z_Construct_UScriptStruct_FHairGroupsRendering_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSlotName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MaterialSlotName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeometrySettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeometrySettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShadowSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ShadowSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdvancedSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdvancedSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupsRendering>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_MaterialSlotName_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_MaterialSlotName = { "MaterialSlotName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsRendering, MaterialSlotName), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_MaterialSlotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_MaterialSlotName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_Material_MetaData[] = {
		{ "Comment", "/* Deprecated */" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "ToolTip", "Deprecated" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsRendering, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_Material_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_GeometrySettings_MetaData[] = {
		{ "Category", "GeometrySettings" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "ToolTip", "Geometry settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_GeometrySettings = { "GeometrySettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsRendering, GeometrySettings), Z_Construct_UScriptStruct_FHairGeometrySettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_GeometrySettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_GeometrySettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_ShadowSettings_MetaData[] = {
		{ "Category", "ShadowSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "ToolTip", "Shadow settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_ShadowSettings = { "ShadowSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsRendering, ShadowSettings), Z_Construct_UScriptStruct_FHairShadowSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_ShadowSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_ShadowSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_AdvancedSettings_MetaData[] = {
		{ "Category", "MiscSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "ToolTip", "Advanced rendering settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_AdvancedSettings = { "AdvancedSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsRendering, AdvancedSettings), Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_AdvancedSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_AdvancedSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_MaterialSlotName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_Material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_GeometrySettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_ShadowSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::NewProp_AdvancedSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGroupsRendering",
		sizeof(FHairGroupsRendering),
		alignof(FHairGroupsRendering),
		Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsRendering()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupsRendering_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupsRendering"), sizeof(FHairGroupsRendering), Get_Z_Construct_UScriptStruct_FHairGroupsRendering_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupsRendering_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupsRendering_Hash() { return 1803224844U; }
class UScriptStruct* FHairAdvancedRenderingSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairAdvancedRenderingSettings"), sizeof(FHairAdvancedRenderingSettings), Get_Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairAdvancedRenderingSettings>()
{
	return FHairAdvancedRenderingSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairAdvancedRenderingSettings(FHairAdvancedRenderingSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairAdvancedRenderingSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairAdvancedRenderingSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairAdvancedRenderingSettings()
	{
		UScriptStruct::DeferCppStructOps<FHairAdvancedRenderingSettings>(FName(TEXT("HairAdvancedRenderingSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairAdvancedRenderingSettings;
	struct Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseStableRasterization_MetaData[];
#endif
		static void NewProp_bUseStableRasterization_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseStableRasterization;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bScatterSceneLighting_MetaData[];
#endif
		static void NewProp_bScatterSceneLighting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bScatterSceneLighting;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairAdvancedRenderingSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bUseStableRasterization_MetaData[] = {
		{ "Category", "AdvancedRenderingSettings" },
		{ "Comment", "/** Insure the hair does not alias. When enable, group of hairs might appear thicker. Isolated hair should remain thin. */" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "ToolTip", "Insure the hair does not alias. When enable, group of hairs might appear thicker. Isolated hair should remain thin." },
	};
#endif
	void Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bUseStableRasterization_SetBit(void* Obj)
	{
		((FHairAdvancedRenderingSettings*)Obj)->bUseStableRasterization = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bUseStableRasterization = { "bUseStableRasterization", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairAdvancedRenderingSettings), &Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bUseStableRasterization_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bUseStableRasterization_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bUseStableRasterization_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bScatterSceneLighting_MetaData[] = {
		{ "Category", "AdvancedRenderingSettings" },
		{ "Comment", "/** Light hair with the scene color. This is used for vellus/short hair to bring light from the surrounding surface, like skin. */" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "ToolTip", "Light hair with the scene color. This is used for vellus/short hair to bring light from the surrounding surface, like skin." },
	};
#endif
	void Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bScatterSceneLighting_SetBit(void* Obj)
	{
		((FHairAdvancedRenderingSettings*)Obj)->bScatterSceneLighting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bScatterSceneLighting = { "bScatterSceneLighting", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairAdvancedRenderingSettings), &Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bScatterSceneLighting_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bScatterSceneLighting_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bScatterSceneLighting_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bUseStableRasterization,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::NewProp_bScatterSceneLighting,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairAdvancedRenderingSettings",
		sizeof(FHairAdvancedRenderingSettings),
		alignof(FHairAdvancedRenderingSettings),
		Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairAdvancedRenderingSettings"), sizeof(FHairAdvancedRenderingSettings), Get_Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairAdvancedRenderingSettings_Hash() { return 1266187723U; }
class UScriptStruct* FHairShadowSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairShadowSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairShadowSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairShadowSettings"), sizeof(FHairShadowSettings), Get_Z_Construct_UScriptStruct_FHairShadowSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairShadowSettings>()
{
	return FHairShadowSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairShadowSettings(FHairShadowSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairShadowSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairShadowSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairShadowSettings()
	{
		UScriptStruct::DeferCppStructOps<FHairShadowSettings>(FName(TEXT("HairShadowSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairShadowSettings;
	struct Z_Construct_UScriptStruct_FHairShadowSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairShadowDensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HairShadowDensity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairRaytracingRadiusScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HairRaytracingRadiusScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseHairRaytracingGeometry_MetaData[];
#endif
		static void NewProp_bUseHairRaytracingGeometry_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseHairRaytracingGeometry;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bVoxelize_MetaData[];
#endif
		static void NewProp_bVoxelize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bVoxelize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairShadowSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairShadowSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_HairShadowDensity_MetaData[] = {
		{ "Category", "ShadowSettings" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Override the hair shadow density factor (unit less). */" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "SliderExponent", "6" },
		{ "ToolTip", "Override the hair shadow density factor (unit less)." },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_HairShadowDensity = { "HairShadowDensity", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairShadowSettings, HairShadowDensity), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_HairShadowDensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_HairShadowDensity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_HairRaytracingRadiusScale_MetaData[] = {
		{ "Category", "ShadowSettings" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Scale the hair geometry radius for ray tracing effects (e.g. shadow) */" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "SliderExponent", "6" },
		{ "ToolTip", "Scale the hair geometry radius for ray tracing effects (e.g. shadow)" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_HairRaytracingRadiusScale = { "HairRaytracingRadiusScale", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairShadowSettings, HairRaytracingRadiusScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_HairRaytracingRadiusScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_HairRaytracingRadiusScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bUseHairRaytracingGeometry_MetaData[] = {
		{ "Category", "ShadowSettings" },
		{ "Comment", "/** Enable hair strands geomtry for raytracing */" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "ToolTip", "Enable hair strands geomtry for raytracing" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bUseHairRaytracingGeometry_SetBit(void* Obj)
	{
		((FHairShadowSettings*)Obj)->bUseHairRaytracingGeometry = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bUseHairRaytracingGeometry = { "bUseHairRaytracingGeometry", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairShadowSettings), &Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bUseHairRaytracingGeometry_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bUseHairRaytracingGeometry_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bUseHairRaytracingGeometry_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bVoxelize_MetaData[] = {
		{ "Category", "ShadowSettings" },
		{ "Comment", "/** Enable stands voxelize for casting shadow and environment occlusion */" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "ToolTip", "Enable stands voxelize for casting shadow and environment occlusion" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bVoxelize_SetBit(void* Obj)
	{
		((FHairShadowSettings*)Obj)->bVoxelize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bVoxelize = { "bVoxelize", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairShadowSettings), &Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bVoxelize_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bVoxelize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bVoxelize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairShadowSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_HairShadowDensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_HairRaytracingRadiusScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bUseHairRaytracingGeometry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairShadowSettings_Statics::NewProp_bVoxelize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairShadowSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairShadowSettings",
		sizeof(FHairShadowSettings),
		alignof(FHairShadowSettings),
		Z_Construct_UScriptStruct_FHairShadowSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairShadowSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairShadowSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairShadowSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairShadowSettings"), sizeof(FHairShadowSettings), Get_Z_Construct_UScriptStruct_FHairShadowSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairShadowSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairShadowSettings_Hash() { return 4264427587U; }
class UScriptStruct* FHairGeometrySettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGeometrySettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGeometrySettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGeometrySettings"), sizeof(FHairGeometrySettings), Get_Z_Construct_UScriptStruct_FHairGeometrySettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGeometrySettings>()
{
	return FHairGeometrySettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGeometrySettings(FHairGeometrySettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGeometrySettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGeometrySettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGeometrySettings()
	{
		UScriptStruct::DeferCppStructOps<FHairGeometrySettings>(FName(TEXT("HairGeometrySettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGeometrySettings;
	struct Z_Construct_UScriptStruct_FHairGeometrySettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HairWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairRootScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HairRootScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairTipScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HairTipScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairClipScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HairClipScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGeometrySettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairWidth_MetaData[] = {
		{ "Category", "GeometrySettings" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Hair width (in centimeters) */" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "SliderExponent", "6" },
		{ "ToolTip", "Hair width (in centimeters)" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairWidth = { "HairWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGeometrySettings, HairWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairRootScale_MetaData[] = {
		{ "Category", "GeometrySettings" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Scale the hair width at the root */" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "SliderExponent", "6" },
		{ "ToolTip", "Scale the hair width at the root" },
		{ "UIMax", "2.0" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairRootScale = { "HairRootScale", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGeometrySettings, HairRootScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairRootScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairRootScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairTipScale_MetaData[] = {
		{ "Category", "GeometrySettings" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Scale the hair with at the tip */" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "SliderExponent", "6" },
		{ "ToolTip", "Scale the hair with at the tip" },
		{ "UIMax", "2.0" },
		{ "UIMin", "0.001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairTipScale = { "HairTipScale", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGeometrySettings, HairTipScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairTipScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairTipScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairClipScale_MetaData[] = {
		{ "Category", "GeometrySettings" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** DEPRECATED: HairClipScale is deprecated and will be removed in next releases. Normalized hair clip length, i.e. at which length hair will be clipped. 1 means no clipping. 0 means hairs are fully clipped */" },
		{ "DeprecatedProperty", "" },
		{ "DisplayName", "Hair Clip Scale (DEPRECATED)" },
		{ "ModuleRelativePath", "Public/GroomAssetRendering.h" },
		{ "SliderExponent", "1" },
		{ "ToolTip", "DEPRECATED: HairClipScale is deprecated and will be removed in next releases. Normalized hair clip length, i.e. at which length hair will be clipped. 1 means no clipping. 0 means hairs are fully clipped" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairClipScale = { "HairClipScale", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGeometrySettings, HairClipScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairClipScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairClipScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairRootScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairTipScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::NewProp_HairClipScale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGeometrySettings",
		sizeof(FHairGeometrySettings),
		alignof(FHairGeometrySettings),
		Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGeometrySettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGeometrySettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGeometrySettings"), sizeof(FHairGeometrySettings), Get_Z_Construct_UScriptStruct_FHairGeometrySettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGeometrySettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGeometrySettings_Hash() { return 2023406403U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
