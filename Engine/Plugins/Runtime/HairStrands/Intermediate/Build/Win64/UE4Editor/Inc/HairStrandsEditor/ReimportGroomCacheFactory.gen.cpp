// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsEditor/Public/ReimportGroomCacheFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReimportGroomCacheFactory() {}
// Cross Module References
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UReimportGroomCacheFactory_NoRegister();
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UReimportGroomCacheFactory();
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UHairStrandsFactory();
	UPackage* Z_Construct_UPackage__Script_HairStrandsEditor();
// End Cross Module References
	void UReimportGroomCacheFactory::StaticRegisterNativesUReimportGroomCacheFactory()
	{
	}
	UClass* Z_Construct_UClass_UReimportGroomCacheFactory_NoRegister()
	{
		return UReimportGroomCacheFactory::StaticClass();
	}
	struct Z_Construct_UClass_UReimportGroomCacheFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReimportGroomCacheFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UHairStrandsFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReimportGroomCacheFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "ReimportGroomCacheFactory.h" },
		{ "ModuleRelativePath", "Public/ReimportGroomCacheFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReimportGroomCacheFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReimportGroomCacheFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReimportGroomCacheFactory_Statics::ClassParams = {
		&UReimportGroomCacheFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UReimportGroomCacheFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReimportGroomCacheFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReimportGroomCacheFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReimportGroomCacheFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReimportGroomCacheFactory, 375144763);
	template<> HAIRSTRANDSEDITOR_API UClass* StaticClass<UReimportGroomCacheFactory>()
	{
		return UReimportGroomCacheFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReimportGroomCacheFactory(Z_Construct_UClass_UReimportGroomCacheFactory, &UReimportGroomCacheFactory::StaticClass, TEXT("/Script/HairStrandsEditor"), TEXT("UReimportGroomCacheFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReimportGroomCacheFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
