// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomImportOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomImportOptions() {}
// Cross Module References
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FGroomHairGroupPreview();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsInterpolation();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomImportOptions_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomImportOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FGroomConversionSettings();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomHairGroupsPreview_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomHairGroupsPreview();
// End Cross Module References
class UScriptStruct* FGroomHairGroupPreview::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FGroomHairGroupPreview_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGroomHairGroupPreview, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("GroomHairGroupPreview"), sizeof(FGroomHairGroupPreview), Get_Z_Construct_UScriptStruct_FGroomHairGroupPreview_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FGroomHairGroupPreview>()
{
	return FGroomHairGroupPreview::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGroomHairGroupPreview(FGroomHairGroupPreview::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("GroomHairGroupPreview"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomHairGroupPreview
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomHairGroupPreview()
	{
		UScriptStruct::DeferCppStructOps<FGroomHairGroupPreview>(FName(TEXT("GroomHairGroupPreview")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomHairGroupPreview;
	struct Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_GroupID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurveCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GuideCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_GuideCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InterpolationSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomImportOptions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGroomHairGroupPreview>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_GroupID_MetaData[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/GroomImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_GroupID = { "GroupID", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomHairGroupPreview, GroupID), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_GroupID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_GroupID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_CurveCount_MetaData[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/GroomImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_CurveCount = { "CurveCount", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomHairGroupPreview, CurveCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_CurveCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_CurveCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_GuideCount_MetaData[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/GroomImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_GuideCount = { "GuideCount", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomHairGroupPreview, GuideCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_GuideCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_GuideCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_InterpolationSettings_MetaData[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/GroomImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_InterpolationSettings = { "InterpolationSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomHairGroupPreview, InterpolationSettings), Z_Construct_UScriptStruct_FHairGroupsInterpolation, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_InterpolationSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_InterpolationSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_GroupID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_CurveCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_GuideCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::NewProp_InterpolationSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"GroomHairGroupPreview",
		sizeof(FGroomHairGroupPreview),
		alignof(FGroomHairGroupPreview),
		Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGroomHairGroupPreview()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGroomHairGroupPreview_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GroomHairGroupPreview"), sizeof(FGroomHairGroupPreview), Get_Z_Construct_UScriptStruct_FGroomHairGroupPreview_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGroomHairGroupPreview_Hash() { return 1861908939U; }
	void UGroomImportOptions::StaticRegisterNativesUGroomImportOptions()
	{
	}
	UClass* Z_Construct_UClass_UGroomImportOptions_NoRegister()
	{
		return UGroomImportOptions::StaticClass();
	}
	struct Z_Construct_UClass_UGroomImportOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConversionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConversionSettings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InterpolationSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InterpolationSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomImportOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomImportOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Hidden" },
		{ "IncludePath", "GroomImportOptions.h" },
		{ "ModuleRelativePath", "Public/GroomImportOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_ConversionSettings_MetaData[] = {
		{ "Category", "Conversion" },
		{ "ModuleRelativePath", "Public/GroomImportOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_ConversionSettings = { "ConversionSettings", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomImportOptions, ConversionSettings), Z_Construct_UScriptStruct_FGroomConversionSettings, METADATA_PARAMS(Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_ConversionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_ConversionSettings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_InterpolationSettings_Inner = { "InterpolationSettings", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairGroupsInterpolation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_InterpolationSettings_MetaData[] = {
		{ "Category", "Hidden" },
		{ "Comment", "/* Interpolation settings per group */" },
		{ "ModuleRelativePath", "Public/GroomImportOptions.h" },
		{ "ToolTip", "Interpolation settings per group" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_InterpolationSettings = { "InterpolationSettings", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomImportOptions, InterpolationSettings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_InterpolationSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_InterpolationSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomImportOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_ConversionSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_InterpolationSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomImportOptions_Statics::NewProp_InterpolationSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomImportOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomImportOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomImportOptions_Statics::ClassParams = {
		&UGroomImportOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomImportOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomImportOptions_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomImportOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomImportOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomImportOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomImportOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomImportOptions, 2157703280);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomImportOptions>()
	{
		return UGroomImportOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomImportOptions(Z_Construct_UClass_UGroomImportOptions, &UGroomImportOptions::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomImportOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomImportOptions);
	void UGroomHairGroupsPreview::StaticRegisterNativesUGroomHairGroupsPreview()
	{
	}
	UClass* Z_Construct_UClass_UGroomHairGroupsPreview_NoRegister()
	{
		return UGroomHairGroupsPreview::StaticClass();
	}
	struct Z_Construct_UClass_UGroomHairGroupsPreview_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Groups_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Groups_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Groups;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomHairGroupsPreview_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomHairGroupsPreview_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Hidden" },
		{ "IncludePath", "GroomImportOptions.h" },
		{ "ModuleRelativePath", "Public/GroomImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomHairGroupsPreview_Statics::NewProp_Groups_Inner = { "Groups", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGroomHairGroupPreview, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomHairGroupsPreview_Statics::NewProp_Groups_MetaData[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/GroomImportOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomHairGroupsPreview_Statics::NewProp_Groups = { "Groups", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomHairGroupsPreview, Groups), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomHairGroupsPreview_Statics::NewProp_Groups_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomHairGroupsPreview_Statics::NewProp_Groups_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomHairGroupsPreview_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomHairGroupsPreview_Statics::NewProp_Groups_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomHairGroupsPreview_Statics::NewProp_Groups,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomHairGroupsPreview_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomHairGroupsPreview>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomHairGroupsPreview_Statics::ClassParams = {
		&UGroomHairGroupsPreview::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomHairGroupsPreview_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomHairGroupsPreview_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomHairGroupsPreview_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomHairGroupsPreview_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomHairGroupsPreview()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomHairGroupsPreview_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomHairGroupsPreview, 3361620768);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomHairGroupsPreview>()
	{
		return UGroomHairGroupsPreview::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomHairGroupsPreview(Z_Construct_UClass_UGroomHairGroupsPreview, &UGroomHairGroupsPreview::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomHairGroupsPreview"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomHairGroupsPreview);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
