// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomComponent() {}
// Cross Module References
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomComponent_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomComponent();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomBindingAsset_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomAsset_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCache_NoRegister();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPhysicsAsset_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraSystem_NoRegister();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupDesc();
	ENGINE_API UClass* Z_Construct_UClass_ULODSyncInterface_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UGroomComponent::execSetBindingAsset)
	{
		P_GET_OBJECT(UGroomBindingAsset,Z_Param_InBinding);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetBindingAsset(Z_Param_InBinding);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGroomComponent::execSetGroomAsset)
	{
		P_GET_OBJECT(UGroomAsset,Z_Param_Asset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetGroomAsset(Z_Param_Asset);
		P_NATIVE_END;
	}
	void UGroomComponent::StaticRegisterNativesUGroomComponent()
	{
		UClass* Class = UGroomComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetBindingAsset", &UGroomComponent::execSetBindingAsset },
			{ "SetGroomAsset", &UGroomComponent::execSetGroomAsset },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics
	{
		struct GroomComponent_eventSetBindingAsset_Parms
		{
			UGroomBindingAsset* InBinding;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InBinding;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics::NewProp_InBinding = { "InBinding", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GroomComponent_eventSetBindingAsset_Parms, InBinding), Z_Construct_UClass_UGroomBindingAsset_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics::NewProp_InBinding,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Groom" },
		{ "Comment", "/* Accessor function for changing Groom binding asset from blueprint/sequencer */" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
		{ "ToolTip", "Accessor function for changing Groom binding asset from blueprint/sequencer" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGroomComponent, nullptr, "SetBindingAsset", nullptr, nullptr, sizeof(GroomComponent_eventSetBindingAsset_Parms), Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGroomComponent_SetBindingAsset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGroomComponent_SetBindingAsset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics
	{
		struct GroomComponent_eventSetGroomAsset_Parms
		{
			UGroomAsset* Asset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Asset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics::NewProp_Asset = { "Asset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GroomComponent_eventSetGroomAsset_Parms, Asset), Z_Construct_UClass_UGroomAsset_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics::NewProp_Asset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Groom" },
		{ "Comment", "/* Accessor function for changing Groom asset from blueprint/sequencer */" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
		{ "ToolTip", "Accessor function for changing Groom asset from blueprint/sequencer" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGroomComponent, nullptr, "SetGroomAsset", nullptr, nullptr, sizeof(GroomComponent_eventSetGroomAsset_Parms), Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGroomComponent_SetGroomAsset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGroomComponent_SetGroomAsset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGroomComponent_NoRegister()
	{
		return UGroomComponent::StaticClass();
	}
	struct Z_Construct_UClass_UGroomComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroomAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GroomAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroomCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GroomCache;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NiagaraComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NiagaraComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NiagaraComponents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceSkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceSkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BindingAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BindingAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhysicsAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PhysicsAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strands_DebugMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Strands_DebugMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strands_DefaultMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Strands_DefaultMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cards_DefaultMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Cards_DefaultMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Meshes_DefaultMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Meshes_DefaultMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngularSpringsSystem_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AngularSpringsSystem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CosseratRodsSystem_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CosseratRodsSystem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttachmentName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AttachmentName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GroomGroupsDesc_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroomGroupsDesc_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GroomGroupsDesc;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRunning_MetaData[];
#endif
		static void NewProp_bRunning_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRunning;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLooping_MetaData[];
#endif
		static void NewProp_bLooping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLooping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bManualTick_MetaData[];
#endif
		static void NewProp_bManualTick_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bManualTick;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElapsedTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElapsedTime;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroomAssetBeingLoaded_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GroomAssetBeingLoaded;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BindingAssetBeingLoaded_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BindingAssetBeingLoaded;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGroomComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGroomComponent_SetBindingAsset, "SetBindingAsset" }, // 30040851
		{ &Z_Construct_UFunction_UGroomComponent_SetGroomAsset, "SetGroomAsset" }, // 188448880
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Rendering" },
		{ "HideCategories", "Object Physics Activation Mobility Components|Activation Mobility Trigger" },
		{ "IncludePath", "GroomComponent.h" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomAsset_MetaData[] = {
		{ "Category", "Groom" },
		{ "Comment", "/** Groom asset . */" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
		{ "ToolTip", "Groom asset ." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomAsset = { "GroomAsset", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, GroomAsset), Z_Construct_UClass_UGroomAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomCache_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "EditCondition", "BindingAsset == nullptr" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomCache = { "GroomCache", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, GroomCache), Z_Construct_UClass_UGroomCache_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomCache_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_NiagaraComponents_Inner = { "NiagaraComponents", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNiagaraComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_NiagaraComponents_MetaData[] = {
		{ "Comment", "/** Niagara components that will be attached to the system*/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
		{ "ToolTip", "Niagara components that will be attached to the system" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_NiagaraComponents = { "NiagaraComponents", nullptr, (EPropertyFlags)0x0010008000002008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, NiagaraComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_NiagaraComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_NiagaraComponents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_SourceSkeletalMesh_MetaData[] = {
		{ "Comment", "// Kept for debugging mesh transfer\n" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
		{ "ToolTip", "Kept for debugging mesh transfer" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_SourceSkeletalMesh = { "SourceSkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, SourceSkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_SourceSkeletalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_SourceSkeletalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_BindingAsset_MetaData[] = {
		{ "Category", "Groom" },
		{ "Comment", "/** Optional binding asset for binding a groom onto a skeletal mesh. If the binding asset is not provided the projection is done at runtime, which implies a large GPU cost at startup time. */" },
		{ "EditCondition", "GroomCache == nullptr" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
		{ "ToolTip", "Optional binding asset for binding a groom onto a skeletal mesh. If the binding asset is not provided the projection is done at runtime, which implies a large GPU cost at startup time." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_BindingAsset = { "BindingAsset", nullptr, (EPropertyFlags)0x0010000200000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, BindingAsset), Z_Construct_UClass_UGroomBindingAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_BindingAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_BindingAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_PhysicsAsset_MetaData[] = {
		{ "Category", "Groom" },
		{ "Comment", "/** Physics asset to be used for hair simulation */" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
		{ "ToolTip", "Physics asset to be used for hair simulation" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_PhysicsAsset = { "PhysicsAsset", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, PhysicsAsset), Z_Construct_UClass_UPhysicsAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_PhysicsAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_PhysicsAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_Strands_DebugMaterial_MetaData[] = {
		{ "Comment", "/* Reference of the default/debug materials for each geometric representation */" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
		{ "ToolTip", "Reference of the default/debug materials for each geometric representation" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_Strands_DebugMaterial = { "Strands_DebugMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, Strands_DebugMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_Strands_DebugMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_Strands_DebugMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_Strands_DefaultMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_Strands_DefaultMaterial = { "Strands_DefaultMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, Strands_DefaultMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_Strands_DefaultMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_Strands_DefaultMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_Cards_DefaultMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_Cards_DefaultMaterial = { "Cards_DefaultMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, Cards_DefaultMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_Cards_DefaultMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_Cards_DefaultMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_Meshes_DefaultMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_Meshes_DefaultMaterial = { "Meshes_DefaultMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, Meshes_DefaultMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_Meshes_DefaultMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_Meshes_DefaultMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_AngularSpringsSystem_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_AngularSpringsSystem = { "AngularSpringsSystem", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, AngularSpringsSystem), Z_Construct_UClass_UNiagaraSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_AngularSpringsSystem_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_AngularSpringsSystem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_CosseratRodsSystem_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_CosseratRodsSystem = { "CosseratRodsSystem", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, CosseratRodsSystem), Z_Construct_UClass_UNiagaraSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_CosseratRodsSystem_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_CosseratRodsSystem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_AttachmentName_MetaData[] = {
		{ "Category", "Groom" },
		{ "Comment", "/** Optional socket name, where the groom component should be attached at, when parented with a skeletal mesh */" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
		{ "ToolTip", "Optional socket name, where the groom component should be attached at, when parented with a skeletal mesh" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_AttachmentName = { "AttachmentName", nullptr, (EPropertyFlags)0x0010040200000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, AttachmentName), METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_AttachmentName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_AttachmentName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomGroupsDesc_Inner = { "GroomGroupsDesc", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHairGroupDesc, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomGroupsDesc_MetaData[] = {
		{ "Category", "Groom" },
		{ "Comment", "/** Groom's groups info. */" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
		{ "ToolTip", "Groom's groups info." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomGroupsDesc = { "GroomGroupsDesc", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, GroomGroupsDesc), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomGroupsDesc_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomGroupsDesc_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_bRunning_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	void Z_Construct_UClass_UGroomComponent_Statics::NewProp_bRunning_SetBit(void* Obj)
	{
		((UGroomComponent*)Obj)->bRunning = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_bRunning = { "bRunning", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomComponent), &Z_Construct_UClass_UGroomComponent_Statics::NewProp_bRunning_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_bRunning_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_bRunning_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_bLooping_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	void Z_Construct_UClass_UGroomComponent_Statics::NewProp_bLooping_SetBit(void* Obj)
	{
		((UGroomComponent*)Obj)->bLooping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_bLooping = { "bLooping", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomComponent), &Z_Construct_UClass_UGroomComponent_Statics::NewProp_bLooping_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_bLooping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_bLooping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_bManualTick_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	void Z_Construct_UClass_UGroomComponent_Statics::NewProp_bManualTick_SetBit(void* Obj)
	{
		((UGroomComponent*)Obj)->bManualTick = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_bManualTick = { "bManualTick", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroomComponent), &Z_Construct_UClass_UGroomComponent_Statics::NewProp_bManualTick_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_bManualTick_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_bManualTick_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_ElapsedTime_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_ElapsedTime = { "ElapsedTime", nullptr, (EPropertyFlags)0x0040000000022001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, ElapsedTime), METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_ElapsedTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_ElapsedTime_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomAssetBeingLoaded_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomAssetBeingLoaded = { "GroomAssetBeingLoaded", nullptr, (EPropertyFlags)0x0040000800002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, GroomAssetBeingLoaded), Z_Construct_UClass_UGroomAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomAssetBeingLoaded_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomAssetBeingLoaded_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomComponent_Statics::NewProp_BindingAssetBeingLoaded_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomComponent_Statics::NewProp_BindingAssetBeingLoaded = { "BindingAssetBeingLoaded", nullptr, (EPropertyFlags)0x0040000800002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomComponent, BindingAssetBeingLoaded), Z_Construct_UClass_UGroomBindingAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::NewProp_BindingAssetBeingLoaded_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::NewProp_BindingAssetBeingLoaded_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_NiagaraComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_NiagaraComponents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_SourceSkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_BindingAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_PhysicsAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_Strands_DebugMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_Strands_DefaultMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_Cards_DefaultMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_Meshes_DefaultMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_AngularSpringsSystem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_CosseratRodsSystem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_AttachmentName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomGroupsDesc_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomGroupsDesc,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_bRunning,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_bLooping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_bManualTick,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_ElapsedTime,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_GroomAssetBeingLoaded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomComponent_Statics::NewProp_BindingAssetBeingLoaded,
#endif // WITH_EDITORONLY_DATA
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UGroomComponent_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_ULODSyncInterface_NoRegister, (int32)VTABLE_OFFSET(UGroomComponent, ILODSyncInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomComponent_Statics::ClassParams = {
		&UGroomComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UGroomComponent_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomComponent, 2864818647);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomComponent>()
	{
		return UGroomComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomComponent(Z_Construct_UClass_UGroomComponent, &UGroomComponent::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
