// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UGroomBindingAsset;
class UGroomAsset;
#ifdef HAIRSTRANDSCORE_GroomComponent_generated_h
#error "GroomComponent.generated.h already included, missing '#pragma once' in GroomComponent.h"
#endif
#define HAIRSTRANDSCORE_GroomComponent_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetBindingAsset); \
	DECLARE_FUNCTION(execSetGroomAsset);


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetBindingAsset); \
	DECLARE_FUNCTION(execSetGroomAsset);


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomComponent(); \
	friend struct Z_Construct_UClass_UGroomComponent_Statics; \
public: \
	DECLARE_CLASS(UGroomComponent, UMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomComponent) \
	virtual UObject* _getUObject() const override { return const_cast<UGroomComponent*>(this); }


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUGroomComponent(); \
	friend struct Z_Construct_UClass_UGroomComponent_Statics; \
public: \
	DECLARE_CLASS(UGroomComponent, UMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomComponent) \
	virtual UObject* _getUObject() const override { return const_cast<UGroomComponent*>(this); }


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomComponent(UGroomComponent&&); \
	NO_API UGroomComponent(const UGroomComponent&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomComponent(UGroomComponent&&); \
	NO_API UGroomComponent(const UGroomComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomComponent)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bRunning() { return STRUCT_OFFSET(UGroomComponent, bRunning); } \
	FORCEINLINE static uint32 __PPO__bLooping() { return STRUCT_OFFSET(UGroomComponent, bLooping); } \
	FORCEINLINE static uint32 __PPO__bManualTick() { return STRUCT_OFFSET(UGroomComponent, bManualTick); } \
	FORCEINLINE static uint32 __PPO__ElapsedTime() { return STRUCT_OFFSET(UGroomComponent, ElapsedTime); }


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_20_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h_23_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GroomComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class UGroomComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
