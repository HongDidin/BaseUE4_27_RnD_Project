// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomCreateFollicleMaskOptions_generated_h
#error "GroomCreateFollicleMaskOptions.generated.h already included, missing '#pragma once' in GroomCreateFollicleMaskOptions.h"
#endif
#define HAIRSTRANDSCORE_GroomCreateFollicleMaskOptions_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_25_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFollicleMaskOptions_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FFollicleMaskOptions>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomCreateFollicleMaskOptions(); \
	friend struct Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics; \
public: \
	DECLARE_CLASS(UGroomCreateFollicleMaskOptions, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomCreateFollicleMaskOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_INCLASS \
private: \
	static void StaticRegisterNativesUGroomCreateFollicleMaskOptions(); \
	friend struct Z_Construct_UClass_UGroomCreateFollicleMaskOptions_Statics; \
public: \
	DECLARE_CLASS(UGroomCreateFollicleMaskOptions, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomCreateFollicleMaskOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomCreateFollicleMaskOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomCreateFollicleMaskOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomCreateFollicleMaskOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomCreateFollicleMaskOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomCreateFollicleMaskOptions(UGroomCreateFollicleMaskOptions&&); \
	NO_API UGroomCreateFollicleMaskOptions(const UGroomCreateFollicleMaskOptions&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomCreateFollicleMaskOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomCreateFollicleMaskOptions(UGroomCreateFollicleMaskOptions&&); \
	NO_API UGroomCreateFollicleMaskOptions(const UGroomCreateFollicleMaskOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomCreateFollicleMaskOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomCreateFollicleMaskOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomCreateFollicleMaskOptions)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_36_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h_39_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GroomCreateFollicleMaskOptions."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class UGroomCreateFollicleMaskOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCreateFollicleMaskOptions_h


#define FOREACH_ENUM_EFOLLICLEMASKCHANNEL(op) \
	op(EFollicleMaskChannel::R) \
	op(EFollicleMaskChannel::G) \
	op(EFollicleMaskChannel::B) \
	op(EFollicleMaskChannel::A) 

enum class EFollicleMaskChannel : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EFollicleMaskChannel>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
