// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_NiagaraDataInterfaceVelocityGrid_generated_h
#error "NiagaraDataInterfaceVelocityGrid.generated.h already included, missing '#pragma once' in NiagaraDataInterfaceVelocityGrid.h"
#endif
#define HAIRSTRANDSCORE_NiagaraDataInterfaceVelocityGrid_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNiagaraDataInterfaceVelocityGrid(); \
	friend struct Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics; \
public: \
	DECLARE_CLASS(UNiagaraDataInterfaceVelocityGrid, UNiagaraDataInterfaceRWBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraDataInterfaceVelocityGrid)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_INCLASS \
private: \
	static void StaticRegisterNativesUNiagaraDataInterfaceVelocityGrid(); \
	friend struct Z_Construct_UClass_UNiagaraDataInterfaceVelocityGrid_Statics; \
public: \
	DECLARE_CLASS(UNiagaraDataInterfaceVelocityGrid, UNiagaraDataInterfaceRWBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UNiagaraDataInterfaceVelocityGrid)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraDataInterfaceVelocityGrid(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNiagaraDataInterfaceVelocityGrid) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraDataInterfaceVelocityGrid); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraDataInterfaceVelocityGrid); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraDataInterfaceVelocityGrid(UNiagaraDataInterfaceVelocityGrid&&); \
	NO_API UNiagaraDataInterfaceVelocityGrid(const UNiagaraDataInterfaceVelocityGrid&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNiagaraDataInterfaceVelocityGrid() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNiagaraDataInterfaceVelocityGrid(UNiagaraDataInterfaceVelocityGrid&&); \
	NO_API UNiagaraDataInterfaceVelocityGrid(const UNiagaraDataInterfaceVelocityGrid&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNiagaraDataInterfaceVelocityGrid); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNiagaraDataInterfaceVelocityGrid); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNiagaraDataInterfaceVelocityGrid)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_108_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h_111_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NiagaraDataInterfaceVelocityGrid."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class UNiagaraDataInterfaceVelocityGrid>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_Niagara_NiagaraDataInterfaceVelocityGrid_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
