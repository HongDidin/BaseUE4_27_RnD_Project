// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomImportOptions_generated_h
#error "GroomImportOptions.generated.h already included, missing '#pragma once' in GroomImportOptions.h"
#endif
#define HAIRSTRANDSCORE_GroomImportOptions_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_28_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGroomHairGroupPreview_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FGroomHairGroupPreview>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomImportOptions(); \
	friend struct Z_Construct_UClass_UGroomImportOptions_Statics; \
public: \
	DECLARE_CLASS(UGroomImportOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUGroomImportOptions(); \
	friend struct Z_Construct_UClass_UGroomImportOptions_Statics; \
public: \
	DECLARE_CLASS(UGroomImportOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomImportOptions(UGroomImportOptions&&); \
	NO_API UGroomImportOptions(const UGroomImportOptions&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomImportOptions(UGroomImportOptions&&); \
	NO_API UGroomImportOptions(const UGroomImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomImportOptions)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_11_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GroomImportOptions."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class UGroomImportOptions>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGroomHairGroupsPreview(); \
	friend struct Z_Construct_UClass_UGroomHairGroupsPreview_Statics; \
public: \
	DECLARE_CLASS(UGroomHairGroupsPreview, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomHairGroupsPreview) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_INCLASS \
private: \
	static void StaticRegisterNativesUGroomHairGroupsPreview(); \
	friend struct Z_Construct_UClass_UGroomHairGroupsPreview_Statics; \
public: \
	DECLARE_CLASS(UGroomHairGroupsPreview, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(UGroomHairGroupsPreview) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomHairGroupsPreview(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomHairGroupsPreview) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomHairGroupsPreview); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomHairGroupsPreview); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomHairGroupsPreview(UGroomHairGroupsPreview&&); \
	NO_API UGroomHairGroupsPreview(const UGroomHairGroupsPreview&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGroomHairGroupsPreview(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGroomHairGroupsPreview(UGroomHairGroupsPreview&&); \
	NO_API UGroomHairGroupsPreview(const UGroomHairGroupsPreview&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGroomHairGroupsPreview); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGroomHairGroupsPreview); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGroomHairGroupsPreview)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_50_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h_53_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GroomHairGroupsPreview."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class UGroomHairGroupsPreview>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomImportOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
