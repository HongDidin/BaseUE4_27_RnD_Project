// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomCacheData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomCacheData() {}
// Cross Module References
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomCacheType();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomCacheAttributes();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FGroomCacheInfo();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FGroomAnimationInfo();
// End Cross Module References
	static UEnum* EGroomCacheType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EGroomCacheType, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EGroomCacheType"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomCacheType>()
	{
		return EGroomCacheType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroomCacheType(EGroomCacheType_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EGroomCacheType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EGroomCacheType_Hash() { return 2582223976U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomCacheType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroomCacheType"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EGroomCacheType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroomCacheType::None", (int64)EGroomCacheType::None },
				{ "EGroomCacheType::Strands", (int64)EGroomCacheType::Strands },
				{ "EGroomCacheType::Guides", (int64)EGroomCacheType::Guides },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Types of GroomCache\n * Strands: animated render strands (including animatable hair attributes)\n * Guides: animated guides that require in-engine simulation (position only)\n */" },
				{ "Guides.Name", "EGroomCacheType::Guides" },
				{ "ModuleRelativePath", "Public/GroomCacheData.h" },
				{ "None.Name", "EGroomCacheType::None" },
				{ "Strands.Name", "EGroomCacheType::Strands" },
				{ "ToolTip", "Types of GroomCache\nStrands: animated render strands (including animatable hair attributes)\nGuides: animated guides that require in-engine simulation (position only)" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EGroomCacheType",
				"EGroomCacheType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EGroomCacheAttributes_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EGroomCacheAttributes, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EGroomCacheAttributes"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomCacheAttributes>()
	{
		return EGroomCacheAttributes_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroomCacheAttributes(EGroomCacheAttributes_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EGroomCacheAttributes"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EGroomCacheAttributes_Hash() { return 758581291U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomCacheAttributes()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroomCacheAttributes"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EGroomCacheAttributes_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroomCacheAttributes::None", (int64)EGroomCacheAttributes::None },
				{ "EGroomCacheAttributes::Position", (int64)EGroomCacheAttributes::Position },
				{ "EGroomCacheAttributes::Width", (int64)EGroomCacheAttributes::Width },
				{ "EGroomCacheAttributes::Color", (int64)EGroomCacheAttributes::Color },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Color.Name", "EGroomCacheAttributes::Color" },
				{ "Comment", "/** Attributes in groom that can be animated */" },
				{ "ModuleRelativePath", "Public/GroomCacheData.h" },
				{ "None.Name", "EGroomCacheAttributes::None" },
				{ "Position.Name", "EGroomCacheAttributes::Position" },
				{ "ToolTip", "Attributes in groom that can be animated" },
				{ "Width.Name", "EGroomCacheAttributes::Width" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EGroomCacheAttributes",
				"EGroomCacheAttributes",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FGroomCacheInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FGroomCacheInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGroomCacheInfo, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("GroomCacheInfo"), sizeof(FGroomCacheInfo), Get_Z_Construct_UScriptStruct_FGroomCacheInfo_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FGroomCacheInfo>()
{
	return FGroomCacheInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGroomCacheInfo(FGroomCacheInfo::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("GroomCacheInfo"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomCacheInfo
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomCacheInfo()
	{
		UScriptStruct::DeferCppStructOps<FGroomCacheInfo>(FName(TEXT("GroomCacheInfo")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomCacheInfo;
	struct Z_Construct_UScriptStruct_FGroomCacheInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Version_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Version;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AnimationInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AnimationInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Information about the GroomCache itself */" },
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
		{ "ToolTip", "Information about the GroomCache itself" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGroomCacheInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Version_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Version = { "Version", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomCacheInfo, Version), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Version_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Version_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomCacheInfo, Type), Z_Construct_UEnum_HairStrandsCore_EGroomCacheType, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_AnimationInfo_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_AnimationInfo = { "AnimationInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomCacheInfo, AnimationInfo), Z_Construct_UScriptStruct_FGroomAnimationInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_AnimationInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_AnimationInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Version,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::NewProp_AnimationInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"GroomCacheInfo",
		sizeof(FGroomCacheInfo),
		alignof(FGroomCacheInfo),
		Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGroomCacheInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGroomCacheInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GroomCacheInfo"), sizeof(FGroomCacheInfo), Get_Z_Construct_UScriptStruct_FGroomCacheInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGroomCacheInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGroomCacheInfo_Hash() { return 99175916U; }
class UScriptStruct* FGroomAnimationInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FGroomAnimationInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGroomAnimationInfo, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("GroomAnimationInfo"), sizeof(FGroomAnimationInfo), Get_Z_Construct_UScriptStruct_FGroomAnimationInfo_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FGroomAnimationInfo>()
{
	return FGroomAnimationInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGroomAnimationInfo(FGroomAnimationInfo::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("GroomAnimationInfo"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomAnimationInfo
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomAnimationInfo()
	{
		UScriptStruct::DeferCppStructOps<FGroomAnimationInfo>(FName(TEXT("GroomAnimationInfo")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomAnimationInfo;
	struct Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumFrames_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_NumFrames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondsPerFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SecondsPerFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Duration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Duration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StartTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EndTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StartFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_EndFrame;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Attributes_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Attributes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Relevant information about a groom animation */" },
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
		{ "ToolTip", "Relevant information about a groom animation" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGroomAnimationInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_NumFrames_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_NumFrames = { "NumFrames", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomAnimationInfo, NumFrames), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_NumFrames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_NumFrames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_SecondsPerFrame_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_SecondsPerFrame = { "SecondsPerFrame", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomAnimationInfo, SecondsPerFrame), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_SecondsPerFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_SecondsPerFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Duration_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Duration = { "Duration", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomAnimationInfo, Duration), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Duration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Duration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_StartTime_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_StartTime = { "StartTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomAnimationInfo, StartTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_StartTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_StartTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_EndTime_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_EndTime = { "EndTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomAnimationInfo, EndTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_EndTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_EndTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_StartFrame_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_StartFrame = { "StartFrame", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomAnimationInfo, StartFrame), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_StartFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_StartFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_EndFrame_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_EndFrame = { "EndFrame", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomAnimationInfo, EndFrame), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_EndFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_EndFrame_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Attributes_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Attributes_MetaData[] = {
		{ "Category", "GroomCache" },
		{ "ModuleRelativePath", "Public/GroomCacheData.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Attributes = { "Attributes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomAnimationInfo, Attributes), Z_Construct_UEnum_HairStrandsCore_EGroomCacheAttributes, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Attributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Attributes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_NumFrames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_SecondsPerFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Duration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_StartTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_EndTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_StartFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_EndFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Attributes_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::NewProp_Attributes,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"GroomAnimationInfo",
		sizeof(FGroomAnimationInfo),
		alignof(FGroomAnimationInfo),
		Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGroomAnimationInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGroomAnimationInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GroomAnimationInfo"), sizeof(FGroomAnimationInfo), Get_Z_Construct_UScriptStruct_FGroomAnimationInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGroomAnimationInfo_Hash() { return 661228151U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
