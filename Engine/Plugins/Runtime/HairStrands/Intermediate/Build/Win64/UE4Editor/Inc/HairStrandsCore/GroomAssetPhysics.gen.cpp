// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomAssetPhysics.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomAssetPhysics() {}
// Cross Module References
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationType();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomStrandsSize();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomNiagaraSolvers();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsPhysics();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairSolverSettings();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairExternalForces();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairMaterialConstraints();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairStrandsParameters();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRuntimeFloatCurve();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairBendConstraint();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairStretchConstraint();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairCollisionConstraint();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraSystem_NoRegister();
// End Cross Module References
	static UEnum* EGroomInterpolationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationType, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EGroomInterpolationType"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomInterpolationType>()
	{
		return EGroomInterpolationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroomInterpolationType(EGroomInterpolationType_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EGroomInterpolationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationType_Hash() { return 2433959003U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroomInterpolationType"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroomInterpolationType::None", (int64)EGroomInterpolationType::None },
				{ "EGroomInterpolationType::RigidTransform", (int64)EGroomInterpolationType::RigidTransform },
				{ "EGroomInterpolationType::OffsetTransform", (int64)EGroomInterpolationType::OffsetTransform },
				{ "EGroomInterpolationType::SmoothTransform", (int64)EGroomInterpolationType::SmoothTransform },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** List of interpolation type */" },
				{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
				{ "None.Hidden", "" },
				{ "None.Name", "EGroomInterpolationType::None" },
				{ "OffsetTransform.DisplayName", "Offset Transform" },
				{ "OffsetTransform.Name", "EGroomInterpolationType::OffsetTransform" },
				{ "RigidTransform.DisplayName", "Rigid Transform" },
				{ "RigidTransform.Name", "EGroomInterpolationType::RigidTransform" },
				{ "SmoothTransform.DisplayName", "Smooth Transform" },
				{ "SmoothTransform.Name", "EGroomInterpolationType::SmoothTransform" },
				{ "ToolTip", "List of interpolation type" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EGroomInterpolationType",
				"EGroomInterpolationType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EGroomStrandsSize_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EGroomStrandsSize, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EGroomStrandsSize"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomStrandsSize>()
	{
		return EGroomStrandsSize_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroomStrandsSize(EGroomStrandsSize_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EGroomStrandsSize"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EGroomStrandsSize_Hash() { return 1527756969U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomStrandsSize()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroomStrandsSize"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EGroomStrandsSize_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroomStrandsSize::None", (int64)EGroomStrandsSize::None },
				{ "EGroomStrandsSize::Size2", (int64)EGroomStrandsSize::Size2 },
				{ "EGroomStrandsSize::Size4", (int64)EGroomStrandsSize::Size4 },
				{ "EGroomStrandsSize::Size8", (int64)EGroomStrandsSize::Size8 },
				{ "EGroomStrandsSize::Size16", (int64)EGroomStrandsSize::Size16 },
				{ "EGroomStrandsSize::Size32", (int64)EGroomStrandsSize::Size32 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Size of each strands*/" },
				{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
				{ "None.Hidden", "" },
				{ "None.Name", "EGroomStrandsSize::None" },
				{ "Size16.DisplayName", "16" },
				{ "Size16.Name", "EGroomStrandsSize::Size16" },
				{ "Size2.DisplayName", "2" },
				{ "Size2.Name", "EGroomStrandsSize::Size2" },
				{ "Size32.DisplayName", "32" },
				{ "Size32.Name", "EGroomStrandsSize::Size32" },
				{ "Size4.DisplayName", "4" },
				{ "Size4.Name", "EGroomStrandsSize::Size4" },
				{ "Size8.DisplayName", "8" },
				{ "Size8.Name", "EGroomStrandsSize::Size8" },
				{ "ToolTip", "Size of each strands" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EGroomStrandsSize",
				"EGroomStrandsSize",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EGroomNiagaraSolvers_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EGroomNiagaraSolvers, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EGroomNiagaraSolvers"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomNiagaraSolvers>()
	{
		return EGroomNiagaraSolvers_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroomNiagaraSolvers(EGroomNiagaraSolvers_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EGroomNiagaraSolvers"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EGroomNiagaraSolvers_Hash() { return 217784791U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomNiagaraSolvers()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroomNiagaraSolvers"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EGroomNiagaraSolvers_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroomNiagaraSolvers::None", (int64)EGroomNiagaraSolvers::None },
				{ "EGroomNiagaraSolvers::CosseratRods", (int64)EGroomNiagaraSolvers::CosseratRods },
				{ "EGroomNiagaraSolvers::AngularSprings", (int64)EGroomNiagaraSolvers::AngularSprings },
				{ "EGroomNiagaraSolvers::CustomSolver", (int64)EGroomNiagaraSolvers::CustomSolver },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AngularSprings.DisplayName", "Groom Springs" },
				{ "AngularSprings.Name", "EGroomNiagaraSolvers::AngularSprings" },
				{ "BlueprintType", "true" },
				{ "Comment", "/** List of niagara solvers */" },
				{ "CosseratRods.DisplayName", "Groom Rods" },
				{ "CosseratRods.Name", "EGroomNiagaraSolvers::CosseratRods" },
				{ "CustomSolver.DisplayName", "Custom Solver" },
				{ "CustomSolver.Name", "EGroomNiagaraSolvers::CustomSolver" },
				{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
				{ "None.Hidden", "" },
				{ "None.Name", "EGroomNiagaraSolvers::None" },
				{ "ToolTip", "List of niagara solvers" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EGroomNiagaraSolvers",
				"EGroomNiagaraSolvers",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FHairGroupsPhysics::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupsPhysics_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupsPhysics, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupsPhysics"), sizeof(FHairGroupsPhysics), Get_Z_Construct_UScriptStruct_FHairGroupsPhysics_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupsPhysics>()
{
	return FHairGroupsPhysics::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupsPhysics(FHairGroupsPhysics::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupsPhysics"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsPhysics
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsPhysics()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupsPhysics>(FName(TEXT("HairGroupsPhysics")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsPhysics;
	struct Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SolverSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SolverSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExternalForces_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExternalForces;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialConstraints_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MaterialConstraints;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StrandsParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StrandsParameters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupsPhysics>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_SolverSettings_MetaData[] = {
		{ "Category", "GroupsPhysics" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Solver Settings for the hair physics" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_SolverSettings = { "SolverSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsPhysics, SolverSettings), Z_Construct_UScriptStruct_FHairSolverSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_SolverSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_SolverSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_ExternalForces_MetaData[] = {
		{ "Category", "GroupsPhysics" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "External Forces for the hair physics" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_ExternalForces = { "ExternalForces", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsPhysics, ExternalForces), Z_Construct_UScriptStruct_FHairExternalForces, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_ExternalForces_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_ExternalForces_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_MaterialConstraints_MetaData[] = {
		{ "Category", "GroupsPhysics" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Material Constraints for the hair physics" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_MaterialConstraints = { "MaterialConstraints", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsPhysics, MaterialConstraints), Z_Construct_UScriptStruct_FHairMaterialConstraints, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_MaterialConstraints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_MaterialConstraints_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_StrandsParameters_MetaData[] = {
		{ "Category", "GroupsPhysics" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Strands Parameters for the hair physics" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_StrandsParameters = { "StrandsParameters", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsPhysics, StrandsParameters), Z_Construct_UScriptStruct_FHairStrandsParameters, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_StrandsParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_StrandsParameters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_SolverSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_ExternalForces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_MaterialConstraints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::NewProp_StrandsParameters,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGroupsPhysics",
		sizeof(FHairGroupsPhysics),
		alignof(FHairGroupsPhysics),
		Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsPhysics()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupsPhysics_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupsPhysics"), sizeof(FHairGroupsPhysics), Get_Z_Construct_UScriptStruct_FHairGroupsPhysics_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupsPhysics_Hash() { return 1832284U; }
class UScriptStruct* FHairStrandsParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairStrandsParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairStrandsParameters, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairStrandsParameters"), sizeof(FHairStrandsParameters), Get_Z_Construct_UScriptStruct_FHairStrandsParameters_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairStrandsParameters>()
{
	return FHairStrandsParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairStrandsParameters(FHairStrandsParameters::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairStrandsParameters"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairStrandsParameters
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairStrandsParameters()
	{
		UScriptStruct::DeferCppStructOps<FHairStrandsParameters>(FName(TEXT("HairStrandsParameters")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairStrandsParameters;
	struct Z_Construct_UScriptStruct_FHairStrandsParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_StrandsSize_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StrandsSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_StrandsSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StrandsDensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StrandsDensity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StrandsSmoothing_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StrandsSmoothing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StrandsThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StrandsThickness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThicknessScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ThicknessScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairStrandsParameters>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSize_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSize_MetaData[] = {
		{ "Category", "StrandsParameters" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Number of particles per guide that will be used for simulation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSize = { "StrandsSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairStrandsParameters, StrandsSize), Z_Construct_UEnum_HairStrandsCore_EGroomStrandsSize, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsDensity_MetaData[] = {
		{ "Category", "StrandsParameters" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Density of the strands in g/cm3" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsDensity = { "StrandsDensity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairStrandsParameters, StrandsDensity), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsDensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsDensity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSmoothing_MetaData[] = {
		{ "Category", "StrandsParameters" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Smoothing between 0 and 1 of the incoming guides curves for better stability" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSmoothing = { "StrandsSmoothing", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairStrandsParameters, StrandsSmoothing), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSmoothing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSmoothing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsThickness_MetaData[] = {
		{ "Category", "StrandsParameters" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Strands thickness in cm that will be used for mass and inertia computation" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsThickness = { "StrandsThickness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairStrandsParameters, StrandsThickness), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsThickness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsThickness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_ThicknessScale_MetaData[] = {
		{ "Category", "StrandsParameters" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "TimeLineLength", "1.0" },
		{ "ToolTip", "This curve determines how much the strands thickness will be scaled along each strand. \n The X axis range is [0,1], 0 mapping the root and 1 the tip" },
		{ "ViewMaxInput", "1.0" },
		{ "ViewMaxOutput", "1.0" },
		{ "ViewMinInput", "0.0" },
		{ "ViewMinOutput", "0.0" },
		{ "XAxisName", "Strand Coordinate (0,1)" },
		{ "YAxisName", "Strands Thickness" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_ThicknessScale = { "ThicknessScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairStrandsParameters, ThicknessScale), Z_Construct_UScriptStruct_FRuntimeFloatCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_ThicknessScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_ThicknessScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSize_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsDensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsSmoothing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_StrandsThickness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::NewProp_ThicknessScale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairStrandsParameters",
		sizeof(FHairStrandsParameters),
		alignof(FHairStrandsParameters),
		Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairStrandsParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairStrandsParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairStrandsParameters"), sizeof(FHairStrandsParameters), Get_Z_Construct_UScriptStruct_FHairStrandsParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairStrandsParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairStrandsParameters_Hash() { return 2651176366U; }
class UScriptStruct* FHairMaterialConstraints::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairMaterialConstraints_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairMaterialConstraints, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairMaterialConstraints"), sizeof(FHairMaterialConstraints), Get_Z_Construct_UScriptStruct_FHairMaterialConstraints_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairMaterialConstraints>()
{
	return FHairMaterialConstraints::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairMaterialConstraints(FHairMaterialConstraints::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairMaterialConstraints"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairMaterialConstraints
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairMaterialConstraints()
	{
		UScriptStruct::DeferCppStructOps<FHairMaterialConstraints>(FName(TEXT("HairMaterialConstraints")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairMaterialConstraints;
	struct Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BendConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BendConstraint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StretchConstraint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CollisionConstraint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairMaterialConstraints>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_BendConstraint_MetaData[] = {
		{ "Category", "MaterialConstraints" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Bend constraint settings to be applied to the hair strands" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_BendConstraint = { "BendConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairMaterialConstraints, BendConstraint), Z_Construct_UScriptStruct_FHairBendConstraint, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_BendConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_BendConstraint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_StretchConstraint_MetaData[] = {
		{ "Category", "MaterialConstraints" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Stretch constraint settings to be applied to the hair strands" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_StretchConstraint = { "StretchConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairMaterialConstraints, StretchConstraint), Z_Construct_UScriptStruct_FHairStretchConstraint, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_StretchConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_StretchConstraint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_CollisionConstraint_MetaData[] = {
		{ "Category", "MaterialConstraints" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Collision constraint settings to be applied to the hair strands" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_CollisionConstraint = { "CollisionConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairMaterialConstraints, CollisionConstraint), Z_Construct_UScriptStruct_FHairCollisionConstraint, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_CollisionConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_CollisionConstraint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_BendConstraint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_StretchConstraint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::NewProp_CollisionConstraint,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairMaterialConstraints",
		sizeof(FHairMaterialConstraints),
		alignof(FHairMaterialConstraints),
		Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairMaterialConstraints()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairMaterialConstraints_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairMaterialConstraints"), sizeof(FHairMaterialConstraints), Get_Z_Construct_UScriptStruct_FHairMaterialConstraints_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairMaterialConstraints_Hash() { return 2095083649U; }
class UScriptStruct* FHairCollisionConstraint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairCollisionConstraint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairCollisionConstraint, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairCollisionConstraint"), sizeof(FHairCollisionConstraint), Get_Z_Construct_UScriptStruct_FHairCollisionConstraint_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairCollisionConstraint>()
{
	return FHairCollisionConstraint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairCollisionConstraint(FHairCollisionConstraint::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairCollisionConstraint"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCollisionConstraint
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCollisionConstraint()
	{
		UScriptStruct::DeferCppStructOps<FHairCollisionConstraint>(FName(TEXT("HairCollisionConstraint")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCollisionConstraint;
	struct Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SolveCollision_MetaData[];
#endif
		static void NewProp_SolveCollision_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_SolveCollision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectCollision_MetaData[];
#endif
		static void NewProp_ProjectCollision_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ProjectCollision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticFriction_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StaticFriction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KineticFriction_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_KineticFriction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StrandsViscosity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StrandsViscosity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GridDimension_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GridDimension;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CollisionRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadiusScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RadiusScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairCollisionConstraint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_SolveCollision_MetaData[] = {
		{ "Category", "CollisionConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Enable the solve of the collision constraint during the xpbd loop" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_SolveCollision_SetBit(void* Obj)
	{
		((FHairCollisionConstraint*)Obj)->SolveCollision = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_SolveCollision = { "SolveCollision", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairCollisionConstraint), &Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_SolveCollision_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_SolveCollision_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_SolveCollision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_ProjectCollision_MetaData[] = {
		{ "Category", "CollisionConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Enable ther projection of the collision constraint after the xpbd loop" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_ProjectCollision_SetBit(void* Obj)
	{
		((FHairCollisionConstraint*)Obj)->ProjectCollision = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_ProjectCollision = { "ProjectCollision", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairCollisionConstraint), &Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_ProjectCollision_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_ProjectCollision_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_ProjectCollision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_StaticFriction_MetaData[] = {
		{ "Category", "Collision Constraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Static friction used for collision against the physics asset" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_StaticFriction = { "StaticFriction", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCollisionConstraint, StaticFriction), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_StaticFriction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_StaticFriction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_KineticFriction_MetaData[] = {
		{ "Category", "CollisionConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Kinetic friction used for collision against the physics asset" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_KineticFriction = { "KineticFriction", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCollisionConstraint, KineticFriction), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_KineticFriction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_KineticFriction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_StrandsViscosity_MetaData[] = {
		{ "Category", "CollisionConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Viscosity parameter between 0 and 1 that will be used for self collision" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_StrandsViscosity = { "StrandsViscosity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCollisionConstraint, StrandsViscosity), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_StrandsViscosity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_StrandsViscosity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_GridDimension_MetaData[] = {
		{ "Category", "CollisionConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Dimension of the grid used to compute the viscosity force" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_GridDimension = { "GridDimension", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCollisionConstraint, GridDimension), Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_GridDimension_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_GridDimension_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_CollisionRadius_MetaData[] = {
		{ "Category", "CollisionConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Radius that will be used for the collision detection against the physics asset" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_CollisionRadius = { "CollisionRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCollisionConstraint, CollisionRadius), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_CollisionRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_CollisionRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_RadiusScale_MetaData[] = {
		{ "Category", "CollisionConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "TimeLineLength", "1.0" },
		{ "ToolTip", "This curve determines how much the collision radius will be scaled along each strand. \n The X axis range is [0,1], 0 mapping the root and 1 the tip" },
		{ "ViewMaxInput", "1.0" },
		{ "ViewMaxOutput", "1.0" },
		{ "ViewMinInput", "0.0" },
		{ "ViewMinOutput", "0.0" },
		{ "XAxisName", "Strand Coordinate (0,1)" },
		{ "YAxisName", "Collision Radius" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_RadiusScale = { "RadiusScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCollisionConstraint, RadiusScale), Z_Construct_UScriptStruct_FRuntimeFloatCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_RadiusScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_RadiusScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_SolveCollision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_ProjectCollision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_StaticFriction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_KineticFriction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_StrandsViscosity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_GridDimension,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_CollisionRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::NewProp_RadiusScale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairCollisionConstraint",
		sizeof(FHairCollisionConstraint),
		alignof(FHairCollisionConstraint),
		Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairCollisionConstraint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairCollisionConstraint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairCollisionConstraint"), sizeof(FHairCollisionConstraint), Get_Z_Construct_UScriptStruct_FHairCollisionConstraint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairCollisionConstraint_Hash() { return 1112687766U; }
class UScriptStruct* FHairStretchConstraint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairStretchConstraint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairStretchConstraint, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairStretchConstraint"), sizeof(FHairStretchConstraint), Get_Z_Construct_UScriptStruct_FHairStretchConstraint_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairStretchConstraint>()
{
	return FHairStretchConstraint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairStretchConstraint(FHairStretchConstraint::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairStretchConstraint"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairStretchConstraint
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairStretchConstraint()
	{
		UScriptStruct::DeferCppStructOps<FHairStretchConstraint>(FName(TEXT("HairStretchConstraint")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairStretchConstraint;
	struct Z_Construct_UScriptStruct_FHairStretchConstraint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SolveStretch_MetaData[];
#endif
		static void NewProp_SolveStretch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_SolveStretch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectStretch_MetaData[];
#endif
		static void NewProp_ProjectStretch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ProjectStretch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchDamping_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StretchDamping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchStiffness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StretchStiffness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StretchScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StretchScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairStretchConstraint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_SolveStretch_MetaData[] = {
		{ "Category", "StretchConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Enable the solve of the stretch constraint during the xpbd loop" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_SolveStretch_SetBit(void* Obj)
	{
		((FHairStretchConstraint*)Obj)->SolveStretch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_SolveStretch = { "SolveStretch", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairStretchConstraint), &Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_SolveStretch_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_SolveStretch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_SolveStretch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_ProjectStretch_MetaData[] = {
		{ "Category", "StretchConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Enable ther projection of the stretch constraint after the xpbd loop" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_ProjectStretch_SetBit(void* Obj)
	{
		((FHairStretchConstraint*)Obj)->ProjectStretch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_ProjectStretch = { "ProjectStretch", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairStretchConstraint), &Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_ProjectStretch_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_ProjectStretch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_ProjectStretch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchDamping_MetaData[] = {
		{ "Category", "StretchConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Damping for the stretch constraint between 0 and 1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchDamping = { "StretchDamping", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairStretchConstraint, StretchDamping), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchDamping_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchDamping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchStiffness_MetaData[] = {
		{ "Category", "StretchConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Stiffness for the stretch constraint in GPa" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchStiffness = { "StretchStiffness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairStretchConstraint, StretchStiffness), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchStiffness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchStiffness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchScale_MetaData[] = {
		{ "Category", "StretchConstraint" },
		{ "DisplayName", "Stiffness Scale" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "TimeLineLength", "1.0" },
		{ "ToolTip", "This curve determines how much the stretch stiffness will be scaled along each strand. \n The X axis range is [0,1], 0 mapping the root and 1 the tip" },
		{ "ViewMaxInput", "1.0" },
		{ "ViewMaxOutput", "1.0" },
		{ "ViewMinInput", "0.0" },
		{ "ViewMinOutput", "0.0" },
		{ "XAxisName", "Strand Coordinate (0,1)" },
		{ "YAxisName", "Stretch Scale" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchScale = { "StretchScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairStretchConstraint, StretchScale), Z_Construct_UScriptStruct_FRuntimeFloatCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_SolveStretch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_ProjectStretch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchDamping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchStiffness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::NewProp_StretchScale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairStretchConstraint",
		sizeof(FHairStretchConstraint),
		alignof(FHairStretchConstraint),
		Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairStretchConstraint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairStretchConstraint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairStretchConstraint"), sizeof(FHairStretchConstraint), Get_Z_Construct_UScriptStruct_FHairStretchConstraint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairStretchConstraint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairStretchConstraint_Hash() { return 554124095U; }
class UScriptStruct* FHairBendConstraint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairBendConstraint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairBendConstraint, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairBendConstraint"), sizeof(FHairBendConstraint), Get_Z_Construct_UScriptStruct_FHairBendConstraint_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairBendConstraint>()
{
	return FHairBendConstraint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairBendConstraint(FHairBendConstraint::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairBendConstraint"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairBendConstraint
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairBendConstraint()
	{
		UScriptStruct::DeferCppStructOps<FHairBendConstraint>(FName(TEXT("HairBendConstraint")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairBendConstraint;
	struct Z_Construct_UScriptStruct_FHairBendConstraint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SolveBend_MetaData[];
#endif
		static void NewProp_SolveBend_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_SolveBend;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectBend_MetaData[];
#endif
		static void NewProp_ProjectBend_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ProjectBend;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BendDamping_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BendDamping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BendStiffness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BendStiffness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BendScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BendScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairBendConstraint_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairBendConstraint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_SolveBend_MetaData[] = {
		{ "Category", "BendConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Enable the solve of the bend constraint during the xpbd loop" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_SolveBend_SetBit(void* Obj)
	{
		((FHairBendConstraint*)Obj)->SolveBend = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_SolveBend = { "SolveBend", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairBendConstraint), &Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_SolveBend_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_SolveBend_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_SolveBend_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_ProjectBend_MetaData[] = {
		{ "Category", "BendConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Enable ther projection of the bend constraint after the xpbd loop" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_ProjectBend_SetBit(void* Obj)
	{
		((FHairBendConstraint*)Obj)->ProjectBend = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_ProjectBend = { "ProjectBend", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairBendConstraint), &Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_ProjectBend_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_ProjectBend_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_ProjectBend_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendDamping_MetaData[] = {
		{ "Category", "BendConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Damping for the bend constraint between 0 and 1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendDamping = { "BendDamping", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairBendConstraint, BendDamping), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendDamping_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendDamping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendStiffness_MetaData[] = {
		{ "Category", "BendConstraint" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Stiffness for the bend constraint in GPa" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendStiffness = { "BendStiffness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairBendConstraint, BendStiffness), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendStiffness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendStiffness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendScale_MetaData[] = {
		{ "Category", "BendConstraint" },
		{ "DisplayName", "Stiffness Scale" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "TimeLineLength", "1.0" },
		{ "ToolTip", "This curve determines how much the bend stiffness will be scaled along each strand. \n The X axis range is [0,1], 0 mapping the root and 1 the tip" },
		{ "ViewMaxInput", "1.0" },
		{ "ViewMaxOutput", "1.0" },
		{ "ViewMinInput", "0.0" },
		{ "ViewMinOutput", "0.0" },
		{ "XAxisName", "Strand Coordinate (0,1)" },
		{ "YAxisName", "Bend Scale" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendScale = { "BendScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairBendConstraint, BendScale), Z_Construct_UScriptStruct_FRuntimeFloatCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairBendConstraint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_SolveBend,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_ProjectBend,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendDamping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendStiffness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairBendConstraint_Statics::NewProp_BendScale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairBendConstraint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairBendConstraint",
		sizeof(FHairBendConstraint),
		alignof(FHairBendConstraint),
		Z_Construct_UScriptStruct_FHairBendConstraint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairBendConstraint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairBendConstraint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairBendConstraint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairBendConstraint"), sizeof(FHairBendConstraint), Get_Z_Construct_UScriptStruct_FHairBendConstraint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairBendConstraint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairBendConstraint_Hash() { return 3679423437U; }
class UScriptStruct* FHairExternalForces::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairExternalForces_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairExternalForces, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairExternalForces"), sizeof(FHairExternalForces), Get_Z_Construct_UScriptStruct_FHairExternalForces_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairExternalForces>()
{
	return FHairExternalForces::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairExternalForces(FHairExternalForces::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairExternalForces"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairExternalForces
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairExternalForces()
	{
		UScriptStruct::DeferCppStructOps<FHairExternalForces>(FName(TEXT("HairExternalForces")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairExternalForces;
	struct Z_Construct_UScriptStruct_FHairExternalForces_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GravityVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GravityVector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AirDrag_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AirDrag;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AirVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AirVelocity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairExternalForces_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairExternalForces>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_GravityVector_MetaData[] = {
		{ "Category", "ExternalForces" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Acceleration vector in cm/s2 to be used for the gravity" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_GravityVector = { "GravityVector", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairExternalForces, GravityVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_GravityVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_GravityVector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_AirDrag_MetaData[] = {
		{ "Category", "ExternalForces" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Coefficient between 0 and 1 to be used for the air drag" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_AirDrag = { "AirDrag", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairExternalForces, AirDrag), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_AirDrag_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_AirDrag_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_AirVelocity_MetaData[] = {
		{ "Category", "ExternalForces" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Velocity of the surrounding air in cm/s" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_AirVelocity = { "AirVelocity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairExternalForces, AirVelocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_AirVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_AirVelocity_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairExternalForces_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_GravityVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_AirDrag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairExternalForces_Statics::NewProp_AirVelocity,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairExternalForces_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairExternalForces",
		sizeof(FHairExternalForces),
		alignof(FHairExternalForces),
		Z_Construct_UScriptStruct_FHairExternalForces_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairExternalForces_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairExternalForces_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairExternalForces_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairExternalForces()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairExternalForces_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairExternalForces"), sizeof(FHairExternalForces), Get_Z_Construct_UScriptStruct_FHairExternalForces_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairExternalForces_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairExternalForces_Hash() { return 2917374021U; }
class UScriptStruct* FHairSolverSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairSolverSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairSolverSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairSolverSettings"), sizeof(FHairSolverSettings), Get_Z_Construct_UScriptStruct_FHairSolverSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairSolverSettings>()
{
	return FHairSolverSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairSolverSettings(FHairSolverSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairSolverSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairSolverSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairSolverSettings()
	{
		UScriptStruct::DeferCppStructOps<FHairSolverSettings>(FName(TEXT("HairSolverSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairSolverSettings;
	struct Z_Construct_UScriptStruct_FHairSolverSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnableSimulation_MetaData[];
#endif
		static void NewProp_EnableSimulation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_EnableSimulation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NiagaraSolver_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NiagaraSolver_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NiagaraSolver;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomSystem_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_CustomSystem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubSteps_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SubSteps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IterationCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IterationCount;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairSolverSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairSolverSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_EnableSimulation_MetaData[] = {
		{ "Category", "SolverSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Enable the simulation on that group" },
	};
#endif
	void Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_EnableSimulation_SetBit(void* Obj)
	{
		((FHairSolverSettings*)Obj)->EnableSimulation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_EnableSimulation = { "EnableSimulation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairSolverSettings), &Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_EnableSimulation_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_EnableSimulation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_EnableSimulation_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_NiagaraSolver_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_NiagaraSolver_MetaData[] = {
		{ "Category", "SolverSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Niagara solver to be used for simulation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_NiagaraSolver = { "NiagaraSolver", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairSolverSettings, NiagaraSolver), Z_Construct_UEnum_HairStrandsCore_EGroomNiagaraSolvers, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_NiagaraSolver_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_NiagaraSolver_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_CustomSystem_MetaData[] = {
		{ "Category", "SolverSettings" },
		{ "EditCondition", "NiagaraSolver == EGroomNiagaraSolvers::CustomSolver" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Custom niagara system to be used if custom solver is picked" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_CustomSystem = { "CustomSystem", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairSolverSettings, CustomSystem), Z_Construct_UClass_UNiagaraSystem_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_CustomSystem_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_CustomSystem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_SubSteps_MetaData[] = {
		{ "Category", "SolverSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Number of sub steps to be done per frame. The actual solver calls are done at 24 fps" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_SubSteps = { "SubSteps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairSolverSettings, SubSteps), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_SubSteps_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_SubSteps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_IterationCount_MetaData[] = {
		{ "Category", "SolverSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetPhysics.h" },
		{ "ToolTip", "Number of iterations to solve the constraints with the xpbd solver" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_IterationCount = { "IterationCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairSolverSettings, IterationCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_IterationCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_IterationCount_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairSolverSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_EnableSimulation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_NiagaraSolver_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_NiagaraSolver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_CustomSystem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_SubSteps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairSolverSettings_Statics::NewProp_IterationCount,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairSolverSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairSolverSettings",
		sizeof(FHairSolverSettings),
		alignof(FHairSolverSettings),
		Z_Construct_UScriptStruct_FHairSolverSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairSolverSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairSolverSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairSolverSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairSolverSettings"), sizeof(FHairSolverSettings), Get_Z_Construct_UScriptStruct_FHairSolverSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairSolverSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairSolverSettings_Hash() { return 2685126838U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
