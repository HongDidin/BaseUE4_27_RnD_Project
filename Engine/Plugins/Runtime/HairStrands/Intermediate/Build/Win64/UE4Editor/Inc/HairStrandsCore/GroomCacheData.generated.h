// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomCacheData_generated_h
#error "GroomCacheData.generated.h already included, missing '#pragma once' in GroomCacheData.h"
#endif
#define HAIRSTRANDSCORE_GroomCacheData_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheData_h_72_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGroomCacheInfo_Statics; \
	HAIRSTRANDSCORE_API static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FGroomCacheInfo>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheData_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGroomAnimationInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FGroomAnimationInfo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomCacheData_h


#define FOREACH_ENUM_EGROOMCACHETYPE(op) \
	op(EGroomCacheType::None) \
	op(EGroomCacheType::Strands) \
	op(EGroomCacheType::Guides) 

enum class EGroomCacheType : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomCacheType>();

#define FOREACH_ENUM_EGROOMCACHEATTRIBUTES(op) \
	op(EGroomCacheAttributes::None) \
	op(EGroomCacheAttributes::Position) \
	op(EGroomCacheAttributes::Width) \
	op(EGroomCacheAttributes::Color) 

enum class EGroomCacheAttributes : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomCacheAttributes>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
