// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomAssetCards.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomAssetCards() {}
// Cross Module References
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EHairCardsSourceType();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EHairCardsGenerationType();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EHairCardsClusterType();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsProceduralCards();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupCardsTextures();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairGroupCardsInfo();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairCardsClusterSettings();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairCardsGeometrySettings();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FHairCardsTextureSettings();
// End Cross Module References
	static UEnum* EHairCardsSourceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EHairCardsSourceType, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EHairCardsSourceType"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairCardsSourceType>()
	{
		return EHairCardsSourceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHairCardsSourceType(EHairCardsSourceType_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EHairCardsSourceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EHairCardsSourceType_Hash() { return 1432144043U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EHairCardsSourceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHairCardsSourceType"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EHairCardsSourceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHairCardsSourceType::Procedural", (int64)EHairCardsSourceType::Procedural },
				{ "EHairCardsSourceType::Imported", (int64)EHairCardsSourceType::Imported },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Imported.DisplayName", "Imported" },
				{ "Imported.Name", "EHairCardsSourceType::Imported" },
				{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
				{ "Procedural.DisplayName", "Procedural (experimental)" },
				{ "Procedural.Name", "EHairCardsSourceType::Procedural" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EHairCardsSourceType",
				"EHairCardsSourceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EHairCardsGenerationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EHairCardsGenerationType, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EHairCardsGenerationType"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairCardsGenerationType>()
	{
		return EHairCardsGenerationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHairCardsGenerationType(EHairCardsGenerationType_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EHairCardsGenerationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EHairCardsGenerationType_Hash() { return 993632599U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EHairCardsGenerationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHairCardsGenerationType"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EHairCardsGenerationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHairCardsGenerationType::CardsCount", (int64)EHairCardsGenerationType::CardsCount },
				{ "EHairCardsGenerationType::UseGuides", (int64)EHairCardsGenerationType::UseGuides },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "CardsCount.Name", "EHairCardsGenerationType::CardsCount" },
				{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
				{ "UseGuides.Name", "EHairCardsGenerationType::UseGuides" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EHairCardsGenerationType",
				"EHairCardsGenerationType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EHairCardsClusterType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EHairCardsClusterType, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EHairCardsClusterType"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EHairCardsClusterType>()
	{
		return EHairCardsClusterType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHairCardsClusterType(EHairCardsClusterType_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EHairCardsClusterType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EHairCardsClusterType_Hash() { return 1720510625U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EHairCardsClusterType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHairCardsClusterType"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EHairCardsClusterType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHairCardsClusterType::Low", (int64)EHairCardsClusterType::Low },
				{ "EHairCardsClusterType::High", (int64)EHairCardsClusterType::High },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "High.Name", "EHairCardsClusterType::High" },
				{ "Low.Name", "EHairCardsClusterType::Low" },
				{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EHairCardsClusterType",
				"EHairCardsClusterType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FHairGroupsCardsSourceDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupsCardsSourceDescription"), sizeof(FHairGroupsCardsSourceDescription), Get_Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupsCardsSourceDescription>()
{
	return FHairGroupsCardsSourceDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupsCardsSourceDescription(FHairGroupsCardsSourceDescription::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupsCardsSourceDescription"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsCardsSourceDescription
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsCardsSourceDescription()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupsCardsSourceDescription>(FName(TEXT("HairGroupsCardsSourceDescription")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsCardsSourceDescription;
	struct Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSlotName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MaterialSlotName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SourceType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SourceType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProceduralMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ProceduralMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProceduralMeshKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProceduralMeshKey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportedMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportedMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProceduralSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProceduralSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Textures_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Textures;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_GroupIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LODIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CardsInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CardsInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportedMeshKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ImportedMeshKey;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupsCardsSourceDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_Material_MetaData[] = {
		{ "Comment", "/* Deprecated */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Deprecated" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_Material_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_MaterialSlotName_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_MaterialSlotName = { "MaterialSlotName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, MaterialSlotName), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_MaterialSlotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_MaterialSlotName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_SourceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_SourceType_MetaData[] = {
		{ "Category", "CardsSource" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_SourceType = { "SourceType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, SourceType), Z_Construct_UEnum_HairStrandsCore_EHairCardsSourceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_SourceType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_SourceType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralMesh_MetaData[] = {
		{ "Category", "CardsSource" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralMesh = { "ProceduralMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, ProceduralMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralMeshKey_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralMeshKey = { "ProceduralMeshKey", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, ProceduralMeshKey), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralMeshKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralMeshKey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ImportedMesh_MetaData[] = {
		{ "Category", "CardsSource" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ImportedMesh = { "ImportedMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, ImportedMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ImportedMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ImportedMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralSettings_MetaData[] = {
		{ "Category", "CardsSource" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralSettings = { "ProceduralSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, ProceduralSettings), Z_Construct_UScriptStruct_FHairGroupsProceduralCards, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_Textures_MetaData[] = {
		{ "Category", "CardsSource" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_Textures = { "Textures", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, Textures), Z_Construct_UScriptStruct_FHairGroupCardsTextures, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_Textures_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_Textures_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_GroupIndex_MetaData[] = {
		{ "Category", "CardsSource" },
		{ "Comment", "/* Group index on which this cards geometry will be used (#hair_todo: change this to be a dropdown selection menu in FHairLODSettings instead) */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Group index on which this cards geometry will be used (#hair_todo: change this to be a dropdown selection menu in FHairLODSettings instead)" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_GroupIndex = { "GroupIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, GroupIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_GroupIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_GroupIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_LODIndex_MetaData[] = {
		{ "Category", "CardsSource" },
		{ "Comment", "/* LOD on which this cards geometry will be used. -1 means not used  (#hair_todo: change this to be a dropdown selection menu in FHairLODSettings instead) */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "LOD on which this cards geometry will be used. -1 means not used  (#hair_todo: change this to be a dropdown selection menu in FHairLODSettings instead)" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, LODIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_LODIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_LODIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_CardsInfo_MetaData[] = {
		{ "Category", "CardsSource" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_CardsInfo = { "CardsInfo", nullptr, (EPropertyFlags)0x0010000000022001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, CardsInfo), Z_Construct_UScriptStruct_FHairGroupCardsInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_CardsInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_CardsInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ImportedMeshKey_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ImportedMeshKey = { "ImportedMeshKey", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsCardsSourceDescription, ImportedMeshKey), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ImportedMeshKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ImportedMeshKey_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_Material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_MaterialSlotName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_SourceType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_SourceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralMeshKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ImportedMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ProceduralSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_Textures,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_GroupIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_LODIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_CardsInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::NewProp_ImportedMeshKey,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGroupsCardsSourceDescription",
		sizeof(FHairGroupsCardsSourceDescription),
		alignof(FHairGroupsCardsSourceDescription),
		Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupsCardsSourceDescription"), sizeof(FHairGroupsCardsSourceDescription), Get_Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupsCardsSourceDescription_Hash() { return 3153953404U; }
class UScriptStruct* FHairGroupCardsTextures::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupCardsTextures_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupCardsTextures, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupCardsTextures"), sizeof(FHairGroupCardsTextures), Get_Z_Construct_UScriptStruct_FHairGroupCardsTextures_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupCardsTextures>()
{
	return FHairGroupCardsTextures::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupCardsTextures(FHairGroupCardsTextures::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupCardsTextures"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupCardsTextures
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupCardsTextures()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupCardsTextures>(FName(TEXT("HairGroupCardsTextures")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupCardsTextures;
	struct Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DepthTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DepthTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CoverageTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CoverageTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TangentTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TangentTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AttributeTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AuxilaryDataTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AuxilaryDataTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupCardsTextures>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_DepthTexture_MetaData[] = {
		{ "Category", "CardsTextures" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_DepthTexture = { "DepthTexture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupCardsTextures, DepthTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_DepthTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_DepthTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_CoverageTexture_MetaData[] = {
		{ "Category", "CardsTextures" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_CoverageTexture = { "CoverageTexture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupCardsTextures, CoverageTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_CoverageTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_CoverageTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_TangentTexture_MetaData[] = {
		{ "Category", "CardsTextures" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_TangentTexture = { "TangentTexture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupCardsTextures, TangentTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_TangentTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_TangentTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_AttributeTexture_MetaData[] = {
		{ "Category", "CardsAttributes" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_AttributeTexture = { "AttributeTexture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupCardsTextures, AttributeTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_AttributeTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_AttributeTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_AuxilaryDataTexture_MetaData[] = {
		{ "Category", "CardsAuxilaryData" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_AuxilaryDataTexture = { "AuxilaryDataTexture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupCardsTextures, AuxilaryDataTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_AuxilaryDataTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_AuxilaryDataTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_DepthTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_CoverageTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_TangentTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_AttributeTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::NewProp_AuxilaryDataTexture,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGroupCardsTextures",
		sizeof(FHairGroupCardsTextures),
		alignof(FHairGroupCardsTextures),
		Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupCardsTextures()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupCardsTextures_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupCardsTextures"), sizeof(FHairGroupCardsTextures), Get_Z_Construct_UScriptStruct_FHairGroupCardsTextures_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupCardsTextures_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupCardsTextures_Hash() { return 3116948564U; }
class UScriptStruct* FHairGroupCardsInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupCardsInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupCardsInfo, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupCardsInfo"), sizeof(FHairGroupCardsInfo), Get_Z_Construct_UScriptStruct_FHairGroupCardsInfo_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupCardsInfo>()
{
	return FHairGroupCardsInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupCardsInfo(FHairGroupCardsInfo::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupCardsInfo"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupCardsInfo
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupCardsInfo()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupCardsInfo>(FName(TEXT("HairGroupCardsInfo")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupCardsInfo;
	struct Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumCards_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumCards;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumCardVertices_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumCardVertices;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupCardsInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewProp_NumCards_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Card Count" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewProp_NumCards = { "NumCards", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupCardsInfo, NumCards), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewProp_NumCards_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewProp_NumCards_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewProp_NumCardVertices_MetaData[] = {
		{ "Category", "Info" },
		{ "DisplayName", "Card Vertex Count" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewProp_NumCardVertices = { "NumCardVertices", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupCardsInfo, NumCardVertices), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewProp_NumCardVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewProp_NumCardVertices_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewProp_NumCards,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::NewProp_NumCardVertices,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGroupCardsInfo",
		sizeof(FHairGroupCardsInfo),
		alignof(FHairGroupCardsInfo),
		Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupCardsInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupCardsInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupCardsInfo"), sizeof(FHairGroupCardsInfo), Get_Z_Construct_UScriptStruct_FHairGroupCardsInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupCardsInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupCardsInfo_Hash() { return 4126354535U; }
class UScriptStruct* FHairGroupsProceduralCards::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairGroupsProceduralCards, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairGroupsProceduralCards"), sizeof(FHairGroupsProceduralCards), Get_Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairGroupsProceduralCards>()
{
	return FHairGroupsProceduralCards::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairGroupsProceduralCards(FHairGroupsProceduralCards::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairGroupsProceduralCards"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsProceduralCards
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsProceduralCards()
	{
		UScriptStruct::DeferCppStructOps<FHairGroupsProceduralCards>(FName(TEXT("HairGroupsProceduralCards")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairGroupsProceduralCards;
	struct Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClusterSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeometrySettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeometrySettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextureSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Version_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Version;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairGroupsProceduralCards>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_ClusterSettings_MetaData[] = {
		{ "Comment", "/* Deprecated */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Deprecated" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_ClusterSettings = { "ClusterSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsProceduralCards, ClusterSettings), Z_Construct_UScriptStruct_FHairCardsClusterSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_ClusterSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_ClusterSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_GeometrySettings_MetaData[] = {
		{ "Category", "GeometrySettings" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Cards geometry settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_GeometrySettings = { "GeometrySettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsProceduralCards, GeometrySettings), Z_Construct_UScriptStruct_FHairCardsGeometrySettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_GeometrySettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_GeometrySettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_TextureSettings_MetaData[] = {
		{ "Category", "TextureSettings" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Cards texture atlast settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_TextureSettings = { "TextureSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsProceduralCards, TextureSettings), Z_Construct_UScriptStruct_FHairCardsTextureSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_TextureSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_TextureSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_Version_MetaData[] = {
		{ "Comment", "/* Use to track when a cards asset need to be regenerated */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Use to track when a cards asset need to be regenerated" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_Version = { "Version", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairGroupsProceduralCards, Version), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_Version_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_Version_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_ClusterSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_GeometrySettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_TextureSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::NewProp_Version,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairGroupsProceduralCards",
		sizeof(FHairGroupsProceduralCards),
		alignof(FHairGroupsProceduralCards),
		Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairGroupsProceduralCards()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairGroupsProceduralCards"), sizeof(FHairGroupsProceduralCards), Get_Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairGroupsProceduralCards_Hash() { return 3289899707U; }
class UScriptStruct* FHairCardsTextureSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairCardsTextureSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairCardsTextureSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairCardsTextureSettings"), sizeof(FHairCardsTextureSettings), Get_Z_Construct_UScriptStruct_FHairCardsTextureSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairCardsTextureSettings>()
{
	return FHairCardsTextureSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairCardsTextureSettings(FHairCardsTextureSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairCardsTextureSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCardsTextureSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCardsTextureSettings()
	{
		UScriptStruct::DeferCppStructOps<FHairCardsTextureSettings>(FName(TEXT("HairCardsTextureSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCardsTextureSettings;
	struct Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AtlasMaxResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AtlasMaxResolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PixelPerCentimeters_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PixelPerCentimeters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LengthTextureCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LengthTextureCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DensityTextureCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DensityTextureCount;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairCardsTextureSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_AtlasMaxResolution_MetaData[] = {
		{ "Category", "AtlasSettings" },
		{ "ClampMin", "512" },
		{ "Comment", "/** Max atlas resolution */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Max atlas resolution" },
		{ "UIMax", "8192" },
		{ "UIMin", "512" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_AtlasMaxResolution = { "AtlasMaxResolution", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsTextureSettings, AtlasMaxResolution), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_AtlasMaxResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_AtlasMaxResolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_PixelPerCentimeters_MetaData[] = {
		{ "Category", "AtlasSettings" },
		{ "ClampMin", "4" },
		{ "Comment", "/** Pixel resolution per centimeters */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Pixel resolution per centimeters" },
		{ "UIMax", "128" },
		{ "UIMin", "2" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_PixelPerCentimeters = { "PixelPerCentimeters", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsTextureSettings, PixelPerCentimeters), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_PixelPerCentimeters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_PixelPerCentimeters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_LengthTextureCount_MetaData[] = {
		{ "Category", "AtlasSettings" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of unique clump textures*/" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Number of unique clump textures" },
		{ "UIMax", "128" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_LengthTextureCount = { "LengthTextureCount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsTextureSettings, LengthTextureCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_LengthTextureCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_LengthTextureCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_DensityTextureCount_MetaData[] = {
		{ "Comment", "/** Number of texture having variation of strands count *///UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = \"AtlasSettings\", meta = (ClampMin = \"1\", UIMin = \"1\", UIMax = \"128\"))\n" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Number of texture having variation of strands count //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = \"AtlasSettings\", meta = (ClampMin = \"1\", UIMin = \"1\", UIMax = \"128\"))" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_DensityTextureCount = { "DensityTextureCount", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsTextureSettings, DensityTextureCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_DensityTextureCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_DensityTextureCount_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_AtlasMaxResolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_PixelPerCentimeters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_LengthTextureCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::NewProp_DensityTextureCount,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairCardsTextureSettings",
		sizeof(FHairCardsTextureSettings),
		alignof(FHairCardsTextureSettings),
		Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairCardsTextureSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairCardsTextureSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairCardsTextureSettings"), sizeof(FHairCardsTextureSettings), Get_Z_Construct_UScriptStruct_FHairCardsTextureSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairCardsTextureSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairCardsTextureSettings_Hash() { return 1145208004U; }
class UScriptStruct* FHairCardsGeometrySettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairCardsGeometrySettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairCardsGeometrySettings"), sizeof(FHairCardsGeometrySettings), Get_Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairCardsGeometrySettings>()
{
	return FHairCardsGeometrySettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairCardsGeometrySettings(FHairCardsGeometrySettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairCardsGeometrySettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCardsGeometrySettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCardsGeometrySettings()
	{
		UScriptStruct::DeferCppStructOps<FHairCardsGeometrySettings>(FName(TEXT("HairCardsGeometrySettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCardsGeometrySettings;
	struct Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_GenerationType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenerationType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_GenerationType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CardsCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CardsCount;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ClusterType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ClusterType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinSegmentLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinSegmentLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngularThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngularThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinCardsLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinCardsLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxCardsLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxCardsLength;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairCardsGeometrySettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_GenerationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_GenerationType_MetaData[] = {
		{ "Category", "ClusterSettings" },
		{ "Comment", "/** Define how cards should be generated. Cards count: define a targeted number of cards. Use guides: use simulation guide as cards. */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Define how cards should be generated. Cards count: define a targeted number of cards. Use guides: use simulation guide as cards." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_GenerationType = { "GenerationType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsGeometrySettings, GenerationType), Z_Construct_UEnum_HairStrandsCore_EHairCardsGenerationType, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_GenerationType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_GenerationType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_CardsCount_MetaData[] = {
		{ "Category", "ClusterSettings" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Define how many cards should be generated. The generated number can be lower, as some cards can be discarded by other options. */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Define how many cards should be generated. The generated number can be lower, as some cards can be discarded by other options." },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_CardsCount = { "CardsCount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsGeometrySettings, CardsCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_CardsCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_CardsCount_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_ClusterType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_ClusterType_MetaData[] = {
		{ "Category", "ClusterSettings" },
		{ "Comment", "/** Quality of clustering when group hair to belong to a similar cards. This does not change the number cards, but only how cards are shaped (size/shape) */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Quality of clustering when group hair to belong to a similar cards. This does not change the number cards, but only how cards are shaped (size/shape)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_ClusterType = { "ClusterType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsGeometrySettings, ClusterType), Z_Construct_UEnum_HairStrandsCore_EHairCardsClusterType, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_ClusterType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_ClusterType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MinSegmentLength_MetaData[] = {
		{ "Category", "GeometrySettings" },
		{ "ClampMin", "0.1" },
		{ "Comment", "/** Minimum size of a card segment */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "SliderExponent", "6" },
		{ "ToolTip", "Minimum size of a card segment" },
		{ "UIMax", "8.0" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MinSegmentLength = { "MinSegmentLength", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsGeometrySettings, MinSegmentLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MinSegmentLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MinSegmentLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_AngularThreshold_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMax", "45" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Max angular difference between adjacents vertices to remove vertices during simplification with MinSegmentLength, in degrees. */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Max angular difference between adjacents vertices to remove vertices during simplification with MinSegmentLength, in degrees." },
		{ "UIMax", "45.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_AngularThreshold = { "AngularThreshold", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsGeometrySettings, AngularThreshold), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_AngularThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_AngularThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MinCardsLength_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Length below which generated cards are discard, as there are considered too small. (Default:0, which means no trimming) */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Length below which generated cards are discard, as there are considered too small. (Default:0, which means no trimming)" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MinCardsLength = { "MinCardsLength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsGeometrySettings, MinCardsLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MinCardsLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MinCardsLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MaxCardsLength_MetaData[] = {
		{ "Category", "DecimationSettings" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Length above which generated cards are discard, as there are considered too larger. (Default:0, which means no trimming) */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Length above which generated cards are discard, as there are considered too larger. (Default:0, which means no trimming)" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MaxCardsLength = { "MaxCardsLength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsGeometrySettings, MaxCardsLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MaxCardsLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MaxCardsLength_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_GenerationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_GenerationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_CardsCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_ClusterType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_ClusterType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MinSegmentLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_AngularThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MinCardsLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::NewProp_MaxCardsLength,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairCardsGeometrySettings",
		sizeof(FHairCardsGeometrySettings),
		alignof(FHairCardsGeometrySettings),
		Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairCardsGeometrySettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairCardsGeometrySettings"), sizeof(FHairCardsGeometrySettings), Get_Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairCardsGeometrySettings_Hash() { return 3173435666U; }
class UScriptStruct* FHairCardsClusterSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FHairCardsClusterSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHairCardsClusterSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("HairCardsClusterSettings"), sizeof(FHairCardsClusterSettings), Get_Z_Construct_UScriptStruct_FHairCardsClusterSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FHairCardsClusterSettings>()
{
	return FHairCardsClusterSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHairCardsClusterSettings(FHairCardsClusterSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("HairCardsClusterSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCardsClusterSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCardsClusterSettings()
	{
		UScriptStruct::DeferCppStructOps<FHairCardsClusterSettings>(FName(TEXT("HairCardsClusterSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFHairCardsClusterSettings;
	struct Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterDecimation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterDecimation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseGuide_MetaData[];
#endif
		static void NewProp_bUseGuide_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseGuide;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/* Deprecated */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Deprecated" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHairCardsClusterSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_ClusterDecimation_MetaData[] = {
		{ "Comment", "/** Decimation factor use to initialized cluster (only used when UseGuide is disabled). This changes the number of generated cards */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Decimation factor use to initialized cluster (only used when UseGuide is disabled). This changes the number of generated cards" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_ClusterDecimation = { "ClusterDecimation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsClusterSettings, ClusterDecimation), METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_ClusterDecimation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_ClusterDecimation_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_Type_MetaData[] = {
		{ "Comment", "/** Quality of clustering when group hair to cluster center. This does not change the number cards, but only how cards are shaped (size/shape) */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Quality of clustering when group hair to cluster center. This does not change the number cards, but only how cards are shaped (size/shape)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHairCardsClusterSettings, Type), Z_Construct_UEnum_HairStrandsCore_EHairCardsClusterType, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_bUseGuide_MetaData[] = {
		{ "Comment", "/** Use the simulation guide to generate the cards instead of the decimation parameter. This changes the number of generated cards. */" },
		{ "ModuleRelativePath", "Public/GroomAssetCards.h" },
		{ "ToolTip", "Use the simulation guide to generate the cards instead of the decimation parameter. This changes the number of generated cards." },
	};
#endif
	void Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_bUseGuide_SetBit(void* Obj)
	{
		((FHairCardsClusterSettings*)Obj)->bUseGuide = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_bUseGuide = { "bUseGuide", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FHairCardsClusterSettings), &Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_bUseGuide_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_bUseGuide_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_bUseGuide_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_ClusterDecimation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::NewProp_bUseGuide,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"HairCardsClusterSettings",
		sizeof(FHairCardsClusterSettings),
		alignof(FHairCardsClusterSettings),
		Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHairCardsClusterSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHairCardsClusterSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HairCardsClusterSettings"), sizeof(FHairCardsClusterSettings), Get_Z_Construct_UScriptStruct_FHairCardsClusterSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHairCardsClusterSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHairCardsClusterSettings_Hash() { return 2019210684U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
