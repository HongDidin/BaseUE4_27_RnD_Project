// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomCreateBindingOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomCreateBindingOptions() {}
// Cross Module References
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCreateBindingOptions_NoRegister();
	HAIRSTRANDSCORE_API UClass* Z_Construct_UClass_UGroomCreateBindingOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomBindingMeshType();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMesh_NoRegister();
	GEOMETRYCACHE_API UClass* Z_Construct_UClass_UGeometryCache_NoRegister();
// End Cross Module References
	void UGroomCreateBindingOptions::StaticRegisterNativesUGroomCreateBindingOptions()
	{
	}
	UClass* Z_Construct_UClass_UGroomCreateBindingOptions_NoRegister()
	{
		return UGroomCreateBindingOptions::StaticClass();
	}
	struct Z_Construct_UClass_UGroomCreateBindingOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_GroomBindingType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroomBindingType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_GroomBindingType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceSkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceSkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetSkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetSkeletalMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceGeometryCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceGeometryCache;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetGeometryCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetGeometryCache;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumInterpolationPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumInterpolationPoints;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MatchingSection_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MatchingSection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroomCreateBindingOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateBindingOptions_Statics::Class_MetaDataParams[] = {
		{ "AutoCollapseCategories", "Conversion" },
		{ "BlueprintType", "true" },
		{ "HideCategories", "Hidden" },
		{ "IncludePath", "GroomCreateBindingOptions.h" },
		{ "ModuleRelativePath", "Public/GroomCreateBindingOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_GroomBindingType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_GroomBindingType_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Type of mesh to create groom binding for */" },
		{ "ModuleRelativePath", "Public/GroomCreateBindingOptions.h" },
		{ "ToolTip", "Type of mesh to create groom binding for" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_GroomBindingType = { "GroomBindingType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateBindingOptions, GroomBindingType), Z_Construct_UEnum_HairStrandsCore_EGroomBindingMeshType, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_GroomBindingType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_GroomBindingType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_SourceSkeletalMesh_MetaData[] = {
		{ "Category", "Conversion" },
		{ "Comment", "/** Skeletal mesh on which the groom has been authored. This is optional, and used only if the hair\n\x09\x09""binding is done a different mesh than the one which it has been authored, i.e., only if the curves \n\x09\x09roots and the surface geometry don't aligned and need to be wrapped/transformed. */" },
		{ "ModuleRelativePath", "Public/GroomCreateBindingOptions.h" },
		{ "ToolTip", "Skeletal mesh on which the groom has been authored. This is optional, and used only if the hair\n              binding is done a different mesh than the one which it has been authored, i.e., only if the curves\n              roots and the surface geometry don't aligned and need to be wrapped/transformed." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_SourceSkeletalMesh = { "SourceSkeletalMesh", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateBindingOptions, SourceSkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_SourceSkeletalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_SourceSkeletalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_TargetSkeletalMesh_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Skeletal mesh on which the groom is attached to. */" },
		{ "ModuleRelativePath", "Public/GroomCreateBindingOptions.h" },
		{ "ToolTip", "Skeletal mesh on which the groom is attached to." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_TargetSkeletalMesh = { "TargetSkeletalMesh", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateBindingOptions, TargetSkeletalMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_TargetSkeletalMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_TargetSkeletalMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_SourceGeometryCache_MetaData[] = {
		{ "Category", "Conversion" },
		{ "Comment", "/** GeometryCache on which the groom has been authored */" },
		{ "ModuleRelativePath", "Public/GroomCreateBindingOptions.h" },
		{ "ToolTip", "GeometryCache on which the groom has been authored" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_SourceGeometryCache = { "SourceGeometryCache", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateBindingOptions, SourceGeometryCache), Z_Construct_UClass_UGeometryCache_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_SourceGeometryCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_SourceGeometryCache_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_TargetGeometryCache_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** GeometryCache on which the groom is attached to. */" },
		{ "ModuleRelativePath", "Public/GroomCreateBindingOptions.h" },
		{ "ToolTip", "GeometryCache on which the groom is attached to." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_TargetGeometryCache = { "TargetGeometryCache", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateBindingOptions, TargetGeometryCache), Z_Construct_UClass_UGeometryCache_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_TargetGeometryCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_TargetGeometryCache_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_NumInterpolationPoints_MetaData[] = {
		{ "Category", "HairInterpolation" },
		{ "ClampMax", "100" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of points used for the rbf interpolation */" },
		{ "ModuleRelativePath", "Public/GroomCreateBindingOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Number of points used for the rbf interpolation" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_NumInterpolationPoints = { "NumInterpolationPoints", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateBindingOptions, NumInterpolationPoints), METADATA_PARAMS(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_NumInterpolationPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_NumInterpolationPoints_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_MatchingSection_MetaData[] = {
		{ "Category", "Conversion" },
		{ "Comment", "/** Section to pick to transfer the position */" },
		{ "ModuleRelativePath", "Public/GroomCreateBindingOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Section to pick to transfer the position" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_MatchingSection = { "MatchingSection", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroomCreateBindingOptions, MatchingSection), METADATA_PARAMS(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_MatchingSection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_MatchingSection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroomCreateBindingOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_GroomBindingType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_GroomBindingType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_SourceSkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_TargetSkeletalMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_SourceGeometryCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_TargetGeometryCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_NumInterpolationPoints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroomCreateBindingOptions_Statics::NewProp_MatchingSection,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroomCreateBindingOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroomCreateBindingOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroomCreateBindingOptions_Statics::ClassParams = {
		&UGroomCreateBindingOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroomCreateBindingOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroomCreateBindingOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroomCreateBindingOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroomCreateBindingOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroomCreateBindingOptions, 2163418186);
	template<> HAIRSTRANDSCORE_API UClass* StaticClass<UGroomCreateBindingOptions>()
	{
		return UGroomCreateBindingOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroomCreateBindingOptions(Z_Construct_UClass_UGroomCreateBindingOptions, &UGroomCreateBindingOptions::StaticClass, TEXT("/Script/HairStrandsCore"), TEXT("UGroomCreateBindingOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroomCreateBindingOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
