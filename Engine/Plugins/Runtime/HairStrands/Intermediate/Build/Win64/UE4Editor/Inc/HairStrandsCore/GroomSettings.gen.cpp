// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsCore/Public/GroomSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroomSettings() {}
// Cross Module References
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationWeight();
	UPackage* Z_Construct_UPackage__Script_HairStrandsCore();
	HAIRSTRANDSCORE_API UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationQuality();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FGroomBuildSettings();
	HAIRSTRANDSCORE_API UScriptStruct* Z_Construct_UScriptStruct_FGroomConversionSettings();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	static UEnum* EGroomInterpolationWeight_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationWeight, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EGroomInterpolationWeight"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomInterpolationWeight>()
	{
		return EGroomInterpolationWeight_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroomInterpolationWeight(EGroomInterpolationWeight_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EGroomInterpolationWeight"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationWeight_Hash() { return 93717821U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationWeight()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroomInterpolationWeight"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationWeight_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroomInterpolationWeight::Parametric", (int64)EGroomInterpolationWeight::Parametric },
				{ "EGroomInterpolationWeight::Root", (int64)EGroomInterpolationWeight::Root },
				{ "EGroomInterpolationWeight::Index", (int64)EGroomInterpolationWeight::Index },
				{ "EGroomInterpolationWeight::Unknown", (int64)EGroomInterpolationWeight::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Index.DisplayName", "Index" },
				{ "Index.Name", "EGroomInterpolationWeight::Index" },
				{ "Index.ToolTip", "Build interpolation data based on guide and strands vertex indices" },
				{ "ModuleRelativePath", "Public/GroomSettings.h" },
				{ "Parametric.DisplayName", "Parametric" },
				{ "Parametric.Name", "EGroomInterpolationWeight::Parametric" },
				{ "Parametric.ToolTip", "Build interpolation data based on curve parametric distance" },
				{ "Root.DisplayName", "Root" },
				{ "Root.Name", "EGroomInterpolationWeight::Root" },
				{ "Root.ToolTip", "Build interpolation data based on distance between guide's root and strands's root" },
				{ "Unknown.Hidden", "" },
				{ "Unknown.Name", "EGroomInterpolationWeight::Unknown" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EGroomInterpolationWeight",
				"EGroomInterpolationWeight",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EGroomInterpolationQuality_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationQuality, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("EGroomInterpolationQuality"));
		}
		return Singleton;
	}
	template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomInterpolationQuality>()
	{
		return EGroomInterpolationQuality_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroomInterpolationQuality(EGroomInterpolationQuality_StaticEnum, TEXT("/Script/HairStrandsCore"), TEXT("EGroomInterpolationQuality"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationQuality_Hash() { return 2499995918U; }
	UEnum* Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationQuality()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroomInterpolationQuality"), 0, Get_Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationQuality_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroomInterpolationQuality::Low", (int64)EGroomInterpolationQuality::Low },
				{ "EGroomInterpolationQuality::Medium", (int64)EGroomInterpolationQuality::Medium },
				{ "EGroomInterpolationQuality::High", (int64)EGroomInterpolationQuality::High },
				{ "EGroomInterpolationQuality::Unknown", (int64)EGroomInterpolationQuality::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "High.DisplayName", "High" },
				{ "High.Name", "EGroomInterpolationQuality::High" },
				{ "High.ToolTip", "Build interpolation data using curve shape matching search. This result in high quality interpolation data, but is relatively slow to build (can takes several dozen of minutes)" },
				{ "Low.DisplayName", "Low" },
				{ "Low.Name", "EGroomInterpolationQuality::Low" },
				{ "Low.ToolTip", "Build interpolation data based on nearst neighbor search. Low quality interpolation data, but fast to build (takes a few minutes)" },
				{ "Medium.DisplayName", "Medium" },
				{ "Medium.Name", "EGroomInterpolationQuality::Medium" },
				{ "Medium.ToolTip", "Build interpolation data using curve shape matching search but within a limited spatial range. This is a tradeoff between Low and high quality in term of quality & build time (can takes several dozen of minutes)" },
				{ "ModuleRelativePath", "Public/GroomSettings.h" },
				{ "Unknown.Hidden", "" },
				{ "Unknown.Name", "EGroomInterpolationQuality::Unknown" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_HairStrandsCore,
				nullptr,
				"EGroomInterpolationQuality",
				"EGroomInterpolationQuality",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FGroomBuildSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FGroomBuildSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGroomBuildSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("GroomBuildSettings"), sizeof(FGroomBuildSettings), Get_Z_Construct_UScriptStruct_FGroomBuildSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FGroomBuildSettings>()
{
	return FGroomBuildSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGroomBuildSettings(FGroomBuildSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("GroomBuildSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomBuildSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomBuildSettings()
	{
		UScriptStruct::DeferCppStructOps<FGroomBuildSettings>(FName(TEXT("GroomBuildSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomBuildSettings;
	struct Z_Construct_UScriptStruct_FGroomBuildSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideGuides_MetaData[];
#endif
		static void NewProp_bOverrideGuides_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideGuides;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HairToGuideDensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HairToGuideDensity;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InterpolationQuality_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationQuality_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InterpolationQuality;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InterpolationDistance_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InterpolationDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeGuide_MetaData[];
#endif
		static void NewProp_bRandomizeGuide_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeGuide;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseUniqueGuide_MetaData[];
#endif
		static void NewProp_bUseUniqueGuide_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseUniqueGuide;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGroomBuildSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bOverrideGuides_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "ModuleRelativePath", "Public/GroomSettings.h" },
		{ "ToolTip", "If checked, override imported guides with generated ones." },
	};
#endif
	void Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bOverrideGuides_SetBit(void* Obj)
	{
		((FGroomBuildSettings*)Obj)->bOverrideGuides = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bOverrideGuides = { "bOverrideGuides", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FGroomBuildSettings), &Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bOverrideGuides_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bOverrideGuides_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bOverrideGuides_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_HairToGuideDensity_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Density factor for converting hair into guide curve if no guides are provided. */" },
		{ "ModuleRelativePath", "Public/GroomSettings.h" },
		{ "ToolTip", "Density factor for converting hair into guide curve if no guides are provided." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_HairToGuideDensity = { "HairToGuideDensity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomBuildSettings, HairToGuideDensity), METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_HairToGuideDensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_HairToGuideDensity_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationQuality_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationQuality_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Interpolation data quality. */" },
		{ "ModuleRelativePath", "Public/GroomSettings.h" },
		{ "ToolTip", "Interpolation data quality." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationQuality = { "InterpolationQuality", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomBuildSettings, InterpolationQuality), Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationQuality, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationQuality_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationQuality_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationDistance_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationDistance_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Interpolation distance metric. */" },
		{ "ModuleRelativePath", "Public/GroomSettings.h" },
		{ "ToolTip", "Interpolation distance metric." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationDistance = { "InterpolationDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomBuildSettings, InterpolationDistance), Z_Construct_UEnum_HairStrandsCore_EGroomInterpolationWeight, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bRandomizeGuide_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Randomize which guides affect a given hair strand. */" },
		{ "ModuleRelativePath", "Public/GroomSettings.h" },
		{ "ToolTip", "Randomize which guides affect a given hair strand." },
	};
#endif
	void Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bRandomizeGuide_SetBit(void* Obj)
	{
		((FGroomBuildSettings*)Obj)->bRandomizeGuide = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bRandomizeGuide = { "bRandomizeGuide", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FGroomBuildSettings), &Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bRandomizeGuide_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bRandomizeGuide_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bRandomizeGuide_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bUseUniqueGuide_MetaData[] = {
		{ "Category", "BuildSettings" },
		{ "Comment", "/** Force a hair strand to be affected by a unique guide. */" },
		{ "ModuleRelativePath", "Public/GroomSettings.h" },
		{ "ToolTip", "Force a hair strand to be affected by a unique guide." },
	};
#endif
	void Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bUseUniqueGuide_SetBit(void* Obj)
	{
		((FGroomBuildSettings*)Obj)->bUseUniqueGuide = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bUseUniqueGuide = { "bUseUniqueGuide", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FGroomBuildSettings), &Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bUseUniqueGuide_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bUseUniqueGuide_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bUseUniqueGuide_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bOverrideGuides,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_HairToGuideDensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationQuality_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationQuality,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationDistance_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_InterpolationDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bRandomizeGuide,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::NewProp_bUseUniqueGuide,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"GroomBuildSettings",
		sizeof(FGroomBuildSettings),
		alignof(FGroomBuildSettings),
		Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGroomBuildSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGroomBuildSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GroomBuildSettings"), sizeof(FGroomBuildSettings), Get_Z_Construct_UScriptStruct_FGroomBuildSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGroomBuildSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGroomBuildSettings_Hash() { return 2979956005U; }
class UScriptStruct* FGroomConversionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HAIRSTRANDSCORE_API uint32 Get_Z_Construct_UScriptStruct_FGroomConversionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGroomConversionSettings, Z_Construct_UPackage__Script_HairStrandsCore(), TEXT("GroomConversionSettings"), sizeof(FGroomConversionSettings), Get_Z_Construct_UScriptStruct_FGroomConversionSettings_Hash());
	}
	return Singleton;
}
template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<FGroomConversionSettings>()
{
	return FGroomConversionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGroomConversionSettings(FGroomConversionSettings::StaticStruct, TEXT("/Script/HairStrandsCore"), TEXT("GroomConversionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomConversionSettings
{
	FScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomConversionSettings()
	{
		UScriptStruct::DeferCppStructOps<FGroomConversionSettings>(FName(TEXT("GroomConversionSettings")));
	}
} ScriptStruct_HairStrandsCore_StaticRegisterNativesFGroomConversionSettings;
	struct Z_Construct_UScriptStruct_FGroomConversionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/GroomSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGroomConversionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewProp_Rotation_MetaData[] = {
		{ "Category", "Conversion" },
		{ "Comment", "/** Rotation in Euler angles in degrees to fix up or front axes */" },
		{ "ModuleRelativePath", "Public/GroomSettings.h" },
		{ "ToolTip", "Rotation in Euler angles in degrees to fix up or front axes" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomConversionSettings, Rotation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewProp_Scale_MetaData[] = {
		{ "Category", "Conversion" },
		{ "Comment", "/** Scale value to convert file unit into centimeters */" },
		{ "ModuleRelativePath", "Public/GroomSettings.h" },
		{ "ToolTip", "Scale value to convert file unit into centimeters" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGroomConversionSettings, Scale), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewProp_Scale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::NewProp_Scale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsCore,
		nullptr,
		&NewStructOps,
		"GroomConversionSettings",
		sizeof(FGroomConversionSettings),
		alignof(FGroomConversionSettings),
		Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGroomConversionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGroomConversionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HairStrandsCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GroomConversionSettings"), sizeof(FGroomConversionSettings), Get_Z_Construct_UScriptStruct_FGroomConversionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGroomConversionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGroomConversionSettings_Hash() { return 3020271097U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
