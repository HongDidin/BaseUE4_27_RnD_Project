// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomAssetPhysics_generated_h
#error "GroomAssetPhysics.generated.h already included, missing '#pragma once' in GroomAssetPhysics.h"
#endif
#define HAIRSTRANDSCORE_GroomAssetPhysics_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetPhysics_h_255_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairGroupsPhysics_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairGroupsPhysics>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetPhysics_h_225_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairStrandsParameters_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairStrandsParameters>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetPhysics_h_203_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairMaterialConstraints_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairMaterialConstraints>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetPhysics_h_160_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairCollisionConstraint_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairCollisionConstraint>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetPhysics_h_130_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairStretchConstraint_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairStretchConstraint>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetPhysics_h_99_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairBendConstraint_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairBendConstraint>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetPhysics_h_77_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairExternalForces_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairExternalForces>();

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetPhysics_h_47_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHairSolverSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> HAIRSTRANDSCORE_API UScriptStruct* StaticStruct<struct FHairSolverSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomAssetPhysics_h


#define FOREACH_ENUM_EGROOMINTERPOLATIONTYPE(op) \
	op(EGroomInterpolationType::None) \
	op(EGroomInterpolationType::RigidTransform) \
	op(EGroomInterpolationType::OffsetTransform) \
	op(EGroomInterpolationType::SmoothTransform) 

enum class EGroomInterpolationType : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomInterpolationType>();

#define FOREACH_ENUM_EGROOMSTRANDSSIZE(op) \
	op(EGroomStrandsSize::None) \
	op(EGroomStrandsSize::Size2) \
	op(EGroomStrandsSize::Size4) \
	op(EGroomStrandsSize::Size8) \
	op(EGroomStrandsSize::Size16) \
	op(EGroomStrandsSize::Size32) 

enum class EGroomStrandsSize : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomStrandsSize>();

#define FOREACH_ENUM_EGROOMNIAGARASOLVERS(op) \
	op(EGroomNiagaraSolvers::None) \
	op(EGroomNiagaraSolvers::CosseratRods) \
	op(EGroomNiagaraSolvers::AngularSprings) \
	op(EGroomNiagaraSolvers::CustomSolver) 

enum class EGroomNiagaraSolvers : uint8;
template<> HAIRSTRANDSCORE_API UEnum* StaticEnum<EGroomNiagaraSolvers>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
