// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HairStrandsEditor/Public/ReimportHairStrandsFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReimportHairStrandsFactory() {}
// Cross Module References
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UReimportHairStrandsFactory_NoRegister();
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UReimportHairStrandsFactory();
	HAIRSTRANDSEDITOR_API UClass* Z_Construct_UClass_UHairStrandsFactory();
	UPackage* Z_Construct_UPackage__Script_HairStrandsEditor();
// End Cross Module References
	void UReimportHairStrandsFactory::StaticRegisterNativesUReimportHairStrandsFactory()
	{
	}
	UClass* Z_Construct_UClass_UReimportHairStrandsFactory_NoRegister()
	{
		return UReimportHairStrandsFactory::StaticClass();
	}
	struct Z_Construct_UClass_UReimportHairStrandsFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReimportHairStrandsFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UHairStrandsFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_HairStrandsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReimportHairStrandsFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "ReimportHairStrandsFactory.h" },
		{ "ModuleRelativePath", "Public/ReimportHairStrandsFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReimportHairStrandsFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReimportHairStrandsFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReimportHairStrandsFactory_Statics::ClassParams = {
		&UReimportHairStrandsFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UReimportHairStrandsFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReimportHairStrandsFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReimportHairStrandsFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReimportHairStrandsFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReimportHairStrandsFactory, 4038677636);
	template<> HAIRSTRANDSEDITOR_API UClass* StaticClass<UReimportHairStrandsFactory>()
	{
		return UReimportHairStrandsFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReimportHairStrandsFactory(Z_Construct_UClass_UReimportHairStrandsFactory, &UReimportHairStrandsFactory::StaticClass, TEXT("/Script/HairStrandsEditor"), TEXT("UReimportHairStrandsFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReimportHairStrandsFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
