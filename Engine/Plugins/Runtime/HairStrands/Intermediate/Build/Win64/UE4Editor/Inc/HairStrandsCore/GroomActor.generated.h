// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HAIRSTRANDSCORE_GroomActor_generated_h
#error "GroomActor.generated.h already included, missing '#pragma once' in GroomActor.h"
#endif
#define HAIRSTRANDSCORE_GroomActor_generated_h

#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_SPARSE_DATA
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_RPC_WRAPPERS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGroomActor(); \
	friend struct Z_Construct_UClass_AGroomActor_Statics; \
public: \
	DECLARE_CLASS(AGroomActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(AGroomActor)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAGroomActor(); \
	friend struct Z_Construct_UClass_AGroomActor_Statics; \
public: \
	DECLARE_CLASS(AGroomActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HairStrandsCore"), NO_API) \
	DECLARE_SERIALIZER(AGroomActor)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGroomActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGroomActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGroomActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGroomActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGroomActor(AGroomActor&&); \
	NO_API AGroomActor(const AGroomActor&); \
public:


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGroomActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGroomActor(AGroomActor&&); \
	NO_API AGroomActor(const AGroomActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGroomActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGroomActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGroomActor)


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_12_PROLOG
#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_RPC_WRAPPERS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_INCLASS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GroomActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HAIRSTRANDSCORE_API UClass* StaticClass<class AGroomActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_HairStrands_Source_HairStrandsCore_Public_GroomActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
