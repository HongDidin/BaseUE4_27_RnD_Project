// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoundVisualizations/Classes/SoundVisualizationStatics.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundVisualizationStatics() {}
// Cross Module References
	SOUNDVISUALIZATIONS_API UClass* Z_Construct_UClass_USoundVisualizationStatics_NoRegister();
	SOUNDVISUALIZATIONS_API UClass* Z_Construct_UClass_USoundVisualizationStatics();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_SoundVisualizations();
	ENGINE_API UClass* Z_Construct_UClass_USoundWave_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(USoundVisualizationStatics::execGetAmplitude)
	{
		P_GET_OBJECT(USoundWave,Z_Param_SoundWave);
		P_GET_PROPERTY(FIntProperty,Z_Param_Channel);
		P_GET_PROPERTY(FFloatProperty,Z_Param_StartTime);
		P_GET_PROPERTY(FFloatProperty,Z_Param_TimeLength);
		P_GET_PROPERTY(FIntProperty,Z_Param_AmplitudeBuckets);
		P_GET_TARRAY_REF(float,Z_Param_Out_OutAmplitudes);
		P_FINISH;
		P_NATIVE_BEGIN;
		USoundVisualizationStatics::GetAmplitude(Z_Param_SoundWave,Z_Param_Channel,Z_Param_StartTime,Z_Param_TimeLength,Z_Param_AmplitudeBuckets,Z_Param_Out_OutAmplitudes);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USoundVisualizationStatics::execCalculateFrequencySpectrum)
	{
		P_GET_OBJECT(USoundWave,Z_Param_SoundWave);
		P_GET_PROPERTY(FIntProperty,Z_Param_Channel);
		P_GET_PROPERTY(FFloatProperty,Z_Param_StartTime);
		P_GET_PROPERTY(FFloatProperty,Z_Param_TimeLength);
		P_GET_PROPERTY(FIntProperty,Z_Param_SpectrumWidth);
		P_GET_TARRAY_REF(float,Z_Param_Out_OutSpectrum);
		P_FINISH;
		P_NATIVE_BEGIN;
		USoundVisualizationStatics::CalculateFrequencySpectrum(Z_Param_SoundWave,Z_Param_Channel,Z_Param_StartTime,Z_Param_TimeLength,Z_Param_SpectrumWidth,Z_Param_Out_OutSpectrum);
		P_NATIVE_END;
	}
	void USoundVisualizationStatics::StaticRegisterNativesUSoundVisualizationStatics()
	{
		UClass* Class = USoundVisualizationStatics::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CalculateFrequencySpectrum", &USoundVisualizationStatics::execCalculateFrequencySpectrum },
			{ "GetAmplitude", &USoundVisualizationStatics::execGetAmplitude },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics
	{
		struct SoundVisualizationStatics_eventCalculateFrequencySpectrum_Parms
		{
			USoundWave* SoundWave;
			int32 Channel;
			float StartTime;
			float TimeLength;
			int32 SpectrumWidth;
			TArray<float> OutSpectrum;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SoundWave;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Channel;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StartTime;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeLength;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SpectrumWidth;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutSpectrum_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutSpectrum;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_SoundWave = { "SoundWave", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventCalculateFrequencySpectrum_Parms, SoundWave), Z_Construct_UClass_USoundWave_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventCalculateFrequencySpectrum_Parms, Channel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_StartTime = { "StartTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventCalculateFrequencySpectrum_Parms, StartTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_TimeLength = { "TimeLength", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventCalculateFrequencySpectrum_Parms, TimeLength), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_SpectrumWidth = { "SpectrumWidth", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventCalculateFrequencySpectrum_Parms, SpectrumWidth), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_OutSpectrum_Inner = { "OutSpectrum", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_OutSpectrum = { "OutSpectrum", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventCalculateFrequencySpectrum_Parms, OutSpectrum), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_SoundWave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_Channel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_StartTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_TimeLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_SpectrumWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_OutSpectrum_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::NewProp_OutSpectrum,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::Function_MetaDataParams[] = {
		{ "Category", "SoundVisualization" },
		{ "Comment", "/** Calculates the frequency spectrum for a window of time for the SoundWave\n\x09 * @param SoundWave - The wave to generate the spectrum for\n\x09 * @param Channel - The channel of the sound to calculate.  Specify 0 to combine channels together\n\x09 * @param StartTime - The beginning of the window to calculate the spectrum of\n\x09 * @param TimeLength - The duration of the window to calculate the spectrum of\n\x09 * @param SpectrumWidth - How wide the spectrum is.  The total samples in the window are divided evenly across the spectrum width.\n\x09 * @return OutSpectrum - The resulting spectrum\n\x09 */" },
		{ "ModuleRelativePath", "Classes/SoundVisualizationStatics.h" },
		{ "ToolTip", "Calculates the frequency spectrum for a window of time for the SoundWave\n@param SoundWave - The wave to generate the spectrum for\n@param Channel - The channel of the sound to calculate.  Specify 0 to combine channels together\n@param StartTime - The beginning of the window to calculate the spectrum of\n@param TimeLength - The duration of the window to calculate the spectrum of\n@param SpectrumWidth - How wide the spectrum is.  The total samples in the window are divided evenly across the spectrum width.\n@return OutSpectrum - The resulting spectrum" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USoundVisualizationStatics, nullptr, "CalculateFrequencySpectrum", nullptr, nullptr, sizeof(SoundVisualizationStatics_eventCalculateFrequencySpectrum_Parms), Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics
	{
		struct SoundVisualizationStatics_eventGetAmplitude_Parms
		{
			USoundWave* SoundWave;
			int32 Channel;
			float StartTime;
			float TimeLength;
			int32 AmplitudeBuckets;
			TArray<float> OutAmplitudes;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SoundWave;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Channel;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StartTime;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeLength;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AmplitudeBuckets;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutAmplitudes_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutAmplitudes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_SoundWave = { "SoundWave", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventGetAmplitude_Parms, SoundWave), Z_Construct_UClass_USoundWave_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventGetAmplitude_Parms, Channel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_StartTime = { "StartTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventGetAmplitude_Parms, StartTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_TimeLength = { "TimeLength", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventGetAmplitude_Parms, TimeLength), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_AmplitudeBuckets = { "AmplitudeBuckets", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventGetAmplitude_Parms, AmplitudeBuckets), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_OutAmplitudes_Inner = { "OutAmplitudes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_OutAmplitudes = { "OutAmplitudes", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SoundVisualizationStatics_eventGetAmplitude_Parms, OutAmplitudes), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_SoundWave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_Channel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_StartTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_TimeLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_AmplitudeBuckets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_OutAmplitudes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::NewProp_OutAmplitudes,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::Function_MetaDataParams[] = {
		{ "Category", "SoundVisualization" },
		{ "Comment", "/** Gathers the amplitude of the wave data for a window of time for the SoundWave\n\x09 * @param SoundWave - The wave to get samples from\n\x09 * @param Channel - The channel of the sound to get.  Specify 0 to combine channels together\n\x09 * @param StartTime - The beginning of the window to get the amplitude from\n\x09 * @param TimeLength - The duration of the window to get the amplitude from\n\x09 * @param AmplitudeBuckets - How many samples to divide the data in to.  The amplitude is averaged from the wave samples for each bucket\n\x09 * @return OutAmplitudes - The resulting amplitudes\n\x09 */" },
		{ "ModuleRelativePath", "Classes/SoundVisualizationStatics.h" },
		{ "ToolTip", "Gathers the amplitude of the wave data for a window of time for the SoundWave\n@param SoundWave - The wave to get samples from\n@param Channel - The channel of the sound to get.  Specify 0 to combine channels together\n@param StartTime - The beginning of the window to get the amplitude from\n@param TimeLength - The duration of the window to get the amplitude from\n@param AmplitudeBuckets - How many samples to divide the data in to.  The amplitude is averaged from the wave samples for each bucket\n@return OutAmplitudes - The resulting amplitudes" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USoundVisualizationStatics, nullptr, "GetAmplitude", nullptr, nullptr, sizeof(SoundVisualizationStatics_eventGetAmplitude_Parms), Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_USoundVisualizationStatics_NoRegister()
	{
		return USoundVisualizationStatics::StaticClass();
	}
	struct Z_Construct_UClass_USoundVisualizationStatics_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundVisualizationStatics_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundVisualizations,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USoundVisualizationStatics_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USoundVisualizationStatics_CalculateFrequencySpectrum, "CalculateFrequencySpectrum" }, // 3287660791
		{ &Z_Construct_UFunction_USoundVisualizationStatics_GetAmplitude, "GetAmplitude" }, // 702592625
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundVisualizationStatics_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SoundVisualizationStatics.h" },
		{ "ModuleRelativePath", "Classes/SoundVisualizationStatics.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundVisualizationStatics_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundVisualizationStatics>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundVisualizationStatics_Statics::ClassParams = {
		&USoundVisualizationStatics::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundVisualizationStatics_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundVisualizationStatics_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundVisualizationStatics()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundVisualizationStatics_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundVisualizationStatics, 1857033739);
	template<> SOUNDVISUALIZATIONS_API UClass* StaticClass<USoundVisualizationStatics>()
	{
		return USoundVisualizationStatics::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundVisualizationStatics(Z_Construct_UClass_USoundVisualizationStatics, &USoundVisualizationStatics::StaticClass, TEXT("/Script/SoundVisualizations"), TEXT("USoundVisualizationStatics"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundVisualizationStatics);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
