// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class USoundWave;
#ifdef SOUNDVISUALIZATIONS_SoundVisualizationStatics_generated_h
#error "SoundVisualizationStatics.generated.h already included, missing '#pragma once' in SoundVisualizationStatics.h"
#endif
#define SOUNDVISUALIZATIONS_SoundVisualizationStatics_generated_h

#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_SPARSE_DATA
#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAmplitude); \
	DECLARE_FUNCTION(execCalculateFrequencySpectrum);


#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAmplitude); \
	DECLARE_FUNCTION(execCalculateFrequencySpectrum);


#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundVisualizationStatics(); \
	friend struct Z_Construct_UClass_USoundVisualizationStatics_Statics; \
public: \
	DECLARE_CLASS(USoundVisualizationStatics, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundVisualizations"), NO_API) \
	DECLARE_SERIALIZER(USoundVisualizationStatics)


#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSoundVisualizationStatics(); \
	friend struct Z_Construct_UClass_USoundVisualizationStatics_Statics; \
public: \
	DECLARE_CLASS(USoundVisualizationStatics, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundVisualizations"), NO_API) \
	DECLARE_SERIALIZER(USoundVisualizationStatics)


#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundVisualizationStatics(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundVisualizationStatics) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundVisualizationStatics); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundVisualizationStatics); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundVisualizationStatics(USoundVisualizationStatics&&); \
	NO_API USoundVisualizationStatics(const USoundVisualizationStatics&); \
public:


#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundVisualizationStatics(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundVisualizationStatics(USoundVisualizationStatics&&); \
	NO_API USoundVisualizationStatics(const USoundVisualizationStatics&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundVisualizationStatics); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundVisualizationStatics); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundVisualizationStatics)


#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_12_PROLOG
#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_INCLASS \
	Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundVisualizationStatics."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOUNDVISUALIZATIONS_API UClass* StaticClass<class USoundVisualizationStatics>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SoundVisualizations_Source_SoundVisualizations_Classes_SoundVisualizationStatics_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
