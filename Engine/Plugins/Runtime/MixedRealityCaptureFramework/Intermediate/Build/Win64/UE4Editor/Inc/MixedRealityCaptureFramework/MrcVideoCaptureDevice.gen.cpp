// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MixedRealityCaptureFramework/Public/MrcVideoCaptureDevice.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMrcVideoCaptureDevice() {}
// Cross Module References
	MIXEDREALITYCAPTUREFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
	MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintAsyncActionBase();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaPlayer_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics
	{
		struct _Script_MixedRealityCaptureFramework_eventMRCaptureFeedDelegate_Parms
		{
			FMrcVideoCaptureFeedIndex FeedRef;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeedRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FeedRef;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::NewProp_FeedRef_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::NewProp_FeedRef = { "FeedRef", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MixedRealityCaptureFramework_eventMRCaptureFeedDelegate_Parms, FeedRef), Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex, METADATA_PARAMS(Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::NewProp_FeedRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::NewProp_FeedRef_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::NewProp_FeedRef,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework, nullptr, "MRCaptureFeedDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MixedRealityCaptureFramework_eventMRCaptureFeedDelegate_Parms), Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
class UScriptStruct* FMrcVideoCaptureFeedIndex::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MIXEDREALITYCAPTUREFRAMEWORK_API uint32 Get_Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex, Z_Construct_UPackage__Script_MixedRealityCaptureFramework(), TEXT("MrcVideoCaptureFeedIndex"), sizeof(FMrcVideoCaptureFeedIndex), Get_Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Hash());
	}
	return Singleton;
}
template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<FMrcVideoCaptureFeedIndex>()
{
	return FMrcVideoCaptureFeedIndex::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMrcVideoCaptureFeedIndex(FMrcVideoCaptureFeedIndex::StaticStruct, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("MrcVideoCaptureFeedIndex"), false, nullptr, nullptr);
static struct FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcVideoCaptureFeedIndex
{
	FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcVideoCaptureFeedIndex()
	{
		UScriptStruct::DeferCppStructOps<FMrcVideoCaptureFeedIndex>(FName(TEXT("MrcVideoCaptureFeedIndex")));
	}
} ScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcVideoCaptureFeedIndex;
	struct Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StreamIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StreamIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FormatIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FormatIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/* FMrcVideoCaptureFeedIndex\n *****************************************************************************/" },
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
		{ "ToolTip", "FMrcVideoCaptureFeedIndex" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMrcVideoCaptureFeedIndex>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_DeviceURL_MetaData[] = {
		{ "Category", "MixedRealityCapture|CaptureDevice" },
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_DeviceURL = { "DeviceURL", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcVideoCaptureFeedIndex, DeviceURL), METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_DeviceURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_DeviceURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_StreamIndex_MetaData[] = {
		{ "Category", "MixedRealityCapture|CaptureDevice" },
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_StreamIndex = { "StreamIndex", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcVideoCaptureFeedIndex, StreamIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_StreamIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_StreamIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_FormatIndex_MetaData[] = {
		{ "Category", "MixedRealityCapture|CaptureDevice" },
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_FormatIndex = { "FormatIndex", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcVideoCaptureFeedIndex, FormatIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_FormatIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_FormatIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_DeviceURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_StreamIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::NewProp_FormatIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
		nullptr,
		&NewStructOps,
		"MrcVideoCaptureFeedIndex",
		sizeof(FMrcVideoCaptureFeedIndex),
		alignof(FMrcVideoCaptureFeedIndex),
		Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MrcVideoCaptureFeedIndex"), sizeof(FMrcVideoCaptureFeedIndex), Get_Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex_Hash() { return 175080718U; }
	DEFINE_FUNCTION(UAsyncTask_OpenMrcVidCaptureFeedBase::execOnVideoFeedOpenFailure)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceUrl);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnVideoFeedOpenFailure(Z_Param_DeviceUrl);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAsyncTask_OpenMrcVidCaptureFeedBase::execOnVideoFeedOpened)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceUrl);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnVideoFeedOpened(Z_Param_DeviceUrl);
		P_NATIVE_END;
	}
	void UAsyncTask_OpenMrcVidCaptureFeedBase::StaticRegisterNativesUAsyncTask_OpenMrcVidCaptureFeedBase()
	{
		UClass* Class = UAsyncTask_OpenMrcVidCaptureFeedBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnVideoFeedOpened", &UAsyncTask_OpenMrcVidCaptureFeedBase::execOnVideoFeedOpened },
			{ "OnVideoFeedOpenFailure", &UAsyncTask_OpenMrcVidCaptureFeedBase::execOnVideoFeedOpenFailure },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics
	{
		struct AsyncTask_OpenMrcVidCaptureFeedBase_eventOnVideoFeedOpened_Parms
		{
			FString DeviceUrl;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceUrl;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics::NewProp_DeviceUrl = { "DeviceUrl", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AsyncTask_OpenMrcVidCaptureFeedBase_eventOnVideoFeedOpened_Parms, DeviceUrl), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics::NewProp_DeviceUrl,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase, nullptr, "OnVideoFeedOpened", nullptr, nullptr, sizeof(AsyncTask_OpenMrcVidCaptureFeedBase_eventOnVideoFeedOpened_Parms), Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics
	{
		struct AsyncTask_OpenMrcVidCaptureFeedBase_eventOnVideoFeedOpenFailure_Parms
		{
			FString DeviceUrl;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceUrl;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics::NewProp_DeviceUrl = { "DeviceUrl", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AsyncTask_OpenMrcVidCaptureFeedBase_eventOnVideoFeedOpenFailure_Parms, DeviceUrl), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics::NewProp_DeviceUrl,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase, nullptr, "OnVideoFeedOpenFailure", nullptr, nullptr, sizeof(AsyncTask_OpenMrcVidCaptureFeedBase_eventOnVideoFeedOpenFailure_Parms), Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_NoRegister()
	{
		return UAsyncTask_OpenMrcVidCaptureFeedBase::StaticClass();
	}
	struct Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSuccess_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSuccess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnFail_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnFail;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaPlayer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaPlayer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintAsyncActionBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpened, "OnVideoFeedOpened" }, // 1925610817
		{ &Z_Construct_UFunction_UAsyncTask_OpenMrcVidCaptureFeedBase_OnVideoFeedOpenFailure, "OnVideoFeedOpenFailure" }, // 3985034822
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MrcVideoCaptureDevice.h" },
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_OnSuccess_MetaData[] = {
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_OnSuccess = { "OnSuccess", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAsyncTask_OpenMrcVidCaptureFeedBase, OnSuccess), Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_OnSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_OnSuccess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_OnFail_MetaData[] = {
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_OnFail = { "OnFail", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAsyncTask_OpenMrcVidCaptureFeedBase, OnFail), Z_Construct_UDelegateFunction_MixedRealityCaptureFramework_MRCaptureFeedDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_OnFail_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_OnFail_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_MediaPlayer_MetaData[] = {
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_MediaPlayer = { "MediaPlayer", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAsyncTask_OpenMrcVidCaptureFeedBase, MediaPlayer), Z_Construct_UClass_UMediaPlayer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_MediaPlayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_MediaPlayer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_OnSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_OnFail,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::NewProp_MediaPlayer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAsyncTask_OpenMrcVidCaptureFeedBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::ClassParams = {
		&UAsyncTask_OpenMrcVidCaptureFeedBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::PropPointers),
		0,
		0x009000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAsyncTask_OpenMrcVidCaptureFeedBase, 921453604);
	template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<UAsyncTask_OpenMrcVidCaptureFeedBase>()
	{
		return UAsyncTask_OpenMrcVidCaptureFeedBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase, &UAsyncTask_OpenMrcVidCaptureFeedBase::StaticClass, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("UAsyncTask_OpenMrcVidCaptureFeedBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAsyncTask_OpenMrcVidCaptureFeedBase);
	void UAsyncTask_OpenMrcVidCaptureDevice::StaticRegisterNativesUAsyncTask_OpenMrcVidCaptureDevice()
	{
	}
	UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice_NoRegister()
	{
		return UAsyncTask_OpenMrcVidCaptureDevice::StaticClass();
	}
	struct Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* UAsyncTask_OpenMrcVidCaptureDevice\n *****************************************************************************/" },
		{ "IncludePath", "MrcVideoCaptureDevice.h" },
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
		{ "ToolTip", "UAsyncTask_OpenMrcVidCaptureDevice" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAsyncTask_OpenMrcVidCaptureDevice>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice_Statics::ClassParams = {
		&UAsyncTask_OpenMrcVidCaptureDevice::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAsyncTask_OpenMrcVidCaptureDevice, 3763338343);
	template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<UAsyncTask_OpenMrcVidCaptureDevice>()
	{
		return UAsyncTask_OpenMrcVidCaptureDevice::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAsyncTask_OpenMrcVidCaptureDevice(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureDevice, &UAsyncTask_OpenMrcVidCaptureDevice::StaticClass, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("UAsyncTask_OpenMrcVidCaptureDevice"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAsyncTask_OpenMrcVidCaptureDevice);
	void UAsyncTask_OpenMrcVidCaptureFeed::StaticRegisterNativesUAsyncTask_OpenMrcVidCaptureFeed()
	{
	}
	UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed_NoRegister()
	{
		return UAsyncTask_OpenMrcVidCaptureFeed::StaticClass();
	}
	struct Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeedBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* UAsyncTask_OpenMrcVidCaptureFeed\n *****************************************************************************/" },
		{ "IncludePath", "MrcVideoCaptureDevice.h" },
		{ "ModuleRelativePath", "Public/MrcVideoCaptureDevice.h" },
		{ "ToolTip", "UAsyncTask_OpenMrcVidCaptureFeed" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAsyncTask_OpenMrcVidCaptureFeed>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed_Statics::ClassParams = {
		&UAsyncTask_OpenMrcVidCaptureFeed::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAsyncTask_OpenMrcVidCaptureFeed, 3844774036);
	template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<UAsyncTask_OpenMrcVidCaptureFeed>()
	{
		return UAsyncTask_OpenMrcVidCaptureFeed::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAsyncTask_OpenMrcVidCaptureFeed(Z_Construct_UClass_UAsyncTask_OpenMrcVidCaptureFeed, &UAsyncTask_OpenMrcVidCaptureFeed::StaticClass, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("UAsyncTask_OpenMrcVidCaptureFeed"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAsyncTask_OpenMrcVidCaptureFeed);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
