// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MixedRealityCaptureFramework/Private/MrcFrameworkSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMrcFrameworkSettings() {}
// Cross Module References
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMrcFrameworkSettings_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMrcFrameworkSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
// End Cross Module References
	void UMrcFrameworkSettings::StaticRegisterNativesUMrcFrameworkSettings()
	{
	}
	UClass* Z_Construct_UClass_UMrcFrameworkSettings_NoRegister()
	{
		return UMrcFrameworkSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMrcFrameworkSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaulVideoSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaulVideoSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultVideoProcessingMat_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultVideoProcessingMat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultRenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultRenderTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultDistortionDisplacementMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultDistortionDisplacementMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaulGarbageMatteMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaulGarbageMatteMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaulGarbageMatteMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaulGarbageMatteMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaulGarbageMatteTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaulGarbageMatteTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMrcFrameworkSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcFrameworkSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MrcFrameworkSettings.h" },
		{ "ModuleRelativePath", "Private/MrcFrameworkSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulVideoSource_MetaData[] = {
		{ "ModuleRelativePath", "Private/MrcFrameworkSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulVideoSource = { "DefaulVideoSource", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcFrameworkSettings, DefaulVideoSource), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulVideoSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulVideoSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultVideoProcessingMat_MetaData[] = {
		{ "ModuleRelativePath", "Private/MrcFrameworkSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultVideoProcessingMat = { "DefaultVideoProcessingMat", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcFrameworkSettings, DefaultVideoProcessingMat), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultVideoProcessingMat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultVideoProcessingMat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultRenderTarget_MetaData[] = {
		{ "ModuleRelativePath", "Private/MrcFrameworkSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultRenderTarget = { "DefaultRenderTarget", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcFrameworkSettings, DefaultRenderTarget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultRenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultRenderTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultDistortionDisplacementMap_MetaData[] = {
		{ "ModuleRelativePath", "Private/MrcFrameworkSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultDistortionDisplacementMap = { "DefaultDistortionDisplacementMap", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcFrameworkSettings, DefaultDistortionDisplacementMap), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultDistortionDisplacementMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultDistortionDisplacementMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteMesh_MetaData[] = {
		{ "ModuleRelativePath", "Private/MrcFrameworkSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteMesh = { "DefaulGarbageMatteMesh", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcFrameworkSettings, DefaulGarbageMatteMesh), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Private/MrcFrameworkSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteMaterial = { "DefaulGarbageMatteMaterial", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcFrameworkSettings, DefaulGarbageMatteMaterial), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteTarget_MetaData[] = {
		{ "ModuleRelativePath", "Private/MrcFrameworkSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteTarget = { "DefaulGarbageMatteTarget", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcFrameworkSettings, DefaulGarbageMatteTarget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMrcFrameworkSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulVideoSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultVideoProcessingMat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultRenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaultDistortionDisplacementMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcFrameworkSettings_Statics::NewProp_DefaulGarbageMatteTarget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMrcFrameworkSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMrcFrameworkSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMrcFrameworkSettings_Statics::ClassParams = {
		&UMrcFrameworkSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMrcFrameworkSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMrcFrameworkSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMrcFrameworkSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcFrameworkSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMrcFrameworkSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMrcFrameworkSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMrcFrameworkSettings, 50179889);
	template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<UMrcFrameworkSettings>()
	{
		return UMrcFrameworkSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMrcFrameworkSettings(Z_Construct_UClass_UMrcFrameworkSettings, &UMrcFrameworkSettings::StaticClass, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("UMrcFrameworkSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMrcFrameworkSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
