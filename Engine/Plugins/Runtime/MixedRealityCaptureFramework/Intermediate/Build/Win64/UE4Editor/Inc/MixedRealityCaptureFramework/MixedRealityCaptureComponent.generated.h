// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FMrcVideoCaptureFeedIndex;
class AActor;
struct FOpenCVLensDistortionParameters;
struct FMrcVideoProcessingParams;
class UMaterialInterface;
class AMrcGarbageMatteActor;
class UMrcCalibrationData;
#ifdef MIXEDREALITYCAPTUREFRAMEWORK_MixedRealityCaptureComponent_generated_h
#error "MixedRealityCaptureComponent.generated.h already included, missing '#pragma once' in MixedRealityCaptureComponent.h"
#endif
#define MIXEDREALITYCAPTUREFRAMEWORK_MixedRealityCaptureComponent_generated_h

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_173_DELEGATE \
struct MixedRealityCaptureComponent_eventMRCaptureFeedOpenedDelegate_Parms \
{ \
	FMrcVideoCaptureFeedIndex FeedRef; \
}; \
static inline void FMRCaptureFeedOpenedDelegate_DelegateWrapper(const FMulticastScriptDelegate& MRCaptureFeedOpenedDelegate, FMrcVideoCaptureFeedIndex const& FeedRef) \
{ \
	MixedRealityCaptureComponent_eventMRCaptureFeedOpenedDelegate_Parms Parms; \
	Parms.FeedRef=FeedRef; \
	MRCaptureFeedOpenedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_SPARSE_DATA
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_RPC_WRAPPERS \
	virtual void ApplyCalibrationData_Implementation(UMrcCalibrationData* ConfigData); \
	virtual UMrcCalibrationData* ConstructCalibrationData_Implementation() const; \
 \
	DECLARE_FUNCTION(execOnVideoFeedOpened); \
	DECLARE_FUNCTION(execSetEnableProjectionDepthTracking); \
	DECLARE_FUNCTION(execGetProjectionActor_K2); \
	DECLARE_FUNCTION(execSetProjectionDepthOffset); \
	DECLARE_FUNCTION(execSetTrackingDelay); \
	DECLARE_FUNCTION(execGetTrackingDelay); \
	DECLARE_FUNCTION(execSetLensDistortionParameters); \
	DECLARE_FUNCTION(execSetCaptureDevice); \
	DECLARE_FUNCTION(execIsTracked); \
	DECLARE_FUNCTION(execDetatchFromDevice); \
	DECLARE_FUNCTION(execSetDeviceAttachment); \
	DECLARE_FUNCTION(execSetVidProcessingParams); \
	DECLARE_FUNCTION(execSetVidProjectionMat); \
	DECLARE_FUNCTION(execSetGarbageMatteActor); \
	DECLARE_FUNCTION(execApplyCalibrationData); \
	DECLARE_FUNCTION(execFillOutCalibrationData); \
	DECLARE_FUNCTION(execConstructCalibrationData); \
	DECLARE_FUNCTION(execLoadConfiguration); \
	DECLARE_FUNCTION(execLoadDefaultConfiguration); \
	DECLARE_FUNCTION(execSaveConfiguration_K2); \
	DECLARE_FUNCTION(execSaveAsDefaultConfiguration_K2);


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void ApplyCalibrationData_Implementation(UMrcCalibrationData* ConfigData); \
	virtual UMrcCalibrationData* ConstructCalibrationData_Implementation() const; \
 \
	DECLARE_FUNCTION(execOnVideoFeedOpened); \
	DECLARE_FUNCTION(execSetEnableProjectionDepthTracking); \
	DECLARE_FUNCTION(execGetProjectionActor_K2); \
	DECLARE_FUNCTION(execSetProjectionDepthOffset); \
	DECLARE_FUNCTION(execSetTrackingDelay); \
	DECLARE_FUNCTION(execGetTrackingDelay); \
	DECLARE_FUNCTION(execSetLensDistortionParameters); \
	DECLARE_FUNCTION(execSetCaptureDevice); \
	DECLARE_FUNCTION(execIsTracked); \
	DECLARE_FUNCTION(execDetatchFromDevice); \
	DECLARE_FUNCTION(execSetDeviceAttachment); \
	DECLARE_FUNCTION(execSetVidProcessingParams); \
	DECLARE_FUNCTION(execSetVidProjectionMat); \
	DECLARE_FUNCTION(execSetGarbageMatteActor); \
	DECLARE_FUNCTION(execApplyCalibrationData); \
	DECLARE_FUNCTION(execFillOutCalibrationData); \
	DECLARE_FUNCTION(execConstructCalibrationData); \
	DECLARE_FUNCTION(execLoadConfiguration); \
	DECLARE_FUNCTION(execLoadDefaultConfiguration); \
	DECLARE_FUNCTION(execSaveConfiguration_K2); \
	DECLARE_FUNCTION(execSaveAsDefaultConfiguration_K2);


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_EVENT_PARMS \
	struct MixedRealityCaptureComponent_eventApplyCalibrationData_Parms \
	{ \
		UMrcCalibrationData* ConfigData; \
	}; \
	struct MixedRealityCaptureComponent_eventConstructCalibrationData_Parms \
	{ \
		UMrcCalibrationData* ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		MixedRealityCaptureComponent_eventConstructCalibrationData_Parms() \
			: ReturnValue(NULL) \
		{ \
		} \
	};


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_CALLBACK_WRAPPERS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMixedRealityCaptureComponent(); \
	friend struct Z_Construct_UClass_UMixedRealityCaptureComponent_Statics; \
public: \
	DECLARE_CLASS(UMixedRealityCaptureComponent, USceneCaptureComponent2D, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMixedRealityCaptureComponent)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUMixedRealityCaptureComponent(); \
	friend struct Z_Construct_UClass_UMixedRealityCaptureComponent_Statics; \
public: \
	DECLARE_CLASS(UMixedRealityCaptureComponent, USceneCaptureComponent2D, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMixedRealityCaptureComponent)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMixedRealityCaptureComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMixedRealityCaptureComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMixedRealityCaptureComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMixedRealityCaptureComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMixedRealityCaptureComponent(UMixedRealityCaptureComponent&&); \
	NO_API UMixedRealityCaptureComponent(const UMixedRealityCaptureComponent&); \
public:


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMixedRealityCaptureComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMixedRealityCaptureComponent(UMixedRealityCaptureComponent&&); \
	NO_API UMixedRealityCaptureComponent(const UMixedRealityCaptureComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMixedRealityCaptureComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMixedRealityCaptureComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMixedRealityCaptureComponent)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ProjectionActor() { return STRUCT_OFFSET(UMixedRealityCaptureComponent, ProjectionActor); } \
	FORCEINLINE static uint32 __PPO__PairedTracker() { return STRUCT_OFFSET(UMixedRealityCaptureComponent, PairedTracker); } \
	FORCEINLINE static uint32 __PPO__TrackingOriginOffset() { return STRUCT_OFFSET(UMixedRealityCaptureComponent, TrackingOriginOffset); } \
	FORCEINLINE static uint32 __PPO__GarbageMatteCaptureComponent() { return STRUCT_OFFSET(UMixedRealityCaptureComponent, GarbageMatteCaptureComponent); } \
	FORCEINLINE static uint32 __PPO__DistortionDisplacementMap() { return STRUCT_OFFSET(UMixedRealityCaptureComponent, DistortionDisplacementMap); }


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_34_PROLOG \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_EVENT_PARMS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_INCLASS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h_37_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MixedRealityCaptureComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<class UMixedRealityCaptureComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MixedRealityCaptureComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
