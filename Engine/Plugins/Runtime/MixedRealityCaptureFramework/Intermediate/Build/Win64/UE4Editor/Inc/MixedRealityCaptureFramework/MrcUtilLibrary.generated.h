// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UTexture;
#ifdef MIXEDREALITYCAPTUREFRAMEWORK_MrcUtilLibrary_generated_h
#error "MrcUtilLibrary.generated.h already included, missing '#pragma once' in MrcUtilLibrary.h"
#endif
#define MIXEDREALITYCAPTUREFRAMEWORK_MrcUtilLibrary_generated_h

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_SPARSE_DATA
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetMixedRealityCaptureTexture); \
	DECLARE_FUNCTION(execSetMixedRealityCaptureBroadcasting); \
	DECLARE_FUNCTION(execIsMixedRealityCaptureBroadcasting);


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetMixedRealityCaptureTexture); \
	DECLARE_FUNCTION(execSetMixedRealityCaptureBroadcasting); \
	DECLARE_FUNCTION(execIsMixedRealityCaptureBroadcasting);


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMrcUtilLibrary(); \
	friend struct Z_Construct_UClass_UMrcUtilLibrary_Statics; \
public: \
	DECLARE_CLASS(UMrcUtilLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcUtilLibrary)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUMrcUtilLibrary(); \
	friend struct Z_Construct_UClass_UMrcUtilLibrary_Statics; \
public: \
	DECLARE_CLASS(UMrcUtilLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcUtilLibrary)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcUtilLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcUtilLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcUtilLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcUtilLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcUtilLibrary(UMrcUtilLibrary&&); \
	NO_API UMrcUtilLibrary(const UMrcUtilLibrary&); \
public:


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcUtilLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcUtilLibrary(UMrcUtilLibrary&&); \
	NO_API UMrcUtilLibrary(const UMrcUtilLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcUtilLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcUtilLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcUtilLibrary)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_15_PROLOG
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_INCLASS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MrcUtilLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<class UMrcUtilLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcUtilLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
