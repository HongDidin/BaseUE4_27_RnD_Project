// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MixedRealityCaptureFramework/Public/MrcCalibrationData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMrcCalibrationData() {}
// Cross Module References
	MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* Z_Construct_UScriptStruct_FMrcCompositingSaveData();
	UPackage* Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
	MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex();
	MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* Z_Construct_UScriptStruct_FMrcVideoProcessingParams();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* Z_Construct_UScriptStruct_FMrcAlignmentSaveData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	HEADMOUNTEDDISPLAY_API UEnum* Z_Construct_UEnum_HeadMountedDisplay_EHMDTrackingOrigin();
	MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* Z_Construct_UScriptStruct_FMrcLensCalibrationData();
	OPENCVLENSDISTORTION_API UScriptStruct* Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMrcCalibrationData_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMrcCalibrationData();
	ENGINE_API UClass* Z_Construct_UClass_USaveGame();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMrcCalibrationSaveGame_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMrcCalibrationSaveGame();
// End Cross Module References
class UScriptStruct* FMrcCompositingSaveData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MIXEDREALITYCAPTUREFRAMEWORK_API uint32 Get_Z_Construct_UScriptStruct_FMrcCompositingSaveData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMrcCompositingSaveData, Z_Construct_UPackage__Script_MixedRealityCaptureFramework(), TEXT("MrcCompositingSaveData"), sizeof(FMrcCompositingSaveData), Get_Z_Construct_UScriptStruct_FMrcCompositingSaveData_Hash());
	}
	return Singleton;
}
template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<FMrcCompositingSaveData>()
{
	return FMrcCompositingSaveData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMrcCompositingSaveData(FMrcCompositingSaveData::StaticStruct, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("MrcCompositingSaveData"), false, nullptr, nullptr);
static struct FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcCompositingSaveData
{
	FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcCompositingSaveData()
	{
		UScriptStruct::DeferCppStructOps<FMrcCompositingSaveData>(FName(TEXT("MrcCompositingSaveData")));
	}
} ScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcCompositingSaveData;
	struct Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureDeviceURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureDeviceURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DepthOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DepthOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingLatency_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TrackingLatency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VideoProcessingParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VideoProcessingParams;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMrcCompositingSaveData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_CaptureDeviceURL_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_CaptureDeviceURL = { "CaptureDeviceURL", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcCompositingSaveData, CaptureDeviceURL), Z_Construct_UScriptStruct_FMrcVideoCaptureFeedIndex, METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_CaptureDeviceURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_CaptureDeviceURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_DepthOffset_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_DepthOffset = { "DepthOffset", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcCompositingSaveData, DepthOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_DepthOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_DepthOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_TrackingLatency_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_TrackingLatency = { "TrackingLatency", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcCompositingSaveData, TrackingLatency), METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_TrackingLatency_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_TrackingLatency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_VideoProcessingParams_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_VideoProcessingParams = { "VideoProcessingParams", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcCompositingSaveData, VideoProcessingParams), Z_Construct_UScriptStruct_FMrcVideoProcessingParams, METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_VideoProcessingParams_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_VideoProcessingParams_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_CaptureDeviceURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_DepthOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_TrackingLatency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::NewProp_VideoProcessingParams,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
		nullptr,
		&NewStructOps,
		"MrcCompositingSaveData",
		sizeof(FMrcCompositingSaveData),
		alignof(FMrcCompositingSaveData),
		Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMrcCompositingSaveData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMrcCompositingSaveData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MrcCompositingSaveData"), sizeof(FMrcCompositingSaveData), Get_Z_Construct_UScriptStruct_FMrcCompositingSaveData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMrcCompositingSaveData_Hash() { return 2555731848U; }
class UScriptStruct* FMrcVideoProcessingParams::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MIXEDREALITYCAPTUREFRAMEWORK_API uint32 Get_Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMrcVideoProcessingParams, Z_Construct_UPackage__Script_MixedRealityCaptureFramework(), TEXT("MrcVideoProcessingParams"), sizeof(FMrcVideoProcessingParams), Get_Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Hash());
	}
	return Singleton;
}
template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<FMrcVideoProcessingParams>()
{
	return FMrcVideoProcessingParams::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMrcVideoProcessingParams(FMrcVideoProcessingParams::StaticStruct, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("MrcVideoProcessingParams"), false, nullptr, nullptr);
static struct FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcVideoProcessingParams
{
	FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcVideoProcessingParams()
	{
		UScriptStruct::DeferCppStructOps<FMrcVideoProcessingParams>(FName(TEXT("MrcVideoProcessingParams")));
	}
} ScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcVideoProcessingParams;
	struct Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaterialScalarParams_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MaterialScalarParams_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialScalarParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_MaterialScalarParams;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MaterialVectorParams_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MaterialVectorParams_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialVectorParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_MaterialVectorParams;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMrcVideoProcessingParams>();
	}
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialScalarParams_ValueProp = { "MaterialScalarParams", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialScalarParams_Key_KeyProp = { "MaterialScalarParams_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialScalarParams_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialScalarParams = { "MaterialScalarParams", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcVideoProcessingParams, MaterialScalarParams), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialScalarParams_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialScalarParams_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialVectorParams_ValueProp = { "MaterialVectorParams", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialVectorParams_Key_KeyProp = { "MaterialVectorParams_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialVectorParams_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialVectorParams = { "MaterialVectorParams", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcVideoProcessingParams, MaterialVectorParams), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialVectorParams_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialVectorParams_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialScalarParams_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialScalarParams_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialScalarParams,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialVectorParams_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialVectorParams_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::NewProp_MaterialVectorParams,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
		nullptr,
		&NewStructOps,
		"MrcVideoProcessingParams",
		sizeof(FMrcVideoProcessingParams),
		alignof(FMrcVideoProcessingParams),
		Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMrcVideoProcessingParams()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MrcVideoProcessingParams"), sizeof(FMrcVideoProcessingParams), Get_Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Hash() { return 1606789497U; }
class UScriptStruct* FMrcGarbageMatteSaveData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MIXEDREALITYCAPTUREFRAMEWORK_API uint32 Get_Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData, Z_Construct_UPackage__Script_MixedRealityCaptureFramework(), TEXT("MrcGarbageMatteSaveData"), sizeof(FMrcGarbageMatteSaveData), Get_Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Hash());
	}
	return Singleton;
}
template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<FMrcGarbageMatteSaveData>()
{
	return FMrcGarbageMatteSaveData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMrcGarbageMatteSaveData(FMrcGarbageMatteSaveData::StaticStruct, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("MrcGarbageMatteSaveData"), false, nullptr, nullptr);
static struct FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcGarbageMatteSaveData
{
	FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcGarbageMatteSaveData()
	{
		UScriptStruct::DeferCppStructOps<FMrcGarbageMatteSaveData>(FName(TEXT("MrcGarbageMatteSaveData")));
	}
} ScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcGarbageMatteSaveData;
	struct Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMrcGarbageMatteSaveData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcGarbageMatteSaveData, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::NewProp_Transform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::NewProp_Transform,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
		nullptr,
		&NewStructOps,
		"MrcGarbageMatteSaveData",
		sizeof(FMrcGarbageMatteSaveData),
		alignof(FMrcGarbageMatteSaveData),
		Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MrcGarbageMatteSaveData"), sizeof(FMrcGarbageMatteSaveData), Get_Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Hash() { return 661397210U; }
class UScriptStruct* FMrcAlignmentSaveData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MIXEDREALITYCAPTUREFRAMEWORK_API uint32 Get_Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMrcAlignmentSaveData, Z_Construct_UPackage__Script_MixedRealityCaptureFramework(), TEXT("MrcAlignmentSaveData"), sizeof(FMrcAlignmentSaveData), Get_Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Hash());
	}
	return Singleton;
}
template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<FMrcAlignmentSaveData>()
{
	return FMrcAlignmentSaveData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMrcAlignmentSaveData(FMrcAlignmentSaveData::StaticStruct, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("MrcAlignmentSaveData"), false, nullptr, nullptr);
static struct FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcAlignmentSaveData
{
	FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcAlignmentSaveData()
	{
		UScriptStruct::DeferCppStructOps<FMrcAlignmentSaveData>(FName(TEXT("MrcAlignmentSaveData")));
	}
} ScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcAlignmentSaveData;
	struct Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Orientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Orientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingAttachmentId_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TrackingAttachmentId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TrackingOrigin;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMrcAlignmentSaveData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_CameraOrigin_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_CameraOrigin = { "CameraOrigin", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcAlignmentSaveData, CameraOrigin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_CameraOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_CameraOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_Orientation_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_Orientation = { "Orientation", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcAlignmentSaveData, Orientation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_Orientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_Orientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_TrackingAttachmentId_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_TrackingAttachmentId = { "TrackingAttachmentId", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcAlignmentSaveData, TrackingAttachmentId), METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_TrackingAttachmentId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_TrackingAttachmentId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_TrackingOrigin_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_TrackingOrigin = { "TrackingOrigin", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcAlignmentSaveData, TrackingOrigin), Z_Construct_UEnum_HeadMountedDisplay_EHMDTrackingOrigin, METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_TrackingOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_TrackingOrigin_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_CameraOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_Orientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_TrackingAttachmentId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::NewProp_TrackingOrigin,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
		nullptr,
		&NewStructOps,
		"MrcAlignmentSaveData",
		sizeof(FMrcAlignmentSaveData),
		alignof(FMrcAlignmentSaveData),
		Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMrcAlignmentSaveData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MrcAlignmentSaveData"), sizeof(FMrcAlignmentSaveData), Get_Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Hash() { return 2996861655U; }
class UScriptStruct* FMrcLensCalibrationData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MIXEDREALITYCAPTUREFRAMEWORK_API uint32 Get_Z_Construct_UScriptStruct_FMrcLensCalibrationData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMrcLensCalibrationData, Z_Construct_UPackage__Script_MixedRealityCaptureFramework(), TEXT("MrcLensCalibrationData"), sizeof(FMrcLensCalibrationData), Get_Z_Construct_UScriptStruct_FMrcLensCalibrationData_Hash());
	}
	return Singleton;
}
template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<FMrcLensCalibrationData>()
{
	return FMrcLensCalibrationData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMrcLensCalibrationData(FMrcLensCalibrationData::StaticStruct, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("MrcLensCalibrationData"), false, nullptr, nullptr);
static struct FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcLensCalibrationData
{
	FScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcLensCalibrationData()
	{
		UScriptStruct::DeferCppStructOps<FMrcLensCalibrationData>(FName(TEXT("MrcLensCalibrationData")));
	}
} ScriptStruct_MixedRealityCaptureFramework_StaticRegisterNativesFMrcLensCalibrationData;
	struct Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FOV_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FOV;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionParameters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMrcLensCalibrationData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewProp_FOV_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewProp_FOV = { "FOV", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcLensCalibrationData, FOV), METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewProp_FOV_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewProp_FOV_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewProp_DistortionParameters_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewProp_DistortionParameters = { "DistortionParameters", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMrcLensCalibrationData, DistortionParameters), Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters, METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewProp_DistortionParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewProp_DistortionParameters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewProp_FOV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::NewProp_DistortionParameters,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
		nullptr,
		&NewStructOps,
		"MrcLensCalibrationData",
		sizeof(FMrcLensCalibrationData),
		alignof(FMrcLensCalibrationData),
		Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMrcLensCalibrationData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMrcLensCalibrationData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MrcLensCalibrationData"), sizeof(FMrcLensCalibrationData), Get_Z_Construct_UScriptStruct_FMrcLensCalibrationData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMrcLensCalibrationData_Hash() { return 1105431244U; }
	void UMrcCalibrationData::StaticRegisterNativesUMrcCalibrationData()
	{
	}
	UClass* Z_Construct_UClass_UMrcCalibrationData_NoRegister()
	{
		return UMrcCalibrationData::StaticClass();
	}
	struct Z_Construct_UClass_UMrcCalibrationData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LensData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlignmentData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AlignmentData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GarbageMatteSaveDatas_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GarbageMatteSaveDatas_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GarbageMatteSaveDatas;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CompositingData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CompositingData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMrcCalibrationData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USaveGame,
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcCalibrationData_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MrcCalibrationData.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_LensData_MetaData[] = {
		{ "Category", "SaveData" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_LensData = { "LensData", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcCalibrationData, LensData), Z_Construct_UScriptStruct_FMrcLensCalibrationData, METADATA_PARAMS(Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_LensData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_LensData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_AlignmentData_MetaData[] = {
		{ "Category", "SaveData" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_AlignmentData = { "AlignmentData", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcCalibrationData, AlignmentData), Z_Construct_UScriptStruct_FMrcAlignmentSaveData, METADATA_PARAMS(Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_AlignmentData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_AlignmentData_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_GarbageMatteSaveDatas_Inner = { "GarbageMatteSaveDatas", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_GarbageMatteSaveDatas_MetaData[] = {
		{ "Category", "SaveData" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_GarbageMatteSaveDatas = { "GarbageMatteSaveDatas", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcCalibrationData, GarbageMatteSaveDatas), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_GarbageMatteSaveDatas_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_GarbageMatteSaveDatas_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_CompositingData_MetaData[] = {
		{ "Category", "SaveData" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_CompositingData = { "CompositingData", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcCalibrationData, CompositingData), Z_Construct_UScriptStruct_FMrcCompositingSaveData, METADATA_PARAMS(Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_CompositingData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_CompositingData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMrcCalibrationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_LensData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_AlignmentData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_GarbageMatteSaveDatas_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_GarbageMatteSaveDatas,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcCalibrationData_Statics::NewProp_CompositingData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMrcCalibrationData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMrcCalibrationData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMrcCalibrationData_Statics::ClassParams = {
		&UMrcCalibrationData::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMrcCalibrationData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationData_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMrcCalibrationData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMrcCalibrationData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMrcCalibrationData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMrcCalibrationData, 3568789905);
	template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<UMrcCalibrationData>()
	{
		return UMrcCalibrationData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMrcCalibrationData(Z_Construct_UClass_UMrcCalibrationData, &UMrcCalibrationData::StaticClass, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("UMrcCalibrationData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMrcCalibrationData);
	void UMrcCalibrationSaveGame::StaticRegisterNativesUMrcCalibrationSaveGame()
	{
	}
	UClass* Z_Construct_UClass_UMrcCalibrationSaveGame_NoRegister()
	{
		return UMrcCalibrationSaveGame::StaticClass();
	}
	struct Z_Construct_UClass_UMrcCalibrationSaveGame_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SaveSlotName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SaveSlotName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UserIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConfigurationSaveVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ConfigurationSaveVersion;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMrcCalibrationData,
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MrcCalibrationData.h" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_SaveSlotName_MetaData[] = {
		{ "Category", "SaveMetadata" },
		{ "Comment", "// Metadata about the save file\n" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
		{ "ToolTip", "Metadata about the save file" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_SaveSlotName = { "SaveSlotName", nullptr, (EPropertyFlags)0x0010000000004004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcCalibrationSaveGame, SaveSlotName), METADATA_PARAMS(Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_SaveSlotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_SaveSlotName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_UserIndex_MetaData[] = {
		{ "Category", "SaveMetadata" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_UserIndex = { "UserIndex", nullptr, (EPropertyFlags)0x0010000000004004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcCalibrationSaveGame, UserIndex), METADATA_PARAMS(Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_UserIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_UserIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_ConfigurationSaveVersion_MetaData[] = {
		{ "Category", "SaveMetadata" },
		{ "ModuleRelativePath", "Public/MrcCalibrationData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_ConfigurationSaveVersion = { "ConfigurationSaveVersion", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcCalibrationSaveGame, ConfigurationSaveVersion), METADATA_PARAMS(Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_ConfigurationSaveVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_ConfigurationSaveVersion_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_SaveSlotName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_UserIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::NewProp_ConfigurationSaveVersion,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMrcCalibrationSaveGame>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::ClassParams = {
		&UMrcCalibrationSaveGame::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMrcCalibrationSaveGame()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMrcCalibrationSaveGame_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMrcCalibrationSaveGame, 743816136);
	template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<UMrcCalibrationSaveGame>()
	{
		return UMrcCalibrationSaveGame::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMrcCalibrationSaveGame(Z_Construct_UClass_UMrcCalibrationSaveGame, &UMrcCalibrationSaveGame::StaticClass, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("UMrcCalibrationSaveGame"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMrcCalibrationSaveGame);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
