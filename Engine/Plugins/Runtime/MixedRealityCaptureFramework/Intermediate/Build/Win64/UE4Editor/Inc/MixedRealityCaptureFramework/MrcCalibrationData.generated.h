// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MIXEDREALITYCAPTUREFRAMEWORK_MrcCalibrationData_generated_h
#error "MrcCalibrationData.generated.h already included, missing '#pragma once' in MrcCalibrationData.h"
#endif
#define MIXEDREALITYCAPTUREFRAMEWORK_MrcCalibrationData_generated_h

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_69_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMrcCompositingSaveData_Statics; \
	MIXEDREALITYCAPTUREFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<struct FMrcCompositingSaveData>();

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_57_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMrcVideoProcessingParams_Statics; \
	MIXEDREALITYCAPTUREFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<struct FMrcVideoProcessingParams>();

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_48_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData_Statics; \
	MIXEDREALITYCAPTUREFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<struct FMrcGarbageMatteSaveData>();

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_30_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMrcAlignmentSaveData_Statics; \
	MIXEDREALITYCAPTUREFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<struct FMrcAlignmentSaveData>();

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMrcLensCalibrationData_Statics; \
	MIXEDREALITYCAPTUREFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* StaticStruct<struct FMrcLensCalibrationData>();

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_SPARSE_DATA
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_RPC_WRAPPERS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMrcCalibrationData(); \
	friend struct Z_Construct_UClass_UMrcCalibrationData_Statics; \
public: \
	DECLARE_CLASS(UMrcCalibrationData, USaveGame, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcCalibrationData) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_INCLASS \
private: \
	static void StaticRegisterNativesUMrcCalibrationData(); \
	friend struct Z_Construct_UClass_UMrcCalibrationData_Statics; \
public: \
	DECLARE_CLASS(UMrcCalibrationData, USaveGame, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcCalibrationData) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcCalibrationData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcCalibrationData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcCalibrationData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcCalibrationData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcCalibrationData(UMrcCalibrationData&&); \
	NO_API UMrcCalibrationData(const UMrcCalibrationData&); \
public:


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcCalibrationData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcCalibrationData(UMrcCalibrationData&&); \
	NO_API UMrcCalibrationData(const UMrcCalibrationData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcCalibrationData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcCalibrationData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcCalibrationData)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_84_PROLOG
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_INCLASS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_87_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MrcCalibrationData."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<class UMrcCalibrationData>();

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_SPARSE_DATA
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_RPC_WRAPPERS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMrcCalibrationSaveGame(); \
	friend struct Z_Construct_UClass_UMrcCalibrationSaveGame_Statics; \
public: \
	DECLARE_CLASS(UMrcCalibrationSaveGame, UMrcCalibrationData, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcCalibrationSaveGame)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_INCLASS \
private: \
	static void StaticRegisterNativesUMrcCalibrationSaveGame(); \
	friend struct Z_Construct_UClass_UMrcCalibrationSaveGame_Statics; \
public: \
	DECLARE_CLASS(UMrcCalibrationSaveGame, UMrcCalibrationData, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcCalibrationSaveGame)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcCalibrationSaveGame(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcCalibrationSaveGame) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcCalibrationSaveGame); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcCalibrationSaveGame); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcCalibrationSaveGame(UMrcCalibrationSaveGame&&); \
	NO_API UMrcCalibrationSaveGame(const UMrcCalibrationSaveGame&); \
public:


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcCalibrationSaveGame(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcCalibrationSaveGame(UMrcCalibrationSaveGame&&); \
	NO_API UMrcCalibrationSaveGame(const UMrcCalibrationSaveGame&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcCalibrationSaveGame); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcCalibrationSaveGame); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcCalibrationSaveGame)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_106_PROLOG
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_INCLASS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h_109_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MrcCalibrationSaveGame."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<class UMrcCalibrationSaveGame>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcCalibrationData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
