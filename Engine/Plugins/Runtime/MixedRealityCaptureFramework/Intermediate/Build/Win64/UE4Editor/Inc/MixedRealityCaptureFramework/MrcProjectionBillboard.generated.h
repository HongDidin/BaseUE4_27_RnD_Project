// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MIXEDREALITYCAPTUREFRAMEWORK_MrcProjectionBillboard_generated_h
#error "MrcProjectionBillboard.generated.h already included, missing '#pragma once' in MrcProjectionBillboard.h"
#endif
#define MIXEDREALITYCAPTUREFRAMEWORK_MrcProjectionBillboard_generated_h

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_SPARSE_DATA
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_RPC_WRAPPERS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMixedRealityCaptureBillboard(); \
	friend struct Z_Construct_UClass_UMixedRealityCaptureBillboard_Statics; \
public: \
	DECLARE_CLASS(UMixedRealityCaptureBillboard, UMaterialBillboardComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMixedRealityCaptureBillboard)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMixedRealityCaptureBillboard(); \
	friend struct Z_Construct_UClass_UMixedRealityCaptureBillboard_Statics; \
public: \
	DECLARE_CLASS(UMixedRealityCaptureBillboard, UMaterialBillboardComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMixedRealityCaptureBillboard)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMixedRealityCaptureBillboard(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMixedRealityCaptureBillboard) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMixedRealityCaptureBillboard); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMixedRealityCaptureBillboard); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMixedRealityCaptureBillboard(UMixedRealityCaptureBillboard&&); \
	NO_API UMixedRealityCaptureBillboard(const UMixedRealityCaptureBillboard&); \
public:


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMixedRealityCaptureBillboard(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMixedRealityCaptureBillboard(UMixedRealityCaptureBillboard&&); \
	NO_API UMixedRealityCaptureBillboard(const UMixedRealityCaptureBillboard&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMixedRealityCaptureBillboard); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMixedRealityCaptureBillboard); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMixedRealityCaptureBillboard)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_12_PROLOG
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_INCLASS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MixedRealityCaptureBillboard."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<class UMixedRealityCaptureBillboard>();

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_SPARSE_DATA
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_RPC_WRAPPERS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMrcProjectionActor(); \
	friend struct Z_Construct_UClass_AMrcProjectionActor_Statics; \
public: \
	DECLARE_CLASS(AMrcProjectionActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(AMrcProjectionActor)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_INCLASS \
private: \
	static void StaticRegisterNativesAMrcProjectionActor(); \
	friend struct Z_Construct_UClass_AMrcProjectionActor_Statics; \
public: \
	DECLARE_CLASS(AMrcProjectionActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(AMrcProjectionActor)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMrcProjectionActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMrcProjectionActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMrcProjectionActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMrcProjectionActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMrcProjectionActor(AMrcProjectionActor&&); \
	NO_API AMrcProjectionActor(const AMrcProjectionActor&); \
public:


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMrcProjectionActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMrcProjectionActor(AMrcProjectionActor&&); \
	NO_API AMrcProjectionActor(const AMrcProjectionActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMrcProjectionActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMrcProjectionActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMrcProjectionActor)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AttachTarget() { return STRUCT_OFFSET(AMrcProjectionActor, AttachTarget); }


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_42_PROLOG
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_INCLASS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h_45_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MrcProjectionActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<class AMrcProjectionActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcProjectionBillboard_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
