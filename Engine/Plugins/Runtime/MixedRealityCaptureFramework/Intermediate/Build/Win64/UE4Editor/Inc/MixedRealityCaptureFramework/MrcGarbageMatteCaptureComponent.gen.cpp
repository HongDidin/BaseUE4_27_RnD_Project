// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MixedRealityCaptureFramework/Public/MrcGarbageMatteCaptureComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMrcGarbageMatteCaptureComponent() {}
// Cross Module References
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMrcGarbageMatteCaptureComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D();
	UPackage* Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMrcCalibrationData_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UScriptStruct* Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMrcFocalDriver_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_AMrcGarbageMatteActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_AMrcGarbageMatteActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMrcGarbageMatteCaptureComponent::execSpawnNewGarbageMatteActor)
	{
		P_GET_OBJECT(USceneComponent,Z_Param_TrackingOrigin);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AMrcGarbageMatteActor**)Z_Param__Result=P_THIS->SpawnNewGarbageMatteActor_Implementation(Z_Param_TrackingOrigin);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMrcGarbageMatteCaptureComponent::execSetFocalDriver)
	{
		P_GET_TINTERFACE(IMrcFocalDriver,Z_Param_InFocalDriver);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFocalDriver(Z_Param_InFocalDriver);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMrcGarbageMatteCaptureComponent::execGetGarbageMatteData)
	{
		P_GET_TARRAY_REF(FMrcGarbageMatteSaveData,Z_Param_Out_GarbageMatteDataOut);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetGarbageMatteData(Z_Param_Out_GarbageMatteDataOut);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMrcGarbageMatteCaptureComponent::execSetGarbageMatteActor)
	{
		P_GET_OBJECT(AMrcGarbageMatteActor,Z_Param_NewActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetGarbageMatteActor(Z_Param_NewActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMrcGarbageMatteCaptureComponent::execApplyCalibrationData)
	{
		P_GET_OBJECT(UMrcCalibrationData,Z_Param_ConfigData);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ApplyCalibrationData_Implementation(Z_Param_ConfigData);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMrcGarbageMatteCaptureComponent::execSetTrackingOrigin)
	{
		P_GET_OBJECT(USceneComponent,Z_Param_TrackingOrigin);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTrackingOrigin(Z_Param_TrackingOrigin);
		P_NATIVE_END;
	}
	static FName NAME_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData = FName(TEXT("ApplyCalibrationData"));
	void UMrcGarbageMatteCaptureComponent::ApplyCalibrationData(const UMrcCalibrationData* ConfigData)
	{
		MrcGarbageMatteCaptureComponent_eventApplyCalibrationData_Parms Parms;
		Parms.ConfigData=ConfigData;
		ProcessEvent(FindFunctionChecked(NAME_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData),&Parms);
	}
	static FName NAME_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor = FName(TEXT("SpawnNewGarbageMatteActor"));
	AMrcGarbageMatteActor* UMrcGarbageMatteCaptureComponent::SpawnNewGarbageMatteActor(USceneComponent* TrackingOrigin)
	{
		MrcGarbageMatteCaptureComponent_eventSpawnNewGarbageMatteActor_Parms Parms;
		Parms.TrackingOrigin=TrackingOrigin;
		ProcessEvent(FindFunctionChecked(NAME_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor),&Parms);
		return Parms.ReturnValue;
	}
	void UMrcGarbageMatteCaptureComponent::StaticRegisterNativesUMrcGarbageMatteCaptureComponent()
	{
		UClass* Class = UMrcGarbageMatteCaptureComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ApplyCalibrationData", &UMrcGarbageMatteCaptureComponent::execApplyCalibrationData },
			{ "GetGarbageMatteData", &UMrcGarbageMatteCaptureComponent::execGetGarbageMatteData },
			{ "SetFocalDriver", &UMrcGarbageMatteCaptureComponent::execSetFocalDriver },
			{ "SetGarbageMatteActor", &UMrcGarbageMatteCaptureComponent::execSetGarbageMatteActor },
			{ "SetTrackingOrigin", &UMrcGarbageMatteCaptureComponent::execSetTrackingOrigin },
			{ "SpawnNewGarbageMatteActor", &UMrcGarbageMatteCaptureComponent::execSpawnNewGarbageMatteActor },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConfigData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConfigData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::NewProp_ConfigData_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::NewProp_ConfigData = { "ConfigData", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteCaptureComponent_eventApplyCalibrationData_Parms, ConfigData), Z_Construct_UClass_UMrcCalibrationData_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::NewProp_ConfigData_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::NewProp_ConfigData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::NewProp_ConfigData,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::Function_MetaDataParams[] = {
		{ "Category", "MixedRealityCapture|GarbageMatting" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMrcGarbageMatteCaptureComponent, nullptr, "ApplyCalibrationData", nullptr, nullptr, sizeof(MrcGarbageMatteCaptureComponent_eventApplyCalibrationData_Parms), Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics
	{
		struct MrcGarbageMatteCaptureComponent_eventGetGarbageMatteData_Parms
		{
			TArray<FMrcGarbageMatteSaveData> GarbageMatteDataOut;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GarbageMatteDataOut_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GarbageMatteDataOut;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::NewProp_GarbageMatteDataOut_Inner = { "GarbageMatteDataOut", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::NewProp_GarbageMatteDataOut = { "GarbageMatteDataOut", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteCaptureComponent_eventGetGarbageMatteData_Parms, GarbageMatteDataOut), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::NewProp_GarbageMatteDataOut_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::NewProp_GarbageMatteDataOut,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::Function_MetaDataParams[] = {
		{ "Category", "MixedRealityCapture|GarbageMatting" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMrcGarbageMatteCaptureComponent, nullptr, "GetGarbageMatteData", nullptr, nullptr, sizeof(MrcGarbageMatteCaptureComponent_eventGetGarbageMatteData_Parms), Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics
	{
		struct MrcGarbageMatteCaptureComponent_eventSetFocalDriver_Parms
		{
			TScriptInterface<IMrcFocalDriver> InFocalDriver;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_InFocalDriver;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics::NewProp_InFocalDriver = { "InFocalDriver", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteCaptureComponent_eventSetFocalDriver_Parms, InFocalDriver), Z_Construct_UClass_UMrcFocalDriver_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics::NewProp_InFocalDriver,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics::Function_MetaDataParams[] = {
		{ "Category", "MixedRealityCapture|GarbageMatting" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMrcGarbageMatteCaptureComponent, nullptr, "SetFocalDriver", nullptr, nullptr, sizeof(MrcGarbageMatteCaptureComponent_eventSetFocalDriver_Parms), Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics
	{
		struct MrcGarbageMatteCaptureComponent_eventSetGarbageMatteActor_Parms
		{
			AMrcGarbageMatteActor* NewActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics::NewProp_NewActor = { "NewActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteCaptureComponent_eventSetGarbageMatteActor_Parms, NewActor), Z_Construct_UClass_AMrcGarbageMatteActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics::NewProp_NewActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "MixedRealityCapture|GarbageMatting" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMrcGarbageMatteCaptureComponent, nullptr, "SetGarbageMatteActor", nullptr, nullptr, sizeof(MrcGarbageMatteCaptureComponent_eventSetGarbageMatteActor_Parms), Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics
	{
		struct MrcGarbageMatteCaptureComponent_eventSetTrackingOrigin_Parms
		{
			USceneComponent* TrackingOrigin;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TrackingOrigin;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::NewProp_TrackingOrigin_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::NewProp_TrackingOrigin = { "TrackingOrigin", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteCaptureComponent_eventSetTrackingOrigin_Parms, TrackingOrigin), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::NewProp_TrackingOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::NewProp_TrackingOrigin_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::NewProp_TrackingOrigin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMrcGarbageMatteCaptureComponent, nullptr, "SetTrackingOrigin", nullptr, nullptr, sizeof(MrcGarbageMatteCaptureComponent_eventSetTrackingOrigin_Parms), Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TrackingOrigin;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::NewProp_TrackingOrigin_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::NewProp_TrackingOrigin = { "TrackingOrigin", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteCaptureComponent_eventSpawnNewGarbageMatteActor_Parms, TrackingOrigin), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::NewProp_TrackingOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::NewProp_TrackingOrigin_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteCaptureComponent_eventSpawnNewGarbageMatteActor_Parms, ReturnValue), Z_Construct_UClass_AMrcGarbageMatteActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::NewProp_TrackingOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "MixedRealityCapture|GarbageMatting" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMrcGarbageMatteCaptureComponent, nullptr, "SpawnNewGarbageMatteActor", nullptr, nullptr, sizeof(MrcGarbageMatteCaptureComponent_eventSpawnNewGarbageMatteActor_Parms), Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_NoRegister()
	{
		return UMrcGarbageMatteCaptureComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GarbageMatteActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_GarbageMatteActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GarbageMatteActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GarbageMatteActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpawnedActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnedActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SpawnedActors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingOriginPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_TrackingOriginPtr;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalDriver_MetaData[];
#endif
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_FocalDriver;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneCaptureComponent2D,
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_ApplyCalibrationData, "ApplyCalibrationData" }, // 3487248781
		{ &Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_GetGarbageMatteData, "GetGarbageMatteData" }, // 555918584
		{ &Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetFocalDriver, "SetFocalDriver" }, // 1405419019
		{ &Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetGarbageMatteActor, "SetGarbageMatteActor" }, // 1758059951
		{ &Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SetTrackingOrigin, "SetTrackingOrigin" }, // 644946579
		{ &Z_Construct_UFunction_UMrcGarbageMatteCaptureComponent_SpawnNewGarbageMatteActor, "SpawnNewGarbageMatteActor" }, // 3204572248
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Rendering" },
		{ "Comment", "/**\n *\x09\n */" },
		{ "HideCategories", "Collision Object Physics SceneComponent Collision Object Physics SceneComponent Mobility Trigger PhysicsVolume" },
		{ "IncludePath", "MrcGarbageMatteCaptureComponent.h" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_GarbageMatteActorClass_MetaData[] = {
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_GarbageMatteActorClass = { "GarbageMatteActorClass", nullptr, (EPropertyFlags)0x0044000000006000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcGarbageMatteCaptureComponent, GarbageMatteActorClass), Z_Construct_UClass_AMrcGarbageMatteActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_GarbageMatteActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_GarbageMatteActorClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_GarbageMatteActor_MetaData[] = {
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_GarbageMatteActor = { "GarbageMatteActor", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcGarbageMatteCaptureComponent, GarbageMatteActor), Z_Construct_UClass_AMrcGarbageMatteActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_GarbageMatteActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_GarbageMatteActor_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_SpawnedActors_Inner = { "SpawnedActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AMrcGarbageMatteActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_SpawnedActors_MetaData[] = {
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_SpawnedActors = { "SpawnedActors", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcGarbageMatteCaptureComponent, SpawnedActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_SpawnedActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_SpawnedActors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_TrackingOriginPtr_MetaData[] = {
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_TrackingOriginPtr = { "TrackingOriginPtr", nullptr, (EPropertyFlags)0x0044000000082008, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcGarbageMatteCaptureComponent, TrackingOriginPtr), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_TrackingOriginPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_TrackingOriginPtr_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_FocalDriver_MetaData[] = {
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_FocalDriver = { "FocalDriver", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMrcGarbageMatteCaptureComponent, FocalDriver), Z_Construct_UClass_UMrcFocalDriver_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_FocalDriver_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_FocalDriver_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_GarbageMatteActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_GarbageMatteActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_SpawnedActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_SpawnedActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_TrackingOriginPtr,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::NewProp_FocalDriver,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMrcGarbageMatteCaptureComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::ClassParams = {
		&UMrcGarbageMatteCaptureComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMrcGarbageMatteCaptureComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMrcGarbageMatteCaptureComponent, 315770414);
	template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<UMrcGarbageMatteCaptureComponent>()
	{
		return UMrcGarbageMatteCaptureComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMrcGarbageMatteCaptureComponent(Z_Construct_UClass_UMrcGarbageMatteCaptureComponent, &UMrcGarbageMatteCaptureComponent::StaticClass, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("UMrcGarbageMatteCaptureComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMrcGarbageMatteCaptureComponent);
	DEFINE_FUNCTION(AMrcGarbageMatteActor::execGetGarbageMatteData)
	{
		P_GET_TARRAY_REF(FMrcGarbageMatteSaveData,Z_Param_Out_GarbageMatteDataOut);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetGarbageMatteData(Z_Param_Out_GarbageMatteDataOut);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMrcGarbageMatteActor::execCreateGarbageMatte)
	{
		P_GET_STRUCT_REF(FMrcGarbageMatteSaveData,Z_Param_Out_GarbageMatteData);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UPrimitiveComponent**)Z_Param__Result=P_THIS->CreateGarbageMatte_Implementation(Z_Param_Out_GarbageMatteData);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMrcGarbageMatteActor::execAddNewGabageMatte)
	{
		P_GET_STRUCT_REF(FMrcGarbageMatteSaveData,Z_Param_Out_GarbageMatteData);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UPrimitiveComponent**)Z_Param__Result=P_THIS->AddNewGabageMatte(Z_Param_Out_GarbageMatteData);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMrcGarbageMatteActor::execApplyCalibrationData)
	{
		P_GET_TARRAY_REF(FMrcGarbageMatteSaveData,Z_Param_Out_GarbageMatteData);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ApplyCalibrationData(Z_Param_Out_GarbageMatteData);
		P_NATIVE_END;
	}
	static FName NAME_AMrcGarbageMatteActor_CreateGarbageMatte = FName(TEXT("CreateGarbageMatte"));
	UPrimitiveComponent* AMrcGarbageMatteActor::CreateGarbageMatte(FMrcGarbageMatteSaveData const& GarbageMatteData)
	{
		MrcGarbageMatteActor_eventCreateGarbageMatte_Parms Parms;
		Parms.GarbageMatteData=GarbageMatteData;
		ProcessEvent(FindFunctionChecked(NAME_AMrcGarbageMatteActor_CreateGarbageMatte),&Parms);
		return Parms.ReturnValue;
	}
	void AMrcGarbageMatteActor::StaticRegisterNativesAMrcGarbageMatteActor()
	{
		UClass* Class = AMrcGarbageMatteActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddNewGabageMatte", &AMrcGarbageMatteActor::execAddNewGabageMatte },
			{ "ApplyCalibrationData", &AMrcGarbageMatteActor::execApplyCalibrationData },
			{ "CreateGarbageMatte", &AMrcGarbageMatteActor::execCreateGarbageMatte },
			{ "GetGarbageMatteData", &AMrcGarbageMatteActor::execGetGarbageMatteData },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics
	{
		struct MrcGarbageMatteActor_eventAddNewGabageMatte_Parms
		{
			FMrcGarbageMatteSaveData GarbageMatteData;
			UPrimitiveComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GarbageMatteData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GarbageMatteData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::NewProp_GarbageMatteData_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::NewProp_GarbageMatteData = { "GarbageMatteData", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteActor_eventAddNewGabageMatte_Parms, GarbageMatteData), Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData, METADATA_PARAMS(Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::NewProp_GarbageMatteData_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::NewProp_GarbageMatteData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteActor_eventAddNewGabageMatte_Parms, ReturnValue), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::NewProp_GarbageMatteData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::Function_MetaDataParams[] = {
		{ "Category", "MixedRealityCapture|GarbageMatting" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMrcGarbageMatteActor, nullptr, "AddNewGabageMatte", nullptr, nullptr, sizeof(MrcGarbageMatteActor_eventAddNewGabageMatte_Parms), Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics
	{
		struct MrcGarbageMatteActor_eventApplyCalibrationData_Parms
		{
			TArray<FMrcGarbageMatteSaveData> GarbageMatteData;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GarbageMatteData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GarbageMatteData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GarbageMatteData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::NewProp_GarbageMatteData_Inner = { "GarbageMatteData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::NewProp_GarbageMatteData_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::NewProp_GarbageMatteData = { "GarbageMatteData", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteActor_eventApplyCalibrationData_Parms, GarbageMatteData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::NewProp_GarbageMatteData_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::NewProp_GarbageMatteData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::NewProp_GarbageMatteData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::NewProp_GarbageMatteData,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::Function_MetaDataParams[] = {
		{ "Category", "MixedRealityCapture|GarbageMatting" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMrcGarbageMatteActor, nullptr, "ApplyCalibrationData", nullptr, nullptr, sizeof(MrcGarbageMatteActor_eventApplyCalibrationData_Parms), Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GarbageMatteData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GarbageMatteData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::NewProp_GarbageMatteData_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::NewProp_GarbageMatteData = { "GarbageMatteData", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteActor_eventCreateGarbageMatte_Parms, GarbageMatteData), Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData, METADATA_PARAMS(Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::NewProp_GarbageMatteData_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::NewProp_GarbageMatteData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteActor_eventCreateGarbageMatte_Parms, ReturnValue), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::NewProp_GarbageMatteData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::Function_MetaDataParams[] = {
		{ "Category", "MixedRealityCapture|GarbageMatting" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMrcGarbageMatteActor, nullptr, "CreateGarbageMatte", nullptr, nullptr, sizeof(MrcGarbageMatteActor_eventCreateGarbageMatte_Parms), Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics
	{
		struct MrcGarbageMatteActor_eventGetGarbageMatteData_Parms
		{
			TArray<FMrcGarbageMatteSaveData> GarbageMatteDataOut;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GarbageMatteDataOut_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GarbageMatteDataOut;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::NewProp_GarbageMatteDataOut_Inner = { "GarbageMatteDataOut", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMrcGarbageMatteSaveData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::NewProp_GarbageMatteDataOut = { "GarbageMatteDataOut", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MrcGarbageMatteActor_eventGetGarbageMatteData_Parms, GarbageMatteDataOut), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::NewProp_GarbageMatteDataOut_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::NewProp_GarbageMatteDataOut,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::Function_MetaDataParams[] = {
		{ "Category", "MixedRealityCapture|GarbageMatting" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMrcGarbageMatteActor, nullptr, "GetGarbageMatteData", nullptr, nullptr, sizeof(MrcGarbageMatteActor_eventGetGarbageMatteData_Parms), Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMrcGarbageMatteActor_NoRegister()
	{
		return AMrcGarbageMatteActor::StaticClass();
	}
	struct Z_Construct_UClass_AMrcGarbageMatteActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GarbageMatteMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GarbageMatteMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GarbageMatteMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GarbageMatteMaterial;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GarbageMattes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GarbageMattes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GarbageMattes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMrcGarbageMatteActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMrcGarbageMatteActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMrcGarbageMatteActor_AddNewGabageMatte, "AddNewGabageMatte" }, // 2329161395
		{ &Z_Construct_UFunction_AMrcGarbageMatteActor_ApplyCalibrationData, "ApplyCalibrationData" }, // 3030855996
		{ &Z_Construct_UFunction_AMrcGarbageMatteActor_CreateGarbageMatte, "CreateGarbageMatte" }, // 994368745
		{ &Z_Construct_UFunction_AMrcGarbageMatteActor_GetGarbageMatteData, "GetGarbageMatteData" }, // 3568246314
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMrcGarbageMatteActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MrcGarbageMatteCaptureComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMatteMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMatteMesh = { "GarbageMatteMesh", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMrcGarbageMatteActor, GarbageMatteMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMatteMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMatteMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMatteMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMatteMaterial = { "GarbageMatteMaterial", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMrcGarbageMatteActor, GarbageMatteMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMatteMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMatteMaterial_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMattes_Inner = { "GarbageMattes", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMattes_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "MixedRealityCapture|GarbageMatting" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MrcGarbageMatteCaptureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMattes = { "GarbageMattes", nullptr, (EPropertyFlags)0x004000800000201c, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMrcGarbageMatteActor, GarbageMattes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMattes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMattes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMrcGarbageMatteActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMatteMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMatteMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMattes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMrcGarbageMatteActor_Statics::NewProp_GarbageMattes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMrcGarbageMatteActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMrcGarbageMatteActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMrcGarbageMatteActor_Statics::ClassParams = {
		&AMrcGarbageMatteActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMrcGarbageMatteActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMrcGarbageMatteActor_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMrcGarbageMatteActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMrcGarbageMatteActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMrcGarbageMatteActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMrcGarbageMatteActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMrcGarbageMatteActor, 51544130);
	template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<AMrcGarbageMatteActor>()
	{
		return AMrcGarbageMatteActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMrcGarbageMatteActor(Z_Construct_UClass_AMrcGarbageMatteActor, &AMrcGarbageMatteActor::StaticClass, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("AMrcGarbageMatteActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMrcGarbageMatteActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
