// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MIXEDREALITYCAPTUREFRAMEWORK_MrcFrameworkSettings_generated_h
#error "MrcFrameworkSettings.generated.h already included, missing '#pragma once' in MrcFrameworkSettings.h"
#endif
#define MIXEDREALITYCAPTUREFRAMEWORK_MrcFrameworkSettings_generated_h

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_SPARSE_DATA
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_RPC_WRAPPERS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMrcFrameworkSettings(); \
	friend struct Z_Construct_UClass_UMrcFrameworkSettings_Statics; \
public: \
	DECLARE_CLASS(UMrcFrameworkSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcFrameworkSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUMrcFrameworkSettings(); \
	friend struct Z_Construct_UClass_UMrcFrameworkSettings_Statics; \
public: \
	DECLARE_CLASS(UMrcFrameworkSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcFrameworkSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcFrameworkSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcFrameworkSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcFrameworkSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcFrameworkSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcFrameworkSettings(UMrcFrameworkSettings&&); \
	NO_API UMrcFrameworkSettings(const UMrcFrameworkSettings&); \
public:


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcFrameworkSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcFrameworkSettings(UMrcFrameworkSettings&&); \
	NO_API UMrcFrameworkSettings(const UMrcFrameworkSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcFrameworkSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcFrameworkSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcFrameworkSettings)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_9_PROLOG
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_INCLASS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h_12_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MrcFrameworkSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<class UMrcFrameworkSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Private_MrcFrameworkSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
