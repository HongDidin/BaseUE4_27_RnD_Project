// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MixedRealityCaptureFramework/Private/MrcProjectionBillboard.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMrcProjectionBillboard() {}
// Cross Module References
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMixedRealityCaptureBillboard_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_UMixedRealityCaptureBillboard();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialBillboardComponent();
	UPackage* Z_Construct_UPackage__Script_MixedRealityCaptureFramework();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_AMrcProjectionActor_NoRegister();
	MIXEDREALITYCAPTUREFRAMEWORK_API UClass* Z_Construct_UClass_AMrcProjectionActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	void UMixedRealityCaptureBillboard::StaticRegisterNativesUMixedRealityCaptureBillboard()
	{
	}
	UClass* Z_Construct_UClass_UMixedRealityCaptureBillboard_NoRegister()
	{
		return UMixedRealityCaptureBillboard::StaticClass();
	}
	struct Z_Construct_UClass_UMixedRealityCaptureBillboard_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMixedRealityCaptureBillboard_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMaterialBillboardComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMixedRealityCaptureBillboard_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object Activation Components|Activation Physics Collision Lighting Mesh PhysicsVolume Mobility VirtualTexture Trigger" },
		{ "IncludePath", "MrcProjectionBillboard.h" },
		{ "ModuleRelativePath", "Private/MrcProjectionBillboard.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMixedRealityCaptureBillboard_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMixedRealityCaptureBillboard>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMixedRealityCaptureBillboard_Statics::ClassParams = {
		&UMixedRealityCaptureBillboard::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00A030A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMixedRealityCaptureBillboard_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMixedRealityCaptureBillboard_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMixedRealityCaptureBillboard()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMixedRealityCaptureBillboard_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMixedRealityCaptureBillboard, 400780088);
	template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<UMixedRealityCaptureBillboard>()
	{
		return UMixedRealityCaptureBillboard::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMixedRealityCaptureBillboard(Z_Construct_UClass_UMixedRealityCaptureBillboard, &UMixedRealityCaptureBillboard::StaticClass, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("UMixedRealityCaptureBillboard"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMixedRealityCaptureBillboard);
	void AMrcProjectionActor::StaticRegisterNativesAMrcProjectionActor()
	{
	}
	UClass* Z_Construct_UClass_AMrcProjectionActor_NoRegister()
	{
		return AMrcProjectionActor::StaticClass();
	}
	struct Z_Construct_UClass_AMrcProjectionActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectionComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ProjectionComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttachTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_AttachTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMrcProjectionActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MixedRealityCaptureFramework,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMrcProjectionActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* AMrcProjectionActor\n *****************************************************************************/" },
		{ "IncludePath", "MrcProjectionBillboard.h" },
		{ "ModuleRelativePath", "Private/MrcProjectionBillboard.h" },
		{ "ToolTip", "AMrcProjectionActor" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMrcProjectionActor_Statics::NewProp_ProjectionComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/MrcProjectionBillboard.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMrcProjectionActor_Statics::NewProp_ProjectionComponent = { "ProjectionComponent", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMrcProjectionActor, ProjectionComponent), Z_Construct_UClass_UMixedRealityCaptureBillboard_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMrcProjectionActor_Statics::NewProp_ProjectionComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMrcProjectionActor_Statics::NewProp_ProjectionComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMrcProjectionActor_Statics::NewProp_AttachTarget_MetaData[] = {
		{ "ModuleRelativePath", "Private/MrcProjectionBillboard.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_AMrcProjectionActor_Statics::NewProp_AttachTarget = { "AttachTarget", nullptr, (EPropertyFlags)0x0044000000082008, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMrcProjectionActor, AttachTarget), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMrcProjectionActor_Statics::NewProp_AttachTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMrcProjectionActor_Statics::NewProp_AttachTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMrcProjectionActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMrcProjectionActor_Statics::NewProp_ProjectionComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMrcProjectionActor_Statics::NewProp_AttachTarget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMrcProjectionActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMrcProjectionActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMrcProjectionActor_Statics::ClassParams = {
		&AMrcProjectionActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMrcProjectionActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMrcProjectionActor_Statics::PropPointers),
		0,
		0x008002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMrcProjectionActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMrcProjectionActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMrcProjectionActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMrcProjectionActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMrcProjectionActor, 3626201005);
	template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<AMrcProjectionActor>()
	{
		return AMrcProjectionActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMrcProjectionActor(Z_Construct_UClass_AMrcProjectionActor, &AMrcProjectionActor::StaticClass, TEXT("/Script/MixedRealityCaptureFramework"), TEXT("AMrcProjectionActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMrcProjectionActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
