// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AMrcGarbageMatteActor;
class USceneComponent;
class IMrcFocalDriver;
struct FMrcGarbageMatteSaveData;
class UMrcCalibrationData;
class UPrimitiveComponent;
#ifdef MIXEDREALITYCAPTUREFRAMEWORK_MrcGarbageMatteCaptureComponent_generated_h
#error "MrcGarbageMatteCaptureComponent.generated.h already included, missing '#pragma once' in MrcGarbageMatteCaptureComponent.h"
#endif
#define MIXEDREALITYCAPTUREFRAMEWORK_MrcGarbageMatteCaptureComponent_generated_h

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_SPARSE_DATA
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_RPC_WRAPPERS \
	virtual AMrcGarbageMatteActor* SpawnNewGarbageMatteActor_Implementation(USceneComponent* TrackingOrigin); \
	virtual void ApplyCalibrationData_Implementation(const UMrcCalibrationData* ConfigData); \
 \
	DECLARE_FUNCTION(execSpawnNewGarbageMatteActor); \
	DECLARE_FUNCTION(execSetFocalDriver); \
	DECLARE_FUNCTION(execGetGarbageMatteData); \
	DECLARE_FUNCTION(execSetGarbageMatteActor); \
	DECLARE_FUNCTION(execApplyCalibrationData); \
	DECLARE_FUNCTION(execSetTrackingOrigin);


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual AMrcGarbageMatteActor* SpawnNewGarbageMatteActor_Implementation(USceneComponent* TrackingOrigin); \
	virtual void ApplyCalibrationData_Implementation(const UMrcCalibrationData* ConfigData); \
 \
	DECLARE_FUNCTION(execSpawnNewGarbageMatteActor); \
	DECLARE_FUNCTION(execSetFocalDriver); \
	DECLARE_FUNCTION(execGetGarbageMatteData); \
	DECLARE_FUNCTION(execSetGarbageMatteActor); \
	DECLARE_FUNCTION(execApplyCalibrationData); \
	DECLARE_FUNCTION(execSetTrackingOrigin);


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_EVENT_PARMS \
	struct MrcGarbageMatteCaptureComponent_eventApplyCalibrationData_Parms \
	{ \
		const UMrcCalibrationData* ConfigData; \
	}; \
	struct MrcGarbageMatteCaptureComponent_eventSpawnNewGarbageMatteActor_Parms \
	{ \
		USceneComponent* TrackingOrigin; \
		AMrcGarbageMatteActor* ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		MrcGarbageMatteCaptureComponent_eventSpawnNewGarbageMatteActor_Parms() \
			: ReturnValue(NULL) \
		{ \
		} \
	};


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_CALLBACK_WRAPPERS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMrcGarbageMatteCaptureComponent(); \
	friend struct Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics; \
public: \
	DECLARE_CLASS(UMrcGarbageMatteCaptureComponent, USceneCaptureComponent2D, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcGarbageMatteCaptureComponent)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUMrcGarbageMatteCaptureComponent(); \
	friend struct Z_Construct_UClass_UMrcGarbageMatteCaptureComponent_Statics; \
public: \
	DECLARE_CLASS(UMrcGarbageMatteCaptureComponent, USceneCaptureComponent2D, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcGarbageMatteCaptureComponent)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcGarbageMatteCaptureComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcGarbageMatteCaptureComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcGarbageMatteCaptureComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcGarbageMatteCaptureComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcGarbageMatteCaptureComponent(UMrcGarbageMatteCaptureComponent&&); \
	NO_API UMrcGarbageMatteCaptureComponent(const UMrcGarbageMatteCaptureComponent&); \
public:


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcGarbageMatteCaptureComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcGarbageMatteCaptureComponent(UMrcGarbageMatteCaptureComponent&&); \
	NO_API UMrcGarbageMatteCaptureComponent(const UMrcGarbageMatteCaptureComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcGarbageMatteCaptureComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcGarbageMatteCaptureComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcGarbageMatteCaptureComponent)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GarbageMatteActorClass() { return STRUCT_OFFSET(UMrcGarbageMatteCaptureComponent, GarbageMatteActorClass); } \
	FORCEINLINE static uint32 __PPO__GarbageMatteActor() { return STRUCT_OFFSET(UMrcGarbageMatteCaptureComponent, GarbageMatteActor); } \
	FORCEINLINE static uint32 __PPO__SpawnedActors() { return STRUCT_OFFSET(UMrcGarbageMatteCaptureComponent, SpawnedActors); } \
	FORCEINLINE static uint32 __PPO__TrackingOriginPtr() { return STRUCT_OFFSET(UMrcGarbageMatteCaptureComponent, TrackingOriginPtr); } \
	FORCEINLINE static uint32 __PPO__FocalDriver() { return STRUCT_OFFSET(UMrcGarbageMatteCaptureComponent, FocalDriver); }


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_17_PROLOG \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_EVENT_PARMS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_INCLASS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MrcGarbageMatteCaptureComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<class UMrcGarbageMatteCaptureComponent>();

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_SPARSE_DATA
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_RPC_WRAPPERS \
	virtual UPrimitiveComponent* CreateGarbageMatte_Implementation(FMrcGarbageMatteSaveData const& GarbageMatteData); \
 \
	DECLARE_FUNCTION(execGetGarbageMatteData); \
	DECLARE_FUNCTION(execCreateGarbageMatte); \
	DECLARE_FUNCTION(execAddNewGabageMatte); \
	DECLARE_FUNCTION(execApplyCalibrationData);


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual UPrimitiveComponent* CreateGarbageMatte_Implementation(FMrcGarbageMatteSaveData const& GarbageMatteData); \
 \
	DECLARE_FUNCTION(execGetGarbageMatteData); \
	DECLARE_FUNCTION(execCreateGarbageMatte); \
	DECLARE_FUNCTION(execAddNewGabageMatte); \
	DECLARE_FUNCTION(execApplyCalibrationData);


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_EVENT_PARMS \
	struct MrcGarbageMatteActor_eventCreateGarbageMatte_Parms \
	{ \
		FMrcGarbageMatteSaveData GarbageMatteData; \
		UPrimitiveComponent* ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		MrcGarbageMatteActor_eventCreateGarbageMatte_Parms() \
			: ReturnValue(NULL) \
		{ \
		} \
	};


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_CALLBACK_WRAPPERS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMrcGarbageMatteActor(); \
	friend struct Z_Construct_UClass_AMrcGarbageMatteActor_Statics; \
public: \
	DECLARE_CLASS(AMrcGarbageMatteActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(AMrcGarbageMatteActor)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_INCLASS \
private: \
	static void StaticRegisterNativesAMrcGarbageMatteActor(); \
	friend struct Z_Construct_UClass_AMrcGarbageMatteActor_Statics; \
public: \
	DECLARE_CLASS(AMrcGarbageMatteActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(AMrcGarbageMatteActor)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMrcGarbageMatteActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMrcGarbageMatteActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMrcGarbageMatteActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMrcGarbageMatteActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMrcGarbageMatteActor(AMrcGarbageMatteActor&&); \
	NO_API AMrcGarbageMatteActor(const AMrcGarbageMatteActor&); \
public:


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMrcGarbageMatteActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMrcGarbageMatteActor(AMrcGarbageMatteActor&&); \
	NO_API AMrcGarbageMatteActor(const AMrcGarbageMatteActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMrcGarbageMatteActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMrcGarbageMatteActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMrcGarbageMatteActor)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GarbageMatteMesh() { return STRUCT_OFFSET(AMrcGarbageMatteActor, GarbageMatteMesh); } \
	FORCEINLINE static uint32 __PPO__GarbageMatteMaterial() { return STRUCT_OFFSET(AMrcGarbageMatteActor, GarbageMatteMaterial); } \
	FORCEINLINE static uint32 __PPO__GarbageMattes() { return STRUCT_OFFSET(AMrcGarbageMatteActor, GarbageMattes); }


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_78_PROLOG \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_EVENT_PARMS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_INCLASS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h_81_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MrcGarbageMatteActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<class AMrcGarbageMatteActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_MrcGarbageMatteCaptureComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
