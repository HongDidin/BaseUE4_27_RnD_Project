// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MIXEDREALITYCAPTUREFRAMEWORK_IMrcFocalDriver_generated_h
#error "IMrcFocalDriver.generated.h already included, missing '#pragma once' in IMrcFocalDriver.h"
#endif
#define MIXEDREALITYCAPTUREFRAMEWORK_IMrcFocalDriver_generated_h

#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_SPARSE_DATA
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_RPC_WRAPPERS \
	virtual float GetHorizontalFieldOfView_Implementation() const { return 0; }; \
 \
	DECLARE_FUNCTION(execGetHorizontalFieldOfView);


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual float GetHorizontalFieldOfView_Implementation() const { return 0; }; \
 \
	DECLARE_FUNCTION(execGetHorizontalFieldOfView);


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_EVENT_PARMS \
	struct MrcFocalDriver_eventGetHorizontalFieldOfView_Parms \
	{ \
		float ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		MrcFocalDriver_eventGetHorizontalFieldOfView_Parms() \
			: ReturnValue(0) \
		{ \
		} \
	};


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_CALLBACK_WRAPPERS
#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcFocalDriver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcFocalDriver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcFocalDriver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcFocalDriver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcFocalDriver(UMrcFocalDriver&&); \
	NO_API UMrcFocalDriver(const UMrcFocalDriver&); \
public:


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMrcFocalDriver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMrcFocalDriver(UMrcFocalDriver&&); \
	NO_API UMrcFocalDriver(const UMrcFocalDriver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMrcFocalDriver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMrcFocalDriver); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMrcFocalDriver)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUMrcFocalDriver(); \
	friend struct Z_Construct_UClass_UMrcFocalDriver_Statics; \
public: \
	DECLARE_CLASS(UMrcFocalDriver, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/MixedRealityCaptureFramework"), NO_API) \
	DECLARE_SERIALIZER(UMrcFocalDriver)


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_ENHANCED_CONSTRUCTORS \
public: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IMrcFocalDriver() {} \
public: \
	typedef UMrcFocalDriver UClassType; \
	typedef IMrcFocalDriver ThisClass; \
	static float Execute_GetHorizontalFieldOfView(const UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_INCLASS_IINTERFACE \
protected: \
	virtual ~IMrcFocalDriver() {} \
public: \
	typedef UMrcFocalDriver UClassType; \
	typedef IMrcFocalDriver ThisClass; \
	static float Execute_GetHorizontalFieldOfView(const UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_11_PROLOG \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_EVENT_PARMS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_CALLBACK_WRAPPERS \
	Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h_15_INCLASS_IINTERFACE_NO_PURE_DECLS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIXEDREALITYCAPTUREFRAMEWORK_API UClass* StaticClass<class UMrcFocalDriver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_MixedRealityCaptureFramework_Source_MixedRealityCaptureFramework_Public_IMrcFocalDriver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
