// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOCATIONSERVICESIOSEDITOR_LocationServicesIOSSettings_generated_h
#error "LocationServicesIOSSettings.generated.h already included, missing '#pragma once' in LocationServicesIOSSettings.h"
#endif
#define LOCATIONSERVICESIOSEDITOR_LocationServicesIOSSettings_generated_h

#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_SPARSE_DATA
#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_RPC_WRAPPERS
#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULocationServicesIOSSettings(); \
	friend struct Z_Construct_UClass_ULocationServicesIOSSettings_Statics; \
public: \
	DECLARE_CLASS(ULocationServicesIOSSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/LocationServicesIOSEditor"), NO_API) \
	DECLARE_SERIALIZER(ULocationServicesIOSSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_INCLASS \
private: \
	static void StaticRegisterNativesULocationServicesIOSSettings(); \
	friend struct Z_Construct_UClass_ULocationServicesIOSSettings_Statics; \
public: \
	DECLARE_CLASS(ULocationServicesIOSSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/LocationServicesIOSEditor"), NO_API) \
	DECLARE_SERIALIZER(ULocationServicesIOSSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULocationServicesIOSSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULocationServicesIOSSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULocationServicesIOSSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULocationServicesIOSSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULocationServicesIOSSettings(ULocationServicesIOSSettings&&); \
	NO_API ULocationServicesIOSSettings(const ULocationServicesIOSSettings&); \
public:


#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULocationServicesIOSSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULocationServicesIOSSettings(ULocationServicesIOSSettings&&); \
	NO_API ULocationServicesIOSSettings(const ULocationServicesIOSSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULocationServicesIOSSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULocationServicesIOSSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULocationServicesIOSSettings)


#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_8_PROLOG
#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_RPC_WRAPPERS \
	Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_INCLASS \
	Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h_12_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LocationServicesIOSSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LOCATIONSERVICESIOSEDITOR_API UClass* StaticClass<class ULocationServicesIOSSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_LocationServicesIOSImpl_Source_LocationServicesIOSEditor_Classes_LocationServicesIOSSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
