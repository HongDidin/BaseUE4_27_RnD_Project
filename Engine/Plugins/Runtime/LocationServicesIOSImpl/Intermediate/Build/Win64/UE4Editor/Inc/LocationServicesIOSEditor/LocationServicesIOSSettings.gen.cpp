// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LocationServicesIOSEditor/Classes/LocationServicesIOSSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLocationServicesIOSSettings() {}
// Cross Module References
	LOCATIONSERVICESIOSEDITOR_API UClass* Z_Construct_UClass_ULocationServicesIOSSettings_NoRegister();
	LOCATIONSERVICESIOSEDITOR_API UClass* Z_Construct_UClass_ULocationServicesIOSSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LocationServicesIOSEditor();
// End Cross Module References
	void ULocationServicesIOSSettings::StaticRegisterNativesULocationServicesIOSSettings()
	{
	}
	UClass* Z_Construct_UClass_ULocationServicesIOSSettings_NoRegister()
	{
		return ULocationServicesIOSSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULocationServicesIOSSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationAlwaysUsageDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LocationAlwaysUsageDescription;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationWhenInUseDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LocationWhenInUseDescription;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULocationServicesIOSSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LocationServicesIOSEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULocationServicesIOSSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LocationServicesIOSSettings.h" },
		{ "ModuleRelativePath", "Classes/LocationServicesIOSSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULocationServicesIOSSettings_Statics::NewProp_LocationAlwaysUsageDescription_MetaData[] = {
		{ "Category", "LocationServices" },
		{ "Comment", "/* Text to display when requesting permission for Location Services */" },
		{ "DisplayName", "Location Services Always Use Permission Text" },
		{ "ModuleRelativePath", "Classes/LocationServicesIOSSettings.h" },
		{ "ToolTip", "Text to display when requesting permission for Location Services" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULocationServicesIOSSettings_Statics::NewProp_LocationAlwaysUsageDescription = { "LocationAlwaysUsageDescription", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULocationServicesIOSSettings, LocationAlwaysUsageDescription), METADATA_PARAMS(Z_Construct_UClass_ULocationServicesIOSSettings_Statics::NewProp_LocationAlwaysUsageDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULocationServicesIOSSettings_Statics::NewProp_LocationAlwaysUsageDescription_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULocationServicesIOSSettings_Statics::NewProp_LocationWhenInUseDescription_MetaData[] = {
		{ "Category", "LocationServices" },
		{ "Comment", "/* Text to display when requesting permission for Location Services */" },
		{ "DisplayName", "Location Services In-Use Permission Text" },
		{ "ModuleRelativePath", "Classes/LocationServicesIOSSettings.h" },
		{ "ToolTip", "Text to display when requesting permission for Location Services" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULocationServicesIOSSettings_Statics::NewProp_LocationWhenInUseDescription = { "LocationWhenInUseDescription", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULocationServicesIOSSettings, LocationWhenInUseDescription), METADATA_PARAMS(Z_Construct_UClass_ULocationServicesIOSSettings_Statics::NewProp_LocationWhenInUseDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULocationServicesIOSSettings_Statics::NewProp_LocationWhenInUseDescription_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULocationServicesIOSSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULocationServicesIOSSettings_Statics::NewProp_LocationAlwaysUsageDescription,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULocationServicesIOSSettings_Statics::NewProp_LocationWhenInUseDescription,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULocationServicesIOSSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULocationServicesIOSSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULocationServicesIOSSettings_Statics::ClassParams = {
		&ULocationServicesIOSSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULocationServicesIOSSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULocationServicesIOSSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_ULocationServicesIOSSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULocationServicesIOSSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULocationServicesIOSSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULocationServicesIOSSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULocationServicesIOSSettings, 376140164);
	template<> LOCATIONSERVICESIOSEDITOR_API UClass* StaticClass<ULocationServicesIOSSettings>()
	{
		return ULocationServicesIOSSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULocationServicesIOSSettings(Z_Construct_UClass_ULocationServicesIOSSettings, &ULocationServicesIOSSettings::StaticClass, TEXT("/Script/LocationServicesIOSEditor"), TEXT("ULocationServicesIOSSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULocationServicesIOSSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
