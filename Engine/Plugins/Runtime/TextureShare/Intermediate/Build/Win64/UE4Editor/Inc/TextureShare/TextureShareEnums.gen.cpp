// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TextureShare/Public/Blueprints/TextureShareEnums.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTextureShareEnums() {}
// Cross Module References
	TEXTURESHARE_API UEnum* Z_Construct_UEnum_TextureShare_ETextureShareBPSyncSurface();
	UPackage* Z_Construct_UPackage__Script_TextureShare();
	TEXTURESHARE_API UEnum* Z_Construct_UEnum_TextureShare_ETextureShareBPSyncFrame();
	TEXTURESHARE_API UEnum* Z_Construct_UEnum_TextureShare_ETextureShareBPSyncConnect();
// End Cross Module References
	static UEnum* ETextureShareBPSyncSurface_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TextureShare_ETextureShareBPSyncSurface, Z_Construct_UPackage__Script_TextureShare(), TEXT("ETextureShareBPSyncSurface"));
		}
		return Singleton;
	}
	template<> TEXTURESHARE_API UEnum* StaticEnum<ETextureShareBPSyncSurface>()
	{
		return ETextureShareBPSyncSurface_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETextureShareBPSyncSurface(ETextureShareBPSyncSurface_StaticEnum, TEXT("/Script/TextureShare"), TEXT("ETextureShareBPSyncSurface"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TextureShare_ETextureShareBPSyncSurface_Hash() { return 3178373341U; }
	UEnum* Z_Construct_UEnum_TextureShare_ETextureShareBPSyncSurface()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TextureShare();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETextureShareBPSyncSurface"), 0, Get_Z_Construct_UEnum_TextureShare_ETextureShareBPSyncSurface_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETextureShareBPSyncSurface::Default", (int64)ETextureShareBPSyncSurface::Default },
				{ "ETextureShareBPSyncSurface::None", (int64)ETextureShareBPSyncSurface::None },
				{ "ETextureShareBPSyncSurface::SyncRead", (int64)ETextureShareBPSyncSurface::SyncRead },
				{ "ETextureShareBPSyncSurface::SyncPairingRead", (int64)ETextureShareBPSyncSurface::SyncPairingRead },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "TextureShare" },
				{ "Comment", "/** Synchronize texture events (LockTexture/UnlockTexture) */" },
				{ "Default.Comment", "/** [Default] - Use module global settings */" },
				{ "Default.Name", "ETextureShareBPSyncSurface::Default" },
				{ "Default.ToolTip", "[Default] - Use module global settings" },
				{ "ModuleRelativePath", "Public/Blueprints/TextureShareEnums.h" },
				{ "None.Comment", "/** [None] - Skip unpaired texture. Unordered read\\write operations */" },
				{ "None.Name", "ETextureShareBPSyncSurface::None" },
				{ "None.ToolTip", "[None] - Skip unpaired texture. Unordered read\\write operations" },
				{ "SyncPairingRead.Comment", "/** [SyncPairingRead] - Required texture pairing. Waiting until other process changed texture (readOP is wait for writeOP from remote process completed) */" },
				{ "SyncPairingRead.Name", "ETextureShareBPSyncSurface::SyncPairingRead" },
				{ "SyncPairingRead.ToolTip", "[SyncPairingRead] - Required texture pairing. Waiting until other process changed texture (readOP is wait for writeOP from remote process completed)" },
				{ "SyncRead.Comment", "/** [SyncRead] - Skip unpaired texture. Waiting until other process changed texture (readOP is wait for writeOP from remote process completed) */" },
				{ "SyncRead.Name", "ETextureShareBPSyncSurface::SyncRead" },
				{ "SyncRead.ToolTip", "[SyncRead] - Skip unpaired texture. Waiting until other process changed texture (readOP is wait for writeOP from remote process completed)" },
				{ "ToolTip", "Synchronize texture events (LockTexture/UnlockTexture)" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TextureShare,
				nullptr,
				"ETextureShareBPSyncSurface",
				"ETextureShareBPSyncSurface",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETextureShareBPSyncFrame_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TextureShare_ETextureShareBPSyncFrame, Z_Construct_UPackage__Script_TextureShare(), TEXT("ETextureShareBPSyncFrame"));
		}
		return Singleton;
	}
	template<> TEXTURESHARE_API UEnum* StaticEnum<ETextureShareBPSyncFrame>()
	{
		return ETextureShareBPSyncFrame_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETextureShareBPSyncFrame(ETextureShareBPSyncFrame_StaticEnum, TEXT("/Script/TextureShare"), TEXT("ETextureShareBPSyncFrame"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TextureShare_ETextureShareBPSyncFrame_Hash() { return 792650421U; }
	UEnum* Z_Construct_UEnum_TextureShare_ETextureShareBPSyncFrame()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TextureShare();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETextureShareBPSyncFrame"), 0, Get_Z_Construct_UEnum_TextureShare_ETextureShareBPSyncFrame_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETextureShareBPSyncFrame::Default", (int64)ETextureShareBPSyncFrame::Default },
				{ "ETextureShareBPSyncFrame::None", (int64)ETextureShareBPSyncFrame::None },
				{ "ETextureShareBPSyncFrame::FrameSync", (int64)ETextureShareBPSyncFrame::FrameSync },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "TextureShare" },
				{ "Comment", "/** Synchronize frame events (BeginFrame/EndFrame) */" },
				{ "Default.Comment", "/** [Default] - Use module global settings */" },
				{ "Default.Name", "ETextureShareBPSyncFrame::Default" },
				{ "Default.ToolTip", "[Default] - Use module global settings" },
				{ "FrameSync.Comment", "/** [FrameSync] - waiting until remote process frame index is equal */" },
				{ "FrameSync.Name", "ETextureShareBPSyncFrame::FrameSync" },
				{ "FrameSync.ToolTip", "[FrameSync] - waiting until remote process frame index is equal" },
				{ "ModuleRelativePath", "Public/Blueprints/TextureShareEnums.h" },
				{ "None.Comment", "/** [None] - Unordered frames */" },
				{ "None.Name", "ETextureShareBPSyncFrame::None" },
				{ "None.ToolTip", "[None] - Unordered frames" },
				{ "ToolTip", "Synchronize frame events (BeginFrame/EndFrame)" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TextureShare,
				nullptr,
				"ETextureShareBPSyncFrame",
				"ETextureShareBPSyncFrame",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETextureShareBPSyncConnect_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TextureShare_ETextureShareBPSyncConnect, Z_Construct_UPackage__Script_TextureShare(), TEXT("ETextureShareBPSyncConnect"));
		}
		return Singleton;
	}
	template<> TEXTURESHARE_API UEnum* StaticEnum<ETextureShareBPSyncConnect>()
	{
		return ETextureShareBPSyncConnect_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETextureShareBPSyncConnect(ETextureShareBPSyncConnect_StaticEnum, TEXT("/Script/TextureShare"), TEXT("ETextureShareBPSyncConnect"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TextureShare_ETextureShareBPSyncConnect_Hash() { return 3864068379U; }
	UEnum* Z_Construct_UEnum_TextureShare_ETextureShareBPSyncConnect()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TextureShare();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETextureShareBPSyncConnect"), 0, Get_Z_Construct_UEnum_TextureShare_ETextureShareBPSyncConnect_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETextureShareBPSyncConnect::Default", (int64)ETextureShareBPSyncConnect::Default },
				{ "ETextureShareBPSyncConnect::None", (int64)ETextureShareBPSyncConnect::None },
				{ "ETextureShareBPSyncConnect::SyncSession", (int64)ETextureShareBPSyncConnect::SyncSession },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "TextureShare" },
				{ "Comment", "/** Synchronize Session state events (BeginSession/EndSession) */" },
				{ "Default.Comment", "/** [Default] - Use module global settings */" },
				{ "Default.Name", "ETextureShareBPSyncConnect::Default" },
				{ "Default.ToolTip", "[Default] - Use module global settings" },
				{ "ModuleRelativePath", "Public/Blueprints/TextureShareEnums.h" },
				{ "None.Comment", "/** [None] - do not wait for remote process */" },
				{ "None.Name", "ETextureShareBPSyncConnect::None" },
				{ "None.ToolTip", "[None] - do not wait for remote process" },
				{ "SyncSession.Comment", "/** [SyncSession] - waiting until remote process not inside BeginSession()/EndSession() */" },
				{ "SyncSession.Name", "ETextureShareBPSyncConnect::SyncSession" },
				{ "SyncSession.ToolTip", "[SyncSession] - waiting until remote process not inside BeginSession()/EndSession()" },
				{ "ToolTip", "Synchronize Session state events (BeginSession/EndSession)" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TextureShare,
				nullptr,
				"ETextureShareBPSyncConnect",
				"ETextureShareBPSyncConnect",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
