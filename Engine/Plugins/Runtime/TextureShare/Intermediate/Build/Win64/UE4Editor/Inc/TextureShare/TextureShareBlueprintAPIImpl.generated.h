// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FTextureShareBPSyncPolicySettings;
struct FTextureShareBPPostprocess;
struct FTextureShareBPSyncPolicy;
#ifdef TEXTURESHARE_TextureShareBlueprintAPIImpl_generated_h
#error "TextureShareBlueprintAPIImpl.generated.h already included, missing '#pragma once' in TextureShareBlueprintAPIImpl.h"
#endif
#define TEXTURESHARE_TextureShareBlueprintAPIImpl_generated_h

#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_SPARSE_DATA
#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetSyncPolicySettings); \
	DECLARE_FUNCTION(execGetSyncPolicySettings); \
	DECLARE_FUNCTION(execApplyTextureSharePostprocess); \
	DECLARE_FUNCTION(execLinkSceneContextToShare); \
	DECLARE_FUNCTION(execReleaseTextureShare); \
	DECLARE_FUNCTION(execCreateTextureShare);


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetSyncPolicySettings); \
	DECLARE_FUNCTION(execGetSyncPolicySettings); \
	DECLARE_FUNCTION(execApplyTextureSharePostprocess); \
	DECLARE_FUNCTION(execLinkSceneContextToShare); \
	DECLARE_FUNCTION(execReleaseTextureShare); \
	DECLARE_FUNCTION(execCreateTextureShare);


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTextureShareAPIImpl(); \
	friend struct Z_Construct_UClass_UTextureShareAPIImpl_Statics; \
public: \
	DECLARE_CLASS(UTextureShareAPIImpl, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TextureShare"), NO_API) \
	DECLARE_SERIALIZER(UTextureShareAPIImpl) \
	virtual UObject* _getUObject() const override { return const_cast<UTextureShareAPIImpl*>(this); }


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUTextureShareAPIImpl(); \
	friend struct Z_Construct_UClass_UTextureShareAPIImpl_Statics; \
public: \
	DECLARE_CLASS(UTextureShareAPIImpl, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TextureShare"), NO_API) \
	DECLARE_SERIALIZER(UTextureShareAPIImpl) \
	virtual UObject* _getUObject() const override { return const_cast<UTextureShareAPIImpl*>(this); }


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextureShareAPIImpl(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextureShareAPIImpl) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextureShareAPIImpl); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextureShareAPIImpl); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextureShareAPIImpl(UTextureShareAPIImpl&&); \
	NO_API UTextureShareAPIImpl(const UTextureShareAPIImpl&); \
public:


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextureShareAPIImpl(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextureShareAPIImpl(UTextureShareAPIImpl&&); \
	NO_API UTextureShareAPIImpl(const UTextureShareAPIImpl&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextureShareAPIImpl); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextureShareAPIImpl); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextureShareAPIImpl)


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_14_PROLOG
#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_SPARSE_DATA \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_RPC_WRAPPERS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_INCLASS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_SPARSE_DATA \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TEXTURESHARE_API UClass* StaticClass<class UTextureShareAPIImpl>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Private_Blueprints_TextureShareBlueprintAPIImpl_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
