// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TextureShare/Public/Blueprints/ITextureShareBlueprintAPI.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeITextureShareBlueprintAPI() {}
// Cross Module References
	TEXTURESHARE_API UClass* Z_Construct_UClass_UTextureShareBlueprintAPI_NoRegister();
	TEXTURESHARE_API UClass* Z_Construct_UClass_UTextureShareBlueprintAPI();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_TextureShare();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPPostprocess();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings();
// End Cross Module References
	DEFINE_FUNCTION(ITextureShareBlueprintAPI::execSetSyncPolicySettings)
	{
		P_GET_STRUCT_REF(FTextureShareBPSyncPolicySettings,Z_Param_Out_InSyncPolicySettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSyncPolicySettings(Z_Param_Out_InSyncPolicySettings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ITextureShareBlueprintAPI::execGetSyncPolicySettings)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTextureShareBPSyncPolicySettings*)Z_Param__Result=P_THIS->GetSyncPolicySettings();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ITextureShareBlueprintAPI::execApplyTextureSharePostprocess)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ShareName);
		P_GET_STRUCT_REF(FTextureShareBPPostprocess,Z_Param_Out_Postprocess);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ApplyTextureSharePostprocess(Z_Param_ShareName,Z_Param_Out_Postprocess);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ITextureShareBlueprintAPI::execLinkSceneContextToShare)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ShareName);
		P_GET_PROPERTY(FIntProperty,Z_Param_StereoscopicPass);
		P_GET_UBOOL(Z_Param_bIsEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->LinkSceneContextToShare(Z_Param_ShareName,Z_Param_StereoscopicPass,Z_Param_bIsEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ITextureShareBlueprintAPI::execReleaseTextureShare)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ShareName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ReleaseTextureShare(Z_Param_ShareName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ITextureShareBlueprintAPI::execCreateTextureShare)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ShareName);
		P_GET_STRUCT(FTextureShareBPSyncPolicy,Z_Param_SyncMode);
		P_GET_UBOOL(Z_Param_bIsServer);
		P_GET_PROPERTY(FFloatProperty,Z_Param_SyncWaitTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CreateTextureShare(Z_Param_ShareName,Z_Param_SyncMode,Z_Param_bIsServer,Z_Param_SyncWaitTime);
		P_NATIVE_END;
	}
	void UTextureShareBlueprintAPI::StaticRegisterNativesUTextureShareBlueprintAPI()
	{
		UClass* Class = UTextureShareBlueprintAPI::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ApplyTextureSharePostprocess", &ITextureShareBlueprintAPI::execApplyTextureSharePostprocess },
			{ "CreateTextureShare", &ITextureShareBlueprintAPI::execCreateTextureShare },
			{ "GetSyncPolicySettings", &ITextureShareBlueprintAPI::execGetSyncPolicySettings },
			{ "LinkSceneContextToShare", &ITextureShareBlueprintAPI::execLinkSceneContextToShare },
			{ "ReleaseTextureShare", &ITextureShareBlueprintAPI::execReleaseTextureShare },
			{ "SetSyncPolicySettings", &ITextureShareBlueprintAPI::execSetSyncPolicySettings },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics
	{
		struct TextureShareBlueprintAPI_eventApplyTextureSharePostprocess_Parms
		{
			FString ShareName;
			FTextureShareBPPostprocess Postprocess;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShareName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShareName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Postprocess_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Postprocess;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_ShareName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_ShareName = { "ShareName", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareBlueprintAPI_eventApplyTextureSharePostprocess_Parms, ShareName), METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_ShareName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_ShareName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_Postprocess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_Postprocess = { "Postprocess", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareBlueprintAPI_eventApplyTextureSharePostprocess_Parms, Postprocess), Z_Construct_UScriptStruct_FTextureShareBPPostprocess, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_Postprocess_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_Postprocess_MetaData)) };
	void Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TextureShareBlueprintAPI_eventApplyTextureSharePostprocess_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareBlueprintAPI_eventApplyTextureSharePostprocess_Parms), &Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_ShareName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_Postprocess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "/**\n\x09 * Send from Input[], wait and receive result to Output[] from the remote process\n\x09 *\n\x09 * @param ShareName - Unique share name (case insensitive)\n\x09 * @param Postprocess - Textures to exchange\n\x09 *\n\x09 * @return True if the success\n\x09 */" },
		{ "DisplayName", "Apply TextureShare Postprocess" },
		{ "ModuleRelativePath", "Public/Blueprints/ITextureShareBlueprintAPI.h" },
		{ "ToolTip", "Send from Input[], wait and receive result to Output[] from the remote process\n\n@param ShareName - Unique share name (case insensitive)\n@param Postprocess - Textures to exchange\n\n@return True if the success" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareBlueprintAPI, nullptr, "ApplyTextureSharePostprocess", nullptr, nullptr, sizeof(TextureShareBlueprintAPI_eventApplyTextureSharePostprocess_Parms), Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics
	{
		struct TextureShareBlueprintAPI_eventCreateTextureShare_Parms
		{
			FString ShareName;
			FTextureShareBPSyncPolicy SyncMode;
			bool bIsServer;
			float SyncWaitTime;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShareName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShareName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SyncMode;
		static void NewProp_bIsServer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsServer;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SyncWaitTime;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_ShareName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_ShareName = { "ShareName", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareBlueprintAPI_eventCreateTextureShare_Parms, ShareName), METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_ShareName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_ShareName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_SyncMode = { "SyncMode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareBlueprintAPI_eventCreateTextureShare_Parms, SyncMode), Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_bIsServer_SetBit(void* Obj)
	{
		((TextureShareBlueprintAPI_eventCreateTextureShare_Parms*)Obj)->bIsServer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_bIsServer = { "bIsServer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareBlueprintAPI_eventCreateTextureShare_Parms), &Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_bIsServer_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_SyncWaitTime = { "SyncWaitTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareBlueprintAPI_eventCreateTextureShare_Parms, SyncWaitTime), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TextureShareBlueprintAPI_eventCreateTextureShare_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareBlueprintAPI_eventCreateTextureShare_Parms), &Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_ShareName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_SyncMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_bIsServer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_SyncWaitTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "/**\n\x09 * Create new textureshare object\n\x09 *\n\x09 * @param ShareName - Unique share name (case insensitive)\n\x09 * @param SyncMode  - Sync options\n\x09 * @param bIsSlave  - process type for pairing (master or slave)\n\x09 *\n\x09 * @return True if the success\n\x09 */" },
		{ "CPP_Default_bIsServer", "true" },
		{ "CPP_Default_SyncWaitTime", "0.030000" },
		{ "DisplayName", "Create TextureShare" },
		{ "ModuleRelativePath", "Public/Blueprints/ITextureShareBlueprintAPI.h" },
		{ "ToolTip", "Create new textureshare object\n\n@param ShareName - Unique share name (case insensitive)\n@param SyncMode  - Sync options\n@param bIsSlave  - process type for pairing (master or slave)\n\n@return True if the success" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareBlueprintAPI, nullptr, "CreateTextureShare", nullptr, nullptr, sizeof(TextureShareBlueprintAPI_eventCreateTextureShare_Parms), Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics
	{
		struct TextureShareBlueprintAPI_eventGetSyncPolicySettings_Parms
		{
			FTextureShareBPSyncPolicySettings ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareBlueprintAPI_eventGetSyncPolicySettings_Parms, ReturnValue), Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "/**\n\x09 * Get global synchronization settings\n\x09 * @return settings data struct\n\x09 */" },
		{ "DisplayName", "Get local process SyncPolicy Settings" },
		{ "ModuleRelativePath", "Public/Blueprints/ITextureShareBlueprintAPI.h" },
		{ "ToolTip", "Get global synchronization settings\n@return settings data struct" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareBlueprintAPI, nullptr, "GetSyncPolicySettings", nullptr, nullptr, sizeof(TextureShareBlueprintAPI_eventGetSyncPolicySettings_Parms), Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics
	{
		struct TextureShareBlueprintAPI_eventLinkSceneContextToShare_Parms
		{
			FString ShareName;
			int32 StereoscopicPass;
			bool bIsEnabled;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShareName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShareName;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_StereoscopicPass;
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_ShareName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_ShareName = { "ShareName", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareBlueprintAPI_eventLinkSceneContextToShare_Parms, ShareName), METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_ShareName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_ShareName_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_StereoscopicPass = { "StereoscopicPass", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareBlueprintAPI_eventLinkSceneContextToShare_Parms, StereoscopicPass), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((TextureShareBlueprintAPI_eventLinkSceneContextToShare_Parms*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareBlueprintAPI_eventLinkSceneContextToShare_Parms), &Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TextureShareBlueprintAPI_eventLinkSceneContextToShare_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareBlueprintAPI_eventLinkSceneContextToShare_Parms), &Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_ShareName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_StereoscopicPass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "/**\n\x09 * Link\\Unlink SceneContext capture (for specified StereoscopicPass) to exist textureshare object\n\x09 *\n\x09 * @param ShareName        - Unique share name (case insensitive)\n\x09 * @param StereoscopicPass - stereo rendering and nDisplay purpose\n\x09 * @param bIsEnabled       - true to create link\n\x09 *\n\x09 * @return True if the success\n\x09 */" },
		{ "CPP_Default_bIsEnabled", "true" },
		{ "CPP_Default_StereoscopicPass", "0" },
		{ "DisplayName", "Link SceneContext To TextureShare" },
		{ "ModuleRelativePath", "Public/Blueprints/ITextureShareBlueprintAPI.h" },
		{ "ToolTip", "Link\\Unlink SceneContext capture (for specified StereoscopicPass) to exist textureshare object\n\n@param ShareName        - Unique share name (case insensitive)\n@param StereoscopicPass - stereo rendering and nDisplay purpose\n@param bIsEnabled       - true to create link\n\n@return True if the success" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareBlueprintAPI, nullptr, "LinkSceneContextToShare", nullptr, nullptr, sizeof(TextureShareBlueprintAPI_eventLinkSceneContextToShare_Parms), Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics
	{
		struct TextureShareBlueprintAPI_eventReleaseTextureShare_Parms
		{
			FString ShareName;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShareName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShareName;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::NewProp_ShareName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::NewProp_ShareName = { "ShareName", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareBlueprintAPI_eventReleaseTextureShare_Parms, ShareName), METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::NewProp_ShareName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::NewProp_ShareName_MetaData)) };
	void Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TextureShareBlueprintAPI_eventReleaseTextureShare_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareBlueprintAPI_eventReleaseTextureShare_Parms), &Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::NewProp_ShareName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "/**\n\x09 * Release exist textureshare object\n\x09 *\n\x09 * @param ShareName - Unique share name (case insensitive)\n\x09 *\n\x09 * @return True if the success\n\x09 */" },
		{ "DisplayName", "Delete TextureShare" },
		{ "ModuleRelativePath", "Public/Blueprints/ITextureShareBlueprintAPI.h" },
		{ "ToolTip", "Release exist textureshare object\n\n@param ShareName - Unique share name (case insensitive)\n\n@return True if the success" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareBlueprintAPI, nullptr, "ReleaseTextureShare", nullptr, nullptr, sizeof(TextureShareBlueprintAPI_eventReleaseTextureShare_Parms), Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics
	{
		struct TextureShareBlueprintAPI_eventSetSyncPolicySettings_Parms
		{
			FTextureShareBPSyncPolicySettings InSyncPolicySettings;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSyncPolicySettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InSyncPolicySettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::NewProp_InSyncPolicySettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::NewProp_InSyncPolicySettings = { "InSyncPolicySettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareBlueprintAPI_eventSetSyncPolicySettings_Parms, InSyncPolicySettings), Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::NewProp_InSyncPolicySettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::NewProp_InSyncPolicySettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::NewProp_InSyncPolicySettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "/**\n\x09 * Get global synchronization settings\n\x09 *\n\x09 * @param Process              - Process logic type: server or client\n\x09 * @param InSyncPolicySettings - settings data struct\n\x09 */" },
		{ "DisplayName", "Set local process SyncPolicy Settings" },
		{ "ModuleRelativePath", "Public/Blueprints/ITextureShareBlueprintAPI.h" },
		{ "ToolTip", "Get global synchronization settings\n\n@param Process              - Process logic type: server or client\n@param InSyncPolicySettings - settings data struct" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareBlueprintAPI, nullptr, "SetSyncPolicySettings", nullptr, nullptr, sizeof(TextureShareBlueprintAPI_eventSetSyncPolicySettings_Parms), Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTextureShareBlueprintAPI_NoRegister()
	{
		return UTextureShareBlueprintAPI::StaticClass();
	}
	struct Z_Construct_UClass_UTextureShareBlueprintAPI_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTextureShareBlueprintAPI_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_TextureShare,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTextureShareBlueprintAPI_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTextureShareBlueprintAPI_ApplyTextureSharePostprocess, "ApplyTextureSharePostprocess" }, // 4004564527
		{ &Z_Construct_UFunction_UTextureShareBlueprintAPI_CreateTextureShare, "CreateTextureShare" }, // 1394502651
		{ &Z_Construct_UFunction_UTextureShareBlueprintAPI_GetSyncPolicySettings, "GetSyncPolicySettings" }, // 779448237
		{ &Z_Construct_UFunction_UTextureShareBlueprintAPI_LinkSceneContextToShare, "LinkSceneContextToShare" }, // 749650916
		{ &Z_Construct_UFunction_UTextureShareBlueprintAPI_ReleaseTextureShare, "ReleaseTextureShare" }, // 2503677616
		{ &Z_Construct_UFunction_UTextureShareBlueprintAPI_SetSyncPolicySettings, "SetSyncPolicySettings" }, // 1241464627
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTextureShareBlueprintAPI_Statics::Class_MetaDataParams[] = {
		{ "CannotImplementInterfaceInBlueprint", "" },
		{ "ModuleRelativePath", "Public/Blueprints/ITextureShareBlueprintAPI.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTextureShareBlueprintAPI_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ITextureShareBlueprintAPI>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTextureShareBlueprintAPI_Statics::ClassParams = {
		&UTextureShareBlueprintAPI::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UTextureShareBlueprintAPI_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTextureShareBlueprintAPI_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTextureShareBlueprintAPI()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTextureShareBlueprintAPI_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTextureShareBlueprintAPI, 4059375737);
	template<> TEXTURESHARE_API UClass* StaticClass<UTextureShareBlueprintAPI>()
	{
		return UTextureShareBlueprintAPI::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTextureShareBlueprintAPI(Z_Construct_UClass_UTextureShareBlueprintAPI, &UTextureShareBlueprintAPI::StaticClass, TEXT("/Script/TextureShare"), TEXT("UTextureShareBlueprintAPI"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTextureShareBlueprintAPI);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
