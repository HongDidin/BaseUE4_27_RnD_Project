// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FTextureShareBPSyncPolicySettings;
struct FTextureShareBPPostprocess;
struct FTextureShareBPSyncPolicy;
#ifdef TEXTURESHARE_ITextureShareBlueprintAPI_generated_h
#error "ITextureShareBlueprintAPI.generated.h already included, missing '#pragma once' in ITextureShareBlueprintAPI.h"
#endif
#define TEXTURESHARE_ITextureShareBlueprintAPI_generated_h

#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_SPARSE_DATA
#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetSyncPolicySettings); \
	DECLARE_FUNCTION(execGetSyncPolicySettings); \
	DECLARE_FUNCTION(execApplyTextureSharePostprocess); \
	DECLARE_FUNCTION(execLinkSceneContextToShare); \
	DECLARE_FUNCTION(execReleaseTextureShare); \
	DECLARE_FUNCTION(execCreateTextureShare);


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetSyncPolicySettings); \
	DECLARE_FUNCTION(execGetSyncPolicySettings); \
	DECLARE_FUNCTION(execApplyTextureSharePostprocess); \
	DECLARE_FUNCTION(execLinkSceneContextToShare); \
	DECLARE_FUNCTION(execReleaseTextureShare); \
	DECLARE_FUNCTION(execCreateTextureShare);


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextureShareBlueprintAPI(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextureShareBlueprintAPI) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextureShareBlueprintAPI); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextureShareBlueprintAPI); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextureShareBlueprintAPI(UTextureShareBlueprintAPI&&); \
	NO_API UTextureShareBlueprintAPI(const UTextureShareBlueprintAPI&); \
public:


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextureShareBlueprintAPI(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextureShareBlueprintAPI(UTextureShareBlueprintAPI&&); \
	NO_API UTextureShareBlueprintAPI(const UTextureShareBlueprintAPI&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextureShareBlueprintAPI); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextureShareBlueprintAPI); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextureShareBlueprintAPI)


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUTextureShareBlueprintAPI(); \
	friend struct Z_Construct_UClass_UTextureShareBlueprintAPI_Statics; \
public: \
	DECLARE_CLASS(UTextureShareBlueprintAPI, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/TextureShare"), NO_API) \
	DECLARE_SERIALIZER(UTextureShareBlueprintAPI)


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~ITextureShareBlueprintAPI() {} \
public: \
	typedef UTextureShareBlueprintAPI UClassType; \
	typedef ITextureShareBlueprintAPI ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_INCLASS_IINTERFACE \
protected: \
	virtual ~ITextureShareBlueprintAPI() {} \
public: \
	typedef UTextureShareBlueprintAPI UClassType; \
	typedef ITextureShareBlueprintAPI ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_15_PROLOG
#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_RPC_WRAPPERS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h_18_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TEXTURESHARE_API UClass* StaticClass<class UTextureShareBlueprintAPI>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_ITextureShareBlueprintAPI_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
