// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ITextureShareBlueprintAPI;
#ifdef TEXTURESHARE_TextureShareBlueprintLib_generated_h
#error "TextureShareBlueprintLib.generated.h already included, missing '#pragma once' in TextureShareBlueprintLib.h"
#endif
#define TEXTURESHARE_TextureShareBlueprintLib_generated_h

#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAPI);


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAPI);


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTextureShareIBlueprintLib(); \
	friend struct Z_Construct_UClass_UTextureShareIBlueprintLib_Statics; \
public: \
	DECLARE_CLASS(UTextureShareIBlueprintLib, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TextureShare"), NO_API) \
	DECLARE_SERIALIZER(UTextureShareIBlueprintLib)


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUTextureShareIBlueprintLib(); \
	friend struct Z_Construct_UClass_UTextureShareIBlueprintLib_Statics; \
public: \
	DECLARE_CLASS(UTextureShareIBlueprintLib, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TextureShare"), NO_API) \
	DECLARE_SERIALIZER(UTextureShareIBlueprintLib)


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextureShareIBlueprintLib(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextureShareIBlueprintLib) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextureShareIBlueprintLib); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextureShareIBlueprintLib); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextureShareIBlueprintLib(UTextureShareIBlueprintLib&&); \
	NO_API UTextureShareIBlueprintLib(const UTextureShareIBlueprintLib&); \
public:


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextureShareIBlueprintLib(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextureShareIBlueprintLib(UTextureShareIBlueprintLib&&); \
	NO_API UTextureShareIBlueprintLib(const UTextureShareIBlueprintLib&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextureShareIBlueprintLib); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextureShareIBlueprintLib); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextureShareIBlueprintLib)


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_13_PROLOG
#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_INCLASS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class TextureShareIBlueprintLib."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TEXTURESHARE_API UClass* StaticClass<class UTextureShareIBlueprintLib>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareBlueprintLib_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
