// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TextureShare/Private/Blueprints/TextureShareBlueprintAPIImpl.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTextureShareBlueprintAPIImpl() {}
// Cross Module References
	TEXTURESHARE_API UClass* Z_Construct_UClass_UTextureShareAPIImpl_NoRegister();
	TEXTURESHARE_API UClass* Z_Construct_UClass_UTextureShareAPIImpl();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_TextureShare();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPPostprocess();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings();
	TEXTURESHARE_API UClass* Z_Construct_UClass_UTextureShareBlueprintAPI_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UTextureShareAPIImpl::execSetSyncPolicySettings)
	{
		P_GET_STRUCT_REF(FTextureShareBPSyncPolicySettings,Z_Param_Out_InSyncPolicySettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSyncPolicySettings(Z_Param_Out_InSyncPolicySettings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTextureShareAPIImpl::execGetSyncPolicySettings)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTextureShareBPSyncPolicySettings*)Z_Param__Result=P_THIS->GetSyncPolicySettings();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTextureShareAPIImpl::execApplyTextureSharePostprocess)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ShareName);
		P_GET_STRUCT_REF(FTextureShareBPPostprocess,Z_Param_Out_Postprocess);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ApplyTextureSharePostprocess(Z_Param_ShareName,Z_Param_Out_Postprocess);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTextureShareAPIImpl::execLinkSceneContextToShare)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ShareName);
		P_GET_PROPERTY(FIntProperty,Z_Param_StereoscopicPass);
		P_GET_UBOOL(Z_Param_bIsEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->LinkSceneContextToShare(Z_Param_ShareName,Z_Param_StereoscopicPass,Z_Param_bIsEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTextureShareAPIImpl::execReleaseTextureShare)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ShareName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ReleaseTextureShare(Z_Param_ShareName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTextureShareAPIImpl::execCreateTextureShare)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ShareName);
		P_GET_STRUCT(FTextureShareBPSyncPolicy,Z_Param_SyncMode);
		P_GET_UBOOL(Z_Param_bIsServer);
		P_GET_PROPERTY(FFloatProperty,Z_Param_SyncWaitTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CreateTextureShare(Z_Param_ShareName,Z_Param_SyncMode,Z_Param_bIsServer,Z_Param_SyncWaitTime);
		P_NATIVE_END;
	}
	void UTextureShareAPIImpl::StaticRegisterNativesUTextureShareAPIImpl()
	{
		UClass* Class = UTextureShareAPIImpl::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ApplyTextureSharePostprocess", &UTextureShareAPIImpl::execApplyTextureSharePostprocess },
			{ "CreateTextureShare", &UTextureShareAPIImpl::execCreateTextureShare },
			{ "GetSyncPolicySettings", &UTextureShareAPIImpl::execGetSyncPolicySettings },
			{ "LinkSceneContextToShare", &UTextureShareAPIImpl::execLinkSceneContextToShare },
			{ "ReleaseTextureShare", &UTextureShareAPIImpl::execReleaseTextureShare },
			{ "SetSyncPolicySettings", &UTextureShareAPIImpl::execSetSyncPolicySettings },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics
	{
		struct TextureShareAPIImpl_eventApplyTextureSharePostprocess_Parms
		{
			FString ShareName;
			FTextureShareBPPostprocess Postprocess;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShareName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShareName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Postprocess_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Postprocess;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_ShareName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_ShareName = { "ShareName", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareAPIImpl_eventApplyTextureSharePostprocess_Parms, ShareName), METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_ShareName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_ShareName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_Postprocess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_Postprocess = { "Postprocess", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareAPIImpl_eventApplyTextureSharePostprocess_Parms, Postprocess), Z_Construct_UScriptStruct_FTextureShareBPPostprocess, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_Postprocess_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_Postprocess_MetaData)) };
	void Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TextureShareAPIImpl_eventApplyTextureSharePostprocess_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareAPIImpl_eventApplyTextureSharePostprocess_Parms), &Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_ShareName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_Postprocess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "DisplayName", "Apply TextureShare Postprocess" },
		{ "ModuleRelativePath", "Private/Blueprints/TextureShareBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareAPIImpl, nullptr, "ApplyTextureSharePostprocess", nullptr, nullptr, sizeof(TextureShareAPIImpl_eventApplyTextureSharePostprocess_Parms), Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics
	{
		struct TextureShareAPIImpl_eventCreateTextureShare_Parms
		{
			FString ShareName;
			FTextureShareBPSyncPolicy SyncMode;
			bool bIsServer;
			float SyncWaitTime;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShareName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShareName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SyncMode;
		static void NewProp_bIsServer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsServer;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SyncWaitTime;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_ShareName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_ShareName = { "ShareName", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareAPIImpl_eventCreateTextureShare_Parms, ShareName), METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_ShareName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_ShareName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_SyncMode = { "SyncMode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareAPIImpl_eventCreateTextureShare_Parms, SyncMode), Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_bIsServer_SetBit(void* Obj)
	{
		((TextureShareAPIImpl_eventCreateTextureShare_Parms*)Obj)->bIsServer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_bIsServer = { "bIsServer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareAPIImpl_eventCreateTextureShare_Parms), &Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_bIsServer_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_SyncWaitTime = { "SyncWaitTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareAPIImpl_eventCreateTextureShare_Parms, SyncWaitTime), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TextureShareAPIImpl_eventCreateTextureShare_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareAPIImpl_eventCreateTextureShare_Parms), &Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_ShareName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_SyncMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_bIsServer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_SyncWaitTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "CPP_Default_bIsServer", "true" },
		{ "CPP_Default_SyncWaitTime", "0.030000" },
		{ "DisplayName", "Create TextureShare" },
		{ "ModuleRelativePath", "Private/Blueprints/TextureShareBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareAPIImpl, nullptr, "CreateTextureShare", nullptr, nullptr, sizeof(TextureShareAPIImpl_eventCreateTextureShare_Parms), Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics
	{
		struct TextureShareAPIImpl_eventGetSyncPolicySettings_Parms
		{
			FTextureShareBPSyncPolicySettings ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareAPIImpl_eventGetSyncPolicySettings_Parms, ReturnValue), Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "DisplayName", "Get local process SyncPolicy Settings" },
		{ "ModuleRelativePath", "Private/Blueprints/TextureShareBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareAPIImpl, nullptr, "GetSyncPolicySettings", nullptr, nullptr, sizeof(TextureShareAPIImpl_eventGetSyncPolicySettings_Parms), Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics
	{
		struct TextureShareAPIImpl_eventLinkSceneContextToShare_Parms
		{
			FString ShareName;
			int32 StereoscopicPass;
			bool bIsEnabled;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShareName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShareName;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_StereoscopicPass;
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_ShareName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_ShareName = { "ShareName", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareAPIImpl_eventLinkSceneContextToShare_Parms, ShareName), METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_ShareName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_ShareName_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_StereoscopicPass = { "StereoscopicPass", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareAPIImpl_eventLinkSceneContextToShare_Parms, StereoscopicPass), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((TextureShareAPIImpl_eventLinkSceneContextToShare_Parms*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareAPIImpl_eventLinkSceneContextToShare_Parms), &Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TextureShareAPIImpl_eventLinkSceneContextToShare_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareAPIImpl_eventLinkSceneContextToShare_Parms), &Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_ShareName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_StereoscopicPass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "CPP_Default_bIsEnabled", "true" },
		{ "CPP_Default_StereoscopicPass", "0" },
		{ "DisplayName", "Link SceneContext To TextureShare" },
		{ "ModuleRelativePath", "Private/Blueprints/TextureShareBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareAPIImpl, nullptr, "LinkSceneContextToShare", nullptr, nullptr, sizeof(TextureShareAPIImpl_eventLinkSceneContextToShare_Parms), Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics
	{
		struct TextureShareAPIImpl_eventReleaseTextureShare_Parms
		{
			FString ShareName;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShareName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShareName;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::NewProp_ShareName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::NewProp_ShareName = { "ShareName", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareAPIImpl_eventReleaseTextureShare_Parms, ShareName), METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::NewProp_ShareName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::NewProp_ShareName_MetaData)) };
	void Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TextureShareAPIImpl_eventReleaseTextureShare_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextureShareAPIImpl_eventReleaseTextureShare_Parms), &Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::NewProp_ShareName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "DisplayName", "Delete TextureShare" },
		{ "ModuleRelativePath", "Private/Blueprints/TextureShareBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareAPIImpl, nullptr, "ReleaseTextureShare", nullptr, nullptr, sizeof(TextureShareAPIImpl_eventReleaseTextureShare_Parms), Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics
	{
		struct TextureShareAPIImpl_eventSetSyncPolicySettings_Parms
		{
			FTextureShareBPSyncPolicySettings InSyncPolicySettings;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSyncPolicySettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InSyncPolicySettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::NewProp_InSyncPolicySettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::NewProp_InSyncPolicySettings = { "InSyncPolicySettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareAPIImpl_eventSetSyncPolicySettings_Parms, InSyncPolicySettings), Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::NewProp_InSyncPolicySettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::NewProp_InSyncPolicySettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::NewProp_InSyncPolicySettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "DisplayName", "Set local process SyncPolicy Settings" },
		{ "ModuleRelativePath", "Private/Blueprints/TextureShareBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareAPIImpl, nullptr, "SetSyncPolicySettings", nullptr, nullptr, sizeof(TextureShareAPIImpl_eventSetSyncPolicySettings_Parms), Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTextureShareAPIImpl_NoRegister()
	{
		return UTextureShareAPIImpl::StaticClass();
	}
	struct Z_Construct_UClass_UTextureShareAPIImpl_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTextureShareAPIImpl_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TextureShare,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTextureShareAPIImpl_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTextureShareAPIImpl_ApplyTextureSharePostprocess, "ApplyTextureSharePostprocess" }, // 4228324835
		{ &Z_Construct_UFunction_UTextureShareAPIImpl_CreateTextureShare, "CreateTextureShare" }, // 4282504511
		{ &Z_Construct_UFunction_UTextureShareAPIImpl_GetSyncPolicySettings, "GetSyncPolicySettings" }, // 236334160
		{ &Z_Construct_UFunction_UTextureShareAPIImpl_LinkSceneContextToShare, "LinkSceneContextToShare" }, // 3212035014
		{ &Z_Construct_UFunction_UTextureShareAPIImpl_ReleaseTextureShare, "ReleaseTextureShare" }, // 1358407370
		{ &Z_Construct_UFunction_UTextureShareAPIImpl_SetSyncPolicySettings, "SetSyncPolicySettings" }, // 3412329144
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTextureShareAPIImpl_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Blueprint API interface implementation\n */" },
		{ "IncludePath", "Blueprints/TextureShareBlueprintAPIImpl.h" },
		{ "ModuleRelativePath", "Private/Blueprints/TextureShareBlueprintAPIImpl.h" },
		{ "ToolTip", "Blueprint API interface implementation" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UTextureShareAPIImpl_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UTextureShareBlueprintAPI_NoRegister, (int32)VTABLE_OFFSET(UTextureShareAPIImpl, ITextureShareBlueprintAPI), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTextureShareAPIImpl_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTextureShareAPIImpl>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTextureShareAPIImpl_Statics::ClassParams = {
		&UTextureShareAPIImpl::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTextureShareAPIImpl_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTextureShareAPIImpl_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTextureShareAPIImpl()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTextureShareAPIImpl_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTextureShareAPIImpl, 2032708643);
	template<> TEXTURESHARE_API UClass* StaticClass<UTextureShareAPIImpl>()
	{
		return UTextureShareAPIImpl::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTextureShareAPIImpl(Z_Construct_UClass_UTextureShareAPIImpl, &UTextureShareAPIImpl::StaticClass, TEXT("/Script/TextureShare"), TEXT("UTextureShareAPIImpl"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTextureShareAPIImpl);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
