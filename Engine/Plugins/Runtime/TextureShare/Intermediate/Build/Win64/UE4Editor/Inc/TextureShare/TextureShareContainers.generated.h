// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TEXTURESHARE_TextureShareContainers_generated_h
#error "TextureShareContainers.generated.h already included, missing '#pragma once' in TextureShareContainers.h"
#endif
#define TEXTURESHARE_TextureShareContainers_generated_h

#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareContainers_h_161_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics; \
	TEXTURESHARE_API static class UScriptStruct* StaticStruct();


template<> TEXTURESHARE_API UScriptStruct* StaticStruct<struct FTextureShareBPSyncPolicySettings>();

#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareContainers_h_128_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics; \
	TEXTURESHARE_API static class UScriptStruct* StaticStruct();


template<> TEXTURESHARE_API UScriptStruct* StaticStruct<struct FTextureShareBPTimeOut>();

#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareContainers_h_103_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics; \
	TEXTURESHARE_API static class UScriptStruct* StaticStruct();


template<> TEXTURESHARE_API UScriptStruct* StaticStruct<struct FTextureShareBPSyncPolicy>();

#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareContainers_h_85_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics; \
	TEXTURESHARE_API static class UScriptStruct* StaticStruct();


template<> TEXTURESHARE_API UScriptStruct* StaticStruct<struct FTextureShareBPPostprocess>();

#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareContainers_h_55_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics; \
	TEXTURESHARE_API static class UScriptStruct* StaticStruct();


template<> TEXTURESHARE_API UScriptStruct* StaticStruct<struct FTextureShareBPTexture2D>();

#define Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareContainers_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics; \
	TEXTURESHARE_API static class UScriptStruct* StaticStruct();


template<> TEXTURESHARE_API UScriptStruct* StaticStruct<struct FTextureShareBPAdditionalData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareContainers_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
