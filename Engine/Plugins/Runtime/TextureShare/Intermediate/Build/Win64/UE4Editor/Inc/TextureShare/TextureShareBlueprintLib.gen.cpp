// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TextureShare/Public/Blueprints/TextureShareBlueprintLib.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTextureShareBlueprintLib() {}
// Cross Module References
	TEXTURESHARE_API UClass* Z_Construct_UClass_UTextureShareIBlueprintLib_NoRegister();
	TEXTURESHARE_API UClass* Z_Construct_UClass_UTextureShareIBlueprintLib();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_TextureShare();
	TEXTURESHARE_API UClass* Z_Construct_UClass_UTextureShareBlueprintAPI_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UTextureShareIBlueprintLib::execGetAPI)
	{
		P_GET_TINTERFACE_REF(ITextureShareBlueprintAPI,Z_Param_Out_OutAPI);
		P_FINISH;
		P_NATIVE_BEGIN;
		UTextureShareIBlueprintLib::GetAPI(Z_Param_Out_OutAPI);
		P_NATIVE_END;
	}
	void UTextureShareIBlueprintLib::StaticRegisterNativesUTextureShareIBlueprintLib()
	{
		UClass* Class = UTextureShareIBlueprintLib::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetAPI", &UTextureShareIBlueprintLib::execGetAPI },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics
	{
		struct TextureShareIBlueprintLib_eventGetAPI_Parms
		{
			TScriptInterface<ITextureShareBlueprintAPI> OutAPI;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_OutAPI;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics::NewProp_OutAPI = { "OutAPI", nullptr, (EPropertyFlags)0x0014000000000180, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextureShareIBlueprintLib_eventGetAPI_Parms, OutAPI), Z_Construct_UClass_UTextureShareBlueprintAPI_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics::NewProp_OutAPI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics::Function_MetaDataParams[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "/** Return Display Cluster API interface. */" },
		{ "DisplayName", "TextureShare API" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareBlueprintLib.h" },
		{ "ToolTip", "Return Display Cluster API interface." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextureShareIBlueprintLib, nullptr, "GetAPI", nullptr, nullptr, sizeof(TextureShareIBlueprintLib_eventGetAPI_Parms), Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTextureShareIBlueprintLib_NoRegister()
	{
		return UTextureShareIBlueprintLib::StaticClass();
	}
	struct Z_Construct_UClass_UTextureShareIBlueprintLib_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTextureShareIBlueprintLib_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_TextureShare,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTextureShareIBlueprintLib_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTextureShareIBlueprintLib_GetAPI, "GetAPI" }, // 2729204023
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTextureShareIBlueprintLib_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Blueprint API function library\n */" },
		{ "IncludePath", "Blueprints/TextureShareBlueprintLib.h" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareBlueprintLib.h" },
		{ "ToolTip", "Blueprint API function library" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTextureShareIBlueprintLib_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTextureShareIBlueprintLib>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTextureShareIBlueprintLib_Statics::ClassParams = {
		&UTextureShareIBlueprintLib::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTextureShareIBlueprintLib_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTextureShareIBlueprintLib_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTextureShareIBlueprintLib()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTextureShareIBlueprintLib_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTextureShareIBlueprintLib, 222421181);
	template<> TEXTURESHARE_API UClass* StaticClass<UTextureShareIBlueprintLib>()
	{
		return UTextureShareIBlueprintLib::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTextureShareIBlueprintLib(Z_Construct_UClass_UTextureShareIBlueprintLib, &UTextureShareIBlueprintLib::StaticClass, TEXT("/Script/TextureShare"), TEXT("UTextureShareIBlueprintLib"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTextureShareIBlueprintLib);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
