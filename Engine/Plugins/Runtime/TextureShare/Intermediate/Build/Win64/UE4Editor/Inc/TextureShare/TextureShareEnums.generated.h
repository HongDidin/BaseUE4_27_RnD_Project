// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TEXTURESHARE_TextureShareEnums_generated_h
#error "TextureShareEnums.generated.h already included, missing '#pragma once' in TextureShareEnums.h"
#endif
#define TEXTURESHARE_TextureShareEnums_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_TextureShare_Source_TextureShare_Public_Blueprints_TextureShareEnums_h


#define FOREACH_ENUM_ETEXTURESHAREBPSYNCSURFACE(op) \
	op(ETextureShareBPSyncSurface::Default) \
	op(ETextureShareBPSyncSurface::None) \
	op(ETextureShareBPSyncSurface::SyncRead) \
	op(ETextureShareBPSyncSurface::SyncPairingRead) 

enum class ETextureShareBPSyncSurface : uint8;
template<> TEXTURESHARE_API UEnum* StaticEnum<ETextureShareBPSyncSurface>();

#define FOREACH_ENUM_ETEXTURESHAREBPSYNCFRAME(op) \
	op(ETextureShareBPSyncFrame::Default) \
	op(ETextureShareBPSyncFrame::None) \
	op(ETextureShareBPSyncFrame::FrameSync) 

enum class ETextureShareBPSyncFrame : uint8;
template<> TEXTURESHARE_API UEnum* StaticEnum<ETextureShareBPSyncFrame>();

#define FOREACH_ENUM_ETEXTURESHAREBPSYNCCONNECT(op) \
	op(ETextureShareBPSyncConnect::Default) \
	op(ETextureShareBPSyncConnect::None) \
	op(ETextureShareBPSyncConnect::SyncSession) 

enum class ETextureShareBPSyncConnect : uint8;
template<> TEXTURESHARE_API UEnum* StaticEnum<ETextureShareBPSyncConnect>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
