// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TextureShare/Public/Blueprints/TextureShareContainers.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTextureShareContainers() {}
// Cross Module References
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings();
	UPackage* Z_Construct_UPackage__Script_TextureShare();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPTimeOut();
	TEXTURESHARE_API UEnum* Z_Construct_UEnum_TextureShare_ETextureShareBPSyncConnect();
	TEXTURESHARE_API UEnum* Z_Construct_UEnum_TextureShare_ETextureShareBPSyncFrame();
	TEXTURESHARE_API UEnum* Z_Construct_UEnum_TextureShare_ETextureShareBPSyncSurface();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPPostprocess();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPAdditionalData();
	TEXTURESHARE_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPTexture2D();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FMatrix();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
// End Cross Module References
class UScriptStruct* FTextureShareBPSyncPolicySettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TEXTURESHARE_API uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings, Z_Construct_UPackage__Script_TextureShare(), TEXT("TextureShareBPSyncPolicySettings"), sizeof(FTextureShareBPSyncPolicySettings), Get_Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Hash());
	}
	return Singleton;
}
template<> TEXTURESHARE_API UScriptStruct* StaticStruct<FTextureShareBPSyncPolicySettings>()
{
	return FTextureShareBPSyncPolicySettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTextureShareBPSyncPolicySettings(FTextureShareBPSyncPolicySettings::StaticStruct, TEXT("/Script/TextureShare"), TEXT("TextureShareBPSyncPolicySettings"), false, nullptr, nullptr);
static struct FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPSyncPolicySettings
{
	FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPSyncPolicySettings()
	{
		UScriptStruct::DeferCppStructOps<FTextureShareBPSyncPolicySettings>(FName(TEXT("TextureShareBPSyncPolicySettings")));
	}
} ScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPSyncPolicySettings;
	struct Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSyncPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultSyncPolicy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeOut_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TimeOut;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTextureShareBPSyncPolicySettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewProp_DefaultSyncPolicy_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Default values for sync policy on local process\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Default values for sync policy on local process" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewProp_DefaultSyncPolicy = { "DefaultSyncPolicy", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPSyncPolicySettings, DefaultSyncPolicy), Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewProp_DefaultSyncPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewProp_DefaultSyncPolicy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewProp_TimeOut_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Timeout values (in seconds) for sync policy on local process\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Timeout values (in seconds) for sync policy on local process" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewProp_TimeOut = { "TimeOut", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPSyncPolicySettings, TimeOut), Z_Construct_UScriptStruct_FTextureShareBPTimeOut, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewProp_TimeOut_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewProp_TimeOut_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewProp_DefaultSyncPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::NewProp_TimeOut,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TextureShare,
		nullptr,
		&NewStructOps,
		"TextureShareBPSyncPolicySettings",
		sizeof(FTextureShareBPSyncPolicySettings),
		alignof(FTextureShareBPSyncPolicySettings),
		Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TextureShare();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TextureShareBPSyncPolicySettings"), sizeof(FTextureShareBPSyncPolicySettings), Get_Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPSyncPolicySettings_Hash() { return 3748105862U; }
class UScriptStruct* FTextureShareBPTimeOut::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TEXTURESHARE_API uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTextureShareBPTimeOut, Z_Construct_UPackage__Script_TextureShare(), TEXT("TextureShareBPTimeOut"), sizeof(FTextureShareBPTimeOut), Get_Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Hash());
	}
	return Singleton;
}
template<> TEXTURESHARE_API UScriptStruct* StaticStruct<FTextureShareBPTimeOut>()
{
	return FTextureShareBPTimeOut::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTextureShareBPTimeOut(FTextureShareBPTimeOut::StaticStruct, TEXT("/Script/TextureShare"), TEXT("TextureShareBPTimeOut"), false, nullptr, nullptr);
static struct FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPTimeOut
{
	FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPTimeOut()
	{
		UScriptStruct::DeferCppStructOps<FTextureShareBPTimeOut>(FName(TEXT("TextureShareBPTimeOut")));
	}
} ScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPTimeOut;
	struct Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionSync_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ConnectionSync;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameSync_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FrameSync;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TexturePairingSync_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TexturePairingSync;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureResourceSync_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TextureResourceSync;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureSync_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TextureSync;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTextureShareBPTimeOut>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_ConnectionSync_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Wait for processes shares connection (ETextureShareBPSyncConnect::Sync) [Seconds, zero for infinite]\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Wait for processes shares connection (ETextureShareBPSyncConnect::Sync) [Seconds, zero for infinite]" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_ConnectionSync = { "ConnectionSync", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPTimeOut, ConnectionSync), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_ConnectionSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_ConnectionSync_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_FrameSync_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Wait for frame index sync (ETextureShareBPSyncFrame::Sync) [Seconds, zero for infinite]\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Wait for frame index sync (ETextureShareBPSyncFrame::Sync) [Seconds, zero for infinite]" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_FrameSync = { "FrameSync", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPTimeOut, FrameSync), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_FrameSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_FrameSync_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TexturePairingSync_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Wait for remote process texture registering (ETextureShareBPSyncSurface::SyncPairingRead) [Seconds, zero for infinite]\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Wait for remote process texture registering (ETextureShareBPSyncSurface::SyncPairingRead) [Seconds, zero for infinite]" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TexturePairingSync = { "TexturePairingSync", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPTimeOut, TexturePairingSync), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TexturePairingSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TexturePairingSync_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TextureResourceSync_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Wait for remote resource(GPU) handle ready (ETextureShareBPSyncFrame::Sync) [Seconds, zero for infinite]\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Wait for remote resource(GPU) handle ready (ETextureShareBPSyncFrame::Sync) [Seconds, zero for infinite]" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TextureResourceSync = { "TextureResourceSync", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPTimeOut, TextureResourceSync), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TextureResourceSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TextureResourceSync_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TextureSync_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Wait before Read op texture until remote process finished texture write op [Seconds, zero for infinite]\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Wait before Read op texture until remote process finished texture write op [Seconds, zero for infinite]" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TextureSync = { "TextureSync", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPTimeOut, TextureSync), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TextureSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TextureSync_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_ConnectionSync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_FrameSync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TexturePairingSync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TextureResourceSync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::NewProp_TextureSync,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TextureShare,
		nullptr,
		&NewStructOps,
		"TextureShareBPTimeOut",
		sizeof(FTextureShareBPTimeOut),
		alignof(FTextureShareBPTimeOut),
		Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPTimeOut()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TextureShare();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TextureShareBPTimeOut"), sizeof(FTextureShareBPTimeOut), Get_Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPTimeOut_Hash() { return 1635876581U; }
class UScriptStruct* FTextureShareBPSyncPolicy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TEXTURESHARE_API uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy, Z_Construct_UPackage__Script_TextureShare(), TEXT("TextureShareBPSyncPolicy"), sizeof(FTextureShareBPSyncPolicy), Get_Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Hash());
	}
	return Singleton;
}
template<> TEXTURESHARE_API UScriptStruct* StaticStruct<FTextureShareBPSyncPolicy>()
{
	return FTextureShareBPSyncPolicy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTextureShareBPSyncPolicy(FTextureShareBPSyncPolicy::StaticStruct, TEXT("/Script/TextureShare"), TEXT("TextureShareBPSyncPolicy"), false, nullptr, nullptr);
static struct FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPSyncPolicy
{
	FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPSyncPolicy()
	{
		UScriptStruct::DeferCppStructOps<FTextureShareBPSyncPolicy>(FName(TEXT("TextureShareBPSyncPolicy")));
	}
} ScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPSyncPolicy;
	struct Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Connection_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Connection_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Connection;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Frame_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frame_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Frame;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Texture_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Texture_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Texture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTextureShareBPSyncPolicy>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Connection_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Connection_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Synchronize Session state events (BeginSession/EndSession)\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Synchronize Session state events (BeginSession/EndSession)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Connection = { "Connection", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPSyncPolicy, Connection), Z_Construct_UEnum_TextureShare_ETextureShareBPSyncConnect, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Connection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Connection_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Frame_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Frame_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Synchronize frame events (BeginFrame/EndFrame)\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Synchronize frame events (BeginFrame/EndFrame)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Frame = { "Frame", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPSyncPolicy, Frame), Z_Construct_UEnum_TextureShare_ETextureShareBPSyncFrame, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Frame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Frame_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Texture_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Texture_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Synchronize texture events (LockTexture/UnlockTexture)\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Synchronize texture events (LockTexture/UnlockTexture)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Texture = { "Texture", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPSyncPolicy, Texture), Z_Construct_UEnum_TextureShare_ETextureShareBPSyncSurface, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Texture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Texture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Connection_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Connection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Frame_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Frame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Texture_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::NewProp_Texture,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TextureShare,
		nullptr,
		&NewStructOps,
		"TextureShareBPSyncPolicy",
		sizeof(FTextureShareBPSyncPolicy),
		alignof(FTextureShareBPSyncPolicy),
		Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TextureShare();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TextureShareBPSyncPolicy"), sizeof(FTextureShareBPSyncPolicy), Get_Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPSyncPolicy_Hash() { return 2862916357U; }
class UScriptStruct* FTextureShareBPPostprocess::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TEXTURESHARE_API uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTextureShareBPPostprocess, Z_Construct_UPackage__Script_TextureShare(), TEXT("TextureShareBPPostprocess"), sizeof(FTextureShareBPPostprocess), Get_Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Hash());
	}
	return Singleton;
}
template<> TEXTURESHARE_API UScriptStruct* StaticStruct<FTextureShareBPPostprocess>()
{
	return FTextureShareBPPostprocess::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTextureShareBPPostprocess(FTextureShareBPPostprocess::StaticStruct, TEXT("/Script/TextureShare"), TEXT("TextureShareBPPostprocess"), false, nullptr, nullptr);
static struct FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPPostprocess
{
	FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPPostprocess()
	{
		UScriptStruct::DeferCppStructOps<FTextureShareBPPostprocess>(FName(TEXT("TextureShareBPPostprocess")));
	}
} ScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPPostprocess;
	struct Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionalData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdditionalData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Send_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Send_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Send;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Receive_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Receive_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Receive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTextureShareBPPostprocess>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_AdditionalData_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Send Additional data\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Send Additional data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_AdditionalData = { "AdditionalData", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPPostprocess, AdditionalData), Z_Construct_UScriptStruct_FTextureShareBPAdditionalData, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_AdditionalData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_AdditionalData_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Send_Inner = { "Send", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTextureShareBPTexture2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Send_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Send this textures to remote process\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Send this textures to remote process" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Send = { "Send", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPPostprocess, Send), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Send_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Send_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Receive_Inner = { "Receive", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTextureShareBPTexture2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Receive_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Receive this texture from remote process\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Receive this texture from remote process" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Receive = { "Receive", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPPostprocess, Receive), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Receive_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Receive_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_AdditionalData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Send_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Send,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Receive_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::NewProp_Receive,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TextureShare,
		nullptr,
		&NewStructOps,
		"TextureShareBPPostprocess",
		sizeof(FTextureShareBPPostprocess),
		alignof(FTextureShareBPPostprocess),
		Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPPostprocess()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TextureShare();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TextureShareBPPostprocess"), sizeof(FTextureShareBPPostprocess), Get_Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPPostprocess_Hash() { return 3184944380U; }
class UScriptStruct* FTextureShareBPTexture2D::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TEXTURESHARE_API uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTextureShareBPTexture2D, Z_Construct_UPackage__Script_TextureShare(), TEXT("TextureShareBPTexture2D"), sizeof(FTextureShareBPTexture2D), Get_Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Hash());
	}
	return Singleton;
}
template<> TEXTURESHARE_API UScriptStruct* StaticStruct<FTextureShareBPTexture2D>()
{
	return FTextureShareBPTexture2D::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTextureShareBPTexture2D(FTextureShareBPTexture2D::StaticStruct, TEXT("/Script/TextureShare"), TEXT("TextureShareBPTexture2D"), false, nullptr, nullptr);
static struct FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPTexture2D
{
	FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPTexture2D()
	{
		UScriptStruct::DeferCppStructOps<FTextureShareBPTexture2D>(FName(TEXT("TextureShareBPTexture2D")));
	}
} ScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPTexture2D;
	struct Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RTT_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RTT;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Texture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Texture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTextureShareBPTexture2D>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_Id_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Texture unique name\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Texture unique name" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPTexture2D, Id), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_Id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_RTT_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Texture source or target (optional)\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Texture source or target (optional)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_RTT = { "RTT", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPTexture2D, RTT), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_RTT_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_RTT_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_Texture_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Texture source (optional)\n" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
		{ "ToolTip", "Texture source (optional)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_Texture = { "Texture", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPTexture2D, Texture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_Texture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_Texture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_Id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_RTT,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::NewProp_Texture,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TextureShare,
		nullptr,
		&NewStructOps,
		"TextureShareBPTexture2D",
		sizeof(FTextureShareBPTexture2D),
		alignof(FTextureShareBPTexture2D),
		Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPTexture2D()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TextureShare();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TextureShareBPTexture2D"), sizeof(FTextureShareBPTexture2D), Get_Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPTexture2D_Hash() { return 3889511857U; }
class UScriptStruct* FTextureShareBPAdditionalData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TEXTURESHARE_API uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData, Z_Construct_UPackage__Script_TextureShare(), TEXT("TextureShareBPAdditionalData"), sizeof(FTextureShareBPAdditionalData), Get_Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Hash());
	}
	return Singleton;
}
template<> TEXTURESHARE_API UScriptStruct* StaticStruct<FTextureShareBPAdditionalData>()
{
	return FTextureShareBPAdditionalData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTextureShareBPAdditionalData(FTextureShareBPAdditionalData::StaticStruct, TEXT("/Script/TextureShare"), TEXT("TextureShareBPAdditionalData"), false, nullptr, nullptr);
static struct FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPAdditionalData
{
	FScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPAdditionalData()
	{
		UScriptStruct::DeferCppStructOps<FTextureShareBPAdditionalData>(FName(TEXT("TextureShareBPAdditionalData")));
	}
} ScriptStruct_TextureShare_StaticRegisterNativesFTextureShareBPAdditionalData;
	struct Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_FrameNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrjMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrjMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTextureShareBPAdditionalData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_FrameNumber_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_FrameNumber = { "FrameNumber", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPAdditionalData, FrameNumber), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_FrameNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_FrameNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_PrjMatrix_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_PrjMatrix = { "PrjMatrix", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPAdditionalData, PrjMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_PrjMatrix_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_PrjMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewLocation_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewLocation = { "ViewLocation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPAdditionalData, ViewLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewRotation_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewRotation = { "ViewRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPAdditionalData, ViewRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewScale_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "ModuleRelativePath", "Public/Blueprints/TextureShareContainers.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewScale = { "ViewScale", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareBPAdditionalData, ViewScale), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_FrameNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_PrjMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::NewProp_ViewScale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TextureShare,
		nullptr,
		&NewStructOps,
		"TextureShareBPAdditionalData",
		sizeof(FTextureShareBPAdditionalData),
		alignof(FTextureShareBPAdditionalData),
		Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTextureShareBPAdditionalData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TextureShare();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TextureShareBPAdditionalData"), sizeof(FTextureShareBPAdditionalData), Get_Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTextureShareBPAdditionalData_Hash() { return 101924272U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
