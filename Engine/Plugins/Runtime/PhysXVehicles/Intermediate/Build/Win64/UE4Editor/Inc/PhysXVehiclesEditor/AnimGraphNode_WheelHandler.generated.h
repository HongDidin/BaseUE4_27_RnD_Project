// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PHYSXVEHICLESEDITOR_AnimGraphNode_WheelHandler_generated_h
#error "AnimGraphNode_WheelHandler.generated.h already included, missing '#pragma once' in AnimGraphNode_WheelHandler.h"
#endif
#define PHYSXVEHICLESEDITOR_AnimGraphNode_WheelHandler_generated_h

#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_SPARSE_DATA
#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_RPC_WRAPPERS
#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnimGraphNode_WheelHandler(); \
	friend struct Z_Construct_UClass_UAnimGraphNode_WheelHandler_Statics; \
public: \
	DECLARE_CLASS(UAnimGraphNode_WheelHandler, UAnimGraphNode_SkeletalControlBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PhysXVehiclesEditor"), PHYSXVEHICLESEDITOR_API) \
	DECLARE_SERIALIZER(UAnimGraphNode_WheelHandler)


#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUAnimGraphNode_WheelHandler(); \
	friend struct Z_Construct_UClass_UAnimGraphNode_WheelHandler_Statics; \
public: \
	DECLARE_CLASS(UAnimGraphNode_WheelHandler, UAnimGraphNode_SkeletalControlBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PhysXVehiclesEditor"), PHYSXVEHICLESEDITOR_API) \
	DECLARE_SERIALIZER(UAnimGraphNode_WheelHandler)


#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PHYSXVEHICLESEDITOR_API UAnimGraphNode_WheelHandler(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimGraphNode_WheelHandler) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PHYSXVEHICLESEDITOR_API, UAnimGraphNode_WheelHandler); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimGraphNode_WheelHandler); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PHYSXVEHICLESEDITOR_API UAnimGraphNode_WheelHandler(UAnimGraphNode_WheelHandler&&); \
	PHYSXVEHICLESEDITOR_API UAnimGraphNode_WheelHandler(const UAnimGraphNode_WheelHandler&); \
public:


#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PHYSXVEHICLESEDITOR_API UAnimGraphNode_WheelHandler(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PHYSXVEHICLESEDITOR_API UAnimGraphNode_WheelHandler(UAnimGraphNode_WheelHandler&&); \
	PHYSXVEHICLESEDITOR_API UAnimGraphNode_WheelHandler(const UAnimGraphNode_WheelHandler&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PHYSXVEHICLESEDITOR_API, UAnimGraphNode_WheelHandler); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimGraphNode_WheelHandler); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimGraphNode_WheelHandler)


#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_15_PROLOG
#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_RPC_WRAPPERS \
	Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_INCLASS \
	Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AnimGraphNode_WheelHandler."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PHYSXVEHICLESEDITOR_API UClass* StaticClass<class UAnimGraphNode_WheelHandler>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_PhysXVehicles_Source_PhysXVehiclesEditor_Public_AnimGraphNode_WheelHandler_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
