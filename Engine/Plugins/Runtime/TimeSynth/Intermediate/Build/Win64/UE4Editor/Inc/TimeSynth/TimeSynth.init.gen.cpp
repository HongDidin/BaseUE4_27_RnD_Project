// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimeSynth_init() {}
	TIMESYNTH_API UFunction* Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature();
	TIMESYNTH_API UFunction* Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature();
	TIMESYNTH_API UFunction* Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TimeSynth()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/TimeSynth",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x82695130,
				0x5DB63FAA,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
