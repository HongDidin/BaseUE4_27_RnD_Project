// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimeSynthEditor/Classes/TimeSynthClip.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimeSynthClip() {}
// Cross Module References
	TIMESYNTHEDITOR_API UClass* Z_Construct_UClass_UTimeSynthClipFactory_NoRegister();
	TIMESYNTHEDITOR_API UClass* Z_Construct_UClass_UTimeSynthClipFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_TimeSynthEditor();
// End Cross Module References
	void UTimeSynthClipFactory::StaticRegisterNativesUTimeSynthClipFactory()
	{
	}
	UClass* Z_Construct_UClass_UTimeSynthClipFactory_NoRegister()
	{
		return UTimeSynthClipFactory::StaticClass();
	}
	struct Z_Construct_UClass_UTimeSynthClipFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTimeSynthClipFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynthEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthClipFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "TimeSynthClip.h" },
		{ "ModuleRelativePath", "Classes/TimeSynthClip.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTimeSynthClipFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTimeSynthClipFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTimeSynthClipFactory_Statics::ClassParams = {
		&UTimeSynthClipFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTimeSynthClipFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClipFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTimeSynthClipFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTimeSynthClipFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTimeSynthClipFactory, 1613536166);
	template<> TIMESYNTHEDITOR_API UClass* StaticClass<UTimeSynthClipFactory>()
	{
		return UTimeSynthClipFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTimeSynthClipFactory(Z_Construct_UClass_UTimeSynthClipFactory, &UTimeSynthClipFactory::StaticClass, TEXT("/Script/TimeSynthEditor"), TEXT("UTimeSynthClipFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTimeSynthClipFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
