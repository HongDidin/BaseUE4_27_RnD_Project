// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimeSynth/Classes/TimeSynthComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimeSynthComponent() {}
// Cross Module References
	TIMESYNTH_API UFunction* Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TimeSynth();
	TIMESYNTH_API UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthEventQuantization();
	TIMESYNTH_API UFunction* Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature();
	TIMESYNTH_API UFunction* Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature();
	TIMESYNTH_API UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthEnvelopeFollowerPeakMode();
	TIMESYNTH_API UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthFilterType();
	TIMESYNTH_API UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthFilter();
	TIMESYNTH_API UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthEventClipQuantization();
	TIMESYNTH_API UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthFFTSize();
	TIMESYNTH_API UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthBeatDivision();
	TIMESYNTH_API UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings();
	TIMESYNTH_API UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthFilterSettings();
	TIMESYNTH_API UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthClipSound();
	ENGINE_API UClass* Z_Construct_UClass_USoundWave_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	TIMESYNTH_API UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthClipHandle();
	TIMESYNTH_API UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthTimeDef();
	TIMESYNTH_API UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings();
	TIMESYNTH_API UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthSpectralData();
	TIMESYNTH_API UClass* Z_Construct_UClass_UTimeSynthVolumeGroup_NoRegister();
	TIMESYNTH_API UClass* Z_Construct_UClass_UTimeSynthVolumeGroup();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	TIMESYNTH_API UClass* Z_Construct_UClass_UTimeSynthClip_NoRegister();
	TIMESYNTH_API UClass* Z_Construct_UClass_UTimeSynthClip();
	TIMESYNTH_API UClass* Z_Construct_UClass_UTimeSynthComponent_NoRegister();
	TIMESYNTH_API UClass* Z_Construct_UClass_UTimeSynthComponent();
	AUDIOMIXER_API UClass* Z_Construct_UClass_USynthComponent();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics
	{
		struct _Script_TimeSynth_eventOnQuantizationEventBP_Parms
		{
			ETimeSynthEventQuantization QuantizationType;
			int32 NumBars;
			float Beat;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_QuantizationType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_QuantizationType;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumBars;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Beat;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::NewProp_QuantizationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::NewProp_QuantizationType = { "QuantizationType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TimeSynth_eventOnQuantizationEventBP_Parms, QuantizationType), Z_Construct_UEnum_TimeSynth_ETimeSynthEventQuantization, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::NewProp_NumBars = { "NumBars", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TimeSynth_eventOnQuantizationEventBP_Parms, NumBars), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::NewProp_Beat = { "Beat", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TimeSynth_eventOnQuantizationEventBP_Parms, Beat), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::NewProp_QuantizationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::NewProp_QuantizationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::NewProp_NumBars,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::NewProp_Beat,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_TimeSynth, nullptr, "OnQuantizationEventBP__DelegateSignature", nullptr, nullptr, sizeof(_Script_TimeSynth_eventOnQuantizationEventBP_Parms), Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics
	{
		struct _Script_TimeSynth_eventOnQuantizationEvent_Parms
		{
			ETimeSynthEventQuantization QuantizationType;
			int32 NumBars;
			float Beat;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_QuantizationType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_QuantizationType;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumBars;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Beat;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::NewProp_QuantizationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::NewProp_QuantizationType = { "QuantizationType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TimeSynth_eventOnQuantizationEvent_Parms, QuantizationType), Z_Construct_UEnum_TimeSynth_ETimeSynthEventQuantization, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::NewProp_NumBars = { "NumBars", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TimeSynth_eventOnQuantizationEvent_Parms, NumBars), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::NewProp_Beat = { "Beat", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TimeSynth_eventOnQuantizationEvent_Parms, Beat), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::NewProp_QuantizationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::NewProp_QuantizationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::NewProp_NumBars,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::NewProp_Beat,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Called on the given quantization type. Supplies quantization event type, the number of bars, and the beat fraction that the event happened in that bar.\n// Beat is a float between 0.0 and the quantization setting for BeatsPerBar. Fractional beats are sub-divisions of a beat.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Called on the given quantization type. Supplies quantization event type, the number of bars, and the beat fraction that the event happened in that bar.\nBeat is a float between 0.0 and the quantization setting for BeatsPerBar. Fractional beats are sub-divisions of a beat." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_TimeSynth, nullptr, "OnQuantizationEvent__DelegateSignature", nullptr, nullptr, sizeof(_Script_TimeSynth_eventOnQuantizationEvent_Parms), Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics
	{
		struct _Script_TimeSynth_eventOnTimeSynthPlaybackTime_Parms
		{
			float SynthPlaybackTimeSeconds;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SynthPlaybackTimeSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics::NewProp_SynthPlaybackTimeSeconds = { "SynthPlaybackTimeSeconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_TimeSynth_eventOnTimeSynthPlaybackTime_Parms, SynthPlaybackTimeSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics::NewProp_SynthPlaybackTimeSeconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Called to get playback time progress callbacks. Time is based off the synth time clock, not game thread time so time will be accurate relative to the synth (minus thread communication latency).\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Called to get playback time progress callbacks. Time is based off the synth time clock, not game thread time so time will be accurate relative to the synth (minus thread communication latency)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_TimeSynth, nullptr, "OnTimeSynthPlaybackTime__DelegateSignature", nullptr, nullptr, sizeof(_Script_TimeSynth_eventOnTimeSynthPlaybackTime_Parms), Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* ETimeSynthEnvelopeFollowerPeakMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimeSynth_ETimeSynthEnvelopeFollowerPeakMode, Z_Construct_UPackage__Script_TimeSynth(), TEXT("ETimeSynthEnvelopeFollowerPeakMode"));
		}
		return Singleton;
	}
	template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthEnvelopeFollowerPeakMode>()
	{
		return ETimeSynthEnvelopeFollowerPeakMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimeSynthEnvelopeFollowerPeakMode(ETimeSynthEnvelopeFollowerPeakMode_StaticEnum, TEXT("/Script/TimeSynth"), TEXT("ETimeSynthEnvelopeFollowerPeakMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimeSynth_ETimeSynthEnvelopeFollowerPeakMode_Hash() { return 1672746626U; }
	UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthEnvelopeFollowerPeakMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimeSynthEnvelopeFollowerPeakMode"), 0, Get_Z_Construct_UEnum_TimeSynth_ETimeSynthEnvelopeFollowerPeakMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimeSynthEnvelopeFollowerPeakMode::MeanSquared", (int64)ETimeSynthEnvelopeFollowerPeakMode::MeanSquared },
				{ "ETimeSynthEnvelopeFollowerPeakMode::RootMeanSquared", (int64)ETimeSynthEnvelopeFollowerPeakMode::RootMeanSquared },
				{ "ETimeSynthEnvelopeFollowerPeakMode::Peak", (int64)ETimeSynthEnvelopeFollowerPeakMode::Peak },
				{ "ETimeSynthEnvelopeFollowerPeakMode::Count", (int64)ETimeSynthEnvelopeFollowerPeakMode::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Count.Hidden", "" },
				{ "Count.Name", "ETimeSynthEnvelopeFollowerPeakMode::Count" },
				{ "MeanSquared.Name", "ETimeSynthEnvelopeFollowerPeakMode::MeanSquared" },
				{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
				{ "Peak.Name", "ETimeSynthEnvelopeFollowerPeakMode::Peak" },
				{ "RootMeanSquared.Name", "ETimeSynthEnvelopeFollowerPeakMode::RootMeanSquared" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimeSynth,
				nullptr,
				"ETimeSynthEnvelopeFollowerPeakMode",
				"ETimeSynthEnvelopeFollowerPeakMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETimeSynthFilterType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimeSynth_ETimeSynthFilterType, Z_Construct_UPackage__Script_TimeSynth(), TEXT("ETimeSynthFilterType"));
		}
		return Singleton;
	}
	template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthFilterType>()
	{
		return ETimeSynthFilterType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimeSynthFilterType(ETimeSynthFilterType_StaticEnum, TEXT("/Script/TimeSynth"), TEXT("ETimeSynthFilterType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimeSynth_ETimeSynthFilterType_Hash() { return 1126792873U; }
	UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthFilterType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimeSynthFilterType"), 0, Get_Z_Construct_UEnum_TimeSynth_ETimeSynthFilterType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimeSynthFilterType::LowPass", (int64)ETimeSynthFilterType::LowPass },
				{ "ETimeSynthFilterType::HighPass", (int64)ETimeSynthFilterType::HighPass },
				{ "ETimeSynthFilterType::BandPass", (int64)ETimeSynthFilterType::BandPass },
				{ "ETimeSynthFilterType::BandStop", (int64)ETimeSynthFilterType::BandStop },
				{ "ETimeSynthFilterType::Count", (int64)ETimeSynthFilterType::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BandPass.Name", "ETimeSynthFilterType::BandPass" },
				{ "BandStop.Name", "ETimeSynthFilterType::BandStop" },
				{ "BlueprintType", "true" },
				{ "Count.Hidden", "" },
				{ "Count.Name", "ETimeSynthFilterType::Count" },
				{ "HighPass.Name", "ETimeSynthFilterType::HighPass" },
				{ "LowPass.Name", "ETimeSynthFilterType::LowPass" },
				{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimeSynth,
				nullptr,
				"ETimeSynthFilterType",
				"ETimeSynthFilterType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETimeSynthFilter_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimeSynth_ETimeSynthFilter, Z_Construct_UPackage__Script_TimeSynth(), TEXT("ETimeSynthFilter"));
		}
		return Singleton;
	}
	template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthFilter>()
	{
		return ETimeSynthFilter_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimeSynthFilter(ETimeSynthFilter_StaticEnum, TEXT("/Script/TimeSynth"), TEXT("ETimeSynthFilter"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimeSynth_ETimeSynthFilter_Hash() { return 1983126748U; }
	UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthFilter()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimeSynthFilter"), 0, Get_Z_Construct_UEnum_TimeSynth_ETimeSynthFilter_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimeSynthFilter::FilterA", (int64)ETimeSynthFilter::FilterA },
				{ "ETimeSynthFilter::FilterB", (int64)ETimeSynthFilter::FilterB },
				{ "ETimeSynthFilter::Count", (int64)ETimeSynthFilter::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "// An enumeration specifying which filter to use\n" },
				{ "Count.Hidden", "" },
				{ "Count.Name", "ETimeSynthFilter::Count" },
				{ "FilterA.Name", "ETimeSynthFilter::FilterA" },
				{ "FilterB.Name", "ETimeSynthFilter::FilterB" },
				{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
				{ "ToolTip", "An enumeration specifying which filter to use" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimeSynth,
				nullptr,
				"ETimeSynthFilter",
				"ETimeSynthFilter",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETimeSynthEventQuantization_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimeSynth_ETimeSynthEventQuantization, Z_Construct_UPackage__Script_TimeSynth(), TEXT("ETimeSynthEventQuantization"));
		}
		return Singleton;
	}
	template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthEventQuantization>()
	{
		return ETimeSynthEventQuantization_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimeSynthEventQuantization(ETimeSynthEventQuantization_StaticEnum, TEXT("/Script/TimeSynth"), TEXT("ETimeSynthEventQuantization"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimeSynth_ETimeSynthEventQuantization_Hash() { return 480484897U; }
	UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthEventQuantization()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimeSynthEventQuantization"), 0, Get_Z_Construct_UEnum_TimeSynth_ETimeSynthEventQuantization_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimeSynthEventQuantization::None", (int64)ETimeSynthEventQuantization::None },
				{ "ETimeSynthEventQuantization::Bars8", (int64)ETimeSynthEventQuantization::Bars8 },
				{ "ETimeSynthEventQuantization::Bars4", (int64)ETimeSynthEventQuantization::Bars4 },
				{ "ETimeSynthEventQuantization::Bars2", (int64)ETimeSynthEventQuantization::Bars2 },
				{ "ETimeSynthEventQuantization::Bar", (int64)ETimeSynthEventQuantization::Bar },
				{ "ETimeSynthEventQuantization::HalfNote", (int64)ETimeSynthEventQuantization::HalfNote },
				{ "ETimeSynthEventQuantization::HalfNoteTriplet", (int64)ETimeSynthEventQuantization::HalfNoteTriplet },
				{ "ETimeSynthEventQuantization::QuarterNote", (int64)ETimeSynthEventQuantization::QuarterNote },
				{ "ETimeSynthEventQuantization::QuarterNoteTriplet", (int64)ETimeSynthEventQuantization::QuarterNoteTriplet },
				{ "ETimeSynthEventQuantization::EighthNote", (int64)ETimeSynthEventQuantization::EighthNote },
				{ "ETimeSynthEventQuantization::EighthNoteTriplet", (int64)ETimeSynthEventQuantization::EighthNoteTriplet },
				{ "ETimeSynthEventQuantization::SixteenthNote", (int64)ETimeSynthEventQuantization::SixteenthNote },
				{ "ETimeSynthEventQuantization::SixteenthNoteTriplet", (int64)ETimeSynthEventQuantization::SixteenthNoteTriplet },
				{ "ETimeSynthEventQuantization::ThirtySecondNote", (int64)ETimeSynthEventQuantization::ThirtySecondNote },
				{ "ETimeSynthEventQuantization::Count", (int64)ETimeSynthEventQuantization::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Bar.DisplayName", "1 Bar" },
				{ "Bar.Name", "ETimeSynthEventQuantization::Bar" },
				{ "Bars2.DisplayName", "2 Bars" },
				{ "Bars2.Name", "ETimeSynthEventQuantization::Bars2" },
				{ "Bars4.DisplayName", "4 Bars" },
				{ "Bars4.Name", "ETimeSynthEventQuantization::Bars4" },
				{ "Bars8.DisplayName", "8 Bars" },
				{ "Bars8.Name", "ETimeSynthEventQuantization::Bars8" },
				{ "BlueprintType", "true" },
				{ "Comment", "// An enumeration for specifying \"global\" quantization for all clips if clips choose global quantization enumeration.\n" },
				{ "Count.Hidden", "" },
				{ "Count.Name", "ETimeSynthEventQuantization::Count" },
				{ "EighthNote.DisplayName", "1/8" },
				{ "EighthNote.Name", "ETimeSynthEventQuantization::EighthNote" },
				{ "EighthNoteTriplet.DisplayName", "1/8 T" },
				{ "EighthNoteTriplet.Name", "ETimeSynthEventQuantization::EighthNoteTriplet" },
				{ "HalfNote.DisplayName", "1/2" },
				{ "HalfNote.Name", "ETimeSynthEventQuantization::HalfNote" },
				{ "HalfNoteTriplet.DisplayName", "1/2 T" },
				{ "HalfNoteTriplet.Name", "ETimeSynthEventQuantization::HalfNoteTriplet" },
				{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
				{ "None.DisplayName", "No Quantization" },
				{ "None.Name", "ETimeSynthEventQuantization::None" },
				{ "QuarterNote.DisplayName", "1/4" },
				{ "QuarterNote.Name", "ETimeSynthEventQuantization::QuarterNote" },
				{ "QuarterNoteTriplet.DisplayName", "1/4 T" },
				{ "QuarterNoteTriplet.Name", "ETimeSynthEventQuantization::QuarterNoteTriplet" },
				{ "SixteenthNote.DisplayName", "1/16" },
				{ "SixteenthNote.Name", "ETimeSynthEventQuantization::SixteenthNote" },
				{ "SixteenthNoteTriplet.DisplayName", "1/16 T" },
				{ "SixteenthNoteTriplet.Name", "ETimeSynthEventQuantization::SixteenthNoteTriplet" },
				{ "ThirtySecondNote.DisplayName", "1/32" },
				{ "ThirtySecondNote.Name", "ETimeSynthEventQuantization::ThirtySecondNote" },
				{ "ToolTip", "An enumeration for specifying \"global\" quantization for all clips if clips choose global quantization enumeration." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimeSynth,
				nullptr,
				"ETimeSynthEventQuantization",
				"ETimeSynthEventQuantization",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETimeSynthEventClipQuantization_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimeSynth_ETimeSynthEventClipQuantization, Z_Construct_UPackage__Script_TimeSynth(), TEXT("ETimeSynthEventClipQuantization"));
		}
		return Singleton;
	}
	template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthEventClipQuantization>()
	{
		return ETimeSynthEventClipQuantization_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimeSynthEventClipQuantization(ETimeSynthEventClipQuantization_StaticEnum, TEXT("/Script/TimeSynth"), TEXT("ETimeSynthEventClipQuantization"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimeSynth_ETimeSynthEventClipQuantization_Hash() { return 1770569685U; }
	UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthEventClipQuantization()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimeSynthEventClipQuantization"), 0, Get_Z_Construct_UEnum_TimeSynth_ETimeSynthEventClipQuantization_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimeSynthEventClipQuantization::Global", (int64)ETimeSynthEventClipQuantization::Global },
				{ "ETimeSynthEventClipQuantization::None", (int64)ETimeSynthEventClipQuantization::None },
				{ "ETimeSynthEventClipQuantization::Bars8", (int64)ETimeSynthEventClipQuantization::Bars8 },
				{ "ETimeSynthEventClipQuantization::Bars4", (int64)ETimeSynthEventClipQuantization::Bars4 },
				{ "ETimeSynthEventClipQuantization::Bars2", (int64)ETimeSynthEventClipQuantization::Bars2 },
				{ "ETimeSynthEventClipQuantization::Bar", (int64)ETimeSynthEventClipQuantization::Bar },
				{ "ETimeSynthEventClipQuantization::HalfNote", (int64)ETimeSynthEventClipQuantization::HalfNote },
				{ "ETimeSynthEventClipQuantization::HalfNoteTriplet", (int64)ETimeSynthEventClipQuantization::HalfNoteTriplet },
				{ "ETimeSynthEventClipQuantization::QuarterNote", (int64)ETimeSynthEventClipQuantization::QuarterNote },
				{ "ETimeSynthEventClipQuantization::QuarterNoteTriplet", (int64)ETimeSynthEventClipQuantization::QuarterNoteTriplet },
				{ "ETimeSynthEventClipQuantization::EighthNote", (int64)ETimeSynthEventClipQuantization::EighthNote },
				{ "ETimeSynthEventClipQuantization::EighthNoteTriplet", (int64)ETimeSynthEventClipQuantization::EighthNoteTriplet },
				{ "ETimeSynthEventClipQuantization::SixteenthNote", (int64)ETimeSynthEventClipQuantization::SixteenthNote },
				{ "ETimeSynthEventClipQuantization::SixteenthNoteTriplet", (int64)ETimeSynthEventClipQuantization::SixteenthNoteTriplet },
				{ "ETimeSynthEventClipQuantization::ThirtySecondNote", (int64)ETimeSynthEventClipQuantization::ThirtySecondNote },
				{ "ETimeSynthEventClipQuantization::Count", (int64)ETimeSynthEventClipQuantization::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Bar.DisplayName", "1 Bar" },
				{ "Bar.Name", "ETimeSynthEventClipQuantization::Bar" },
				{ "Bars2.DisplayName", "2 Bars" },
				{ "Bars2.Name", "ETimeSynthEventClipQuantization::Bars2" },
				{ "Bars4.DisplayName", "4 Bars" },
				{ "Bars4.Name", "ETimeSynthEventClipQuantization::Bars4" },
				{ "Bars8.DisplayName", "8 Bars" },
				{ "Bars8.Name", "ETimeSynthEventClipQuantization::Bars8" },
				{ "BlueprintType", "true" },
				{ "Comment", "// An enumeration for specifying quantization for time synth clips\n" },
				{ "Count.Hidden", "" },
				{ "Count.Name", "ETimeSynthEventClipQuantization::Count" },
				{ "EighthNote.DisplayName", "1/8" },
				{ "EighthNote.Name", "ETimeSynthEventClipQuantization::EighthNote" },
				{ "EighthNoteTriplet.DisplayName", "1/8 T" },
				{ "EighthNoteTriplet.Name", "ETimeSynthEventClipQuantization::EighthNoteTriplet" },
				{ "Global.DisplayName", "Global Quantization" },
				{ "Global.Name", "ETimeSynthEventClipQuantization::Global" },
				{ "HalfNote.DisplayName", "1/2" },
				{ "HalfNote.Name", "ETimeSynthEventClipQuantization::HalfNote" },
				{ "HalfNoteTriplet.DisplayName", "1/2 T" },
				{ "HalfNoteTriplet.Name", "ETimeSynthEventClipQuantization::HalfNoteTriplet" },
				{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
				{ "None.DisplayName", "No Quantization" },
				{ "None.Name", "ETimeSynthEventClipQuantization::None" },
				{ "QuarterNote.DisplayName", "1/4" },
				{ "QuarterNote.Name", "ETimeSynthEventClipQuantization::QuarterNote" },
				{ "QuarterNoteTriplet.DisplayName", "1/4 T" },
				{ "QuarterNoteTriplet.Name", "ETimeSynthEventClipQuantization::QuarterNoteTriplet" },
				{ "SixteenthNote.DisplayName", "1/16" },
				{ "SixteenthNote.Name", "ETimeSynthEventClipQuantization::SixteenthNote" },
				{ "SixteenthNoteTriplet.DisplayName", "1/16 T" },
				{ "SixteenthNoteTriplet.Name", "ETimeSynthEventClipQuantization::SixteenthNoteTriplet" },
				{ "ThirtySecondNote.DisplayName", "1/32" },
				{ "ThirtySecondNote.Name", "ETimeSynthEventClipQuantization::ThirtySecondNote" },
				{ "ToolTip", "An enumeration for specifying quantization for time synth clips" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimeSynth,
				nullptr,
				"ETimeSynthEventClipQuantization",
				"ETimeSynthEventClipQuantization",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETimeSynthFFTSize_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimeSynth_ETimeSynthFFTSize, Z_Construct_UPackage__Script_TimeSynth(), TEXT("ETimeSynthFFTSize"));
		}
		return Singleton;
	}
	template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthFFTSize>()
	{
		return ETimeSynthFFTSize_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimeSynthFFTSize(ETimeSynthFFTSize_StaticEnum, TEXT("/Script/TimeSynth"), TEXT("ETimeSynthFFTSize"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimeSynth_ETimeSynthFFTSize_Hash() { return 1489227426U; }
	UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthFFTSize()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimeSynthFFTSize"), 0, Get_Z_Construct_UEnum_TimeSynth_ETimeSynthFFTSize_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimeSynthFFTSize::Min_64", (int64)ETimeSynthFFTSize::Min_64 },
				{ "ETimeSynthFFTSize::Small_256", (int64)ETimeSynthFFTSize::Small_256 },
				{ "ETimeSynthFFTSize::Medium_512", (int64)ETimeSynthFFTSize::Medium_512 },
				{ "ETimeSynthFFTSize::Large_1024", (int64)ETimeSynthFFTSize::Large_1024 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Large_1024.Name", "ETimeSynthFFTSize::Large_1024" },
				{ "Medium_512.Name", "ETimeSynthFFTSize::Medium_512" },
				{ "Min_64.Name", "ETimeSynthFFTSize::Min_64" },
				{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
				{ "Small_256.Name", "ETimeSynthFFTSize::Small_256" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimeSynth,
				nullptr,
				"ETimeSynthFFTSize",
				"ETimeSynthFFTSize",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETimeSynthBeatDivision_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimeSynth_ETimeSynthBeatDivision, Z_Construct_UPackage__Script_TimeSynth(), TEXT("ETimeSynthBeatDivision"));
		}
		return Singleton;
	}
	template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthBeatDivision>()
	{
		return ETimeSynthBeatDivision_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimeSynthBeatDivision(ETimeSynthBeatDivision_StaticEnum, TEXT("/Script/TimeSynth"), TEXT("ETimeSynthBeatDivision"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimeSynth_ETimeSynthBeatDivision_Hash() { return 699643152U; }
	UEnum* Z_Construct_UEnum_TimeSynth_ETimeSynthBeatDivision()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimeSynthBeatDivision"), 0, Get_Z_Construct_UEnum_TimeSynth_ETimeSynthBeatDivision_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimeSynthBeatDivision::One", (int64)ETimeSynthBeatDivision::One },
				{ "ETimeSynthBeatDivision::Two", (int64)ETimeSynthBeatDivision::Two },
				{ "ETimeSynthBeatDivision::Four", (int64)ETimeSynthBeatDivision::Four },
				{ "ETimeSynthBeatDivision::Eight", (int64)ETimeSynthBeatDivision::Eight },
				{ "ETimeSynthBeatDivision::Sixteen", (int64)ETimeSynthBeatDivision::Sixteen },
				{ "ETimeSynthBeatDivision::Count", (int64)ETimeSynthBeatDivision::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Count.Hidden", "" },
				{ "Count.Name", "ETimeSynthBeatDivision::Count" },
				{ "Eight.DisplayName", "8" },
				{ "Eight.Name", "ETimeSynthBeatDivision::Eight" },
				{ "Four.DisplayName", "4" },
				{ "Four.Name", "ETimeSynthBeatDivision::Four" },
				{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
				{ "One.DisplayName", "1" },
				{ "One.Name", "ETimeSynthBeatDivision::One" },
				{ "Sixteen.DisplayName", "16" },
				{ "Sixteen.Name", "ETimeSynthBeatDivision::Sixteen" },
				{ "Two.DisplayName", "2" },
				{ "Two.Name", "ETimeSynthBeatDivision::Two" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimeSynth,
				nullptr,
				"ETimeSynthBeatDivision",
				"ETimeSynthBeatDivision",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FTimeSynthEnvelopeFollowerSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMESYNTH_API uint32 Get_Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings, Z_Construct_UPackage__Script_TimeSynth(), TEXT("TimeSynthEnvelopeFollowerSettings"), sizeof(FTimeSynthEnvelopeFollowerSettings), Get_Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Hash());
	}
	return Singleton;
}
template<> TIMESYNTH_API UScriptStruct* StaticStruct<FTimeSynthEnvelopeFollowerSettings>()
{
	return FTimeSynthEnvelopeFollowerSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings(FTimeSynthEnvelopeFollowerSettings::StaticStruct, TEXT("/Script/TimeSynth"), TEXT("TimeSynthEnvelopeFollowerSettings"), false, nullptr, nullptr);
static struct FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthEnvelopeFollowerSettings
{
	FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthEnvelopeFollowerSettings()
	{
		UScriptStruct::DeferCppStructOps<FTimeSynthEnvelopeFollowerSettings>(FName(TEXT("TimeSynthEnvelopeFollowerSettings")));
	}
} ScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthEnvelopeFollowerSettings;
	struct Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttackTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AttackTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReleaseTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReleaseTime;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PeakMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PeakMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PeakMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsAnalogMode_MetaData[];
#endif
		static void NewProp_bIsAnalogMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsAnalogMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimeSynthEnvelopeFollowerSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_AttackTime_MetaData[] = {
		{ "Category", "Envelope Follower" },
		{ "ClampMin", "0.0" },
		{ "Comment", "// The attack time of the envelope follower in milliseconds\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The attack time of the envelope follower in milliseconds" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_AttackTime = { "AttackTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthEnvelopeFollowerSettings, AttackTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_AttackTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_AttackTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_ReleaseTime_MetaData[] = {
		{ "Category", "Envelope Follower" },
		{ "ClampMin", "0.0" },
		{ "Comment", "// The release time of the envelope follower in milliseconds\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The release time of the envelope follower in milliseconds" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_ReleaseTime = { "ReleaseTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthEnvelopeFollowerSettings, ReleaseTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_ReleaseTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_ReleaseTime_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_PeakMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_PeakMode_MetaData[] = {
		{ "Category", "Envelope Follower" },
		{ "Comment", "// The peak mode of the envelope follower\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The peak mode of the envelope follower" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_PeakMode = { "PeakMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthEnvelopeFollowerSettings, PeakMode), Z_Construct_UEnum_TimeSynth_ETimeSynthEnvelopeFollowerPeakMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_PeakMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_PeakMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_bIsAnalogMode_MetaData[] = {
		{ "Category", "Envelope Follower" },
		{ "Comment", "// Whether or not the envelope follower is in analog mode\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Whether or not the envelope follower is in analog mode" },
	};
#endif
	void Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_bIsAnalogMode_SetBit(void* Obj)
	{
		((FTimeSynthEnvelopeFollowerSettings*)Obj)->bIsAnalogMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_bIsAnalogMode = { "bIsAnalogMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimeSynthEnvelopeFollowerSettings), &Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_bIsAnalogMode_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_bIsAnalogMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_bIsAnalogMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_AttackTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_ReleaseTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_PeakMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_PeakMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::NewProp_bIsAnalogMode,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynth,
		nullptr,
		&NewStructOps,
		"TimeSynthEnvelopeFollowerSettings",
		sizeof(FTimeSynthEnvelopeFollowerSettings),
		alignof(FTimeSynthEnvelopeFollowerSettings),
		Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimeSynthEnvelopeFollowerSettings"), sizeof(FTimeSynthEnvelopeFollowerSettings), Get_Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Hash() { return 157915012U; }
class UScriptStruct* FTimeSynthFilterSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMESYNTH_API uint32 Get_Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimeSynthFilterSettings, Z_Construct_UPackage__Script_TimeSynth(), TEXT("TimeSynthFilterSettings"), sizeof(FTimeSynthFilterSettings), Get_Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Hash());
	}
	return Singleton;
}
template<> TIMESYNTH_API UScriptStruct* StaticStruct<FTimeSynthFilterSettings>()
{
	return FTimeSynthFilterSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimeSynthFilterSettings(FTimeSynthFilterSettings::StaticStruct, TEXT("/Script/TimeSynth"), TEXT("TimeSynthFilterSettings"), false, nullptr, nullptr);
static struct FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthFilterSettings
{
	FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthFilterSettings()
	{
		UScriptStruct::DeferCppStructOps<FTimeSynthFilterSettings>(FName(TEXT("TimeSynthFilterSettings")));
	}
} ScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthFilterSettings;
	struct Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FilterType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FilterType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutoffFrequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CutoffFrequency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterQ_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FilterQ;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimeSynthFilterSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterType_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The type of filter to use.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The type of filter to use." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterType = { "FilterType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthFilterSettings, FilterType), Z_Construct_UEnum_TimeSynth_ETimeSynthFilterType, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_CutoffFrequency_MetaData[] = {
		{ "Category", "Filter" },
		{ "ClampMin", "20.0" },
		{ "Comment", "// The filter cutoff frequency\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The filter cutoff frequency" },
		{ "UIMax", "12000.0" },
		{ "UIMin", "20.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_CutoffFrequency = { "CutoffFrequency", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthFilterSettings, CutoffFrequency), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_CutoffFrequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_CutoffFrequency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterQ_MetaData[] = {
		{ "Category", "Filter" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.5" },
		{ "Comment", "// The filter resonance.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The filter resonance." },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterQ = { "FilterQ", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthFilterSettings, FilterQ), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterQ_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterQ_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_CutoffFrequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::NewProp_FilterQ,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynth,
		nullptr,
		&NewStructOps,
		"TimeSynthFilterSettings",
		sizeof(FTimeSynthFilterSettings),
		alignof(FTimeSynthFilterSettings),
		Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthFilterSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimeSynthFilterSettings"), sizeof(FTimeSynthFilterSettings), Get_Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Hash() { return 3575977222U; }
class UScriptStruct* FTimeSynthClipSound::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMESYNTH_API uint32 Get_Z_Construct_UScriptStruct_FTimeSynthClipSound_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimeSynthClipSound, Z_Construct_UPackage__Script_TimeSynth(), TEXT("TimeSynthClipSound"), sizeof(FTimeSynthClipSound), Get_Z_Construct_UScriptStruct_FTimeSynthClipSound_Hash());
	}
	return Singleton;
}
template<> TIMESYNTH_API UScriptStruct* StaticStruct<FTimeSynthClipSound>()
{
	return FTimeSynthClipSound::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimeSynthClipSound(FTimeSynthClipSound::StaticStruct, TEXT("/Script/TimeSynth"), TEXT("TimeSynthClipSound"), false, nullptr, nullptr);
static struct FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthClipSound
{
	FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthClipSound()
	{
		UScriptStruct::DeferCppStructOps<FTimeSynthClipSound>(FName(TEXT("TimeSynthClipSound")));
	}
} ScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthClipSound;
	struct Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoundWave_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SoundWave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomWeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RandomWeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistanceRange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimeSynthClipSound>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_SoundWave_MetaData[] = {
		{ "Category", "ClipSound" },
		{ "Comment", "// The sound wave clip to play\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The sound wave clip to play" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_SoundWave = { "SoundWave", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthClipSound, SoundWave), Z_Construct_UClass_USoundWave_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_SoundWave_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_SoundWave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_RandomWeight_MetaData[] = {
		{ "Category", "ClipSound" },
		{ "Comment", "// The sound wave clip to play\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The sound wave clip to play" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_RandomWeight = { "RandomWeight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthClipSound, RandomWeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_RandomWeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_RandomWeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_DistanceRange_MetaData[] = {
		{ "Category", "ClipSound" },
		{ "Comment", "// The distance range of the clip. If zeroed, will play the clip at any range.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The distance range of the clip. If zeroed, will play the clip at any range." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_DistanceRange = { "DistanceRange", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthClipSound, DistanceRange), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_DistanceRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_DistanceRange_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_SoundWave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_RandomWeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::NewProp_DistanceRange,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynth,
		nullptr,
		&NewStructOps,
		"TimeSynthClipSound",
		sizeof(FTimeSynthClipSound),
		alignof(FTimeSynthClipSound),
		Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthClipSound()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimeSynthClipSound_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimeSynthClipSound"), sizeof(FTimeSynthClipSound), Get_Z_Construct_UScriptStruct_FTimeSynthClipSound_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimeSynthClipSound_Hash() { return 439734608U; }
class UScriptStruct* FTimeSynthClipHandle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMESYNTH_API uint32 Get_Z_Construct_UScriptStruct_FTimeSynthClipHandle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimeSynthClipHandle, Z_Construct_UPackage__Script_TimeSynth(), TEXT("TimeSynthClipHandle"), sizeof(FTimeSynthClipHandle), Get_Z_Construct_UScriptStruct_FTimeSynthClipHandle_Hash());
	}
	return Singleton;
}
template<> TIMESYNTH_API UScriptStruct* StaticStruct<FTimeSynthClipHandle>()
{
	return FTimeSynthClipHandle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimeSynthClipHandle(FTimeSynthClipHandle::StaticStruct, TEXT("/Script/TimeSynth"), TEXT("TimeSynthClipHandle"), false, nullptr, nullptr);
static struct FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthClipHandle
{
	FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthClipHandle()
	{
		UScriptStruct::DeferCppStructOps<FTimeSynthClipHandle>(FName(TEXT("TimeSynthClipHandle")));
	}
} ScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthClipHandle;
	struct Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClipName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ClipName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClipId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ClipId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Struct used to define a handle to a clip\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Struct used to define a handle to a clip" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimeSynthClipHandle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewProp_ClipName_MetaData[] = {
		{ "Category", "Synth|TimeSynth" },
		{ "Comment", "// The number of bars\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The number of bars" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewProp_ClipName = { "ClipName", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthClipHandle, ClipName), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewProp_ClipName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewProp_ClipName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewProp_ClipId_MetaData[] = {
		{ "Category", "Synth|TimeSynth" },
		{ "Comment", "// The Id of the clip\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The Id of the clip" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewProp_ClipId = { "ClipId", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthClipHandle, ClipId), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewProp_ClipId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewProp_ClipId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewProp_ClipName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::NewProp_ClipId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynth,
		nullptr,
		&NewStructOps,
		"TimeSynthClipHandle",
		sizeof(FTimeSynthClipHandle),
		alignof(FTimeSynthClipHandle),
		Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthClipHandle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimeSynthClipHandle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimeSynthClipHandle"), sizeof(FTimeSynthClipHandle), Get_Z_Construct_UScriptStruct_FTimeSynthClipHandle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimeSynthClipHandle_Hash() { return 1645841871U; }
class UScriptStruct* FTimeSynthTimeDef::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMESYNTH_API uint32 Get_Z_Construct_UScriptStruct_FTimeSynthTimeDef_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimeSynthTimeDef, Z_Construct_UPackage__Script_TimeSynth(), TEXT("TimeSynthTimeDef"), sizeof(FTimeSynthTimeDef), Get_Z_Construct_UScriptStruct_FTimeSynthTimeDef_Hash());
	}
	return Singleton;
}
template<> TIMESYNTH_API UScriptStruct* StaticStruct<FTimeSynthTimeDef>()
{
	return FTimeSynthTimeDef::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimeSynthTimeDef(FTimeSynthTimeDef::StaticStruct, TEXT("/Script/TimeSynth"), TEXT("TimeSynthTimeDef"), false, nullptr, nullptr);
static struct FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthTimeDef
{
	FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthTimeDef()
	{
		UScriptStruct::DeferCppStructOps<FTimeSynthTimeDef>(FName(TEXT("TimeSynthTimeDef")));
	}
} ScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthTimeDef;
	struct Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumBars_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumBars;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumBeats_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumBeats;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Struct using to define a time range for the time synth in quantized time units\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Struct using to define a time range for the time synth in quantized time units" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimeSynthTimeDef>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewProp_NumBars_MetaData[] = {
		{ "Category", "Synth|TimeSynth" },
		{ "ClampMin", "0" },
		{ "Comment", "// The number of bars\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The number of bars" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewProp_NumBars = { "NumBars", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthTimeDef, NumBars), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewProp_NumBars_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewProp_NumBars_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewProp_NumBeats_MetaData[] = {
		{ "Category", "Synth|TimeSynth" },
		{ "ClampMin", "0" },
		{ "Comment", "// The number of beats\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The number of beats" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewProp_NumBeats = { "NumBeats", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthTimeDef, NumBeats), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewProp_NumBeats_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewProp_NumBeats_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewProp_NumBars,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::NewProp_NumBeats,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynth,
		nullptr,
		&NewStructOps,
		"TimeSynthTimeDef",
		sizeof(FTimeSynthTimeDef),
		alignof(FTimeSynthTimeDef),
		Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthTimeDef()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimeSynthTimeDef_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimeSynthTimeDef"), sizeof(FTimeSynthTimeDef), Get_Z_Construct_UScriptStruct_FTimeSynthTimeDef_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimeSynthTimeDef_Hash() { return 3766217573U; }
class UScriptStruct* FTimeSynthQuantizationSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMESYNTH_API uint32 Get_Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings, Z_Construct_UPackage__Script_TimeSynth(), TEXT("TimeSynthQuantizationSettings"), sizeof(FTimeSynthQuantizationSettings), Get_Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Hash());
	}
	return Singleton;
}
template<> TIMESYNTH_API UScriptStruct* StaticStruct<FTimeSynthQuantizationSettings>()
{
	return FTimeSynthQuantizationSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimeSynthQuantizationSettings(FTimeSynthQuantizationSettings::StaticStruct, TEXT("/Script/TimeSynth"), TEXT("TimeSynthQuantizationSettings"), false, nullptr, nullptr);
static struct FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthQuantizationSettings
{
	FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthQuantizationSettings()
	{
		UScriptStruct::DeferCppStructOps<FTimeSynthQuantizationSettings>(FName(TEXT("TimeSynthQuantizationSettings")));
	}
} ScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthQuantizationSettings;
	struct Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BeatsPerMinute_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BeatsPerMinute;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BeatsPerBar_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BeatsPerBar;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BeatDivision_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BeatDivision_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BeatDivision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventDelaySeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EventDelaySeconds;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_GlobalQuantization_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GlobalQuantization_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_GlobalQuantization;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Struct defining the time synth global quantization settings\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Struct defining the time synth global quantization settings" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimeSynthQuantizationSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatsPerMinute_MetaData[] = {
		{ "Category", "Synth|TimeSynth|PlayClip" },
		{ "ClampMax", "999.0" },
		{ "ClampMin", "1.0" },
		{ "Comment", "// The beats per minute of the pulse. Musical convention gives this as BPM for \"quarter notes\" (BeatDivision = 4).\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The beats per minute of the pulse. Musical convention gives this as BPM for \"quarter notes\" (BeatDivision = 4)." },
		{ "UIMax", "999.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatsPerMinute = { "BeatsPerMinute", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthQuantizationSettings, BeatsPerMinute), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatsPerMinute_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatsPerMinute_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatsPerBar_MetaData[] = {
		{ "Category", "Synth|TimeSynth|PlayClip" },
		{ "ClampMin", "1" },
		{ "Comment", "// Defines numerator when determining beat time in seconds\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Defines numerator when determining beat time in seconds" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatsPerBar = { "BeatsPerBar", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthQuantizationSettings, BeatsPerBar), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatsPerBar_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatsPerBar_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatDivision_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatDivision_MetaData[] = {
		{ "Category", "Synth|TimeSynth|PlayClip" },
		{ "ClampMin", "1" },
		{ "Comment", "// Amount of beats in a whole note. Defines number of beats in a measure.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Amount of beats in a whole note. Defines number of beats in a measure." },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatDivision = { "BeatDivision", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthQuantizationSettings, BeatDivision), Z_Construct_UEnum_TimeSynth_ETimeSynthBeatDivision, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatDivision_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatDivision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_EventDelaySeconds_MetaData[] = {
		{ "Category", "Synth|TimeSynth|PlayClip" },
		{ "ClampMin", "0.0" },
		{ "Comment", "// The amount of latency to add to time synth events to allow BP delegates to perform logic on game thread\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The amount of latency to add to time synth events to allow BP delegates to perform logic on game thread" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_EventDelaySeconds = { "EventDelaySeconds", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthQuantizationSettings, EventDelaySeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_EventDelaySeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_EventDelaySeconds_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_GlobalQuantization_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_GlobalQuantization_MetaData[] = {
		{ "Category", "Synth|TimeSynth|PlayClip" },
		{ "Comment", "// This is the rate at which FOnTimeSynthEvent callbacks are made.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "This is the rate at which FOnTimeSynthEvent callbacks are made." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_GlobalQuantization = { "GlobalQuantization", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthQuantizationSettings, GlobalQuantization), Z_Construct_UEnum_TimeSynth_ETimeSynthEventQuantization, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_GlobalQuantization_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_GlobalQuantization_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatsPerMinute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatsPerBar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatDivision_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_BeatDivision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_EventDelaySeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_GlobalQuantization_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::NewProp_GlobalQuantization,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynth,
		nullptr,
		&NewStructOps,
		"TimeSynthQuantizationSettings",
		sizeof(FTimeSynthQuantizationSettings),
		alignof(FTimeSynthQuantizationSettings),
		Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimeSynthQuantizationSettings"), sizeof(FTimeSynthQuantizationSettings), Get_Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Hash() { return 3275163091U; }
class UScriptStruct* FTimeSynthSpectralData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMESYNTH_API uint32 Get_Z_Construct_UScriptStruct_FTimeSynthSpectralData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimeSynthSpectralData, Z_Construct_UPackage__Script_TimeSynth(), TEXT("TimeSynthSpectralData"), sizeof(FTimeSynthSpectralData), Get_Z_Construct_UScriptStruct_FTimeSynthSpectralData_Hash());
	}
	return Singleton;
}
template<> TIMESYNTH_API UScriptStruct* StaticStruct<FTimeSynthSpectralData>()
{
	return FTimeSynthSpectralData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimeSynthSpectralData(FTimeSynthSpectralData::StaticStruct, TEXT("/Script/TimeSynth"), TEXT("TimeSynthSpectralData"), false, nullptr, nullptr);
static struct FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthSpectralData
{
	FScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthSpectralData()
	{
		UScriptStruct::DeferCppStructOps<FTimeSynthSpectralData>(FName(TEXT("TimeSynthSpectralData")));
	}
} ScriptStruct_TimeSynth_StaticRegisterNativesFTimeSynthSpectralData;
	struct Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrequencyHz_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FrequencyHz;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Magnitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Magnitude;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimeSynthSpectralData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewProp_FrequencyHz_MetaData[] = {
		{ "Category", "SpectralData" },
		{ "Comment", "// The frequency hz of the spectrum value\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The frequency hz of the spectrum value" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewProp_FrequencyHz = { "FrequencyHz", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthSpectralData, FrequencyHz), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewProp_FrequencyHz_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewProp_FrequencyHz_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewProp_Magnitude_MetaData[] = {
		{ "Category", "SpectralData" },
		{ "Comment", "// The magnitude of the spectrum at this frequency\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The magnitude of the spectrum at this frequency" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewProp_Magnitude = { "Magnitude", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimeSynthSpectralData, Magnitude), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewProp_Magnitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewProp_Magnitude_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewProp_FrequencyHz,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::NewProp_Magnitude,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynth,
		nullptr,
		&NewStructOps,
		"TimeSynthSpectralData",
		sizeof(FTimeSynthSpectralData),
		alignof(FTimeSynthSpectralData),
		Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimeSynthSpectralData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimeSynthSpectralData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimeSynth();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimeSynthSpectralData"), sizeof(FTimeSynthSpectralData), Get_Z_Construct_UScriptStruct_FTimeSynthSpectralData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimeSynthSpectralData_Hash() { return 1764116448U; }
	void UTimeSynthVolumeGroup::StaticRegisterNativesUTimeSynthVolumeGroup()
	{
	}
	UClass* Z_Construct_UClass_UTimeSynthVolumeGroup_NoRegister()
	{
		return UTimeSynthVolumeGroup::StaticClass();
	}
	struct Z_Construct_UClass_UTimeSynthVolumeGroup_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultVolume_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DefaultVolume;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynth,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Synth" },
		{ "IncludePath", "TimeSynthComponent.h" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::NewProp_DefaultVolume_MetaData[] = {
		{ "Category", "Clip" },
		{ "Comment", "// The default volume of the volume group\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The default volume of the volume group" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::NewProp_DefaultVolume = { "DefaultVolume", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthVolumeGroup, DefaultVolume), METADATA_PARAMS(Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::NewProp_DefaultVolume_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::NewProp_DefaultVolume_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::NewProp_DefaultVolume,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTimeSynthVolumeGroup>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::ClassParams = {
		&UTimeSynthVolumeGroup::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTimeSynthVolumeGroup()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTimeSynthVolumeGroup_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTimeSynthVolumeGroup, 4005462305);
	template<> TIMESYNTH_API UClass* StaticClass<UTimeSynthVolumeGroup>()
	{
		return UTimeSynthVolumeGroup::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTimeSynthVolumeGroup(Z_Construct_UClass_UTimeSynthVolumeGroup, &UTimeSynthVolumeGroup::StaticClass, TEXT("/Script/TimeSynth"), TEXT("UTimeSynthVolumeGroup"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTimeSynthVolumeGroup);
	void UTimeSynthClip::StaticRegisterNativesUTimeSynthClip()
	{
	}
	UClass* Z_Construct_UClass_UTimeSynthClip_NoRegister()
	{
		return UTimeSynthClip::StaticClass();
	}
	struct Z_Construct_UClass_UTimeSynthClip_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Sounds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sounds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Sounds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VolumeScaleDb_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VolumeScaleDb;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PitchScaleSemitones_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PitchScaleSemitones;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FadeInTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FadeInTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyFadeOut_MetaData[];
#endif
		static void NewProp_bApplyFadeOut_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyFadeOut;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FadeOutTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FadeOutTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClipDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClipDuration;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ClipQuantization_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClipQuantization_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ClipQuantization;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTimeSynthClip_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynth,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthClip_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Synth" },
		{ "IncludePath", "TimeSynthComponent.h" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_Sounds_Inner = { "Sounds", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTimeSynthClipSound, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_Sounds_MetaData[] = {
		{ "Category", "Clip" },
		{ "Comment", "// Array of possible choices for the clip, allows randomization and distance picking\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Array of possible choices for the clip, allows randomization and distance picking" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_Sounds = { "Sounds", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthClip, Sounds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_Sounds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_Sounds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_VolumeScaleDb_MetaData[] = {
		{ "Category", "VolumeControl" },
		{ "Comment", "// The volume scale range of the clip\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The volume scale range of the clip" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_VolumeScaleDb = { "VolumeScaleDb", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthClip, VolumeScaleDb), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_VolumeScaleDb_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_VolumeScaleDb_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_PitchScaleSemitones_MetaData[] = {
		{ "Category", "TimeControl" },
		{ "Comment", "// The pitch scale range of the clip (in semi-tone range)\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The pitch scale range of the clip (in semi-tone range)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_PitchScaleSemitones = { "PitchScaleSemitones", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthClip, PitchScaleSemitones), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_PitchScaleSemitones_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_PitchScaleSemitones_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_FadeInTime_MetaData[] = {
		{ "Category", "VolumeControl" },
		{ "Comment", "// The amount of time to fade in the clip from the start\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The amount of time to fade in the clip from the start" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_FadeInTime = { "FadeInTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthClip, FadeInTime), Z_Construct_UScriptStruct_FTimeSynthTimeDef, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_FadeInTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_FadeInTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_bApplyFadeOut_MetaData[] = {
		{ "Category", "VolumeControl" },
		{ "Comment", "// If true, the clip will apply a fade when the clip duration expires. Otherwise, the clip plays out past the \"duration\".\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "If true, the clip will apply a fade when the clip duration expires. Otherwise, the clip plays out past the \"duration\"." },
	};
#endif
	void Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_bApplyFadeOut_SetBit(void* Obj)
	{
		((UTimeSynthClip*)Obj)->bApplyFadeOut = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_bApplyFadeOut = { "bApplyFadeOut", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTimeSynthClip), &Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_bApplyFadeOut_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_bApplyFadeOut_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_bApplyFadeOut_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_FadeOutTime_MetaData[] = {
		{ "Category", "VolumeControl" },
		{ "Comment", "// The amount of time to fade out the clip when it reaches its set duration.\n" },
		{ "EditCondition", "bApplyFadeOut" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The amount of time to fade out the clip when it reaches its set duration." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_FadeOutTime = { "FadeOutTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthClip, FadeOutTime), Z_Construct_UScriptStruct_FTimeSynthTimeDef, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_FadeOutTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_FadeOutTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipDuration_MetaData[] = {
		{ "Category", "TimeControl" },
		{ "Comment", "// The clip duration\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The clip duration" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipDuration = { "ClipDuration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthClip, ClipDuration), Z_Construct_UScriptStruct_FTimeSynthTimeDef, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipDuration_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipQuantization_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipQuantization_MetaData[] = {
		{ "Category", "TimeControl" },
		{ "Comment", "// The clip duration\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The clip duration" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipQuantization = { "ClipQuantization", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthClip, ClipQuantization), Z_Construct_UEnum_TimeSynth_ETimeSynthEventClipQuantization, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipQuantization_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipQuantization_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTimeSynthClip_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_Sounds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_Sounds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_VolumeScaleDb,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_PitchScaleSemitones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_FadeInTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_bApplyFadeOut,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_FadeOutTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipQuantization_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthClip_Statics::NewProp_ClipQuantization,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTimeSynthClip_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTimeSynthClip>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTimeSynthClip_Statics::ClassParams = {
		&UTimeSynthClip::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTimeSynthClip_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClip_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTimeSynthClip_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthClip_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTimeSynthClip()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTimeSynthClip_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTimeSynthClip, 900660691);
	template<> TIMESYNTH_API UClass* StaticClass<UTimeSynthClip>()
	{
		return UTimeSynthClip::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTimeSynthClip(Z_Construct_UClass_UTimeSynthClip, &UTimeSynthClip::StaticClass, TEXT("/Script/TimeSynth"), TEXT("UTimeSynthClip"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTimeSynthClip);
	DEFINE_FUNCTION(UTimeSynthComponent::execHasActiveClips)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->HasActiveClips();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execSetFFTSize)
	{
		P_GET_ENUM(ETimeSynthFFTSize,Z_Param_InFFTSize);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFFTSize(ETimeSynthFFTSize(Z_Param_InFFTSize));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execSetEnvelopeFollowerEnabled)
	{
		P_GET_UBOOL(Z_Param_bInIsEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetEnvelopeFollowerEnabled(Z_Param_bInIsEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execSetFilterEnabled)
	{
		P_GET_ENUM(ETimeSynthFilter,Z_Param_Filter);
		P_GET_UBOOL(Z_Param_bIsEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFilterEnabled(ETimeSynthFilter(Z_Param_Filter),Z_Param_bIsEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execSetEnvelopeFollowerSettings)
	{
		P_GET_STRUCT_REF(FTimeSynthEnvelopeFollowerSettings,Z_Param_Out_InSettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetEnvelopeFollowerSettings(Z_Param_Out_InSettings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execSetFilterSettings)
	{
		P_GET_ENUM(ETimeSynthFilter,Z_Param_Filter);
		P_GET_STRUCT_REF(FTimeSynthFilterSettings,Z_Param_Out_InSettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFilterSettings(ETimeSynthFilter(Z_Param_Filter),Z_Param_Out_InSettings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execAddQuantizationEventDelegate)
	{
		P_GET_ENUM(ETimeSynthEventQuantization,Z_Param_QuantizationType);
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_OnQuantizationEvent);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddQuantizationEventDelegate(ETimeSynthEventQuantization(Z_Param_QuantizationType),FOnQuantizationEventBP(Z_Param_Out_OnQuantizationEvent));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execGetMaxActiveClipLimit)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetMaxActiveClipLimit();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execGetEnvelopeFollowerValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetEnvelopeFollowerValue();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execGetSpectralData)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FTimeSynthSpectralData>*)Z_Param__Result=P_THIS->GetSpectralData();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execStopSoundsOnVolumeGroupWithFadeOverride)
	{
		P_GET_OBJECT(UTimeSynthVolumeGroup,Z_Param_InVolumeGroup);
		P_GET_ENUM(ETimeSynthEventClipQuantization,Z_Param_EventQuantization);
		P_GET_STRUCT_REF(FTimeSynthTimeDef,Z_Param_Out_FadeTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopSoundsOnVolumeGroupWithFadeOverride(Z_Param_InVolumeGroup,ETimeSynthEventClipQuantization(Z_Param_EventQuantization),Z_Param_Out_FadeTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execStopSoundsOnVolumeGroup)
	{
		P_GET_OBJECT(UTimeSynthVolumeGroup,Z_Param_InVolumeGroup);
		P_GET_ENUM(ETimeSynthEventClipQuantization,Z_Param_EventQuantization);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopSoundsOnVolumeGroup(Z_Param_InVolumeGroup,ETimeSynthEventClipQuantization(Z_Param_EventQuantization));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execSetVolumeGroup)
	{
		P_GET_OBJECT(UTimeSynthVolumeGroup,Z_Param_InVolumeGroup);
		P_GET_PROPERTY(FFloatProperty,Z_Param_VolumeDb);
		P_GET_PROPERTY(FFloatProperty,Z_Param_FadeTimeSec);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetVolumeGroup(Z_Param_InVolumeGroup,Z_Param_VolumeDb,Z_Param_FadeTimeSec);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execStopClipWithFadeOverride)
	{
		P_GET_STRUCT(FTimeSynthClipHandle,Z_Param_InClipHandle);
		P_GET_ENUM(ETimeSynthEventClipQuantization,Z_Param_EventQuantization);
		P_GET_STRUCT_REF(FTimeSynthTimeDef,Z_Param_Out_FadeTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopClipWithFadeOverride(Z_Param_InClipHandle,ETimeSynthEventClipQuantization(Z_Param_EventQuantization),Z_Param_Out_FadeTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execStopClip)
	{
		P_GET_STRUCT(FTimeSynthClipHandle,Z_Param_InClipHandle);
		P_GET_ENUM(ETimeSynthEventClipQuantization,Z_Param_EventQuantization);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopClip(Z_Param_InClipHandle,ETimeSynthEventClipQuantization(Z_Param_EventQuantization));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execPlayClip)
	{
		P_GET_OBJECT(UTimeSynthClip,Z_Param_InClip);
		P_GET_OBJECT(UTimeSynthVolumeGroup,Z_Param_InVolumeGroup);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTimeSynthClipHandle*)Z_Param__Result=P_THIS->PlayClip(Z_Param_InClip,Z_Param_InVolumeGroup);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execResetSeed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetSeed();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execSetSeed)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_InSeed);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSeed(Z_Param_InSeed);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execGetBPM)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetBPM();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execSetBPM)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_BeatsPerMinute);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetBPM(Z_Param_BeatsPerMinute);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTimeSynthComponent::execSetQuantizationSettings)
	{
		P_GET_STRUCT_REF(FTimeSynthQuantizationSettings,Z_Param_Out_InQuantizationSettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetQuantizationSettings(Z_Param_Out_InQuantizationSettings);
		P_NATIVE_END;
	}
	void UTimeSynthComponent::StaticRegisterNativesUTimeSynthComponent()
	{
		UClass* Class = UTimeSynthComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddQuantizationEventDelegate", &UTimeSynthComponent::execAddQuantizationEventDelegate },
			{ "GetBPM", &UTimeSynthComponent::execGetBPM },
			{ "GetEnvelopeFollowerValue", &UTimeSynthComponent::execGetEnvelopeFollowerValue },
			{ "GetMaxActiveClipLimit", &UTimeSynthComponent::execGetMaxActiveClipLimit },
			{ "GetSpectralData", &UTimeSynthComponent::execGetSpectralData },
			{ "HasActiveClips", &UTimeSynthComponent::execHasActiveClips },
			{ "PlayClip", &UTimeSynthComponent::execPlayClip },
			{ "ResetSeed", &UTimeSynthComponent::execResetSeed },
			{ "SetBPM", &UTimeSynthComponent::execSetBPM },
			{ "SetEnvelopeFollowerEnabled", &UTimeSynthComponent::execSetEnvelopeFollowerEnabled },
			{ "SetEnvelopeFollowerSettings", &UTimeSynthComponent::execSetEnvelopeFollowerSettings },
			{ "SetFFTSize", &UTimeSynthComponent::execSetFFTSize },
			{ "SetFilterEnabled", &UTimeSynthComponent::execSetFilterEnabled },
			{ "SetFilterSettings", &UTimeSynthComponent::execSetFilterSettings },
			{ "SetQuantizationSettings", &UTimeSynthComponent::execSetQuantizationSettings },
			{ "SetSeed", &UTimeSynthComponent::execSetSeed },
			{ "SetVolumeGroup", &UTimeSynthComponent::execSetVolumeGroup },
			{ "StopClip", &UTimeSynthComponent::execStopClip },
			{ "StopClipWithFadeOverride", &UTimeSynthComponent::execStopClipWithFadeOverride },
			{ "StopSoundsOnVolumeGroup", &UTimeSynthComponent::execStopSoundsOnVolumeGroup },
			{ "StopSoundsOnVolumeGroupWithFadeOverride", &UTimeSynthComponent::execStopSoundsOnVolumeGroupWithFadeOverride },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics
	{
		struct TimeSynthComponent_eventAddQuantizationEventDelegate_Parms
		{
			ETimeSynthEventQuantization QuantizationType;
			FScriptDelegate OnQuantizationEvent;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_QuantizationType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_QuantizationType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnQuantizationEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_OnQuantizationEvent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::NewProp_QuantizationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::NewProp_QuantizationType = { "QuantizationType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventAddQuantizationEventDelegate_Parms, QuantizationType), Z_Construct_UEnum_TimeSynth_ETimeSynthEventQuantization, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::NewProp_OnQuantizationEvent_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::NewProp_OnQuantizationEvent = { "OnQuantizationEvent", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventAddQuantizationEventDelegate_Parms, OnQuantizationEvent), Z_Construct_UDelegateFunction_TimeSynth_OnQuantizationEventBP__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::NewProp_OnQuantizationEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::NewProp_OnQuantizationEvent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::NewProp_QuantizationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::NewProp_QuantizationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::NewProp_OnQuantizationEvent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Quantization" },
		{ "Comment", "// Register an event to respond to a specific quantization event\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Register an event to respond to a specific quantization event" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "AddQuantizationEventDelegate", nullptr, nullptr, sizeof(TimeSynthComponent_eventAddQuantizationEventDelegate_Parms), Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics
	{
		struct TimeSynthComponent_eventGetBPM_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventGetBPM_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Returns the current BPM of the time synth\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Returns the current BPM of the time synth" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "GetBPM", nullptr, nullptr, sizeof(TimeSynthComponent_eventGetBPM_Parms), Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_GetBPM()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_GetBPM_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics
	{
		struct TimeSynthComponent_eventGetEnvelopeFollowerValue_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventGetEnvelopeFollowerValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Returns the current envelope follower value. Call at whatever rate desired\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Returns the current envelope follower value. Call at whatever rate desired" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "GetEnvelopeFollowerValue", nullptr, nullptr, sizeof(TimeSynthComponent_eventGetEnvelopeFollowerValue_Parms), Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics
	{
		struct TimeSynthComponent_eventGetMaxActiveClipLimit_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventGetMaxActiveClipLimit_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "GetMaxActiveClipLimit", nullptr, nullptr, sizeof(TimeSynthComponent_eventGetMaxActiveClipLimit_Parms), Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics
	{
		struct TimeSynthComponent_eventGetSpectralData_Parms
		{
			TArray<FTimeSynthSpectralData> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTimeSynthSpectralData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventGetSpectralData_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Returns the spectral data if spectrum analysis is enabled. \n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Returns the spectral data if spectrum analysis is enabled." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "GetSpectralData", nullptr, nullptr, sizeof(TimeSynthComponent_eventGetSpectralData_Parms), Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics
	{
		struct TimeSynthComponent_eventHasActiveClips_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TimeSynthComponent_eventHasActiveClips_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TimeSynthComponent_eventHasActiveClips_Parms), &Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::Function_MetaDataParams[] = {
		{ "Category", "Playback State" },
		{ "Comment", "// Check to see if clips are actively generating sound on the TimeSynth\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Check to see if clips are actively generating sound on the TimeSynth" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "HasActiveClips", nullptr, nullptr, sizeof(TimeSynthComponent_eventHasActiveClips_Parms), Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics
	{
		struct TimeSynthComponent_eventPlayClip_Parms
		{
			UTimeSynthClip* InClip;
			UTimeSynthVolumeGroup* InVolumeGroup;
			FTimeSynthClipHandle ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InClip;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InVolumeGroup;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::NewProp_InClip = { "InClip", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventPlayClip_Parms, InClip), Z_Construct_UClass_UTimeSynthClip_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::NewProp_InVolumeGroup = { "InVolumeGroup", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventPlayClip_Parms, InVolumeGroup), Z_Construct_UClass_UTimeSynthVolumeGroup_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventPlayClip_Parms, ReturnValue), Z_Construct_UScriptStruct_FTimeSynthClipHandle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::NewProp_InClip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::NewProp_InVolumeGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Plays the given clip using the global quantization setting\n" },
		{ "CPP_Default_InVolumeGroup", "None" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Plays the given clip using the global quantization setting" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "PlayClip", nullptr, nullptr, sizeof(TimeSynthComponent_eventPlayClip_Parms), Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_PlayClip()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_PlayClip_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_ResetSeed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_ResetSeed_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Resets the internal seed to it's current seed (allows repeating same random choices)\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Resets the internal seed to it's current seed (allows repeating same random choices)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_ResetSeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "ResetSeed", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_ResetSeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_ResetSeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_ResetSeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_ResetSeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics
	{
		struct TimeSynthComponent_eventSetBPM_Parms
		{
			float BeatsPerMinute;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BeatsPerMinute_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BeatsPerMinute;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::NewProp_BeatsPerMinute_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::NewProp_BeatsPerMinute = { "BeatsPerMinute", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetBPM_Parms, BeatsPerMinute), METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::NewProp_BeatsPerMinute_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::NewProp_BeatsPerMinute_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::NewProp_BeatsPerMinute,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Sets just the BPM of the time synth on the next bar event.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Sets just the BPM of the time synth on the next bar event." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "SetBPM", nullptr, nullptr, sizeof(TimeSynthComponent_eventSetBPM_Parms), Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_SetBPM()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_SetBPM_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics
	{
		struct TimeSynthComponent_eventSetEnvelopeFollowerEnabled_Parms
		{
			bool bInIsEnabled;
		};
		static void NewProp_bInIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInIsEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::NewProp_bInIsEnabled_SetBit(void* Obj)
	{
		((TimeSynthComponent_eventSetEnvelopeFollowerEnabled_Parms*)Obj)->bInIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::NewProp_bInIsEnabled = { "bInIsEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TimeSynthComponent_eventSetEnvelopeFollowerEnabled_Parms), &Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::NewProp_bInIsEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::NewProp_bInIsEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Filter" },
		{ "Comment", "// Enables or disables the envelope follower\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Enables or disables the envelope follower" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "SetEnvelopeFollowerEnabled", nullptr, nullptr, sizeof(TimeSynthComponent_eventSetEnvelopeFollowerEnabled_Parms), Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics
	{
		struct TimeSynthComponent_eventSetEnvelopeFollowerSettings_Parms
		{
			FTimeSynthEnvelopeFollowerSettings InSettings;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::NewProp_InSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::NewProp_InSettings = { "InSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetEnvelopeFollowerSettings_Parms, InSettings), Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::NewProp_InSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::NewProp_InSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::NewProp_InSettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Filter" },
		{ "Comment", "// Set the envelope follower settings\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Set the envelope follower settings" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "SetEnvelopeFollowerSettings", nullptr, nullptr, sizeof(TimeSynthComponent_eventSetEnvelopeFollowerSettings_Parms), Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics
	{
		struct TimeSynthComponent_eventSetFFTSize_Parms
		{
			ETimeSynthFFTSize InFFTSize;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InFFTSize_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InFFTSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::NewProp_InFFTSize_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::NewProp_InFFTSize = { "InFFTSize", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetFFTSize_Parms, InFFTSize), Z_Construct_UEnum_TimeSynth_ETimeSynthFFTSize, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::NewProp_InFFTSize_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::NewProp_InFFTSize,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Spectral Analysis" },
		{ "Comment", "// Sets the desired FFT Size for the spectrum analyzer\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Sets the desired FFT Size for the spectrum analyzer" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "SetFFTSize", nullptr, nullptr, sizeof(TimeSynthComponent_eventSetFFTSize_Parms), Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics
	{
		struct TimeSynthComponent_eventSetFilterEnabled_Parms
		{
			ETimeSynthFilter Filter;
			bool bIsEnabled;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Filter_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Filter;
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::NewProp_Filter_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::NewProp_Filter = { "Filter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetFilterEnabled_Parms, Filter), Z_Construct_UEnum_TimeSynth_ETimeSynthFilter, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((TimeSynthComponent_eventSetFilterEnabled_Parms*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TimeSynthComponent_eventSetFilterEnabled_Parms), &Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::NewProp_Filter_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::NewProp_Filter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::NewProp_bIsEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Filter" },
		{ "Comment", "// Enables or disables the filter\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Enables or disables the filter" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "SetFilterEnabled", nullptr, nullptr, sizeof(TimeSynthComponent_eventSetFilterEnabled_Parms), Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics
	{
		struct TimeSynthComponent_eventSetFilterSettings_Parms
		{
			ETimeSynthFilter Filter;
			FTimeSynthFilterSettings InSettings;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Filter_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Filter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::NewProp_Filter_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::NewProp_Filter = { "Filter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetFilterSettings_Parms, Filter), Z_Construct_UEnum_TimeSynth_ETimeSynthFilter, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::NewProp_InSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::NewProp_InSettings = { "InSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetFilterSettings_Parms, InSettings), Z_Construct_UScriptStruct_FTimeSynthFilterSettings, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::NewProp_InSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::NewProp_InSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::NewProp_Filter_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::NewProp_Filter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::NewProp_InSettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Filter" },
		{ "Comment", "// Set the filter settings for the filter at the particular index\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Set the filter settings for the filter at the particular index" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "SetFilterSettings", nullptr, nullptr, sizeof(TimeSynthComponent_eventSetFilterSettings_Parms), Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics
	{
		struct TimeSynthComponent_eventSetQuantizationSettings_Parms
		{
			FTimeSynthQuantizationSettings InQuantizationSettings;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InQuantizationSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InQuantizationSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::NewProp_InQuantizationSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::NewProp_InQuantizationSettings = { "InQuantizationSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetQuantizationSettings_Parms, InQuantizationSettings), Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::NewProp_InQuantizationSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::NewProp_InQuantizationSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::NewProp_InQuantizationSettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Sets the quantization settings on the time synth\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Sets the quantization settings on the time synth" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "SetQuantizationSettings", nullptr, nullptr, sizeof(TimeSynthComponent_eventSetQuantizationSettings_Parms), Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics
	{
		struct TimeSynthComponent_eventSetSeed_Parms
		{
			int32 InSeed;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InSeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics::NewProp_InSeed = { "InSeed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetSeed_Parms, InSeed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics::NewProp_InSeed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Sets the seed of the internal random stream so choices can be repeated or controlled.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Sets the seed of the internal random stream so choices can be repeated or controlled." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "SetSeed", nullptr, nullptr, sizeof(TimeSynthComponent_eventSetSeed_Parms), Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_SetSeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_SetSeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics
	{
		struct TimeSynthComponent_eventSetVolumeGroup_Parms
		{
			UTimeSynthVolumeGroup* InVolumeGroup;
			float VolumeDb;
			float FadeTimeSec;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InVolumeGroup;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VolumeDb;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FadeTimeSec;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::NewProp_InVolumeGroup = { "InVolumeGroup", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetVolumeGroup_Parms, InVolumeGroup), Z_Construct_UClass_UTimeSynthVolumeGroup_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::NewProp_VolumeDb = { "VolumeDb", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetVolumeGroup_Parms, VolumeDb), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::NewProp_FadeTimeSec = { "FadeTimeSec", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventSetVolumeGroup_Parms, FadeTimeSec), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::NewProp_InVolumeGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::NewProp_VolumeDb,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::NewProp_FadeTimeSec,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Sets the volume (in dB) of the given volume group over the supplied FadeTime\n" },
		{ "CPP_Default_FadeTimeSec", "0.000000" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Sets the volume (in dB) of the given volume group over the supplied FadeTime" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "SetVolumeGroup", nullptr, nullptr, sizeof(TimeSynthComponent_eventSetVolumeGroup_Parms), Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics
	{
		struct TimeSynthComponent_eventStopClip_Parms
		{
			FTimeSynthClipHandle InClipHandle;
			ETimeSynthEventClipQuantization EventQuantization;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InClipHandle;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventQuantization_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EventQuantization;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::NewProp_InClipHandle = { "InClipHandle", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventStopClip_Parms, InClipHandle), Z_Construct_UScriptStruct_FTimeSynthClipHandle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::NewProp_EventQuantization_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::NewProp_EventQuantization = { "EventQuantization", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventStopClip_Parms, EventQuantization), Z_Construct_UEnum_TimeSynth_ETimeSynthEventClipQuantization, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::NewProp_InClipHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::NewProp_EventQuantization_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::NewProp_EventQuantization,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Stops the clip on the desired quantization boundary with given fade time. Uses clip's fade time.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Stops the clip on the desired quantization boundary with given fade time. Uses clip's fade time." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "StopClip", nullptr, nullptr, sizeof(TimeSynthComponent_eventStopClip_Parms), Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_StopClip()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_StopClip_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics
	{
		struct TimeSynthComponent_eventStopClipWithFadeOverride_Parms
		{
			FTimeSynthClipHandle InClipHandle;
			ETimeSynthEventClipQuantization EventQuantization;
			FTimeSynthTimeDef FadeTime;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InClipHandle;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventQuantization_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EventQuantization;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FadeTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FadeTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_InClipHandle = { "InClipHandle", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventStopClipWithFadeOverride_Parms, InClipHandle), Z_Construct_UScriptStruct_FTimeSynthClipHandle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_EventQuantization_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_EventQuantization = { "EventQuantization", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventStopClipWithFadeOverride_Parms, EventQuantization), Z_Construct_UEnum_TimeSynth_ETimeSynthEventClipQuantization, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_FadeTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_FadeTime = { "FadeTime", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventStopClipWithFadeOverride_Parms, FadeTime), Z_Construct_UScriptStruct_FTimeSynthTimeDef, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_FadeTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_FadeTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_InClipHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_EventQuantization_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_EventQuantization,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::NewProp_FadeTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Stops the clip on the desired quantization boundary with given fade time. Overrides the clip's fade time.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Stops the clip on the desired quantization boundary with given fade time. Overrides the clip's fade time." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "StopClipWithFadeOverride", nullptr, nullptr, sizeof(TimeSynthComponent_eventStopClipWithFadeOverride_Parms), Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics
	{
		struct TimeSynthComponent_eventStopSoundsOnVolumeGroup_Parms
		{
			UTimeSynthVolumeGroup* InVolumeGroup;
			ETimeSynthEventClipQuantization EventQuantization;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InVolumeGroup;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventQuantization_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EventQuantization;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::NewProp_InVolumeGroup = { "InVolumeGroup", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventStopSoundsOnVolumeGroup_Parms, InVolumeGroup), Z_Construct_UClass_UTimeSynthVolumeGroup_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::NewProp_EventQuantization_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::NewProp_EventQuantization = { "EventQuantization", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventStopSoundsOnVolumeGroup_Parms, EventQuantization), Z_Construct_UEnum_TimeSynth_ETimeSynthEventClipQuantization, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::NewProp_InVolumeGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::NewProp_EventQuantization_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::NewProp_EventQuantization,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Stops clips playing on given volume group. Clips use their fade time.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Stops clips playing on given volume group. Clips use their fade time." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "StopSoundsOnVolumeGroup", nullptr, nullptr, sizeof(TimeSynthComponent_eventStopSoundsOnVolumeGroup_Parms), Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics
	{
		struct TimeSynthComponent_eventStopSoundsOnVolumeGroupWithFadeOverride_Parms
		{
			UTimeSynthVolumeGroup* InVolumeGroup;
			ETimeSynthEventClipQuantization EventQuantization;
			FTimeSynthTimeDef FadeTime;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InVolumeGroup;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventQuantization_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EventQuantization;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FadeTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FadeTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_InVolumeGroup = { "InVolumeGroup", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventStopSoundsOnVolumeGroupWithFadeOverride_Parms, InVolumeGroup), Z_Construct_UClass_UTimeSynthVolumeGroup_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_EventQuantization_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_EventQuantization = { "EventQuantization", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventStopSoundsOnVolumeGroupWithFadeOverride_Parms, EventQuantization), Z_Construct_UEnum_TimeSynth_ETimeSynthEventClipQuantization, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_FadeTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_FadeTime = { "FadeTime", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TimeSynthComponent_eventStopSoundsOnVolumeGroupWithFadeOverride_Parms, FadeTime), Z_Construct_UScriptStruct_FTimeSynthTimeDef, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_FadeTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_FadeTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_InVolumeGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_EventQuantization_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_EventQuantization,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::NewProp_FadeTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::Function_MetaDataParams[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Stops clips playing on given volume group with the given fade time override.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Stops clips playing on given volume group with the given fade time override." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTimeSynthComponent, nullptr, "StopSoundsOnVolumeGroupWithFadeOverride", nullptr, nullptr, sizeof(TimeSynthComponent_eventStopSoundsOnVolumeGroupWithFadeOverride_Parms), Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTimeSynthComponent_NoRegister()
	{
		return UTimeSynthComponent::StaticClass();
	}
	struct Z_Construct_UClass_UTimeSynthComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_QuantizationSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_QuantizationSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableSpectralAnalysis_MetaData[];
#endif
		static void NewProp_bEnableSpectralAnalysis_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableSpectralAnalysis;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FrequenciesToAnalyze_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrequenciesToAnalyze_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FrequenciesToAnalyze;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FFTSize_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FFTSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FFTSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPlaybackTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPlaybackTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsFilterAEnabled_MetaData[];
#endif
		static void NewProp_bIsFilterAEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsFilterAEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsFilterBEnabled_MetaData[];
#endif
		static void NewProp_bIsFilterBEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsFilterBEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterASettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilterASettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterBSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilterBSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnvelopeFollowerEnabled_MetaData[];
#endif
		static void NewProp_bIsEnvelopeFollowerEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnvelopeFollowerEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnvelopeFollowerSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EnvelopeFollowerSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxPoolSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxPoolSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTimeSynthComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USynthComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_TimeSynth,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTimeSynthComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTimeSynthComponent_AddQuantizationEventDelegate, "AddQuantizationEventDelegate" }, // 4279370551
		{ &Z_Construct_UFunction_UTimeSynthComponent_GetBPM, "GetBPM" }, // 1691494375
		{ &Z_Construct_UFunction_UTimeSynthComponent_GetEnvelopeFollowerValue, "GetEnvelopeFollowerValue" }, // 4005211057
		{ &Z_Construct_UFunction_UTimeSynthComponent_GetMaxActiveClipLimit, "GetMaxActiveClipLimit" }, // 19804560
		{ &Z_Construct_UFunction_UTimeSynthComponent_GetSpectralData, "GetSpectralData" }, // 3605654417
		{ &Z_Construct_UFunction_UTimeSynthComponent_HasActiveClips, "HasActiveClips" }, // 4058318376
		{ &Z_Construct_UFunction_UTimeSynthComponent_PlayClip, "PlayClip" }, // 3623359362
		{ &Z_Construct_UFunction_UTimeSynthComponent_ResetSeed, "ResetSeed" }, // 455417724
		{ &Z_Construct_UFunction_UTimeSynthComponent_SetBPM, "SetBPM" }, // 3334630953
		{ &Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerEnabled, "SetEnvelopeFollowerEnabled" }, // 2798020121
		{ &Z_Construct_UFunction_UTimeSynthComponent_SetEnvelopeFollowerSettings, "SetEnvelopeFollowerSettings" }, // 3378708309
		{ &Z_Construct_UFunction_UTimeSynthComponent_SetFFTSize, "SetFFTSize" }, // 1599345762
		{ &Z_Construct_UFunction_UTimeSynthComponent_SetFilterEnabled, "SetFilterEnabled" }, // 1448360498
		{ &Z_Construct_UFunction_UTimeSynthComponent_SetFilterSettings, "SetFilterSettings" }, // 4233471229
		{ &Z_Construct_UFunction_UTimeSynthComponent_SetQuantizationSettings, "SetQuantizationSettings" }, // 3929645961
		{ &Z_Construct_UFunction_UTimeSynthComponent_SetSeed, "SetSeed" }, // 3528028599
		{ &Z_Construct_UFunction_UTimeSynthComponent_SetVolumeGroup, "SetVolumeGroup" }, // 808839479
		{ &Z_Construct_UFunction_UTimeSynthComponent_StopClip, "StopClip" }, // 2803009011
		{ &Z_Construct_UFunction_UTimeSynthComponent_StopClipWithFadeOverride, "StopClipWithFadeOverride" }, // 1552297948
		{ &Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroup, "StopSoundsOnVolumeGroup" }, // 308241732
		{ &Z_Construct_UFunction_UTimeSynthComponent_StopSoundsOnVolumeGroupWithFadeOverride, "StopSoundsOnVolumeGroupWithFadeOverride" }, // 1651186552
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Synth" },
		{ "HideCategories", "Object ActorComponent Physics Rendering Mobility LOD Trigger PhysicsVolume" },
		{ "IncludePath", "TimeSynthComponent.h" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_QuantizationSettings_MetaData[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// The default quantizations settings\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The default quantizations settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_QuantizationSettings = { "QuantizationSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthComponent, QuantizationSettings), Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_QuantizationSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_QuantizationSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bEnableSpectralAnalysis_MetaData[] = {
		{ "Category", "Spectral Analysis" },
		{ "Comment", "// Whether or not we are enabling spectrum analysis on the synth component. Enabling will result in FFT analysis being run.\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Whether or not we are enabling spectrum analysis on the synth component. Enabling will result in FFT analysis being run." },
	};
#endif
	void Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bEnableSpectralAnalysis_SetBit(void* Obj)
	{
		((UTimeSynthComponent*)Obj)->bEnableSpectralAnalysis = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bEnableSpectralAnalysis = { "bEnableSpectralAnalysis", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(UTimeSynthComponent), &Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bEnableSpectralAnalysis_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bEnableSpectralAnalysis_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bEnableSpectralAnalysis_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FrequenciesToAnalyze_Inner = { "FrequenciesToAnalyze", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FrequenciesToAnalyze_MetaData[] = {
		{ "Category", "Spectral Analysis" },
		{ "Comment", "// What frequencies to report magnitudes for during spectrum analysis\n" },
		{ "EditCondition", "bEnableSpectralAnalysis" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "What frequencies to report magnitudes for during spectrum analysis" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FrequenciesToAnalyze = { "FrequenciesToAnalyze", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthComponent, FrequenciesToAnalyze), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FrequenciesToAnalyze_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FrequenciesToAnalyze_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FFTSize_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FFTSize_MetaData[] = {
		{ "Category", "Spectral Analysis" },
		{ "Comment", "// What FFT bin-size to use. Smaller makes it more time-reactive but less accurate in frequency space.\n" },
		{ "EditCondition", "bEnableSpectralAnalysis" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "What FFT bin-size to use. Smaller makes it more time-reactive but less accurate in frequency space." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FFTSize = { "FFTSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthComponent, FFTSize), Z_Construct_UEnum_TimeSynth_ETimeSynthFFTSize, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FFTSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FFTSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_OnPlaybackTime_MetaData[] = {
		{ "Category", "TimeSynth" },
		{ "Comment", "// Delegate to get continuous playback time in seconds\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Delegate to get continuous playback time in seconds" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_OnPlaybackTime = { "OnPlaybackTime", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthComponent, OnPlaybackTime), Z_Construct_UDelegateFunction_TimeSynth_OnTimeSynthPlaybackTime__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_OnPlaybackTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_OnPlaybackTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterAEnabled_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// Whether or not the filter A is enabled\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Whether or not the filter A is enabled" },
	};
#endif
	void Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterAEnabled_SetBit(void* Obj)
	{
		((UTimeSynthComponent*)Obj)->bIsFilterAEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterAEnabled = { "bIsFilterAEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(UTimeSynthComponent), &Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterAEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterAEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterAEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterBEnabled_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// Whether or not the filter B is enabled\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Whether or not the filter B is enabled" },
	};
#endif
	void Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterBEnabled_SetBit(void* Obj)
	{
		((UTimeSynthComponent*)Obj)->bIsFilterBEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterBEnabled = { "bIsFilterBEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(UTimeSynthComponent), &Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterBEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterBEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterBEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FilterASettings_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The filter settings to use for filter A\n" },
		{ "EditCondition", "bIsFilterAEnabled" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The filter settings to use for filter A" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FilterASettings = { "FilterASettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthComponent, FilterASettings), Z_Construct_UScriptStruct_FTimeSynthFilterSettings, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FilterASettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FilterASettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FilterBSettings_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The filter settings to use for filter B\n" },
		{ "EditCondition", "bIsFilterBEnabled" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The filter settings to use for filter B" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FilterBSettings = { "FilterBSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthComponent, FilterBSettings), Z_Construct_UScriptStruct_FTimeSynthFilterSettings, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FilterBSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FilterBSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsEnvelopeFollowerEnabled_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// Whether or not the filter is enabled\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Whether or not the filter is enabled" },
	};
#endif
	void Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsEnvelopeFollowerEnabled_SetBit(void* Obj)
	{
		((UTimeSynthComponent*)Obj)->bIsEnvelopeFollowerEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsEnvelopeFollowerEnabled = { "bIsEnvelopeFollowerEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(UTimeSynthComponent), &Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsEnvelopeFollowerEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsEnvelopeFollowerEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsEnvelopeFollowerEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_EnvelopeFollowerSettings_MetaData[] = {
		{ "Category", "Filter" },
		{ "Comment", "// The envelope follower settings to use\n" },
		{ "EditCondition", "bisEnvelopeFollowerEnabled" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "The envelope follower settings to use" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_EnvelopeFollowerSettings = { "EnvelopeFollowerSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthComponent, EnvelopeFollowerSettings), Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings, METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_EnvelopeFollowerSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_EnvelopeFollowerSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_MaxPoolSize_MetaData[] = {
		{ "Category", "TimeSynth" },
		{ "ClampMin", "20" },
		{ "Comment", "// Set the Max Pool Size\n" },
		{ "ModuleRelativePath", "Classes/TimeSynthComponent.h" },
		{ "ToolTip", "Set the Max Pool Size" },
		{ "UIMax", "50" },
		{ "UIMin", "20" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_MaxPoolSize = { "MaxPoolSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimeSynthComponent, MaxPoolSize), METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_MaxPoolSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_MaxPoolSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTimeSynthComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_QuantizationSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bEnableSpectralAnalysis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FrequenciesToAnalyze_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FrequenciesToAnalyze,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FFTSize_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FFTSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_OnPlaybackTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterAEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsFilterBEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FilterASettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_FilterBSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_bIsEnvelopeFollowerEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_EnvelopeFollowerSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimeSynthComponent_Statics::NewProp_MaxPoolSize,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTimeSynthComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTimeSynthComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTimeSynthComponent_Statics::ClassParams = {
		&UTimeSynthComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UTimeSynthComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTimeSynthComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTimeSynthComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTimeSynthComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTimeSynthComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTimeSynthComponent, 1989214422);
	template<> TIMESYNTH_API UClass* StaticClass<UTimeSynthComponent>()
	{
		return UTimeSynthComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTimeSynthComponent(Z_Construct_UClass_UTimeSynthComponent, &UTimeSynthComponent::StaticClass, TEXT("/Script/TimeSynth"), TEXT("UTimeSynthComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTimeSynthComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
