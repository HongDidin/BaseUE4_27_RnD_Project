// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TIMESYNTHEDITOR_TimeSynthClip_generated_h
#error "TimeSynthClip.generated.h already included, missing '#pragma once' in TimeSynthClip.h"
#endif
#define TIMESYNTHEDITOR_TimeSynthClip_generated_h

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_SPARSE_DATA
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_RPC_WRAPPERS
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTimeSynthClipFactory(); \
	friend struct Z_Construct_UClass_UTimeSynthClipFactory_Statics; \
public: \
	DECLARE_CLASS(UTimeSynthClipFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TimeSynthEditor"), TIMESYNTHEDITOR_API) \
	DECLARE_SERIALIZER(UTimeSynthClipFactory)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUTimeSynthClipFactory(); \
	friend struct Z_Construct_UClass_UTimeSynthClipFactory_Statics; \
public: \
	DECLARE_CLASS(UTimeSynthClipFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TimeSynthEditor"), TIMESYNTHEDITOR_API) \
	DECLARE_SERIALIZER(UTimeSynthClipFactory)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TIMESYNTHEDITOR_API UTimeSynthClipFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimeSynthClipFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TIMESYNTHEDITOR_API, UTimeSynthClipFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimeSynthClipFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TIMESYNTHEDITOR_API UTimeSynthClipFactory(UTimeSynthClipFactory&&); \
	TIMESYNTHEDITOR_API UTimeSynthClipFactory(const UTimeSynthClipFactory&); \
public:


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TIMESYNTHEDITOR_API UTimeSynthClipFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TIMESYNTHEDITOR_API UTimeSynthClipFactory(UTimeSynthClipFactory&&); \
	TIMESYNTHEDITOR_API UTimeSynthClipFactory(const UTimeSynthClipFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TIMESYNTHEDITOR_API, UTimeSynthClipFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimeSynthClipFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimeSynthClipFactory)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_24_PROLOG
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_SPARSE_DATA \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_RPC_WRAPPERS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_INCLASS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_SPARSE_DATA \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h_27_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class TimeSynthClipFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMESYNTHEDITOR_API UClass* StaticClass<class UTimeSynthClipFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_TimeSynth_Source_TimeSynthEditor_Classes_TimeSynthClip_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
