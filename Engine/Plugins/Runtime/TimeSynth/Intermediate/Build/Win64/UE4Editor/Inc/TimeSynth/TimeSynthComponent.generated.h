// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class ETimeSynthEventQuantization : uint8;
enum class ETimeSynthFFTSize : uint8;
enum class ETimeSynthFilter : uint8;
struct FTimeSynthEnvelopeFollowerSettings;
struct FTimeSynthFilterSettings;
struct FTimeSynthSpectralData;
class UTimeSynthVolumeGroup;
enum class ETimeSynthEventClipQuantization : uint8;
struct FTimeSynthTimeDef;
struct FTimeSynthClipHandle;
class UTimeSynthClip;
struct FTimeSynthQuantizationSettings;
#ifdef TIMESYNTH_TimeSynthComponent_generated_h
#error "TimeSynthComponent.generated.h already included, missing '#pragma once' in TimeSynthComponent.h"
#endif
#define TIMESYNTH_TimeSynthComponent_generated_h

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_324_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimeSynthEnvelopeFollowerSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> TIMESYNTH_API UScriptStruct* StaticStruct<struct FTimeSynthEnvelopeFollowerSettings>();

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_297_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimeSynthFilterSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> TIMESYNTH_API UScriptStruct* StaticStruct<struct FTimeSynthFilterSettings>();

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_209_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimeSynthClipSound_Statics; \
	static class UScriptStruct* StaticStruct();


template<> TIMESYNTH_API UScriptStruct* StaticStruct<struct FTimeSynthClipSound>();

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_190_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimeSynthClipHandle_Statics; \
	static class UScriptStruct* StaticStruct();


template<> TIMESYNTH_API UScriptStruct* StaticStruct<struct FTimeSynthClipHandle>();

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_159_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimeSynthTimeDef_Statics; \
	static class UScriptStruct* StaticStruct();


template<> TIMESYNTH_API UScriptStruct* StaticStruct<struct FTimeSynthTimeDef>();

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_132_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimeSynthQuantizationSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> TIMESYNTH_API UScriptStruct* StaticStruct<struct FTimeSynthQuantizationSettings>();

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_106_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimeSynthSpectralData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> TIMESYNTH_API UScriptStruct* StaticStruct<struct FTimeSynthSpectralData>();

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_125_DELEGATE \
struct _Script_TimeSynth_eventOnQuantizationEventBP_Parms \
{ \
	ETimeSynthEventQuantization QuantizationType; \
	int32 NumBars; \
	float Beat; \
}; \
static inline void FOnQuantizationEventBP_DelegateWrapper(const FScriptDelegate& OnQuantizationEventBP, ETimeSynthEventQuantization QuantizationType, int32 NumBars, float Beat) \
{ \
	_Script_TimeSynth_eventOnQuantizationEventBP_Parms Parms; \
	Parms.QuantizationType=QuantizationType; \
	Parms.NumBars=NumBars; \
	Parms.Beat=Beat; \
	OnQuantizationEventBP.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_123_DELEGATE \
struct _Script_TimeSynth_eventOnQuantizationEvent_Parms \
{ \
	ETimeSynthEventQuantization QuantizationType; \
	int32 NumBars; \
	float Beat; \
}; \
static inline void FOnQuantizationEvent_DelegateWrapper(const FMulticastScriptDelegate& OnQuantizationEvent, ETimeSynthEventQuantization QuantizationType, int32 NumBars, float Beat) \
{ \
	_Script_TimeSynth_eventOnQuantizationEvent_Parms Parms; \
	Parms.QuantizationType=QuantizationType; \
	Parms.NumBars=NumBars; \
	Parms.Beat=Beat; \
	OnQuantizationEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_119_DELEGATE \
struct _Script_TimeSynth_eventOnTimeSynthPlaybackTime_Parms \
{ \
	float SynthPlaybackTimeSeconds; \
}; \
static inline void FOnTimeSynthPlaybackTime_DelegateWrapper(const FMulticastScriptDelegate& OnTimeSynthPlaybackTime, float SynthPlaybackTimeSeconds) \
{ \
	_Script_TimeSynth_eventOnTimeSynthPlaybackTime_Parms Parms; \
	Parms.SynthPlaybackTimeSeconds=SynthPlaybackTimeSeconds; \
	OnTimeSynthPlaybackTime.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_SPARSE_DATA
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_RPC_WRAPPERS
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTimeSynthVolumeGroup(); \
	friend struct Z_Construct_UClass_UTimeSynthVolumeGroup_Statics; \
public: \
	DECLARE_CLASS(UTimeSynthVolumeGroup, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TimeSynth"), NO_API) \
	DECLARE_SERIALIZER(UTimeSynthVolumeGroup)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_INCLASS \
private: \
	static void StaticRegisterNativesUTimeSynthVolumeGroup(); \
	friend struct Z_Construct_UClass_UTimeSynthVolumeGroup_Statics; \
public: \
	DECLARE_CLASS(UTimeSynthVolumeGroup, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TimeSynth"), NO_API) \
	DECLARE_SERIALIZER(UTimeSynthVolumeGroup)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTimeSynthVolumeGroup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimeSynthVolumeGroup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimeSynthVolumeGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimeSynthVolumeGroup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimeSynthVolumeGroup(UTimeSynthVolumeGroup&&); \
	NO_API UTimeSynthVolumeGroup(const UTimeSynthVolumeGroup&); \
public:


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTimeSynthVolumeGroup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimeSynthVolumeGroup(UTimeSynthVolumeGroup&&); \
	NO_API UTimeSynthVolumeGroup(const UTimeSynthVolumeGroup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimeSynthVolumeGroup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimeSynthVolumeGroup); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimeSynthVolumeGroup)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_224_PROLOG
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_SPARSE_DATA \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_RPC_WRAPPERS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_INCLASS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_SPARSE_DATA \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_227_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMESYNTH_API UClass* StaticClass<class UTimeSynthVolumeGroup>();

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_SPARSE_DATA
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_RPC_WRAPPERS
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTimeSynthClip(); \
	friend struct Z_Construct_UClass_UTimeSynthClip_Statics; \
public: \
	DECLARE_CLASS(UTimeSynthClip, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TimeSynth"), NO_API) \
	DECLARE_SERIALIZER(UTimeSynthClip)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_INCLASS \
private: \
	static void StaticRegisterNativesUTimeSynthClip(); \
	friend struct Z_Construct_UClass_UTimeSynthClip_Statics; \
public: \
	DECLARE_CLASS(UTimeSynthClip, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TimeSynth"), NO_API) \
	DECLARE_SERIALIZER(UTimeSynthClip)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTimeSynthClip(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimeSynthClip) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimeSynthClip); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimeSynthClip); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimeSynthClip(UTimeSynthClip&&); \
	NO_API UTimeSynthClip(const UTimeSynthClip&); \
public:


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimeSynthClip(UTimeSynthClip&&); \
	NO_API UTimeSynthClip(const UTimeSynthClip&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimeSynthClip); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimeSynthClip); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTimeSynthClip)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_236_PROLOG
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_SPARSE_DATA \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_RPC_WRAPPERS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_INCLASS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_SPARSE_DATA \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_239_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMESYNTH_API UClass* StaticClass<class UTimeSynthClip>();

#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_SPARSE_DATA
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHasActiveClips); \
	DECLARE_FUNCTION(execSetFFTSize); \
	DECLARE_FUNCTION(execSetEnvelopeFollowerEnabled); \
	DECLARE_FUNCTION(execSetFilterEnabled); \
	DECLARE_FUNCTION(execSetEnvelopeFollowerSettings); \
	DECLARE_FUNCTION(execSetFilterSettings); \
	DECLARE_FUNCTION(execAddQuantizationEventDelegate); \
	DECLARE_FUNCTION(execGetMaxActiveClipLimit); \
	DECLARE_FUNCTION(execGetEnvelopeFollowerValue); \
	DECLARE_FUNCTION(execGetSpectralData); \
	DECLARE_FUNCTION(execStopSoundsOnVolumeGroupWithFadeOverride); \
	DECLARE_FUNCTION(execStopSoundsOnVolumeGroup); \
	DECLARE_FUNCTION(execSetVolumeGroup); \
	DECLARE_FUNCTION(execStopClipWithFadeOverride); \
	DECLARE_FUNCTION(execStopClip); \
	DECLARE_FUNCTION(execPlayClip); \
	DECLARE_FUNCTION(execResetSeed); \
	DECLARE_FUNCTION(execSetSeed); \
	DECLARE_FUNCTION(execGetBPM); \
	DECLARE_FUNCTION(execSetBPM); \
	DECLARE_FUNCTION(execSetQuantizationSettings);


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHasActiveClips); \
	DECLARE_FUNCTION(execSetFFTSize); \
	DECLARE_FUNCTION(execSetEnvelopeFollowerEnabled); \
	DECLARE_FUNCTION(execSetFilterEnabled); \
	DECLARE_FUNCTION(execSetEnvelopeFollowerSettings); \
	DECLARE_FUNCTION(execSetFilterSettings); \
	DECLARE_FUNCTION(execAddQuantizationEventDelegate); \
	DECLARE_FUNCTION(execGetMaxActiveClipLimit); \
	DECLARE_FUNCTION(execGetEnvelopeFollowerValue); \
	DECLARE_FUNCTION(execGetSpectralData); \
	DECLARE_FUNCTION(execStopSoundsOnVolumeGroupWithFadeOverride); \
	DECLARE_FUNCTION(execStopSoundsOnVolumeGroup); \
	DECLARE_FUNCTION(execSetVolumeGroup); \
	DECLARE_FUNCTION(execStopClipWithFadeOverride); \
	DECLARE_FUNCTION(execStopClip); \
	DECLARE_FUNCTION(execPlayClip); \
	DECLARE_FUNCTION(execResetSeed); \
	DECLARE_FUNCTION(execSetSeed); \
	DECLARE_FUNCTION(execGetBPM); \
	DECLARE_FUNCTION(execSetBPM); \
	DECLARE_FUNCTION(execSetQuantizationSettings);


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTimeSynthComponent(); \
	friend struct Z_Construct_UClass_UTimeSynthComponent_Statics; \
public: \
	DECLARE_CLASS(UTimeSynthComponent, USynthComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimeSynth"), NO_API) \
	DECLARE_SERIALIZER(UTimeSynthComponent)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_INCLASS \
private: \
	static void StaticRegisterNativesUTimeSynthComponent(); \
	friend struct Z_Construct_UClass_UTimeSynthComponent_Statics; \
public: \
	DECLARE_CLASS(UTimeSynthComponent, USynthComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimeSynth"), NO_API) \
	DECLARE_SERIALIZER(UTimeSynthComponent)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTimeSynthComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimeSynthComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimeSynthComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimeSynthComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimeSynthComponent(UTimeSynthComponent&&); \
	NO_API UTimeSynthComponent(const UTimeSynthComponent&); \
public:


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimeSynthComponent(UTimeSynthComponent&&); \
	NO_API UTimeSynthComponent(const UTimeSynthComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimeSynthComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimeSynthComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimeSynthComponent)


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_400_PROLOG
#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_SPARSE_DATA \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_RPC_WRAPPERS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_INCLASS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_SPARSE_DATA \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h_403_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMESYNTH_API UClass* StaticClass<class UTimeSynthComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_TimeSynth_Source_TimeSynth_Classes_TimeSynthComponent_h


#define FOREACH_ENUM_ETIMESYNTHENVELOPEFOLLOWERPEAKMODE(op) \
	op(ETimeSynthEnvelopeFollowerPeakMode::MeanSquared) \
	op(ETimeSynthEnvelopeFollowerPeakMode::RootMeanSquared) \
	op(ETimeSynthEnvelopeFollowerPeakMode::Peak) \
	op(ETimeSynthEnvelopeFollowerPeakMode::Count) 

enum class ETimeSynthEnvelopeFollowerPeakMode : uint8;
template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthEnvelopeFollowerPeakMode>();

#define FOREACH_ENUM_ETIMESYNTHFILTERTYPE(op) \
	op(ETimeSynthFilterType::LowPass) \
	op(ETimeSynthFilterType::HighPass) \
	op(ETimeSynthFilterType::BandPass) \
	op(ETimeSynthFilterType::BandStop) \
	op(ETimeSynthFilterType::Count) 

enum class ETimeSynthFilterType : uint8;
template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthFilterType>();

#define FOREACH_ENUM_ETIMESYNTHFILTER(op) \
	op(ETimeSynthFilter::FilterA) \
	op(ETimeSynthFilter::FilterB) \
	op(ETimeSynthFilter::Count) 

enum class ETimeSynthFilter : uint8;
template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthFilter>();

#define FOREACH_ENUM_ETIMESYNTHEVENTQUANTIZATION(op) \
	op(ETimeSynthEventQuantization::None) \
	op(ETimeSynthEventQuantization::Bars8) \
	op(ETimeSynthEventQuantization::Bars4) \
	op(ETimeSynthEventQuantization::Bars2) \
	op(ETimeSynthEventQuantization::Bar) \
	op(ETimeSynthEventQuantization::HalfNote) \
	op(ETimeSynthEventQuantization::HalfNoteTriplet) \
	op(ETimeSynthEventQuantization::QuarterNote) \
	op(ETimeSynthEventQuantization::QuarterNoteTriplet) \
	op(ETimeSynthEventQuantization::EighthNote) \
	op(ETimeSynthEventQuantization::EighthNoteTriplet) \
	op(ETimeSynthEventQuantization::SixteenthNote) \
	op(ETimeSynthEventQuantization::SixteenthNoteTriplet) \
	op(ETimeSynthEventQuantization::ThirtySecondNote) \
	op(ETimeSynthEventQuantization::Count) 

enum class ETimeSynthEventQuantization : uint8;
template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthEventQuantization>();

#define FOREACH_ENUM_ETIMESYNTHEVENTCLIPQUANTIZATION(op) \
	op(ETimeSynthEventClipQuantization::Global) \
	op(ETimeSynthEventClipQuantization::None) \
	op(ETimeSynthEventClipQuantization::Bars8) \
	op(ETimeSynthEventClipQuantization::Bars4) \
	op(ETimeSynthEventClipQuantization::Bars2) \
	op(ETimeSynthEventClipQuantization::Bar) \
	op(ETimeSynthEventClipQuantization::HalfNote) \
	op(ETimeSynthEventClipQuantization::HalfNoteTriplet) \
	op(ETimeSynthEventClipQuantization::QuarterNote) \
	op(ETimeSynthEventClipQuantization::QuarterNoteTriplet) \
	op(ETimeSynthEventClipQuantization::EighthNote) \
	op(ETimeSynthEventClipQuantization::EighthNoteTriplet) \
	op(ETimeSynthEventClipQuantization::SixteenthNote) \
	op(ETimeSynthEventClipQuantization::SixteenthNoteTriplet) \
	op(ETimeSynthEventClipQuantization::ThirtySecondNote) \
	op(ETimeSynthEventClipQuantization::Count) 

enum class ETimeSynthEventClipQuantization : uint8;
template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthEventClipQuantization>();

#define FOREACH_ENUM_ETIMESYNTHFFTSIZE(op) \
	op(ETimeSynthFFTSize::Min_64) \
	op(ETimeSynthFFTSize::Small_256) \
	op(ETimeSynthFFTSize::Medium_512) \
	op(ETimeSynthFFTSize::Large_1024) 

enum class ETimeSynthFFTSize : uint8;
template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthFFTSize>();

#define FOREACH_ENUM_ETIMESYNTHBEATDIVISION(op) \
	op(ETimeSynthBeatDivision::One) \
	op(ETimeSynthBeatDivision::Two) \
	op(ETimeSynthBeatDivision::Four) \
	op(ETimeSynthBeatDivision::Eight) \
	op(ETimeSynthBeatDivision::Sixteen) \
	op(ETimeSynthBeatDivision::Count) 

enum class ETimeSynthBeatDivision : uint8;
template<> TIMESYNTH_API UEnum* StaticEnum<ETimeSynthBeatDivision>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
