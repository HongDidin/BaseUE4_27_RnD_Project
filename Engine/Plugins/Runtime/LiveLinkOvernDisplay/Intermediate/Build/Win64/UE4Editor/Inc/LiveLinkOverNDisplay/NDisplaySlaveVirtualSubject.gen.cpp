// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkOverNDisplay/Private/NDisplaySlaveVirtualSubject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNDisplaySlaveVirtualSubject() {}
// Cross Module References
	LIVELINKOVERNDISPLAY_API UClass* Z_Construct_UClass_UNDisplaySlaveVirtualSubject_NoRegister();
	LIVELINKOVERNDISPLAY_API UClass* Z_Construct_UClass_UNDisplaySlaveVirtualSubject();
	LIVELINKINTERFACE_API UClass* Z_Construct_UClass_ULiveLinkVirtualSubject();
	UPackage* Z_Construct_UPackage__Script_LiveLinkOverNDisplay();
// End Cross Module References
	void UNDisplaySlaveVirtualSubject::StaticRegisterNativesUNDisplaySlaveVirtualSubject()
	{
	}
	UClass* Z_Construct_UClass_UNDisplaySlaveVirtualSubject_NoRegister()
	{
		return UNDisplaySlaveVirtualSubject::StaticClass();
	}
	struct Z_Construct_UClass_UNDisplaySlaveVirtualSubject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNDisplaySlaveVirtualSubject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkVirtualSubject,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkOverNDisplay,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNDisplaySlaveVirtualSubject_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * LiveLink VirtualSubject used on nDisplay slaves to replicate real subjects\n * Master sends data to use for each frame to make sure all machines display / uses the same data\n * Uses Replicator object that this module creates.\n */" },
		{ "IncludePath", "NDisplaySlaveVirtualSubject.h" },
		{ "ModuleRelativePath", "Private/NDisplaySlaveVirtualSubject.h" },
		{ "ToolTip", "LiveLink VirtualSubject used on nDisplay slaves to replicate real subjects\nMaster sends data to use for each frame to make sure all machines display / uses the same data\nUses Replicator object that this module creates." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNDisplaySlaveVirtualSubject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNDisplaySlaveVirtualSubject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNDisplaySlaveVirtualSubject_Statics::ClassParams = {
		&UNDisplaySlaveVirtualSubject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNDisplaySlaveVirtualSubject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNDisplaySlaveVirtualSubject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNDisplaySlaveVirtualSubject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNDisplaySlaveVirtualSubject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNDisplaySlaveVirtualSubject, 3142637660);
	template<> LIVELINKOVERNDISPLAY_API UClass* StaticClass<UNDisplaySlaveVirtualSubject>()
	{
		return UNDisplaySlaveVirtualSubject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNDisplaySlaveVirtualSubject(Z_Construct_UClass_UNDisplaySlaveVirtualSubject, &UNDisplaySlaveVirtualSubject::StaticClass, TEXT("/Script/LiveLinkOverNDisplay"), TEXT("UNDisplaySlaveVirtualSubject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNDisplaySlaveVirtualSubject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
