// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKOVERNDISPLAY_NDisplaySlaveVirtualSubject_generated_h
#error "NDisplaySlaveVirtualSubject.generated.h already included, missing '#pragma once' in NDisplaySlaveVirtualSubject.h"
#endif
#define LIVELINKOVERNDISPLAY_NDisplaySlaveVirtualSubject_generated_h

#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_SPARSE_DATA
#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_RPC_WRAPPERS
#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNDisplaySlaveVirtualSubject(); \
	friend struct Z_Construct_UClass_UNDisplaySlaveVirtualSubject_Statics; \
public: \
	DECLARE_CLASS(UNDisplaySlaveVirtualSubject, ULiveLinkVirtualSubject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkOverNDisplay"), NO_API) \
	DECLARE_SERIALIZER(UNDisplaySlaveVirtualSubject)


#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUNDisplaySlaveVirtualSubject(); \
	friend struct Z_Construct_UClass_UNDisplaySlaveVirtualSubject_Statics; \
public: \
	DECLARE_CLASS(UNDisplaySlaveVirtualSubject, ULiveLinkVirtualSubject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkOverNDisplay"), NO_API) \
	DECLARE_SERIALIZER(UNDisplaySlaveVirtualSubject)


#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNDisplaySlaveVirtualSubject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNDisplaySlaveVirtualSubject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNDisplaySlaveVirtualSubject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNDisplaySlaveVirtualSubject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNDisplaySlaveVirtualSubject(UNDisplaySlaveVirtualSubject&&); \
	NO_API UNDisplaySlaveVirtualSubject(const UNDisplaySlaveVirtualSubject&); \
public:


#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNDisplaySlaveVirtualSubject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNDisplaySlaveVirtualSubject(UNDisplaySlaveVirtualSubject&&); \
	NO_API UNDisplaySlaveVirtualSubject(const UNDisplaySlaveVirtualSubject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNDisplaySlaveVirtualSubject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNDisplaySlaveVirtualSubject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNDisplaySlaveVirtualSubject)


#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_17_PROLOG
#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_SPARSE_DATA \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_RPC_WRAPPERS \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_INCLASS \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_SPARSE_DATA \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKOVERNDISPLAY_API UClass* StaticClass<class UNDisplaySlaveVirtualSubject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Private_NDisplaySlaveVirtualSubject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
