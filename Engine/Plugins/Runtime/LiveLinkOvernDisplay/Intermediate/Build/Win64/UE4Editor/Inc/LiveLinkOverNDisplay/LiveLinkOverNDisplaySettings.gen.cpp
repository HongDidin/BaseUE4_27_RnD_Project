// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkOverNDisplay/Public/LiveLinkOverNDisplaySettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkOverNDisplaySettings() {}
// Cross Module References
	LIVELINKOVERNDISPLAY_API UClass* Z_Construct_UClass_ULiveLinkOverNDisplaySettings_NoRegister();
	LIVELINKOVERNDISPLAY_API UClass* Z_Construct_UClass_ULiveLinkOverNDisplaySettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LiveLinkOverNDisplay();
// End Cross Module References
	void ULiveLinkOverNDisplaySettings::StaticRegisterNativesULiveLinkOverNDisplaySettings()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkOverNDisplaySettings_NoRegister()
	{
		return ULiveLinkOverNDisplaySettings::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkOverNDisplay,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LiveLinkOverNDisplaySettings.h" },
		{ "ModuleRelativePath", "Public/LiveLinkOverNDisplaySettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "LiveLinkOverNDisplay Settings" },
		{ "Comment", "/** \n\x09 * Enables nDisplay specific LiveLink subjects management across nDisplay cluster. \n\x09 * @note Can be overrided via the command line using -EnableLiveLinkOverNDisplay=false\n\x09 */" },
		{ "ModuleRelativePath", "Public/LiveLinkOverNDisplaySettings.h" },
		{ "ToolTip", "Enables nDisplay specific LiveLink subjects management across nDisplay cluster.\n@note Can be overrided via the command line using -EnableLiveLinkOverNDisplay=false" },
	};
#endif
	void Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((ULiveLinkOverNDisplaySettings*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0020080000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULiveLinkOverNDisplaySettings), &Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::NewProp_bIsEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::NewProp_bIsEnabled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkOverNDisplaySettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::ClassParams = {
		&ULiveLinkOverNDisplaySettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkOverNDisplaySettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkOverNDisplaySettings, 1345734092);
	template<> LIVELINKOVERNDISPLAY_API UClass* StaticClass<ULiveLinkOverNDisplaySettings>()
	{
		return ULiveLinkOverNDisplaySettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkOverNDisplaySettings(Z_Construct_UClass_ULiveLinkOverNDisplaySettings, &ULiveLinkOverNDisplaySettings::StaticClass, TEXT("/Script/LiveLinkOverNDisplay"), TEXT("ULiveLinkOverNDisplaySettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkOverNDisplaySettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
