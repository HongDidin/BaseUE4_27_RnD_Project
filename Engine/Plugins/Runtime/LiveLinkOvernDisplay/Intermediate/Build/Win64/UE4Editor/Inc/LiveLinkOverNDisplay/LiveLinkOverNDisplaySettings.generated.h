// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKOVERNDISPLAY_LiveLinkOverNDisplaySettings_generated_h
#error "LiveLinkOverNDisplaySettings.generated.h already included, missing '#pragma once' in LiveLinkOverNDisplaySettings.h"
#endif
#define LIVELINKOVERNDISPLAY_LiveLinkOverNDisplaySettings_generated_h

#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_SPARSE_DATA
#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_RPC_WRAPPERS
#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkOverNDisplaySettings(); \
	friend struct Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkOverNDisplaySettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LiveLinkOverNDisplay"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkOverNDisplaySettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkOverNDisplaySettings(); \
	friend struct Z_Construct_UClass_ULiveLinkOverNDisplaySettings_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkOverNDisplaySettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LiveLinkOverNDisplay"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkOverNDisplaySettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkOverNDisplaySettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkOverNDisplaySettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkOverNDisplaySettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkOverNDisplaySettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkOverNDisplaySettings(ULiveLinkOverNDisplaySettings&&); \
	NO_API ULiveLinkOverNDisplaySettings(const ULiveLinkOverNDisplaySettings&); \
public:


#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkOverNDisplaySettings(ULiveLinkOverNDisplaySettings&&); \
	NO_API ULiveLinkOverNDisplaySettings(const ULiveLinkOverNDisplaySettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkOverNDisplaySettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkOverNDisplaySettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULiveLinkOverNDisplaySettings)


#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bIsEnabled() { return STRUCT_OFFSET(ULiveLinkOverNDisplaySettings, bIsEnabled); }


#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_9_PROLOG
#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_RPC_WRAPPERS \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_INCLASS \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKOVERNDISPLAY_API UClass* StaticClass<class ULiveLinkOverNDisplaySettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_LiveLinkOvernDisplay_Source_LiveLinkOverNDisplay_Public_LiveLinkOverNDisplaySettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
