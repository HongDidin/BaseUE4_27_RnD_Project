// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SYNTHESISEDITOR_EpicSynth1PresetBank_generated_h
#error "EpicSynth1PresetBank.generated.h already included, missing '#pragma once' in EpicSynth1PresetBank.h"
#endif
#define SYNTHESISEDITOR_EpicSynth1PresetBank_generated_h

#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_SPARSE_DATA
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUModularSynthPresetBankFactory(); \
	friend struct Z_Construct_UClass_UModularSynthPresetBankFactory_Statics; \
public: \
	DECLARE_CLASS(UModularSynthPresetBankFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SynthesisEditor"), SYNTHESISEDITOR_API) \
	DECLARE_SERIALIZER(UModularSynthPresetBankFactory)


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUModularSynthPresetBankFactory(); \
	friend struct Z_Construct_UClass_UModularSynthPresetBankFactory_Statics; \
public: \
	DECLARE_CLASS(UModularSynthPresetBankFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SynthesisEditor"), SYNTHESISEDITOR_API) \
	DECLARE_SERIALIZER(UModularSynthPresetBankFactory)


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SYNTHESISEDITOR_API UModularSynthPresetBankFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UModularSynthPresetBankFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SYNTHESISEDITOR_API, UModularSynthPresetBankFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UModularSynthPresetBankFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SYNTHESISEDITOR_API UModularSynthPresetBankFactory(UModularSynthPresetBankFactory&&); \
	SYNTHESISEDITOR_API UModularSynthPresetBankFactory(const UModularSynthPresetBankFactory&); \
public:


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SYNTHESISEDITOR_API UModularSynthPresetBankFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SYNTHESISEDITOR_API UModularSynthPresetBankFactory(UModularSynthPresetBankFactory&&); \
	SYNTHESISEDITOR_API UModularSynthPresetBankFactory(const UModularSynthPresetBankFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SYNTHESISEDITOR_API, UModularSynthPresetBankFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UModularSynthPresetBankFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UModularSynthPresetBankFactory)


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_22_PROLOG
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_INCLASS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h_25_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ModularSynthPresetBankFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SYNTHESISEDITOR_API UClass* StaticClass<class UModularSynthPresetBankFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_EpicSynth1PresetBank_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
