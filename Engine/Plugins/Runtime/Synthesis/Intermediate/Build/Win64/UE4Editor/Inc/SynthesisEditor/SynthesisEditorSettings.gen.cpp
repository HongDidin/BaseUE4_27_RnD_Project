// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SynthesisEditor/Classes/SynthesisEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSynthesisEditorSettings() {}
// Cross Module References
	SYNTHESISEDITOR_API UClass* Z_Construct_UClass_USynthesisEditorSettings_NoRegister();
	SYNTHESISEDITOR_API UClass* Z_Construct_UClass_USynthesisEditorSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_SynthesisEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
// End Cross Module References
	void USynthesisEditorSettings::StaticRegisterNativesUSynthesisEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_USynthesisEditorSettings_NoRegister()
	{
		return USynthesisEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_USynthesisEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BitCrusherWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BitCrusherWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChorusWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChorusWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicsProcessorWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DynamicsProcessorWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnvelopeFollowerWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EnvelopeFollowerWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EQWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EQWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilterWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoldbackDistortionWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FoldbackDistortionWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MidSideSpreaderWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MidSideSpreaderWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PannerWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PannerWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhaserWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PhaserWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RingModulationWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RingModulationWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimpleDelayWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SimpleDelayWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StereoDelayWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StereoDelayWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveShaperWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaveShaperWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubmixConvolutionReverbWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SubmixConvolutionReverbWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubmixDelayWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SubmixDelayWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubmixFilterWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SubmixFilterWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubmixFlexiverbWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SubmixFlexiverbWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubmixStereoDelayWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SubmixStereoDelayWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubmixTapDelayWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SubmixTapDelayWidget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USynthesisEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_SynthesisEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Synthesis" },
		{ "IncludePath", "SynthesisEditorSettings.h" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_BitCrusherWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_BitCrusherWidget = { "BitCrusherWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, BitCrusherWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_BitCrusherWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_BitCrusherWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_ChorusWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_ChorusWidget = { "ChorusWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, ChorusWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_ChorusWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_ChorusWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_DynamicsProcessorWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_DynamicsProcessorWidget = { "DynamicsProcessorWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, DynamicsProcessorWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_DynamicsProcessorWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_DynamicsProcessorWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_EnvelopeFollowerWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_EnvelopeFollowerWidget = { "EnvelopeFollowerWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, EnvelopeFollowerWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_EnvelopeFollowerWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_EnvelopeFollowerWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_EQWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_EQWidget = { "EQWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, EQWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_EQWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_EQWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_FilterWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "DisplayName", "Filter Widget (Source)" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_FilterWidget = { "FilterWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, FilterWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_FilterWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_FilterWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_FoldbackDistortionWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_FoldbackDistortionWidget = { "FoldbackDistortionWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, FoldbackDistortionWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_FoldbackDistortionWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_FoldbackDistortionWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_MidSideSpreaderWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_MidSideSpreaderWidget = { "MidSideSpreaderWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, MidSideSpreaderWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_MidSideSpreaderWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_MidSideSpreaderWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_PannerWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_PannerWidget = { "PannerWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, PannerWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_PannerWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_PannerWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_PhaserWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_PhaserWidget = { "PhaserWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, PhaserWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_PhaserWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_PhaserWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_RingModulationWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_RingModulationWidget = { "RingModulationWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, RingModulationWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_RingModulationWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_RingModulationWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SimpleDelayWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SimpleDelayWidget = { "SimpleDelayWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, SimpleDelayWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SimpleDelayWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SimpleDelayWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_StereoDelayWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_StereoDelayWidget = { "StereoDelayWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, StereoDelayWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_StereoDelayWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_StereoDelayWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_WaveShaperWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Source Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_WaveShaperWidget = { "WaveShaperWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, WaveShaperWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_WaveShaperWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_WaveShaperWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixConvolutionReverbWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Submix Effects" },
		{ "DisplayName", "Convolution Reverb Widget" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixConvolutionReverbWidget = { "SubmixConvolutionReverbWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, SubmixConvolutionReverbWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixConvolutionReverbWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixConvolutionReverbWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixDelayWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Submix Effects" },
		{ "DisplayName", "Delay Widget (Submix)" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixDelayWidget = { "SubmixDelayWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, SubmixDelayWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixDelayWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixDelayWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixFilterWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Submix Effects" },
		{ "DisplayName", "Filter Widget (Submix)" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixFilterWidget = { "SubmixFilterWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, SubmixFilterWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixFilterWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixFilterWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixFlexiverbWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Submix Effects" },
		{ "DisplayName", "Flexiverb Widget" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixFlexiverbWidget = { "SubmixFlexiverbWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, SubmixFlexiverbWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixFlexiverbWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixFlexiverbWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixStereoDelayWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Submix Effects" },
		{ "DisplayName", "Stereo Delay (Submix)" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixStereoDelayWidget = { "SubmixStereoDelayWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, SubmixStereoDelayWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixStereoDelayWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixStereoDelayWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixTapDelayWidget_MetaData[] = {
		{ "AllowedClasses", "WidgetBlueprint" },
		{ "Category", "Submix Effects" },
		{ "ModuleRelativePath", "Classes/SynthesisEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixTapDelayWidget = { "SubmixTapDelayWidget", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USynthesisEditorSettings, SubmixTapDelayWidget), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixTapDelayWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixTapDelayWidget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USynthesisEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_BitCrusherWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_ChorusWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_DynamicsProcessorWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_EnvelopeFollowerWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_EQWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_FilterWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_FoldbackDistortionWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_MidSideSpreaderWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_PannerWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_PhaserWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_RingModulationWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SimpleDelayWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_StereoDelayWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_WaveShaperWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixConvolutionReverbWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixDelayWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixFilterWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixFlexiverbWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixStereoDelayWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USynthesisEditorSettings_Statics::NewProp_SubmixTapDelayWidget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USynthesisEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USynthesisEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USynthesisEditorSettings_Statics::ClassParams = {
		&USynthesisEditorSettings::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USynthesisEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_USynthesisEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USynthesisEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USynthesisEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USynthesisEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USynthesisEditorSettings, 2830114677);
	template<> SYNTHESISEDITOR_API UClass* StaticClass<USynthesisEditorSettings>()
	{
		return USynthesisEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USynthesisEditorSettings(Z_Construct_UClass_USynthesisEditorSettings, &USynthesisEditorSettings::StaticClass, TEXT("/Script/SynthesisEditor"), TEXT("USynthesisEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USynthesisEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
