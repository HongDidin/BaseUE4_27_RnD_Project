// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SYNTHESISEDITOR_SynthesisEditorSettings_generated_h
#error "SynthesisEditorSettings.generated.h already included, missing '#pragma once' in SynthesisEditorSettings.h"
#endif
#define SYNTHESISEDITOR_SynthesisEditorSettings_generated_h

#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSynthesisEditorSettings(); \
	friend struct Z_Construct_UClass_USynthesisEditorSettings_Statics; \
public: \
	DECLARE_CLASS(USynthesisEditorSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/SynthesisEditor"), NO_API) \
	DECLARE_SERIALIZER(USynthesisEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorSettings");} \



#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSynthesisEditorSettings(); \
	friend struct Z_Construct_UClass_USynthesisEditorSettings_Statics; \
public: \
	DECLARE_CLASS(USynthesisEditorSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/SynthesisEditor"), NO_API) \
	DECLARE_SERIALIZER(USynthesisEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorSettings");} \



#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USynthesisEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USynthesisEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USynthesisEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USynthesisEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USynthesisEditorSettings(USynthesisEditorSettings&&); \
	NO_API USynthesisEditorSettings(const USynthesisEditorSettings&); \
public:


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USynthesisEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USynthesisEditorSettings(USynthesisEditorSettings&&); \
	NO_API USynthesisEditorSettings(const USynthesisEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USynthesisEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USynthesisEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USynthesisEditorSettings)


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_11_PROLOG
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_INCLASS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SYNTHESISEDITOR_API UClass* StaticClass<class USynthesisEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_SynthesisEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
