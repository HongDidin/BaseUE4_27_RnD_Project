// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#pragma once


#include "SynthesisEditor/Classes/AudioImpulseResponseAsset.h"
#include "SynthesisEditor/Classes/EpicSynth1PresetBank.h"
#include "SynthesisEditor/Classes/MonoWaveTablePresetBank.h"
#include "SynthesisEditor/Classes/SynthesisEditorSettings.h"

