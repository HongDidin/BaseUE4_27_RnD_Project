// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SynthesisEditor/Classes/EpicSynth1PresetBank.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEpicSynth1PresetBank() {}
// Cross Module References
	SYNTHESISEDITOR_API UClass* Z_Construct_UClass_UModularSynthPresetBankFactory_NoRegister();
	SYNTHESISEDITOR_API UClass* Z_Construct_UClass_UModularSynthPresetBankFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_SynthesisEditor();
// End Cross Module References
	void UModularSynthPresetBankFactory::StaticRegisterNativesUModularSynthPresetBankFactory()
	{
	}
	UClass* Z_Construct_UClass_UModularSynthPresetBankFactory_NoRegister()
	{
		return UModularSynthPresetBankFactory::StaticClass();
	}
	struct Z_Construct_UClass_UModularSynthPresetBankFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UModularSynthPresetBankFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_SynthesisEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UModularSynthPresetBankFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "EpicSynth1PresetBank.h" },
		{ "ModuleRelativePath", "Classes/EpicSynth1PresetBank.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UModularSynthPresetBankFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UModularSynthPresetBankFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UModularSynthPresetBankFactory_Statics::ClassParams = {
		&UModularSynthPresetBankFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UModularSynthPresetBankFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UModularSynthPresetBankFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UModularSynthPresetBankFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UModularSynthPresetBankFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UModularSynthPresetBankFactory, 265207912);
	template<> SYNTHESISEDITOR_API UClass* StaticClass<UModularSynthPresetBankFactory>()
	{
		return UModularSynthPresetBankFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UModularSynthPresetBankFactory(Z_Construct_UClass_UModularSynthPresetBankFactory, &UModularSynthPresetBankFactory::StaticClass, TEXT("/Script/SynthesisEditor"), TEXT("UModularSynthPresetBankFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UModularSynthPresetBankFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
