// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SYNTHESISEDITOR_AudioImpulseResponseAsset_generated_h
#error "AudioImpulseResponseAsset.generated.h already included, missing '#pragma once' in AudioImpulseResponseAsset.h"
#endif
#define SYNTHESISEDITOR_AudioImpulseResponseAsset_generated_h

#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_SPARSE_DATA
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAudioImpulseResponseFactory(); \
	friend struct Z_Construct_UClass_UAudioImpulseResponseFactory_Statics; \
public: \
	DECLARE_CLASS(UAudioImpulseResponseFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SynthesisEditor"), SYNTHESISEDITOR_API) \
	DECLARE_SERIALIZER(UAudioImpulseResponseFactory)


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_INCLASS \
private: \
	static void StaticRegisterNativesUAudioImpulseResponseFactory(); \
	friend struct Z_Construct_UClass_UAudioImpulseResponseFactory_Statics; \
public: \
	DECLARE_CLASS(UAudioImpulseResponseFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SynthesisEditor"), SYNTHESISEDITOR_API) \
	DECLARE_SERIALIZER(UAudioImpulseResponseFactory)


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SYNTHESISEDITOR_API UAudioImpulseResponseFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAudioImpulseResponseFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SYNTHESISEDITOR_API, UAudioImpulseResponseFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAudioImpulseResponseFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SYNTHESISEDITOR_API UAudioImpulseResponseFactory(UAudioImpulseResponseFactory&&); \
	SYNTHESISEDITOR_API UAudioImpulseResponseFactory(const UAudioImpulseResponseFactory&); \
public:


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SYNTHESISEDITOR_API UAudioImpulseResponseFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SYNTHESISEDITOR_API UAudioImpulseResponseFactory(UAudioImpulseResponseFactory&&); \
	SYNTHESISEDITOR_API UAudioImpulseResponseFactory(const UAudioImpulseResponseFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SYNTHESISEDITOR_API, UAudioImpulseResponseFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAudioImpulseResponseFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAudioImpulseResponseFactory)


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_32_PROLOG
#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_SPARSE_DATA \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_INCLASS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_SPARSE_DATA \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h_35_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AudioImpulseResponseFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SYNTHESISEDITOR_API UClass* StaticClass<class UAudioImpulseResponseFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Synthesis_Source_SynthesisEditor_Classes_AudioImpulseResponseAsset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
