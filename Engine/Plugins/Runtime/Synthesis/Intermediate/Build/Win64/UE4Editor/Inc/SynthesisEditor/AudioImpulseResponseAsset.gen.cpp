// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SynthesisEditor/Classes/AudioImpulseResponseAsset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAudioImpulseResponseAsset() {}
// Cross Module References
	SYNTHESISEDITOR_API UClass* Z_Construct_UClass_UAudioImpulseResponseFactory_NoRegister();
	SYNTHESISEDITOR_API UClass* Z_Construct_UClass_UAudioImpulseResponseFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_SynthesisEditor();
// End Cross Module References
	void UAudioImpulseResponseFactory::StaticRegisterNativesUAudioImpulseResponseFactory()
	{
	}
	UClass* Z_Construct_UClass_UAudioImpulseResponseFactory_NoRegister()
	{
		return UAudioImpulseResponseFactory::StaticClass();
	}
	struct Z_Construct_UClass_UAudioImpulseResponseFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAudioImpulseResponseFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_SynthesisEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioImpulseResponseFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "AudioImpulseResponseAsset.h" },
		{ "ModuleRelativePath", "Classes/AudioImpulseResponseAsset.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAudioImpulseResponseFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAudioImpulseResponseFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAudioImpulseResponseFactory_Statics::ClassParams = {
		&UAudioImpulseResponseFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAudioImpulseResponseFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioImpulseResponseFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAudioImpulseResponseFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAudioImpulseResponseFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAudioImpulseResponseFactory, 3878982576);
	template<> SYNTHESISEDITOR_API UClass* StaticClass<UAudioImpulseResponseFactory>()
	{
		return UAudioImpulseResponseFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAudioImpulseResponseFactory(Z_Construct_UClass_UAudioImpulseResponseFactory, &UAudioImpulseResponseFactory::StaticClass, TEXT("/Script/SynthesisEditor"), TEXT("UAudioImpulseResponseFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAudioImpulseResponseFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
