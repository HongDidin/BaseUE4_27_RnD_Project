// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OSCModulationMixing/Public/OSCModulationMixingStatics.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOSCModulationMixingStatics() {}
// Cross Module References
	OSCMODULATIONMIXING_API UEnum* Z_Construct_UEnum_OSCModulationMixing_EOSCModulationMessage();
	UPackage* Z_Construct_UPackage__Script_OSCModulationMixing();
	OSCMODULATIONMIXING_API UEnum* Z_Construct_UEnum_OSCModulationMixing_EOSCModulationBundle();
	OSCMODULATIONMIXING_API UClass* Z_Construct_UClass_UOSCModulationMixingStatics_NoRegister();
	OSCMODULATIONMIXING_API UClass* Z_Construct_UClass_UOSCModulationMixingStatics();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundControlBusMix_NoRegister();
	OSC_API UScriptStruct* Z_Construct_UScriptStruct_FOSCBundle();
	OSC_API UScriptStruct* Z_Construct_UScriptStruct_FOSCAddress();
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FSoundControlBusMixStage();
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FSoundModulationMixValue();
	OSC_API UClass* Z_Construct_UClass_UOSCClient_NoRegister();
// End Cross Module References
	static UEnum* EOSCModulationMessage_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OSCModulationMixing_EOSCModulationMessage, Z_Construct_UPackage__Script_OSCModulationMixing(), TEXT("EOSCModulationMessage"));
		}
		return Singleton;
	}
	template<> OSCMODULATIONMIXING_API UEnum* StaticEnum<EOSCModulationMessage>()
	{
		return EOSCModulationMessage_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOSCModulationMessage(EOSCModulationMessage_StaticEnum, TEXT("/Script/OSCModulationMixing"), TEXT("EOSCModulationMessage"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OSCModulationMixing_EOSCModulationMessage_Hash() { return 3475587605U; }
	UEnum* Z_Construct_UEnum_OSCModulationMixing_EOSCModulationMessage()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OSCModulationMixing();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOSCModulationMessage"), 0, Get_Z_Construct_UEnum_OSCModulationMixing_EOSCModulationMessage_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOSCModulationMessage::Invalid", (int64)EOSCModulationMessage::Invalid },
				{ "EOSCModulationMessage::LoadProfile", (int64)EOSCModulationMessage::LoadProfile },
				{ "EOSCModulationMessage::SaveProfile", (int64)EOSCModulationMessage::SaveProfile },
				{ "EOSCModulationMessage::Count", (int64)EOSCModulationMessage::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Count.Hidden", "" },
				{ "Count.Name", "EOSCModulationMessage::Count" },
				{ "Invalid.Name", "EOSCModulationMessage::Invalid" },
				{ "LoadProfile.Name", "EOSCModulationMessage::LoadProfile" },
				{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
				{ "SaveProfile.Name", "EOSCModulationMessage::SaveProfile" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OSCModulationMixing,
				nullptr,
				"EOSCModulationMessage",
				"EOSCModulationMessage",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EOSCModulationBundle_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OSCModulationMixing_EOSCModulationBundle, Z_Construct_UPackage__Script_OSCModulationMixing(), TEXT("EOSCModulationBundle"));
		}
		return Singleton;
	}
	template<> OSCMODULATIONMIXING_API UEnum* StaticEnum<EOSCModulationBundle>()
	{
		return EOSCModulationBundle_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOSCModulationBundle(EOSCModulationBundle_StaticEnum, TEXT("/Script/OSCModulationMixing"), TEXT("EOSCModulationBundle"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OSCModulationMixing_EOSCModulationBundle_Hash() { return 2064907345U; }
	UEnum* Z_Construct_UEnum_OSCModulationMixing_EOSCModulationBundle()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OSCModulationMixing();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOSCModulationBundle"), 0, Get_Z_Construct_UEnum_OSCModulationMixing_EOSCModulationBundle_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOSCModulationBundle::Invalid", (int64)EOSCModulationBundle::Invalid },
				{ "EOSCModulationBundle::LoadMix", (int64)EOSCModulationBundle::LoadMix },
				{ "EOSCModulationBundle::Count", (int64)EOSCModulationBundle::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Count.Hidden", "" },
				{ "Count.Name", "EOSCModulationBundle::Count" },
				{ "Invalid.Name", "EOSCModulationBundle::Invalid" },
				{ "LoadMix.Name", "EOSCModulationBundle::LoadMix" },
				{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OSCModulationMixing,
				nullptr,
				"EOSCModulationBundle",
				"EOSCModulationBundle",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UOSCModulationMixingStatics::execOSCBundleToStageValues)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT_REF(FOSCBundle,Z_Param_Out_Bundle);
		P_GET_STRUCT_REF(FOSCAddress,Z_Param_Out_MixPath);
		P_GET_TARRAY_REF(FOSCAddress,Z_Param_Out_BusPaths);
		P_GET_TARRAY_REF(FString,Z_Param_Out_BusClassNames);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FSoundModulationMixValue>*)Z_Param__Result=UOSCModulationMixingStatics::OSCBundleToStageValues(Z_Param_WorldContextObject,Z_Param_Out_Bundle,Z_Param_Out_MixPath,Z_Param_Out_BusPaths,Z_Param_Out_BusClassNames);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOSCModulationMixingStatics::execRequestMix)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(UOSCClient,Z_Param_Client);
		P_GET_STRUCT_REF(FOSCAddress,Z_Param_Out_MixPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOSCModulationMixingStatics::RequestMix(Z_Param_WorldContextObject,Z_Param_Client,Z_Param_Out_MixPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOSCModulationMixingStatics::execGetOSCBundleType)
	{
		P_GET_STRUCT_REF(FOSCBundle,Z_Param_Out_Bundle);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EOSCModulationBundle*)Z_Param__Result=UOSCModulationMixingStatics::GetOSCBundleType(Z_Param_Out_Bundle);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOSCModulationMixingStatics::execCopyMixToOSCBundle)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBusMix,Z_Param_Mix);
		P_GET_STRUCT_REF(FOSCBundle,Z_Param_Out_Bundle);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOSCModulationMixingStatics::CopyMixToOSCBundle(Z_Param_WorldContextObject,Z_Param_Mix,Z_Param_Out_Bundle);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOSCModulationMixingStatics::execCopyStagesToOSCBundle)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT_REF(FOSCAddress,Z_Param_Out_PathAddress);
		P_GET_TARRAY_REF(FSoundControlBusMixStage,Z_Param_Out_Stages);
		P_GET_STRUCT_REF(FOSCBundle,Z_Param_Out_Bundle);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOSCModulationMixingStatics::CopyStagesToOSCBundle(Z_Param_WorldContextObject,Z_Param_Out_PathAddress,Z_Param_Out_Stages,Z_Param_Out_Bundle);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOSCModulationMixingStatics::execGetProfileSavePath)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FOSCAddress*)Z_Param__Result=UOSCModulationMixingStatics::GetProfileSavePath();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOSCModulationMixingStatics::execGetProfileLoadPath)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FOSCAddress*)Z_Param__Result=UOSCModulationMixingStatics::GetProfileLoadPath();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOSCModulationMixingStatics::execGetMixLoadPattern)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FOSCAddress*)Z_Param__Result=UOSCModulationMixingStatics::GetMixLoadPattern();
		P_NATIVE_END;
	}
	void UOSCModulationMixingStatics::StaticRegisterNativesUOSCModulationMixingStatics()
	{
		UClass* Class = UOSCModulationMixingStatics::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CopyMixToOSCBundle", &UOSCModulationMixingStatics::execCopyMixToOSCBundle },
			{ "CopyStagesToOSCBundle", &UOSCModulationMixingStatics::execCopyStagesToOSCBundle },
			{ "GetMixLoadPattern", &UOSCModulationMixingStatics::execGetMixLoadPattern },
			{ "GetOSCBundleType", &UOSCModulationMixingStatics::execGetOSCBundleType },
			{ "GetProfileLoadPath", &UOSCModulationMixingStatics::execGetProfileLoadPath },
			{ "GetProfileSavePath", &UOSCModulationMixingStatics::execGetProfileSavePath },
			{ "OSCBundleToStageValues", &UOSCModulationMixingStatics::execOSCBundleToStageValues },
			{ "RequestMix", &UOSCModulationMixingStatics::execRequestMix },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics
	{
		struct OSCModulationMixingStatics_eventCopyMixToOSCBundle_Parms
		{
			UObject* WorldContextObject;
			USoundControlBusMix* Mix;
			FOSCBundle Bundle;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mix;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Bundle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventCopyMixToOSCBundle_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::NewProp_Mix = { "Mix", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventCopyMixToOSCBundle_Parms, Mix), Z_Construct_UClass_USoundControlBusMix_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::NewProp_Bundle = { "Bundle", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventCopyMixToOSCBundle_Parms, Bundle), Z_Construct_UScriptStruct_FOSCBundle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::NewProp_Mix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::NewProp_Bundle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|OSC|Modulation" },
		{ "Comment", "/** Converts Control Bus Mix to OSCBundle representation to send over network via OSC protocol */" },
		{ "DisplayName", "Copy Mix State to OSC Bundle" },
		{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
		{ "ToolTip", "Converts Control Bus Mix to OSCBundle representation to send over network via OSC protocol" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOSCModulationMixingStatics, nullptr, "CopyMixToOSCBundle", nullptr, nullptr, sizeof(OSCModulationMixingStatics_eventCopyMixToOSCBundle_Parms), Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics
	{
		struct OSCModulationMixingStatics_eventCopyStagesToOSCBundle_Parms
		{
			UObject* WorldContextObject;
			FOSCAddress PathAddress;
			TArray<FSoundControlBusMixStage> Stages;
			FOSCBundle Bundle;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PathAddress;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Stages_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Stages_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Stages;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Bundle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventCopyStagesToOSCBundle_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_PathAddress_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_PathAddress = { "PathAddress", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventCopyStagesToOSCBundle_Parms, PathAddress), Z_Construct_UScriptStruct_FOSCAddress, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_PathAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_PathAddress_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_Stages_Inner = { "Stages", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoundControlBusMixStage, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_Stages_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_Stages = { "Stages", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventCopyStagesToOSCBundle_Parms, Stages), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_Stages_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_Stages_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_Bundle = { "Bundle", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventCopyStagesToOSCBundle_Parms, Bundle), Z_Construct_UScriptStruct_FOSCBundle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_PathAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_Stages_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_Stages,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::NewProp_Bundle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|OSC|Modulation" },
		{ "Comment", "/** Converts stage array to OSCBundle representation to send over network via OSC protocol */" },
		{ "DisplayName", "Copy Mix Stages to OSC Bundle" },
		{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
		{ "ToolTip", "Converts stage array to OSCBundle representation to send over network via OSC protocol" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOSCModulationMixingStatics, nullptr, "CopyStagesToOSCBundle", nullptr, nullptr, sizeof(OSCModulationMixingStatics_eventCopyStagesToOSCBundle_Parms), Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics
	{
		struct OSCModulationMixingStatics_eventGetMixLoadPattern_Parms
		{
			FOSCAddress ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventGetMixLoadPattern_Parms, ReturnValue), Z_Construct_UScriptStruct_FOSCAddress, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|OSC|Modulation" },
		{ "Comment", "/** Returns OSC Address pattern for loading a mix */" },
		{ "DisplayName", "Get Load Mix Pattern" },
		{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
		{ "ToolTip", "Returns OSC Address pattern for loading a mix" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOSCModulationMixingStatics, nullptr, "GetMixLoadPattern", nullptr, nullptr, sizeof(OSCModulationMixingStatics_eventGetMixLoadPattern_Parms), Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics
	{
		struct OSCModulationMixingStatics_eventGetOSCBundleType_Parms
		{
			FOSCBundle Bundle;
			EOSCModulationBundle ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bundle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Bundle;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::NewProp_Bundle_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::NewProp_Bundle = { "Bundle", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventGetOSCBundleType_Parms, Bundle), Z_Construct_UScriptStruct_FOSCBundle, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::NewProp_Bundle_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::NewProp_Bundle_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventGetOSCBundleType_Parms, ReturnValue), Z_Construct_UEnum_OSCModulationMixing_EOSCModulationBundle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::NewProp_Bundle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|OSC|Modulation" },
		{ "Comment", "/** Returns whether bundle contains recognized payload of OSC Modulation Data */" },
		{ "DisplayName", "Get OSC Modulation Bundle Type" },
		{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
		{ "ToolTip", "Returns whether bundle contains recognized payload of OSC Modulation Data" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOSCModulationMixingStatics, nullptr, "GetOSCBundleType", nullptr, nullptr, sizeof(OSCModulationMixingStatics_eventGetOSCBundleType_Parms), Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics
	{
		struct OSCModulationMixingStatics_eventGetProfileLoadPath_Parms
		{
			FOSCAddress ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventGetProfileLoadPath_Parms, ReturnValue), Z_Construct_UScriptStruct_FOSCAddress, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|OSC|Modulation" },
		{ "Comment", "/** Returns OSC Address path for loading a profile */" },
		{ "DisplayName", "Get Load Profile Path" },
		{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
		{ "ToolTip", "Returns OSC Address path for loading a profile" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOSCModulationMixingStatics, nullptr, "GetProfileLoadPath", nullptr, nullptr, sizeof(OSCModulationMixingStatics_eventGetProfileLoadPath_Parms), Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics
	{
		struct OSCModulationMixingStatics_eventGetProfileSavePath_Parms
		{
			FOSCAddress ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventGetProfileSavePath_Parms, ReturnValue), Z_Construct_UScriptStruct_FOSCAddress, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|OSC|Modulation" },
		{ "Comment", "/** Returns OSC Address path for saving a profile */" },
		{ "DisplayName", "Get Save Profile Path" },
		{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
		{ "ToolTip", "Returns OSC Address path for saving a profile" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOSCModulationMixingStatics, nullptr, "GetProfileSavePath", nullptr, nullptr, sizeof(OSCModulationMixingStatics_eventGetProfileSavePath_Parms), Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics
	{
		struct OSCModulationMixingStatics_eventOSCBundleToStageValues_Parms
		{
			UObject* WorldContextObject;
			FOSCBundle Bundle;
			FOSCAddress MixPath;
			TArray<FOSCAddress> BusPaths;
			TArray<FString> BusClassNames;
			TArray<FSoundModulationMixValue> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bundle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Bundle;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MixPath;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BusPaths_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BusPaths;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BusClassNames_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BusClassNames;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventOSCBundleToStageValues_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_Bundle_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_Bundle = { "Bundle", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventOSCBundleToStageValues_Parms, Bundle), Z_Construct_UScriptStruct_FOSCBundle, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_Bundle_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_Bundle_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_MixPath = { "MixPath", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventOSCBundleToStageValues_Parms, MixPath), Z_Construct_UScriptStruct_FOSCAddress, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_BusPaths_Inner = { "BusPaths", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FOSCAddress, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_BusPaths = { "BusPaths", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventOSCBundleToStageValues_Parms, BusPaths), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_BusClassNames_Inner = { "BusClassNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_BusClassNames = { "BusClassNames", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventOSCBundleToStageValues_Parms, BusClassNames), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoundModulationMixValue, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "DisplayName", "Bus Values" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventOSCBundleToStageValues_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_Bundle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_MixPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_BusPaths_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_BusPaths,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_BusClassNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_BusClassNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|OSC|Modulation" },
		{ "Comment", "/** Converts OSCBundle to Control Bus Values & Mix Path from which it came */" },
		{ "DisplayName", "OSCBundle To Stage Values" },
		{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
		{ "ToolTip", "Converts OSCBundle to Control Bus Values & Mix Path from which it came" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOSCModulationMixingStatics, nullptr, "OSCBundleToStageValues", nullptr, nullptr, sizeof(OSCModulationMixingStatics_eventOSCBundleToStageValues_Parms), Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics
	{
		struct OSCModulationMixingStatics_eventRequestMix_Parms
		{
			UObject* WorldContextObject;
			UOSCClient* Client;
			FOSCAddress MixPath;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Client;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MixPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MixPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventRequestMix_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::NewProp_Client = { "Client", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventRequestMix_Parms, Client), Z_Construct_UClass_UOSCClient_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::NewProp_MixPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::NewProp_MixPath = { "MixPath", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OSCModulationMixingStatics_eventRequestMix_Parms, MixPath), Z_Construct_UScriptStruct_FOSCAddress, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::NewProp_MixPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::NewProp_MixPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::NewProp_Client,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::NewProp_MixPath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|OSC|Modulation" },
		{ "Comment", "/** Request mix update from server with loaded content. */" },
		{ "DisplayName", "Request Mix State Update" },
		{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
		{ "ToolTip", "Request mix update from server with loaded content." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOSCModulationMixingStatics, nullptr, "RequestMix", nullptr, nullptr, sizeof(OSCModulationMixingStatics_eventRequestMix_Parms), Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UOSCModulationMixingStatics_NoRegister()
	{
		return UOSCModulationMixingStatics::StaticClass();
	}
	struct Z_Construct_UClass_UOSCModulationMixingStatics_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOSCModulationMixingStatics_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_OSCModulationMixing,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOSCModulationMixingStatics_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOSCModulationMixingStatics_CopyMixToOSCBundle, "CopyMixToOSCBundle" }, // 2822532793
		{ &Z_Construct_UFunction_UOSCModulationMixingStatics_CopyStagesToOSCBundle, "CopyStagesToOSCBundle" }, // 748143578
		{ &Z_Construct_UFunction_UOSCModulationMixingStatics_GetMixLoadPattern, "GetMixLoadPattern" }, // 2364443584
		{ &Z_Construct_UFunction_UOSCModulationMixingStatics_GetOSCBundleType, "GetOSCBundleType" }, // 1888007401
		{ &Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileLoadPath, "GetProfileLoadPath" }, // 1727720516
		{ &Z_Construct_UFunction_UOSCModulationMixingStatics_GetProfileSavePath, "GetProfileSavePath" }, // 2475021886
		{ &Z_Construct_UFunction_UOSCModulationMixingStatics_OSCBundleToStageValues, "OSCBundleToStageValues" }, // 897278184
		{ &Z_Construct_UFunction_UOSCModulationMixingStatics_RequestMix, "RequestMix" }, // 2199766405
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOSCModulationMixingStatics_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OSCModulationMixingStatics.h" },
		{ "ModuleRelativePath", "Public/OSCModulationMixingStatics.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOSCModulationMixingStatics_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOSCModulationMixingStatics>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOSCModulationMixingStatics_Statics::ClassParams = {
		&UOSCModulationMixingStatics::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOSCModulationMixingStatics_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOSCModulationMixingStatics_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOSCModulationMixingStatics()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOSCModulationMixingStatics_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOSCModulationMixingStatics, 884575694);
	template<> OSCMODULATIONMIXING_API UClass* StaticClass<UOSCModulationMixingStatics>()
	{
		return UOSCModulationMixingStatics::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOSCModulationMixingStatics(Z_Construct_UClass_UOSCModulationMixingStatics, &UOSCModulationMixingStatics::StaticClass, TEXT("/Script/OSCModulationMixing"), TEXT("UOSCModulationMixingStatics"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOSCModulationMixingStatics);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
