// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
struct FOSCBundle;
struct FOSCAddress;
struct FSoundModulationMixValue;
class UOSCClient;
enum class EOSCModulationBundle : uint8;
class USoundControlBusMix;
struct FSoundControlBusMixStage;
#ifdef OSCMODULATIONMIXING_OSCModulationMixingStatics_generated_h
#error "OSCModulationMixingStatics.generated.h already included, missing '#pragma once' in OSCModulationMixingStatics.h"
#endif
#define OSCMODULATIONMIXING_OSCModulationMixingStatics_generated_h

#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_SPARSE_DATA
#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOSCBundleToStageValues); \
	DECLARE_FUNCTION(execRequestMix); \
	DECLARE_FUNCTION(execGetOSCBundleType); \
	DECLARE_FUNCTION(execCopyMixToOSCBundle); \
	DECLARE_FUNCTION(execCopyStagesToOSCBundle); \
	DECLARE_FUNCTION(execGetProfileSavePath); \
	DECLARE_FUNCTION(execGetProfileLoadPath); \
	DECLARE_FUNCTION(execGetMixLoadPattern);


#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOSCBundleToStageValues); \
	DECLARE_FUNCTION(execRequestMix); \
	DECLARE_FUNCTION(execGetOSCBundleType); \
	DECLARE_FUNCTION(execCopyMixToOSCBundle); \
	DECLARE_FUNCTION(execCopyStagesToOSCBundle); \
	DECLARE_FUNCTION(execGetProfileSavePath); \
	DECLARE_FUNCTION(execGetProfileLoadPath); \
	DECLARE_FUNCTION(execGetMixLoadPattern);


#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOSCModulationMixingStatics(); \
	friend struct Z_Construct_UClass_UOSCModulationMixingStatics_Statics; \
public: \
	DECLARE_CLASS(UOSCModulationMixingStatics, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OSCModulationMixing"), NO_API) \
	DECLARE_SERIALIZER(UOSCModulationMixingStatics)


#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_INCLASS \
private: \
	static void StaticRegisterNativesUOSCModulationMixingStatics(); \
	friend struct Z_Construct_UClass_UOSCModulationMixingStatics_Statics; \
public: \
	DECLARE_CLASS(UOSCModulationMixingStatics, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OSCModulationMixing"), NO_API) \
	DECLARE_SERIALIZER(UOSCModulationMixingStatics)


#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOSCModulationMixingStatics(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOSCModulationMixingStatics) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOSCModulationMixingStatics); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOSCModulationMixingStatics); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOSCModulationMixingStatics(UOSCModulationMixingStatics&&); \
	NO_API UOSCModulationMixingStatics(const UOSCModulationMixingStatics&); \
public:


#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOSCModulationMixingStatics(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOSCModulationMixingStatics(UOSCModulationMixingStatics&&); \
	NO_API UOSCModulationMixingStatics(const UOSCModulationMixingStatics&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOSCModulationMixingStatics); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOSCModulationMixingStatics); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOSCModulationMixingStatics)


#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_35_PROLOG
#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_SPARSE_DATA \
	Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_RPC_WRAPPERS \
	Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_INCLASS \
	Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_SPARSE_DATA \
	Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h_38_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OSCModulationMixingStatics."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OSCMODULATIONMIXING_API UClass* StaticClass<class UOSCModulationMixingStatics>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_OSCModulationMixing_Source_OSCModulationMixing_Public_OSCModulationMixingStatics_h


#define FOREACH_ENUM_EOSCMODULATIONMESSAGE(op) \
	op(EOSCModulationMessage::Invalid) \
	op(EOSCModulationMessage::LoadProfile) \
	op(EOSCModulationMessage::SaveProfile) \
	op(EOSCModulationMessage::Count) 

enum class EOSCModulationMessage : uint8;
template<> OSCMODULATIONMIXING_API UEnum* StaticEnum<EOSCModulationMessage>();

#define FOREACH_ENUM_EOSCMODULATIONBUNDLE(op) \
	op(EOSCModulationBundle::Invalid) \
	op(EOSCModulationBundle::LoadMix) \
	op(EOSCModulationBundle::Count) 

enum class EOSCModulationBundle : uint8;
template<> OSCMODULATIONMIXING_API UEnum* StaticEnum<EOSCModulationBundle>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
