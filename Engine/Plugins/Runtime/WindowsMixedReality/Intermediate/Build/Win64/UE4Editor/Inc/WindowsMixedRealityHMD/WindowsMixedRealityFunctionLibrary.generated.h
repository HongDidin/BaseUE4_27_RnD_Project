// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EHMDSpatialLocatability : uint8;
struct FVector;
enum class EControllerHand : uint8;
enum class EHMDTrackingStatus : uint8;
enum class EHMDInputControllerButtons : uint8;
struct FPointerPoseInfo;
#ifdef WINDOWSMIXEDREALITYHMD_WindowsMixedRealityFunctionLibrary_generated_h
#error "WindowsMixedRealityFunctionLibrary.generated.h already included, missing '#pragma once' in WindowsMixedRealityFunctionLibrary.h"
#endif
#define WINDOWSMIXEDREALITYHMD_WindowsMixedRealityFunctionLibrary_generated_h

#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_44_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPointerPoseInfo_Statics; \
	WINDOWSMIXEDREALITYHMD_API static class UScriptStruct* StaticStruct();


template<> WINDOWSMIXEDREALITYHMD_API UScriptStruct* StaticStruct<struct FPointerPoseInfo>();

#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_62_DELEGATE \
struct _Script_WindowsMixedRealityHMD_eventTrackingChangeCallback_Parms \
{ \
	EHMDSpatialLocatability locatability; \
}; \
static inline void FTrackingChangeCallback_DelegateWrapper(const FScriptDelegate& TrackingChangeCallback, EHMDSpatialLocatability locatability) \
{ \
	_Script_WindowsMixedRealityHMD_eventTrackingChangeCallback_Parms Parms; \
	Parms.locatability=locatability; \
	TrackingChangeCallback.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_SPARSE_DATA
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetFocusPointForFrame); \
	DECLARE_FUNCTION(execGetControllerTrackingStatus); \
	DECLARE_FUNCTION(execIsSelectPressed); \
	DECLARE_FUNCTION(execIsGrasped); \
	DECLARE_FUNCTION(execIsButtonDown); \
	DECLARE_FUNCTION(execIsButtonClicked); \
	DECLARE_FUNCTION(execGetPointerPoseInfo); \
	DECLARE_FUNCTION(execIsTrackingAvailable); \
	DECLARE_FUNCTION(execLockMouseToCenter); \
	DECLARE_FUNCTION(execIsDisplayOpaque); \
	DECLARE_FUNCTION(execIsCurrentlyImmersive); \
	DECLARE_FUNCTION(execToggleImmersive); \
	DECLARE_FUNCTION(execGetVersionString);


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetFocusPointForFrame); \
	DECLARE_FUNCTION(execGetControllerTrackingStatus); \
	DECLARE_FUNCTION(execIsSelectPressed); \
	DECLARE_FUNCTION(execIsGrasped); \
	DECLARE_FUNCTION(execIsButtonDown); \
	DECLARE_FUNCTION(execIsButtonClicked); \
	DECLARE_FUNCTION(execGetPointerPoseInfo); \
	DECLARE_FUNCTION(execIsTrackingAvailable); \
	DECLARE_FUNCTION(execLockMouseToCenter); \
	DECLARE_FUNCTION(execIsDisplayOpaque); \
	DECLARE_FUNCTION(execIsCurrentlyImmersive); \
	DECLARE_FUNCTION(execToggleImmersive); \
	DECLARE_FUNCTION(execGetVersionString);


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWindowsMixedRealityFunctionLibrary(); \
	friend struct Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UWindowsMixedRealityFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WindowsMixedRealityHMD"), NO_API) \
	DECLARE_SERIALIZER(UWindowsMixedRealityFunctionLibrary)


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_INCLASS \
private: \
	static void StaticRegisterNativesUWindowsMixedRealityFunctionLibrary(); \
	friend struct Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UWindowsMixedRealityFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WindowsMixedRealityHMD"), NO_API) \
	DECLARE_SERIALIZER(UWindowsMixedRealityFunctionLibrary)


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWindowsMixedRealityFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWindowsMixedRealityFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWindowsMixedRealityFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWindowsMixedRealityFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWindowsMixedRealityFunctionLibrary(UWindowsMixedRealityFunctionLibrary&&); \
	NO_API UWindowsMixedRealityFunctionLibrary(const UWindowsMixedRealityFunctionLibrary&); \
public:


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWindowsMixedRealityFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWindowsMixedRealityFunctionLibrary(UWindowsMixedRealityFunctionLibrary&&); \
	NO_API UWindowsMixedRealityFunctionLibrary(const UWindowsMixedRealityFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWindowsMixedRealityFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWindowsMixedRealityFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWindowsMixedRealityFunctionLibrary)


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_67_PROLOG
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_SPARSE_DATA \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_RPC_WRAPPERS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_INCLASS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_SPARSE_DATA \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h_70_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class WindowsMixedRealityFunctionLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WINDOWSMIXEDREALITYHMD_API UClass* StaticClass<class UWindowsMixedRealityFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHMD_Public_WindowsMixedRealityFunctionLibrary_h


#define FOREACH_ENUM_EHMDTRACKINGSTATUS(op) \
	op(EHMDTrackingStatus::NotTracked) \
	op(EHMDTrackingStatus::InertialOnly) \
	op(EHMDTrackingStatus::Tracked) 

enum class EHMDTrackingStatus : uint8;
template<> WINDOWSMIXEDREALITYHMD_API UEnum* StaticEnum<EHMDTrackingStatus>();

#define FOREACH_ENUM_EHMDINPUTCONTROLLERBUTTONS(op) \
	op(EHMDInputControllerButtons::Select) \
	op(EHMDInputControllerButtons::Grasp) \
	op(EHMDInputControllerButtons::Menu) \
	op(EHMDInputControllerButtons::Thumbstick) \
	op(EHMDInputControllerButtons::Touchpad) \
	op(EHMDInputControllerButtons::TouchpadIsTouched) \
	op(EHMDInputControllerButtons::Count) 

enum class EHMDInputControllerButtons : uint8;
template<> WINDOWSMIXEDREALITYHMD_API UEnum* StaticEnum<EHMDInputControllerButtons>();

#define FOREACH_ENUM_EHMDSPATIALLOCATABILITY(op) \
	op(EHMDSpatialLocatability::Unavailable) \
	op(EHMDSpatialLocatability::OrientationOnly) \
	op(EHMDSpatialLocatability::PositionalTrackingActivating) \
	op(EHMDSpatialLocatability::PositionalTrackingActive) \
	op(EHMDSpatialLocatability::PositionalTrackingInhibited) 

enum class EHMDSpatialLocatability : uint8;
template<> WINDOWSMIXEDREALITYHMD_API UEnum* StaticEnum<EHMDSpatialLocatability>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
