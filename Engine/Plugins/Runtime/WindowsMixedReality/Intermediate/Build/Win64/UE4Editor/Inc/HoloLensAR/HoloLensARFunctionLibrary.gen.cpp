// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HoloLensAR/Public/HoloLensARFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHoloLensARFunctionLibrary() {}
// Cross Module References
	HOLOLENSAR_API UClass* Z_Construct_UClass_UHoloLensARFunctionLibrary_NoRegister();
	HOLOLENSAR_API UClass* Z_Construct_UClass_UHoloLensARFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_HoloLensAR();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	HOLOLENSAR_API UClass* Z_Construct_UClass_UWMRARPin_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UARPin_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
// End Cross Module References
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execSetUseLegacyHandMeshVisualization)
	{
		P_GET_UBOOL(Z_Param_UseLegacyHandMeshVisualization);
		P_FINISH;
		P_NATIVE_BEGIN;
		UHoloLensARFunctionLibrary::SetUseLegacyHandMeshVisualization(Z_Param_UseLegacyHandMeshVisualization);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execHideKeyboard)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UHoloLensARFunctionLibrary::HideKeyboard();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execShowKeyboard)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UHoloLensARFunctionLibrary::ShowKeyboard();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execStopQRCodeCapture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UHoloLensARFunctionLibrary::StopQRCodeCapture();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execStartQRCodeCapture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UHoloLensARFunctionLibrary::StartQRCodeCapture();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execStopCameraCapture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UHoloLensARFunctionLibrary::StopCameraCapture();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execStartCameraCapture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UHoloLensARFunctionLibrary::StartCameraCapture();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execGetWorldSpaceRayFromCameraPoint)
	{
		P_GET_STRUCT(FVector2D,Z_Param_pixelCoordinate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=UHoloLensARFunctionLibrary::GetWorldSpaceRayFromCameraPoint(Z_Param_pixelCoordinate);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execGetPVCameraIntrinsics)
	{
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_focalLength);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_width);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_height);
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_principalPoint);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_radialDistortion);
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_tangentialDistortion);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UHoloLensARFunctionLibrary::GetPVCameraIntrinsics(Z_Param_Out_focalLength,Z_Param_Out_width,Z_Param_Out_height,Z_Param_Out_principalPoint,Z_Param_Out_radialDistortion,Z_Param_Out_tangentialDistortion);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execGetPVCameraToWorldTransform)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=UHoloLensARFunctionLibrary::GetPVCameraToWorldTransform();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execResizeMixedRealityCamera)
	{
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_size);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FIntPoint*)Z_Param__Result=UHoloLensARFunctionLibrary::ResizeMixedRealityCamera(Z_Param_Out_size);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execSetEnabledMixedRealityCamera)
	{
		P_GET_UBOOL(Z_Param_IsEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		UHoloLensARFunctionLibrary::SetEnabledMixedRealityCamera(Z_Param_IsEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execRemoveARPinFromWMRAnchorStore)
	{
		P_GET_OBJECT(UARPin,Z_Param_InPin);
		P_FINISH;
		P_NATIVE_BEGIN;
		UHoloLensARFunctionLibrary::RemoveARPinFromWMRAnchorStore(Z_Param_InPin);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execSaveARPinToWMRAnchorStore)
	{
		P_GET_OBJECT(UARPin,Z_Param_InPin);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UHoloLensARFunctionLibrary::SaveARPinToWMRAnchorStore(Z_Param_InPin);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execLoadWMRAnchorStoreARPins)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UWMRARPin*>*)Z_Param__Result=UHoloLensARFunctionLibrary::LoadWMRAnchorStoreARPins();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHoloLensARFunctionLibrary::execCreateNamedARPin)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_Name);
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_PinToWorldTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UWMRARPin**)Z_Param__Result=UHoloLensARFunctionLibrary::CreateNamedARPin(Z_Param_Name,Z_Param_Out_PinToWorldTransform);
		P_NATIVE_END;
	}
	void UHoloLensARFunctionLibrary::StaticRegisterNativesUHoloLensARFunctionLibrary()
	{
		UClass* Class = UHoloLensARFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateNamedARPin", &UHoloLensARFunctionLibrary::execCreateNamedARPin },
			{ "GetPVCameraIntrinsics", &UHoloLensARFunctionLibrary::execGetPVCameraIntrinsics },
			{ "GetPVCameraToWorldTransform", &UHoloLensARFunctionLibrary::execGetPVCameraToWorldTransform },
			{ "GetWorldSpaceRayFromCameraPoint", &UHoloLensARFunctionLibrary::execGetWorldSpaceRayFromCameraPoint },
			{ "HideKeyboard", &UHoloLensARFunctionLibrary::execHideKeyboard },
			{ "LoadWMRAnchorStoreARPins", &UHoloLensARFunctionLibrary::execLoadWMRAnchorStoreARPins },
			{ "RemoveARPinFromWMRAnchorStore", &UHoloLensARFunctionLibrary::execRemoveARPinFromWMRAnchorStore },
			{ "ResizeMixedRealityCamera", &UHoloLensARFunctionLibrary::execResizeMixedRealityCamera },
			{ "SaveARPinToWMRAnchorStore", &UHoloLensARFunctionLibrary::execSaveARPinToWMRAnchorStore },
			{ "SetEnabledMixedRealityCamera", &UHoloLensARFunctionLibrary::execSetEnabledMixedRealityCamera },
			{ "SetUseLegacyHandMeshVisualization", &UHoloLensARFunctionLibrary::execSetUseLegacyHandMeshVisualization },
			{ "ShowKeyboard", &UHoloLensARFunctionLibrary::execShowKeyboard },
			{ "StartCameraCapture", &UHoloLensARFunctionLibrary::execStartCameraCapture },
			{ "StartQRCodeCapture", &UHoloLensARFunctionLibrary::execStartQRCodeCapture },
			{ "StopCameraCapture", &UHoloLensARFunctionLibrary::execStopCameraCapture },
			{ "StopQRCodeCapture", &UHoloLensARFunctionLibrary::execStopQRCodeCapture },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics
	{
		struct HoloLensARFunctionLibrary_eventCreateNamedARPin_Parms
		{
			FName Name;
			FTransform PinToWorldTransform;
			UWMRARPin* ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PinToWorldTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PinToWorldTransform;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventCreateNamedARPin_Parms, Name), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::NewProp_PinToWorldTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::NewProp_PinToWorldTransform = { "PinToWorldTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventCreateNamedARPin_Parms, PinToWorldTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::NewProp_PinToWorldTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::NewProp_PinToWorldTransform_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventCreateNamedARPin_Parms, ReturnValue), Z_Construct_UClass_UWMRARPin_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::NewProp_PinToWorldTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR|ARPin" },
		{ "Comment", "/**\n\x09 * Create an UARPin with the specified name, which will also be the name used to store it in the Windows Mixed Reality Anchor Store.\n\x09 *\n\x09 * @param Name\x09\x09\x09\x09\x09\x09The name of the anchor.  If the Name is already in use creation will fail.  A leading \"_\" is reserved for automatically named anchors. Do not start your names with an underscore.  The name 'None' is illegal.\n\x09 * @param PinToWorldTransform\x09\x09The Pin which the component will be updated by.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Please transition to use the functions in ARBlueprintLibrary which save pins to the anchor store by a 'saveId' rather than using the built in WMR anchorId." },
		{ "Keywords", "hololensar wmr pin ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Create an UARPin with the specified name, which will also be the name used to store it in the Windows Mixed Reality Anchor Store.\n\n@param Name                                          The name of the anchor.  If the Name is already in use creation will fail.  A leading \"_\" is reserved for automatically named anchors. Do not start your names with an underscore.  The name 'None' is illegal.\n@param PinToWorldTransform           The Pin which the component will be updated by." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "CreateNamedARPin", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventCreateNamedARPin_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics
	{
		struct HoloLensARFunctionLibrary_eventGetPVCameraIntrinsics_Parms
		{
			FVector2D focalLength;
			int32 width;
			int32 height;
			FVector2D principalPoint;
			FVector radialDistortion;
			FVector2D tangentialDistortion;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_focalLength;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_width;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_height;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_principalPoint;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_radialDistortion;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_tangentialDistortion;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_focalLength = { "focalLength", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventGetPVCameraIntrinsics_Parms, focalLength), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_width = { "width", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventGetPVCameraIntrinsics_Parms, width), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_height = { "height", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventGetPVCameraIntrinsics_Parms, height), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_principalPoint = { "principalPoint", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventGetPVCameraIntrinsics_Parms, principalPoint), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_radialDistortion = { "radialDistortion", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventGetPVCameraIntrinsics_Parms, radialDistortion), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_tangentialDistortion = { "tangentialDistortion", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventGetPVCameraIntrinsics_Parms, tangentialDistortion), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((HoloLensARFunctionLibrary_eventGetPVCameraIntrinsics_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HoloLensARFunctionLibrary_eventGetPVCameraIntrinsics_Parms), &Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_focalLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_principalPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_radialDistortion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_tangentialDistortion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "Comment", "/**\n\x09 * Get the PV Camera intrinsics.\n\x09 */" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Get the PV Camera intrinsics." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "GetPVCameraIntrinsics", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventGetPVCameraIntrinsics_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics
	{
		struct HoloLensARFunctionLibrary_eventGetPVCameraToWorldTransform_Parms
		{
			FTransform ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventGetPVCameraToWorldTransform_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "Comment", "/**\n\x09 * Get the transform from PV camera space to Unreal world space.\n\x09 */" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Get the transform from PV camera space to Unreal world space." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "GetPVCameraToWorldTransform", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventGetPVCameraToWorldTransform_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics
	{
		struct HoloLensARFunctionLibrary_eventGetWorldSpaceRayFromCameraPoint_Parms
		{
			FVector2D pixelCoordinate;
			FVector ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_pixelCoordinate;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::NewProp_pixelCoordinate = { "pixelCoordinate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventGetWorldSpaceRayFromCameraPoint_Parms, pixelCoordinate), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventGetWorldSpaceRayFromCameraPoint_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::NewProp_pixelCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "Comment", "/**\n\x09 * Get a ray into the scene from a camera point.\n\x09 * X is left/right\n\x09 * Y is up/down\n\x09 */" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Get a ray into the scene from a camera point.\nX is left/right\nY is up/down" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "GetWorldSpaceRayFromCameraPoint", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventGetWorldSpaceRayFromCameraPoint_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics
	{
		struct HoloLensARFunctionLibrary_eventHideKeyboard_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((HoloLensARFunctionLibrary_eventHideKeyboard_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HoloLensARFunctionLibrary_eventHideKeyboard_Parms), &Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "The keyboard should be auto-shown and hidden" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "HideKeyboard", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventHideKeyboard_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics
	{
		struct HoloLensARFunctionLibrary_eventLoadWMRAnchorStoreARPins_Parms
		{
			TArray<UWMRARPin*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UWMRARPin_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventLoadWMRAnchorStoreARPins_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR|ARPin" },
		{ "Comment", "/**\n\x09 * Load all ARPins from the Windows Mixed Reality Anchor Store.\n\x09 * Note: Pins of the same name as anchor store pins will be overwritten by the anchor store pin.\n\x09 *\n\x09 * @return\x09\x09\x09\x09\x09""Array of Pins that were loaded.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Please use LoadARPinsFromLocalStore" },
		{ "Keywords", "hololensar wmr pin ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Load all ARPins from the Windows Mixed Reality Anchor Store.\nNote: Pins of the same name as anchor store pins will be overwritten by the anchor store pin.\n\n@return                                      Array of Pins that were loaded." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "LoadWMRAnchorStoreARPins", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventLoadWMRAnchorStoreARPins_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics
	{
		struct HoloLensARFunctionLibrary_eventRemoveARPinFromWMRAnchorStore_Parms
		{
			UARPin* InPin;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPin;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics::NewProp_InPin = { "InPin", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventRemoveARPinFromWMRAnchorStore_Parms, InPin), Z_Construct_UClass_UARPin_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics::NewProp_InPin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR|ARPin" },
		{ "Comment", "/**\n\x09 * Remove an ARPin from the the Windows Mixed Reality Anchor Store.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Please use RemoveARPinFromLocalStore" },
		{ "Keywords", "hololensar wmr pin ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Remove an ARPin from the the Windows Mixed Reality Anchor Store." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "RemoveARPinFromWMRAnchorStore", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventRemoveARPinFromWMRAnchorStore_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics
	{
		struct HoloLensARFunctionLibrary_eventResizeMixedRealityCamera_Parms
		{
			FIntPoint size;
			FIntPoint ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_size;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::NewProp_size_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::NewProp_size = { "size", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventResizeMixedRealityCamera_Parms, size), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::NewProp_size_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::NewProp_size_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventResizeMixedRealityCamera_Parms, ReturnValue), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::NewProp_size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "Comment", "/**\n\x09 * Change screen size of Mixed Reality Capture camera.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use ResizeXRCamera" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Change screen size of Mixed Reality Capture camera." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "ResizeMixedRealityCamera", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventResizeMixedRealityCamera_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics
	{
		struct HoloLensARFunctionLibrary_eventSaveARPinToWMRAnchorStore_Parms
		{
			UARPin* InPin;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPin;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::NewProp_InPin = { "InPin", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HoloLensARFunctionLibrary_eventSaveARPinToWMRAnchorStore_Parms, InPin), Z_Construct_UClass_UARPin_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((HoloLensARFunctionLibrary_eventSaveARPinToWMRAnchorStore_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HoloLensARFunctionLibrary_eventSaveARPinToWMRAnchorStore_Parms), &Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::NewProp_InPin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR|ARPin" },
		{ "Comment", "/**\n\x09 * Save an ARPin to the the Windows Mixed Reality Anchor Store.\n\x09 *\n\x09 * @return\x09\x09\x09\x09\x09True if saved successfully.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Please use SaveARPinToLocalStore" },
		{ "Keywords", "hololensar wmr pin ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Save an ARPin to the the Windows Mixed Reality Anchor Store.\n\n@return                                      True if saved successfully." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "SaveARPinToWMRAnchorStore", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventSaveARPinToWMRAnchorStore_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics
	{
		struct HoloLensARFunctionLibrary_eventSetEnabledMixedRealityCamera_Parms
		{
			bool IsEnabled;
		};
		static void NewProp_IsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::NewProp_IsEnabled_SetBit(void* Obj)
	{
		((HoloLensARFunctionLibrary_eventSetEnabledMixedRealityCamera_Parms*)Obj)->IsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::NewProp_IsEnabled = { "IsEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HoloLensARFunctionLibrary_eventSetEnabledMixedRealityCamera_Parms), &Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::NewProp_IsEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::NewProp_IsEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "Comment", "/**\n\x09 * Enable or disable Mixed Reality Capture camera.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use SetEnabledXRCamera" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Enable or disable Mixed Reality Capture camera." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "SetEnabledMixedRealityCamera", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventSetEnabledMixedRealityCamera_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics
	{
		struct HoloLensARFunctionLibrary_eventSetUseLegacyHandMeshVisualization_Parms
		{
			bool UseLegacyHandMeshVisualization;
		};
		static void NewProp_UseLegacyHandMeshVisualization_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_UseLegacyHandMeshVisualization;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::NewProp_UseLegacyHandMeshVisualization_SetBit(void* Obj)
	{
		((HoloLensARFunctionLibrary_eventSetUseLegacyHandMeshVisualization_Parms*)Obj)->UseLegacyHandMeshVisualization = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::NewProp_UseLegacyHandMeshVisualization = { "UseLegacyHandMeshVisualization", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HoloLensARFunctionLibrary_eventSetUseLegacyHandMeshVisualization_Parms), &Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::NewProp_UseLegacyHandMeshVisualization_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::NewProp_UseLegacyHandMeshVisualization,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "Comment", "// Use the legacy MRMesh support for rendering the hand tracker.  Otherwise, default to XRVisualization.\n" },
		{ "Keywords", "hololensar hand mesh ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Use the legacy MRMesh support for rendering the hand tracker.  Otherwise, default to XRVisualization." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "SetUseLegacyHandMeshVisualization", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventSetUseLegacyHandMeshVisualization_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics
	{
		struct HoloLensARFunctionLibrary_eventShowKeyboard_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((HoloLensARFunctionLibrary_eventShowKeyboard_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HoloLensARFunctionLibrary_eventShowKeyboard_Parms), &Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "The keyboard should be auto-shown and hidden" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "ShowKeyboard", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventShowKeyboard_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics
	{
		struct HoloLensARFunctionLibrary_eventStartCameraCapture_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((HoloLensARFunctionLibrary_eventStartCameraCapture_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HoloLensARFunctionLibrary_eventStartCameraCapture_Parms), &Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "Comment", "/**\n\x09 * Turn the camera on.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use ToggleARCapture" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Turn the camera on." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "StartCameraCapture", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventStartCameraCapture_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics
	{
		struct HoloLensARFunctionLibrary_eventStartQRCodeCapture_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((HoloLensARFunctionLibrary_eventStartQRCodeCapture_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HoloLensARFunctionLibrary_eventStartQRCodeCapture_Parms), &Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "Comment", "/**\n\x09 * Start looking for QRCodes.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use ToggleARCapture" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Start looking for QRCodes." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "StartQRCodeCapture", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventStartQRCodeCapture_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics
	{
		struct HoloLensARFunctionLibrary_eventStopCameraCapture_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((HoloLensARFunctionLibrary_eventStopCameraCapture_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HoloLensARFunctionLibrary_eventStopCameraCapture_Parms), &Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "Comment", "/**\n\x09 * Turn the camera off.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use ToggleARCapture" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Turn the camera off." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "StopCameraCapture", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventStopCameraCapture_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics
	{
		struct HoloLensARFunctionLibrary_eventStopQRCodeCapture_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((HoloLensARFunctionLibrary_eventStopQRCodeCapture_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HoloLensARFunctionLibrary_eventStopQRCodeCapture_Parms), &Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "HoloLensAR" },
		{ "Comment", "/**\n\x09 * Stop looking for QRCodes.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use ToggleARCapture" },
		{ "Keywords", "hololensar wmr ar all" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "Stop looking for QRCodes." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoloLensARFunctionLibrary, nullptr, "StopQRCodeCapture", nullptr, nullptr, sizeof(HoloLensARFunctionLibrary_eventStopQRCodeCapture_Parms), Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UHoloLensARFunctionLibrary_NoRegister()
	{
		return UHoloLensARFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UHoloLensARFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHoloLensARFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_HoloLensAR,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UHoloLensARFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_CreateNamedARPin, "CreateNamedARPin" }, // 986294708
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraIntrinsics, "GetPVCameraIntrinsics" }, // 2792838901
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetPVCameraToWorldTransform, "GetPVCameraToWorldTransform" }, // 872915131
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_GetWorldSpaceRayFromCameraPoint, "GetWorldSpaceRayFromCameraPoint" }, // 853132550
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_HideKeyboard, "HideKeyboard" }, // 2802932710
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_LoadWMRAnchorStoreARPins, "LoadWMRAnchorStoreARPins" }, // 269764875
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_RemoveARPinFromWMRAnchorStore, "RemoveARPinFromWMRAnchorStore" }, // 3189462286
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_ResizeMixedRealityCamera, "ResizeMixedRealityCamera" }, // 1620548635
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_SaveARPinToWMRAnchorStore, "SaveARPinToWMRAnchorStore" }, // 1277124285
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetEnabledMixedRealityCamera, "SetEnabledMixedRealityCamera" }, // 3843304464
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_SetUseLegacyHandMeshVisualization, "SetUseLegacyHandMeshVisualization" }, // 3258923633
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_ShowKeyboard, "ShowKeyboard" }, // 3305164420
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartCameraCapture, "StartCameraCapture" }, // 1182041183
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_StartQRCodeCapture, "StartQRCodeCapture" }, // 4124639411
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopCameraCapture, "StopCameraCapture" }, // 3461566769
		{ &Z_Construct_UFunction_UHoloLensARFunctionLibrary_StopQRCodeCapture, "StopQRCodeCapture" }, // 3827745405
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensARFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A function library that provides static/Blueprint functions for HoloLensAR.*/" },
		{ "IncludePath", "HoloLensARFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/HoloLensARFunctionLibrary.h" },
		{ "ToolTip", "A function library that provides static/Blueprint functions for HoloLensAR." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHoloLensARFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHoloLensARFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHoloLensARFunctionLibrary_Statics::ClassParams = {
		&UHoloLensARFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UHoloLensARFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensARFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHoloLensARFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHoloLensARFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHoloLensARFunctionLibrary, 2183596165);
	template<> HOLOLENSAR_API UClass* StaticClass<UHoloLensARFunctionLibrary>()
	{
		return UHoloLensARFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHoloLensARFunctionLibrary(Z_Construct_UClass_UHoloLensARFunctionLibrary, &UHoloLensARFunctionLibrary::StaticClass, TEXT("/Script/HoloLensAR"), TEXT("UHoloLensARFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHoloLensARFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
