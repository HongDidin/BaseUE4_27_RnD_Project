// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WINDOWSMIXEDREALITYHANDTRACKING_WindowsMixedRealityHandTrackingTypes_generated_h
#error "WindowsMixedRealityHandTrackingTypes.generated.h already included, missing '#pragma once' in WindowsMixedRealityHandTrackingTypes.h"
#endif
#define WINDOWSMIXEDREALITYHANDTRACKING_WindowsMixedRealityHandTrackingTypes_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Public_WindowsMixedRealityHandTrackingTypes_h


#define FOREACH_ENUM_EWMRHANDKEYPOINT(op) \
	op(EWMRHandKeypoint::Palm) \
	op(EWMRHandKeypoint::Wrist) \
	op(EWMRHandKeypoint::ThumbMetacarpal) \
	op(EWMRHandKeypoint::ThumbProximal) \
	op(EWMRHandKeypoint::ThumbDistal) \
	op(EWMRHandKeypoint::ThumbTip) \
	op(EWMRHandKeypoint::IndexMetacarpal) \
	op(EWMRHandKeypoint::IndexProximal) \
	op(EWMRHandKeypoint::IndexIntermediate) \
	op(EWMRHandKeypoint::IndexDistal) \
	op(EWMRHandKeypoint::IndexTip) \
	op(EWMRHandKeypoint::MiddleMetacarpal) \
	op(EWMRHandKeypoint::MiddleProximal) \
	op(EWMRHandKeypoint::MiddleIntermediate) \
	op(EWMRHandKeypoint::MiddleDistal) \
	op(EWMRHandKeypoint::MiddleTip) \
	op(EWMRHandKeypoint::RingMetacarpal) \
	op(EWMRHandKeypoint::RingProximal) \
	op(EWMRHandKeypoint::RingIntermediate) \
	op(EWMRHandKeypoint::RingDistal) \
	op(EWMRHandKeypoint::RingTip) \
	op(EWMRHandKeypoint::LittleMetacarpal) \
	op(EWMRHandKeypoint::LittleProximal) \
	op(EWMRHandKeypoint::LittleIntermediate) \
	op(EWMRHandKeypoint::LittleDistal) \
	op(EWMRHandKeypoint::LittleTip) 

enum class EWMRHandKeypoint : uint8;
template<> WINDOWSMIXEDREALITYHANDTRACKING_API UEnum* StaticEnum<EWMRHandKeypoint>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
