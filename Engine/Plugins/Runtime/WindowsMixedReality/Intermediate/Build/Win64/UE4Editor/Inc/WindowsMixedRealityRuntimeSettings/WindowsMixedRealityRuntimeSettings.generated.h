// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WINDOWSMIXEDREALITYRUNTIMESETTINGS_WindowsMixedRealityRuntimeSettings_generated_h
#error "WindowsMixedRealityRuntimeSettings.generated.h already included, missing '#pragma once' in WindowsMixedRealityRuntimeSettings.h"
#endif
#define WINDOWSMIXEDREALITYRUNTIMESETTINGS_WindowsMixedRealityRuntimeSettings_generated_h

#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_SPARSE_DATA
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_RPC_WRAPPERS
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWindowsMixedRealityRuntimeSettings(); \
	friend struct Z_Construct_UClass_UWindowsMixedRealityRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(UWindowsMixedRealityRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/WindowsMixedRealityRuntimeSettings"), NO_API) \
	DECLARE_SERIALIZER(UWindowsMixedRealityRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUWindowsMixedRealityRuntimeSettings(); \
	friend struct Z_Construct_UClass_UWindowsMixedRealityRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(UWindowsMixedRealityRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/WindowsMixedRealityRuntimeSettings"), NO_API) \
	DECLARE_SERIALIZER(UWindowsMixedRealityRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWindowsMixedRealityRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWindowsMixedRealityRuntimeSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWindowsMixedRealityRuntimeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWindowsMixedRealityRuntimeSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWindowsMixedRealityRuntimeSettings(UWindowsMixedRealityRuntimeSettings&&); \
	NO_API UWindowsMixedRealityRuntimeSettings(const UWindowsMixedRealityRuntimeSettings&); \
public:


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWindowsMixedRealityRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWindowsMixedRealityRuntimeSettings(UWindowsMixedRealityRuntimeSettings&&); \
	NO_API UWindowsMixedRealityRuntimeSettings(const UWindowsMixedRealityRuntimeSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWindowsMixedRealityRuntimeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWindowsMixedRealityRuntimeSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWindowsMixedRealityRuntimeSettings)


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_20_PROLOG
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_RPC_WRAPPERS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_INCLASS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h_24_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WINDOWSMIXEDREALITYRUNTIMESETTINGS_API UClass* StaticClass<class UWindowsMixedRealityRuntimeSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityRuntimeSettings_Classes_WindowsMixedRealityRuntimeSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
