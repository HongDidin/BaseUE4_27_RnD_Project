// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "WindowsMixedRealityHMD/Public/WindowsMixedRealityFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWindowsMixedRealityFunctionLibrary() {}
// Cross Module References
	WINDOWSMIXEDREALITYHMD_API UFunction* Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_WindowsMixedRealityHMD();
	WINDOWSMIXEDREALITYHMD_API UEnum* Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDSpatialLocatability();
	WINDOWSMIXEDREALITYHMD_API UEnum* Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDTrackingStatus();
	WINDOWSMIXEDREALITYHMD_API UEnum* Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDInputControllerButtons();
	WINDOWSMIXEDREALITYHMD_API UScriptStruct* Z_Construct_UScriptStruct_FPointerPoseInfo();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	WINDOWSMIXEDREALITYHMD_API UClass* Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_NoRegister();
	WINDOWSMIXEDREALITYHMD_API UClass* Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	INPUTCORE_API UEnum* Z_Construct_UEnum_InputCore_EControllerHand();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics
	{
		struct _Script_WindowsMixedRealityHMD_eventTrackingChangeCallback_Parms
		{
			EHMDSpatialLocatability locatability;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_locatability_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_locatability;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::NewProp_locatability_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::NewProp_locatability = { "locatability", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_WindowsMixedRealityHMD_eventTrackingChangeCallback_Parms, locatability), Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDSpatialLocatability, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::NewProp_locatability_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::NewProp_locatability,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_WindowsMixedRealityHMD, nullptr, "TrackingChangeCallback__DelegateSignature", nullptr, nullptr, sizeof(_Script_WindowsMixedRealityHMD_eventTrackingChangeCallback_Parms), Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_WindowsMixedRealityHMD_TrackingChangeCallback__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EHMDTrackingStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDTrackingStatus, Z_Construct_UPackage__Script_WindowsMixedRealityHMD(), TEXT("EHMDTrackingStatus"));
		}
		return Singleton;
	}
	template<> WINDOWSMIXEDREALITYHMD_API UEnum* StaticEnum<EHMDTrackingStatus>()
	{
		return EHMDTrackingStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHMDTrackingStatus(EHMDTrackingStatus_StaticEnum, TEXT("/Script/WindowsMixedRealityHMD"), TEXT("EHMDTrackingStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDTrackingStatus_Hash() { return 3952016360U; }
	UEnum* Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDTrackingStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_WindowsMixedRealityHMD();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHMDTrackingStatus"), 0, Get_Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDTrackingStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHMDTrackingStatus::NotTracked", (int64)EHMDTrackingStatus::NotTracked },
				{ "EHMDTrackingStatus::InertialOnly", (int64)EHMDTrackingStatus::InertialOnly },
				{ "EHMDTrackingStatus::Tracked", (int64)EHMDTrackingStatus::Tracked },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "InertialOnly.Name", "EHMDTrackingStatus::InertialOnly" },
				{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
				{ "NotTracked.Name", "EHMDTrackingStatus::NotTracked" },
				{ "Tracked.Name", "EHMDTrackingStatus::Tracked" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_WindowsMixedRealityHMD,
				nullptr,
				"EHMDTrackingStatus",
				"EHMDTrackingStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EHMDInputControllerButtons_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDInputControllerButtons, Z_Construct_UPackage__Script_WindowsMixedRealityHMD(), TEXT("EHMDInputControllerButtons"));
		}
		return Singleton;
	}
	template<> WINDOWSMIXEDREALITYHMD_API UEnum* StaticEnum<EHMDInputControllerButtons>()
	{
		return EHMDInputControllerButtons_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHMDInputControllerButtons(EHMDInputControllerButtons_StaticEnum, TEXT("/Script/WindowsMixedRealityHMD"), TEXT("EHMDInputControllerButtons"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDInputControllerButtons_Hash() { return 3199241175U; }
	UEnum* Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDInputControllerButtons()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_WindowsMixedRealityHMD();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHMDInputControllerButtons"), 0, Get_Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDInputControllerButtons_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHMDInputControllerButtons::Select", (int64)EHMDInputControllerButtons::Select },
				{ "EHMDInputControllerButtons::Grasp", (int64)EHMDInputControllerButtons::Grasp },
				{ "EHMDInputControllerButtons::Menu", (int64)EHMDInputControllerButtons::Menu },
				{ "EHMDInputControllerButtons::Thumbstick", (int64)EHMDInputControllerButtons::Thumbstick },
				{ "EHMDInputControllerButtons::Touchpad", (int64)EHMDInputControllerButtons::Touchpad },
				{ "EHMDInputControllerButtons::TouchpadIsTouched", (int64)EHMDInputControllerButtons::TouchpadIsTouched },
				{ "EHMDInputControllerButtons::Count", (int64)EHMDInputControllerButtons::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "WindowsMixedRealityHMD" },
				{ "Count.Hidden", "" },
				{ "Count.Name", "EHMDInputControllerButtons::Count" },
				{ "Grasp.Name", "EHMDInputControllerButtons::Grasp" },
				{ "Menu.Name", "EHMDInputControllerButtons::Menu" },
				{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
				{ "Select.Name", "EHMDInputControllerButtons::Select" },
				{ "Thumbstick.Name", "EHMDInputControllerButtons::Thumbstick" },
				{ "Touchpad.Name", "EHMDInputControllerButtons::Touchpad" },
				{ "TouchpadIsTouched.Name", "EHMDInputControllerButtons::TouchpadIsTouched" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_WindowsMixedRealityHMD,
				nullptr,
				"EHMDInputControllerButtons",
				"EHMDInputControllerButtons",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EHMDSpatialLocatability_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDSpatialLocatability, Z_Construct_UPackage__Script_WindowsMixedRealityHMD(), TEXT("EHMDSpatialLocatability"));
		}
		return Singleton;
	}
	template<> WINDOWSMIXEDREALITYHMD_API UEnum* StaticEnum<EHMDSpatialLocatability>()
	{
		return EHMDSpatialLocatability_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHMDSpatialLocatability(EHMDSpatialLocatability_StaticEnum, TEXT("/Script/WindowsMixedRealityHMD"), TEXT("EHMDSpatialLocatability"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDSpatialLocatability_Hash() { return 3988877252U; }
	UEnum* Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDSpatialLocatability()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_WindowsMixedRealityHMD();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHMDSpatialLocatability"), 0, Get_Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDSpatialLocatability_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHMDSpatialLocatability::Unavailable", (int64)EHMDSpatialLocatability::Unavailable },
				{ "EHMDSpatialLocatability::OrientationOnly", (int64)EHMDSpatialLocatability::OrientationOnly },
				{ "EHMDSpatialLocatability::PositionalTrackingActivating", (int64)EHMDSpatialLocatability::PositionalTrackingActivating },
				{ "EHMDSpatialLocatability::PositionalTrackingActive", (int64)EHMDSpatialLocatability::PositionalTrackingActive },
				{ "EHMDSpatialLocatability::PositionalTrackingInhibited", (int64)EHMDSpatialLocatability::PositionalTrackingInhibited },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "WindowsMixedRealityHMD" },
				{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
				{ "OrientationOnly.Name", "EHMDSpatialLocatability::OrientationOnly" },
				{ "PositionalTrackingActivating.Name", "EHMDSpatialLocatability::PositionalTrackingActivating" },
				{ "PositionalTrackingActive.Name", "EHMDSpatialLocatability::PositionalTrackingActive" },
				{ "PositionalTrackingInhibited.Name", "EHMDSpatialLocatability::PositionalTrackingInhibited" },
				{ "Unavailable.Name", "EHMDSpatialLocatability::Unavailable" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_WindowsMixedRealityHMD,
				nullptr,
				"EHMDSpatialLocatability",
				"EHMDSpatialLocatability",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FPointerPoseInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WINDOWSMIXEDREALITYHMD_API uint32 Get_Z_Construct_UScriptStruct_FPointerPoseInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPointerPoseInfo, Z_Construct_UPackage__Script_WindowsMixedRealityHMD(), TEXT("PointerPoseInfo"), sizeof(FPointerPoseInfo), Get_Z_Construct_UScriptStruct_FPointerPoseInfo_Hash());
	}
	return Singleton;
}
template<> WINDOWSMIXEDREALITYHMD_API UScriptStruct* StaticStruct<FPointerPoseInfo>()
{
	return FPointerPoseInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPointerPoseInfo(FPointerPoseInfo::StaticStruct, TEXT("/Script/WindowsMixedRealityHMD"), TEXT("PointerPoseInfo"), false, nullptr, nullptr);
static struct FScriptStruct_WindowsMixedRealityHMD_StaticRegisterNativesFPointerPoseInfo
{
	FScriptStruct_WindowsMixedRealityHMD_StaticRegisterNativesFPointerPoseInfo()
	{
		UScriptStruct::DeferCppStructOps<FPointerPoseInfo>(FName(TEXT("PointerPoseInfo")));
	}
} ScriptStruct_WindowsMixedRealityHMD_StaticRegisterNativesFPointerPoseInfo;
	struct Z_Construct_UScriptStruct_FPointerPoseInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Origin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Origin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Direction_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Direction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Up_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Up;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Orientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Orientation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TrackingStatus_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingStatus_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TrackingStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "WindowsMixedRealityHMD" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPointerPoseInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Origin_MetaData[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Origin = { "Origin", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPointerPoseInfo, Origin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Origin_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Origin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Direction_MetaData[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Direction = { "Direction", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPointerPoseInfo, Direction), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Direction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Direction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Up_MetaData[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Up = { "Up", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPointerPoseInfo, Up), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Up_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Up_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Orientation_MetaData[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Orientation = { "Orientation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPointerPoseInfo, Orientation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Orientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Orientation_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_TrackingStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_TrackingStatus_MetaData[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_TrackingStatus = { "TrackingStatus", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPointerPoseInfo, TrackingStatus), Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDTrackingStatus, METADATA_PARAMS(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_TrackingStatus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_TrackingStatus_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Origin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Direction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Up,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_Orientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_TrackingStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::NewProp_TrackingStatus,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WindowsMixedRealityHMD,
		nullptr,
		&NewStructOps,
		"PointerPoseInfo",
		sizeof(FPointerPoseInfo),
		alignof(FPointerPoseInfo),
		Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPointerPoseInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPointerPoseInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WindowsMixedRealityHMD();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PointerPoseInfo"), sizeof(FPointerPoseInfo), Get_Z_Construct_UScriptStruct_FPointerPoseInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPointerPoseInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPointerPoseInfo_Hash() { return 4219586260U; }
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execSetFocusPointForFrame)
	{
		P_GET_STRUCT(FVector,Z_Param_position);
		P_FINISH;
		P_NATIVE_BEGIN;
		UWindowsMixedRealityFunctionLibrary::SetFocusPointForFrame(Z_Param_position);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execGetControllerTrackingStatus)
	{
		P_GET_ENUM(EControllerHand,Z_Param_hand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EHMDTrackingStatus*)Z_Param__Result=UWindowsMixedRealityFunctionLibrary::GetControllerTrackingStatus(EControllerHand(Z_Param_hand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execIsSelectPressed)
	{
		P_GET_ENUM(EControllerHand,Z_Param_hand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UWindowsMixedRealityFunctionLibrary::IsSelectPressed(EControllerHand(Z_Param_hand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execIsGrasped)
	{
		P_GET_ENUM(EControllerHand,Z_Param_hand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UWindowsMixedRealityFunctionLibrary::IsGrasped(EControllerHand(Z_Param_hand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execIsButtonDown)
	{
		P_GET_ENUM(EControllerHand,Z_Param_hand);
		P_GET_ENUM(EHMDInputControllerButtons,Z_Param_button);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UWindowsMixedRealityFunctionLibrary::IsButtonDown(EControllerHand(Z_Param_hand),EHMDInputControllerButtons(Z_Param_button));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execIsButtonClicked)
	{
		P_GET_ENUM(EControllerHand,Z_Param_hand);
		P_GET_ENUM(EHMDInputControllerButtons,Z_Param_button);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UWindowsMixedRealityFunctionLibrary::IsButtonClicked(EControllerHand(Z_Param_hand),EHMDInputControllerButtons(Z_Param_button));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execGetPointerPoseInfo)
	{
		P_GET_ENUM(EControllerHand,Z_Param_hand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FPointerPoseInfo*)Z_Param__Result=UWindowsMixedRealityFunctionLibrary::GetPointerPoseInfo(EControllerHand(Z_Param_hand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execIsTrackingAvailable)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UWindowsMixedRealityFunctionLibrary::IsTrackingAvailable();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execLockMouseToCenter)
	{
		P_GET_UBOOL(Z_Param_locked);
		P_FINISH;
		P_NATIVE_BEGIN;
		UWindowsMixedRealityFunctionLibrary::LockMouseToCenter(Z_Param_locked);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execIsDisplayOpaque)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UWindowsMixedRealityFunctionLibrary::IsDisplayOpaque();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execIsCurrentlyImmersive)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UWindowsMixedRealityFunctionLibrary::IsCurrentlyImmersive();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execToggleImmersive)
	{
		P_GET_UBOOL(Z_Param_immersive);
		P_FINISH;
		P_NATIVE_BEGIN;
		UWindowsMixedRealityFunctionLibrary::ToggleImmersive(Z_Param_immersive);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityFunctionLibrary::execGetVersionString)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UWindowsMixedRealityFunctionLibrary::GetVersionString();
		P_NATIVE_END;
	}
	void UWindowsMixedRealityFunctionLibrary::StaticRegisterNativesUWindowsMixedRealityFunctionLibrary()
	{
		UClass* Class = UWindowsMixedRealityFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetControllerTrackingStatus", &UWindowsMixedRealityFunctionLibrary::execGetControllerTrackingStatus },
			{ "GetPointerPoseInfo", &UWindowsMixedRealityFunctionLibrary::execGetPointerPoseInfo },
			{ "GetVersionString", &UWindowsMixedRealityFunctionLibrary::execGetVersionString },
			{ "IsButtonClicked", &UWindowsMixedRealityFunctionLibrary::execIsButtonClicked },
			{ "IsButtonDown", &UWindowsMixedRealityFunctionLibrary::execIsButtonDown },
			{ "IsCurrentlyImmersive", &UWindowsMixedRealityFunctionLibrary::execIsCurrentlyImmersive },
			{ "IsDisplayOpaque", &UWindowsMixedRealityFunctionLibrary::execIsDisplayOpaque },
			{ "IsGrasped", &UWindowsMixedRealityFunctionLibrary::execIsGrasped },
			{ "IsSelectPressed", &UWindowsMixedRealityFunctionLibrary::execIsSelectPressed },
			{ "IsTrackingAvailable", &UWindowsMixedRealityFunctionLibrary::execIsTrackingAvailable },
			{ "LockMouseToCenter", &UWindowsMixedRealityFunctionLibrary::execLockMouseToCenter },
			{ "SetFocusPointForFrame", &UWindowsMixedRealityFunctionLibrary::execSetFocusPointForFrame },
			{ "ToggleImmersive", &UWindowsMixedRealityFunctionLibrary::execToggleImmersive },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventGetControllerTrackingStatus_Parms
		{
			EControllerHand hand;
			EHMDTrackingStatus ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::NewProp_hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::NewProp_hand = { "hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventGetControllerTrackingStatus_Parms, hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventGetControllerTrackingStatus_Parms, ReturnValue), Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDTrackingStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::NewProp_hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::NewProp_hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09 * Returns tracking state for the controller\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use GetMotionControllerData TrackingStatus" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Returns tracking state for the controller" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "GetControllerTrackingStatus", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventGetControllerTrackingStatus_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventGetPointerPoseInfo_Parms
		{
			EControllerHand hand;
			FPointerPoseInfo ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_hand;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::NewProp_hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::NewProp_hand = { "hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventGetPointerPoseInfo_Parms, hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventGetPointerPoseInfo_Parms, ReturnValue), Z_Construct_UScriptStruct_FPointerPoseInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::NewProp_hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::NewProp_hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09 * Returns the pose information to determine what a WMR device is pointing at.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use GetMotionControllerData Aim members" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Returns the pose information to determine what a WMR device is pointing at." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "GetPointerPoseInfo", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventGetPointerPoseInfo_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventGetVersionString_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventGetVersionString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09* Returns name of WindowsMR device type.\n\x09*/" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Generic HMD library version" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Returns name of WindowsMR device type." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "GetVersionString", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventGetVersionString_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventIsButtonClicked_Parms
		{
			EControllerHand hand;
			EHMDInputControllerButtons button;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_button_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_button;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_hand = { "hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventIsButtonClicked_Parms, hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_button_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_button = { "button", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventIsButtonClicked_Parms, button), Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDInputControllerButtons, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WindowsMixedRealityFunctionLibrary_eventIsButtonClicked_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityFunctionLibrary_eventIsButtonClicked_Parms), &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_button_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_button,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09 * Returns true if the button was clicked.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Input Action Mappings to get events" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Returns true if the button was clicked." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "IsButtonClicked", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventIsButtonClicked_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventIsButtonDown_Parms
		{
			EControllerHand hand;
			EHMDInputControllerButtons button;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_button_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_button;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_hand = { "hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventIsButtonDown_Parms, hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_button_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_button = { "button", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventIsButtonDown_Parms, button), Z_Construct_UEnum_WindowsMixedRealityHMD_EHMDInputControllerButtons, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WindowsMixedRealityFunctionLibrary_eventIsButtonDown_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityFunctionLibrary_eventIsButtonDown_Parms), &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_button_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_button,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09 * Returns true if the button is held down.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Input Action Mappings to gets events" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Returns true if the button is held down." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "IsButtonDown", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventIsButtonDown_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventIsCurrentlyImmersive_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WindowsMixedRealityFunctionLibrary_eventIsCurrentlyImmersive_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityFunctionLibrary_eventIsCurrentlyImmersive_Parms), &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09* Returns true if currently rendering immersive, or false if rendering as a slate.\n\x09*/" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Returns true if currently rendering immersive, or false if rendering as a slate." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "IsCurrentlyImmersive", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventIsCurrentlyImmersive_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventIsDisplayOpaque_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WindowsMixedRealityFunctionLibrary_eventIsDisplayOpaque_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityFunctionLibrary_eventIsDisplayOpaque_Parms), &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09* Returns true if running on a WMR VR device, false if running on HoloLens.\n\x09*/" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use GetXRSystemFlags and check for bIsAR" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Returns true if running on a WMR VR device, false if running on HoloLens." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "IsDisplayOpaque", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventIsDisplayOpaque_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventIsGrasped_Parms
		{
			EControllerHand hand;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_hand;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::NewProp_hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::NewProp_hand = { "hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventIsGrasped_Parms, hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WindowsMixedRealityFunctionLibrary_eventIsGrasped_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityFunctionLibrary_eventIsGrasped_Parms), &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::NewProp_hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::NewProp_hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09 * Returns true if an input device detects a grasp/grab action.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Input Action Mappings to get left or right grip events.  Also available in GetMotionControllerData" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Returns true if an input device detects a grasp/grab action." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "IsGrasped", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventIsGrasped_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventIsSelectPressed_Parms
		{
			EControllerHand hand;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_hand;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::NewProp_hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::NewProp_hand = { "hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventIsSelectPressed_Parms, hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WindowsMixedRealityFunctionLibrary_eventIsSelectPressed_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityFunctionLibrary_eventIsSelectPressed_Parms), &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::NewProp_hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::NewProp_hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09 * Returns true if a hand or motion controller is experiencing a primary Select press.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Input Action Mappings to get left or right trigger events" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Returns true if a hand or motion controller is experiencing a primary Select press." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "IsSelectPressed", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventIsSelectPressed_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventIsTrackingAvailable_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WindowsMixedRealityFunctionLibrary_eventIsTrackingAvailable_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityFunctionLibrary_eventIsTrackingAvailable_Parms), &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09 * Returns true if a WMR VR device or HoloLens are tracking the environment.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use GetHMDData TrackingStatus member" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Returns true if a WMR VR device or HoloLens are tracking the environment." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "IsTrackingAvailable", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventIsTrackingAvailable_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventLockMouseToCenter_Parms
		{
			bool locked;
		};
		static void NewProp_locked_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_locked;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::NewProp_locked_SetBit(void* Obj)
	{
		((WindowsMixedRealityFunctionLibrary_eventLockMouseToCenter_Parms*)Obj)->locked = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::NewProp_locked = { "locked", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityFunctionLibrary_eventLockMouseToCenter_Parms), &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::NewProp_locked_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::NewProp_locked,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "No longer needed" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "LockMouseToCenter", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventLockMouseToCenter_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventSetFocusPointForFrame_Parms
		{
			FVector position;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_position;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics::NewProp_position = { "position", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityFunctionLibrary_eventSetFocusPointForFrame_Parms, position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics::NewProp_position,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "No longer needed" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "SetFocusPointForFrame", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventSetFocusPointForFrame_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics
	{
		struct WindowsMixedRealityFunctionLibrary_eventToggleImmersive_Parms
		{
			bool immersive;
		};
		static void NewProp_immersive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_immersive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::NewProp_immersive_SetBit(void* Obj)
	{
		((WindowsMixedRealityFunctionLibrary_eventToggleImmersive_Parms*)Obj)->immersive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::NewProp_immersive = { "immersive", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityFunctionLibrary_eventToggleImmersive_Parms), &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::NewProp_immersive_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::NewProp_immersive,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::Function_MetaDataParams[] = {
		{ "Category", "WindowsMixedRealityHMD" },
		{ "Comment", "/**\n\x09* Sets game context to immersive or slate.\n\x09* immersive: true for immersive context, false for slate.\n\x09*/" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Sets game context to immersive or slate.\nimmersive: true for immersive context, false for slate." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, nullptr, "ToggleImmersive", nullptr, nullptr, sizeof(WindowsMixedRealityFunctionLibrary_eventToggleImmersive_Parms), Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_NoRegister()
	{
		return UWindowsMixedRealityFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_WindowsMixedRealityHMD,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetControllerTrackingStatus, "GetControllerTrackingStatus" }, // 1276814868
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetPointerPoseInfo, "GetPointerPoseInfo" }, // 354916934
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_GetVersionString, "GetVersionString" }, // 2810403532
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonClicked, "IsButtonClicked" }, // 4254010281
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsButtonDown, "IsButtonDown" }, // 3149642642
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsCurrentlyImmersive, "IsCurrentlyImmersive" }, // 3832621028
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsDisplayOpaque, "IsDisplayOpaque" }, // 3454889001
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsGrasped, "IsGrasped" }, // 1131939060
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsSelectPressed, "IsSelectPressed" }, // 1015162988
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_IsTrackingAvailable, "IsTrackingAvailable" }, // 1303918669
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_LockMouseToCenter, "LockMouseToCenter" }, // 1412508047
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_SetFocusPointForFrame, "SetFocusPointForFrame" }, // 1766131084
		{ &Z_Construct_UFunction_UWindowsMixedRealityFunctionLibrary_ToggleImmersive, "ToggleImmersive" }, // 577087934
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Windows Mixed Reality Extensions Function Library\n*/" },
		{ "IncludePath", "WindowsMixedRealityFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityFunctionLibrary.h" },
		{ "ToolTip", "Windows Mixed Reality Extensions Function Library" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWindowsMixedRealityFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics::ClassParams = {
		&UWindowsMixedRealityFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWindowsMixedRealityFunctionLibrary, 660979623);
	template<> WINDOWSMIXEDREALITYHMD_API UClass* StaticClass<UWindowsMixedRealityFunctionLibrary>()
	{
		return UWindowsMixedRealityFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWindowsMixedRealityFunctionLibrary(Z_Construct_UClass_UWindowsMixedRealityFunctionLibrary, &UWindowsMixedRealityFunctionLibrary::StaticClass, TEXT("/Script/WindowsMixedRealityHMD"), TEXT("UWindowsMixedRealityFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWindowsMixedRealityFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
