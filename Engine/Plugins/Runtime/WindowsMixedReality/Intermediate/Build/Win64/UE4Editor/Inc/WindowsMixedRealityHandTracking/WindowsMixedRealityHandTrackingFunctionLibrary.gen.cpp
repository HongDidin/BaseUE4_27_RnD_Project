// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "WindowsMixedRealityHandTracking/Public/WindowsMixedRealityHandTrackingFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWindowsMixedRealityHandTrackingFunctionLibrary() {}
// Cross Module References
	WINDOWSMIXEDREALITYHANDTRACKING_API UClass* Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_NoRegister();
	WINDOWSMIXEDREALITYHANDTRACKING_API UClass* Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_WindowsMixedRealityHandTracking();
	INPUTCORE_API UEnum* Z_Construct_UEnum_InputCore_EControllerHand();
	WINDOWSMIXEDREALITYHANDTRACKING_API UEnum* Z_Construct_UEnum_WindowsMixedRealityHandTracking_EWMRHandKeypoint();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References
	DEFINE_FUNCTION(UWindowsMixedRealityHandTrackingFunctionLibrary::execGetHandJointTransform)
	{
		P_GET_ENUM(EControllerHand,Z_Param_Hand);
		P_GET_ENUM(EWMRHandKeypoint,Z_Param_Keypoint);
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_Transform);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_Radius);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UWindowsMixedRealityHandTrackingFunctionLibrary::GetHandJointTransform(EControllerHand(Z_Param_Hand),EWMRHandKeypoint(Z_Param_Keypoint),Z_Param_Out_Transform,Z_Param_Out_Radius);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWindowsMixedRealityHandTrackingFunctionLibrary::execSupportsHandTracking)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UWindowsMixedRealityHandTrackingFunctionLibrary::SupportsHandTracking();
		P_NATIVE_END;
	}
	void UWindowsMixedRealityHandTrackingFunctionLibrary::StaticRegisterNativesUWindowsMixedRealityHandTrackingFunctionLibrary()
	{
		UClass* Class = UWindowsMixedRealityHandTrackingFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetHandJointTransform", &UWindowsMixedRealityHandTrackingFunctionLibrary::execGetHandJointTransform },
			{ "SupportsHandTracking", &UWindowsMixedRealityHandTrackingFunctionLibrary::execSupportsHandTracking },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics
	{
		struct WindowsMixedRealityHandTrackingFunctionLibrary_eventGetHandJointTransform_Parms
		{
			EControllerHand Hand;
			EWMRHandKeypoint Keypoint;
			FTransform Transform;
			float Radius;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Keypoint_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Keypoint;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityHandTrackingFunctionLibrary_eventGetHandJointTransform_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Keypoint_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Keypoint = { "Keypoint", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityHandTrackingFunctionLibrary_eventGetHandJointTransform_Parms, Keypoint), Z_Construct_UEnum_WindowsMixedRealityHandTracking_EWMRHandKeypoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityHandTrackingFunctionLibrary_eventGetHandJointTransform_Parms, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WindowsMixedRealityHandTrackingFunctionLibrary_eventGetHandJointTransform_Parms, Radius), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WindowsMixedRealityHandTrackingFunctionLibrary_eventGetHandJointTransform_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityHandTrackingFunctionLibrary_eventGetHandJointTransform_Parms), &Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Hand_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Keypoint_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Keypoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "HandTracking|WindowsMixedReality" },
		{ "Comment", "/**\n\x09Get Transform for a point on the hand.\n\n\x09@param Hand\n\x09@param Keypoint the specific joint or wrist point to fetch.\n\x09@param Transform The joint's transform.\n\x09@param Radius The distance from the joint position to the surface of the hand.\n\x09@return true if the output param was populated with a valid value, false means that the tracking is lost and output is undefined.\n\x09*/" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use GetMotionControllerData HandKeyPositions and HandKeyRadii" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityHandTrackingFunctionLibrary.h" },
		{ "ToolTip", "Get Transform for a point on the hand.\n\n@param Hand\n@param Keypoint the specific joint or wrist point to fetch.\n@param Transform The joint's transform.\n@param Radius The distance from the joint position to the surface of the hand.\n@return true if the output param was populated with a valid value, false means that the tracking is lost and output is undefined." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary, nullptr, "GetHandJointTransform", nullptr, nullptr, sizeof(WindowsMixedRealityHandTrackingFunctionLibrary_eventGetHandJointTransform_Parms), Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics
	{
		struct WindowsMixedRealityHandTrackingFunctionLibrary_eventSupportsHandTracking_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WindowsMixedRealityHandTrackingFunctionLibrary_eventSupportsHandTracking_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WindowsMixedRealityHandTrackingFunctionLibrary_eventSupportsHandTracking_Parms), &Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::Function_MetaDataParams[] = {
		{ "Category", "HandTracking|WindowsMixedReality" },
		{ "Comment", "/**\n\x09Returns true if hand tracking available.\n\x09*/" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use GetXRSystemFlags and check for SupportsHandTracking" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityHandTrackingFunctionLibrary.h" },
		{ "ToolTip", "Returns true if hand tracking available." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary, nullptr, "SupportsHandTracking", nullptr, nullptr, sizeof(WindowsMixedRealityHandTrackingFunctionLibrary_eventSupportsHandTracking_Parms), Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_NoRegister()
	{
		return UWindowsMixedRealityHandTrackingFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_WindowsMixedRealityHandTracking,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_GetHandJointTransform, "GetHandJointTransform" }, // 3762138629
		{ &Z_Construct_UFunction_UWindowsMixedRealityHandTrackingFunctionLibrary_SupportsHandTracking, "SupportsHandTracking" }, // 120507538
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "WindowsMixedReality" },
		{ "IncludePath", "WindowsMixedRealityHandTrackingFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/WindowsMixedRealityHandTrackingFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWindowsMixedRealityHandTrackingFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_Statics::ClassParams = {
		&UWindowsMixedRealityHandTrackingFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWindowsMixedRealityHandTrackingFunctionLibrary, 187641226);
	template<> WINDOWSMIXEDREALITYHANDTRACKING_API UClass* StaticClass<UWindowsMixedRealityHandTrackingFunctionLibrary>()
	{
		return UWindowsMixedRealityHandTrackingFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary(Z_Construct_UClass_UWindowsMixedRealityHandTrackingFunctionLibrary, &UWindowsMixedRealityHandTrackingFunctionLibrary::StaticClass, TEXT("/Script/WindowsMixedRealityHandTracking"), TEXT("UWindowsMixedRealityHandTrackingFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWindowsMixedRealityHandTrackingFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
