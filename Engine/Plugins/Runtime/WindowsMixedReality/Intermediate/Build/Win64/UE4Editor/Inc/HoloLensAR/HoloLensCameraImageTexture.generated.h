// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOLOLENSAR_HoloLensCameraImageTexture_generated_h
#error "HoloLensCameraImageTexture.generated.h already included, missing '#pragma once' in HoloLensCameraImageTexture.h"
#endif
#define HOLOLENSAR_HoloLensCameraImageTexture_generated_h

#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_SPARSE_DATA
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_RPC_WRAPPERS
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHoloLensCameraImageTexture(); \
	friend struct Z_Construct_UClass_UHoloLensCameraImageTexture_Statics; \
public: \
	DECLARE_CLASS(UHoloLensCameraImageTexture, UARTextureCameraImage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HoloLensAR"), NO_API) \
	DECLARE_SERIALIZER(UHoloLensCameraImageTexture)


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUHoloLensCameraImageTexture(); \
	friend struct Z_Construct_UClass_UHoloLensCameraImageTexture_Statics; \
public: \
	DECLARE_CLASS(UHoloLensCameraImageTexture, UARTextureCameraImage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HoloLensAR"), NO_API) \
	DECLARE_SERIALIZER(UHoloLensCameraImageTexture)


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoloLensCameraImageTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoloLensCameraImageTexture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoloLensCameraImageTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoloLensCameraImageTexture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoloLensCameraImageTexture(UHoloLensCameraImageTexture&&); \
	NO_API UHoloLensCameraImageTexture(const UHoloLensCameraImageTexture&); \
public:


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHoloLensCameraImageTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHoloLensCameraImageTexture(UHoloLensCameraImageTexture&&); \
	NO_API UHoloLensCameraImageTexture(const UHoloLensCameraImageTexture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHoloLensCameraImageTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoloLensCameraImageTexture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoloLensCameraImageTexture)


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_27_PROLOG
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_SPARSE_DATA \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_RPC_WRAPPERS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_INCLASS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_SPARSE_DATA \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h_31_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class HoloLensCameraImageTexture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOLOLENSAR_API UClass* StaticClass<class UHoloLensCameraImageTexture>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_WindowsMixedReality_Source_HoloLensAR_Private_HoloLensCameraImageTexture_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
