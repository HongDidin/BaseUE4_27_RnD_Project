// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WINDOWSMIXEDREALITYHANDTRACKING_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_generated_h
#error "WindowsMixedRealityHandTrackingLiveLinkRemapAsset.generated.h already included, missing '#pragma once' in WindowsMixedRealityHandTrackingLiveLinkRemapAsset.h"
#endif
#define WINDOWSMIXEDREALITYHANDTRACKING_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_generated_h

#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_SPARSE_DATA
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_RPC_WRAPPERS
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWindowsMixedRealityHandTrackingLiveLinkRemapAsset(); \
	friend struct Z_Construct_UClass_UWindowsMixedRealityHandTrackingLiveLinkRemapAsset_Statics; \
public: \
	DECLARE_CLASS(UWindowsMixedRealityHandTrackingLiveLinkRemapAsset, ULiveLinkRetargetAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WindowsMixedRealityHandTracking"), NO_API) \
	DECLARE_SERIALIZER(UWindowsMixedRealityHandTrackingLiveLinkRemapAsset)


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_INCLASS \
private: \
	static void StaticRegisterNativesUWindowsMixedRealityHandTrackingLiveLinkRemapAsset(); \
	friend struct Z_Construct_UClass_UWindowsMixedRealityHandTrackingLiveLinkRemapAsset_Statics; \
public: \
	DECLARE_CLASS(UWindowsMixedRealityHandTrackingLiveLinkRemapAsset, ULiveLinkRetargetAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WindowsMixedRealityHandTracking"), NO_API) \
	DECLARE_SERIALIZER(UWindowsMixedRealityHandTrackingLiveLinkRemapAsset)


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWindowsMixedRealityHandTrackingLiveLinkRemapAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWindowsMixedRealityHandTrackingLiveLinkRemapAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWindowsMixedRealityHandTrackingLiveLinkRemapAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWindowsMixedRealityHandTrackingLiveLinkRemapAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWindowsMixedRealityHandTrackingLiveLinkRemapAsset(UWindowsMixedRealityHandTrackingLiveLinkRemapAsset&&); \
	NO_API UWindowsMixedRealityHandTrackingLiveLinkRemapAsset(const UWindowsMixedRealityHandTrackingLiveLinkRemapAsset&); \
public:


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWindowsMixedRealityHandTrackingLiveLinkRemapAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWindowsMixedRealityHandTrackingLiveLinkRemapAsset(UWindowsMixedRealityHandTrackingLiveLinkRemapAsset&&); \
	NO_API UWindowsMixedRealityHandTrackingLiveLinkRemapAsset(const UWindowsMixedRealityHandTrackingLiveLinkRemapAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWindowsMixedRealityHandTrackingLiveLinkRemapAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWindowsMixedRealityHandTrackingLiveLinkRemapAsset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWindowsMixedRealityHandTrackingLiveLinkRemapAsset)


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_30_PROLOG
#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_SPARSE_DATA \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_RPC_WRAPPERS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_INCLASS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_SPARSE_DATA \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h_34_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class WindowsMixedRealityHandTrackingLiveLinkRemapAsset."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WINDOWSMIXEDREALITYHANDTRACKING_API UClass* StaticClass<class UWindowsMixedRealityHandTrackingLiveLinkRemapAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_WindowsMixedReality_Source_WindowsMixedRealityHandTracking_Private_WindowsMixedRealityHandTrackingLiveLinkRemapAsset_h


#define FOREACH_ENUM_EQUATSWIZZLEAXIS(op) \
	op(EQuatSwizzleAxis::X) \
	op(EQuatSwizzleAxis::Y) \
	op(EQuatSwizzleAxis::Z) \
	op(EQuatSwizzleAxis::W) \
	op(EQuatSwizzleAxis::MinusX) \
	op(EQuatSwizzleAxis::MinusY) \
	op(EQuatSwizzleAxis::MinusZ) \
	op(EQuatSwizzleAxis::MinusW) 

enum class EQuatSwizzleAxis : uint8;
template<> WINDOWSMIXEDREALITYHANDTRACKING_API UEnum* StaticEnum<EQuatSwizzleAxis>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
