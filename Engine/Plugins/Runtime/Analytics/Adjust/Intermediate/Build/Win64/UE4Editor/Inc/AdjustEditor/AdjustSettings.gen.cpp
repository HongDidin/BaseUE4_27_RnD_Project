// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AdjustEditor/Classes/AdjustSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAdjustSettings() {}
// Cross Module References
	ADJUSTEDITOR_API UEnum* Z_Construct_UEnum_AdjustEditor_EAndroidAdjustLogging();
	UPackage* Z_Construct_UPackage__Script_AdjustEditor();
	ADJUSTEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FAdjustEventMapping();
	ADJUSTEDITOR_API UClass* Z_Construct_UClass_UAdjustSettings_NoRegister();
	ADJUSTEDITOR_API UClass* Z_Construct_UClass_UAdjustSettings();
	ANALYTICSVISUALEDITING_API UClass* Z_Construct_UClass_UAnalyticsSettingsBase();
// End Cross Module References
	static UEnum* EAndroidAdjustLogging_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AdjustEditor_EAndroidAdjustLogging, Z_Construct_UPackage__Script_AdjustEditor(), TEXT("EAndroidAdjustLogging"));
		}
		return Singleton;
	}
	template<> ADJUSTEDITOR_API UEnum* StaticEnum<EAndroidAdjustLogging::Type>()
	{
		return EAndroidAdjustLogging_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAndroidAdjustLogging(EAndroidAdjustLogging_StaticEnum, TEXT("/Script/AdjustEditor"), TEXT("EAndroidAdjustLogging"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AdjustEditor_EAndroidAdjustLogging_Hash() { return 1672413330U; }
	UEnum* Z_Construct_UEnum_AdjustEditor_EAndroidAdjustLogging()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AdjustEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAndroidAdjustLogging"), 0, Get_Z_Construct_UEnum_AdjustEditor_EAndroidAdjustLogging_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAndroidAdjustLogging::verbose", (int64)EAndroidAdjustLogging::verbose },
				{ "EAndroidAdjustLogging::debug", (int64)EAndroidAdjustLogging::debug },
				{ "EAndroidAdjustLogging::info", (int64)EAndroidAdjustLogging::info },
				{ "EAndroidAdjustLogging::warn", (int64)EAndroidAdjustLogging::warn },
				{ "EAndroidAdjustLogging::error", (int64)EAndroidAdjustLogging::error },
				{ "EAndroidAdjustLogging::assert", (int64)EAndroidAdjustLogging::assert },
				{ "EAndroidAdjustLogging::supress", (int64)EAndroidAdjustLogging::supress },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "assert.Comment", "/** disable errors as well */" },
				{ "assert.DisplayName", "ASSERT" },
				{ "assert.Name", "EAndroidAdjustLogging::assert" },
				{ "assert.ToolTip", "disable errors as well" },
				{ "debug.Comment", "/** enable more logging */" },
				{ "debug.DisplayName", "DEBUG" },
				{ "debug.Name", "EAndroidAdjustLogging::debug" },
				{ "debug.ToolTip", "enable more logging" },
				{ "error.Comment", "/** disable warnings as well */" },
				{ "error.DisplayName", "ERROR" },
				{ "error.Name", "EAndroidAdjustLogging::error" },
				{ "error.ToolTip", "disable warnings as well" },
				{ "info.Comment", "/** the default */" },
				{ "info.DisplayName", "INFO" },
				{ "info.Name", "EAndroidAdjustLogging::info" },
				{ "info.ToolTip", "the default" },
				{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
				{ "supress.Comment", "/** disable all log output */" },
				{ "supress.DisplayName", "SUPRESS" },
				{ "supress.Name", "EAndroidAdjustLogging::supress" },
				{ "supress.ToolTip", "disable all log output" },
				{ "verbose.Comment", "/** enable all logging */" },
				{ "verbose.DisplayName", "VERBOSE" },
				{ "verbose.Name", "EAndroidAdjustLogging::verbose" },
				{ "verbose.ToolTip", "enable all logging" },
				{ "warn.Comment", "/** disable info logging */" },
				{ "warn.DisplayName", "WARN" },
				{ "warn.Name", "EAndroidAdjustLogging::warn" },
				{ "warn.ToolTip", "disable info logging" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AdjustEditor,
				nullptr,
				"EAndroidAdjustLogging",
				"EAndroidAdjustLogging::Type",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Namespaced,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FAdjustEventMapping::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ADJUSTEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FAdjustEventMapping_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAdjustEventMapping, Z_Construct_UPackage__Script_AdjustEditor(), TEXT("AdjustEventMapping"), sizeof(FAdjustEventMapping), Get_Z_Construct_UScriptStruct_FAdjustEventMapping_Hash());
	}
	return Singleton;
}
template<> ADJUSTEDITOR_API UScriptStruct* StaticStruct<FAdjustEventMapping>()
{
	return FAdjustEventMapping::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAdjustEventMapping(FAdjustEventMapping::StaticStruct, TEXT("/Script/AdjustEditor"), TEXT("AdjustEventMapping"), false, nullptr, nullptr);
static struct FScriptStruct_AdjustEditor_StaticRegisterNativesFAdjustEventMapping
{
	FScriptStruct_AdjustEditor_StaticRegisterNativesFAdjustEventMapping()
	{
		UScriptStruct::DeferCppStructOps<FAdjustEventMapping>(FName(TEXT("AdjustEventMapping")));
	}
} ScriptStruct_AdjustEditor_StaticRegisterNativesFAdjustEventMapping;
	struct Z_Construct_UScriptStruct_FAdjustEventMapping_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Token_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Token;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds the event name and corresponding token from Adjust dashboard.\n */" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Holds the event name and corresponding token from Adjust dashboard." },
	};
#endif
	void* Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAdjustEventMapping>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "AdjustEvents" },
		{ "Comment", "/** The event name (the one passed in to RecordEvent calls). */" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "The event name (the one passed in to RecordEvent calls)." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAdjustEventMapping, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewProp_Token_MetaData[] = {
		{ "Category", "AdjustEvents" },
		{ "Comment", "/** The token of the corresponding event, generated by the Adjust dashboard. */" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "The token of the corresponding event, generated by the Adjust dashboard." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewProp_Token = { "Token", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAdjustEventMapping, Token), METADATA_PARAMS(Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewProp_Token_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewProp_Token_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::NewProp_Token,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AdjustEditor,
		nullptr,
		&NewStructOps,
		"AdjustEventMapping",
		sizeof(FAdjustEventMapping),
		alignof(FAdjustEventMapping),
		Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAdjustEventMapping()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAdjustEventMapping_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AdjustEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AdjustEventMapping"), sizeof(FAdjustEventMapping), Get_Z_Construct_UScriptStruct_FAdjustEventMapping_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAdjustEventMapping_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAdjustEventMapping_Hash() { return 3131963474U; }
	void UAdjustSettings::StaticRegisterNativesUAdjustSettings()
	{
	}
	UClass* Z_Construct_UClass_UAdjustSettings_NoRegister()
	{
		return UAdjustSettings::StaticClass();
	}
	struct Z_Construct_UClass_UAdjustSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSandboxNondistribution_MetaData[];
#endif
		static void NewProp_bSandboxNondistribution_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSandboxNondistribution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSandboxDistribution_MetaData[];
#endif
		static void NewProp_bSandboxDistribution_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSandboxDistribution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppToken_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AppToken;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LogLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultTracker_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultTracker;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProcessName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProcessName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEventBuffering_MetaData[];
#endif
		static void NewProp_bEventBuffering_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEventBuffering;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSendInBackground_MetaData[];
#endif
		static void NewProp_bSendInBackground_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSendInBackground;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DelayStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DelayStart;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EventMap_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EventMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAdjustSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnalyticsSettingsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AdjustEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AdjustSettings.h" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxNondistribution_MetaData[] = {
		{ "Category", "Adjust" },
		{ "Comment", "// Check to run in sandbox mode instead of production for non-distribution builds\n" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "DisplayName", "Sandbox mode for non-distribution?" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Check to run in sandbox mode instead of production for non-distribution builds" },
	};
#endif
	void Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxNondistribution_SetBit(void* Obj)
	{
		((UAdjustSettings*)Obj)->bSandboxNondistribution = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxNondistribution = { "bSandboxNondistribution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAdjustSettings), &Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxNondistribution_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxNondistribution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxNondistribution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxDistribution_MetaData[] = {
		{ "Category", "Adjust" },
		{ "Comment", "// Check to run in sandbox mode instead of production for distribution builds\n" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "DisplayName", "Sandbox mode for distribution?" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Check to run in sandbox mode instead of production for distribution builds" },
	};
#endif
	void Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxDistribution_SetBit(void* Obj)
	{
		((UAdjustSettings*)Obj)->bSandboxDistribution = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxDistribution = { "bSandboxDistribution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAdjustSettings), &Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxDistribution_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxDistribution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxDistribution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::NewProp_AppToken_MetaData[] = {
		{ "Category", "Adjust" },
		{ "Comment", "// Application token from dashboard\n" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "DisplayName", "Application token" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Application token from dashboard" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_AppToken = { "AppToken", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdjustSettings, AppToken), METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_AppToken_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_AppToken_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::NewProp_LogLevel_MetaData[] = {
		{ "Category", "Adjust" },
		{ "Comment", "// Level of verbosity to use for logging\n" },
		{ "DisplayName", "Logging level" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Level of verbosity to use for logging" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_LogLevel = { "LogLevel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdjustSettings, LogLevel), Z_Construct_UEnum_AdjustEditor_EAndroidAdjustLogging, METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_LogLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_LogLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::NewProp_DefaultTracker_MetaData[] = {
		{ "Category", "Adjust" },
		{ "Comment", "// Token from dashboard for tracker URL (may be left empty) \n" },
		{ "DisplayName", "Default tracker token" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Token from dashboard for tracker URL (may be left empty)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_DefaultTracker = { "DefaultTracker", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdjustSettings, DefaultTracker), METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_DefaultTracker_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_DefaultTracker_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::NewProp_ProcessName_MetaData[] = {
		{ "Category", "Adjust" },
		{ "Comment", "// Override the process name; will use the package name if not provided\n" },
		{ "DisplayName", "Process name (optional)" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Override the process name; will use the package name if not provided" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_ProcessName = { "ProcessName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdjustSettings, ProcessName), METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_ProcessName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_ProcessName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bEventBuffering_MetaData[] = {
		{ "Category", "Adjust" },
		{ "Comment", "// Optional event buffering (send in batches once a minute instead of immediately)\n" },
		{ "DisplayName", "Enable event buffering" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Optional event buffering (send in batches once a minute instead of immediately)" },
	};
#endif
	void Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bEventBuffering_SetBit(void* Obj)
	{
		((UAdjustSettings*)Obj)->bEventBuffering = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bEventBuffering = { "bEventBuffering", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAdjustSettings), &Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bEventBuffering_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bEventBuffering_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bEventBuffering_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSendInBackground_MetaData[] = {
		{ "Category", "Adjust" },
		{ "Comment", "// Send while in application in background\n" },
		{ "DisplayName", "Send while in background" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Send while in application in background" },
	};
#endif
	void Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSendInBackground_SetBit(void* Obj)
	{
		((UAdjustSettings*)Obj)->bSendInBackground = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSendInBackground = { "bSendInBackground", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAdjustSettings), &Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSendInBackground_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSendInBackground_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSendInBackground_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::NewProp_DelayStart_MetaData[] = {
		{ "Category", "Adjust" },
		{ "Comment", "// Optional start delay (up to 10 seconds) before first events are sent\n" },
		{ "DisplayName", "Delay start (seconds)" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Optional start delay (up to 10 seconds) before first events are sent" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_DelayStart = { "DelayStart", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdjustSettings, DelayStart), METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_DelayStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_DelayStart_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_EventMap_Inner = { "EventMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAdjustEventMapping, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdjustSettings_Statics::NewProp_EventMap_MetaData[] = {
		{ "Category", "AdjustEvents" },
		{ "Comment", "// Mapping of event names to tokens generated by Adjust dashboard.\n" },
		{ "ModuleRelativePath", "Classes/AdjustSettings.h" },
		{ "ToolTip", "Mapping of event names to tokens generated by Adjust dashboard." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAdjustSettings_Statics::NewProp_EventMap = { "EventMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdjustSettings, EventMap), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_EventMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::NewProp_EventMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAdjustSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxNondistribution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSandboxDistribution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_AppToken,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_LogLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_DefaultTracker,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_ProcessName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bEventBuffering,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_bSendInBackground,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_DelayStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_EventMap_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdjustSettings_Statics::NewProp_EventMap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAdjustSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAdjustSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAdjustSettings_Statics::ClassParams = {
		&UAdjustSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAdjustSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAdjustSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAdjustSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAdjustSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAdjustSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAdjustSettings, 2809126223);
	template<> ADJUSTEDITOR_API UClass* StaticClass<UAdjustSettings>()
	{
		return UAdjustSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAdjustSettings(Z_Construct_UClass_UAdjustSettings, &UAdjustSettings::StaticClass, TEXT("/Script/AdjustEditor"), TEXT("UAdjustSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAdjustSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
