// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FAnalyticsEventAttr;
#ifdef ANALYTICSBLUEPRINTLIBRARY_AnalyticsBlueprintLibrary_generated_h
#error "AnalyticsBlueprintLibrary.generated.h already included, missing '#pragma once' in AnalyticsBlueprintLibrary.h"
#endif
#define ANALYTICSBLUEPRINTLIBRARY_AnalyticsBlueprintLibrary_generated_h

#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnalyticsEventAttr_Statics; \
	ANALYTICSBLUEPRINTLIBRARY_API static class UScriptStruct* StaticStruct();


template<> ANALYTICSBLUEPRINTLIBRARY_API UScriptStruct* StaticStruct<struct FAnalyticsEventAttr>();

#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_SPARSE_DATA
#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRecordProgress); \
	DECLARE_FUNCTION(execRecordProgressWithAttributes); \
	DECLARE_FUNCTION(execRecordProgressWithFullHierarchyAndAttributes); \
	DECLARE_FUNCTION(execRecordError); \
	DECLARE_FUNCTION(execRecordErrorWithAttributes); \
	DECLARE_FUNCTION(execSetBuildInfo); \
	DECLARE_FUNCTION(execSetGender); \
	DECLARE_FUNCTION(execSetLocation); \
	DECLARE_FUNCTION(execSetAge); \
	DECLARE_FUNCTION(execSetUserId); \
	DECLARE_FUNCTION(execGetUserId); \
	DECLARE_FUNCTION(execSetSessionId); \
	DECLARE_FUNCTION(execGetSessionId); \
	DECLARE_FUNCTION(execMakeEventAttribute); \
	DECLARE_FUNCTION(execRecordCurrencyGivenWithAttributes); \
	DECLARE_FUNCTION(execRecordCurrencyGiven); \
	DECLARE_FUNCTION(execRecordCurrencyPurchase); \
	DECLARE_FUNCTION(execRecordSimpleCurrencyPurchaseWithAttributes); \
	DECLARE_FUNCTION(execRecordSimpleCurrencyPurchase); \
	DECLARE_FUNCTION(execRecordSimpleItemPurchaseWithAttributes); \
	DECLARE_FUNCTION(execRecordSimpleItemPurchase); \
	DECLARE_FUNCTION(execRecordItemPurchase); \
	DECLARE_FUNCTION(execRecordEventWithAttributes); \
	DECLARE_FUNCTION(execRecordEventWithAttribute); \
	DECLARE_FUNCTION(execRecordEvent); \
	DECLARE_FUNCTION(execFlushEvents); \
	DECLARE_FUNCTION(execEndSession); \
	DECLARE_FUNCTION(execStartSessionWithAttributes); \
	DECLARE_FUNCTION(execStartSession);


#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRecordProgress); \
	DECLARE_FUNCTION(execRecordProgressWithAttributes); \
	DECLARE_FUNCTION(execRecordProgressWithFullHierarchyAndAttributes); \
	DECLARE_FUNCTION(execRecordError); \
	DECLARE_FUNCTION(execRecordErrorWithAttributes); \
	DECLARE_FUNCTION(execSetBuildInfo); \
	DECLARE_FUNCTION(execSetGender); \
	DECLARE_FUNCTION(execSetLocation); \
	DECLARE_FUNCTION(execSetAge); \
	DECLARE_FUNCTION(execSetUserId); \
	DECLARE_FUNCTION(execGetUserId); \
	DECLARE_FUNCTION(execSetSessionId); \
	DECLARE_FUNCTION(execGetSessionId); \
	DECLARE_FUNCTION(execMakeEventAttribute); \
	DECLARE_FUNCTION(execRecordCurrencyGivenWithAttributes); \
	DECLARE_FUNCTION(execRecordCurrencyGiven); \
	DECLARE_FUNCTION(execRecordCurrencyPurchase); \
	DECLARE_FUNCTION(execRecordSimpleCurrencyPurchaseWithAttributes); \
	DECLARE_FUNCTION(execRecordSimpleCurrencyPurchase); \
	DECLARE_FUNCTION(execRecordSimpleItemPurchaseWithAttributes); \
	DECLARE_FUNCTION(execRecordSimpleItemPurchase); \
	DECLARE_FUNCTION(execRecordItemPurchase); \
	DECLARE_FUNCTION(execRecordEventWithAttributes); \
	DECLARE_FUNCTION(execRecordEventWithAttribute); \
	DECLARE_FUNCTION(execRecordEvent); \
	DECLARE_FUNCTION(execFlushEvents); \
	DECLARE_FUNCTION(execEndSession); \
	DECLARE_FUNCTION(execStartSessionWithAttributes); \
	DECLARE_FUNCTION(execStartSession);


#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnalyticsBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UAnalyticsBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UAnalyticsBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AnalyticsBlueprintLibrary"), NO_API) \
	DECLARE_SERIALIZER(UAnalyticsBlueprintLibrary)


#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUAnalyticsBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UAnalyticsBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UAnalyticsBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AnalyticsBlueprintLibrary"), NO_API) \
	DECLARE_SERIALIZER(UAnalyticsBlueprintLibrary)


#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnalyticsBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnalyticsBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnalyticsBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnalyticsBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnalyticsBlueprintLibrary(UAnalyticsBlueprintLibrary&&); \
	NO_API UAnalyticsBlueprintLibrary(const UAnalyticsBlueprintLibrary&); \
public:


#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnalyticsBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnalyticsBlueprintLibrary(UAnalyticsBlueprintLibrary&&); \
	NO_API UAnalyticsBlueprintLibrary(const UAnalyticsBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnalyticsBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnalyticsBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnalyticsBlueprintLibrary)


#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_25_PROLOG
#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_SPARSE_DATA \
	Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_INCLASS \
	Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_SPARSE_DATA \
	Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h_29_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AnalyticsBlueprintLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ANALYTICSBLUEPRINTLIBRARY_API UClass* StaticClass<class UAnalyticsBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Analytics_AnalyticsBlueprintLibrary_Source_AnalyticsBlueprintLibrary_Classes_AnalyticsBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
