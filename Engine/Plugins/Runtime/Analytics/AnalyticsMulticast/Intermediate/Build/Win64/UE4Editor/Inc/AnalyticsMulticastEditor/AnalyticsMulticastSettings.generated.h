// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ANALYTICSMULTICASTEDITOR_AnalyticsMulticastSettings_generated_h
#error "AnalyticsMulticastSettings.generated.h already included, missing '#pragma once' in AnalyticsMulticastSettings.h"
#endif
#define ANALYTICSMULTICASTEDITOR_AnalyticsMulticastSettings_generated_h

#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnalyticsMulticastSettings(); \
	friend struct Z_Construct_UClass_UAnalyticsMulticastSettings_Statics; \
public: \
	DECLARE_CLASS(UAnalyticsMulticastSettings, UAnalyticsSettingsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AnalyticsMulticastEditor"), NO_API) \
	DECLARE_SERIALIZER(UAnalyticsMulticastSettings)


#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUAnalyticsMulticastSettings(); \
	friend struct Z_Construct_UClass_UAnalyticsMulticastSettings_Statics; \
public: \
	DECLARE_CLASS(UAnalyticsMulticastSettings, UAnalyticsSettingsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AnalyticsMulticastEditor"), NO_API) \
	DECLARE_SERIALIZER(UAnalyticsMulticastSettings)


#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnalyticsMulticastSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnalyticsMulticastSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnalyticsMulticastSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnalyticsMulticastSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnalyticsMulticastSettings(UAnalyticsMulticastSettings&&); \
	NO_API UAnalyticsMulticastSettings(const UAnalyticsMulticastSettings&); \
public:


#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnalyticsMulticastSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnalyticsMulticastSettings(UAnalyticsMulticastSettings&&); \
	NO_API UAnalyticsMulticastSettings(const UAnalyticsMulticastSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnalyticsMulticastSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnalyticsMulticastSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnalyticsMulticastSettings)


#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_10_PROLOG
#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_INCLASS \
	Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AnalyticsMulticastSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ANALYTICSMULTICASTEDITOR_API UClass* StaticClass<class UAnalyticsMulticastSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Analytics_AnalyticsMulticast_Source_AnaltyicsMulticastEditor_Classes_AnalyticsMulticastSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
