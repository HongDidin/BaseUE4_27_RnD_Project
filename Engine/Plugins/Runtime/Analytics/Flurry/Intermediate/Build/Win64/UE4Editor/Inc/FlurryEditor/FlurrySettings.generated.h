// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FLURRYEDITOR_FlurrySettings_generated_h
#error "FlurrySettings.generated.h already included, missing '#pragma once' in FlurrySettings.h"
#endif
#define FLURRYEDITOR_FlurrySettings_generated_h

#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFlurrySettings(); \
	friend struct Z_Construct_UClass_UFlurrySettings_Statics; \
public: \
	DECLARE_CLASS(UFlurrySettings, UAnalyticsSettingsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FlurryEditor"), NO_API) \
	DECLARE_SERIALIZER(UFlurrySettings)


#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUFlurrySettings(); \
	friend struct Z_Construct_UClass_UFlurrySettings_Statics; \
public: \
	DECLARE_CLASS(UFlurrySettings, UAnalyticsSettingsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FlurryEditor"), NO_API) \
	DECLARE_SERIALIZER(UFlurrySettings)


#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFlurrySettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFlurrySettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFlurrySettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFlurrySettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFlurrySettings(UFlurrySettings&&); \
	NO_API UFlurrySettings(const UFlurrySettings&); \
public:


#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFlurrySettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFlurrySettings(UFlurrySettings&&); \
	NO_API UFlurrySettings(const UFlurrySettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFlurrySettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFlurrySettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFlurrySettings)


#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_10_PROLOG
#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_INCLASS \
	Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class FlurrySettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FLURRYEDITOR_API UClass* StaticClass<class UFlurrySettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Analytics_Flurry_Source_FlurryEditor_Classes_FlurrySettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
