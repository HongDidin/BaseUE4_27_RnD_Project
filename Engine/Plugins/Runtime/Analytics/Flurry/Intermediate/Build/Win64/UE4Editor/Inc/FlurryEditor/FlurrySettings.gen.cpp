// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FlurryEditor/Classes/FlurrySettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFlurrySettings() {}
// Cross Module References
	FLURRYEDITOR_API UClass* Z_Construct_UClass_UFlurrySettings_NoRegister();
	FLURRYEDITOR_API UClass* Z_Construct_UClass_UFlurrySettings();
	ANALYTICSVISUALEDITING_API UClass* Z_Construct_UClass_UAnalyticsSettingsBase();
	UPackage* Z_Construct_UPackage__Script_FlurryEditor();
// End Cross Module References
	void UFlurrySettings::StaticRegisterNativesUFlurrySettings()
	{
	}
	UClass* Z_Construct_UClass_UFlurrySettings_NoRegister()
	{
		return UFlurrySettings::StaticClass();
	}
	struct Z_Construct_UClass_UFlurrySettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReleaseApiKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReleaseApiKey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugApiKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DebugApiKey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TestApiKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TestApiKey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DevelopmentApiKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DevelopmentApiKey;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFlurrySettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnalyticsSettingsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_FlurryEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFlurrySettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FlurrySettings.h" },
		{ "ModuleRelativePath", "Classes/FlurrySettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFlurrySettings_Statics::NewProp_ReleaseApiKey_MetaData[] = {
		{ "Category", "Flurry" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Classes/FlurrySettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UFlurrySettings_Statics::NewProp_ReleaseApiKey = { "ReleaseApiKey", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFlurrySettings, ReleaseApiKey), METADATA_PARAMS(Z_Construct_UClass_UFlurrySettings_Statics::NewProp_ReleaseApiKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFlurrySettings_Statics::NewProp_ReleaseApiKey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFlurrySettings_Statics::NewProp_DebugApiKey_MetaData[] = {
		{ "Category", "Flurry" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Classes/FlurrySettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UFlurrySettings_Statics::NewProp_DebugApiKey = { "DebugApiKey", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFlurrySettings, DebugApiKey), METADATA_PARAMS(Z_Construct_UClass_UFlurrySettings_Statics::NewProp_DebugApiKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFlurrySettings_Statics::NewProp_DebugApiKey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFlurrySettings_Statics::NewProp_TestApiKey_MetaData[] = {
		{ "Category", "Flurry" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Classes/FlurrySettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UFlurrySettings_Statics::NewProp_TestApiKey = { "TestApiKey", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFlurrySettings, TestApiKey), METADATA_PARAMS(Z_Construct_UClass_UFlurrySettings_Statics::NewProp_TestApiKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFlurrySettings_Statics::NewProp_TestApiKey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFlurrySettings_Statics::NewProp_DevelopmentApiKey_MetaData[] = {
		{ "Category", "Flurry" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Classes/FlurrySettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UFlurrySettings_Statics::NewProp_DevelopmentApiKey = { "DevelopmentApiKey", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFlurrySettings, DevelopmentApiKey), METADATA_PARAMS(Z_Construct_UClass_UFlurrySettings_Statics::NewProp_DevelopmentApiKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFlurrySettings_Statics::NewProp_DevelopmentApiKey_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFlurrySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFlurrySettings_Statics::NewProp_ReleaseApiKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFlurrySettings_Statics::NewProp_DebugApiKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFlurrySettings_Statics::NewProp_TestApiKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFlurrySettings_Statics::NewProp_DevelopmentApiKey,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFlurrySettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFlurrySettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFlurrySettings_Statics::ClassParams = {
		&UFlurrySettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFlurrySettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFlurrySettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFlurrySettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFlurrySettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFlurrySettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFlurrySettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFlurrySettings, 3113260139);
	template<> FLURRYEDITOR_API UClass* StaticClass<UFlurrySettings>()
	{
		return UFlurrySettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFlurrySettings(Z_Construct_UClass_UFlurrySettings, &UFlurrySettings::StaticClass, TEXT("/Script/FlurryEditor"), TEXT("UFlurrySettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFlurrySettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
