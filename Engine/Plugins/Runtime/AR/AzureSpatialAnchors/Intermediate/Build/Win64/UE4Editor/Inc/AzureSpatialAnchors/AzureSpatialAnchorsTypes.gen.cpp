// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AzureSpatialAnchors/Public/AzureSpatialAnchorsTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAzureSpatialAnchorsTypes() {}
// Cross Module References
	AZURESPATIALANCHORS_API UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsSessionUserFeedback();
	UPackage* Z_Construct_UPackage__Script_AzureSpatialAnchors();
	AZURESPATIALANCHORS_API UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateAnchorStatus();
	AZURESPATIALANCHORS_API UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateStrategy();
	AZURESPATIALANCHORS_API UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorDataCategory();
	AZURESPATIALANCHORS_API UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsResult();
	AZURESPATIALANCHORS_API UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLogVerbosity();
	AZURESPATIALANCHORS_API UScriptStruct* Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig();
	AZURESPATIALANCHORS_API UScriptStruct* Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus();
	AZURESPATIALANCHORS_API UScriptStruct* Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria();
	AZURESPATIALANCHORS_API UClass* Z_Construct_UClass_UAzureCloudSpatialAnchor_NoRegister();
	AZURESPATIALANCHORS_API UScriptStruct* Z_Construct_UScriptStruct_FCoarseLocalizationSettings();
	AZURESPATIALANCHORS_API UScriptStruct* Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration();
// End Cross Module References
	static UEnum* EAzureSpatialAnchorsSessionUserFeedback_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsSessionUserFeedback, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("EAzureSpatialAnchorsSessionUserFeedback"));
		}
		return Singleton;
	}
	template<> AZURESPATIALANCHORS_API UEnum* StaticEnum<EAzureSpatialAnchorsSessionUserFeedback>()
	{
		return EAzureSpatialAnchorsSessionUserFeedback_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAzureSpatialAnchorsSessionUserFeedback(EAzureSpatialAnchorsSessionUserFeedback_StaticEnum, TEXT("/Script/AzureSpatialAnchors"), TEXT("EAzureSpatialAnchorsSessionUserFeedback"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsSessionUserFeedback_Hash() { return 2811943751U; }
	UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsSessionUserFeedback()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAzureSpatialAnchorsSessionUserFeedback"), 0, Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsSessionUserFeedback_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAzureSpatialAnchorsSessionUserFeedback::None", (int64)EAzureSpatialAnchorsSessionUserFeedback::None },
				{ "EAzureSpatialAnchorsSessionUserFeedback::NotEnoughMotion", (int64)EAzureSpatialAnchorsSessionUserFeedback::NotEnoughMotion },
				{ "EAzureSpatialAnchorsSessionUserFeedback::MotionTooQuick", (int64)EAzureSpatialAnchorsSessionUserFeedback::MotionTooQuick },
				{ "EAzureSpatialAnchorsSessionUserFeedback::NotEnoughFeatures", (int64)EAzureSpatialAnchorsSessionUserFeedback::NotEnoughFeatures },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "AzureSpatialAnchors" },
				{ "Comment", "// Note: this must match winrt::Microsoft::Azure::SpatialAnchors::SessionUserFeedback\n" },
				{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
				{ "MotionTooQuick.Comment", "// Device is not moving enough to create a neighborhood of key-frames.\n" },
				{ "MotionTooQuick.Name", "EAzureSpatialAnchorsSessionUserFeedback::MotionTooQuick" },
				{ "MotionTooQuick.ToolTip", "Device is not moving enough to create a neighborhood of key-frames." },
				{ "None.Name", "EAzureSpatialAnchorsSessionUserFeedback::None" },
				{ "NotEnoughFeatures.Comment", "// Device is moving too quickly for stable tracking.\n// Note: skipped 3  - presumably these values are used as bit flags somewhere?\n" },
				{ "NotEnoughFeatures.Name", "EAzureSpatialAnchorsSessionUserFeedback::NotEnoughFeatures" },
				{ "NotEnoughFeatures.ToolTip", "Device is moving too quickly for stable tracking.\nNote: skipped 3  - presumably these values are used as bit flags somewhere?" },
				{ "NotEnoughMotion.Comment", "// No specific feedback is available.\n" },
				{ "NotEnoughMotion.Name", "EAzureSpatialAnchorsSessionUserFeedback::NotEnoughMotion" },
				{ "NotEnoughMotion.ToolTip", "No specific feedback is available." },
				{ "ToolTip", "Note: this must match winrt::Microsoft::Azure::SpatialAnchors::SessionUserFeedback" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
				nullptr,
				"EAzureSpatialAnchorsSessionUserFeedback",
				"EAzureSpatialAnchorsSessionUserFeedback",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAzureSpatialAnchorsLocateAnchorStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateAnchorStatus, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("EAzureSpatialAnchorsLocateAnchorStatus"));
		}
		return Singleton;
	}
	template<> AZURESPATIALANCHORS_API UEnum* StaticEnum<EAzureSpatialAnchorsLocateAnchorStatus>()
	{
		return EAzureSpatialAnchorsLocateAnchorStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAzureSpatialAnchorsLocateAnchorStatus(EAzureSpatialAnchorsLocateAnchorStatus_StaticEnum, TEXT("/Script/AzureSpatialAnchors"), TEXT("EAzureSpatialAnchorsLocateAnchorStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateAnchorStatus_Hash() { return 732178203U; }
	UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateAnchorStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAzureSpatialAnchorsLocateAnchorStatus"), 0, Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateAnchorStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAzureSpatialAnchorsLocateAnchorStatus::AlreadyTracked", (int64)EAzureSpatialAnchorsLocateAnchorStatus::AlreadyTracked },
				{ "EAzureSpatialAnchorsLocateAnchorStatus::Located", (int64)EAzureSpatialAnchorsLocateAnchorStatus::Located },
				{ "EAzureSpatialAnchorsLocateAnchorStatus::NotLocated", (int64)EAzureSpatialAnchorsLocateAnchorStatus::NotLocated },
				{ "EAzureSpatialAnchorsLocateAnchorStatus::NotLocatedAnchorDoesNotExist", (int64)EAzureSpatialAnchorsLocateAnchorStatus::NotLocatedAnchorDoesNotExist },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AlreadyTracked.Name", "EAzureSpatialAnchorsLocateAnchorStatus::AlreadyTracked" },
				{ "BlueprintType", "true" },
				{ "Category", "AzureSpatialAnchors" },
				{ "Comment", "// Note: this must match winrt::Microsoft::Azure::SpatialAnchors::LocateAnchorStatus\n" },
				{ "Located.Comment", "// The anchor was already being tracked.\n" },
				{ "Located.Name", "EAzureSpatialAnchorsLocateAnchorStatus::Located" },
				{ "Located.ToolTip", "The anchor was already being tracked." },
				{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
				{ "NotLocated.Comment", "// The anchor was found.\n" },
				{ "NotLocated.Name", "EAzureSpatialAnchorsLocateAnchorStatus::NotLocated" },
				{ "NotLocated.ToolTip", "The anchor was found." },
				{ "NotLocatedAnchorDoesNotExist.Comment", "// The anchor was not found.\n" },
				{ "NotLocatedAnchorDoesNotExist.Name", "EAzureSpatialAnchorsLocateAnchorStatus::NotLocatedAnchorDoesNotExist" },
				{ "NotLocatedAnchorDoesNotExist.ToolTip", "The anchor was not found." },
				{ "ToolTip", "Note: this must match winrt::Microsoft::Azure::SpatialAnchors::LocateAnchorStatus" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
				nullptr,
				"EAzureSpatialAnchorsLocateAnchorStatus",
				"EAzureSpatialAnchorsLocateAnchorStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAzureSpatialAnchorsLocateStrategy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateStrategy, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("EAzureSpatialAnchorsLocateStrategy"));
		}
		return Singleton;
	}
	template<> AZURESPATIALANCHORS_API UEnum* StaticEnum<EAzureSpatialAnchorsLocateStrategy>()
	{
		return EAzureSpatialAnchorsLocateStrategy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAzureSpatialAnchorsLocateStrategy(EAzureSpatialAnchorsLocateStrategy_StaticEnum, TEXT("/Script/AzureSpatialAnchors"), TEXT("EAzureSpatialAnchorsLocateStrategy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateStrategy_Hash() { return 129633733U; }
	UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateStrategy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAzureSpatialAnchorsLocateStrategy"), 0, Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateStrategy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAzureSpatialAnchorsLocateStrategy::AnyStrategy", (int64)EAzureSpatialAnchorsLocateStrategy::AnyStrategy },
				{ "EAzureSpatialAnchorsLocateStrategy::VisualInformation", (int64)EAzureSpatialAnchorsLocateStrategy::VisualInformation },
				{ "EAzureSpatialAnchorsLocateStrategy::Relationship", (int64)EAzureSpatialAnchorsLocateStrategy::Relationship },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AnyStrategy.Name", "EAzureSpatialAnchorsLocateStrategy::AnyStrategy" },
				{ "BlueprintType", "true" },
				{ "Category", "AzureSpatialAnchors" },
				{ "Comment", "// Note: this must match winrt::Microsoft::Azure::SpatialAnchors::LocateStrategy\n" },
				{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
				{ "Relationship.Comment", "// Indicates that anchors will be located primarily by visual information.\n" },
				{ "Relationship.Name", "EAzureSpatialAnchorsLocateStrategy::Relationship" },
				{ "Relationship.ToolTip", "Indicates that anchors will be located primarily by visual information." },
				{ "ToolTip", "Note: this must match winrt::Microsoft::Azure::SpatialAnchors::LocateStrategy" },
				{ "VisualInformation.Comment", "// Indicates that any method is acceptable.\n" },
				{ "VisualInformation.Name", "EAzureSpatialAnchorsLocateStrategy::VisualInformation" },
				{ "VisualInformation.ToolTip", "Indicates that any method is acceptable." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
				nullptr,
				"EAzureSpatialAnchorsLocateStrategy",
				"EAzureSpatialAnchorsLocateStrategy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAzureSpatialAnchorDataCategory_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorDataCategory, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("EAzureSpatialAnchorDataCategory"));
		}
		return Singleton;
	}
	template<> AZURESPATIALANCHORS_API UEnum* StaticEnum<EAzureSpatialAnchorDataCategory>()
	{
		return EAzureSpatialAnchorDataCategory_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAzureSpatialAnchorDataCategory(EAzureSpatialAnchorDataCategory_StaticEnum, TEXT("/Script/AzureSpatialAnchors"), TEXT("EAzureSpatialAnchorDataCategory"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorDataCategory_Hash() { return 232585707U; }
	UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorDataCategory()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAzureSpatialAnchorDataCategory"), 0, Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorDataCategory_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAzureSpatialAnchorDataCategory::None", (int64)EAzureSpatialAnchorDataCategory::None },
				{ "EAzureSpatialAnchorDataCategory::Properties", (int64)EAzureSpatialAnchorDataCategory::Properties },
				{ "EAzureSpatialAnchorDataCategory::Spatial", (int64)EAzureSpatialAnchorDataCategory::Spatial },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "AzureSpatialAnchors" },
				{ "Comment", "// Note: this must match winrt::Microsoft::Azure::SpatialAnchors::AnchorDataCategory\n" },
				{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
				{ "None.Name", "EAzureSpatialAnchorDataCategory::None" },
				{ "Properties.Comment", "// No data is returned.\n" },
				{ "Properties.Name", "EAzureSpatialAnchorDataCategory::Properties" },
				{ "Properties.ToolTip", "No data is returned." },
				{ "Spatial.Comment", "// Get only Anchor metadata properties including AppProperties.\n" },
				{ "Spatial.Name", "EAzureSpatialAnchorDataCategory::Spatial" },
				{ "Spatial.ToolTip", "Get only Anchor metadata properties including AppProperties." },
				{ "ToolTip", "Note: this must match winrt::Microsoft::Azure::SpatialAnchors::AnchorDataCategory" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
				nullptr,
				"EAzureSpatialAnchorDataCategory",
				"EAzureSpatialAnchorDataCategory",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAzureSpatialAnchorsResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsResult, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("EAzureSpatialAnchorsResult"));
		}
		return Singleton;
	}
	template<> AZURESPATIALANCHORS_API UEnum* StaticEnum<EAzureSpatialAnchorsResult>()
	{
		return EAzureSpatialAnchorsResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAzureSpatialAnchorsResult(EAzureSpatialAnchorsResult_StaticEnum, TEXT("/Script/AzureSpatialAnchors"), TEXT("EAzureSpatialAnchorsResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsResult_Hash() { return 3795935245U; }
	UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAzureSpatialAnchorsResult"), 0, Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAzureSpatialAnchorsResult::Success", (int64)EAzureSpatialAnchorsResult::Success },
				{ "EAzureSpatialAnchorsResult::NotStarted", (int64)EAzureSpatialAnchorsResult::NotStarted },
				{ "EAzureSpatialAnchorsResult::Started", (int64)EAzureSpatialAnchorsResult::Started },
				{ "EAzureSpatialAnchorsResult::FailAlreadyStarted", (int64)EAzureSpatialAnchorsResult::FailAlreadyStarted },
				{ "EAzureSpatialAnchorsResult::FailNoARPin", (int64)EAzureSpatialAnchorsResult::FailNoARPin },
				{ "EAzureSpatialAnchorsResult::FailBadLocalAnchorID", (int64)EAzureSpatialAnchorsResult::FailBadLocalAnchorID },
				{ "EAzureSpatialAnchorsResult::FailBadCloudAnchorIdentifier", (int64)EAzureSpatialAnchorsResult::FailBadCloudAnchorIdentifier },
				{ "EAzureSpatialAnchorsResult::FailAnchorIdAlreadyUsed", (int64)EAzureSpatialAnchorsResult::FailAnchorIdAlreadyUsed },
				{ "EAzureSpatialAnchorsResult::FailAnchorDoesNotExist", (int64)EAzureSpatialAnchorsResult::FailAnchorDoesNotExist },
				{ "EAzureSpatialAnchorsResult::FailAnchorAlreadyTracked", (int64)EAzureSpatialAnchorsResult::FailAnchorAlreadyTracked },
				{ "EAzureSpatialAnchorsResult::FailNoAnchor", (int64)EAzureSpatialAnchorsResult::FailNoAnchor },
				{ "EAzureSpatialAnchorsResult::FailNoCloudAnchor", (int64)EAzureSpatialAnchorsResult::FailNoCloudAnchor },
				{ "EAzureSpatialAnchorsResult::FailNoLocalAnchor", (int64)EAzureSpatialAnchorsResult::FailNoLocalAnchor },
				{ "EAzureSpatialAnchorsResult::FailNoSession", (int64)EAzureSpatialAnchorsResult::FailNoSession },
				{ "EAzureSpatialAnchorsResult::FailNoWatcher", (int64)EAzureSpatialAnchorsResult::FailNoWatcher },
				{ "EAzureSpatialAnchorsResult::FailNotEnoughData", (int64)EAzureSpatialAnchorsResult::FailNotEnoughData },
				{ "EAzureSpatialAnchorsResult::FailBadLifetime", (int64)EAzureSpatialAnchorsResult::FailBadLifetime },
				{ "EAzureSpatialAnchorsResult::FailSeeErrorString", (int64)EAzureSpatialAnchorsResult::FailSeeErrorString },
				{ "EAzureSpatialAnchorsResult::NotLocated", (int64)EAzureSpatialAnchorsResult::NotLocated },
				{ "EAzureSpatialAnchorsResult::Canceled", (int64)EAzureSpatialAnchorsResult::Canceled },
				{ "EAzureSpatialAnchorsResult::FailUnknown", (int64)EAzureSpatialAnchorsResult::FailUnknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Canceled.Name", "EAzureSpatialAnchorsResult::Canceled" },
				{ "Category", "AR|AzureSpatialAnchors" },
				{ "Comment", "// Note: this Result enum must match AzureSpatialAnchorsInterop::AsyncResult in MixedRealityInterop.h\n" },
				{ "FailAlreadyStarted.Name", "EAzureSpatialAnchorsResult::FailAlreadyStarted" },
				{ "FailAnchorAlreadyTracked.Name", "EAzureSpatialAnchorsResult::FailAnchorAlreadyTracked" },
				{ "FailAnchorDoesNotExist.Name", "EAzureSpatialAnchorsResult::FailAnchorDoesNotExist" },
				{ "FailAnchorIdAlreadyUsed.Name", "EAzureSpatialAnchorsResult::FailAnchorIdAlreadyUsed" },
				{ "FailBadCloudAnchorIdentifier.Name", "EAzureSpatialAnchorsResult::FailBadCloudAnchorIdentifier" },
				{ "FailBadLifetime.Name", "EAzureSpatialAnchorsResult::FailBadLifetime" },
				{ "FailBadLocalAnchorID.Name", "EAzureSpatialAnchorsResult::FailBadLocalAnchorID" },
				{ "FailNoAnchor.Name", "EAzureSpatialAnchorsResult::FailNoAnchor" },
				{ "FailNoARPin.Name", "EAzureSpatialAnchorsResult::FailNoARPin" },
				{ "FailNoCloudAnchor.Name", "EAzureSpatialAnchorsResult::FailNoCloudAnchor" },
				{ "FailNoLocalAnchor.Name", "EAzureSpatialAnchorsResult::FailNoLocalAnchor" },
				{ "FailNoSession.Name", "EAzureSpatialAnchorsResult::FailNoSession" },
				{ "FailNotEnoughData.Name", "EAzureSpatialAnchorsResult::FailNotEnoughData" },
				{ "FailNoWatcher.Name", "EAzureSpatialAnchorsResult::FailNoWatcher" },
				{ "FailSeeErrorString.Name", "EAzureSpatialAnchorsResult::FailSeeErrorString" },
				{ "FailUnknown.Name", "EAzureSpatialAnchorsResult::FailUnknown" },
				{ "Keywords", "azure spatial anchor hololens wmr pin ar all" },
				{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
				{ "NotLocated.Name", "EAzureSpatialAnchorsResult::NotLocated" },
				{ "NotStarted.Name", "EAzureSpatialAnchorsResult::NotStarted" },
				{ "Started.Name", "EAzureSpatialAnchorsResult::Started" },
				{ "Success.Name", "EAzureSpatialAnchorsResult::Success" },
				{ "ToolTip", "Note: this Result enum must match AzureSpatialAnchorsInterop::AsyncResult in MixedRealityInterop.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
				nullptr,
				"EAzureSpatialAnchorsResult",
				"EAzureSpatialAnchorsResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAzureSpatialAnchorsLogVerbosity_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLogVerbosity, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("EAzureSpatialAnchorsLogVerbosity"));
		}
		return Singleton;
	}
	template<> AZURESPATIALANCHORS_API UEnum* StaticEnum<EAzureSpatialAnchorsLogVerbosity>()
	{
		return EAzureSpatialAnchorsLogVerbosity_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAzureSpatialAnchorsLogVerbosity(EAzureSpatialAnchorsLogVerbosity_StaticEnum, TEXT("/Script/AzureSpatialAnchors"), TEXT("EAzureSpatialAnchorsLogVerbosity"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLogVerbosity_Hash() { return 3081842028U; }
	UEnum* Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLogVerbosity()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAzureSpatialAnchorsLogVerbosity"), 0, Get_Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLogVerbosity_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAzureSpatialAnchorsLogVerbosity::None", (int64)EAzureSpatialAnchorsLogVerbosity::None },
				{ "EAzureSpatialAnchorsLogVerbosity::Error", (int64)EAzureSpatialAnchorsLogVerbosity::Error },
				{ "EAzureSpatialAnchorsLogVerbosity::Warning", (int64)EAzureSpatialAnchorsLogVerbosity::Warning },
				{ "EAzureSpatialAnchorsLogVerbosity::Information", (int64)EAzureSpatialAnchorsLogVerbosity::Information },
				{ "EAzureSpatialAnchorsLogVerbosity::Debug", (int64)EAzureSpatialAnchorsLogVerbosity::Debug },
				{ "EAzureSpatialAnchorsLogVerbosity::All", (int64)EAzureSpatialAnchorsLogVerbosity::All },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "All.Name", "EAzureSpatialAnchorsLogVerbosity::All" },
				{ "BlueprintType", "true" },
				{ "Category", "AR|AzureSpatialAnchors" },
				{ "Comment", "// Note: this must match winrt::Microsoft::Azure::SpatialAnchors::SessionLogLevel\n" },
				{ "Debug.Name", "EAzureSpatialAnchorsLogVerbosity::Debug" },
				{ "Error.Name", "EAzureSpatialAnchorsLogVerbosity::Error" },
				{ "Information.Name", "EAzureSpatialAnchorsLogVerbosity::Information" },
				{ "Keywords", "azure spatial anchor hololens wmr pin ar all" },
				{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
				{ "None.Name", "EAzureSpatialAnchorsLogVerbosity::None" },
				{ "ToolTip", "Note: this must match winrt::Microsoft::Azure::SpatialAnchors::SessionLogLevel" },
				{ "Warning.Name", "EAzureSpatialAnchorsLogVerbosity::Warning" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
				nullptr,
				"EAzureSpatialAnchorsLogVerbosity",
				"EAzureSpatialAnchorsLogVerbosity",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FAzureSpatialAnchorsDiagnosticsConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AZURESPATIALANCHORS_API uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("AzureSpatialAnchorsDiagnosticsConfig"), sizeof(FAzureSpatialAnchorsDiagnosticsConfig), Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Hash());
	}
	return Singleton;
}
template<> AZURESPATIALANCHORS_API UScriptStruct* StaticStruct<FAzureSpatialAnchorsDiagnosticsConfig>()
{
	return FAzureSpatialAnchorsDiagnosticsConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig(FAzureSpatialAnchorsDiagnosticsConfig::StaticStruct, TEXT("/Script/AzureSpatialAnchors"), TEXT("AzureSpatialAnchorsDiagnosticsConfig"), false, nullptr, nullptr);
static struct FScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsDiagnosticsConfig
{
	FScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsDiagnosticsConfig()
	{
		UScriptStruct::DeferCppStructOps<FAzureSpatialAnchorsDiagnosticsConfig>(FName(TEXT("AzureSpatialAnchorsDiagnosticsConfig")));
	}
} ScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsDiagnosticsConfig;
	struct Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImagesEnabled_MetaData[];
#endif
		static void NewProp_bImagesEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImagesEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogDirectory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LogDirectory;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LogLevel_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LogLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxDiskSizeInMB_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MaxDiskSizeInMB;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAzureSpatialAnchorsDiagnosticsConfig>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_bImagesEnabled_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_bImagesEnabled_SetBit(void* Obj)
	{
		((FAzureSpatialAnchorsDiagnosticsConfig*)Obj)->bImagesEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_bImagesEnabled = { "bImagesEnabled", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FAzureSpatialAnchorsDiagnosticsConfig), &Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_bImagesEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_bImagesEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_bImagesEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogDirectory_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogDirectory = { "LogDirectory", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsDiagnosticsConfig, LogDirectory), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogDirectory_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogDirectory_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogLevel_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogLevel_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogLevel = { "LogLevel", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsDiagnosticsConfig, LogLevel), Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLogVerbosity, METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_MaxDiskSizeInMB_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_MaxDiskSizeInMB = { "MaxDiskSizeInMB", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsDiagnosticsConfig, MaxDiskSizeInMB), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_MaxDiskSizeInMB_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_MaxDiskSizeInMB_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_bImagesEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogDirectory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogLevel_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_LogLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::NewProp_MaxDiskSizeInMB,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
		nullptr,
		&NewStructOps,
		"AzureSpatialAnchorsDiagnosticsConfig",
		sizeof(FAzureSpatialAnchorsDiagnosticsConfig),
		alignof(FAzureSpatialAnchorsDiagnosticsConfig),
		Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AzureSpatialAnchorsDiagnosticsConfig"), sizeof(FAzureSpatialAnchorsDiagnosticsConfig), Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsDiagnosticsConfig_Hash() { return 1383493148U; }
class UScriptStruct* FAzureSpatialAnchorsSessionStatus::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AZURESPATIALANCHORS_API uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("AzureSpatialAnchorsSessionStatus"), sizeof(FAzureSpatialAnchorsSessionStatus), Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Hash());
	}
	return Singleton;
}
template<> AZURESPATIALANCHORS_API UScriptStruct* StaticStruct<FAzureSpatialAnchorsSessionStatus>()
{
	return FAzureSpatialAnchorsSessionStatus::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAzureSpatialAnchorsSessionStatus(FAzureSpatialAnchorsSessionStatus::StaticStruct, TEXT("/Script/AzureSpatialAnchors"), TEXT("AzureSpatialAnchorsSessionStatus"), false, nullptr, nullptr);
static struct FScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsSessionStatus
{
	FScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsSessionStatus()
	{
		UScriptStruct::DeferCppStructOps<FAzureSpatialAnchorsSessionStatus>(FName(TEXT("AzureSpatialAnchorsSessionStatus")));
	}
} ScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsSessionStatus;
	struct Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReadyForCreateProgress_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReadyForCreateProgress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecommendedForCreateProgress_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RecommendedForCreateProgress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionCreateHash_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SessionCreateHash;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionLocateHash_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SessionLocateHash;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_feedback_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_feedback_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_feedback;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAzureSpatialAnchorsSessionStatus>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_ReadyForCreateProgress_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_ReadyForCreateProgress = { "ReadyForCreateProgress", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsSessionStatus, ReadyForCreateProgress), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_ReadyForCreateProgress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_ReadyForCreateProgress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_RecommendedForCreateProgress_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_RecommendedForCreateProgress = { "RecommendedForCreateProgress", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsSessionStatus, RecommendedForCreateProgress), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_RecommendedForCreateProgress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_RecommendedForCreateProgress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_SessionCreateHash_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_SessionCreateHash = { "SessionCreateHash", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsSessionStatus, SessionCreateHash), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_SessionCreateHash_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_SessionCreateHash_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_SessionLocateHash_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_SessionLocateHash = { "SessionLocateHash", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsSessionStatus, SessionLocateHash), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_SessionLocateHash_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_SessionLocateHash_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_feedback_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_feedback_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_feedback = { "feedback", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsSessionStatus, feedback), Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsSessionUserFeedback, METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_feedback_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_feedback_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_ReadyForCreateProgress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_RecommendedForCreateProgress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_SessionCreateHash,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_SessionLocateHash,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_feedback_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::NewProp_feedback,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
		nullptr,
		&NewStructOps,
		"AzureSpatialAnchorsSessionStatus",
		sizeof(FAzureSpatialAnchorsSessionStatus),
		alignof(FAzureSpatialAnchorsSessionStatus),
		Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AzureSpatialAnchorsSessionStatus"), sizeof(FAzureSpatialAnchorsSessionStatus), Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionStatus_Hash() { return 2380049956U; }
class UScriptStruct* FAzureSpatialAnchorsLocateCriteria::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AZURESPATIALANCHORS_API uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("AzureSpatialAnchorsLocateCriteria"), sizeof(FAzureSpatialAnchorsLocateCriteria), Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Hash());
	}
	return Singleton;
}
template<> AZURESPATIALANCHORS_API UScriptStruct* StaticStruct<FAzureSpatialAnchorsLocateCriteria>()
{
	return FAzureSpatialAnchorsLocateCriteria::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria(FAzureSpatialAnchorsLocateCriteria::StaticStruct, TEXT("/Script/AzureSpatialAnchors"), TEXT("AzureSpatialAnchorsLocateCriteria"), false, nullptr, nullptr);
static struct FScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsLocateCriteria
{
	FScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsLocateCriteria()
	{
		UScriptStruct::DeferCppStructOps<FAzureSpatialAnchorsLocateCriteria>(FName(TEXT("AzureSpatialAnchorsLocateCriteria")));
	}
} ScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsLocateCriteria;
	struct Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBypassCache_MetaData[];
#endif
		static void NewProp_bBypassCache_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBypassCache;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Identifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Identifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Identifiers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NearAnchor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NearAnchor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NearAnchorDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NearAnchorDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NearAnchorMaxResultCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NearAnchorMaxResultCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSearchNearDevice_MetaData[];
#endif
		static void NewProp_bSearchNearDevice_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSearchNearDevice;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NearDeviceDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NearDeviceDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NearDeviceMaxResultCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NearDeviceMaxResultCount;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RequestedCategories_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequestedCategories_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RequestedCategories;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Strategy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strategy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Strategy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAzureSpatialAnchorsLocateCriteria>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bBypassCache_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "Comment", "/**\n\x09 * If true the device local cache of anchors is ignored.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "If true the device local cache of anchors is ignored." },
	};
#endif
	void Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bBypassCache_SetBit(void* Obj)
	{
		((FAzureSpatialAnchorsLocateCriteria*)Obj)->bBypassCache = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bBypassCache = { "bBypassCache", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FAzureSpatialAnchorsLocateCriteria), &Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bBypassCache_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bBypassCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bBypassCache_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Identifiers_Inner = { "Identifiers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Identifiers_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "Comment", "/**\n\x09 * List of specific anchor identifiers to locate.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "List of specific anchor identifiers to locate." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Identifiers = { "Identifiers", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsLocateCriteria, Identifiers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Identifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Identifiers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchor_MetaData[] = {
		{ "Category", "AzureSpatialAnchors | NearAnchor" },
		{ "Comment", "/**\n\x09 * Specify (optionally) an anchor around which to locate anchors.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "Specify (optionally) an anchor around which to locate anchors." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchor = { "NearAnchor", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsLocateCriteria, NearAnchor), Z_Construct_UClass_UAzureCloudSpatialAnchor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchorDistance_MetaData[] = {
		{ "Category", "AzureSpatialAnchors | NearAnchor" },
		{ "Comment", "/**\n\x09 * Specify the distance at which to locate anchors near the NearAnchor, in cm.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "Specify the distance at which to locate anchors near the NearAnchor, in cm." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchorDistance = { "NearAnchorDistance", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsLocateCriteria, NearAnchorDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchorDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchorDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchorMaxResultCount_MetaData[] = {
		{ "Category", "AzureSpatialAnchors | NearAnchor" },
		{ "Comment", "/**\n\x09 * Specify the maximum number of anchors around the NearAnchor to locate.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "Specify the maximum number of anchors around the NearAnchor to locate." },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchorMaxResultCount = { "NearAnchorMaxResultCount", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsLocateCriteria, NearAnchorMaxResultCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchorMaxResultCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchorMaxResultCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bSearchNearDevice_MetaData[] = {
		{ "Category", "AzureSpatialAnchors | NearDevice" },
		{ "Comment", "/**\n\x09 * Specify whether to search near the device location.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "Specify whether to search near the device location." },
	};
#endif
	void Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bSearchNearDevice_SetBit(void* Obj)
	{
		((FAzureSpatialAnchorsLocateCriteria*)Obj)->bSearchNearDevice = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bSearchNearDevice = { "bSearchNearDevice", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FAzureSpatialAnchorsLocateCriteria), &Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bSearchNearDevice_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bSearchNearDevice_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bSearchNearDevice_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearDeviceDistance_MetaData[] = {
		{ "Category", "AzureSpatialAnchors | NearDevice" },
		{ "Comment", "/**\n\x09 * Specify the distance at which to locate anchors near the device, in cm.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "Specify the distance at which to locate anchors near the device, in cm." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearDeviceDistance = { "NearDeviceDistance", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsLocateCriteria, NearDeviceDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearDeviceDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearDeviceDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearDeviceMaxResultCount_MetaData[] = {
		{ "Category", "AzureSpatialAnchors | NearDevice" },
		{ "Comment", "/**\n\x09 * Specify the maximum number of anchors around the device to locate.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "Specify the maximum number of anchors around the device to locate." },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearDeviceMaxResultCount = { "NearDeviceMaxResultCount", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsLocateCriteria, NearDeviceMaxResultCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearDeviceMaxResultCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearDeviceMaxResultCount_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_RequestedCategories_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_RequestedCategories_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "Comment", "/**\n\x09 * Specify what data to retrieve.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "Specify what data to retrieve." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_RequestedCategories = { "RequestedCategories", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsLocateCriteria, RequestedCategories), Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorDataCategory, METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_RequestedCategories_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_RequestedCategories_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Strategy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Strategy_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "Comment", "/**\n\x09 * Specify the method by which anchors will be located.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "Specify the method by which anchors will be located." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Strategy = { "Strategy", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsLocateCriteria, Strategy), Z_Construct_UEnum_AzureSpatialAnchors_EAzureSpatialAnchorsLocateStrategy, METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Strategy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Strategy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bBypassCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Identifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Identifiers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchorDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearAnchorMaxResultCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_bSearchNearDevice,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearDeviceDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_NearDeviceMaxResultCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_RequestedCategories_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_RequestedCategories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Strategy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::NewProp_Strategy,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
		nullptr,
		&NewStructOps,
		"AzureSpatialAnchorsLocateCriteria",
		sizeof(FAzureSpatialAnchorsLocateCriteria),
		alignof(FAzureSpatialAnchorsLocateCriteria),
		Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AzureSpatialAnchorsLocateCriteria"), sizeof(FAzureSpatialAnchorsLocateCriteria), Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsLocateCriteria_Hash() { return 2768366903U; }
class UScriptStruct* FCoarseLocalizationSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AZURESPATIALANCHORS_API uint32 Get_Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCoarseLocalizationSettings, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("CoarseLocalizationSettings"), sizeof(FCoarseLocalizationSettings), Get_Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Hash());
	}
	return Singleton;
}
template<> AZURESPATIALANCHORS_API UScriptStruct* StaticStruct<FCoarseLocalizationSettings>()
{
	return FCoarseLocalizationSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCoarseLocalizationSettings(FCoarseLocalizationSettings::StaticStruct, TEXT("/Script/AzureSpatialAnchors"), TEXT("CoarseLocalizationSettings"), false, nullptr, nullptr);
static struct FScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFCoarseLocalizationSettings
{
	FScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFCoarseLocalizationSettings()
	{
		UScriptStruct::DeferCppStructOps<FCoarseLocalizationSettings>(FName(TEXT("CoarseLocalizationSettings")));
	}
} ScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFCoarseLocalizationSettings;
	struct Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnable_MetaData[];
#endif
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableGPS_MetaData[];
#endif
		static void NewProp_bEnableGPS_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableGPS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableWifi_MetaData[];
#endif
		static void NewProp_bEnableWifi_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableWifi;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BLEBeaconUUIDs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BLEBeaconUUIDs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BLEBeaconUUIDs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCoarseLocalizationSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnable_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "Comment", "/**\n\x09* If true coarse localization will be active\n\x09*/" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "If true coarse localization will be active" },
	};
#endif
	void Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((FCoarseLocalizationSettings*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCoarseLocalizationSettings), &Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableGPS_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "Comment", "/**\n\x09* If true GPS can be used for localization (\"location\" must also be enabled in Project Settings->Platforms->Hololens->Capabilities)\n\x09*/" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "If true GPS can be used for localization (\"location\" must also be enabled in Project Settings->Platforms->Hololens->Capabilities)" },
	};
#endif
	void Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableGPS_SetBit(void* Obj)
	{
		((FCoarseLocalizationSettings*)Obj)->bEnableGPS = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableGPS = { "bEnableGPS", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCoarseLocalizationSettings), &Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableGPS_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableGPS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableGPS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableWifi_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "Comment", "/**\n\x09* If true WiFi  can be used for localization (\"wiFiControl\" must also be enabled in Project Settings->Platforms->Hololens->Capabilities)\n\x09*/" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "If true WiFi  can be used for localization (\"wiFiControl\" must also be enabled in Project Settings->Platforms->Hololens->Capabilities)" },
	};
#endif
	void Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableWifi_SetBit(void* Obj)
	{
		((FCoarseLocalizationSettings*)Obj)->bEnableWifi = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableWifi = { "bEnableWifi", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCoarseLocalizationSettings), &Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableWifi_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableWifi_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableWifi_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_BLEBeaconUUIDs_Inner = { "BLEBeaconUUIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_BLEBeaconUUIDs_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "Comment", "/**\n\x09 * List of bluetooth beacon uuids that can be used for localization (\"bluetooth\" must also be enabled in Project Settings->Platforms->Hololens->Capabilities)\n\x09 */" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
		{ "ToolTip", "List of bluetooth beacon uuids that can be used for localization (\"bluetooth\" must also be enabled in Project Settings->Platforms->Hololens->Capabilities)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_BLEBeaconUUIDs = { "BLEBeaconUUIDs", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCoarseLocalizationSettings, BLEBeaconUUIDs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_BLEBeaconUUIDs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_BLEBeaconUUIDs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableGPS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_bEnableWifi,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_BLEBeaconUUIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::NewProp_BLEBeaconUUIDs,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
		nullptr,
		&NewStructOps,
		"CoarseLocalizationSettings",
		sizeof(FCoarseLocalizationSettings),
		alignof(FCoarseLocalizationSettings),
		Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCoarseLocalizationSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CoarseLocalizationSettings"), sizeof(FCoarseLocalizationSettings), Get_Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCoarseLocalizationSettings_Hash() { return 3320259553U; }
class UScriptStruct* FAzureSpatialAnchorsSessionConfiguration::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AZURESPATIALANCHORS_API uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration, Z_Construct_UPackage__Script_AzureSpatialAnchors(), TEXT("AzureSpatialAnchorsSessionConfiguration"), sizeof(FAzureSpatialAnchorsSessionConfiguration), Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Hash());
	}
	return Singleton;
}
template<> AZURESPATIALANCHORS_API UScriptStruct* StaticStruct<FAzureSpatialAnchorsSessionConfiguration>()
{
	return FAzureSpatialAnchorsSessionConfiguration::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration(FAzureSpatialAnchorsSessionConfiguration::StaticStruct, TEXT("/Script/AzureSpatialAnchors"), TEXT("AzureSpatialAnchorsSessionConfiguration"), false, nullptr, nullptr);
static struct FScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsSessionConfiguration
{
	FScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsSessionConfiguration()
	{
		UScriptStruct::DeferCppStructOps<FAzureSpatialAnchorsSessionConfiguration>(FName(TEXT("AzureSpatialAnchorsSessionConfiguration")));
	}
} ScriptStruct_AzureSpatialAnchors_StaticRegisterNativesFAzureSpatialAnchorsSessionConfiguration;
	struct Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccessToken_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AccessToken;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccountId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AccountId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccountKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AccountKey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccountDomain_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AccountDomain;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AuthenticationToken_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AuthenticationToken;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAzureSpatialAnchorsSessionConfiguration>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccessToken_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccessToken = { "AccessToken", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsSessionConfiguration, AccessToken), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccessToken_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccessToken_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountId_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountId = { "AccountId", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsSessionConfiguration, AccountId), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountKey_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountKey = { "AccountKey", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsSessionConfiguration, AccountKey), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountKey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountDomain_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountDomain = { "AccountDomain", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsSessionConfiguration, AccountDomain), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountDomain_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountDomain_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AuthenticationToken_MetaData[] = {
		{ "Category", "AzureSpatialAnchors" },
		{ "ModuleRelativePath", "Public/AzureSpatialAnchorsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AuthenticationToken = { "AuthenticationToken", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAzureSpatialAnchorsSessionConfiguration, AuthenticationToken), METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AuthenticationToken_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AuthenticationToken_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccessToken,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AccountDomain,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::NewProp_AuthenticationToken,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AzureSpatialAnchors,
		nullptr,
		&NewStructOps,
		"AzureSpatialAnchorsSessionConfiguration",
		sizeof(FAzureSpatialAnchorsSessionConfiguration),
		alignof(FAzureSpatialAnchorsSessionConfiguration),
		Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AzureSpatialAnchors();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AzureSpatialAnchorsSessionConfiguration"), sizeof(FAzureSpatialAnchorsSessionConfiguration), Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAzureSpatialAnchorsSessionConfiguration_Hash() { return 2585215070U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
