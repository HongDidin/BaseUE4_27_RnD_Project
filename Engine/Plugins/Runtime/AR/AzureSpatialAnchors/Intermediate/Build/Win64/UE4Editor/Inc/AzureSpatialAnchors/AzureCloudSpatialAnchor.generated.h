// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
 
#ifdef AZURESPATIALANCHORS_AzureCloudSpatialAnchor_generated_h
#error "AzureCloudSpatialAnchor.generated.h already included, missing '#pragma once' in AzureCloudSpatialAnchor.h"
#endif
#define AZURESPATIALANCHORS_AzureCloudSpatialAnchor_generated_h

#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetExpiration); \
	DECLARE_FUNCTION(execGetExpiration); \
	DECLARE_FUNCTION(execSetAppProperties); \
	DECLARE_FUNCTION(execGetAppProperties); \
	DECLARE_FUNCTION(execGetAzureCloudIdentifier);


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetExpiration); \
	DECLARE_FUNCTION(execGetExpiration); \
	DECLARE_FUNCTION(execSetAppProperties); \
	DECLARE_FUNCTION(execGetAppProperties); \
	DECLARE_FUNCTION(execGetAzureCloudIdentifier);


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAzureCloudSpatialAnchor(); \
	friend struct Z_Construct_UClass_UAzureCloudSpatialAnchor_Statics; \
public: \
	DECLARE_CLASS(UAzureCloudSpatialAnchor, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AzureSpatialAnchors"), NO_API) \
	DECLARE_SERIALIZER(UAzureCloudSpatialAnchor)


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUAzureCloudSpatialAnchor(); \
	friend struct Z_Construct_UClass_UAzureCloudSpatialAnchor_Statics; \
public: \
	DECLARE_CLASS(UAzureCloudSpatialAnchor, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AzureSpatialAnchors"), NO_API) \
	DECLARE_SERIALIZER(UAzureCloudSpatialAnchor)


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAzureCloudSpatialAnchor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAzureCloudSpatialAnchor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAzureCloudSpatialAnchor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAzureCloudSpatialAnchor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAzureCloudSpatialAnchor(UAzureCloudSpatialAnchor&&); \
	NO_API UAzureCloudSpatialAnchor(const UAzureCloudSpatialAnchor&); \
public:


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAzureCloudSpatialAnchor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAzureCloudSpatialAnchor(UAzureCloudSpatialAnchor&&); \
	NO_API UAzureCloudSpatialAnchor(const UAzureCloudSpatialAnchor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAzureCloudSpatialAnchor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAzureCloudSpatialAnchor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAzureCloudSpatialAnchor)


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_10_PROLOG
#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_INCLASS \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AZURESPATIALANCHORS_API UClass* StaticClass<class UAzureCloudSpatialAnchor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureCloudSpatialAnchor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
