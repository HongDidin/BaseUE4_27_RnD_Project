// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EAzureSpatialAnchorsSessionUserFeedback : uint8;
enum class EAzureSpatialAnchorsLocateAnchorStatus : uint8;
class UAzureCloudSpatialAnchor;
#ifdef AZURESPATIALANCHORS_AzureSpatialAnchorsEventComponent_generated_h
#error "AzureSpatialAnchorsEventComponent.generated.h already included, missing '#pragma once' in AzureSpatialAnchorsEventComponent.h"
#endif
#define AZURESPATIALANCHORS_AzureSpatialAnchorsEventComponent_generated_h

#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_34_DELEGATE \
struct AzureSpatialAnchorsEventComponent_eventASASessionUpdatedDelegate_Parms \
{ \
	float ReadyForCreateProgress; \
	float RecommendedForCreateProgress; \
	int32 SessionCreateHash; \
	int32 SessionLocateHash; \
	EAzureSpatialAnchorsSessionUserFeedback Feedback; \
}; \
static inline void FASASessionUpdatedDelegate_DelegateWrapper(const FMulticastScriptDelegate& ASASessionUpdatedDelegate, float ReadyForCreateProgress, float RecommendedForCreateProgress, int32 SessionCreateHash, int32 SessionLocateHash, EAzureSpatialAnchorsSessionUserFeedback Feedback) \
{ \
	AzureSpatialAnchorsEventComponent_eventASASessionUpdatedDelegate_Parms Parms; \
	Parms.ReadyForCreateProgress=ReadyForCreateProgress; \
	Parms.RecommendedForCreateProgress=RecommendedForCreateProgress; \
	Parms.SessionCreateHash=SessionCreateHash; \
	Parms.SessionLocateHash=SessionLocateHash; \
	Parms.Feedback=Feedback; \
	ASASessionUpdatedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_30_DELEGATE \
struct AzureSpatialAnchorsEventComponent_eventASALocateAnchorsCompletedDelegate_Parms \
{ \
	int32 WatcherIdentifier; \
	bool WasCanceled; \
}; \
static inline void FASALocateAnchorsCompletedDelegate_DelegateWrapper(const FMulticastScriptDelegate& ASALocateAnchorsCompletedDelegate, int32 WatcherIdentifier, bool WasCanceled) \
{ \
	AzureSpatialAnchorsEventComponent_eventASALocateAnchorsCompletedDelegate_Parms Parms; \
	Parms.WatcherIdentifier=WatcherIdentifier; \
	Parms.WasCanceled=WasCanceled ? true : false; \
	ASALocateAnchorsCompletedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_26_DELEGATE \
struct AzureSpatialAnchorsEventComponent_eventASAAnchorLocatedDelegate_Parms \
{ \
	int32 WatcherIdentifier; \
	EAzureSpatialAnchorsLocateAnchorStatus Status; \
	UAzureCloudSpatialAnchor* CloudSpatialAnchor; \
}; \
static inline void FASAAnchorLocatedDelegate_DelegateWrapper(const FMulticastScriptDelegate& ASAAnchorLocatedDelegate, int32 WatcherIdentifier, EAzureSpatialAnchorsLocateAnchorStatus Status, UAzureCloudSpatialAnchor* CloudSpatialAnchor) \
{ \
	AzureSpatialAnchorsEventComponent_eventASAAnchorLocatedDelegate_Parms Parms; \
	Parms.WatcherIdentifier=WatcherIdentifier; \
	Parms.Status=Status; \
	Parms.CloudSpatialAnchor=CloudSpatialAnchor; \
	ASAAnchorLocatedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAzureSpatialAnchorsEventComponent(); \
	friend struct Z_Construct_UClass_UAzureSpatialAnchorsEventComponent_Statics; \
public: \
	DECLARE_CLASS(UAzureSpatialAnchorsEventComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AzureSpatialAnchors"), NO_API) \
	DECLARE_SERIALIZER(UAzureSpatialAnchorsEventComponent)


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUAzureSpatialAnchorsEventComponent(); \
	friend struct Z_Construct_UClass_UAzureSpatialAnchorsEventComponent_Statics; \
public: \
	DECLARE_CLASS(UAzureSpatialAnchorsEventComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AzureSpatialAnchors"), NO_API) \
	DECLARE_SERIALIZER(UAzureSpatialAnchorsEventComponent)


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAzureSpatialAnchorsEventComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAzureSpatialAnchorsEventComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAzureSpatialAnchorsEventComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAzureSpatialAnchorsEventComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAzureSpatialAnchorsEventComponent(UAzureSpatialAnchorsEventComponent&&); \
	NO_API UAzureSpatialAnchorsEventComponent(const UAzureSpatialAnchorsEventComponent&); \
public:


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAzureSpatialAnchorsEventComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAzureSpatialAnchorsEventComponent(UAzureSpatialAnchorsEventComponent&&); \
	NO_API UAzureSpatialAnchorsEventComponent(const UAzureSpatialAnchorsEventComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAzureSpatialAnchorsEventComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAzureSpatialAnchorsEventComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAzureSpatialAnchorsEventComponent)


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_17_PROLOG
#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_INCLASS \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AzureSpatialAnchorsEventComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AZURESPATIALANCHORS_API UClass* StaticClass<class UAzureSpatialAnchorsEventComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_AzureSpatialAnchors_Source_AzureSpatialAnchors_Public_AzureSpatialAnchorsEventComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
