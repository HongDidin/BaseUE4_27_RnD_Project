// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HoloLensPlatformEditor/Private/HoloLensLocalizedResources.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHoloLensLocalizedResources() {}
// Cross Module References
	HOLOLENSPLATFORMEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources();
	UPackage* Z_Construct_UPackage__Script_HoloLensPlatformEditor();
	HOLOLENSPLATFORMEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources();
	HOLOLENSPLATFORMEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources();
// End Cross Module References
class UScriptStruct* FHoloLensCorePackageLocalizedResources::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HOLOLENSPLATFORMEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources, Z_Construct_UPackage__Script_HoloLensPlatformEditor(), TEXT("HoloLensCorePackageLocalizedResources"), sizeof(FHoloLensCorePackageLocalizedResources), Get_Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Hash());
	}
	return Singleton;
}
template<> HOLOLENSPLATFORMEDITOR_API UScriptStruct* StaticStruct<FHoloLensCorePackageLocalizedResources>()
{
	return FHoloLensCorePackageLocalizedResources::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHoloLensCorePackageLocalizedResources(FHoloLensCorePackageLocalizedResources::StaticStruct, TEXT("/Script/HoloLensPlatformEditor"), TEXT("HoloLensCorePackageLocalizedResources"), false, nullptr, nullptr);
static struct FScriptStruct_HoloLensPlatformEditor_StaticRegisterNativesFHoloLensCorePackageLocalizedResources
{
	FScriptStruct_HoloLensPlatformEditor_StaticRegisterNativesFHoloLensCorePackageLocalizedResources()
	{
		UScriptStruct::DeferCppStructOps<FHoloLensCorePackageLocalizedResources>(FName(TEXT("HoloLensCorePackageLocalizedResources")));
	}
} ScriptStruct_HoloLensPlatformEditor_StaticRegisterNativesFHoloLensCorePackageLocalizedResources;
	struct Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CultureId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CultureId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Strings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Images_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Images;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHoloLensCorePackageLocalizedResources>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_CultureId_MetaData[] = {
		{ "Category", "Packaging" },
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_CultureId = { "CultureId", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHoloLensCorePackageLocalizedResources, CultureId), METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_CultureId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_CultureId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_Strings_MetaData[] = {
		{ "Category", "Packaging" },
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_Strings = { "Strings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHoloLensCorePackageLocalizedResources, Strings), Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources, METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_Strings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_Strings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_Images_MetaData[] = {
		{ "Category", "Packaging" },
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_Images = { "Images", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHoloLensCorePackageLocalizedResources, Images), Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources, METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_Images_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_Images_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_CultureId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_Strings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::NewProp_Images,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HoloLensPlatformEditor,
		nullptr,
		&NewStructOps,
		"HoloLensCorePackageLocalizedResources",
		sizeof(FHoloLensCorePackageLocalizedResources),
		alignof(FHoloLensCorePackageLocalizedResources),
		Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HoloLensPlatformEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HoloLensCorePackageLocalizedResources"), sizeof(FHoloLensCorePackageLocalizedResources), Get_Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Hash() { return 1025909178U; }
class UScriptStruct* FHoloLensCorePackageImageResources::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HOLOLENSPLATFORMEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources, Z_Construct_UPackage__Script_HoloLensPlatformEditor(), TEXT("HoloLensCorePackageImageResources"), sizeof(FHoloLensCorePackageImageResources), Get_Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Hash());
	}
	return Singleton;
}
template<> HOLOLENSPLATFORMEDITOR_API UScriptStruct* StaticStruct<FHoloLensCorePackageImageResources>()
{
	return FHoloLensCorePackageImageResources::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHoloLensCorePackageImageResources(FHoloLensCorePackageImageResources::StaticStruct, TEXT("/Script/HoloLensPlatformEditor"), TEXT("HoloLensCorePackageImageResources"), false, nullptr, nullptr);
static struct FScriptStruct_HoloLensPlatformEditor_StaticRegisterNativesFHoloLensCorePackageImageResources
{
	FScriptStruct_HoloLensPlatformEditor_StaticRegisterNativesFHoloLensCorePackageImageResources()
	{
		UScriptStruct::DeferCppStructOps<FHoloLensCorePackageImageResources>(FName(TEXT("HoloLensCorePackageImageResources")));
	}
} ScriptStruct_HoloLensPlatformEditor_StaticRegisterNativesFHoloLensCorePackageImageResources;
	struct Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHoloLensCorePackageImageResources>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HoloLensPlatformEditor,
		nullptr,
		&NewStructOps,
		"HoloLensCorePackageImageResources",
		sizeof(FHoloLensCorePackageImageResources),
		alignof(FHoloLensCorePackageImageResources),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HoloLensPlatformEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HoloLensCorePackageImageResources"), sizeof(FHoloLensCorePackageImageResources), Get_Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Hash() { return 293268107U; }
class UScriptStruct* FHoloLensCorePackageStringResources::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern HOLOLENSPLATFORMEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources, Z_Construct_UPackage__Script_HoloLensPlatformEditor(), TEXT("HoloLensCorePackageStringResources"), sizeof(FHoloLensCorePackageStringResources), Get_Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Hash());
	}
	return Singleton;
}
template<> HOLOLENSPLATFORMEDITOR_API UScriptStruct* StaticStruct<FHoloLensCorePackageStringResources>()
{
	return FHoloLensCorePackageStringResources::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHoloLensCorePackageStringResources(FHoloLensCorePackageStringResources::StaticStruct, TEXT("/Script/HoloLensPlatformEditor"), TEXT("HoloLensCorePackageStringResources"), false, nullptr, nullptr);
static struct FScriptStruct_HoloLensPlatformEditor_StaticRegisterNativesFHoloLensCorePackageStringResources
{
	FScriptStruct_HoloLensPlatformEditor_StaticRegisterNativesFHoloLensCorePackageStringResources()
	{
		UScriptStruct::DeferCppStructOps<FHoloLensCorePackageStringResources>(FName(TEXT("HoloLensCorePackageStringResources")));
	}
} ScriptStruct_HoloLensPlatformEditor_StaticRegisterNativesFHoloLensCorePackageStringResources;
	struct Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageDisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PackageDisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PublisherDisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PublisherDisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PackageDescription;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ApplicationDisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ApplicationDisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ApplicationDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ApplicationDescription;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHoloLensCorePackageStringResources>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PackageDisplayName_MetaData[] = {
		{ "Category", "Packaging" },
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PackageDisplayName = { "PackageDisplayName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHoloLensCorePackageStringResources, PackageDisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PackageDisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PackageDisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PublisherDisplayName_MetaData[] = {
		{ "Category", "Packaging" },
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PublisherDisplayName = { "PublisherDisplayName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHoloLensCorePackageStringResources, PublisherDisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PublisherDisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PublisherDisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PackageDescription_MetaData[] = {
		{ "Category", "Packaging" },
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PackageDescription = { "PackageDescription", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHoloLensCorePackageStringResources, PackageDescription), METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PackageDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PackageDescription_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_ApplicationDisplayName_MetaData[] = {
		{ "Category", "Packaging" },
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_ApplicationDisplayName = { "ApplicationDisplayName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHoloLensCorePackageStringResources, ApplicationDisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_ApplicationDisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_ApplicationDisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_ApplicationDescription_MetaData[] = {
		{ "Category", "Packaging" },
		{ "ModuleRelativePath", "Private/HoloLensLocalizedResources.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_ApplicationDescription = { "ApplicationDescription", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHoloLensCorePackageStringResources, ApplicationDescription), METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_ApplicationDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_ApplicationDescription_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PackageDisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PublisherDisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_PackageDescription,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_ApplicationDisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::NewProp_ApplicationDescription,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_HoloLensPlatformEditor,
		nullptr,
		&NewStructOps,
		"HoloLensCorePackageStringResources",
		sizeof(FHoloLensCorePackageStringResources),
		alignof(FHoloLensCorePackageStringResources),
		Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_HoloLensPlatformEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HoloLensCorePackageStringResources"), sizeof(FHoloLensCorePackageStringResources), Get_Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Hash() { return 2005915149U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
