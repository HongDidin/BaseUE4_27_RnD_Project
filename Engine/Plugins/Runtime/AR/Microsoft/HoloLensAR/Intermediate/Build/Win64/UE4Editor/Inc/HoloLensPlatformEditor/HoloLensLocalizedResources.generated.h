// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOLOLENSPLATFORMEDITOR_HoloLensLocalizedResources_generated_h
#error "HoloLensLocalizedResources.generated.h already included, missing '#pragma once' in HoloLensLocalizedResources.h"
#endif
#define HOLOLENSPLATFORMEDITOR_HoloLensLocalizedResources_generated_h

#define Engine_Plugins_Runtime_AR_Microsoft_HoloLensAR_Source_HoloLensPlatformEditor_Private_HoloLensLocalizedResources_h_37_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources_Statics; \
	HOLOLENSPLATFORMEDITOR_API static class UScriptStruct* StaticStruct();


template<> HOLOLENSPLATFORMEDITOR_API UScriptStruct* StaticStruct<struct FHoloLensCorePackageLocalizedResources>();

#define Engine_Plugins_Runtime_AR_Microsoft_HoloLensAR_Source_HoloLensPlatformEditor_Private_HoloLensLocalizedResources_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHoloLensCorePackageImageResources_Statics; \
	HOLOLENSPLATFORMEDITOR_API static class UScriptStruct* StaticStruct();


template<> HOLOLENSPLATFORMEDITOR_API UScriptStruct* StaticStruct<struct FHoloLensCorePackageImageResources>();

#define Engine_Plugins_Runtime_AR_Microsoft_HoloLensAR_Source_HoloLensPlatformEditor_Private_HoloLensLocalizedResources_h_10_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHoloLensCorePackageStringResources_Statics; \
	HOLOLENSPLATFORMEDITOR_API static class UScriptStruct* StaticStruct();


template<> HOLOLENSPLATFORMEDITOR_API UScriptStruct* StaticStruct<struct FHoloLensCorePackageStringResources>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Microsoft_HoloLensAR_Source_HoloLensPlatformEditor_Private_HoloLensLocalizedResources_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
