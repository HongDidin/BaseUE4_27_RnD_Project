// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HoloLensPlatformEditor/Private/HoloLensTargetSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHoloLensTargetSettings() {}
// Cross Module References
	HOLOLENSPLATFORMEDITOR_API UClass* Z_Construct_UClass_UHoloLensTargetSettings_NoRegister();
	HOLOLENSPLATFORMEDITOR_API UClass* Z_Construct_UClass_UHoloLensTargetSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_HoloLensPlatformEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	HOLOLENSPLATFORMEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources();
	WINDOWSTARGETPLATFORM_API UEnum* Z_Construct_UEnum_WindowsTargetPlatform_ECompilerVersion();
// End Cross Module References
	void UHoloLensTargetSettings::StaticRegisterNativesUHoloLensTargetSettings()
	{
	}
	UClass* Z_Construct_UClass_UHoloLensTargetSettings_NoRegister()
	{
		return UHoloLensTargetSettings::StaticClass();
	}
	struct Z_Construct_UClass_UHoloLensTargetSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBuildForEmulation_MetaData[];
#endif
		static void NewProp_bBuildForEmulation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBuildForEmulation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBuildForDevice_MetaData[];
#endif
		static void NewProp_bBuildForDevice_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBuildForDevice;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseNameForLogo_MetaData[];
#endif
		static void NewProp_bUseNameForLogo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseNameForLogo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBuildForRetailWindowsStore_MetaData[];
#endif
		static void NewProp_bBuildForRetailWindowsStore_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBuildForRetailWindowsStore;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoIncrementVersion_MetaData[];
#endif
		static void NewProp_bAutoIncrementVersion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoIncrementVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldCreateAppInstaller_MetaData[];
#endif
		static void NewProp_bShouldCreateAppInstaller_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldCreateAppInstaller;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppInstallerInstallationURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AppInstallerInstallationURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoursBetweenUpdateChecks_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_HoursBetweenUpdateChecks;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnablePIXProfiling_MetaData[];
#endif
		static void NewProp_bEnablePIXProfiling_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnablePIXProfiling;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TileBackgroundColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TileBackgroundColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SplashScreenBackgroundColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SplashScreenBackgroundColor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PerCultureResources_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerCultureResources_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PerCultureResources;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetDeviceFamily_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TargetDeviceFamily;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinimumPlatformVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MinimumPlatformVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumPlatformVersionTested_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MaximumPlatformVersionTested;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxTrianglesPerCubicMeter_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxTrianglesPerCubicMeter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpatialMeshingVolumeSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpatialMeshingVolumeSize;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CompilerVersion_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CompilerVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CompilerVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Windows10SDKVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Windows10SDKVersion;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CapabilityList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapabilityList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CapabilityList;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceCapabilityList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceCapabilityList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeviceCapabilityList;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UapCapabilityList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UapCapabilityList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UapCapabilityList;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Uap2CapabilityList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Uap2CapabilityList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Uap2CapabilityList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSetDefaultCapabilities_MetaData[];
#endif
		static void NewProp_bSetDefaultCapabilities_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSetDefaultCapabilities;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpatializationPlugin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SpatializationPlugin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReverbPlugin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReverbPlugin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OcclusionPlugin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OcclusionPlugin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoundCueCookQualityIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SoundCueCookQualityIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHoloLensTargetSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_HoloLensPlatformEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements the settings for the HoloLens target platform.\n */" },
		{ "IncludePath", "HoloLensTargetSettings.h" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Implements the settings for the HoloLens target platform." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForEmulation_MetaData[] = {
		{ "Category", "HoloLens" },
		{ "Comment", "/**\n\x09 * When checked, a build that can be run via emulation is added\n\x09 */" },
		{ "DisplayName", "Build for HoloLens Emulation" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "When checked, a build that can be run via emulation is added" },
	};
#endif
	void Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForEmulation_SetBit(void* Obj)
	{
		((UHoloLensTargetSettings*)Obj)->bBuildForEmulation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForEmulation = { "bBuildForEmulation", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHoloLensTargetSettings), &Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForEmulation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForEmulation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForEmulation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForDevice_MetaData[] = {
		{ "Category", "HoloLens" },
		{ "Comment", "/**\n\x09 * When the box checked the final bundle has binaries of ARM64 OSes.\n\x09 */" },
		{ "DisplayName", "Build for HoloLens Device" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "When the box checked the final bundle has binaries of ARM64 OSes." },
	};
#endif
	void Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForDevice_SetBit(void* Obj)
	{
		((UHoloLensTargetSettings*)Obj)->bBuildForDevice = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForDevice = { "bBuildForDevice", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHoloLensTargetSettings), &Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForDevice_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForDevice_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForDevice_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bUseNameForLogo_MetaData[] = {
		{ "Category", "Packaging" },
		{ "DisplayName", "Use Name in App Logo" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
	};
#endif
	void Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bUseNameForLogo_SetBit(void* Obj)
	{
		((UHoloLensTargetSettings*)Obj)->bUseNameForLogo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bUseNameForLogo = { "bUseNameForLogo", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHoloLensTargetSettings), &Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bUseNameForLogo_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bUseNameForLogo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bUseNameForLogo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForRetailWindowsStore_MetaData[] = {
		{ "Category", "Packaging" },
		{ "Comment", "/**\n\x09 * Controls whether to use the retail Windows Store environment for license checks.  This must be turned on\n\x09 * when building for submission to the Windows Store, or when sideloading outside of Developer Mode.  Note,\n\x09 * however, that testing a build with this flag enables requires that the product is listed in the retail\n\x09 * catalog for the Windows Store.\n\x09 */" },
		{ "DisplayName", "Use Retail Windows Store Environment" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Controls whether to use the retail Windows Store environment for license checks.  This must be turned on\nwhen building for submission to the Windows Store, or when sideloading outside of Developer Mode.  Note,\nhowever, that testing a build with this flag enables requires that the product is listed in the retail\ncatalog for the Windows Store." },
	};
#endif
	void Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForRetailWindowsStore_SetBit(void* Obj)
	{
		((UHoloLensTargetSettings*)Obj)->bBuildForRetailWindowsStore = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForRetailWindowsStore = { "bBuildForRetailWindowsStore", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHoloLensTargetSettings), &Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForRetailWindowsStore_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForRetailWindowsStore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForRetailWindowsStore_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bAutoIncrementVersion_MetaData[] = {
		{ "Category", "Packaging" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
	};
#endif
	void Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bAutoIncrementVersion_SetBit(void* Obj)
	{
		((UHoloLensTargetSettings*)Obj)->bAutoIncrementVersion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bAutoIncrementVersion = { "bAutoIncrementVersion", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHoloLensTargetSettings), &Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bAutoIncrementVersion_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bAutoIncrementVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bAutoIncrementVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bShouldCreateAppInstaller_MetaData[] = {
		{ "Category", "App Installer" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
	};
#endif
	void Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bShouldCreateAppInstaller_SetBit(void* Obj)
	{
		((UHoloLensTargetSettings*)Obj)->bShouldCreateAppInstaller = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bShouldCreateAppInstaller = { "bShouldCreateAppInstaller", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHoloLensTargetSettings), &Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bShouldCreateAppInstaller_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bShouldCreateAppInstaller_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bShouldCreateAppInstaller_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_AppInstallerInstallationURL_MetaData[] = {
		{ "Category", "App Installer" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_AppInstallerInstallationURL = { "AppInstallerInstallationURL", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, AppInstallerInstallationURL), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_AppInstallerInstallationURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_AppInstallerInstallationURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_HoursBetweenUpdateChecks_MetaData[] = {
		{ "Category", "App Installer" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "0 will check on every app launch." },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_HoursBetweenUpdateChecks = { "HoursBetweenUpdateChecks", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, HoursBetweenUpdateChecks), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_HoursBetweenUpdateChecks_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_HoursBetweenUpdateChecks_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bEnablePIXProfiling_MetaData[] = {
		{ "Category", "Rendering" },
		{ "DisplayName", "Enable PIX Profiling" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
	};
#endif
	void Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bEnablePIXProfiling_SetBit(void* Obj)
	{
		((UHoloLensTargetSettings*)Obj)->bEnablePIXProfiling = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bEnablePIXProfiling = { "bEnablePIXProfiling", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHoloLensTargetSettings), &Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bEnablePIXProfiling_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bEnablePIXProfiling_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bEnablePIXProfiling_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_TileBackgroundColor_MetaData[] = {
		{ "Category", "Packaging" },
		{ "HideAlphaChannel", "" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_TileBackgroundColor = { "TileBackgroundColor", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, TileBackgroundColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_TileBackgroundColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_TileBackgroundColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SplashScreenBackgroundColor_MetaData[] = {
		{ "Category", "Packaging" },
		{ "HideAlphaChannel", "" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SplashScreenBackgroundColor = { "SplashScreenBackgroundColor", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, SplashScreenBackgroundColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SplashScreenBackgroundColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SplashScreenBackgroundColor_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_PerCultureResources_Inner = { "PerCultureResources", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FHoloLensCorePackageLocalizedResources, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_PerCultureResources_MetaData[] = {
		{ "Category", "Packaging" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_PerCultureResources = { "PerCultureResources", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, PerCultureResources), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_PerCultureResources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_PerCultureResources_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_TargetDeviceFamily_MetaData[] = {
		{ "Category", "OS Info" },
		{ "Comment", "/**\n\x09 * Identifies the device family that your package will target.\n\x09 */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Identifies the device family that your package will target." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_TargetDeviceFamily = { "TargetDeviceFamily", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, TargetDeviceFamily), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_TargetDeviceFamily_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_TargetDeviceFamily_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MinimumPlatformVersion_MetaData[] = {
		{ "Category", "OS Info" },
		{ "Comment", "/**\n\x09 * Minimum version of the HoloLens platform required to run this title.\n\x09 * It will not be possible to deploy the build on earlier versions.\n\x09 */" },
		{ "DisplayName", "Minimum supported platform version" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Minimum version of the HoloLens platform required to run this title.\nIt will not be possible to deploy the build on earlier versions." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MinimumPlatformVersion = { "MinimumPlatformVersion", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, MinimumPlatformVersion), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MinimumPlatformVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MinimumPlatformVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MaximumPlatformVersionTested_MetaData[] = {
		{ "Category", "OS Info" },
		{ "Comment", "/**\n\x09 * Specifies the maximum version of the universal platform on which the\n\x09 * title is known to work as expected.  When deployed to later versions\n\x09 * the title will experience the runtime behavior of the version given here.\n\x09 */" },
		{ "DisplayName", "Maximum tested platform version" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Specifies the maximum version of the universal platform on which the\ntitle is known to work as expected.  When deployed to later versions\nthe title will experience the runtime behavior of the version given here." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MaximumPlatformVersionTested = { "MaximumPlatformVersionTested", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, MaximumPlatformVersionTested), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MaximumPlatformVersionTested_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MaximumPlatformVersionTested_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MaxTrianglesPerCubicMeter_MetaData[] = {
		{ "Category", "Spatial Mapping" },
		{ "Comment", "/** Used by the HoloLens to indicate triangle density when generating meshes. Defaults to 500 per cubic meter */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Used by the HoloLens to indicate triangle density when generating meshes. Defaults to 500 per cubic meter" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MaxTrianglesPerCubicMeter = { "MaxTrianglesPerCubicMeter", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, MaxTrianglesPerCubicMeter), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MaxTrianglesPerCubicMeter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MaxTrianglesPerCubicMeter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SpatialMeshingVolumeSize_MetaData[] = {
		{ "Category", "Spatial Mapping" },
		{ "Comment", "/** Used by the HoloLens to indicate how large (in meters) of a volume to use for generating meshes. Defaults to a 20m cube */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Used by the HoloLens to indicate how large (in meters) of a volume to use for generating meshes. Defaults to a 20m cube" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SpatialMeshingVolumeSize = { "SpatialMeshingVolumeSize", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, SpatialMeshingVolumeSize), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SpatialMeshingVolumeSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SpatialMeshingVolumeSize_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CompilerVersion_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CompilerVersion_MetaData[] = {
		{ "Category", "Toolchain" },
		{ "Comment", "/** The compiler version to use for this project. May be different to the chosen IDE. */" },
		{ "DisplayName", "Compiler Version" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "The compiler version to use for this project. May be different to the chosen IDE." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CompilerVersion = { "CompilerVersion", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, CompilerVersion), Z_Construct_UEnum_WindowsTargetPlatform_ECompilerVersion, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CompilerVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CompilerVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Windows10SDKVersion_MetaData[] = {
		{ "Category", "Toolchain" },
		{ "DisplayName", "Windows 10 SDK Version" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Windows10SDKVersion = { "Windows10SDKVersion", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, Windows10SDKVersion), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Windows10SDKVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Windows10SDKVersion_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CapabilityList_Inner = { "CapabilityList", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CapabilityList_MetaData[] = {
		{ "Category", "Capabilities" },
		{ "Comment", "/**\n\x09 * List of supported <Capability><Capability> elements for the application.\n\x09 */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "List of supported <Capability><Capability> elements for the application." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CapabilityList = { "CapabilityList", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, CapabilityList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CapabilityList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CapabilityList_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_DeviceCapabilityList_Inner = { "DeviceCapabilityList", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_DeviceCapabilityList_MetaData[] = {
		{ "Category", "Capabilities" },
		{ "Comment", "/**\n\x09 * List of supported <Capability><DeviceCapability> elements for the application.\n\x09 */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "List of supported <Capability><DeviceCapability> elements for the application." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_DeviceCapabilityList = { "DeviceCapabilityList", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, DeviceCapabilityList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_DeviceCapabilityList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_DeviceCapabilityList_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_UapCapabilityList_Inner = { "UapCapabilityList", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_UapCapabilityList_MetaData[] = {
		{ "Category", "Capabilities" },
		{ "Comment", "/**\n\x09 * List of supported <Capability><uap:Capability> elements for the application.\n\x09 */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "List of supported <Capability><uap:Capability> elements for the application." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_UapCapabilityList = { "UapCapabilityList", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, UapCapabilityList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_UapCapabilityList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_UapCapabilityList_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Uap2CapabilityList_Inner = { "Uap2CapabilityList", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Uap2CapabilityList_MetaData[] = {
		{ "Category", "Capabilities" },
		{ "Comment", "/**\n\x09 * List of supported <Capability><uap2:Capability> elements for the application.\n\x09 */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "List of supported <Capability><uap2:Capability> elements for the application." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Uap2CapabilityList = { "Uap2CapabilityList", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, Uap2CapabilityList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Uap2CapabilityList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Uap2CapabilityList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bSetDefaultCapabilities_MetaData[] = {
		{ "Category", "Capabilities" },
		{ "Comment", "/**\n\x09 * Set default capabilities (InternetClientServer and PrivateNetworkClientServer) for the application.\n\x09 */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Set default capabilities (InternetClientServer and PrivateNetworkClientServer) for the application." },
	};
#endif
	void Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bSetDefaultCapabilities_SetBit(void* Obj)
	{
		((UHoloLensTargetSettings*)Obj)->bSetDefaultCapabilities = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bSetDefaultCapabilities = { "bSetDefaultCapabilities", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHoloLensTargetSettings), &Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bSetDefaultCapabilities_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bSetDefaultCapabilities_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bSetDefaultCapabilities_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SpatializationPlugin_MetaData[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Which of the currently enabled spatialization plugins to use on Windows. */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Which of the currently enabled spatialization plugins to use on Windows." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SpatializationPlugin = { "SpatializationPlugin", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, SpatializationPlugin), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SpatializationPlugin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SpatializationPlugin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_ReverbPlugin_MetaData[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Which of the currently enabled reverb plugins to use on Windows. */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Which of the currently enabled reverb plugins to use on Windows." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_ReverbPlugin = { "ReverbPlugin", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, ReverbPlugin), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_ReverbPlugin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_ReverbPlugin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_OcclusionPlugin_MetaData[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Which of the currently enabled occlusion plugins to use on Windows. */" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Which of the currently enabled occlusion plugins to use on Windows." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_OcclusionPlugin = { "OcclusionPlugin", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, OcclusionPlugin), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_OcclusionPlugin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_OcclusionPlugin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SoundCueCookQualityIndex_MetaData[] = {
		{ "Category", "Audio|CookOverrides" },
		{ "Comment", "/** Quality Level to COOK SoundCues at (if set, all other levels will be stripped by the cooker). */" },
		{ "DisplayName", "Sound Cue Cook Quality" },
		{ "ModuleRelativePath", "Private/HoloLensTargetSettings.h" },
		{ "ToolTip", "Quality Level to COOK SoundCues at (if set, all other levels will be stripped by the cooker)." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SoundCueCookQualityIndex = { "SoundCueCookQualityIndex", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHoloLensTargetSettings, SoundCueCookQualityIndex), METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SoundCueCookQualityIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SoundCueCookQualityIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UHoloLensTargetSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForEmulation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForDevice,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bUseNameForLogo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bBuildForRetailWindowsStore,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bAutoIncrementVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bShouldCreateAppInstaller,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_AppInstallerInstallationURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_HoursBetweenUpdateChecks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bEnablePIXProfiling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_TileBackgroundColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SplashScreenBackgroundColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_PerCultureResources_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_PerCultureResources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_TargetDeviceFamily,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MinimumPlatformVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MaximumPlatformVersionTested,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_MaxTrianglesPerCubicMeter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SpatialMeshingVolumeSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CompilerVersion_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CompilerVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Windows10SDKVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CapabilityList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_CapabilityList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_DeviceCapabilityList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_DeviceCapabilityList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_UapCapabilityList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_UapCapabilityList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Uap2CapabilityList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_Uap2CapabilityList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_bSetDefaultCapabilities,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SpatializationPlugin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_ReverbPlugin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_OcclusionPlugin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHoloLensTargetSettings_Statics::NewProp_SoundCueCookQualityIndex,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHoloLensTargetSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHoloLensTargetSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHoloLensTargetSettings_Statics::ClassParams = {
		&UHoloLensTargetSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UHoloLensTargetSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UHoloLensTargetSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHoloLensTargetSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHoloLensTargetSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHoloLensTargetSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHoloLensTargetSettings, 1519231577);
	template<> HOLOLENSPLATFORMEDITOR_API UClass* StaticClass<UHoloLensTargetSettings>()
	{
		return UHoloLensTargetSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHoloLensTargetSettings(Z_Construct_UClass_UHoloLensTargetSettings, &UHoloLensTargetSettings::StaticClass, TEXT("/Script/HoloLensPlatformEditor"), TEXT("UHoloLensTargetSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHoloLensTargetSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
