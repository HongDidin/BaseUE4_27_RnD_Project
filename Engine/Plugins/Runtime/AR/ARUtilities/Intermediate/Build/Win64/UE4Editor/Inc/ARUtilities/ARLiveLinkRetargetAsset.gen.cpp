// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ARUtilities/Public/ARLiveLinkRetargetAsset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeARLiveLinkRetargetAsset() {}
// Cross Module References
	ARUTILITIES_API UEnum* Z_Construct_UEnum_ARUtilities_EARLiveLinkSourceType();
	UPackage* Z_Construct_UPackage__Script_ARUtilities();
	ARUTILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FARKitPoseTrackingConfig();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ARUTILITIES_API UClass* Z_Construct_UClass_UARLiveLinkRetargetAsset_NoRegister();
	ARUTILITIES_API UClass* Z_Construct_UClass_UARLiveLinkRetargetAsset();
	LIVELINK_API UClass* Z_Construct_UClass_ULiveLinkRetargetAsset();
// End Cross Module References
	static UEnum* EARLiveLinkSourceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ARUtilities_EARLiveLinkSourceType, Z_Construct_UPackage__Script_ARUtilities(), TEXT("EARLiveLinkSourceType"));
		}
		return Singleton;
	}
	template<> ARUTILITIES_API UEnum* StaticEnum<EARLiveLinkSourceType>()
	{
		return EARLiveLinkSourceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EARLiveLinkSourceType(EARLiveLinkSourceType_StaticEnum, TEXT("/Script/ARUtilities"), TEXT("EARLiveLinkSourceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ARUtilities_EARLiveLinkSourceType_Hash() { return 2606307086U; }
	UEnum* Z_Construct_UEnum_ARUtilities_EARLiveLinkSourceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ARUtilities();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EARLiveLinkSourceType"), 0, Get_Z_Construct_UEnum_ARUtilities_EARLiveLinkSourceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EARLiveLinkSourceType::None", (int64)EARLiveLinkSourceType::None },
				{ "EARLiveLinkSourceType::ARKitPoseTracking", (int64)EARLiveLinkSourceType::ARKitPoseTracking },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ARKitPoseTracking.Name", "EARLiveLinkSourceType::ARKitPoseTracking" },
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/ARLiveLinkRetargetAsset.h" },
				{ "None.Name", "EARLiveLinkSourceType::None" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ARUtilities,
				nullptr,
				"EARLiveLinkSourceType",
				"EARLiveLinkSourceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FARKitPoseTrackingConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ARUTILITIES_API uint32 Get_Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FARKitPoseTrackingConfig, Z_Construct_UPackage__Script_ARUtilities(), TEXT("ARKitPoseTrackingConfig"), sizeof(FARKitPoseTrackingConfig), Get_Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Hash());
	}
	return Singleton;
}
template<> ARUTILITIES_API UScriptStruct* StaticStruct<FARKitPoseTrackingConfig>()
{
	return FARKitPoseTrackingConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FARKitPoseTrackingConfig(FARKitPoseTrackingConfig::StaticStruct, TEXT("/Script/ARUtilities"), TEXT("ARKitPoseTrackingConfig"), false, nullptr, nullptr);
static struct FScriptStruct_ARUtilities_StaticRegisterNativesFARKitPoseTrackingConfig
{
	FScriptStruct_ARUtilities_StaticRegisterNativesFARKitPoseTrackingConfig()
	{
		UScriptStruct::DeferCppStructOps<FARKitPoseTrackingConfig>(FName(TEXT("ARKitPoseTrackingConfig")));
	}
} ScriptStruct_ARUtilities_StaticRegisterNativesFARKitPoseTrackingConfig;
	struct Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HumanForward_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HumanForward;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshForward_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeshForward;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/ARLiveLinkRetargetAsset.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FARKitPoseTrackingConfig>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewProp_HumanForward_MetaData[] = {
		{ "Category", "LiveLink|ARKit" },
		{ "ModuleRelativePath", "Public/ARLiveLinkRetargetAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewProp_HumanForward = { "HumanForward", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FARKitPoseTrackingConfig, HumanForward), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewProp_HumanForward_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewProp_HumanForward_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewProp_MeshForward_MetaData[] = {
		{ "Category", "LiveLink|ARKit" },
		{ "ModuleRelativePath", "Public/ARLiveLinkRetargetAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewProp_MeshForward = { "MeshForward", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FARKitPoseTrackingConfig, MeshForward), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewProp_MeshForward_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewProp_MeshForward_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewProp_HumanForward,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::NewProp_MeshForward,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ARUtilities,
		nullptr,
		&NewStructOps,
		"ARKitPoseTrackingConfig",
		sizeof(FARKitPoseTrackingConfig),
		alignof(FARKitPoseTrackingConfig),
		Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FARKitPoseTrackingConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ARUtilities();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ARKitPoseTrackingConfig"), sizeof(FARKitPoseTrackingConfig), Get_Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Hash() { return 109730634U; }
	void UARLiveLinkRetargetAsset::StaticRegisterNativesUARLiveLinkRetargetAsset()
	{
	}
	UClass* Z_Construct_UClass_UARLiveLinkRetargetAsset_NoRegister()
	{
		return UARLiveLinkRetargetAsset::StaticClass();
	}
	struct Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SourceType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SourceType;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_BoneMap_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_BoneMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_BoneMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ARKitPoseTrackingConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ARKitPoseTrackingConfig;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkRetargetAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_ARUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Platform agnostic live link retarget asset */" },
		{ "IncludePath", "ARLiveLinkRetargetAsset.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ARLiveLinkRetargetAsset.h" },
		{ "ToolTip", "Platform agnostic live link retarget asset" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_SourceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_SourceType_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "ModuleRelativePath", "Public/ARLiveLinkRetargetAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_SourceType = { "SourceType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UARLiveLinkRetargetAsset, SourceType), Z_Construct_UEnum_ARUtilities_EARLiveLinkSourceType, METADATA_PARAMS(Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_SourceType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_SourceType_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_BoneMap_ValueProp = { "BoneMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_BoneMap_Key_KeyProp = { "BoneMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_BoneMap_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "Comment", "/** Mapping from AR platform bone name to UE4 skeleton bone name */" },
		{ "ModuleRelativePath", "Public/ARLiveLinkRetargetAsset.h" },
		{ "ToolTip", "Mapping from AR platform bone name to UE4 skeleton bone name" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_BoneMap = { "BoneMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UARLiveLinkRetargetAsset, BoneMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_BoneMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_BoneMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_ARKitPoseTrackingConfig_MetaData[] = {
		{ "Category", "LiveLink|ARKit" },
		{ "Comment", "/** Configuration when using ARKit pose tracking */" },
		{ "editcondition", "SourceType == EARLiveLinkSourceType::ARKitPoseTracking" },
		{ "ModuleRelativePath", "Public/ARLiveLinkRetargetAsset.h" },
		{ "ToolTip", "Configuration when using ARKit pose tracking" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_ARKitPoseTrackingConfig = { "ARKitPoseTrackingConfig", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UARLiveLinkRetargetAsset, ARKitPoseTrackingConfig), Z_Construct_UScriptStruct_FARKitPoseTrackingConfig, METADATA_PARAMS(Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_ARKitPoseTrackingConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_ARKitPoseTrackingConfig_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_SourceType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_SourceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_BoneMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_BoneMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_BoneMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::NewProp_ARKitPoseTrackingConfig,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UARLiveLinkRetargetAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::ClassParams = {
		&UARLiveLinkRetargetAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UARLiveLinkRetargetAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UARLiveLinkRetargetAsset, 1072532066);
	template<> ARUTILITIES_API UClass* StaticClass<UARLiveLinkRetargetAsset>()
	{
		return UARLiveLinkRetargetAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UARLiveLinkRetargetAsset(Z_Construct_UClass_UARLiveLinkRetargetAsset, &UARLiveLinkRetargetAsset::StaticClass, TEXT("/Script/ARUtilities"), TEXT("UARLiveLinkRetargetAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UARLiveLinkRetargetAsset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
