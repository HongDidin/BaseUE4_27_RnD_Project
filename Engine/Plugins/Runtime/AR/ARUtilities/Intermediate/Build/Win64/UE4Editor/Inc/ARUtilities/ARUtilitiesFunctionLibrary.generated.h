// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMaterialInstanceDynamic;
class UTexture;
#ifdef ARUTILITIES_ARUtilitiesFunctionLibrary_generated_h
#error "ARUtilitiesFunctionLibrary.generated.h already included, missing '#pragma once' in ARUtilitiesFunctionLibrary.h"
#endif
#define ARUTILITIES_ARUtilitiesFunctionLibrary_generated_h

#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateWorldToMeterScale); \
	DECLARE_FUNCTION(execUpdateSceneDepthTexture); \
	DECLARE_FUNCTION(execUpdateCameraTextureParam);


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateWorldToMeterScale); \
	DECLARE_FUNCTION(execUpdateSceneDepthTexture); \
	DECLARE_FUNCTION(execUpdateCameraTextureParam);


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUARUtilitiesFunctionLibrary(); \
	friend struct Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UARUtilitiesFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ARUtilities"), NO_API) \
	DECLARE_SERIALIZER(UARUtilitiesFunctionLibrary)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUARUtilitiesFunctionLibrary(); \
	friend struct Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UARUtilitiesFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ARUtilities"), NO_API) \
	DECLARE_SERIALIZER(UARUtilitiesFunctionLibrary)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARUtilitiesFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARUtilitiesFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARUtilitiesFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARUtilitiesFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARUtilitiesFunctionLibrary(UARUtilitiesFunctionLibrary&&); \
	NO_API UARUtilitiesFunctionLibrary(const UARUtilitiesFunctionLibrary&); \
public:


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARUtilitiesFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARUtilitiesFunctionLibrary(UARUtilitiesFunctionLibrary&&); \
	NO_API UARUtilitiesFunctionLibrary(const UARUtilitiesFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARUtilitiesFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARUtilitiesFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARUtilitiesFunctionLibrary)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_13_PROLOG
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_INCLASS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARUTILITIES_API UClass* StaticClass<class UARUtilitiesFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARUtilitiesFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
