// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ARUtilities/Public/ARUtilitiesFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeARUtilitiesFunctionLibrary() {}
// Cross Module References
	ARUTILITIES_API UClass* Z_Construct_UClass_UARUtilitiesFunctionLibrary_NoRegister();
	ARUTILITIES_API UClass* Z_Construct_UClass_UARUtilitiesFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_ARUtilities();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UARUtilitiesFunctionLibrary::execUpdateWorldToMeterScale)
	{
		P_GET_OBJECT(UMaterialInstanceDynamic,Z_Param_MaterialInstance);
		P_GET_PROPERTY(FFloatProperty,Z_Param_WorldToMeterScale);
		P_FINISH;
		P_NATIVE_BEGIN;
		UARUtilitiesFunctionLibrary::UpdateWorldToMeterScale(Z_Param_MaterialInstance,Z_Param_WorldToMeterScale);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UARUtilitiesFunctionLibrary::execUpdateSceneDepthTexture)
	{
		P_GET_OBJECT(UMaterialInstanceDynamic,Z_Param_MaterialInstance);
		P_GET_OBJECT(UTexture,Z_Param_SceneDepthTexture);
		P_GET_PROPERTY(FFloatProperty,Z_Param_DepthToMeterScale);
		P_FINISH;
		P_NATIVE_BEGIN;
		UARUtilitiesFunctionLibrary::UpdateSceneDepthTexture(Z_Param_MaterialInstance,Z_Param_SceneDepthTexture,Z_Param_DepthToMeterScale);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UARUtilitiesFunctionLibrary::execUpdateCameraTextureParam)
	{
		P_GET_OBJECT(UMaterialInstanceDynamic,Z_Param_MaterialInstance);
		P_GET_OBJECT(UTexture,Z_Param_CameraTexture);
		P_GET_PROPERTY(FFloatProperty,Z_Param_ColorScale);
		P_FINISH;
		P_NATIVE_BEGIN;
		UARUtilitiesFunctionLibrary::UpdateCameraTextureParam(Z_Param_MaterialInstance,Z_Param_CameraTexture,Z_Param_ColorScale);
		P_NATIVE_END;
	}
	void UARUtilitiesFunctionLibrary::StaticRegisterNativesUARUtilitiesFunctionLibrary()
	{
		UClass* Class = UARUtilitiesFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "UpdateCameraTextureParam", &UARUtilitiesFunctionLibrary::execUpdateCameraTextureParam },
			{ "UpdateSceneDepthTexture", &UARUtilitiesFunctionLibrary::execUpdateSceneDepthTexture },
			{ "UpdateWorldToMeterScale", &UARUtilitiesFunctionLibrary::execUpdateWorldToMeterScale },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics
	{
		struct ARUtilitiesFunctionLibrary_eventUpdateCameraTextureParam_Parms
		{
			UMaterialInstanceDynamic* MaterialInstance;
			UTexture* CameraTexture;
			float ColorScale;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialInstance;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraTexture;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ColorScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::NewProp_MaterialInstance = { "MaterialInstance", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARUtilitiesFunctionLibrary_eventUpdateCameraTextureParam_Parms, MaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::NewProp_CameraTexture = { "CameraTexture", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARUtilitiesFunctionLibrary_eventUpdateCameraTextureParam_Parms, CameraTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::NewProp_ColorScale = { "ColorScale", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARUtilitiesFunctionLibrary_eventUpdateCameraTextureParam_Parms, ColorScale), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::NewProp_MaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::NewProp_CameraTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::NewProp_ColorScale,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::Function_MetaDataParams[] = {
		{ "Category", "AR Utilities" },
		{ "Comment", "/**\n\x09 * Update material texture parameter using pre-defined names:\n\x09 * For regular texture: CameraTexture\n\x09 * For external texture: ExternalCameraTexture\n\x09 */" },
		{ "CPP_Default_ColorScale", "1.000000" },
		{ "ModuleRelativePath", "Public/ARUtilitiesFunctionLibrary.h" },
		{ "ToolTip", "Update material texture parameter using pre-defined names:\nFor regular texture: CameraTexture\nFor external texture: ExternalCameraTexture" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UARUtilitiesFunctionLibrary, nullptr, "UpdateCameraTextureParam", nullptr, nullptr, sizeof(ARUtilitiesFunctionLibrary_eventUpdateCameraTextureParam_Parms), Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics
	{
		struct ARUtilitiesFunctionLibrary_eventUpdateSceneDepthTexture_Parms
		{
			UMaterialInstanceDynamic* MaterialInstance;
			UTexture* SceneDepthTexture;
			float DepthToMeterScale;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialInstance;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneDepthTexture;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DepthToMeterScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::NewProp_MaterialInstance = { "MaterialInstance", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARUtilitiesFunctionLibrary_eventUpdateSceneDepthTexture_Parms, MaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::NewProp_SceneDepthTexture = { "SceneDepthTexture", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARUtilitiesFunctionLibrary_eventUpdateSceneDepthTexture_Parms, SceneDepthTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::NewProp_DepthToMeterScale = { "DepthToMeterScale", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARUtilitiesFunctionLibrary_eventUpdateSceneDepthTexture_Parms, DepthToMeterScale), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::NewProp_MaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::NewProp_SceneDepthTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::NewProp_DepthToMeterScale,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::Function_MetaDataParams[] = {
		{ "Category", "AR Utilities" },
		{ "Comment", "/**\n\x09 * Update material texture parameter using pre-defined names:\n\x09 * Scene depth texture: SceneDepthTexture\n\x09 * Depth to meter scale: DepthToMeterScale\n\x09 */" },
		{ "CPP_Default_DepthToMeterScale", "1.000000" },
		{ "ModuleRelativePath", "Public/ARUtilitiesFunctionLibrary.h" },
		{ "ToolTip", "Update material texture parameter using pre-defined names:\nScene depth texture: SceneDepthTexture\nDepth to meter scale: DepthToMeterScale" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UARUtilitiesFunctionLibrary, nullptr, "UpdateSceneDepthTexture", nullptr, nullptr, sizeof(ARUtilitiesFunctionLibrary_eventUpdateSceneDepthTexture_Parms), Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics
	{
		struct ARUtilitiesFunctionLibrary_eventUpdateWorldToMeterScale_Parms
		{
			UMaterialInstanceDynamic* MaterialInstance;
			float WorldToMeterScale;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialInstance;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WorldToMeterScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::NewProp_MaterialInstance = { "MaterialInstance", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARUtilitiesFunctionLibrary_eventUpdateWorldToMeterScale_Parms, MaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::NewProp_WorldToMeterScale = { "WorldToMeterScale", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARUtilitiesFunctionLibrary_eventUpdateWorldToMeterScale_Parms, WorldToMeterScale), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::NewProp_MaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::NewProp_WorldToMeterScale,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::Function_MetaDataParams[] = {
		{ "Category", "AR Utilities" },
		{ "Comment", "/**\n\x09 * Update material texture parameter using pre-defined names:\n\x09 * World to meter scale: WorldToMeterScale\n\x09 */" },
		{ "CPP_Default_WorldToMeterScale", "100.000000" },
		{ "ModuleRelativePath", "Public/ARUtilitiesFunctionLibrary.h" },
		{ "ToolTip", "Update material texture parameter using pre-defined names:\nWorld to meter scale: WorldToMeterScale" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UARUtilitiesFunctionLibrary, nullptr, "UpdateWorldToMeterScale", nullptr, nullptr, sizeof(ARUtilitiesFunctionLibrary_eventUpdateWorldToMeterScale_Parms), Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UARUtilitiesFunctionLibrary_NoRegister()
	{
		return UARUtilitiesFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_ARUtilities,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateCameraTextureParam, "UpdateCameraTextureParam" }, // 3033389911
		{ &Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateSceneDepthTexture, "UpdateSceneDepthTexture" }, // 4203786737
		{ &Z_Construct_UFunction_UARUtilitiesFunctionLibrary_UpdateWorldToMeterScale, "UpdateWorldToMeterScale" }, // 3810461986
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ARUtilitiesFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/ARUtilitiesFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UARUtilitiesFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics::ClassParams = {
		&UARUtilitiesFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UARUtilitiesFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UARUtilitiesFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UARUtilitiesFunctionLibrary, 2016499151);
	template<> ARUTILITIES_API UClass* StaticClass<UARUtilitiesFunctionLibrary>()
	{
		return UARUtilitiesFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UARUtilitiesFunctionLibrary(Z_Construct_UClass_UARUtilitiesFunctionLibrary, &UARUtilitiesFunctionLibrary::StaticClass, TEXT("/Script/ARUtilities"), TEXT("UARUtilitiesFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UARUtilitiesFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
