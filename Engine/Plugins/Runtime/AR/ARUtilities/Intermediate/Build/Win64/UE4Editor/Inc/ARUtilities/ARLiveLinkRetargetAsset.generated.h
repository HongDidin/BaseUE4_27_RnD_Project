// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARUTILITIES_ARLiveLinkRetargetAsset_generated_h
#error "ARLiveLinkRetargetAsset.generated.h already included, missing '#pragma once' in ARLiveLinkRetargetAsset.h"
#endif
#define ARUTILITIES_ARLiveLinkRetargetAsset_generated_h

#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_23_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FARKitPoseTrackingConfig_Statics; \
	static class UScriptStruct* StaticStruct();


template<> ARUTILITIES_API UScriptStruct* StaticStruct<struct FARKitPoseTrackingConfig>();

#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUARLiveLinkRetargetAsset(); \
	friend struct Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics; \
public: \
	DECLARE_CLASS(UARLiveLinkRetargetAsset, ULiveLinkRetargetAsset, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/ARUtilities"), NO_API) \
	DECLARE_SERIALIZER(UARLiveLinkRetargetAsset)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUARLiveLinkRetargetAsset(); \
	friend struct Z_Construct_UClass_UARLiveLinkRetargetAsset_Statics; \
public: \
	DECLARE_CLASS(UARLiveLinkRetargetAsset, ULiveLinkRetargetAsset, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/ARUtilities"), NO_API) \
	DECLARE_SERIALIZER(UARLiveLinkRetargetAsset)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARLiveLinkRetargetAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARLiveLinkRetargetAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARLiveLinkRetargetAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARLiveLinkRetargetAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARLiveLinkRetargetAsset(UARLiveLinkRetargetAsset&&); \
	NO_API UARLiveLinkRetargetAsset(const UARLiveLinkRetargetAsset&); \
public:


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARLiveLinkRetargetAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARLiveLinkRetargetAsset(UARLiveLinkRetargetAsset&&); \
	NO_API UARLiveLinkRetargetAsset(const UARLiveLinkRetargetAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARLiveLinkRetargetAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARLiveLinkRetargetAsset); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARLiveLinkRetargetAsset)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_34_PROLOG
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_INCLASS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARUTILITIES_API UClass* StaticClass<class UARLiveLinkRetargetAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARLiveLinkRetargetAsset_h


#define FOREACH_ENUM_EARLIVELINKSOURCETYPE(op) \
	op(EARLiveLinkSourceType::None) \
	op(EARLiveLinkSourceType::ARKitPoseTracking) 

enum class EARLiveLinkSourceType : uint8;
template<> ARUTILITIES_API UEnum* StaticEnum<EARLiveLinkSourceType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
