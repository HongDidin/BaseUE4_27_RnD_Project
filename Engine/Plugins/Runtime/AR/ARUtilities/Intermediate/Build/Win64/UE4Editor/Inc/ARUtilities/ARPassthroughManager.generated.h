// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPassthroughMaterialUpdateComponent;
#ifdef ARUTILITIES_ARPassthroughManager_generated_h
#error "ARPassthroughManager.generated.h already included, missing '#pragma once' in ARPassthroughManager.h"
#endif
#define ARUTILITIES_ARPassthroughManager_generated_h

#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetPassthroughMaterialUpdateComponent);


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetPassthroughMaterialUpdateComponent);


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAARPassthroughManager(); \
	friend struct Z_Construct_UClass_AARPassthroughManager_Statics; \
public: \
	DECLARE_CLASS(AARPassthroughManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ARUtilities"), NO_API) \
	DECLARE_SERIALIZER(AARPassthroughManager)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAARPassthroughManager(); \
	friend struct Z_Construct_UClass_AARPassthroughManager_Statics; \
public: \
	DECLARE_CLASS(AARPassthroughManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ARUtilities"), NO_API) \
	DECLARE_SERIALIZER(AARPassthroughManager)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AARPassthroughManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AARPassthroughManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AARPassthroughManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AARPassthroughManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AARPassthroughManager(AARPassthroughManager&&); \
	NO_API AARPassthroughManager(const AARPassthroughManager&); \
public:


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AARPassthroughManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AARPassthroughManager(AARPassthroughManager&&); \
	NO_API AARPassthroughManager(const AARPassthroughManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AARPassthroughManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AARPassthroughManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AARPassthroughManager)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ARComponentClasses() { return STRUCT_OFFSET(AARPassthroughManager, ARComponentClasses); } \
	FORCEINLINE static uint32 __PPO__PassthroughUpdateComponent() { return STRUCT_OFFSET(AARPassthroughManager, PassthroughUpdateComponent); }


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_17_PROLOG
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_INCLASS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ARPassthroughManager."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARUTILITIES_API UClass* StaticClass<class AARPassthroughManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_ARPassthroughManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
