// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FLinearColor;
class UPrimitiveComponent;
#ifdef ARUTILITIES_PassthroughMaterialUpdateComponent_generated_h
#error "PassthroughMaterialUpdateComponent.generated.h already included, missing '#pragma once' in PassthroughMaterialUpdateComponent.h"
#endif
#define ARUTILITIES_PassthroughMaterialUpdateComponent_generated_h

#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetPassthroughDebugColor); \
	DECLARE_FUNCTION(execRemoveAffectedComponent); \
	DECLARE_FUNCTION(execAddAffectedComponent);


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetPassthroughDebugColor); \
	DECLARE_FUNCTION(execRemoveAffectedComponent); \
	DECLARE_FUNCTION(execAddAffectedComponent);


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPassthroughMaterialUpdateComponent(); \
	friend struct Z_Construct_UClass_UPassthroughMaterialUpdateComponent_Statics; \
public: \
	DECLARE_CLASS(UPassthroughMaterialUpdateComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ARUtilities"), NO_API) \
	DECLARE_SERIALIZER(UPassthroughMaterialUpdateComponent)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUPassthroughMaterialUpdateComponent(); \
	friend struct Z_Construct_UClass_UPassthroughMaterialUpdateComponent_Statics; \
public: \
	DECLARE_CLASS(UPassthroughMaterialUpdateComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ARUtilities"), NO_API) \
	DECLARE_SERIALIZER(UPassthroughMaterialUpdateComponent)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPassthroughMaterialUpdateComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPassthroughMaterialUpdateComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPassthroughMaterialUpdateComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPassthroughMaterialUpdateComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPassthroughMaterialUpdateComponent(UPassthroughMaterialUpdateComponent&&); \
	NO_API UPassthroughMaterialUpdateComponent(const UPassthroughMaterialUpdateComponent&); \
public:


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPassthroughMaterialUpdateComponent(UPassthroughMaterialUpdateComponent&&); \
	NO_API UPassthroughMaterialUpdateComponent(const UPassthroughMaterialUpdateComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPassthroughMaterialUpdateComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPassthroughMaterialUpdateComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPassthroughMaterialUpdateComponent)


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TextureType() { return STRUCT_OFFSET(UPassthroughMaterialUpdateComponent, TextureType); } \
	FORCEINLINE static uint32 __PPO__PassthroughMaterial() { return STRUCT_OFFSET(UPassthroughMaterialUpdateComponent, PassthroughMaterial); } \
	FORCEINLINE static uint32 __PPO__PassthroughMaterialExternalTexture() { return STRUCT_OFFSET(UPassthroughMaterialUpdateComponent, PassthroughMaterialExternalTexture); } \
	FORCEINLINE static uint32 __PPO__PassthroughDebugColor() { return STRUCT_OFFSET(UPassthroughMaterialUpdateComponent, PassthroughDebugColor); } \
	FORCEINLINE static uint32 __PPO__AffectedComponents() { return STRUCT_OFFSET(UPassthroughMaterialUpdateComponent, AffectedComponents); } \
	FORCEINLINE static uint32 __PPO__PendingComponents() { return STRUCT_OFFSET(UPassthroughMaterialUpdateComponent, PendingComponents); }


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_18_PROLOG
#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_INCLASS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARUTILITIES_API UClass* StaticClass<class UPassthroughMaterialUpdateComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_ARUtilities_Source_ARUtilities_Public_PassthroughMaterialUpdateComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
