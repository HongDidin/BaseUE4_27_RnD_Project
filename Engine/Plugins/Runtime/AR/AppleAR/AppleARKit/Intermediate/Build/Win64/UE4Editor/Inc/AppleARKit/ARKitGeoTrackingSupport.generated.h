// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APPLEARKIT_ARKitGeoTrackingSupport_generated_h
#error "ARKitGeoTrackingSupport.generated.h already included, missing '#pragma once' in ARKitGeoTrackingSupport.h"
#endif
#define APPLEARKIT_ARKitGeoTrackingSupport_generated_h

#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUARKitGeoTrackingSupport(); \
	friend struct Z_Construct_UClass_UARKitGeoTrackingSupport_Statics; \
public: \
	DECLARE_CLASS(UARKitGeoTrackingSupport, UARGeoTrackingSupport, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UARKitGeoTrackingSupport)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUARKitGeoTrackingSupport(); \
	friend struct Z_Construct_UClass_UARKitGeoTrackingSupport_Statics; \
public: \
	DECLARE_CLASS(UARKitGeoTrackingSupport, UARGeoTrackingSupport, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UARKitGeoTrackingSupport)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARKitGeoTrackingSupport(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARKitGeoTrackingSupport) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARKitGeoTrackingSupport); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARKitGeoTrackingSupport); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARKitGeoTrackingSupport(UARKitGeoTrackingSupport&&); \
	NO_API UARKitGeoTrackingSupport(const UARKitGeoTrackingSupport&); \
public:


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARKitGeoTrackingSupport(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARKitGeoTrackingSupport(UARKitGeoTrackingSupport&&); \
	NO_API UARKitGeoTrackingSupport(const UARKitGeoTrackingSupport&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARKitGeoTrackingSupport); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARKitGeoTrackingSupport); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARKitGeoTrackingSupport)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_10_PROLOG
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_INCLASS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEARKIT_API UClass* StaticClass<class UARKitGeoTrackingSupport>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ARKitGeoTrackingSupport_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
