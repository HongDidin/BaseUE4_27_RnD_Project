// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKit/Private/AppleARKitLightEstimate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleARKitLightEstimate() {}
// Cross Module References
	APPLEARKIT_API UScriptStruct* Z_Construct_UScriptStruct_FAppleARKitLightEstimate();
	UPackage* Z_Construct_UPackage__Script_AppleARKit();
// End Cross Module References
class UScriptStruct* FAppleARKitLightEstimate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APPLEARKIT_API uint32 Get_Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAppleARKitLightEstimate, Z_Construct_UPackage__Script_AppleARKit(), TEXT("AppleARKitLightEstimate"), sizeof(FAppleARKitLightEstimate), Get_Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Hash());
	}
	return Singleton;
}
template<> APPLEARKIT_API UScriptStruct* StaticStruct<FAppleARKitLightEstimate>()
{
	return FAppleARKitLightEstimate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAppleARKitLightEstimate(FAppleARKitLightEstimate::StaticStruct, TEXT("/Script/AppleARKit"), TEXT("AppleARKitLightEstimate"), false, nullptr, nullptr);
static struct FScriptStruct_AppleARKit_StaticRegisterNativesFAppleARKitLightEstimate
{
	FScriptStruct_AppleARKit_StaticRegisterNativesFAppleARKitLightEstimate()
	{
		UScriptStruct::DeferCppStructOps<FAppleARKitLightEstimate>(FName(TEXT("AppleARKitLightEstimate")));
	}
} ScriptStruct_AppleARKit_StaticRegisterNativesFAppleARKitLightEstimate;
	struct Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsValid_MetaData[];
#endif
		static void NewProp_bIsValid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsValid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AmbientIntensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AmbientIntensity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AmbientColorTemperatureKelvin_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AmbientColorTemperatureKelvin;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * A light estimate represented as spherical harmonics\n */" },
		{ "ModuleRelativePath", "Private/AppleARKitLightEstimate.h" },
		{ "ToolTip", "A light estimate represented as spherical harmonics" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAppleARKitLightEstimate>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_bIsValid_MetaData[] = {
		{ "Comment", "/** True if light estimation was enabled for the session and light estimation was successful */" },
		{ "ModuleRelativePath", "Private/AppleARKitLightEstimate.h" },
		{ "ToolTip", "True if light estimation was enabled for the session and light estimation was successful" },
	};
#endif
	void Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_bIsValid_SetBit(void* Obj)
	{
		((FAppleARKitLightEstimate*)Obj)->bIsValid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_bIsValid = { "bIsValid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FAppleARKitLightEstimate), &Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_bIsValid_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_bIsValid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_bIsValid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_AmbientIntensity_MetaData[] = {
		{ "Comment", "/**\n\x09 * Ambient intensity of the lighting.\n\x09 * \n\x09 * In a well lit environment, this value is close to 1000. It typically ranges from 0 \n\x09 * (very dark) to around 2000 (very bright).\n\x09 */" },
		{ "ModuleRelativePath", "Private/AppleARKitLightEstimate.h" },
		{ "ToolTip", "Ambient intensity of the lighting.\n\nIn a well lit environment, this value is close to 1000. It typically ranges from 0\n(very dark) to around 2000 (very bright)." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_AmbientIntensity = { "AmbientIntensity", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAppleARKitLightEstimate, AmbientIntensity), METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_AmbientIntensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_AmbientIntensity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_AmbientColorTemperatureKelvin_MetaData[] = {
		{ "Comment", "/**\n\x09 * Color Temperature in Kelvin of light\n\x09 *\n\x09 */" },
		{ "ModuleRelativePath", "Private/AppleARKitLightEstimate.h" },
		{ "ToolTip", "Color Temperature in Kelvin of light" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_AmbientColorTemperatureKelvin = { "AmbientColorTemperatureKelvin", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAppleARKitLightEstimate, AmbientColorTemperatureKelvin), METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_AmbientColorTemperatureKelvin_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_AmbientColorTemperatureKelvin_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_bIsValid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_AmbientIntensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::NewProp_AmbientColorTemperatureKelvin,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
		nullptr,
		&NewStructOps,
		"AppleARKitLightEstimate",
		sizeof(FAppleARKitLightEstimate),
		alignof(FAppleARKitLightEstimate),
		Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAppleARKitLightEstimate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AppleARKit();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AppleARKitLightEstimate"), sizeof(FAppleARKitLightEstimate), Get_Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Hash() { return 3679875382U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
