// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APPLEARKIT_ExternalTextureMaterialExpression_generated_h
#error "ExternalTextureMaterialExpression.generated.h already included, missing '#pragma once' in ExternalTextureMaterialExpression.h"
#endif
#define APPLEARKIT_ExternalTextureMaterialExpression_generated_h

#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_MaterialExpressionARKitPassthroughCamera(); \
	friend struct Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_MaterialExpressionARKitPassthroughCamera, UMaterialExpression, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_MaterialExpressionARKitPassthroughCamera)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_MaterialExpressionARKitPassthroughCamera(); \
	friend struct Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_MaterialExpressionARKitPassthroughCamera, UMaterialExpression, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_MaterialExpressionARKitPassthroughCamera)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_MaterialExpressionARKitPassthroughCamera(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_MaterialExpressionARKitPassthroughCamera) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_MaterialExpressionARKitPassthroughCamera); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_MaterialExpressionARKitPassthroughCamera); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_MaterialExpressionARKitPassthroughCamera(UDEPRECATED_MaterialExpressionARKitPassthroughCamera&&); \
	NO_API UDEPRECATED_MaterialExpressionARKitPassthroughCamera(const UDEPRECATED_MaterialExpressionARKitPassthroughCamera&); \
public:


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_MaterialExpressionARKitPassthroughCamera(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_MaterialExpressionARKitPassthroughCamera(UDEPRECATED_MaterialExpressionARKitPassthroughCamera&&); \
	NO_API UDEPRECATED_MaterialExpressionARKitPassthroughCamera(const UDEPRECATED_MaterialExpressionARKitPassthroughCamera&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_MaterialExpressionARKitPassthroughCamera); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_MaterialExpressionARKitPassthroughCamera); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_MaterialExpressionARKitPassthroughCamera)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_20_PROLOG
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_INCLASS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h_23_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MaterialExpressionARKitPassthroughCamera."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEARKIT_API UClass* StaticClass<class UDEPRECATED_MaterialExpressionARKitPassthroughCamera>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_ExternalTextureMaterialExpression_h


#define FOREACH_ENUM_EARKITTEXTURETYPE(op) \
	op(TextureY) \
	op(TextureCbCr) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
