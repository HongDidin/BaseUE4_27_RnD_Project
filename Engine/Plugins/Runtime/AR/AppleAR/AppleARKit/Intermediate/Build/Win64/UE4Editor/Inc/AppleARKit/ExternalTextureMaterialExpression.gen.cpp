// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKit/Private/ExternalTextureMaterialExpression.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExternalTextureMaterialExpression() {}
// Cross Module References
	APPLEARKIT_API UEnum* Z_Construct_UEnum_AppleARKit_EARKitTextureType();
	UPackage* Z_Construct_UPackage__Script_AppleARKit();
	APPLEARKIT_API UClass* Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialExpression();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FExpressionInput();
// End Cross Module References
	static UEnum* EARKitTextureType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AppleARKit_EARKitTextureType, Z_Construct_UPackage__Script_AppleARKit(), TEXT("EARKitTextureType"));
		}
		return Singleton;
	}
	template<> APPLEARKIT_API UEnum* StaticEnum<EARKitTextureType>()
	{
		return EARKitTextureType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EARKitTextureType(EARKitTextureType_StaticEnum, TEXT("/Script/AppleARKit"), TEXT("EARKitTextureType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AppleARKit_EARKitTextureType_Hash() { return 2166380789U; }
	UEnum* Z_Construct_UEnum_AppleARKit_EARKitTextureType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AppleARKit();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EARKitTextureType"), 0, Get_Z_Construct_UEnum_AppleARKit_EARKitTextureType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "TextureY", (int64)TextureY },
				{ "TextureCbCr", (int64)TextureCbCr },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Private/ExternalTextureMaterialExpression.h" },
				{ "TextureCbCr.Name", "TextureCbCr" },
				{ "TextureY.Name", "TextureY" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AppleARKit,
				nullptr,
				"EARKitTextureType",
				"EARKitTextureType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Regular,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDEPRECATED_MaterialExpressionARKitPassthroughCamera::StaticRegisterNativesUDEPRECATED_MaterialExpressionARKitPassthroughCamera()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_NoRegister()
	{
		return UDEPRECATED_MaterialExpressionARKitPassthroughCamera::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Coordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Coordinates;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConstCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ConstCoordinate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureType_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TextureType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMaterialExpression,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Implements a node sampling from the ARKit Passthrough external textures.\n*/" },
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "ExternalTextureMaterialExpression.h" },
		{ "ModuleRelativePath", "Private/ExternalTextureMaterialExpression.h" },
		{ "ToolTip", "Implements a node sampling from the ARKit Passthrough external textures." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_Coordinates_MetaData[] = {
		{ "ModuleRelativePath", "Private/ExternalTextureMaterialExpression.h" },
		{ "RequiredInput", "false" },
		{ "ToolTip", "Defaults to 'ConstCoordinate' if not specified" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_Coordinates = { "Coordinates", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_MaterialExpressionARKitPassthroughCamera, Coordinates), Z_Construct_UScriptStruct_FExpressionInput, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_Coordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_Coordinates_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_ConstCoordinate_MetaData[] = {
		{ "Category", "UMaterialExpressionARKitPassthroughCamera" },
		{ "Comment", "/** Only used if Coordinates is not hooked up */" },
		{ "ModuleRelativePath", "Private/ExternalTextureMaterialExpression.h" },
		{ "ToolTip", "Only used if Coordinates is not hooked up" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_ConstCoordinate = { "ConstCoordinate", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_MaterialExpressionARKitPassthroughCamera, ConstCoordinate), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_ConstCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_ConstCoordinate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_TextureType_MetaData[] = {
		{ "Category", "UMaterialExpressionARKitPassthroughCamera" },
		{ "ModuleRelativePath", "Private/ExternalTextureMaterialExpression.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_TextureType = { "TextureType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_MaterialExpressionARKitPassthroughCamera, TextureType), Z_Construct_UEnum_AppleARKit_EARKitTextureType, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_TextureType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_TextureType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_Coordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_ConstCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::NewProp_TextureType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_MaterialExpressionARKitPassthroughCamera>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::ClassParams = {
		&UDEPRECATED_MaterialExpressionARKitPassthroughCamera::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::PropPointers),
		0,
		0x020022A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_MaterialExpressionARKitPassthroughCamera, 740531993);
	template<> APPLEARKIT_API UClass* StaticClass<UDEPRECATED_MaterialExpressionARKitPassthroughCamera>()
	{
		return UDEPRECATED_MaterialExpressionARKitPassthroughCamera::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera(Z_Construct_UClass_UDEPRECATED_MaterialExpressionARKitPassthroughCamera, &UDEPRECATED_MaterialExpressionARKitPassthroughCamera::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UDEPRECATED_MaterialExpressionARKitPassthroughCamera"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_MaterialExpressionARKitPassthroughCamera);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
