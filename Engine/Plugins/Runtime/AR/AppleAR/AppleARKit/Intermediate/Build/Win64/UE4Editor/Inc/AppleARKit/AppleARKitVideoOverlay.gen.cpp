// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKit/Private/AppleARKitVideoOverlay.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleARKitVideoOverlay() {}
// Cross Module References
	APPLEARKIT_API UClass* Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UARKitCameraOverlayMaterialLoader();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_AppleARKit();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void UARKitCameraOverlayMaterialLoader::StaticRegisterNativesUARKitCameraOverlayMaterialLoader()
	{
	}
	UClass* Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_NoRegister()
	{
		return UARKitCameraOverlayMaterialLoader::StaticClass();
	}
	struct Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultCameraOverlayMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultCameraOverlayMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DepthOcclusionOverlayMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DepthOcclusionOverlayMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MatteOcclusionOverlayMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MatteOcclusionOverlayMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneDepthOcclusionMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneDepthOcclusionMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneDepthColorationMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneDepthColorationMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Helper class to ensure the ARKit camera material is cooked. */" },
		{ "IncludePath", "AppleARKitVideoOverlay.h" },
		{ "ModuleRelativePath", "Private/AppleARKitVideoOverlay.h" },
		{ "ToolTip", "Helper class to ensure the ARKit camera material is cooked." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_DefaultCameraOverlayMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Private/AppleARKitVideoOverlay.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_DefaultCameraOverlayMaterial = { "DefaultCameraOverlayMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UARKitCameraOverlayMaterialLoader, DefaultCameraOverlayMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_DefaultCameraOverlayMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_DefaultCameraOverlayMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_DepthOcclusionOverlayMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Private/AppleARKitVideoOverlay.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_DepthOcclusionOverlayMaterial = { "DepthOcclusionOverlayMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UARKitCameraOverlayMaterialLoader, DepthOcclusionOverlayMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_DepthOcclusionOverlayMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_DepthOcclusionOverlayMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_MatteOcclusionOverlayMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Private/AppleARKitVideoOverlay.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_MatteOcclusionOverlayMaterial = { "MatteOcclusionOverlayMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UARKitCameraOverlayMaterialLoader, MatteOcclusionOverlayMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_MatteOcclusionOverlayMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_MatteOcclusionOverlayMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_SceneDepthOcclusionMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Private/AppleARKitVideoOverlay.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_SceneDepthOcclusionMaterial = { "SceneDepthOcclusionMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UARKitCameraOverlayMaterialLoader, SceneDepthOcclusionMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_SceneDepthOcclusionMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_SceneDepthOcclusionMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_SceneDepthColorationMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Private/AppleARKitVideoOverlay.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_SceneDepthColorationMaterial = { "SceneDepthColorationMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UARKitCameraOverlayMaterialLoader, SceneDepthColorationMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_SceneDepthColorationMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_SceneDepthColorationMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_DefaultCameraOverlayMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_DepthOcclusionOverlayMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_MatteOcclusionOverlayMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_SceneDepthOcclusionMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::NewProp_SceneDepthColorationMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UARKitCameraOverlayMaterialLoader>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::ClassParams = {
		&UARKitCameraOverlayMaterialLoader::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UARKitCameraOverlayMaterialLoader()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UARKitCameraOverlayMaterialLoader, 3337821315);
	template<> APPLEARKIT_API UClass* StaticClass<UARKitCameraOverlayMaterialLoader>()
	{
		return UARKitCameraOverlayMaterialLoader::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UARKitCameraOverlayMaterialLoader(Z_Construct_UClass_UARKitCameraOverlayMaterialLoader, &UARKitCameraOverlayMaterialLoader::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UARKitCameraOverlayMaterialLoader"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UARKitCameraOverlayMaterialLoader);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
