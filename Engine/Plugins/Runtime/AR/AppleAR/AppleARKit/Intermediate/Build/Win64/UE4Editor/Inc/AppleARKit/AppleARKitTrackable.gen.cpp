// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKit/Private/AppleARKitTrackable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleARKitTrackable() {}
// Cross Module References
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UAREnvironmentCaptureProbe();
	UPackage* Z_Construct_UPackage__Script_AppleARKit();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_NoRegister();
// End Cross Module References
	void UAppleARKitEnvironmentCaptureProbe::StaticRegisterNativesUAppleARKitEnvironmentCaptureProbe()
	{
	}
	UClass* Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_NoRegister()
	{
		return UAppleARKitEnvironmentCaptureProbe::StaticClass();
	}
	struct Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ARKitEnvironmentTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ARKitEnvironmentTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAREnvironmentCaptureProbe,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleARKitTrackable.h" },
		{ "ModuleRelativePath", "Private/AppleARKitTrackable.h" },
		{ "NotBlueprintType", "true" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::NewProp_ARKitEnvironmentTexture_MetaData[] = {
		{ "Comment", "/** The cube map of the reflected environment */" },
		{ "ModuleRelativePath", "Private/AppleARKitTrackable.h" },
		{ "ToolTip", "The cube map of the reflected environment" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::NewProp_ARKitEnvironmentTexture = { "ARKitEnvironmentTexture", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAppleARKitEnvironmentCaptureProbe, ARKitEnvironmentTexture), Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::NewProp_ARKitEnvironmentTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::NewProp_ARKitEnvironmentTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::NewProp_ARKitEnvironmentTexture,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAppleARKitEnvironmentCaptureProbe>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::ClassParams = {
		&UAppleARKitEnvironmentCaptureProbe::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAppleARKitEnvironmentCaptureProbe, 1228481585);
	template<> APPLEARKIT_API UClass* StaticClass<UAppleARKitEnvironmentCaptureProbe>()
	{
		return UAppleARKitEnvironmentCaptureProbe::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAppleARKitEnvironmentCaptureProbe(Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbe, &UAppleARKitEnvironmentCaptureProbe::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UAppleARKitEnvironmentCaptureProbe"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAppleARKitEnvironmentCaptureProbe);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
