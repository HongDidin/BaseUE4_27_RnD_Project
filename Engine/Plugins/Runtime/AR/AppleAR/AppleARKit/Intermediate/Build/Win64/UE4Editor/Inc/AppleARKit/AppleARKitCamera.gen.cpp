// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKit/Private/AppleARKitCamera.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleARKitCamera() {}
// Cross Module References
	APPLEARKIT_API UScriptStruct* Z_Construct_UScriptStruct_FAppleARKitCamera();
	UPackage* Z_Construct_UPackage__Script_AppleARKit();
	AUGMENTEDREALITY_API UEnum* Z_Construct_UEnum_AugmentedReality_EARTrackingQuality();
	AUGMENTEDREALITY_API UEnum* Z_Construct_UEnum_AugmentedReality_EARTrackingQualityReason();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
class UScriptStruct* FAppleARKitCamera::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APPLEARKIT_API uint32 Get_Z_Construct_UScriptStruct_FAppleARKitCamera_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAppleARKitCamera, Z_Construct_UPackage__Script_AppleARKit(), TEXT("AppleARKitCamera"), sizeof(FAppleARKitCamera), Get_Z_Construct_UScriptStruct_FAppleARKitCamera_Hash());
	}
	return Singleton;
}
template<> APPLEARKIT_API UScriptStruct* StaticStruct<FAppleARKitCamera>()
{
	return FAppleARKitCamera::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAppleARKitCamera(FAppleARKitCamera::StaticStruct, TEXT("/Script/AppleARKit"), TEXT("AppleARKitCamera"), false, nullptr, nullptr);
static struct FScriptStruct_AppleARKit_StaticRegisterNativesFAppleARKitCamera
{
	FScriptStruct_AppleARKit_StaticRegisterNativesFAppleARKitCamera()
	{
		UScriptStruct::DeferCppStructOps<FAppleARKitCamera>(FName(TEXT("AppleARKitCamera")));
	}
} ScriptStruct_AppleARKit_StaticRegisterNativesFAppleARKitCamera;
	struct Z_Construct_UScriptStruct_FAppleARKitCamera_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TrackingQuality_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingQuality_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TrackingQuality;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TrackingQualityReason_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingQualityReason_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TrackingQualityReason;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Orientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Orientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Translation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Translation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImageResolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocalLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrincipalPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrincipalPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::Struct_MetaDataParams[] = {
		{ "Category", "AppleARKit" },
		{ "Comment", "/**\n * A model representing the camera and its properties at a single point in time.\n */" },
		{ "ModuleRelativePath", "Private/AppleARKitCamera.h" },
		{ "ToolTip", "A model representing the camera and its properties at a single point in time." },
	};
#endif
	void* Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAppleARKitCamera>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQuality_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQuality_MetaData[] = {
		{ "Comment", "/**\n\x09 * The tracking quality of the camera.\n\x09 */" },
		{ "ModuleRelativePath", "Private/AppleARKitCamera.h" },
		{ "ToolTip", "The tracking quality of the camera." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQuality = { "TrackingQuality", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAppleARKitCamera, TrackingQuality), Z_Construct_UEnum_AugmentedReality_EARTrackingQuality, METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQuality_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQuality_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQualityReason_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQualityReason_MetaData[] = {
		{ "Comment", "/**\n\x09 * The reason for the current tracking quality of the camera.\n\x09 */" },
		{ "ModuleRelativePath", "Private/AppleARKitCamera.h" },
		{ "ToolTip", "The reason for the current tracking quality of the camera." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQualityReason = { "TrackingQualityReason", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAppleARKitCamera, TrackingQualityReason), Z_Construct_UEnum_AugmentedReality_EARTrackingQualityReason, METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQualityReason_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQualityReason_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Transform_MetaData[] = {
		{ "Comment", "/**\n\x09 * The transformation matrix that defines the camera's rotation and translation in world coordinates.\n\x09 */" },
		{ "ModuleRelativePath", "Private/AppleARKitCamera.h" },
		{ "ToolTip", "The transformation matrix that defines the camera's rotation and translation in world coordinates." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAppleARKitCamera, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Orientation_MetaData[] = {
		{ "Comment", "/* Raw orientation of the camera */" },
		{ "ModuleRelativePath", "Private/AppleARKitCamera.h" },
		{ "ToolTip", "Raw orientation of the camera" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Orientation = { "Orientation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAppleARKitCamera, Orientation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Orientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Orientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Translation_MetaData[] = {
		{ "Comment", "/* Raw position of the camera */" },
		{ "ModuleRelativePath", "Private/AppleARKitCamera.h" },
		{ "ToolTip", "Raw position of the camera" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Translation = { "Translation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAppleARKitCamera, Translation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Translation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Translation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_ImageResolution_MetaData[] = {
		{ "Comment", "/**\n\x09 * Camera image resolution in pixels\n\x09 */" },
		{ "ModuleRelativePath", "Private/AppleARKitCamera.h" },
		{ "ToolTip", "Camera image resolution in pixels" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_ImageResolution = { "ImageResolution", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAppleARKitCamera, ImageResolution), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_ImageResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_ImageResolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_FocalLength_MetaData[] = {
		{ "Comment", "/**\n\x09 * Camera focal length in pixels\n\x09 */" },
		{ "ModuleRelativePath", "Private/AppleARKitCamera.h" },
		{ "ToolTip", "Camera focal length in pixels" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_FocalLength = { "FocalLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAppleARKitCamera, FocalLength), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_FocalLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_FocalLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_PrincipalPoint_MetaData[] = {
		{ "Comment", "/**\n\x09 * Camera principal point in pixels\n\x09 */" },
		{ "ModuleRelativePath", "Private/AppleARKitCamera.h" },
		{ "ToolTip", "Camera principal point in pixels" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_PrincipalPoint = { "PrincipalPoint", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAppleARKitCamera, PrincipalPoint), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_PrincipalPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_PrincipalPoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQuality_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQuality,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQualityReason_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_TrackingQualityReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Orientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_Translation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_ImageResolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_FocalLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::NewProp_PrincipalPoint,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
		nullptr,
		&NewStructOps,
		"AppleARKitCamera",
		sizeof(FAppleARKitCamera),
		alignof(FAppleARKitCamera),
		Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAppleARKitCamera()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAppleARKitCamera_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AppleARKit();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AppleARKitCamera"), sizeof(FAppleARKitCamera), Get_Z_Construct_UScriptStruct_FAppleARKitCamera_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAppleARKitCamera_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAppleARKitCamera_Hash() { return 279578519U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
