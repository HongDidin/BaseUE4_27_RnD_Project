// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKit/Private/AppleARKitTimecodeProvider.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleARKitTimecodeProvider() {}
// Cross Module References
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitTimecodeProvider_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitTimecodeProvider();
	ENGINE_API UClass* Z_Construct_UClass_UTimecodeProvider();
	UPackage* Z_Construct_UPackage__Script_AppleARKit();
// End Cross Module References
	void UAppleARKitTimecodeProvider::StaticRegisterNativesUAppleARKitTimecodeProvider()
	{
	}
	UClass* Z_Construct_UClass_UAppleARKitTimecodeProvider_NoRegister()
	{
		return UAppleARKitTimecodeProvider::StaticClass();
	}
	struct Z_Construct_UClass_UAppleARKitTimecodeProvider_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAppleARKitTimecodeProvider_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTimecodeProvider,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleARKitTimecodeProvider_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * This class is an implementation of the ITimecodeProvider and is used to abstract\n * out the calculation of the frame & time for an update\n */" },
		{ "IncludePath", "AppleARKitTimecodeProvider.h" },
		{ "ModuleRelativePath", "Private/AppleARKitTimecodeProvider.h" },
		{ "ToolTip", "This class is an implementation of the ITimecodeProvider and is used to abstract\nout the calculation of the frame & time for an update" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAppleARKitTimecodeProvider_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAppleARKitTimecodeProvider>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAppleARKitTimecodeProvider_Statics::ClassParams = {
		&UAppleARKitTimecodeProvider::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAppleARKitTimecodeProvider_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleARKitTimecodeProvider_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAppleARKitTimecodeProvider()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAppleARKitTimecodeProvider_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAppleARKitTimecodeProvider, 3507031968);
	template<> APPLEARKIT_API UClass* StaticClass<UAppleARKitTimecodeProvider>()
	{
		return UAppleARKitTimecodeProvider::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAppleARKitTimecodeProvider(Z_Construct_UClass_UAppleARKitTimecodeProvider, &UAppleARKitTimecodeProvider::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UAppleARKitTimecodeProvider"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAppleARKitTimecodeProvider);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
