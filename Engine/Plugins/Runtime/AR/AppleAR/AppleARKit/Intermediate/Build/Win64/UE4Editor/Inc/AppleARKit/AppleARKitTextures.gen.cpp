// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKit/Public/AppleARKitTextures.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleARKitTextures() {}
// Cross Module References
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitTextureCameraImage_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitTextureCameraImage();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UARTextureCameraImage();
	UPackage* Z_Construct_UPackage__Script_AppleARKit();
	APPLEIMAGEUTILS_API UClass* Z_Construct_UClass_UAppleImageInterface_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitTextureCameraDepth_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitTextureCameraDepth();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UARTextureCameraDepth();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UAREnvironmentCaptureProbeTexture();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitOcclusionTexture_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitOcclusionTexture();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UARTexture();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitCameraVideoTexture_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UAppleARKitCameraVideoTexture();
// End Cross Module References
	void UAppleARKitTextureCameraImage::StaticRegisterNativesUAppleARKitTextureCameraImage()
	{
	}
	UClass* Z_Construct_UClass_UAppleARKitTextureCameraImage_NoRegister()
	{
		return UAppleARKitTextureCameraImage::StaticClass();
	}
	struct Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UARTextureCameraImage,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleARKitTextures.h" },
		{ "ModuleRelativePath", "Public/AppleARKitTextures.h" },
		{ "NotBlueprintType", "true" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UAppleImageInterface_NoRegister, (int32)VTABLE_OFFSET(UAppleARKitTextureCameraImage, IAppleImageInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAppleARKitTextureCameraImage>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics::ClassParams = {
		&UAppleARKitTextureCameraImage::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAppleARKitTextureCameraImage()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAppleARKitTextureCameraImage, 889270311);
	template<> APPLEARKIT_API UClass* StaticClass<UAppleARKitTextureCameraImage>()
	{
		return UAppleARKitTextureCameraImage::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAppleARKitTextureCameraImage(Z_Construct_UClass_UAppleARKitTextureCameraImage, &UAppleARKitTextureCameraImage::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UAppleARKitTextureCameraImage"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAppleARKitTextureCameraImage);
	void UAppleARKitTextureCameraDepth::StaticRegisterNativesUAppleARKitTextureCameraDepth()
	{
	}
	UClass* Z_Construct_UClass_UAppleARKitTextureCameraDepth_NoRegister()
	{
		return UAppleARKitTextureCameraDepth::StaticClass();
	}
	struct Z_Construct_UClass_UAppleARKitTextureCameraDepth_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAppleARKitTextureCameraDepth_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UARTextureCameraDepth,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleARKitTextureCameraDepth_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleARKitTextures.h" },
		{ "ModuleRelativePath", "Public/AppleARKitTextures.h" },
		{ "NotBlueprintType", "true" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAppleARKitTextureCameraDepth_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAppleARKitTextureCameraDepth>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAppleARKitTextureCameraDepth_Statics::ClassParams = {
		&UAppleARKitTextureCameraDepth::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAppleARKitTextureCameraDepth_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleARKitTextureCameraDepth_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAppleARKitTextureCameraDepth()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAppleARKitTextureCameraDepth_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAppleARKitTextureCameraDepth, 2557100348);
	template<> APPLEARKIT_API UClass* StaticClass<UAppleARKitTextureCameraDepth>()
	{
		return UAppleARKitTextureCameraDepth::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAppleARKitTextureCameraDepth(Z_Construct_UClass_UAppleARKitTextureCameraDepth, &UAppleARKitTextureCameraDepth::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UAppleARKitTextureCameraDepth"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAppleARKitTextureCameraDepth);
	void UAppleARKitEnvironmentCaptureProbeTexture::StaticRegisterNativesUAppleARKitEnvironmentCaptureProbeTexture()
	{
	}
	UClass* Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_NoRegister()
	{
		return UAppleARKitEnvironmentCaptureProbeTexture::StaticClass();
	}
	struct Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAREnvironmentCaptureProbeTexture,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "AppleARKitTextures.h" },
		{ "ModuleRelativePath", "Public/AppleARKitTextures.h" },
		{ "NotBlueprintType", "true" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UAppleImageInterface_NoRegister, (int32)VTABLE_OFFSET(UAppleARKitEnvironmentCaptureProbeTexture, IAppleImageInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAppleARKitEnvironmentCaptureProbeTexture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics::ClassParams = {
		&UAppleARKitEnvironmentCaptureProbeTexture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAppleARKitEnvironmentCaptureProbeTexture, 540262549);
	template<> APPLEARKIT_API UClass* StaticClass<UAppleARKitEnvironmentCaptureProbeTexture>()
	{
		return UAppleARKitEnvironmentCaptureProbeTexture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAppleARKitEnvironmentCaptureProbeTexture(Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture, &UAppleARKitEnvironmentCaptureProbeTexture::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UAppleARKitEnvironmentCaptureProbeTexture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAppleARKitEnvironmentCaptureProbeTexture);
	void UAppleARKitOcclusionTexture::StaticRegisterNativesUAppleARKitOcclusionTexture()
	{
	}
	UClass* Z_Construct_UClass_UAppleARKitOcclusionTexture_NoRegister()
	{
		return UAppleARKitOcclusionTexture::StaticClass();
	}
	struct Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UARTexture,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleARKitTextures.h" },
		{ "ModuleRelativePath", "Public/AppleARKitTextures.h" },
		{ "NotBlueprintType", "true" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UAppleImageInterface_NoRegister, (int32)VTABLE_OFFSET(UAppleARKitOcclusionTexture, IAppleImageInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAppleARKitOcclusionTexture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics::ClassParams = {
		&UAppleARKitOcclusionTexture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAppleARKitOcclusionTexture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAppleARKitOcclusionTexture, 2455487511);
	template<> APPLEARKIT_API UClass* StaticClass<UAppleARKitOcclusionTexture>()
	{
		return UAppleARKitOcclusionTexture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAppleARKitOcclusionTexture(Z_Construct_UClass_UAppleARKitOcclusionTexture, &UAppleARKitOcclusionTexture::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UAppleARKitOcclusionTexture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAppleARKitOcclusionTexture);
	void UAppleARKitCameraVideoTexture::StaticRegisterNativesUAppleARKitCameraVideoTexture()
	{
	}
	UClass* Z_Construct_UClass_UAppleARKitCameraVideoTexture_NoRegister()
	{
		return UAppleARKitCameraVideoTexture::StaticClass();
	}
	struct Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UARTextureCameraImage,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleARKitTextures.h" },
		{ "ModuleRelativePath", "Public/AppleARKitTextures.h" },
		{ "NotBlueprintType", "true" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UAppleImageInterface_NoRegister, (int32)VTABLE_OFFSET(UAppleARKitCameraVideoTexture, IAppleImageInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAppleARKitCameraVideoTexture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics::ClassParams = {
		&UAppleARKitCameraVideoTexture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAppleARKitCameraVideoTexture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAppleARKitCameraVideoTexture, 2320067229);
	template<> APPLEARKIT_API UClass* StaticClass<UAppleARKitCameraVideoTexture>()
	{
		return UAppleARKitCameraVideoTexture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAppleARKitCameraVideoTexture(Z_Construct_UClass_UAppleARKitCameraVideoTexture, &UAppleARKitCameraVideoTexture::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UAppleARKitCameraVideoTexture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAppleARKitCameraVideoTexture);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
