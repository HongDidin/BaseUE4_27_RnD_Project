// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKit/Private/ARKitGeoTrackingSupport.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeARKitGeoTrackingSupport() {}
// Cross Module References
	APPLEARKIT_API UClass* Z_Construct_UClass_UARKitGeoTrackingSupport_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UARKitGeoTrackingSupport();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UARGeoTrackingSupport();
	UPackage* Z_Construct_UPackage__Script_AppleARKit();
// End Cross Module References
	void UARKitGeoTrackingSupport::StaticRegisterNativesUARKitGeoTrackingSupport()
	{
	}
	UClass* Z_Construct_UClass_UARKitGeoTrackingSupport_NoRegister()
	{
		return UARKitGeoTrackingSupport::StaticClass();
	}
	struct Z_Construct_UClass_UARKitGeoTrackingSupport_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UARKitGeoTrackingSupport_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UARGeoTrackingSupport,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARKitGeoTrackingSupport_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ARKitGeoTrackingSupport.h" },
		{ "ModuleRelativePath", "Private/ARKitGeoTrackingSupport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UARKitGeoTrackingSupport_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UARKitGeoTrackingSupport>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UARKitGeoTrackingSupport_Statics::ClassParams = {
		&UARKitGeoTrackingSupport::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UARKitGeoTrackingSupport_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UARKitGeoTrackingSupport_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UARKitGeoTrackingSupport()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UARKitGeoTrackingSupport_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UARKitGeoTrackingSupport, 802325388);
	template<> APPLEARKIT_API UClass* StaticClass<UARKitGeoTrackingSupport>()
	{
		return UARKitGeoTrackingSupport::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UARKitGeoTrackingSupport(Z_Construct_UClass_UARKitGeoTrackingSupport, &UARKitGeoTrackingSupport::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UARKitGeoTrackingSupport"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UARKitGeoTrackingSupport);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
