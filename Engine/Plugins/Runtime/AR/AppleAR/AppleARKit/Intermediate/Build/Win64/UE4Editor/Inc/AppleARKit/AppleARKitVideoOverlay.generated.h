// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APPLEARKIT_AppleARKitVideoOverlay_generated_h
#error "AppleARKitVideoOverlay.generated.h already included, missing '#pragma once' in AppleARKitVideoOverlay.h"
#endif
#define APPLEARKIT_AppleARKitVideoOverlay_generated_h

#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUARKitCameraOverlayMaterialLoader(); \
	friend struct Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics; \
public: \
	DECLARE_CLASS(UARKitCameraOverlayMaterialLoader, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UARKitCameraOverlayMaterialLoader)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUARKitCameraOverlayMaterialLoader(); \
	friend struct Z_Construct_UClass_UARKitCameraOverlayMaterialLoader_Statics; \
public: \
	DECLARE_CLASS(UARKitCameraOverlayMaterialLoader, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UARKitCameraOverlayMaterialLoader)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARKitCameraOverlayMaterialLoader(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARKitCameraOverlayMaterialLoader) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARKitCameraOverlayMaterialLoader); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARKitCameraOverlayMaterialLoader); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARKitCameraOverlayMaterialLoader(UARKitCameraOverlayMaterialLoader&&); \
	NO_API UARKitCameraOverlayMaterialLoader(const UARKitCameraOverlayMaterialLoader&); \
public:


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARKitCameraOverlayMaterialLoader(UARKitCameraOverlayMaterialLoader&&); \
	NO_API UARKitCameraOverlayMaterialLoader(const UARKitCameraOverlayMaterialLoader&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARKitCameraOverlayMaterialLoader); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARKitCameraOverlayMaterialLoader); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UARKitCameraOverlayMaterialLoader)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_28_PROLOG
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_INCLASS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEARKIT_API UClass* StaticClass<class UARKitCameraOverlayMaterialLoader>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitVideoOverlay_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
