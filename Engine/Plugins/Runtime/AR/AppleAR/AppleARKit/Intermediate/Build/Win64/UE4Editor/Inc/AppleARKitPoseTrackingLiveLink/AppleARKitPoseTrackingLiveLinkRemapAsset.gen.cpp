// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKitPoseTrackingLiveLink/Private/AppleARKitPoseTrackingLiveLinkRemapAsset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleARKitPoseTrackingLiveLinkRemapAsset() {}
// Cross Module References
	APPLEARKITPOSETRACKINGLIVELINK_API UClass* Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_NoRegister();
	APPLEARKITPOSETRACKINGLIVELINK_API UClass* Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset();
	LIVELINK_API UClass* Z_Construct_UClass_ULiveLinkRetargetAsset();
	UPackage* Z_Construct_UPackage__Script_AppleARKitPoseTrackingLiveLink();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset::StaticRegisterNativesUDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_NoRegister()
	{
		return UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppleARKitHumanForward_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AppleARKitHumanForward;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshForward_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeshForward;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_AppleARKitBoneNamesToMeshBoneNames_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_AppleARKitBoneNamesToMeshBoneNames_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppleARKitBoneNamesToMeshBoneNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_AppleARKitBoneNamesToMeshBoneNames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkRetargetAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKitPoseTrackingLiveLink,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DeprecationMessage", "This class is deprecated. Please use \"ARLiveLinkRetargetAsset\" instead." },
		{ "IncludePath", "AppleARKitPoseTrackingLiveLinkRemapAsset.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/AppleARKitPoseTrackingLiveLinkRemapAsset.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitHumanForward_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "ModuleRelativePath", "Private/AppleARKitPoseTrackingLiveLinkRemapAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitHumanForward = { "AppleARKitHumanForward", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset, AppleARKitHumanForward), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitHumanForward_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitHumanForward_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_MeshForward_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "ModuleRelativePath", "Private/AppleARKitPoseTrackingLiveLinkRemapAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_MeshForward = { "MeshForward", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset, MeshForward), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_MeshForward_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_MeshForward_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitBoneNamesToMeshBoneNames_ValueProp = { "AppleARKitBoneNamesToMeshBoneNames", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitBoneNamesToMeshBoneNames_Key_KeyProp = { "AppleARKitBoneNamesToMeshBoneNames_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitBoneNamesToMeshBoneNames_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "ModuleRelativePath", "Private/AppleARKitPoseTrackingLiveLinkRemapAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitBoneNamesToMeshBoneNames = { "AppleARKitBoneNamesToMeshBoneNames", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset, AppleARKitBoneNamesToMeshBoneNames), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitBoneNamesToMeshBoneNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitBoneNamesToMeshBoneNames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitHumanForward,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_MeshForward,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitBoneNamesToMeshBoneNames_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitBoneNamesToMeshBoneNames_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::NewProp_AppleARKitBoneNamesToMeshBoneNames,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::ClassParams = {
		&UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::PropPointers),
		0,
		0x020002A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset, 803144907);
	template<> APPLEARKITPOSETRACKINGLIVELINK_API UClass* StaticClass<UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset>()
	{
		return UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset(Z_Construct_UClass_UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset, &UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset::StaticClass, TEXT("/Script/AppleARKitPoseTrackingLiveLink"), TEXT("UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_AppleARKitPoseTrackingLiveLinkRemapAsset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
