// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APPLEARKIT_AppleARKitLightEstimate_generated_h
#error "AppleARKitLightEstimate.generated.h already included, missing '#pragma once' in AppleARKitLightEstimate.h"
#endif
#define APPLEARKIT_AppleARKitLightEstimate_generated_h

#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitLightEstimate_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAppleARKitLightEstimate_Statics; \
	static class UScriptStruct* StaticStruct();


template<> APPLEARKIT_API UScriptStruct* StaticStruct<struct FAppleARKitLightEstimate>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Private_AppleARKitLightEstimate_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
