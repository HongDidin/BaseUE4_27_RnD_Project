// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKit/Private/AppleARKitFrame.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleARKitFrame() {}
// Cross Module References
	APPLEARKIT_API UScriptStruct* Z_Construct_UScriptStruct_FAppleARKitFrame();
	UPackage* Z_Construct_UPackage__Script_AppleARKit();
// End Cross Module References
class UScriptStruct* FAppleARKitFrame::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APPLEARKIT_API uint32 Get_Z_Construct_UScriptStruct_FAppleARKitFrame_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAppleARKitFrame, Z_Construct_UPackage__Script_AppleARKit(), TEXT("AppleARKitFrame"), sizeof(FAppleARKitFrame), Get_Z_Construct_UScriptStruct_FAppleARKitFrame_Hash());
	}
	return Singleton;
}
template<> APPLEARKIT_API UScriptStruct* StaticStruct<FAppleARKitFrame>()
{
	return FAppleARKitFrame::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAppleARKitFrame(FAppleARKitFrame::StaticStruct, TEXT("/Script/AppleARKit"), TEXT("AppleARKitFrame"), false, nullptr, nullptr);
static struct FScriptStruct_AppleARKit_StaticRegisterNativesFAppleARKitFrame
{
	FScriptStruct_AppleARKit_StaticRegisterNativesFAppleARKitFrame()
	{
		UScriptStruct::DeferCppStructOps<FAppleARKitFrame>(FName(TEXT("AppleARKitFrame")));
	}
} ScriptStruct_AppleARKit_StaticRegisterNativesFAppleARKitFrame;
	struct Z_Construct_UScriptStruct_FAppleARKitFrame_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAppleARKitFrame_Statics::Struct_MetaDataParams[] = {
		{ "Category", "AppleARKit" },
		{ "Comment", "/**\n * An object representing a frame processed by FAppleARKitSystem.\n * @discussion Each frame contains information about the current state of the scene.\n */" },
		{ "ModuleRelativePath", "Private/AppleARKitFrame.h" },
		{ "ToolTip", "An object representing a frame processed by FAppleARKitSystem.\n@discussion Each frame contains information about the current state of the scene." },
	};
#endif
	void* Z_Construct_UScriptStruct_FAppleARKitFrame_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAppleARKitFrame>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAppleARKitFrame_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
		nullptr,
		&NewStructOps,
		"AppleARKitFrame",
		sizeof(FAppleARKitFrame),
		alignof(FAppleARKitFrame),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAppleARKitFrame_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAppleARKitFrame_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAppleARKitFrame()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAppleARKitFrame_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AppleARKit();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AppleARKitFrame"), sizeof(FAppleARKitFrame), Get_Z_Construct_UScriptStruct_FAppleARKitFrame_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAppleARKitFrame_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAppleARKitFrame_Hash() { return 76812715U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
