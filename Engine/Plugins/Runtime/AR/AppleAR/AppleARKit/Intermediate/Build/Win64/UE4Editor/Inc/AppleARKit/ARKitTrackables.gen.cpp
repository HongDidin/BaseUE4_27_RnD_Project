// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleARKit/Private/ARKitTrackables.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeARKitTrackables() {}
// Cross Module References
	APPLEARKIT_API UClass* Z_Construct_UClass_UARKitMeshGeometry_NoRegister();
	APPLEARKIT_API UClass* Z_Construct_UClass_UARKitMeshGeometry();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UARMeshGeometry();
	UPackage* Z_Construct_UPackage__Script_AppleARKit();
// End Cross Module References
	void UARKitMeshGeometry::StaticRegisterNativesUARKitMeshGeometry()
	{
	}
	UClass* Z_Construct_UClass_UARKitMeshGeometry_NoRegister()
	{
		return UARKitMeshGeometry::StaticClass();
	}
	struct Z_Construct_UClass_UARKitMeshGeometry_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UARKitMeshGeometry_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UARMeshGeometry,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleARKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARKitMeshGeometry_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ARKitTrackables.h" },
		{ "ModuleRelativePath", "Private/ARKitTrackables.h" },
		{ "NotBlueprintType", "true" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UARKitMeshGeometry_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UARKitMeshGeometry>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UARKitMeshGeometry_Statics::ClassParams = {
		&UARKitMeshGeometry::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UARKitMeshGeometry_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UARKitMeshGeometry_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UARKitMeshGeometry()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UARKitMeshGeometry_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UARKitMeshGeometry, 1003761560);
	template<> APPLEARKIT_API UClass* StaticClass<UARKitMeshGeometry>()
	{
		return UARKitMeshGeometry::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UARKitMeshGeometry(Z_Construct_UClass_UARKitMeshGeometry, &UARKitMeshGeometry::StaticClass, TEXT("/Script/AppleARKit"), TEXT("UARKitMeshGeometry"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UARKitMeshGeometry);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
