// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APPLEARKIT_AppleARKitTextures_generated_h
#error "AppleARKitTextures.generated.h already included, missing '#pragma once' in AppleARKitTextures.h"
#endif
#define APPLEARKIT_AppleARKitTextures_generated_h

#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAppleARKitTextureCameraImage(); \
	friend struct Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics; \
public: \
	DECLARE_CLASS(UAppleARKitTextureCameraImage, UARTextureCameraImage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UAppleARKitTextureCameraImage) \
	virtual UObject* _getUObject() const override { return const_cast<UAppleARKitTextureCameraImage*>(this); }


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUAppleARKitTextureCameraImage(); \
	friend struct Z_Construct_UClass_UAppleARKitTextureCameraImage_Statics; \
public: \
	DECLARE_CLASS(UAppleARKitTextureCameraImage, UARTextureCameraImage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UAppleARKitTextureCameraImage) \
	virtual UObject* _getUObject() const override { return const_cast<UAppleARKitTextureCameraImage*>(this); }


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleARKitTextureCameraImage(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleARKitTextureCameraImage) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleARKitTextureCameraImage); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleARKitTextureCameraImage); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleARKitTextureCameraImage(UAppleARKitTextureCameraImage&&); \
	NO_API UAppleARKitTextureCameraImage(const UAppleARKitTextureCameraImage&); \
public:


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleARKitTextureCameraImage(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleARKitTextureCameraImage(UAppleARKitTextureCameraImage&&); \
	NO_API UAppleARKitTextureCameraImage(const UAppleARKitTextureCameraImage&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleARKitTextureCameraImage); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleARKitTextureCameraImage); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleARKitTextureCameraImage)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_27_PROLOG
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_INCLASS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_32_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AppleARKitTextureCameraImage."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEARKIT_API UClass* StaticClass<class UAppleARKitTextureCameraImage>();

#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAppleARKitTextureCameraDepth(); \
	friend struct Z_Construct_UClass_UAppleARKitTextureCameraDepth_Statics; \
public: \
	DECLARE_CLASS(UAppleARKitTextureCameraDepth, UARTextureCameraDepth, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UAppleARKitTextureCameraDepth)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_INCLASS \
private: \
	static void StaticRegisterNativesUAppleARKitTextureCameraDepth(); \
	friend struct Z_Construct_UClass_UAppleARKitTextureCameraDepth_Statics; \
public: \
	DECLARE_CLASS(UAppleARKitTextureCameraDepth, UARTextureCameraDepth, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UAppleARKitTextureCameraDepth)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleARKitTextureCameraDepth(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleARKitTextureCameraDepth) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleARKitTextureCameraDepth); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleARKitTextureCameraDepth); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleARKitTextureCameraDepth(UAppleARKitTextureCameraDepth&&); \
	NO_API UAppleARKitTextureCameraDepth(const UAppleARKitTextureCameraDepth&); \
public:


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleARKitTextureCameraDepth(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleARKitTextureCameraDepth(UAppleARKitTextureCameraDepth&&); \
	NO_API UAppleARKitTextureCameraDepth(const UAppleARKitTextureCameraDepth&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleARKitTextureCameraDepth); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleARKitTextureCameraDepth); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleARKitTextureCameraDepth)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_59_PROLOG
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_INCLASS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_63_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AppleARKitTextureCameraDepth."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEARKIT_API UClass* StaticClass<class UAppleARKitTextureCameraDepth>();

#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAppleARKitEnvironmentCaptureProbeTexture(); \
	friend struct Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics; \
public: \
	DECLARE_CLASS(UAppleARKitEnvironmentCaptureProbeTexture, UAREnvironmentCaptureProbeTexture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UAppleARKitEnvironmentCaptureProbeTexture) \
	virtual UObject* _getUObject() const override { return const_cast<UAppleARKitEnvironmentCaptureProbeTexture*>(this); }


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_INCLASS \
private: \
	static void StaticRegisterNativesUAppleARKitEnvironmentCaptureProbeTexture(); \
	friend struct Z_Construct_UClass_UAppleARKitEnvironmentCaptureProbeTexture_Statics; \
public: \
	DECLARE_CLASS(UAppleARKitEnvironmentCaptureProbeTexture, UAREnvironmentCaptureProbeTexture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UAppleARKitEnvironmentCaptureProbeTexture) \
	virtual UObject* _getUObject() const override { return const_cast<UAppleARKitEnvironmentCaptureProbeTexture*>(this); }


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleARKitEnvironmentCaptureProbeTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleARKitEnvironmentCaptureProbeTexture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleARKitEnvironmentCaptureProbeTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleARKitEnvironmentCaptureProbeTexture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleARKitEnvironmentCaptureProbeTexture(UAppleARKitEnvironmentCaptureProbeTexture&&); \
	NO_API UAppleARKitEnvironmentCaptureProbeTexture(const UAppleARKitEnvironmentCaptureProbeTexture&); \
public:


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleARKitEnvironmentCaptureProbeTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleARKitEnvironmentCaptureProbeTexture(UAppleARKitEnvironmentCaptureProbeTexture&&); \
	NO_API UAppleARKitEnvironmentCaptureProbeTexture(const UAppleARKitEnvironmentCaptureProbeTexture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleARKitEnvironmentCaptureProbeTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleARKitEnvironmentCaptureProbeTexture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleARKitEnvironmentCaptureProbeTexture)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_87_PROLOG
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_INCLASS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_92_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AppleARKitEnvironmentCaptureProbeTexture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEARKIT_API UClass* StaticClass<class UAppleARKitEnvironmentCaptureProbeTexture>();

#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAppleARKitOcclusionTexture(); \
	friend struct Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics; \
public: \
	DECLARE_CLASS(UAppleARKitOcclusionTexture, UARTexture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UAppleARKitOcclusionTexture) \
	virtual UObject* _getUObject() const override { return const_cast<UAppleARKitOcclusionTexture*>(this); }


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_INCLASS \
private: \
	static void StaticRegisterNativesUAppleARKitOcclusionTexture(); \
	friend struct Z_Construct_UClass_UAppleARKitOcclusionTexture_Statics; \
public: \
	DECLARE_CLASS(UAppleARKitOcclusionTexture, UARTexture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UAppleARKitOcclusionTexture) \
	virtual UObject* _getUObject() const override { return const_cast<UAppleARKitOcclusionTexture*>(this); }


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleARKitOcclusionTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleARKitOcclusionTexture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleARKitOcclusionTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleARKitOcclusionTexture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleARKitOcclusionTexture(UAppleARKitOcclusionTexture&&); \
	NO_API UAppleARKitOcclusionTexture(const UAppleARKitOcclusionTexture&); \
public:


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleARKitOcclusionTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleARKitOcclusionTexture(UAppleARKitOcclusionTexture&&); \
	NO_API UAppleARKitOcclusionTexture(const UAppleARKitOcclusionTexture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleARKitOcclusionTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleARKitOcclusionTexture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleARKitOcclusionTexture)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_118_PROLOG
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_INCLASS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_123_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AppleARKitOcclusionTexture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEARKIT_API UClass* StaticClass<class UAppleARKitOcclusionTexture>();

#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAppleARKitCameraVideoTexture(); \
	friend struct Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics; \
public: \
	DECLARE_CLASS(UAppleARKitCameraVideoTexture, UARTextureCameraImage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UAppleARKitCameraVideoTexture) \
	virtual UObject* _getUObject() const override { return const_cast<UAppleARKitCameraVideoTexture*>(this); }


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_INCLASS \
private: \
	static void StaticRegisterNativesUAppleARKitCameraVideoTexture(); \
	friend struct Z_Construct_UClass_UAppleARKitCameraVideoTexture_Statics; \
public: \
	DECLARE_CLASS(UAppleARKitCameraVideoTexture, UARTextureCameraImage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AppleARKit"), NO_API) \
	DECLARE_SERIALIZER(UAppleARKitCameraVideoTexture) \
	virtual UObject* _getUObject() const override { return const_cast<UAppleARKitCameraVideoTexture*>(this); }


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleARKitCameraVideoTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleARKitCameraVideoTexture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleARKitCameraVideoTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleARKitCameraVideoTexture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleARKitCameraVideoTexture(UAppleARKitCameraVideoTexture&&); \
	NO_API UAppleARKitCameraVideoTexture(const UAppleARKitCameraVideoTexture&); \
public:


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleARKitCameraVideoTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleARKitCameraVideoTexture(UAppleARKitCameraVideoTexture&&); \
	NO_API UAppleARKitCameraVideoTexture(const UAppleARKitCameraVideoTexture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleARKitCameraVideoTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleARKitCameraVideoTexture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleARKitCameraVideoTexture)


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_148_PROLOG
#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_INCLASS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h_151_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AppleARKitCameraVideoTexture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEARKIT_API UClass* StaticClass<class UAppleARKitCameraVideoTexture>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_AppleAR_AppleARKit_Source_AppleARKit_Public_AppleARKitTextures_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
