// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EGoogleARCoreAugmentedFaceRegion : uint8;
struct FTransform;
#ifdef GOOGLEARCOREBASE_GoogleARCoreAugmentedFace_generated_h
#error "GoogleARCoreAugmentedFace.generated.h already included, missing '#pragma once' in GoogleARCoreAugmentedFace.h"
#endif
#define GOOGLEARCOREBASE_GoogleARCoreAugmentedFace_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetLocalToTrackingTransformOfRegion); \
	DECLARE_FUNCTION(execGetLocalToWorldTransformOfRegion);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetLocalToTrackingTransformOfRegion); \
	DECLARE_FUNCTION(execGetLocalToWorldTransformOfRegion);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleARCoreAugmentedFace(); \
	friend struct Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreAugmentedFace, UARFaceGeometry, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreAugmentedFace)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleARCoreAugmentedFace(); \
	friend struct Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreAugmentedFace, UARFaceGeometry, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreAugmentedFace)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreAugmentedFace(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleARCoreAugmentedFace) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreAugmentedFace); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreAugmentedFace); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreAugmentedFace(UGoogleARCoreAugmentedFace&&); \
	NO_API UGoogleARCoreAugmentedFace(const UGoogleARCoreAugmentedFace&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreAugmentedFace() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreAugmentedFace(UGoogleARCoreAugmentedFace&&); \
	NO_API UGoogleARCoreAugmentedFace(const UGoogleARCoreAugmentedFace&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreAugmentedFace); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreAugmentedFace); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGoogleARCoreAugmentedFace)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_30_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCOREBASE_API UClass* StaticClass<class UGoogleARCoreAugmentedFace>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedFace_h


#define FOREACH_ENUM_EGOOGLEARCOREAUGMENTEDFACEREGION(op) \
	op(EGoogleARCoreAugmentedFaceRegion::NoseTip) \
	op(EGoogleARCoreAugmentedFaceRegion::ForeheadLeft) \
	op(EGoogleARCoreAugmentedFaceRegion::ForeheadRight) 

enum class EGoogleARCoreAugmentedFaceRegion : uint8;
template<> GOOGLEARCOREBASE_API UEnum* StaticEnum<EGoogleARCoreAugmentedFaceRegion>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
