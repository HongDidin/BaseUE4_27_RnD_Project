// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEARCORERENDERING_GoogleARCorePassthroughCameraRenderer_generated_h
#error "GoogleARCorePassthroughCameraRenderer.generated.h already included, missing '#pragma once' in GoogleARCorePassthroughCameraRenderer.h"
#endif
#define GOOGLEARCORERENDERING_GoogleARCorePassthroughCameraRenderer_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleARCoreCameraOverlayMaterialLoader(); \
	friend struct Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreCameraOverlayMaterialLoader, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreRendering"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreCameraOverlayMaterialLoader)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleARCoreCameraOverlayMaterialLoader(); \
	friend struct Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreCameraOverlayMaterialLoader, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreRendering"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreCameraOverlayMaterialLoader)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreCameraOverlayMaterialLoader(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleARCoreCameraOverlayMaterialLoader) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreCameraOverlayMaterialLoader); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreCameraOverlayMaterialLoader); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreCameraOverlayMaterialLoader(UGoogleARCoreCameraOverlayMaterialLoader&&); \
	NO_API UGoogleARCoreCameraOverlayMaterialLoader(const UGoogleARCoreCameraOverlayMaterialLoader&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreCameraOverlayMaterialLoader(UGoogleARCoreCameraOverlayMaterialLoader&&); \
	NO_API UGoogleARCoreCameraOverlayMaterialLoader(const UGoogleARCoreCameraOverlayMaterialLoader&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreCameraOverlayMaterialLoader); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreCameraOverlayMaterialLoader); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGoogleARCoreCameraOverlayMaterialLoader)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_27_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h_30_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCORERENDERING_API UClass* StaticClass<class UGoogleARCoreCameraOverlayMaterialLoader>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Public_GoogleARCorePassthroughCameraRenderer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
