// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FTransform;
class UARFaceGeometry;
struct FVector;
struct FVector2D;
#ifdef GOOGLEARCOREBASE_GoogleARCoreFaceMeshComponent_generated_h
#error "GoogleARCoreFaceMeshComponent.generated.h already included, missing '#pragma once' in GoogleARCoreFaceMeshComponent.h"
#endif
#define GOOGLEARCOREBASE_GoogleARCoreFaceMeshComponent_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetTransform); \
	DECLARE_FUNCTION(execBindARFaceGeometry); \
	DECLARE_FUNCTION(execSetAutoBind); \
	DECLARE_FUNCTION(execUpdateMesh); \
	DECLARE_FUNCTION(execCreateMesh);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetTransform); \
	DECLARE_FUNCTION(execBindARFaceGeometry); \
	DECLARE_FUNCTION(execSetAutoBind); \
	DECLARE_FUNCTION(execUpdateMesh); \
	DECLARE_FUNCTION(execCreateMesh);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_GoogleARCoreFaceMeshComponent(); \
	friend struct Z_Construct_UClass_UDEPRECATED_GoogleARCoreFaceMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_GoogleARCoreFaceMeshComponent, UProceduralMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_GoogleARCoreFaceMeshComponent)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_GoogleARCoreFaceMeshComponent(); \
	friend struct Z_Construct_UClass_UDEPRECATED_GoogleARCoreFaceMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_GoogleARCoreFaceMeshComponent, UProceduralMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_GoogleARCoreFaceMeshComponent)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_GoogleARCoreFaceMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_GoogleARCoreFaceMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_GoogleARCoreFaceMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_GoogleARCoreFaceMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_GoogleARCoreFaceMeshComponent(UDEPRECATED_GoogleARCoreFaceMeshComponent&&); \
	NO_API UDEPRECATED_GoogleARCoreFaceMeshComponent(const UDEPRECATED_GoogleARCoreFaceMeshComponent&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_GoogleARCoreFaceMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_GoogleARCoreFaceMeshComponent(UDEPRECATED_GoogleARCoreFaceMeshComponent&&); \
	NO_API UDEPRECATED_GoogleARCoreFaceMeshComponent(const UDEPRECATED_GoogleARCoreFaceMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_GoogleARCoreFaceMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_GoogleARCoreFaceMeshComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_GoogleARCoreFaceMeshComponent)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_26_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h_29_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GoogleARCoreFaceMeshComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCOREBASE_API UClass* StaticClass<class UDEPRECATED_GoogleARCoreFaceMeshComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreFaceMeshComponent_h


#define FOREACH_ENUM_EARCOREFACECOMPONENTTRANSFORMMIXING(op) \
	op(EARCoreFaceComponentTransformMixing::ComponentOnly) \
	op(EARCoreFaceComponentTransformMixing::ComponentLocationTrackedRotation) \
	op(EARCoreFaceComponentTransformMixing::TrackingOnly) 

enum class EARCoreFaceComponentTransformMixing : uint8;
template<> GOOGLEARCOREBASE_API UEnum* StaticEnum<EARCoreFaceComponentTransformMixing>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
