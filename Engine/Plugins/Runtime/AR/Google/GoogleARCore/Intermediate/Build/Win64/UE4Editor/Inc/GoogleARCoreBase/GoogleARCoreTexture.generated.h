// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEARCOREBASE_GoogleARCoreTexture_generated_h
#error "GoogleARCoreTexture.generated.h already included, missing '#pragma once' in GoogleARCoreTexture.h"
#endif
#define GOOGLEARCOREBASE_GoogleARCoreTexture_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUARCoreCameraTexture(); \
	friend struct Z_Construct_UClass_UARCoreCameraTexture_Statics; \
public: \
	DECLARE_CLASS(UARCoreCameraTexture, UARTexture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UARCoreCameraTexture)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUARCoreCameraTexture(); \
	friend struct Z_Construct_UClass_UARCoreCameraTexture_Statics; \
public: \
	DECLARE_CLASS(UARCoreCameraTexture, UARTexture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UARCoreCameraTexture)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARCoreCameraTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARCoreCameraTexture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARCoreCameraTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARCoreCameraTexture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARCoreCameraTexture(UARCoreCameraTexture&&); \
	NO_API UARCoreCameraTexture(const UARCoreCameraTexture&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARCoreCameraTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARCoreCameraTexture(UARCoreCameraTexture&&); \
	NO_API UARCoreCameraTexture(const UARCoreCameraTexture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARCoreCameraTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARCoreCameraTexture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARCoreCameraTexture)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_15_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ARCoreCameraTexture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCOREBASE_API UClass* StaticClass<class UARCoreCameraTexture>();

#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUARCoreDepthTexture(); \
	friend struct Z_Construct_UClass_UARCoreDepthTexture_Statics; \
public: \
	DECLARE_CLASS(UARCoreDepthTexture, UARTexture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UARCoreDepthTexture)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUARCoreDepthTexture(); \
	friend struct Z_Construct_UClass_UARCoreDepthTexture_Statics; \
public: \
	DECLARE_CLASS(UARCoreDepthTexture, UARTexture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UARCoreDepthTexture)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARCoreDepthTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARCoreDepthTexture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARCoreDepthTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARCoreDepthTexture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARCoreDepthTexture(UARCoreDepthTexture&&); \
	NO_API UARCoreDepthTexture(const UARCoreDepthTexture&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARCoreDepthTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARCoreDepthTexture(UARCoreDepthTexture&&); \
	NO_API UARCoreDepthTexture(const UARCoreDepthTexture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARCoreDepthTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARCoreDepthTexture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARCoreDepthTexture)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_30_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h_33_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ARCoreDepthTexture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCOREBASE_API UClass* StaticClass<class UARCoreDepthTexture>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Private_GoogleARCoreTexture_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
