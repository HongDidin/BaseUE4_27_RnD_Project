// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleARCoreRendering/Private/MaterialExpressionGoogleARCorePassthroughCamera.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMaterialExpressionGoogleARCorePassthroughCamera() {}
// Cross Module References
	GOOGLEARCORERENDERING_API UClass* Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_NoRegister();
	GOOGLEARCORERENDERING_API UClass* Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialExpression();
	UPackage* Z_Construct_UPackage__Script_GoogleARCoreRendering();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FExpressionInput();
// End Cross Module References
	void UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera::StaticRegisterNativesUDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_NoRegister()
	{
		return UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Coordinates_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Coordinates;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConstCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ConstCoordinate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMaterialExpression,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreRendering,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Implements a node sampling from the ARCore Passthrough external texture.\n*/" },
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "MaterialExpressionGoogleARCorePassthroughCamera.h" },
		{ "ModuleRelativePath", "Private/MaterialExpressionGoogleARCorePassthroughCamera.h" },
		{ "ToolTip", "Implements a node sampling from the ARCore Passthrough external texture." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::NewProp_Coordinates_MetaData[] = {
		{ "ModuleRelativePath", "Private/MaterialExpressionGoogleARCorePassthroughCamera.h" },
		{ "RequiredInput", "false" },
		{ "ToolTip", "Defaults to 'ConstCoordinate' if not specified" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::NewProp_Coordinates = { "Coordinates", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera, Coordinates), Z_Construct_UScriptStruct_FExpressionInput, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::NewProp_Coordinates_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::NewProp_Coordinates_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::NewProp_ConstCoordinate_MetaData[] = {
		{ "Category", "UMaterialExpressionGoogleARCorePassthroughCamera" },
		{ "Comment", "/** only used if Coordinates are not hooked up. */" },
		{ "ModuleRelativePath", "Private/MaterialExpressionGoogleARCorePassthroughCamera.h" },
		{ "ToolTip", "only used if Coordinates are not hooked up." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::NewProp_ConstCoordinate = { "ConstCoordinate", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera, ConstCoordinate), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::NewProp_ConstCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::NewProp_ConstCoordinate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::NewProp_Coordinates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::NewProp_ConstCoordinate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::ClassParams = {
		&UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::PropPointers),
		0,
		0x021022A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera, 2754094426);
	template<> GOOGLEARCORERENDERING_API UClass* StaticClass<UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera>()
	{
		return UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera(Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera, &UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera::StaticClass, TEXT("/Script/GoogleARCoreRendering"), TEXT("UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
