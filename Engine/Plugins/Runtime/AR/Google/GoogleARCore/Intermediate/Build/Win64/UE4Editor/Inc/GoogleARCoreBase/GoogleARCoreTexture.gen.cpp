// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleARCoreBase/Private/GoogleARCoreTexture.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleARCoreTexture() {}
// Cross Module References
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UARCoreCameraTexture_NoRegister();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UARCoreCameraTexture();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UARTexture();
	UPackage* Z_Construct_UPackage__Script_GoogleARCoreBase();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UARCoreDepthTexture_NoRegister();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UARCoreDepthTexture();
// End Cross Module References
	void UARCoreCameraTexture::StaticRegisterNativesUARCoreCameraTexture()
	{
	}
	UClass* Z_Construct_UClass_UARCoreCameraTexture_NoRegister()
	{
		return UARCoreCameraTexture::StaticClass();
	}
	struct Z_Construct_UClass_UARCoreCameraTexture_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UARCoreCameraTexture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UARTexture,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreBase,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARCoreCameraTexture_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GoogleARCoreTexture.h" },
		{ "ModuleRelativePath", "Private/GoogleARCoreTexture.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UARCoreCameraTexture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UARCoreCameraTexture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UARCoreCameraTexture_Statics::ClassParams = {
		&UARCoreCameraTexture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UARCoreCameraTexture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UARCoreCameraTexture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UARCoreCameraTexture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UARCoreCameraTexture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UARCoreCameraTexture, 4064071404);
	template<> GOOGLEARCOREBASE_API UClass* StaticClass<UARCoreCameraTexture>()
	{
		return UARCoreCameraTexture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UARCoreCameraTexture(Z_Construct_UClass_UARCoreCameraTexture, &UARCoreCameraTexture::StaticClass, TEXT("/Script/GoogleARCoreBase"), TEXT("UARCoreCameraTexture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UARCoreCameraTexture);
	void UARCoreDepthTexture::StaticRegisterNativesUARCoreDepthTexture()
	{
	}
	UClass* Z_Construct_UClass_UARCoreDepthTexture_NoRegister()
	{
		return UARCoreDepthTexture::StaticClass();
	}
	struct Z_Construct_UClass_UARCoreDepthTexture_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UARCoreDepthTexture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UARTexture,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreBase,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UARCoreDepthTexture_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GoogleARCoreTexture.h" },
		{ "ModuleRelativePath", "Private/GoogleARCoreTexture.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UARCoreDepthTexture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UARCoreDepthTexture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UARCoreDepthTexture_Statics::ClassParams = {
		&UARCoreDepthTexture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UARCoreDepthTexture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UARCoreDepthTexture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UARCoreDepthTexture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UARCoreDepthTexture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UARCoreDepthTexture, 2122616085);
	template<> GOOGLEARCOREBASE_API UClass* StaticClass<UARCoreDepthTexture>()
	{
		return UARCoreDepthTexture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UARCoreDepthTexture(Z_Construct_UClass_UARCoreDepthTexture, &UARCoreDepthTexture::StaticClass, TEXT("/Script/GoogleARCoreBase"), TEXT("UARCoreDepthTexture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UARCoreDepthTexture);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
