// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef GOOGLEARCOREBASE_GoogleARCoreAugmentedImage_generated_h
#error "GoogleARCoreAugmentedImage.generated.h already included, missing '#pragma once' in GoogleARCoreAugmentedImage.h"
#endif
#define GOOGLEARCOREBASE_GoogleARCoreAugmentedImage_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetImageName); \
	DECLARE_FUNCTION(execGetImageIndex); \
	DECLARE_FUNCTION(execGetExtent); \
	DECLARE_FUNCTION(execGetCenter);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetImageName); \
	DECLARE_FUNCTION(execGetImageIndex); \
	DECLARE_FUNCTION(execGetExtent); \
	DECLARE_FUNCTION(execGetCenter);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleARCoreAugmentedImage(); \
	friend struct Z_Construct_UClass_UGoogleARCoreAugmentedImage_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreAugmentedImage, UARTrackedImage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreAugmentedImage)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleARCoreAugmentedImage(); \
	friend struct Z_Construct_UClass_UGoogleARCoreAugmentedImage_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreAugmentedImage, UARTrackedImage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreAugmentedImage)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreAugmentedImage(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleARCoreAugmentedImage) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreAugmentedImage); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreAugmentedImage); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreAugmentedImage(UGoogleARCoreAugmentedImage&&); \
	NO_API UGoogleARCoreAugmentedImage(const UGoogleARCoreAugmentedImage&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreAugmentedImage() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreAugmentedImage(UGoogleARCoreAugmentedImage&&); \
	NO_API UGoogleARCoreAugmentedImage(const UGoogleARCoreAugmentedImage&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreAugmentedImage); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreAugmentedImage); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGoogleARCoreAugmentedImage)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ImageIndex() { return STRUCT_OFFSET(UGoogleARCoreAugmentedImage, ImageIndex); } \
	FORCEINLINE static uint32 __PPO__ImageName() { return STRUCT_OFFSET(UGoogleARCoreAugmentedImage, ImageName); }


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_14_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCOREBASE_API UClass* StaticClass<class UGoogleARCoreAugmentedImage>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreAugmentedImage_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
