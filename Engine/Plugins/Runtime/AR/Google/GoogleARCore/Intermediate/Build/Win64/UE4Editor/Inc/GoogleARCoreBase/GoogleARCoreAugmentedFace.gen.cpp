// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleARCoreBase/Public/GoogleARCoreAugmentedFace.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleARCoreAugmentedFace() {}
// Cross Module References
	GOOGLEARCOREBASE_API UEnum* Z_Construct_UEnum_GoogleARCoreBase_EGoogleARCoreAugmentedFaceRegion();
	UPackage* Z_Construct_UPackage__Script_GoogleARCoreBase();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UGoogleARCoreAugmentedFace_NoRegister();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UGoogleARCoreAugmentedFace();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UARFaceGeometry();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References
	static UEnum* EGoogleARCoreAugmentedFaceRegion_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_GoogleARCoreBase_EGoogleARCoreAugmentedFaceRegion, Z_Construct_UPackage__Script_GoogleARCoreBase(), TEXT("EGoogleARCoreAugmentedFaceRegion"));
		}
		return Singleton;
	}
	template<> GOOGLEARCOREBASE_API UEnum* StaticEnum<EGoogleARCoreAugmentedFaceRegion>()
	{
		return EGoogleARCoreAugmentedFaceRegion_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGoogleARCoreAugmentedFaceRegion(EGoogleARCoreAugmentedFaceRegion_StaticEnum, TEXT("/Script/GoogleARCoreBase"), TEXT("EGoogleARCoreAugmentedFaceRegion"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_GoogleARCoreBase_EGoogleARCoreAugmentedFaceRegion_Hash() { return 3623989263U; }
	UEnum* Z_Construct_UEnum_GoogleARCoreBase_EGoogleARCoreAugmentedFaceRegion()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleARCoreBase();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGoogleARCoreAugmentedFaceRegion"), 0, Get_Z_Construct_UEnum_GoogleARCoreBase_EGoogleARCoreAugmentedFaceRegion_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGoogleARCoreAugmentedFaceRegion::NoseTip", (int64)EGoogleARCoreAugmentedFaceRegion::NoseTip },
				{ "EGoogleARCoreAugmentedFaceRegion::ForeheadLeft", (int64)EGoogleARCoreAugmentedFaceRegion::ForeheadLeft },
				{ "EGoogleARCoreAugmentedFaceRegion::ForeheadRight", (int64)EGoogleARCoreAugmentedFaceRegion::ForeheadRight },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n * @ingroup GoogleARCoreBase\n * Describes the face regions for which the pose can be queried. Left and right\n * are defined relative to the actor (the person that the mesh belongs to).\n */" },
				{ "ForeheadLeft.Comment", "/* A region around the left forehead of the AugmentedFace. */" },
				{ "ForeheadLeft.Name", "EGoogleARCoreAugmentedFaceRegion::ForeheadLeft" },
				{ "ForeheadLeft.ToolTip", "A region around the left forehead of the AugmentedFace." },
				{ "ForeheadRight.Comment", "/* A region around the right forehead of the AugmentedFace. */" },
				{ "ForeheadRight.Name", "EGoogleARCoreAugmentedFaceRegion::ForeheadRight" },
				{ "ForeheadRight.ToolTip", "A region around the right forehead of the AugmentedFace." },
				{ "ModuleRelativePath", "Public/GoogleARCoreAugmentedFace.h" },
				{ "NoseTip.Comment", "/* A region around the nose of the AugmentedFace. */" },
				{ "NoseTip.Name", "EGoogleARCoreAugmentedFaceRegion::NoseTip" },
				{ "NoseTip.ToolTip", "A region around the nose of the AugmentedFace." },
				{ "ToolTip", "@ingroup GoogleARCoreBase\nDescribes the face regions for which the pose can be queried. Left and right\nare defined relative to the actor (the person that the mesh belongs to)." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_GoogleARCoreBase,
				nullptr,
				"EGoogleARCoreAugmentedFaceRegion",
				"EGoogleARCoreAugmentedFaceRegion",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UGoogleARCoreAugmentedFace::execGetLocalToTrackingTransformOfRegion)
	{
		P_GET_ENUM(EGoogleARCoreAugmentedFaceRegion,Z_Param_Region);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetLocalToTrackingTransformOfRegion(EGoogleARCoreAugmentedFaceRegion(Z_Param_Region));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleARCoreAugmentedFace::execGetLocalToWorldTransformOfRegion)
	{
		P_GET_ENUM(EGoogleARCoreAugmentedFaceRegion,Z_Param_Region);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=P_THIS->GetLocalToWorldTransformOfRegion(EGoogleARCoreAugmentedFaceRegion(Z_Param_Region));
		P_NATIVE_END;
	}
	void UGoogleARCoreAugmentedFace::StaticRegisterNativesUGoogleARCoreAugmentedFace()
	{
		UClass* Class = UGoogleARCoreAugmentedFace::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetLocalToTrackingTransformOfRegion", &UGoogleARCoreAugmentedFace::execGetLocalToTrackingTransformOfRegion },
			{ "GetLocalToWorldTransformOfRegion", &UGoogleARCoreAugmentedFace::execGetLocalToWorldTransformOfRegion },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics
	{
		struct GoogleARCoreAugmentedFace_eventGetLocalToTrackingTransformOfRegion_Parms
		{
			EGoogleARCoreAugmentedFaceRegion Region;
			FTransform ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Region_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Region;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::NewProp_Region_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::NewProp_Region = { "Region", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleARCoreAugmentedFace_eventGetLocalToTrackingTransformOfRegion_Parms, Region), Z_Construct_UEnum_GoogleARCoreBase_EGoogleARCoreAugmentedFaceRegion, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleARCoreAugmentedFace_eventGetLocalToTrackingTransformOfRegion_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::NewProp_Region_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::NewProp_Region,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleARCore|AugmentedImage" },
		{ "Comment", "/**\n\x09 * Returns the latest known local-to-tracking transform of the given face region.\n\x09 */" },
		{ "ModuleRelativePath", "Public/GoogleARCoreAugmentedFace.h" },
		{ "ToolTip", "Returns the latest known local-to-tracking transform of the given face region." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleARCoreAugmentedFace, nullptr, "GetLocalToTrackingTransformOfRegion", nullptr, nullptr, sizeof(GoogleARCoreAugmentedFace_eventGetLocalToTrackingTransformOfRegion_Parms), Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics
	{
		struct GoogleARCoreAugmentedFace_eventGetLocalToWorldTransformOfRegion_Parms
		{
			EGoogleARCoreAugmentedFaceRegion Region;
			FTransform ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Region_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Region;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::NewProp_Region_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::NewProp_Region = { "Region", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleARCoreAugmentedFace_eventGetLocalToWorldTransformOfRegion_Parms, Region), Z_Construct_UEnum_GoogleARCoreBase_EGoogleARCoreAugmentedFaceRegion, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleARCoreAugmentedFace_eventGetLocalToWorldTransformOfRegion_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::NewProp_Region_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::NewProp_Region,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleARCore|AugmentedImage" },
		{ "Comment", "/**\n\x09 * Returns the latest known local-to-world transform of the given face region.\n\x09 */" },
		{ "ModuleRelativePath", "Public/GoogleARCoreAugmentedFace.h" },
		{ "ToolTip", "Returns the latest known local-to-world transform of the given face region." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleARCoreAugmentedFace, nullptr, "GetLocalToWorldTransformOfRegion", nullptr, nullptr, sizeof(GoogleARCoreAugmentedFace_eventGetLocalToWorldTransformOfRegion_Parms), Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGoogleARCoreAugmentedFace_NoRegister()
	{
		return UGoogleARCoreAugmentedFace::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UARFaceGeometry,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreBase,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToTrackingTransformOfRegion, "GetLocalToTrackingTransformOfRegion" }, // 3882959427
		{ &Z_Construct_UFunction_UGoogleARCoreAugmentedFace_GetLocalToWorldTransformOfRegion, "GetLocalToWorldTransformOfRegion" }, // 1121456476
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * An UObject representing a face detected by ARCore.\n */" },
		{ "IncludePath", "GoogleARCoreAugmentedFace.h" },
		{ "ModuleRelativePath", "Public/GoogleARCoreAugmentedFace.h" },
		{ "ToolTip", "An UObject representing a face detected by ARCore." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleARCoreAugmentedFace>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics::ClassParams = {
		&UGoogleARCoreAugmentedFace::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleARCoreAugmentedFace()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleARCoreAugmentedFace_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleARCoreAugmentedFace, 197306669);
	template<> GOOGLEARCOREBASE_API UClass* StaticClass<UGoogleARCoreAugmentedFace>()
	{
		return UGoogleARCoreAugmentedFace::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleARCoreAugmentedFace(Z_Construct_UClass_UGoogleARCoreAugmentedFace, &UGoogleARCoreAugmentedFace::StaticClass, TEXT("/Script/GoogleARCoreBase"), TEXT("UGoogleARCoreAugmentedFace"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleARCoreAugmentedFace);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
