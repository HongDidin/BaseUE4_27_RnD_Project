// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEARCOREBASE_GoogleARCorePlaneRendererComponent_generated_h
#error "GoogleARCorePlaneRendererComponent.generated.h already included, missing '#pragma once' in GoogleARCorePlaneRendererComponent.h"
#endif
#define GOOGLEARCOREBASE_GoogleARCorePlaneRendererComponent_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_GoogleARCorePlaneRendererComponent(); \
	friend struct Z_Construct_UClass_UDEPRECATED_GoogleARCorePlaneRendererComponent_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_GoogleARCorePlaneRendererComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_GoogleARCorePlaneRendererComponent)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_GoogleARCorePlaneRendererComponent(); \
	friend struct Z_Construct_UClass_UDEPRECATED_GoogleARCorePlaneRendererComponent_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_GoogleARCorePlaneRendererComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_GoogleARCorePlaneRendererComponent)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_GoogleARCorePlaneRendererComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_GoogleARCorePlaneRendererComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_GoogleARCorePlaneRendererComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_GoogleARCorePlaneRendererComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_GoogleARCorePlaneRendererComponent(UDEPRECATED_GoogleARCorePlaneRendererComponent&&); \
	NO_API UDEPRECATED_GoogleARCorePlaneRendererComponent(const UDEPRECATED_GoogleARCorePlaneRendererComponent&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_GoogleARCorePlaneRendererComponent(UDEPRECATED_GoogleARCorePlaneRendererComponent&&); \
	NO_API UDEPRECATED_GoogleARCorePlaneRendererComponent(const UDEPRECATED_GoogleARCorePlaneRendererComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_GoogleARCorePlaneRendererComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_GoogleARCorePlaneRendererComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDEPRECATED_GoogleARCorePlaneRendererComponent)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_13_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCOREBASE_API UClass* StaticClass<class UDEPRECATED_GoogleARCorePlaneRendererComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCorePlaneRendererComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
