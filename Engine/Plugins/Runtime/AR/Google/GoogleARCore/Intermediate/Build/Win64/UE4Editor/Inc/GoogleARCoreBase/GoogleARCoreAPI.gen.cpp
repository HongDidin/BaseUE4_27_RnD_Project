// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleARCoreBase/Private/GoogleARCoreAPI.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleARCoreAPI() {}
// Cross Module References
	GOOGLEARCOREBASE_API UScriptStruct* Z_Construct_UScriptStruct_FARCorePointer();
	UPackage* Z_Construct_UPackage__Script_GoogleARCoreBase();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UGoogleARCoreUObjectManager_NoRegister();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UGoogleARCoreUObjectManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UGoogleARCorePointCloud_NoRegister();
	AUGMENTEDREALITY_API UScriptStruct* Z_Construct_UScriptStruct_FTrackedGeometryGroup();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UARPin_NoRegister();
// End Cross Module References
class UScriptStruct* FARCorePointer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern GOOGLEARCOREBASE_API uint32 Get_Z_Construct_UScriptStruct_FARCorePointer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FARCorePointer, Z_Construct_UPackage__Script_GoogleARCoreBase(), TEXT("ARCorePointer"), sizeof(FARCorePointer), Get_Z_Construct_UScriptStruct_FARCorePointer_Hash());
	}
	return Singleton;
}
template<> GOOGLEARCOREBASE_API UScriptStruct* StaticStruct<FARCorePointer>()
{
	return FARCorePointer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FARCorePointer(FARCorePointer::StaticStruct, TEXT("/Script/GoogleARCoreBase"), TEXT("ARCorePointer"), false, nullptr, nullptr);
static struct FScriptStruct_GoogleARCoreBase_StaticRegisterNativesFARCorePointer
{
	FScriptStruct_GoogleARCoreBase_StaticRegisterNativesFARCorePointer()
	{
		UScriptStruct::DeferCppStructOps<FARCorePointer>(FName(TEXT("ARCorePointer")));
	}
} ScriptStruct_GoogleARCoreBase_StaticRegisterNativesFARCorePointer;
	struct Z_Construct_UScriptStruct_FARCorePointer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FARCorePointer_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// A wrapper class that stores a native pointer internally, which can be used as the key type for TMap\n" },
		{ "ModuleRelativePath", "Private/GoogleARCoreAPI.h" },
		{ "ToolTip", "A wrapper class that stores a native pointer internally, which can be used as the key type for TMap" },
	};
#endif
	void* Z_Construct_UScriptStruct_FARCorePointer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FARCorePointer>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FARCorePointer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreBase,
		nullptr,
		&NewStructOps,
		"ARCorePointer",
		sizeof(FARCorePointer),
		alignof(FARCorePointer),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FARCorePointer_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FARCorePointer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FARCorePointer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FARCorePointer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_GoogleARCoreBase();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ARCorePointer"), sizeof(FARCorePointer), Get_Z_Construct_UScriptStruct_FARCorePointer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FARCorePointer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FARCorePointer_Hash() { return 3716824122U; }
	void UGoogleARCoreUObjectManager::StaticRegisterNativesUGoogleARCoreUObjectManager()
	{
	}
	UClass* Z_Construct_UClass_UGoogleARCoreUObjectManager_NoRegister()
	{
		return UGoogleARCoreUObjectManager::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LatestPointCloud_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LatestPointCloud;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TrackableHandleMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TrackableHandleMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackableHandleMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_TrackableHandleMap;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HandleToAnchorMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandleToAnchorMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandleToAnchorMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_HandleToAnchorMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreBase,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GoogleARCoreAPI.h" },
		{ "ModuleRelativePath", "Private/GoogleARCoreAPI.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_LatestPointCloud_MetaData[] = {
		{ "ModuleRelativePath", "Private/GoogleARCoreAPI.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_LatestPointCloud = { "LatestPointCloud", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleARCoreUObjectManager, LatestPointCloud), Z_Construct_UClass_UGoogleARCorePointCloud_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_LatestPointCloud_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_LatestPointCloud_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_TrackableHandleMap_ValueProp = { "TrackableHandleMap", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FTrackedGeometryGroup, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_TrackableHandleMap_Key_KeyProp = { "TrackableHandleMap_Key", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FARCorePointer, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_TrackableHandleMap_MetaData[] = {
		{ "Comment", "// pointer type is ArTrackable*\n" },
		{ "ModuleRelativePath", "Private/GoogleARCoreAPI.h" },
		{ "ToolTip", "pointer type is ArTrackable*" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_TrackableHandleMap = { "TrackableHandleMap", nullptr, (EPropertyFlags)0x0010008000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleARCoreUObjectManager, TrackableHandleMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_TrackableHandleMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_TrackableHandleMap_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_HandleToAnchorMap_ValueProp = { "HandleToAnchorMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UARPin_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_HandleToAnchorMap_Key_KeyProp = { "HandleToAnchorMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FARCorePointer, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_HandleToAnchorMap_MetaData[] = {
		{ "Comment", "// pointer type is ArAnchor*\n" },
		{ "ModuleRelativePath", "Private/GoogleARCoreAPI.h" },
		{ "ToolTip", "pointer type is ArAnchor*" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_HandleToAnchorMap = { "HandleToAnchorMap", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleARCoreUObjectManager, HandleToAnchorMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_HandleToAnchorMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_HandleToAnchorMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_LatestPointCloud,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_TrackableHandleMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_TrackableHandleMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_TrackableHandleMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_HandleToAnchorMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_HandleToAnchorMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::NewProp_HandleToAnchorMap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleARCoreUObjectManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::ClassParams = {
		&UGoogleARCoreUObjectManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleARCoreUObjectManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleARCoreUObjectManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleARCoreUObjectManager, 389507196);
	template<> GOOGLEARCOREBASE_API UClass* StaticClass<UGoogleARCoreUObjectManager>()
	{
		return UGoogleARCoreUObjectManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleARCoreUObjectManager(Z_Construct_UClass_UGoogleARCoreUObjectManager, &UGoogleARCoreUObjectManager::StaticClass, TEXT("/Script/GoogleARCoreBase"), TEXT("UGoogleARCoreUObjectManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleARCoreUObjectManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
