// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleARCoreBase/Private/GoogleARCoreDependencyHandler.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleARCoreDependencyHandler() {}
// Cross Module References
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UGoogleARCoreDependencyHandler_NoRegister();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UGoogleARCoreDependencyHandler();
	AUGMENTEDREALITY_API UClass* Z_Construct_UClass_UARDependencyHandler();
	UPackage* Z_Construct_UPackage__Script_GoogleARCoreBase();
// End Cross Module References
	void UGoogleARCoreDependencyHandler::StaticRegisterNativesUGoogleARCoreDependencyHandler()
	{
	}
	UClass* Z_Construct_UClass_UGoogleARCoreDependencyHandler_NoRegister()
	{
		return UGoogleARCoreDependencyHandler::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleARCoreDependencyHandler_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleARCoreDependencyHandler_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UARDependencyHandler,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreBase,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreDependencyHandler_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GoogleARCoreDependencyHandler.h" },
		{ "ModuleRelativePath", "Private/GoogleARCoreDependencyHandler.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleARCoreDependencyHandler_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleARCoreDependencyHandler>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleARCoreDependencyHandler_Statics::ClassParams = {
		&UGoogleARCoreDependencyHandler::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreDependencyHandler_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreDependencyHandler_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleARCoreDependencyHandler()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleARCoreDependencyHandler_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleARCoreDependencyHandler, 3192776645);
	template<> GOOGLEARCOREBASE_API UClass* StaticClass<UGoogleARCoreDependencyHandler>()
	{
		return UGoogleARCoreDependencyHandler::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleARCoreDependencyHandler(Z_Construct_UClass_UGoogleARCoreDependencyHandler, &UGoogleARCoreDependencyHandler::StaticClass, TEXT("/Script/GoogleARCoreBase"), TEXT("UGoogleARCoreDependencyHandler"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleARCoreDependencyHandler);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
