// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleARCoreRendering/Public/GoogleARCorePassthroughCameraRenderer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleARCorePassthroughCameraRenderer() {}
// Cross Module References
	GOOGLEARCORERENDERING_API UClass* Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_NoRegister();
	GOOGLEARCORERENDERING_API UClass* Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_GoogleARCoreRendering();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void UGoogleARCoreCameraOverlayMaterialLoader::StaticRegisterNativesUGoogleARCoreCameraOverlayMaterialLoader()
	{
	}
	UClass* Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_NoRegister()
	{
		return UGoogleARCoreCameraOverlayMaterialLoader::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegularOverlayMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RegularOverlayMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugOverlayMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DebugOverlayMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DepthOcclusionMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DepthOcclusionMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DepthColorationMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DepthColorationMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreRendering,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A helper class that is used to load the GoogleARCorePassthroughCameraMaterial from its default object. */" },
		{ "IncludePath", "GoogleARCorePassthroughCameraRenderer.h" },
		{ "ModuleRelativePath", "Public/GoogleARCorePassthroughCameraRenderer.h" },
		{ "ToolTip", "A helper class that is used to load the GoogleARCorePassthroughCameraMaterial from its default object." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_RegularOverlayMaterial_MetaData[] = {
		{ "Comment", "/** A pointer to the camera overlay material that will be used to render the passthrough camera texture as background. */" },
		{ "ModuleRelativePath", "Public/GoogleARCorePassthroughCameraRenderer.h" },
		{ "ToolTip", "A pointer to the camera overlay material that will be used to render the passthrough camera texture as background." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_RegularOverlayMaterial = { "RegularOverlayMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleARCoreCameraOverlayMaterialLoader, RegularOverlayMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_RegularOverlayMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_RegularOverlayMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DebugOverlayMaterial_MetaData[] = {
		{ "Comment", "/** A pointer to the camera overlay material that will be used to render the passthrough camera texture as background. */" },
		{ "ModuleRelativePath", "Public/GoogleARCorePassthroughCameraRenderer.h" },
		{ "ToolTip", "A pointer to the camera overlay material that will be used to render the passthrough camera texture as background." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DebugOverlayMaterial = { "DebugOverlayMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleARCoreCameraOverlayMaterialLoader, DebugOverlayMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DebugOverlayMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DebugOverlayMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DepthOcclusionMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/GoogleARCorePassthroughCameraRenderer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DepthOcclusionMaterial = { "DepthOcclusionMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleARCoreCameraOverlayMaterialLoader, DepthOcclusionMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DepthOcclusionMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DepthOcclusionMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DepthColorationMaterial_MetaData[] = {
		{ "Comment", "/** Material used for rendering the coloration of the depth map. */" },
		{ "ModuleRelativePath", "Public/GoogleARCorePassthroughCameraRenderer.h" },
		{ "ToolTip", "Material used for rendering the coloration of the depth map." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DepthColorationMaterial = { "DepthColorationMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleARCoreCameraOverlayMaterialLoader, DepthColorationMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DepthColorationMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DepthColorationMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_RegularOverlayMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DebugOverlayMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DepthOcclusionMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::NewProp_DepthColorationMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleARCoreCameraOverlayMaterialLoader>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::ClassParams = {
		&UGoogleARCoreCameraOverlayMaterialLoader::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleARCoreCameraOverlayMaterialLoader, 451407504);
	template<> GOOGLEARCORERENDERING_API UClass* StaticClass<UGoogleARCoreCameraOverlayMaterialLoader>()
	{
		return UGoogleARCoreCameraOverlayMaterialLoader::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleARCoreCameraOverlayMaterialLoader(Z_Construct_UClass_UGoogleARCoreCameraOverlayMaterialLoader, &UGoogleARCoreCameraOverlayMaterialLoader::StaticClass, TEXT("/Script/GoogleARCoreRendering"), TEXT("UGoogleARCoreCameraOverlayMaterialLoader"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleARCoreCameraOverlayMaterialLoader);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
