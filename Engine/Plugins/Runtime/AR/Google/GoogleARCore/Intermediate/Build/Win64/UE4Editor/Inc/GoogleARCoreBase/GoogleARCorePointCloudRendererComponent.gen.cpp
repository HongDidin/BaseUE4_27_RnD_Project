// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleARCoreBase/Public/GoogleARCorePointCloudRendererComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleARCorePointCloudRendererComponent() {}
// Cross Module References
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_NoRegister();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_GoogleARCoreBase();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
// End Cross Module References
	void UDEPRECATED_GoogleARCorePointCloudRendererComponent::StaticRegisterNativesUDEPRECATED_GoogleARCorePointCloudRendererComponent()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_NoRegister()
	{
		return UDEPRECATED_GoogleARCorePointCloudRendererComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PointColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PointSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreBase,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "GoogleARCore" },
		{ "Comment", "/**\n * A helper component that renders the latest point cloud from the ARCore tracking session.\n * NOTE: This class is now deprecated, use UPointCloudComponent from the \"PointCloud\" plugin.\n */" },
		{ "DevelopmentStatus", "Experimental" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "GoogleARCorePointCloudRendererComponent.h" },
		{ "ModuleRelativePath", "Public/GoogleARCorePointCloudRendererComponent.h" },
		{ "ToolTip", "A helper component that renders the latest point cloud from the ARCore tracking session.\nNOTE: This class is now deprecated, use UPointCloudComponent from the \"PointCloud\" plugin." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::NewProp_PointColor_MetaData[] = {
		{ "Category", "GoogleARCore|PointCloudRenderer" },
		{ "Comment", "/** The color of the point. */" },
		{ "ModuleRelativePath", "Public/GoogleARCorePointCloudRendererComponent.h" },
		{ "ToolTip", "The color of the point." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::NewProp_PointColor = { "PointColor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_GoogleARCorePointCloudRendererComponent, PointColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::NewProp_PointColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::NewProp_PointColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::NewProp_PointSize_MetaData[] = {
		{ "Category", "GoogleARCore|PointCloudRenderer" },
		{ "Comment", "/** The size of the point. */" },
		{ "ModuleRelativePath", "Public/GoogleARCorePointCloudRendererComponent.h" },
		{ "ToolTip", "The size of the point." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::NewProp_PointSize = { "PointSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_GoogleARCorePointCloudRendererComponent, PointSize), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::NewProp_PointSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::NewProp_PointSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::NewProp_PointColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::NewProp_PointSize,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_GoogleARCorePointCloudRendererComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::ClassParams = {
		&UDEPRECATED_GoogleARCorePointCloudRendererComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::PropPointers),
		0,
		0x02B002A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_GoogleARCorePointCloudRendererComponent, 2003919054);
	template<> GOOGLEARCOREBASE_API UClass* StaticClass<UDEPRECATED_GoogleARCorePointCloudRendererComponent>()
	{
		return UDEPRECATED_GoogleARCorePointCloudRendererComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent(Z_Construct_UClass_UDEPRECATED_GoogleARCorePointCloudRendererComponent, &UDEPRECATED_GoogleARCorePointCloudRendererComponent::StaticClass, TEXT("/Script/GoogleARCoreBase"), TEXT("UDEPRECATED_GoogleARCorePointCloudRendererComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_GoogleARCorePointCloudRendererComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
