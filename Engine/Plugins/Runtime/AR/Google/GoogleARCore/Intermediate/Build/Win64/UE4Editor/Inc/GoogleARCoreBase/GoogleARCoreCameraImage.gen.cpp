// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleARCoreBase/Public/GoogleARCoreCameraImage.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleARCoreCameraImage() {}
// Cross Module References
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UGoogleARCoreCameraImage_NoRegister();
	GOOGLEARCOREBASE_API UClass* Z_Construct_UClass_UGoogleARCoreCameraImage();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_GoogleARCoreBase();
// End Cross Module References
	DEFINE_FUNCTION(UGoogleARCoreCameraImage::execGetPlaneCount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetPlaneCount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleARCoreCameraImage::execGetHeight)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetHeight();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleARCoreCameraImage::execGetWidth)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetWidth();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoogleARCoreCameraImage::execRelease)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Release();
		P_NATIVE_END;
	}
	void UGoogleARCoreCameraImage::StaticRegisterNativesUGoogleARCoreCameraImage()
	{
		UClass* Class = UGoogleARCoreCameraImage::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetHeight", &UGoogleARCoreCameraImage::execGetHeight },
			{ "GetPlaneCount", &UGoogleARCoreCameraImage::execGetPlaneCount },
			{ "GetWidth", &UGoogleARCoreCameraImage::execGetWidth },
			{ "Release", &UGoogleARCoreCameraImage::execRelease },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics
	{
		struct GoogleARCoreCameraImage_eventGetHeight_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleARCoreCameraImage_eventGetHeight_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleARCore|CameraImage" },
		{ "Comment", "/**\n\x09 * Get the height of the image in pixels.\n\x09 *\n\x09 * @return The height.\n\x09 */" },
		{ "Keywords", "googlear arcore cameraimage" },
		{ "ModuleRelativePath", "Public/GoogleARCoreCameraImage.h" },
		{ "ToolTip", "Get the height of the image in pixels.\n\n@return The height." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleARCoreCameraImage, nullptr, "GetHeight", nullptr, nullptr, sizeof(GoogleARCoreCameraImage_eventGetHeight_Parms), Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics
	{
		struct GoogleARCoreCameraImage_eventGetPlaneCount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleARCoreCameraImage_eventGetPlaneCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleARCore|CameraImage" },
		{ "Comment", "/**\n\x09 * Get the number of data planes in the image.\n\x09 *\n\x09 * @return The plane count.\n\x09 */" },
		{ "Keywords", "googlear arcore cameraimage" },
		{ "ModuleRelativePath", "Public/GoogleARCoreCameraImage.h" },
		{ "ToolTip", "Get the number of data planes in the image.\n\n@return The plane count." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleARCoreCameraImage, nullptr, "GetPlaneCount", nullptr, nullptr, sizeof(GoogleARCoreCameraImage_eventGetPlaneCount_Parms), Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics
	{
		struct GoogleARCoreCameraImage_eventGetWidth_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoogleARCoreCameraImage_eventGetWidth_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleARCore|CameraImage" },
		{ "Comment", "/**\n\x09 * Get the width of the image in pixels.\n\x09 *\n\x09 * @return The width.\n\x09 */" },
		{ "Keywords", "googlear arcore cameraimage" },
		{ "ModuleRelativePath", "Public/GoogleARCoreCameraImage.h" },
		{ "ToolTip", "Get the width of the image in pixels.\n\n@return The width." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleARCoreCameraImage, nullptr, "GetWidth", nullptr, nullptr, sizeof(GoogleARCoreCameraImage_eventGetWidth_Parms), Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoogleARCoreCameraImage_Release_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoogleARCoreCameraImage_Release_Statics::Function_MetaDataParams[] = {
		{ "Category", "GoogleARCore|CameraImage" },
		{ "Comment", "/**\n\x09 * Explicitly release the ARCore resources owned by this object.\n\x09 */" },
		{ "Keywords", "googlear arcore cameraimage" },
		{ "ModuleRelativePath", "Public/GoogleARCoreCameraImage.h" },
		{ "ToolTip", "Explicitly release the ARCore resources owned by this object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoogleARCoreCameraImage_Release_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoogleARCoreCameraImage, nullptr, "Release", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoogleARCoreCameraImage_Release_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoogleARCoreCameraImage_Release_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoogleARCoreCameraImage_Release()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGoogleARCoreCameraImage_Release_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGoogleARCoreCameraImage_NoRegister()
	{
		return UGoogleARCoreCameraImage::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleARCoreCameraImage_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleARCoreCameraImage_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreBase,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGoogleARCoreCameraImage_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGoogleARCoreCameraImage_GetHeight, "GetHeight" }, // 1319258528
		{ &Z_Construct_UFunction_UGoogleARCoreCameraImage_GetPlaneCount, "GetPlaneCount" }, // 865940659
		{ &Z_Construct_UFunction_UGoogleARCoreCameraImage_GetWidth, "GetWidth" }, // 2541546663
		{ &Z_Construct_UFunction_UGoogleARCoreCameraImage_Release, "Release" }, // 516728257
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreCameraImage_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * An object that represents an acquired CPU-accessible camera image.\n */" },
		{ "IncludePath", "GoogleARCoreCameraImage.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/GoogleARCoreCameraImage.h" },
		{ "ToolTip", "An object that represents an acquired CPU-accessible camera image." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleARCoreCameraImage_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleARCoreCameraImage>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleARCoreCameraImage_Statics::ClassParams = {
		&UGoogleARCoreCameraImage::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreCameraImage_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreCameraImage_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleARCoreCameraImage()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleARCoreCameraImage_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleARCoreCameraImage, 1184117054);
	template<> GOOGLEARCOREBASE_API UClass* StaticClass<UGoogleARCoreCameraImage>()
	{
		return UGoogleARCoreCameraImage::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleARCoreCameraImage(Z_Construct_UClass_UGoogleARCoreCameraImage, &UGoogleARCoreCameraImage::StaticClass, TEXT("/Script/GoogleARCoreBase"), TEXT("UGoogleARCoreCameraImage"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleARCoreCameraImage);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
