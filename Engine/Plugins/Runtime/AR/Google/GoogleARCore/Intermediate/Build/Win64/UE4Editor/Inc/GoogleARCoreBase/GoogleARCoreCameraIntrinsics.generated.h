// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEARCOREBASE_GoogleARCoreCameraIntrinsics_generated_h
#error "GoogleARCoreCameraIntrinsics.generated.h already included, missing '#pragma once' in GoogleARCoreCameraIntrinsics.h"
#endif
#define GOOGLEARCOREBASE_GoogleARCoreCameraIntrinsics_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetImageDimensions); \
	DECLARE_FUNCTION(execGetPrincipalPoint); \
	DECLARE_FUNCTION(execGetFocalLength);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetImageDimensions); \
	DECLARE_FUNCTION(execGetPrincipalPoint); \
	DECLARE_FUNCTION(execGetFocalLength);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleARCoreCameraIntrinsics(); \
	friend struct Z_Construct_UClass_UGoogleARCoreCameraIntrinsics_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreCameraIntrinsics, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreCameraIntrinsics)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleARCoreCameraIntrinsics(); \
	friend struct Z_Construct_UClass_UGoogleARCoreCameraIntrinsics_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreCameraIntrinsics, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreBase"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreCameraIntrinsics)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreCameraIntrinsics(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleARCoreCameraIntrinsics) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreCameraIntrinsics); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreCameraIntrinsics); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreCameraIntrinsics(UGoogleARCoreCameraIntrinsics&&); \
	NO_API UGoogleARCoreCameraIntrinsics(const UGoogleARCoreCameraIntrinsics&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreCameraIntrinsics(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreCameraIntrinsics(UGoogleARCoreCameraIntrinsics&&); \
	NO_API UGoogleARCoreCameraIntrinsics(const UGoogleARCoreCameraIntrinsics&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreCameraIntrinsics); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreCameraIntrinsics); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleARCoreCameraIntrinsics)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_20_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCOREBASE_API UClass* StaticClass<class UGoogleARCoreCameraIntrinsics>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreBase_Public_GoogleARCoreCameraIntrinsics_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
