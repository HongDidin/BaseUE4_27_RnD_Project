// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEARCORERENDERING_MaterialExpressionGoogleARCorePassthroughCamera_generated_h
#error "MaterialExpressionGoogleARCorePassthroughCamera.generated.h already included, missing '#pragma once' in MaterialExpressionGoogleARCorePassthroughCamera.h"
#endif
#define GOOGLEARCORERENDERING_MaterialExpressionGoogleARCorePassthroughCamera_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera(); \
	friend struct Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera, UMaterialExpression, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/GoogleARCoreRendering"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera(); \
	friend struct Z_Construct_UClass_UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera, UMaterialExpression, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/GoogleARCoreRendering"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera&&); \
	NO_API UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera(const UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera&&); \
	NO_API UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera(const UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_14_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MaterialExpressionGoogleARCorePassthroughCamera."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCORERENDERING_API UClass* StaticClass<class UDEPRECATED_MaterialExpressionGoogleARCorePassthroughCamera>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCore_Source_GoogleARCoreRendering_Private_MaterialExpressionGoogleARCorePassthroughCamera_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
