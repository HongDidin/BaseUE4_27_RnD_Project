// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCloudARPin;
enum class EARPinCloudTaskResult : uint8;
class UARPin;
class UObject;
struct FLatentActionInfo;
struct FGoogleARCoreServicesConfig;
#ifdef GOOGLEARCORESERVICES_GoogleARCoreServicesFunctionLibrary_generated_h
#error "GoogleARCoreServicesFunctionLibrary.generated.h already included, missing '#pragma once' in GoogleARCoreServicesFunctionLibrary.h"
#endif
#define GOOGLEARCORESERVICES_GoogleARCoreServicesFunctionLibrary_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAllCloudARPin); \
	DECLARE_FUNCTION(execRemoveCloudARPin); \
	DECLARE_FUNCTION(execCreateAndResolveCloudARPin); \
	DECLARE_FUNCTION(execCreateAndHostCloudARPin); \
	DECLARE_FUNCTION(execCreateAndResolveCloudARPinLatentAction); \
	DECLARE_FUNCTION(execCreateAndHostCloudARPinLatentAction); \
	DECLARE_FUNCTION(execConfigGoogleARCoreServices);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAllCloudARPin); \
	DECLARE_FUNCTION(execRemoveCloudARPin); \
	DECLARE_FUNCTION(execCreateAndResolveCloudARPin); \
	DECLARE_FUNCTION(execCreateAndHostCloudARPin); \
	DECLARE_FUNCTION(execCreateAndResolveCloudARPinLatentAction); \
	DECLARE_FUNCTION(execCreateAndHostCloudARPinLatentAction); \
	DECLARE_FUNCTION(execConfigGoogleARCoreServices);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleARCoreServicesFunctionLibrary(); \
	friend struct Z_Construct_UClass_UGoogleARCoreServicesFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreServicesFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreServices"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreServicesFunctionLibrary)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleARCoreServicesFunctionLibrary(); \
	friend struct Z_Construct_UClass_UGoogleARCoreServicesFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreServicesFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreServices"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreServicesFunctionLibrary)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreServicesFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleARCoreServicesFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreServicesFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreServicesFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreServicesFunctionLibrary(UGoogleARCoreServicesFunctionLibrary&&); \
	NO_API UGoogleARCoreServicesFunctionLibrary(const UGoogleARCoreServicesFunctionLibrary&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreServicesFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreServicesFunctionLibrary(UGoogleARCoreServicesFunctionLibrary&&); \
	NO_API UGoogleARCoreServicesFunctionLibrary(const UGoogleARCoreServicesFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreServicesFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreServicesFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleARCoreServicesFunctionLibrary)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_14_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCORESERVICES_API UClass* StaticClass<class UGoogleARCoreServicesFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
