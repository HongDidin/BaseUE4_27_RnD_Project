// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GoogleARCoreServices/Private/GoogleARCoreServicesEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoogleARCoreServicesEditorSettings() {}
// Cross Module References
	GOOGLEARCORESERVICES_API UClass* Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_NoRegister();
	GOOGLEARCORESERVICES_API UClass* Z_Construct_UClass_UGoogleARCoreServicesEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_GoogleARCoreServices();
// End Cross Module References
	void UGoogleARCoreServicesEditorSettings::StaticRegisterNativesUGoogleARCoreServicesEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_NoRegister()
	{
		return UGoogleARCoreServicesEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AndroidAPIKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AndroidAPIKey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IOSAPIKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IOSAPIKey;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GoogleARCoreServices,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Helper class used to expose GoogleARCoreServices setting in the Editor plugin settings.\n*/" },
		{ "IncludePath", "GoogleARCoreServicesEditorSettings.h" },
		{ "ModuleRelativePath", "Private/GoogleARCoreServicesEditorSettings.h" },
		{ "ToolTip", "Helper class used to expose GoogleARCoreServices setting in the Editor plugin settings." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::NewProp_AndroidAPIKey_MetaData[] = {
		{ "Category", "ARCore Services Plugin Settings" },
		{ "Comment", "/** The API key for GoogleARCoreServices on Android platform. */" },
		{ "ModuleRelativePath", "Private/GoogleARCoreServicesEditorSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "The API key for GoogleARCoreServices on Android platform." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::NewProp_AndroidAPIKey = { "AndroidAPIKey", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleARCoreServicesEditorSettings, AndroidAPIKey), METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::NewProp_AndroidAPIKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::NewProp_AndroidAPIKey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::NewProp_IOSAPIKey_MetaData[] = {
		{ "Category", "ARCore Services Plugin Settings" },
		{ "Comment", "/** The API key for GoogleARCoreServices on iOS platform. */" },
		{ "ModuleRelativePath", "Private/GoogleARCoreServicesEditorSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "The API key for GoogleARCoreServices on iOS platform." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::NewProp_IOSAPIKey = { "IOSAPIKey", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoogleARCoreServicesEditorSettings, IOSAPIKey), METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::NewProp_IOSAPIKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::NewProp_IOSAPIKey_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::NewProp_AndroidAPIKey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::NewProp_IOSAPIKey,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoogleARCoreServicesEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::ClassParams = {
		&UGoogleARCoreServicesEditorSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoogleARCoreServicesEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGoogleARCoreServicesEditorSettings, 1727472349);
	template<> GOOGLEARCORESERVICES_API UClass* StaticClass<UGoogleARCoreServicesEditorSettings>()
	{
		return UGoogleARCoreServicesEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGoogleARCoreServicesEditorSettings(Z_Construct_UClass_UGoogleARCoreServicesEditorSettings, &UGoogleARCoreServicesEditorSettings::StaticClass, TEXT("/Script/GoogleARCoreServices"), TEXT("UGoogleARCoreServicesEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoogleARCoreServicesEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
