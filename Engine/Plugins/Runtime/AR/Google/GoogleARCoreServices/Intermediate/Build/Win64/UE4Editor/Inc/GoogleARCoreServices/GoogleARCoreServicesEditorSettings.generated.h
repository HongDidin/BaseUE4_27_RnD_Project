// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GOOGLEARCORESERVICES_GoogleARCoreServicesEditorSettings_generated_h
#error "GoogleARCoreServicesEditorSettings.generated.h already included, missing '#pragma once' in GoogleARCoreServicesEditorSettings.h"
#endif
#define GOOGLEARCORESERVICES_GoogleARCoreServicesEditorSettings_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoogleARCoreServicesEditorSettings(); \
	friend struct Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreServicesEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleARCoreServices"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreServicesEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUGoogleARCoreServicesEditorSettings(); \
	friend struct Z_Construct_UClass_UGoogleARCoreServicesEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UGoogleARCoreServicesEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/GoogleARCoreServices"), NO_API) \
	DECLARE_SERIALIZER(UGoogleARCoreServicesEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreServicesEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleARCoreServicesEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreServicesEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreServicesEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreServicesEditorSettings(UGoogleARCoreServicesEditorSettings&&); \
	NO_API UGoogleARCoreServicesEditorSettings(const UGoogleARCoreServicesEditorSettings&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoogleARCoreServicesEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoogleARCoreServicesEditorSettings(UGoogleARCoreServicesEditorSettings&&); \
	NO_API UGoogleARCoreServicesEditorSettings(const UGoogleARCoreServicesEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoogleARCoreServicesEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoogleARCoreServicesEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoogleARCoreServicesEditorSettings)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_9_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCORESERVICES_API UClass* StaticClass<class UGoogleARCoreServicesEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Private_GoogleARCoreServicesEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
