// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class ECloudARPinCloudState : uint8;
#ifdef GOOGLEARCORESERVICES_GoogleARCoreServicesTypes_generated_h
#error "GoogleARCoreServicesTypes.generated.h already included, missing '#pragma once' in GoogleARCoreServicesTypes.h"
#endif
#define GOOGLEARCORESERVICES_GoogleARCoreServicesTypes_generated_h

#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_35_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGoogleARCoreServicesConfig_Statics; \
	GOOGLEARCORESERVICES_API static class UScriptStruct* StaticStruct();


template<> GOOGLEARCORESERVICES_API UScriptStruct* StaticStruct<struct FGoogleARCoreServicesConfig>();

#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_SPARSE_DATA
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetARPinCloudState); \
	DECLARE_FUNCTION(execGetCloudID);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetARPinCloudState); \
	DECLARE_FUNCTION(execGetCloudID);


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCloudARPin(); \
	friend struct Z_Construct_UClass_UCloudARPin_Statics; \
public: \
	DECLARE_CLASS(UCloudARPin, UARPin, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreServices"), NO_API) \
	DECLARE_SERIALIZER(UCloudARPin)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_INCLASS \
private: \
	static void StaticRegisterNativesUCloudARPin(); \
	friend struct Z_Construct_UClass_UCloudARPin_Statics; \
public: \
	DECLARE_CLASS(UCloudARPin, UARPin, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GoogleARCoreServices"), NO_API) \
	DECLARE_SERIALIZER(UCloudARPin)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCloudARPin(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCloudARPin) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCloudARPin); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCloudARPin); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCloudARPin(UCloudARPin&&); \
	NO_API UCloudARPin(const UCloudARPin&); \
public:


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCloudARPin(UCloudARPin&&); \
	NO_API UCloudARPin(const UCloudARPin&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCloudARPin); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCloudARPin); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCloudARPin)


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_143_PROLOG
#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_INCLASS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_SPARSE_DATA \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h_146_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GOOGLEARCORESERVICES_API UClass* StaticClass<class UCloudARPin>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AR_Google_GoogleARCoreServices_Source_GoogleARCoreServices_Public_GoogleARCoreServicesTypes_h


#define FOREACH_ENUM_ECLOUDARPINCLOUDSTATE(op) \
	op(ECloudARPinCloudState::NotHosted) \
	op(ECloudARPinCloudState::InProgress) \
	op(ECloudARPinCloudState::Success) \
	op(ECloudARPinCloudState::ErrorInternalError) \
	op(ECloudARPinCloudState::ErrorNotAuthorized) \
	op(ECloudARPinCloudState::ErrorLocalizationFailure) \
	op(ECloudARPinCloudState::ErrorServiceUnavailable) \
	op(ECloudARPinCloudState::ErrorResourceExhausted) \
	op(ECloudARPinCloudState::ErrorHostingDatasetProcessingFailed) \
	op(ECloudARPinCloudState::ErrorResolvingCloudIDNotFound) \
	op(ECloudARPinCloudState::ErrorSDKVersionTooOld) \
	op(ECloudARPinCloudState::ErrorSDKVersionTooNew) 

enum class ECloudARPinCloudState : uint8;
template<> GOOGLEARCORESERVICES_API UEnum* StaticEnum<ECloudARPinCloudState>();

#define FOREACH_ENUM_EARPINCLOUDTASKRESULT(op) \
	op(EARPinCloudTaskResult::Success) \
	op(EARPinCloudTaskResult::Failed) \
	op(EARPinCloudTaskResult::Started) \
	op(EARPinCloudTaskResult::CloudARPinNotEnabled) \
	op(EARPinCloudTaskResult::NotTracking) \
	op(EARPinCloudTaskResult::SessionPaused) \
	op(EARPinCloudTaskResult::InvalidPin) \
	op(EARPinCloudTaskResult::ResourceExhausted) 

enum class EARPinCloudTaskResult : uint8;
template<> GOOGLEARCORESERVICES_API UEnum* StaticEnum<EARPinCloudTaskResult>();

#define FOREACH_ENUM_EARPINCLOUDMODE(op) \
	op(EARPinCloudMode::Disabled) \
	op(EARPinCloudMode::Enabled) 

enum class EARPinCloudMode : uint8;
template<> GOOGLEARCORESERVICES_API UEnum* StaticEnum<EARPinCloudMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
