// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OSC/Public/OSCBundle.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOSCBundle() {}
// Cross Module References
	OSC_API UScriptStruct* Z_Construct_UScriptStruct_FOSCBundle();
	UPackage* Z_Construct_UPackage__Script_OSC();
// End Cross Module References
class UScriptStruct* FOSCBundle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OSC_API uint32 Get_Z_Construct_UScriptStruct_FOSCBundle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOSCBundle, Z_Construct_UPackage__Script_OSC(), TEXT("OSCBundle"), sizeof(FOSCBundle), Get_Z_Construct_UScriptStruct_FOSCBundle_Hash());
	}
	return Singleton;
}
template<> OSC_API UScriptStruct* StaticStruct<FOSCBundle>()
{
	return FOSCBundle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOSCBundle(FOSCBundle::StaticStruct, TEXT("/Script/OSC"), TEXT("OSCBundle"), false, nullptr, nullptr);
static struct FScriptStruct_OSC_StaticRegisterNativesFOSCBundle
{
	FScriptStruct_OSC_StaticRegisterNativesFOSCBundle()
	{
		UScriptStruct::DeferCppStructOps<FOSCBundle>(FName(TEXT("OSCBundle")));
	}
} ScriptStruct_OSC_StaticRegisterNativesFOSCBundle;
	struct Z_Construct_UScriptStruct_FOSCBundle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOSCBundle_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/OSCBundle.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOSCBundle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOSCBundle>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOSCBundle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OSC,
		nullptr,
		&NewStructOps,
		"OSCBundle",
		sizeof(FOSCBundle),
		alignof(FOSCBundle),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOSCBundle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOSCBundle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOSCBundle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOSCBundle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OSC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OSCBundle"), sizeof(FOSCBundle), Get_Z_Construct_UScriptStruct_FOSCBundle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOSCBundle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOSCBundle_Hash() { return 1751480170U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
