// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OSC_OSCAddress_generated_h
#error "OSCAddress.generated.h already included, missing '#pragma once' in OSCAddress.h"
#endif
#define OSC_OSCAddress_generated_h

#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCAddress_h_22_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOSCAddress_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Containers() { return STRUCT_OFFSET(FOSCAddress, Containers); } \
	FORCEINLINE static uint32 __PPO__Method() { return STRUCT_OFFSET(FOSCAddress, Method); }


template<> OSC_API UScriptStruct* StaticStruct<struct FOSCAddress>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCAddress_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
