// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FOSCAddress;
struct FOSCMessage;
struct FOSCBundle;
#ifdef OSC_OSCServer_generated_h
#error "OSCServer.generated.h already included, missing '#pragma once' in OSCServer.h"
#endif
#define OSC_OSCServer_generated_h

#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_27_DELEGATE \
struct _Script_OSC_eventOSCDispatchMessageEventBP_Parms \
{ \
	FOSCAddress AddressPattern; \
	FOSCMessage Message; \
	FString IPAddress; \
	int32 Port; \
}; \
static inline void FOSCDispatchMessageEventBP_DelegateWrapper(const FScriptDelegate& OSCDispatchMessageEventBP, FOSCAddress const& AddressPattern, FOSCMessage const& Message, const FString& IPAddress, int32 Port) \
{ \
	_Script_OSC_eventOSCDispatchMessageEventBP_Parms Parms; \
	Parms.AddressPattern=AddressPattern; \
	Parms.Message=Message; \
	Parms.IPAddress=IPAddress; \
	Parms.Port=Port; \
	OSCDispatchMessageEventBP.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_25_DELEGATE \
struct _Script_OSC_eventOSCReceivedBundleEvent_Parms \
{ \
	FOSCBundle Bundle; \
	FString IPAddress; \
	int32 Port; \
}; \
static inline void FOSCReceivedBundleEvent_DelegateWrapper(const FMulticastScriptDelegate& OSCReceivedBundleEvent, FOSCBundle const& Bundle, const FString& IPAddress, int32 Port) \
{ \
	_Script_OSC_eventOSCReceivedBundleEvent_Parms Parms; \
	Parms.Bundle=Bundle; \
	Parms.IPAddress=IPAddress; \
	Parms.Port=Port; \
	OSCReceivedBundleEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_24_DELEGATE \
struct _Script_OSC_eventOSCDispatchMessageEvent_Parms \
{ \
	FOSCAddress AddressPattern; \
	FOSCMessage Message; \
	FString IPAddress; \
	int32 Port; \
}; \
static inline void FOSCDispatchMessageEvent_DelegateWrapper(const FMulticastScriptDelegate& OSCDispatchMessageEvent, FOSCAddress const& AddressPattern, FOSCMessage const& Message, const FString& IPAddress, int32 Port) \
{ \
	_Script_OSC_eventOSCDispatchMessageEvent_Parms Parms; \
	Parms.AddressPattern=AddressPattern; \
	Parms.Message=Message; \
	Parms.IPAddress=IPAddress; \
	Parms.Port=Port; \
	OSCDispatchMessageEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_22_DELEGATE \
struct _Script_OSC_eventOSCReceivedMessageEvent_Parms \
{ \
	FOSCMessage Message; \
	FString IPAddress; \
	int32 Port; \
}; \
static inline void FOSCReceivedMessageEvent_DelegateWrapper(const FMulticastScriptDelegate& OSCReceivedMessageEvent, FOSCMessage const& Message, const FString& IPAddress, int32 Port) \
{ \
	_Script_OSC_eventOSCReceivedMessageEvent_Parms Parms; \
	Parms.Message=Message; \
	Parms.IPAddress=IPAddress; \
	Parms.Port=Port; \
	OSCReceivedMessageEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_SPARSE_DATA
#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetBoundOSCAddressPatterns); \
	DECLARE_FUNCTION(execUnbindAllEventsFromOnOSCAddressPatternMatching); \
	DECLARE_FUNCTION(execUnbindAllEventsFromOnOSCAddressPatternMatchesPath); \
	DECLARE_FUNCTION(execUnbindEventFromOnOSCAddressPatternMatchesPath); \
	DECLARE_FUNCTION(execBindEventToOnOSCAddressPatternMatchesPath); \
	DECLARE_FUNCTION(execGetWhitelistedClients); \
	DECLARE_FUNCTION(execGetPort); \
	DECLARE_FUNCTION(execGetIpAddress); \
	DECLARE_FUNCTION(execClearWhitelistedClients); \
	DECLARE_FUNCTION(execRemoveWhitelistedClient); \
	DECLARE_FUNCTION(execAddWhitelistedClient); \
	DECLARE_FUNCTION(execSetWhitelistClientsEnabled); \
	DECLARE_FUNCTION(execStop); \
	DECLARE_FUNCTION(execSetMulticastLoopback); \
	DECLARE_FUNCTION(execSetAddress); \
	DECLARE_FUNCTION(execListen); \
	DECLARE_FUNCTION(execIsActive); \
	DECLARE_FUNCTION(execGetMulticastLoopback);


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetBoundOSCAddressPatterns); \
	DECLARE_FUNCTION(execUnbindAllEventsFromOnOSCAddressPatternMatching); \
	DECLARE_FUNCTION(execUnbindAllEventsFromOnOSCAddressPatternMatchesPath); \
	DECLARE_FUNCTION(execUnbindEventFromOnOSCAddressPatternMatchesPath); \
	DECLARE_FUNCTION(execBindEventToOnOSCAddressPatternMatchesPath); \
	DECLARE_FUNCTION(execGetWhitelistedClients); \
	DECLARE_FUNCTION(execGetPort); \
	DECLARE_FUNCTION(execGetIpAddress); \
	DECLARE_FUNCTION(execClearWhitelistedClients); \
	DECLARE_FUNCTION(execRemoveWhitelistedClient); \
	DECLARE_FUNCTION(execAddWhitelistedClient); \
	DECLARE_FUNCTION(execSetWhitelistClientsEnabled); \
	DECLARE_FUNCTION(execStop); \
	DECLARE_FUNCTION(execSetMulticastLoopback); \
	DECLARE_FUNCTION(execSetAddress); \
	DECLARE_FUNCTION(execListen); \
	DECLARE_FUNCTION(execIsActive); \
	DECLARE_FUNCTION(execGetMulticastLoopback);


#if WITH_EDITOR
#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_EDITOR_ONLY_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetTickInEditor);


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetTickInEditor);


#else
#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_EDITOR_ONLY_RPC_WRAPPERS
#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS
#endif //WITH_EDITOR
#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOSCServer(); \
	friend struct Z_Construct_UClass_UOSCServer_Statics; \
public: \
	DECLARE_CLASS(UOSCServer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OSC"), NO_API) \
	DECLARE_SERIALIZER(UOSCServer)


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_INCLASS \
private: \
	static void StaticRegisterNativesUOSCServer(); \
	friend struct Z_Construct_UClass_UOSCServer_Statics; \
public: \
	DECLARE_CLASS(UOSCServer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OSC"), NO_API) \
	DECLARE_SERIALIZER(UOSCServer)


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOSCServer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOSCServer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOSCServer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOSCServer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOSCServer(UOSCServer&&); \
	NO_API UOSCServer(const UOSCServer&); \
public:


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOSCServer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOSCServer(UOSCServer&&); \
	NO_API UOSCServer(const UOSCServer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOSCServer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOSCServer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOSCServer)


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_56_PROLOG
#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_SPARSE_DATA \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_RPC_WRAPPERS \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_EDITOR_ONLY_RPC_WRAPPERS \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_INCLASS \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_SPARSE_DATA \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h_59_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OSCServer."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OSC_API UClass* StaticClass<class UOSCServer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_OSC_Source_OSC_Public_OSCServer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
