// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OSC/Public/OSCAddress.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOSCAddress() {}
// Cross Module References
	OSC_API UScriptStruct* Z_Construct_UScriptStruct_FOSCAddress();
	UPackage* Z_Construct_UPackage__Script_OSC();
// End Cross Module References
class UScriptStruct* FOSCAddress::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OSC_API uint32 Get_Z_Construct_UScriptStruct_FOSCAddress_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOSCAddress, Z_Construct_UPackage__Script_OSC(), TEXT("OSCAddress"), sizeof(FOSCAddress), Get_Z_Construct_UScriptStruct_FOSCAddress_Hash());
	}
	return Singleton;
}
template<> OSC_API UScriptStruct* StaticStruct<FOSCAddress>()
{
	return FOSCAddress::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOSCAddress(FOSCAddress::StaticStruct, TEXT("/Script/OSC"), TEXT("OSCAddress"), false, nullptr, nullptr);
static struct FScriptStruct_OSC_StaticRegisterNativesFOSCAddress
{
	FScriptStruct_OSC_StaticRegisterNativesFOSCAddress()
	{
		UScriptStruct::DeferCppStructOps<FOSCAddress>(FName(TEXT("OSCAddress")));
	}
} ScriptStruct_OSC_StaticRegisterNativesFOSCAddress;
	struct Z_Construct_UScriptStruct_FOSCAddress_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Containers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Containers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Containers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Method_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Method;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOSCAddress_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// namespace OSC\n" },
		{ "ModuleRelativePath", "Public/OSCAddress.h" },
		{ "ToolTip", "namespace OSC" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOSCAddress_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOSCAddress>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Containers_Inner = { "Containers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Containers_MetaData[] = {
		{ "Comment", "// Ordered array of container names\n" },
		{ "ModuleRelativePath", "Public/OSCAddress.h" },
		{ "ToolTip", "Ordered array of container names" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Containers = { "Containers", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOSCAddress, Containers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Containers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Containers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Method_MetaData[] = {
		{ "Comment", "// Method name of string\n" },
		{ "ModuleRelativePath", "Public/OSCAddress.h" },
		{ "ToolTip", "Method name of string" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Method = { "Method", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOSCAddress, Method), METADATA_PARAMS(Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Method_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Method_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOSCAddress_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Containers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Containers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOSCAddress_Statics::NewProp_Method,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOSCAddress_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OSC,
		nullptr,
		&NewStructOps,
		"OSCAddress",
		sizeof(FOSCAddress),
		alignof(FOSCAddress),
		Z_Construct_UScriptStruct_FOSCAddress_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOSCAddress_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOSCAddress_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOSCAddress_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOSCAddress()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOSCAddress_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OSC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OSCAddress"), sizeof(FOSCAddress), Get_Z_Construct_UScriptStruct_FOSCAddress_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOSCAddress_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOSCAddress_Hash() { return 3657102923U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
