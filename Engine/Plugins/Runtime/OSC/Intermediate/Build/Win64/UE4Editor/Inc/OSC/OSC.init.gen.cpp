// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOSC_init() {}
	OSC_API UFunction* Z_Construct_UDelegateFunction_OSC_OSCReceivedMessageEvent__DelegateSignature();
	OSC_API UFunction* Z_Construct_UDelegateFunction_OSC_OSCDispatchMessageEvent__DelegateSignature();
	OSC_API UFunction* Z_Construct_UDelegateFunction_OSC_OSCReceivedBundleEvent__DelegateSignature();
	OSC_API UFunction* Z_Construct_UDelegateFunction_OSC_OSCDispatchMessageEventBP__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_OSC()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_OSC_OSCReceivedMessageEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_OSC_OSCDispatchMessageEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_OSC_OSCReceivedBundleEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_OSC_OSCDispatchMessageEventBP__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/OSC",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x3B3FE450,
				0x24B64018,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
