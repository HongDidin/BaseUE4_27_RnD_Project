// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WEBBROWSERWIDGET_WebBrowser_generated_h
#error "WebBrowser.generated.h already included, missing '#pragma once' in WebBrowser.h"
#endif
#define WEBBROWSERWIDGET_WebBrowser_generated_h

#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_20_DELEGATE \
struct WebBrowser_eventOnBeforePopup_Parms \
{ \
	FString URL; \
	FString Frame; \
}; \
static inline void FOnBeforePopup_DelegateWrapper(const FMulticastScriptDelegate& OnBeforePopup, const FString& URL, const FString& Frame) \
{ \
	WebBrowser_eventOnBeforePopup_Parms Parms; \
	Parms.URL=URL; \
	Parms.Frame=Frame; \
	OnBeforePopup.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_19_DELEGATE \
struct WebBrowser_eventOnUrlChanged_Parms \
{ \
	FText Text; \
}; \
static inline void FOnUrlChanged_DelegateWrapper(const FMulticastScriptDelegate& OnUrlChanged, FText const& Text) \
{ \
	WebBrowser_eventOnUrlChanged_Parms Parms; \
	Parms.Text=Text; \
	OnUrlChanged.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetUrl); \
	DECLARE_FUNCTION(execGetTitleText); \
	DECLARE_FUNCTION(execExecuteJavascript); \
	DECLARE_FUNCTION(execLoadString); \
	DECLARE_FUNCTION(execLoadURL);


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetUrl); \
	DECLARE_FUNCTION(execGetTitleText); \
	DECLARE_FUNCTION(execExecuteJavascript); \
	DECLARE_FUNCTION(execLoadString); \
	DECLARE_FUNCTION(execLoadURL);


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWebBrowser(); \
	friend struct Z_Construct_UClass_UWebBrowser_Statics; \
public: \
	DECLARE_CLASS(UWebBrowser, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WebBrowserWidget"), NO_API) \
	DECLARE_SERIALIZER(UWebBrowser)


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUWebBrowser(); \
	friend struct Z_Construct_UClass_UWebBrowser_Statics; \
public: \
	DECLARE_CLASS(UWebBrowser, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WebBrowserWidget"), NO_API) \
	DECLARE_SERIALIZER(UWebBrowser)


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWebBrowser(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWebBrowser) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWebBrowser); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWebBrowser); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWebBrowser(UWebBrowser&&); \
	NO_API UWebBrowser(const UWebBrowser&); \
public:


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWebBrowser(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWebBrowser(UWebBrowser&&); \
	NO_API UWebBrowser(const UWebBrowser&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWebBrowser); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWebBrowser); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWebBrowser)


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__InitialURL() { return STRUCT_OFFSET(UWebBrowser, InitialURL); } \
	FORCEINLINE static uint32 __PPO__bSupportsTransparency() { return STRUCT_OFFSET(UWebBrowser, bSupportsTransparency); }


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_13_PROLOG
#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_INCLASS \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class WebBrowser."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WEBBROWSERWIDGET_API UClass* StaticClass<class UWebBrowser>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowser_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
