// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WEBBROWSERWIDGET_WebBrowserAssetManager_generated_h
#error "WebBrowserAssetManager.generated.h already included, missing '#pragma once' in WebBrowserAssetManager.h"
#endif
#define WEBBROWSERWIDGET_WebBrowserAssetManager_generated_h

#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_RPC_WRAPPERS
#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWebBrowserAssetManager(); \
	friend struct Z_Construct_UClass_UWebBrowserAssetManager_Statics; \
public: \
	DECLARE_CLASS(UWebBrowserAssetManager, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WebBrowserWidget"), NO_API) \
	DECLARE_SERIALIZER(UWebBrowserAssetManager)


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUWebBrowserAssetManager(); \
	friend struct Z_Construct_UClass_UWebBrowserAssetManager_Statics; \
public: \
	DECLARE_CLASS(UWebBrowserAssetManager, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WebBrowserWidget"), NO_API) \
	DECLARE_SERIALIZER(UWebBrowserAssetManager)


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWebBrowserAssetManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWebBrowserAssetManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWebBrowserAssetManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWebBrowserAssetManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWebBrowserAssetManager(UWebBrowserAssetManager&&); \
	NO_API UWebBrowserAssetManager(const UWebBrowserAssetManager&); \
public:


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWebBrowserAssetManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWebBrowserAssetManager(UWebBrowserAssetManager&&); \
	NO_API UWebBrowserAssetManager(const UWebBrowserAssetManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWebBrowserAssetManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWebBrowserAssetManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWebBrowserAssetManager)


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DefaultMaterial() { return STRUCT_OFFSET(UWebBrowserAssetManager, DefaultMaterial); }


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_13_PROLOG
#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_INCLASS \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class WebBrowserAssetManager."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WEBBROWSERWIDGET_API UClass* StaticClass<class UWebBrowserAssetManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_WebBrowserWidget_Source_WebBrowserWidget_Public_WebBrowserAssetManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
