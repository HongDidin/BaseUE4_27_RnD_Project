// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleImageUtilsBlueprintSupport/Classes/AppleImageUtilsBlueprintSupport.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleImageUtilsBlueprintSupport() {}
// Cross Module References
	APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* Z_Construct_UClass_UK2Node_ConvertToJPEG_NoRegister();
	APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* Z_Construct_UClass_UK2Node_ConvertToJPEG();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_BaseAsyncTask();
	UPackage* Z_Construct_UPackage__Script_AppleImageUtilsBlueprintSupport();
	APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* Z_Construct_UClass_UK2Node_ConvertToHEIF_NoRegister();
	APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* Z_Construct_UClass_UK2Node_ConvertToHEIF();
	APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* Z_Construct_UClass_UK2Node_ConvertToTIFF_NoRegister();
	APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* Z_Construct_UClass_UK2Node_ConvertToTIFF();
	APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* Z_Construct_UClass_UK2Node_ConvertToPNG_NoRegister();
	APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* Z_Construct_UClass_UK2Node_ConvertToPNG();
// End Cross Module References
	void UK2Node_ConvertToJPEG::StaticRegisterNativesUK2Node_ConvertToJPEG()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_ConvertToJPEG_NoRegister()
	{
		return UK2Node_ConvertToJPEG::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_ConvertToJPEG_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_ConvertToJPEG_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_BaseAsyncTask,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleImageUtilsBlueprintSupport,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_ConvertToJPEG_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleImageUtilsBlueprintSupport.h" },
		{ "ModuleRelativePath", "Classes/AppleImageUtilsBlueprintSupport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_ConvertToJPEG_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_ConvertToJPEG>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_ConvertToJPEG_Statics::ClassParams = {
		&UK2Node_ConvertToJPEG::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_ConvertToJPEG_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_ConvertToJPEG_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_ConvertToJPEG()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_ConvertToJPEG_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_ConvertToJPEG, 1262631632);
	template<> APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* StaticClass<UK2Node_ConvertToJPEG>()
	{
		return UK2Node_ConvertToJPEG::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_ConvertToJPEG(Z_Construct_UClass_UK2Node_ConvertToJPEG, &UK2Node_ConvertToJPEG::StaticClass, TEXT("/Script/AppleImageUtilsBlueprintSupport"), TEXT("UK2Node_ConvertToJPEG"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_ConvertToJPEG);
	void UK2Node_ConvertToHEIF::StaticRegisterNativesUK2Node_ConvertToHEIF()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_ConvertToHEIF_NoRegister()
	{
		return UK2Node_ConvertToHEIF::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_ConvertToHEIF_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_ConvertToHEIF_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_BaseAsyncTask,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleImageUtilsBlueprintSupport,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_ConvertToHEIF_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleImageUtilsBlueprintSupport.h" },
		{ "ModuleRelativePath", "Classes/AppleImageUtilsBlueprintSupport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_ConvertToHEIF_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_ConvertToHEIF>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_ConvertToHEIF_Statics::ClassParams = {
		&UK2Node_ConvertToHEIF::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_ConvertToHEIF_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_ConvertToHEIF_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_ConvertToHEIF()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_ConvertToHEIF_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_ConvertToHEIF, 2052496979);
	template<> APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* StaticClass<UK2Node_ConvertToHEIF>()
	{
		return UK2Node_ConvertToHEIF::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_ConvertToHEIF(Z_Construct_UClass_UK2Node_ConvertToHEIF, &UK2Node_ConvertToHEIF::StaticClass, TEXT("/Script/AppleImageUtilsBlueprintSupport"), TEXT("UK2Node_ConvertToHEIF"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_ConvertToHEIF);
	void UK2Node_ConvertToTIFF::StaticRegisterNativesUK2Node_ConvertToTIFF()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_ConvertToTIFF_NoRegister()
	{
		return UK2Node_ConvertToTIFF::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_ConvertToTIFF_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_ConvertToTIFF_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_BaseAsyncTask,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleImageUtilsBlueprintSupport,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_ConvertToTIFF_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleImageUtilsBlueprintSupport.h" },
		{ "ModuleRelativePath", "Classes/AppleImageUtilsBlueprintSupport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_ConvertToTIFF_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_ConvertToTIFF>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_ConvertToTIFF_Statics::ClassParams = {
		&UK2Node_ConvertToTIFF::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_ConvertToTIFF_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_ConvertToTIFF_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_ConvertToTIFF()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_ConvertToTIFF_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_ConvertToTIFF, 1113405431);
	template<> APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* StaticClass<UK2Node_ConvertToTIFF>()
	{
		return UK2Node_ConvertToTIFF::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_ConvertToTIFF(Z_Construct_UClass_UK2Node_ConvertToTIFF, &UK2Node_ConvertToTIFF::StaticClass, TEXT("/Script/AppleImageUtilsBlueprintSupport"), TEXT("UK2Node_ConvertToTIFF"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_ConvertToTIFF);
	void UK2Node_ConvertToPNG::StaticRegisterNativesUK2Node_ConvertToPNG()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_ConvertToPNG_NoRegister()
	{
		return UK2Node_ConvertToPNG::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_ConvertToPNG_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_ConvertToPNG_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_BaseAsyncTask,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleImageUtilsBlueprintSupport,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_ConvertToPNG_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AppleImageUtilsBlueprintSupport.h" },
		{ "ModuleRelativePath", "Classes/AppleImageUtilsBlueprintSupport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_ConvertToPNG_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_ConvertToPNG>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_ConvertToPNG_Statics::ClassParams = {
		&UK2Node_ConvertToPNG::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_ConvertToPNG_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_ConvertToPNG_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_ConvertToPNG()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_ConvertToPNG_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_ConvertToPNG, 2702030389);
	template<> APPLEIMAGEUTILSBLUEPRINTSUPPORT_API UClass* StaticClass<UK2Node_ConvertToPNG>()
	{
		return UK2Node_ConvertToPNG::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_ConvertToPNG(Z_Construct_UClass_UK2Node_ConvertToPNG, &UK2Node_ConvertToPNG::StaticClass, TEXT("/Script/AppleImageUtilsBlueprintSupport"), TEXT("UK2Node_ConvertToPNG"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_ConvertToPNG);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
