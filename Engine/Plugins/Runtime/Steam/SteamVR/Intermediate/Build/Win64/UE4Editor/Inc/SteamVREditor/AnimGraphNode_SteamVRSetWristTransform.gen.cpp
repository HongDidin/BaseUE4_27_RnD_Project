// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamVREditor/Public/AnimGraphNode_SteamVRSetWristTransform.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimGraphNode_SteamVRSetWristTransform() {}
// Cross Module References
	STEAMVREDITOR_API UClass* Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_NoRegister();
	STEAMVREDITOR_API UClass* Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform();
	ANIMGRAPH_API UClass* Z_Construct_UClass_UAnimGraphNode_Base();
	UPackage* Z_Construct_UPackage__Script_SteamVREditor();
	STEAMVRINPUTDEVICE_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_SteamVRSetWristTransform();
// End Cross Module References
	void UAnimGraphNode_SteamVRSetWristTransform::StaticRegisterNativesUAnimGraphNode_SteamVRSetWristTransform()
	{
	}
	UClass* Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_NoRegister()
	{
		return UAnimGraphNode_SteamVRSetWristTransform::StaticClass();
	}
	struct Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Node_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Node;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimGraphNode_Base,
		(UObject* (*)())Z_Construct_UPackage__Script_SteamVREditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AnimGraphNode_SteamVRSetWristTransform.h" },
		{ "ModuleRelativePath", "Public/AnimGraphNode_SteamVRSetWristTransform.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::NewProp_Node_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimGraphNode_SteamVRSetWristTransform.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::NewProp_Node = { "Node", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAnimGraphNode_SteamVRSetWristTransform, Node), Z_Construct_UScriptStruct_FAnimNode_SteamVRSetWristTransform, METADATA_PARAMS(Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::NewProp_Node_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::NewProp_Node_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::NewProp_Node,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAnimGraphNode_SteamVRSetWristTransform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::ClassParams = {
		&UAnimGraphNode_SteamVRSetWristTransform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAnimGraphNode_SteamVRSetWristTransform, 581162432);
	template<> STEAMVREDITOR_API UClass* StaticClass<UAnimGraphNode_SteamVRSetWristTransform>()
	{
		return UAnimGraphNode_SteamVRSetWristTransform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAnimGraphNode_SteamVRSetWristTransform(Z_Construct_UClass_UAnimGraphNode_SteamVRSetWristTransform, &UAnimGraphNode_SteamVRSetWristTransform::StaticClass, TEXT("/Script/SteamVREditor"), TEXT("UAnimGraphNode_SteamVRSetWristTransform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAnimGraphNode_SteamVRSetWristTransform);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
