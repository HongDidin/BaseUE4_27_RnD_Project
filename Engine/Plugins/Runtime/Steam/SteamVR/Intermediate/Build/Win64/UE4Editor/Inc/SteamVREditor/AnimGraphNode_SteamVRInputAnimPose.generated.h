// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STEAMVREDITOR_AnimGraphNode_SteamVRInputAnimPose_generated_h
#error "AnimGraphNode_SteamVRInputAnimPose.generated.h already included, missing '#pragma once' in AnimGraphNode_SteamVRInputAnimPose.h"
#endif
#define STEAMVREDITOR_AnimGraphNode_SteamVRInputAnimPose_generated_h

#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_SPARSE_DATA
#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnimGraphNode_SteamVRInputAnimPose(); \
	friend struct Z_Construct_UClass_UAnimGraphNode_SteamVRInputAnimPose_Statics; \
public: \
	DECLARE_CLASS(UAnimGraphNode_SteamVRInputAnimPose, UAnimGraphNode_Base, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SteamVREditor"), NO_API) \
	DECLARE_SERIALIZER(UAnimGraphNode_SteamVRInputAnimPose)


#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_INCLASS \
private: \
	static void StaticRegisterNativesUAnimGraphNode_SteamVRInputAnimPose(); \
	friend struct Z_Construct_UClass_UAnimGraphNode_SteamVRInputAnimPose_Statics; \
public: \
	DECLARE_CLASS(UAnimGraphNode_SteamVRInputAnimPose, UAnimGraphNode_Base, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SteamVREditor"), NO_API) \
	DECLARE_SERIALIZER(UAnimGraphNode_SteamVRInputAnimPose)


#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnimGraphNode_SteamVRInputAnimPose(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimGraphNode_SteamVRInputAnimPose) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnimGraphNode_SteamVRInputAnimPose); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimGraphNode_SteamVRInputAnimPose); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnimGraphNode_SteamVRInputAnimPose(UAnimGraphNode_SteamVRInputAnimPose&&); \
	NO_API UAnimGraphNode_SteamVRInputAnimPose(const UAnimGraphNode_SteamVRInputAnimPose&); \
public:


#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnimGraphNode_SteamVRInputAnimPose(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnimGraphNode_SteamVRInputAnimPose(UAnimGraphNode_SteamVRInputAnimPose&&); \
	NO_API UAnimGraphNode_SteamVRInputAnimPose(const UAnimGraphNode_SteamVRInputAnimPose&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnimGraphNode_SteamVRInputAnimPose); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimGraphNode_SteamVRInputAnimPose); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimGraphNode_SteamVRInputAnimPose)


#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_40_PROLOG
#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_INCLASS \
	Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h_43_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AnimGraphNode_SteamVRInputAnimPose."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STEAMVREDITOR_API UClass* StaticClass<class UAnimGraphNode_SteamVRInputAnimPose>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Steam_SteamVR_Source_SteamVREditor_Public_AnimGraphNode_SteamVRInputAnimPose_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
